﻿using UnityEngine;
using System.Collections.Generic;
using UnityEditor;
using System.IO;
using System.Xml.Serialization;
using System.Linq;

[CustomEditor(typeof(AbilitiesLibrary))]
public class AbilitiesLibraryEditor : Editor
{
    public override void OnInspectorGUI()
    {
        DrawDefaultInspector();

        AbilitiesLibrary library = (AbilitiesLibrary)target;
        if (GUILayout.Button("Parse To XML"))
        {
			// Todo move path to separate constant
            File.WriteAllText("../Server/WebSocketServer/Common/Resources/abilities.xml", library.Save());
            AssetDatabase.SaveAssets();
            AssetDatabase.Refresh();
        }

        if(GUILayout.Button("Get From XML"))
        {
            List<Common.ClientData> loadedResources = Common.XmlSerializer.Deserialize<List<Common.ClientData>>(library.AbilitiesXML.text);
            if (loadedResources != null)
            {
                foreach(var data in loadedResources)
                {
                    Ability ability = library.Abilities.Find(a => a.Id == data.Id);
                    if(ability != null)
                    {
                        ability.Values = data.Values;
                    }
                }
            }
            AssetDatabase.SaveAssets();
            AssetDatabase.Refresh();
        }

        if (GUILayout.Button("Copy"))
        {
            var asset = ScriptableObject.CreateInstance<AbilitiesLibrary>();

            AssetDatabase.CreateAsset(asset, "Assets/AbilitiesLibrary.asset");

            foreach (var ability in library.Abilities)
            {
                ability.DescriptionUpgradeKey = ability.DescriptionKey + "Upgrade";
            }

            asset.Abilities = library.Abilities;

            AssetDatabase.SaveAssets();
            AssetDatabase.Refresh();
        }
    }
}
