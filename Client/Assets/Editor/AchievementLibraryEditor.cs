﻿using UnityEngine;
using System.Collections.Generic;
using UnityEditor;
using System.IO;
using System.Xml.Serialization;
using System.Linq;

[CustomEditor(typeof(AchievementLibrary))]
public class AchievementLibraryEditor : Editor
{
    public override void OnInspectorGUI()
    {
        DrawDefaultInspector();

        AchievementLibrary library = (AchievementLibrary)target;

        if (GUILayout.Button("Get From XML"))
        {
            var asset = ScriptableObject.CreateInstance<AchievementLibrary>();

            AssetDatabase.CreateAsset(asset, "Assets/AchievementLibrary.asset");

            asset.Achievements = new List<Achievement>();
            List<Common.Resources.Achievement> loadedResources = Common.XmlSerializer.Deserialize<List<Common.Resources.Achievement>>(library.AchievementsXML.text);
            if (loadedResources != null)
            {
                foreach (var data in loadedResources)
                {
                    Achievement achievement = new Achievement();
                    if (achievement != null)
                    {
                        achievement.Steps = new List<Achievement.AchievementStep>();
                        foreach (var step in data.Steps)
                        {
                            var s = new Achievement.AchievementStep();
                            s.Quantity = step.Quantity;
                            s.RewardId = step.RewardId;
                            s.Threshold = step.Threshold;
                            achievement.Steps.Add(s);
                        }
                        
                    }
                    asset.Achievements.Add(achievement);
                }
            }
            AssetDatabase.SaveAssets();
            AssetDatabase.Refresh();
        }
    }
}
