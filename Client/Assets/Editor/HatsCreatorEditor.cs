﻿using UnityEngine;
using System.Collections;
using UnityEditor;

[CustomEditor(typeof(HatsCreator))]
public class HatsCreatorEditor : Editor
{
    public override void OnInspectorGUI()
    {
        DrawDefaultInspector();

        var creator = (HatsCreator)target;
        if (GUILayout.Button("Create"))
        {
            creator.Create();
        }
    }
}
