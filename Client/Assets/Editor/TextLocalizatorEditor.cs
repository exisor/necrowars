﻿using UnityEngine;
using System.Collections;
using UnityEditor;
using UnityEngine.UI;

[CustomEditor(typeof(TextLocalizator))]
public class TextLocalizatorEditor : Editor
{
    public override void OnInspectorGUI()
    {
        DrawDefaultInspector();

        if (GUILayout.Button("Show Library"))
        {
            var library = (ScriptableObject)AssetDatabase.LoadAssetAtPath("Assets/Resources/Libraries/localization/GUI.asset", typeof(ScriptableObject));
            var localizator = (TextLocalizator)target;

            if (localizator.Library != null)
            {
                library = localizator.Library;
            }

            Selection.activeObject = AssetDatabase.LoadMainAssetAtPath("Assets/Resources/Libraries/localization/" + library.name + ".asset");
        }

        if (GUILayout.Button("Add to Library"))
        {
            var library = (LocalizationLibrary)AssetDatabase.LoadAssetAtPath("Assets/Resources/Libraries/localization/GUI.asset", typeof(LocalizationLibrary));
            //var library = (LocalizationLibrary)AssetDatabase.LoadAssetAtPath("Assets/Resources/Libraries/localization/Tutorial.asset", typeof(LocalizationLibrary));
            var localizator = (TextLocalizator)target;

            if(localizator.Library != null)
            {
                library = localizator.Library;
            }

            string libraryName = library.name;
            var datas = library.Datas;

            string text = localizator.GetComponent<Text>().text;

            LocalizationData data = new LocalizationData();
            
            if(string.IsNullOrEmpty(localizator.Key))
            {
                data.Key = text;
            }
            else
            {
                data.Key = localizator.Key;
            } 
            
            data.ENG = text;

            var dataInLibrary = datas.Find(d => d.Key == data.Key);
            if (dataInLibrary == null)
            {
                datas.Add(data);
            }
            else
            {
                dataInLibrary.ENG = text;
            }
            localizator.Key = data.Key;

            /*
            AssetDatabase.MoveAsset("Assets/Resources/Libraries/localization/GUI.asset", "Assets/GUI.asset");
            AssetDatabase.MoveAsset("Assets/GUI.asset", "Assets/Resources/Libraries/localization/GUI.asset");*/

            /*AssetDatabase.MoveAsset("Assets/Resources/Libraries/localization/Tutorial.asset", "Assets/Tutorial.asset");
            AssetDatabase.MoveAsset("Assets/Tutorial.asset", "Assets/Resources/Libraries/localization/Tutorial.asset");*/

            AssetDatabase.MoveAsset("Assets/Resources/Libraries/localization/" + libraryName + ".asset", "Assets/" + libraryName + ".asset");
            AssetDatabase.MoveAsset("Assets/" + libraryName + ".asset", "Assets/Resources/Libraries/localization/" + libraryName + ".asset");

            AssetDatabase.SaveAssets();
            AssetDatabase.Refresh();
        }
    }
}
