﻿using UnityEngine;
using System.Collections;
using UnityEditor;

[CustomEditor(typeof(TileCreator))]
public class TileCreatorEditor : Editor
{
    public override void OnInspectorGUI()
    {
        DrawDefaultInspector();

        var creator = (TileCreator)target;
        if (GUILayout.Button("Generate"))
        {
            creator.Generate();
        }
        if (GUILayout.Button("Clear"))
        {
            creator.Clear();
        }
    }
}
