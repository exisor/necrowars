﻿using UnityEngine;
using System.Collections;
using UnityEditor;
using System.IO;
using System.Text;
using System.Xml.Serialization;

[CustomEditor(typeof(ItemsLibrary))]
public class ItemsLibraryEditor : Editor
{
    public override void OnInspectorGUI()
    {
        DrawDefaultInspector();

        ItemsLibrary library = (ItemsLibrary)target;
        if (GUILayout.Button("Parse To XML"))
        {
            File.WriteAllText("../Server/WebSocketServer/Common/Resources/items.xml", library.Save(), Encoding.Unicode);
            AssetDatabase.SaveAssets();
            AssetDatabase.Refresh();
        }
        if (GUILayout.Button("Copy"))
        {
            var asset = ScriptableObject.CreateInstance<ItemsLibrary>();

            AssetDatabase.CreateAsset(asset, "Assets/ItemsLibrary.asset");

            asset.Items = library.Items;
            asset.CurrencyItems = library.CurrencyItems;
            asset.Rarity = library.Rarity;

            AssetDatabase.SaveAssets();
            AssetDatabase.Refresh();
        }
    }
}
