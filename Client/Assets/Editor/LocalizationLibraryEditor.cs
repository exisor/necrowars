﻿using UnityEngine;
using System.Collections.Generic;
using UnityEditor;
using System.IO;
using System.Xml.Serialization;
using System.Linq;
using System.IO;
using System.Text;

[CustomEditor(typeof(LocalizationLibrary))]
public class LocalizationLibraryEditor : Editor
{
    public override void OnInspectorGUI()
    {
        DrawDefaultInspector();

        LocalizationLibrary library = (LocalizationLibrary)target;
        if (GUILayout.Button("Parse To XML"))
        {
            var serializer = new XmlSerializer(typeof(List<LocalizationData>));
            var stringWriter = new StringWriter();
            serializer.Serialize(stringWriter, library.Datas);
            File.WriteAllText("../Localization/" + target.name + ".xml", stringWriter.ToString(), Encoding.Unicode);
            AssetDatabase.SaveAssets();
            AssetDatabase.Refresh();
        }
        if (GUILayout.Button("Copy"))
        {
            var asset = ScriptableObject.CreateInstance<LocalizationLibrary>();

            AssetDatabase.CreateAsset(asset, "Assets/Abilities.asset");

            List<LocalizationData> datas = new List<LocalizationData>();
            foreach (var data in library.Datas)
            {
                var newData = data.Clone();
                datas.Add(newData);
            }
            foreach (var data in library.Datas)
            {
                if(data.Key.IndexOf("Descr") != -1)
                {
                    var newData = data.Clone();
                    newData.Key += "Upgrade";
                    datas.Add(newData);
                }
            }

            asset.Datas = datas;

            AssetDatabase.SaveAssets();
            AssetDatabase.Refresh();

            Selection.activeObject = asset;
        }
    }
}
