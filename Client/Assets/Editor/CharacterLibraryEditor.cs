﻿using UnityEngine;
using System.Collections;
using UnityEditor;
using System.IO;
using System.Text;
using System.Xml.Serialization;

[CustomEditor(typeof(CharactersLibrary))]
public class CharactersLibraryEditor : Editor
{
    public override void OnInspectorGUI()
    {
        DrawDefaultInspector();

        CharactersLibrary library = (CharactersLibrary)target;
        if (GUILayout.Button("Parse To XML"))
        {
            File.WriteAllText("../Server/WebSocketServer/Common/Resources/characters.xml", library.Save(), Encoding.Unicode);
            AssetDatabase.SaveAssets();
            AssetDatabase.Refresh();
        }
    }
}
