﻿using UnityEngine;
using System.Collections.Generic;
using UnityEditor;
using System.IO;
using System.Xml.Serialization;
using System.Linq;

[CustomEditor(typeof(QuestLibrary))]
public class QuestLibraryEditor : Editor
{
    public override void OnInspectorGUI()
    {
        DrawDefaultInspector();

        QuestLibrary library = (QuestLibrary)target;

        if (GUILayout.Button("Get From XML"))
        {
            List<Common.Resources.DailyQuest> loadedResources = Common.XmlSerializer.Deserialize<List<Common.Resources.DailyQuest>>(library.QuestsXML.text);
            if (loadedResources != null)
            {
                foreach (var data in loadedResources)
                {
                    Quest quest = library.Quests.Find(a => a.Id == data.Id);
                    if (quest != null)
                    {
                        quest.Threshold = data.Threshold;
                    }
                }
            }
            AssetDatabase.SaveAssets();
            AssetDatabase.Refresh();
        }
    }
}
