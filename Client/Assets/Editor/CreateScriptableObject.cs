﻿using UnityEditor;
using UnityEngine;
using UnityEngine.EventSystems;
using System.Collections.Generic;
using System.IO;
using System.Linq;

[CustomEditor(typeof(AbilitiesLibrary)), CanEditMultipleObjects]
public class CreateScriptableObject : Editor
{
    [MenuItem("Assets/Create/Abilities library")]
    public static void CreateAbilitiesLibrary()
    {		
		var asset = ScriptableObject.CreateInstance<AbilitiesLibrary>();

        AssetDatabase.CreateAsset(asset, "Assets/AbilitiesLibrary.asset");
        AssetDatabase.SaveAssets();

        EditorUtility.FocusProjectWindow();

        Selection.activeObject = asset;
    }

    [MenuItem("Assets/Create/Characters library")]
    public static void CreateCharactersLibrary()
    {
        var asset = ScriptableObject.CreateInstance<CharactersLibrary>();

        AssetDatabase.CreateAsset(asset, "Assets/CharactersLibrary.asset");
        AssetDatabase.SaveAssets();

        EditorUtility.FocusProjectWindow();

        Selection.activeObject = asset;
    }

    [MenuItem("Assets/Create/Items library")]
    public static void CreateItemsLibrary()
    {
        var asset = ScriptableObject.CreateInstance<ItemsLibrary>();

        AssetDatabase.CreateAsset(asset, "Assets/ItemsLibrary.asset");
        AssetDatabase.SaveAssets();

        EditorUtility.FocusProjectWindow();

        Selection.activeObject = asset;
    }

    [MenuItem("Assets/Create/Localization library")]
    public static void CreateLocalizationLibrary()
    {
        var asset = ScriptableObject.CreateInstance<LocalizationLibrary>();

        AssetDatabase.CreateAsset(asset, "Assets/LocalizationLibrary.asset");
        AssetDatabase.SaveAssets();

        EditorUtility.FocusProjectWindow();

        Selection.activeObject = asset;
    }

    [MenuItem("Assets/Create/PowerUp library")]
    public static void CreatePowerUpLibrary()
    {
        var asset = ScriptableObject.CreateInstance<PowerUpLibrary>();

        AssetDatabase.CreateAsset(asset, "Assets/PowerUpLibrary.asset");
        AssetDatabase.SaveAssets();

        EditorUtility.FocusProjectWindow();

        Selection.activeObject = asset;
    }

    [MenuItem("Assets/Create/Chest library")]
    public static void CreateChestLibrary()
    {
        var asset = ScriptableObject.CreateInstance<ChestLibrary>();

        AssetDatabase.CreateAsset(asset, "Assets/ChestLibrary.asset");
        AssetDatabase.SaveAssets();

        EditorUtility.FocusProjectWindow();

        Selection.activeObject = asset;
    }

    [MenuItem("Assets/Create/Achievement library")]
    public static void CreateAchievementLibrary()
    {
        var asset = ScriptableObject.CreateInstance<AchievementLibrary>();

        AssetDatabase.CreateAsset(asset, "Assets/AchievementLibrary.asset");
        AssetDatabase.SaveAssets();

        EditorUtility.FocusProjectWindow();

        Selection.activeObject = asset;
    }

    [MenuItem("Assets/Create/Quests library")]
    public static void CreateQuestLibrary()
    {
        var asset = ScriptableObject.CreateInstance<QuestLibrary>();

        AssetDatabase.CreateAsset(asset, "Assets/QuestLibrary.asset");
        AssetDatabase.SaveAssets();

        EditorUtility.FocusProjectWindow();

        Selection.activeObject = asset;
    }

    [MenuItem("Assets/Create/Player Event Library")]
    public static void CreatePlayerEventLibrary()
    {
        var asset = ScriptableObject.CreateInstance<PlayerEventLibrary>();

        AssetDatabase.CreateAsset(asset, "Assets/PlayerEventLibrary.asset");
        AssetDatabase.SaveAssets();

        EditorUtility.FocusProjectWindow();

        Selection.activeObject = asset;
    }

    [MenuItem("Assets/Create/Set Library")]
    public static void CreateSetLibrary()
    {
        var asset = ScriptableObject.CreateInstance<SetLibrary>();

        AssetDatabase.CreateAsset(asset, "Assets/SetLibrary.asset");
        AssetDatabase.SaveAssets();

        EditorUtility.FocusProjectWindow();

        Selection.activeObject = asset;
    }

    [MenuItem("Assets/Create/Affix library")]
    public static void CreateAffixesLibrary()
    {
        var asset = ScriptableObject.CreateInstance<AffixesLibrary>();

        AssetDatabase.CreateAsset(asset, "Assets/AffixesLibrary.asset");
        AssetDatabase.SaveAssets();

        EditorUtility.FocusProjectWindow();

        Selection.activeObject = asset;
    }

    [MenuItem("Assets/Create/Item Name Prefix library")]
    public static void CreateItemPrefixLibrary()
    {
        var asset = ScriptableObject.CreateInstance<ItemNamePrefixLibrary>();

        var sr = File.OpenText("Assets/Resources/Libraries/localization/ITEMS_PRE_RU.txt");
        asset.Create(sr.ReadToEnd().Split("\n"[0]).ToList());
        sr.Close();

        AssetDatabase.CreateAsset(asset, "Assets/ItemNamePrefixLibrary.asset");
        AssetDatabase.SaveAssets();

        EditorUtility.FocusProjectWindow();

        Selection.activeObject = asset;
    }

    [MenuItem("Assets/Create/Item Name Suffix library")]
    public static void CreateItemSuffixLibrary()
    {
        var asset = ScriptableObject.CreateInstance<ItemNameSuffixLibrary>();

        var sr = File.OpenText("Assets/Resources/Libraries/localization/ITEMS_SUFFIX_RU.txt");
        asset.Create(sr.ReadToEnd().Split("\n"[0]).ToList());
        sr.Close();

        AssetDatabase.CreateAsset(asset, "Assets/ItemNameSuffixLibrary.asset");
        AssetDatabase.SaveAssets();

        EditorUtility.FocusProjectWindow();

        Selection.activeObject = asset;
    }




    [MenuItem("Assets/Create/Tooltip/GetAbilityTooltip")]
    public static void CreateGetAbilityTooltip()
    {
        var asset = ScriptableObject.CreateInstance<GetAbilityTooltip>();

        AssetDatabase.CreateAsset(asset, "Assets/GetAbilityTooltip.asset");
        AssetDatabase.SaveAssets();

        EditorUtility.FocusProjectWindow();

        Selection.activeObject = asset;
    }

    [MenuItem("Assets/Create/Tooltip/AddModToAbilityTooltip")]
    public static void CreateAddModToAbilityTooltip()
    {
        var asset = ScriptableObject.CreateInstance<AddModToAbilityTooltip>();

        AssetDatabase.CreateAsset(asset, "Assets/AddModToAbilityTooltip.asset");
        AssetDatabase.SaveAssets();

        EditorUtility.FocusProjectWindow();

        Selection.activeObject = asset;
    }

    [MenuItem("Assets/Create/Tooltip/TinitusTooltip")]
    public static void CreateTinitusTooltip()
    {
        var asset = ScriptableObject.CreateInstance<TinitusTooltip>();

        AssetDatabase.CreateAsset(asset, "Assets/TinitusTooltip.asset");
        AssetDatabase.SaveAssets();

        EditorUtility.FocusProjectWindow();

        Selection.activeObject = asset;
    }

    [MenuItem("Assets/Create/Items/GoldLogic")]
    public static void CreateRarityLogic()
    {
        var asset = ScriptableObject.CreateInstance<GoldLogic>();

        AssetDatabase.CreateAsset(asset, "Assets/GoldLogic.asset");
        AssetDatabase.SaveAssets();

        EditorUtility.FocusProjectWindow();

        Selection.activeObject = asset;
    }
}