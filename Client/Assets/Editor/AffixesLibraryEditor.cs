﻿using UnityEngine;
using System.Collections.Generic;
using UnityEditor;
using System.IO;
using System.Xml.Serialization;
using System.Linq;

[CustomEditor(typeof(AffixesLibrary))]
public class AffixesLibraryEditor : Editor
{
    public override void OnInspectorGUI()
    {
        DrawDefaultInspector();

        AffixesLibrary library = (AffixesLibrary)target;

        if (GUILayout.Button("Copy"))
        {
            var asset = ScriptableObject.CreateInstance<AffixesLibrary>();

            AssetDatabase.CreateAsset(asset, "Assets/AffixesLibrary.asset");

            foreach (var affix in library.Affixes)
            {
                affix.EnchantDescriptionKey = affix.DescriptionKey + "Enchant";
            }

            asset.Affixes = library.Affixes;

            AssetDatabase.SaveAssets();
            AssetDatabase.Refresh();
        }
    }
}
