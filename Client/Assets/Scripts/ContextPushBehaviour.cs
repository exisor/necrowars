﻿using UnityEngine;

namespace Assets.Scripts
{
    public class ContextPushBehaviour : MonoBehaviour
    {
        private bool _pushed;
        [SerializeField] private ContextType _context;

        private void Push()
        {
            if (!_pushed)
            {
                _pushed = true;
                Context.Switch(_context, ContextSwitch.Push);
            }
        }

        private void Pop()
        {
            if (_pushed)
            {
                _pushed = false;
                Context.Switch(_context, ContextSwitch.Pop);
            }
        }

        void Awake()
        {
            Push();
        }

        void OnEnable()
        {
            Push();   
        }

        void OnDisable()
        {
            Pop();
        }

        void OnDestroy()
        {
            Pop();
        }
    }
}
