﻿using UnityEngine;
using System;
using System.Collections;
using Assets.Scripts;
using UnityEngine.SceneManagement;
using System.Collections.Generic;
using System.Globalization;
using ClientProtocol;

public class ApplicationManager : MonoBehaviour 
{
    static public int MIN_FRAMERATE = 20;
	public static ApplicationManager Instance = null;

	public Action OnLoadLevelCallback;

    public int FrameRate = 5;

    private float _deltaTime = 0.0f;

    void Awake () 
	{
		Instance = this;
        FrameRate = PlayerPrefs.GetInt("Graphics", MIN_FRAMERATE / 2);

        DontDestroyOnLoad(transform.gameObject);
    }

    void Start ()
    {
        Analytics.gua.sendEventHit("Loading", "PreloadFinished", "", 0);
#if DEBUG
        var debugInterface = Resources.Load<GameObject>("Prefabs/UI/Debug/DebugInterface");
        Instantiate(debugInterface);
#endif

#if UNITY_EDITOR || !UNITY_WEBGL
        DefaultStart();
#endif
#if !UNITY_EDITOR && UNITY_WEBGL
         Application.ExternalEval(
          @"
            if(window.location.host.search('kong') != -1)
            {
                function onKongLoadCompleted() 
                {
                    kongregate = kongregateAPI.getAPI();
                    
                    if(kongregate.services.isGuest())
                    {
                        kongregate.services.addEventListener('login', onKongregateInPageLogin);

                        function onKongregateInPageLogin() 
                        {
                            var params = kongregate.services.getUserId() + '|' +
                                         kongregate.services.getUsername() + '|' +
                                         kongregate.services.getGameAuthToken();
                            SendMessage('Managers', 'OnKongregateAPILoaded', params);
                        }
                        SendMessage('Managers', 'OnKongregateGuest', params);
                    }
                    else
                    {
                        var params = kongregate.services.getUserId() + '|' +
                                     kongregate.services.getUsername() + '|' +
                                     kongregate.services.getGameAuthToken();

                        console.log(params);
                        SendMessage('Managers', 'OnKongregateAPILoaded', params);
                    }
                }
                SendMessage('Managers', 'SetEnglish');
                kongregateAPI.loadAPI(onKongLoadCompleted);
            }
            else if(window.location.search != '')
            {
                function onOGZInitCompleted(success)
                {
                    function onOGZUserCompleted(json)
                    {
                        params = {};
                        var query = window.location.search.substring(1);

                        var query_elements = query.split('&');
                        for (var i = 0; i < query_elements.length; i++) {
                            s = query_elements[i].split('=');
                            key = decodeURIComponent(s[0]);
                            val = decodeURIComponent(s[1]);
                            params[key] = val;
                        }

                        SendMessage('Managers', 'OnOGZAPILoaded', json.data.uid.toString() + '|' + json.data.first_name  + '|' 
                        + params['access_token'] + '|' + json.data.pic + '|' + params['platform']);
                    }
                    OGZ.callAPI('users/current/', {}, onOGZUserCompleted);
                }
                OGZ.init(onOGZInitCompleted);
            }
            else
            {
                SendMessage('Managers', 'DebugUrl', 'default' + window.location.href);
                SendMessage('Managers', 'DefaultStart');
            };"
        );
#endif
    }

    public void DebugUrl(string url)
    {
        Camera.main.GetComponent<FPSDisplay>().message = " url " + url;
    }

    public void DefaultStart()
    {
        SocketManager.Instance.Connect();
    }

    public void SetEnglish()
    {
        Localization.SetCurrentLanguage(Localization.LanguageEnum.Eng);
    }

    public void OnOGZAPILoaded(string userInfoString)
    {
        Debug.Log(userInfoString);
        var info = userInfoString.Split('|');

        SocketManager.Instance.UserLogin = info[0];
        SocketManager.Instance.UserName = info[1];
        SocketManager.Instance.UserPassword = info[2];

        if(info[4] == "vk")
        {
            DataManager.Instance.Social = DataManager.SocialType.VK;
        }
        else if (info[4] == "ok")
        {
            DataManager.Instance.Social = DataManager.SocialType.OK;
        }
        else if (info[4] == "fb")
        {
            DataManager.Instance.Social = DataManager.SocialType.FB;
        }
        else if (info[4] == "mm")
        {
            DataManager.Instance.Social = DataManager.SocialType.MM;
        }

        SocketManager.Instance.Connect();

        StartCoroutine(NetHandler.Instance.LoadAvatar(info[3]));
    }

    public void OnKongregateGuest()
    {
        UIManager.Instance.ShowScreen(GameScreen.ScreenNameEnum.KongGuest);
    }

    public void OnKongregateAPILoaded(string userInfoString)
    {
        var info = userInfoString.Split('|');

        SocketManager.Instance.UserLogin = info[0];
        SocketManager.Instance.UserName = info[1];
        SocketManager.Instance.UserPassword = info[2];
        DataManager.Instance.Social = DataManager.SocialType.Kong;

        SocketManager.Instance.Connect();
    }

    public void OnItemList(string items)
    {
        Camera.main.GetComponent<FPSDisplay>().message = " result " + items;
    }

    public void Init()
    {
        if (DataManager.Instance.Social == DataManager.SocialType.Kong)
        {
            Application.ExternalCall("kongregate.stats.submit", "loaded", 1);
        }

        SceneManager.LoadScene("Lobby");
        SoundManager.PlayMusic("bgm2");
        
        UIManager.Instance.MainMenuView.ShowMain();

        UIManager.Instance.ShowScreen(Tutorial.GetGameScreen());
    }

    public void ShowLobby(Action callback)
    {
        OnLoadLevelCallback = callback;
        Debug.logger.logEnabled = true;
        SceneManager.LoadScene("Lobby");
        UIManager.Instance.MainMenuView.ShowMain();
    }

    public void ShowBattle(Action callback)
    {
        UIManager.Instance.ShowPreloader();
        OnLoadLevelCallback = callback;
        Debug.logger.logEnabled = false;
        SceneManager.LoadScene("Battle");
        UIManager.Instance.MainMenuView.ShowBattle();
    }

    public void ShowTutorial()
    {
        UIManager.Instance.ShowPreloader();
        SceneManager.LoadScene("Tutorial");
        UIManager.Instance.MainMenuView.ShowBattle();
    }

    public void LoadCallback()
	{
		if(OnLoadLevelCallback != null)
		{
			OnLoadLevelCallback();
			OnLoadLevelCallback = null;
		}
	}

    public void SetFrameRate(int frame)
    {
        FrameRate = frame;
        PlayerPrefs.SetInt("Graphics", FrameRate);
    }
}
