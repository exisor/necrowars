﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class ScrollableFitter : MonoBehaviour
{
    public int MinSize = 200;

    public GameObject UpArrow;
    public GameObject DownArrow;
    public RectTransform Child;
    private LayoutElement _layout;

    void Start()
    {
        _layout = GetComponent<LayoutElement>();
    }

    void OnEnable()
    {
        var pos = Child.anchoredPosition;
        pos.y = 0;
        Child.anchoredPosition = pos;
    }

    void Update ()
    {
        _layout.minHeight = Child.sizeDelta.y > MinSize ? MinSize : Child.sizeDelta.y;
        UpArrow.SetActive(Child.sizeDelta.y > MinSize && Child.anchoredPosition.y != 0);
        DownArrow.SetActive(Child.sizeDelta.y > MinSize && Child.anchoredPosition.y != Child.sizeDelta.y - MinSize);

        if (Child.sizeDelta.y > MinSize)
        {
            var pos = Child.anchoredPosition;
            pos.y -= Input.GetAxis("Mouse ScrollWheel") * 40;
            if(pos.y < 0)
            {
                pos.y = 0;
            }
            else if(pos.y > Child.sizeDelta.y - MinSize)
            {
                pos.y = Child.sizeDelta.y - MinSize;
            }
            Child.anchoredPosition = pos;
        }
    }
}
