﻿using UnityEngine;
using System.Collections;

public class AlwaysZeroRotation : MonoBehaviour
{	
	void Update ()
    {
        transform.eulerAngles = Vector3.zero;
	}
}
