﻿using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;

public class BlockGameInputBehavior: MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
{
    private bool _isOver = false;

    public void OnPointerEnter(PointerEventData eventData)
    {
        _isOver = true;
        UIManager.Instance.IsPointerOverGUI = true;
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        _isOver = false;
        UIManager.Instance.IsPointerOverGUI = false;
    }

    void OnDisable()
    {
        if(_isOver)
        {
            UIManager.Instance.IsPointerOverGUI = false;
        }
        _isOver = false;
    }
}
