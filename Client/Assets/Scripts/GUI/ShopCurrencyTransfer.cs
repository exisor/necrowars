﻿using UnityEngine;
using UnityEngine.UI;

public class ShopCurrencyTransfer : MonoBehaviour
{
    void Start()
    {
        GetComponent<Button>().onClick.AddListener(OnClick);
    }

    public void OnClick()
    {
        SocketManager.Instance.GetShopItems();
        UIManager.Instance.ShowScreen(GameScreen.ScreenNameEnum.Shop);
    }
}
