﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class BattleAbilityView : MonoBehaviour
{
    public delegate void AbilityClickAction(int slot);

    public Image Image;
    public Image CooldownImage;
    public AbilityClient AbilityClient;
    public ActiveAbility ActiveAbility;
    public KeyCode HotKey;

    public event AbilityClickAction EventAbilityClick;
    public event AbilityClickAction EventAbilityDown;
    public event AbilityClickAction EventAbilityUp;

    public void OnDisable()
    {
        SetAbility(null, null);
    }

    public void SetAbility(ActiveAbility activeAbility, AbilityClient abilityClient)
    {
        AbilityClient = abilityClient;
        ActiveAbility = activeAbility;

        Image.gameObject.SetActive(abilityClient != null);
        CooldownImage.enabled = abilityClient != null;

        if (abilityClient != null)
        {
            Image.sprite = AbilityClient.Ability.Sprite;
            CooldownImage.sprite = AbilityClient.Ability.Sprite;
        }
        else
        {
            Image.sprite = null;
            CooldownImage.sprite = null;
        }
    }

    public void Update()
    {
        if (ActiveAbility != null)
        {
            if (Input.GetKeyDown(HotKey))
            {
                ActiveAbility.OnPreUse();
            }
            else if (Input.GetKeyUp(HotKey))
            {
                ActiveAbility.OnUse((Vector2)Camera.main.ScreenToWorldPoint(Input.mousePosition));
            }

            CooldownImage.enabled = ActiveAbility.CooldownTimer > 0;
            if (ActiveAbility.CooldownTimer > 0)
            {
                CooldownImage.fillAmount = ActiveAbility.CooldownTimer / ActiveAbility.Cooldown;
            }
            else
            {
                CooldownImage.fillAmount = 1;
            }
        }
    }

    public void OnClick()
    {
        if (EventAbilityClick != null)
        {
            EventAbilityClick(transform.GetSiblingIndex());
        }
    }

    public void OnDown()
    {
        if (ActiveAbility != null)
        {
            ActiveAbility.OnPreUse();
        }

        if (EventAbilityDown != null)
        {
            EventAbilityDown(transform.GetSiblingIndex());
        }
    }

    public void OnUp()
    {
        if (ActiveAbility != null)
        {
            ActiveAbility.OnUse((Vector2)Camera.main.ScreenToWorldPoint(Input.mousePosition));
        }
        if (EventAbilityUp != null)
        {
            EventAbilityUp(transform.GetSiblingIndex());
        }
    }

    public void OnPointerEnter()
    {
        UIManager.Instance.IsPointerOverGUI = true;
    }

    public void OnPointerExit()
    {
        UIManager.Instance.IsPointerOverGUI = false;
    }
}
