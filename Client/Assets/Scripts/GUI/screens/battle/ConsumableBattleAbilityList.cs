﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections.Generic;
using DG.Tweening;

public class ConsumableBattleAbilityList : MonoBehaviour
{
    private static int OFFSET = 30;

    public ConsumableBattleAbilityView ConsumableBattleAbilityPrefab;
    public RectTransform Container;
    public RectTransform BackGroundTransform;
    public GridLayoutGroup ContainerGrid;
    public KeyCode HotKey;
    public KeyCode UpKey;
    public KeyCode DownKey;

    private ConsumableBattleAbilityView _currentView;
    private bool _isListVisible;

    private List<ConsumableBattleAbilityView> _views = new List<ConsumableBattleAbilityView>();

    public void AddAbility(AbilityWithCharges activeAbility)
    {
        gameObject.SetActive(true);

        ConsumableBattleAbilityView view = (ConsumableBattleAbilityView)Instantiate(ConsumableBattleAbilityPrefab, Container, false);
        view.SetAbility(activeAbility, ResourceManager.Instance.GetAbilityById(activeAbility.Id));
        view.EventAbilityClick += OnAbilityClick;
        _views.Add(view);

        SetListVisible(false);

        UpdateCurrent();
    }

    public void OnDisable()
    {
        foreach(var view in _views)
        {
            view.EventAbilityClick -= OnAbilityClick;
            Destroy(view.gameObject);
        }
        _views.Clear();

        _currentView = null;
        _isListVisible = false;

        gameObject.SetActive(false);
    }

    private void OnAbilityClick(ConsumableBattleAbilityView view)
    {
        if(_views.Count == 1)
        {
            _currentView.ActiveAbility.OnUse((Vector2)Camera.main.ScreenToWorldPoint(Input.mousePosition));
        }
        if(_currentView == view)
        {
            SetListVisible(!_isListVisible);
        }
        else
        {
            while(_currentView != view)
            {
                Up();
            }
            SetListVisible(false); 
        }
    }

    private void SetListVisible(bool value)
    {
        _isListVisible = value;
        int l = _isListVisible ? _views.Count : 1;

        var sizeDelta = BackGroundTransform.sizeDelta;
        sizeDelta.y = OFFSET + (l * ContainerGrid.cellSize.y) + ((l - 1) * ContainerGrid.spacing.y);
        BackGroundTransform.sizeDelta = sizeDelta;

        for (int i = 1; i < _views.Count; i++)
        {
            Container.GetChild(i).gameObject.SetActive(value);
        }
    }

    private void UpdateCurrent()
    {
        if (_views.Count > 0)
        {
            if(_currentView != null)
            {
                _currentView.gameObject.SetActive(false);
            }
            _currentView = Container.GetChild(0).GetComponent<ConsumableBattleAbilityView>();
            _currentView.gameObject.SetActive(true);
        }
    }

    public void Update()
    {
        if (Input.GetKeyUp(HotKey) && _currentView != null)
        {
            _currentView.ActiveAbility.OnUse((Vector2)Camera.main.ScreenToWorldPoint(Input.mousePosition));
        }

        if (Input.GetKeyUp(UpKey))
        {
            Up();
        }
        if (Input.GetKeyUp(DownKey))
        {
            Down();
        }
    }

    public void Up()
    {
        Container.GetChild(_views.Count - 1).transform.SetAsFirstSibling();
        UpdateCurrent();
    }

    public void Down()
    {
        Container.GetChild(0).transform.SetAsLastSibling();
        UpdateCurrent();
    }
}
