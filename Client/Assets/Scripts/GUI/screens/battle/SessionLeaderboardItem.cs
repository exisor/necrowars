﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class SessionLeaderboardItem : MonoBehaviour 
{
	public Text UserName;
	public Text UserPoints;
    public Text UserPosition;

    public void OnEnable()
	{
		UserName.text = "";
		UserPoints.text = "";
    }

    public void SetPosition(int pos)
    {
        UserPosition.text = pos.ToString();
    }

    public void SetLeader(Army army)
	{
		if(army != null)
		{
			UserName.text = army.PlayerName;
			UserPoints.text = army.Points.ToString();
		}
		else
		{
			UserName.text = "";
			UserPoints.text = "";
		}
	}
}
