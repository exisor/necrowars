﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System;

public class ConsumableBattleAbilityView : MonoBehaviour
{
    public delegate void AbilityClickAction(ConsumableBattleAbilityView Ability);

    public Image Image;
    public Image CooldownImage;
    public Text Charges;
    public Ability Ability;
    public AbilityWithCharges ActiveAbility;

    public event AbilityClickAction EventAbilityClick;

    public void SetAbility(AbilityWithCharges activeAbility, Ability ability)
    {
        Ability = ability;
        ActiveAbility = activeAbility;

        if (Ability != null)
        {
            Image.sprite = Ability.Sprite;
            CooldownImage.sprite = Ability.Sprite;
        }
        else
        {
            Image.sprite = null;
            CooldownImage.sprite = null;
        }
    }

    public void Update()
    {
        Charges.text = ActiveAbility.Charges.ToString();

        CooldownImage.enabled = ActiveAbility.CooldownTimer > 0;
        if (ActiveAbility.CooldownTimer > 0)
        {
            CooldownImage.fillAmount = ActiveAbility.CooldownTimer / ActiveAbility.Cooldown;
        }
        else
        {
            CooldownImage.fillAmount = 1;
        }
    }

    public void OnClick()
    {
        if (EventAbilityClick != null)
        {
            EventAbilityClick(this);
        }
    }

    public void OnPointerEnter()
    {
        UIManager.Instance.IsPointerOverGUI = true;
    }

    public void OnPointerExit()
    {
        UIManager.Instance.IsPointerOverGUI = false;
    }
}
