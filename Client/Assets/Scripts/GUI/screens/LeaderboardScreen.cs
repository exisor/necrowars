﻿using UnityEngine;
using System.Collections.Generic;
using UnityEngine.UI;

public class LeaderboardScreen : GameScreen
{
    public LeaderboardItem MyPlayer;
    public LeaderboardItem ItemPrefab;
    public RectTransform Container;
    public GridLayoutGroup Grid;

    private List<LeaderboardItem> _items = new List<LeaderboardItem>();

    public void OnOkClick()
    {
        UIManager.Instance.ShowScreen(GameScreen.ScreenNameEnum.Main);
    }

    public void OnEnable()
    {

    }

    public void OnDisable()
    {
        foreach (LeaderboardItem item in _items)
        {
            item.ItemClick -= OnItemClick;
            Destroy(item.gameObject);
        }
        _items.Clear();
    }

    public void ShowLeaderboard(List<Common.DTO.LeaderboardRecord> records)
    {
        foreach(var record in records)
        {
            LeaderboardItem itemView = (LeaderboardItem)Instantiate(ItemPrefab, Container, false);
            itemView.SetRecord(record);
            itemView.ItemClick += OnItemClick;

            _items.Add(itemView);
        }

        var myPlayerRecord = records.Find(r => r.CharacterId == DataManager.Instance.GetCurrentCharacterId());
        MyPlayer.gameObject.SetActive(myPlayerRecord != null);

        if(myPlayerRecord != null)
        {
            MyPlayer.SetRecord(myPlayerRecord, true);
        }

        var sizeDelta = Container.sizeDelta;
        sizeDelta.y = (Grid.spacing.y + Grid.cellSize.y) * _items.Count;
        Container.sizeDelta = sizeDelta;
    }

    private void OnItemClick(Common.DTO.LeaderboardRecord record)
    {
        SocketManager.Instance.GetProfile(record.CharacterId);
    }
}
