﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine.Networking;

public class BattleScreen : GameScreen
{
    public List<BattleAbilityView> AbilityViews = new List<BattleAbilityView>();
    public List<SessionLeaderboardItem> Leaders = new List<SessionLeaderboardItem>();
    public ConsumableBattleAbilityList ConsumableAbilityList;
    public List<GameObject> DestroyInTutorial = new List<GameObject>();

    public SessionLeaderboardItem PlayerInfo;
    public RectTransform Leaderboard;
    public RectTransform LeaderboardHint;

    public RectTransform LeaderIndicator;
    public RectTransform SacrificeZoneIndicator;

    public Text ArenaText;
    public Text LeaderHealth;
    public RectTransform HPBar;
    public RectTransform HPBarBackground;

    public EffectGUIView EffectPrefab;
    public RectTransform EffectContainerTransform;

    public RectTransform ChestBarTransform;
    public Text ChestName;
    public Text ChestCount;
    public Image ChestBarImage;
    public Image ChestBarBackgroundImage;
    public _2dxFX_EdgeColor EdgeColor;

    public Text UnitsCounterText;
    public Text UnitsLimitText;
    public int UnitsLimit = 150;
    public int PointsTest = 0;

    public GameObject SacrificeButton;
    public GameObject SacrificeText;
    public NetworkInstanceId SacrificeZoneId;

    private List<Chest> _chests;
    private PlayerArmy Army;
    private ArmyLeader Leader;
    private Chest _currentChest;
    private Chest _newChest;
    private int _chestCount = 0;

    private bool _isAnimatedChest;
    private bool _isLeaderboard;
    private float _leaderboardWidth;

    private Dictionary<TargetEffect, EffectGUIView> _effects = new Dictionary<TargetEffect, EffectGUIView>();

    void Awake()
    {
        _leaderboardWidth = Leaderboard.sizeDelta.x;
        UnitsCounterText.text = "0";
        UnitsLimitText.text = UnitsLimit.ToString();
    }

    void OnEnable()
    {
        _chests = ResourceManager.Instance.GetAllChests();

        Army = GameManager.Instance.PlayerArmy;

        if (Army != null)
        {
            Leader = Army.Leader;
        }
        _chestCount = 0;
        EdgeColor._Strength = 0;
        _isAnimatedChest = false;
        _isLeaderboard = false;
        Leaderboard.DOAnchorPosX(_leaderboardWidth, 0f);
        _currentChest = _chests[0];
        UpdateChestView(_currentChest, 0, 0, 0);

        if(DataManager.Instance.CurrentCharacter.Level == 1)
        {
            ArenaText.text = Localization.GetString("GUI_TUTOR_ARENA");
        }
        else if (DataManager.Instance.CurrentCharacter.Level <= 50)
        {
            ArenaText.text = Localization.GetString("GUI_NEWBIE_ARENA");
        } 
        else
        {
            ArenaText.text = Localization.GetString("GUI_WARRIOR_ARENA");
        }

        SetDefaultView(true);
    }

    void OnDisable()
    {
        foreach(var item in _effects)
        {
            Destroy(_effects[item.Key].gameObject);
        }
        _effects.Clear();
        SacrificeButton.SetActive(false);
        SacrificeText.SetActive(false);
        LeaderIndicator.gameObject.SetActive(false);
        SacrificeZoneIndicator.gameObject.SetActive(false);
    }

    public void SetAbilityInSlot(ActiveAbility ability)
    {
        AbilityViews[ability.Slot].SetAbility(ability, DataManager.Instance.GetAbility((int)ability.Id));
    }

    public void SetDefaultView(bool value)
    {
        for (int i = 0, n = DestroyInTutorial.Count; i < n; i++)
        {
            DestroyInTutorial[i].SetActive(value);
        }
    }

    public void SetConsumableAbilityInSlot(AbilityWithCharges ability)
    {
        ConsumableAbilityList.AddAbility(ability);
    }

    public override void OnExperienceChanged()
    {

    }

    public override void OnRequiredExperienceChanged()
    {

    }

    public override void OnGoldChanged()
    {

    }

    public override void OnCharacterLevelChanged()
    {

    }

    public void OnSacrificeClick()
    {
        GameManager.Instance.PlayerArmy.CmdInteract(SacrificeZoneId);
    }

    private void Update()
    {
        if (Army == null)
        {
            return;
        }

        if (Input.GetKeyDown(KeyCode.Tab))
        {
            _isLeaderboard = !_isLeaderboard;
            LeaderboardHint.gameObject.SetActive(!_isLeaderboard);
            Leaderboard.DOKill();
            Leaderboard.DOAnchorPosX(_isLeaderboard ? 0 : _leaderboardWidth, 0.3f);
        }

        UnitsCounterText.text = Army.AliveUnits.ToString();
        UnitsLimitText.text = Army.VikingsLimit.ToString();

        UpdateChest();

        UpdateHPBar(Leader.CurrentHp, Leader.MaxHp);        

        PlayerInfo.SetLeader(GameManager.Instance.PlayerArmy);
        PlayerInfo.SetPosition(GameManager.Instance.PlayerLeaderboardPosition);

        var armys = GameManager.Instance.GetLeaderboard(Leaders.Count);

        for (int i = 0; i < Leaders.Count; i++)
        {
            if (i < armys.Count)
            {
                Leaders[i].SetLeader(armys[i]);
            }
            else
            {
                Leaders[i].SetLeader(null);
            }

        }
    }

    public void AddEffect(TargetEffect effect)
    {
        EffectGUIView effectGUI = (EffectGUIView)Instantiate(EffectPrefab, EffectContainerTransform, false);
        effectGUI.SetData(effect);
        _effects[effect] = effectGUI;
    }

    public void RemoveEffect(TargetEffect effect)
    {
        if(_effects.ContainsKey(effect))
        {
            Destroy(_effects[effect].gameObject);
            _effects.Remove(effect);
        }
    }

    public void UpdateHPBar(float current, float max)
    {
        LeaderHealth.text = current.ToString();

        var sizeDelta = HPBar.sizeDelta;
        float value = current / max;
        sizeDelta.x = value * HPBarBackground.sizeDelta.x;
        HPBar.sizeDelta = sizeDelta;
    }

    private void UpdateChest()
    {
        long start = 0;
        long end = 0;
        int points = Army.Points + PointsTest;
        int newChestCount = 0;

        foreach (Chest chest in _chests)
        {
            start = end;
            end = chest.GetPointsRequirement(DataManager.Instance.CurrentCharacter.Level);
            
            if (end > points)
            {
                _newChest = chest;
                break;
            }
        }
        while (points > end)
        {
            _newChest = _chests[_chests.Count - 1];
            newChestCount++;
            start = end;
            end += _newChest.GetPointsRequirement(DataManager.Instance.CurrentCharacter.Level);
        }
        if(!_isAnimatedChest)
        {
            if (_currentChest != _newChest || _chestCount != newChestCount)
            {
                _isAnimatedChest = true;
                
                DOTween.To(x => EdgeColor._Strength = x, 0f, 0.4f, 0.5f).OnComplete(() =>
                    DOTween.To(x => EdgeColor._Strength = x, 0.4f, 0f, 0.5f).OnComplete(() =>
                        _isAnimatedChest = false));
                UpdateChestView(_currentChest, 1, 100, _chestCount);
                _currentChest = _newChest;
                _chestCount = newChestCount;
            }
            else 
            {
                var progress = (float)(points - start) / (float)(end - start);
                UpdateChestView(_currentChest, progress, (int)(progress * 100), _chestCount);
            }
        }
    }

    private void UpdateChestView(Chest chest, float anchorY, int percent, int chestCount)
    {
        ChestName.text = Localization.GetString(chest.NameKey).ToUpper() + " " + percent.ToString() + "%";
        ChestBarImage.sprite = chest.Sprite;
        ChestBarBackgroundImage.sprite = chest.Sprite;
        ChestBarTransform.anchorMax = new Vector2(1, anchorY);
        ChestCount.text = chestCount > 0 ? "+" + chestCount.ToString() : "";
    }
}