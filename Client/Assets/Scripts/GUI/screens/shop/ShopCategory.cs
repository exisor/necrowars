﻿using UnityEngine;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using DG.Tweening;

public class ShopCategory : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
{
    public delegate void CategoryClickAction(int index);

    public List<long> Items;

    public Image Image;
    public Sprite DefaultSprite;
    public Sprite ActiveSprite;
    public Sprite HoverSprite;

    public Vector2 DefaultPosition;
    public Vector2 ActivePosition;
    public Vector2 HoverPosition;

    public RectTransform TextTransform;

    public bool isActive;
    public bool isCurrency;
    public bool isDiscount;

    public event CategoryClickAction CategoryClick;

    public void OnPointerEnter(PointerEventData eventData)
    {
        if(!isActive)
        {
            Image.sprite = HoverSprite;
            TextTransform.anchoredPosition = HoverPosition;
        }
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        if (!isActive)
        {
            Image.sprite = DefaultSprite;
            TextTransform.anchoredPosition = DefaultPosition;
        }
    }

    public void SetActive(bool value)
    {
        isActive = value;
        Image.sprite = isActive ? ActiveSprite : DefaultSprite;
        TextTransform.anchoredPosition = isActive ? ActivePosition : DefaultPosition;
    }

    public void OnClick()
    {
        if (CategoryClick != null)
        {
            CategoryClick(transform.GetSiblingIndex());
        }
    }
}
