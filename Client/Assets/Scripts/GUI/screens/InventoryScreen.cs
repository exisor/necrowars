﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using System.Linq;

public class InventoryScreen : GameScreen 
{
    private static int ITEMS_ON_PAGE = 8;

    public ItemView ItemPrefab;
    public ItemSlot ItemSlotPrefab;
    public RectTransform ItemSlotsContainer;

    public Text Currency;
    public Text Gold;
    public Text PagingText;
    public GameObject LeftButton;
    public GameObject RightButton;

    public GameObject Characteristics;
    public GameObject Bonuses;

    public Text HP;
    public Text Armor;
    public Text Attack;
    public Text Regen;
    public Text Speed;
    public Text Spell;
    public Text BonusesText;

    private int _pageCount;
    private int _currentPage;

    public List<ItemSlot> EquipmentSlots = new List<ItemSlot>();
    private List<ItemSlot> _slots = new List<ItemSlot>();
	private List<ItemView> _items = new List<ItemView>();

    public void OnEnable()
	{
        foreach(var slot in EquipmentSlots)
        {
            var itemClient = DataManager.Instance.CurrentCharacter.GetEquipByType(slot.SlotType);
            if(itemClient != null)
            {
                _items.Add(CreateItemView(itemClient, slot));
            }
            slot.EventChangeItem += OnChangeEquipment;
        }

        OnEquipChanged();
        OnCurrencyChanged();
        OnGoldChanged();
        OnHPChanged(DataManager.Instance.CurrentCharacter.MaxHP);
        OnMinDamageChanged(DataManager.Instance.CurrentCharacter.MinDamage);
        OnArmorChanged(DataManager.Instance.CurrentCharacter.Armor);
        OnRegenChanged(DataManager.Instance.CurrentCharacter.HPRegen);
        OnSpeedChanged(DataManager.Instance.CurrentCharacter.AttackSpeed);
        OnSpellChanged(DataManager.Instance.CurrentCharacter.SpellDamage);

        DataManager.Instance.CurrentCharacter.MaxHP.ValueChanged += OnHPChanged;
        DataManager.Instance.CurrentCharacter.Armor.ValueChanged += OnArmorChanged;
        DataManager.Instance.CurrentCharacter.MinDamage.ValueChanged += OnMinDamageChanged;
        DataManager.Instance.CurrentCharacter.MaxDamage.ValueChanged += OnMaxDamageChanged;
        DataManager.Instance.CurrentCharacter.HPRegen.ValueChanged += OnRegenChanged;
        DataManager.Instance.CurrentCharacter.AttackSpeed.ValueChanged += OnSpeedChanged;
        DataManager.Instance.CurrentCharacter.SpellDamage.ValueChanged += OnSpellChanged;

        List<ItemClient> items = DataManager.Instance.GetAllUnequipedItems();
        int size = DataManager.Instance.InventorySize;

        if (items.Count > size)
        {
            size = items.Count;
        }

        _pageCount = (size - 1) / ITEMS_ON_PAGE + 1;
        _currentPage = 0;
        

        for (int i = 0; i < size; i++)
        {
            ItemSlot itemSlot = (ItemSlot)Instantiate(ItemSlotPrefab, ItemSlotsContainer, false);
            itemSlot.gameObject.SetActive(false);
            _slots.Add(itemSlot);

            if (i < items.Count)
            {
                _items.Add(CreateItemView(items[i], itemSlot));
            }
        }

        UpdateInventory();
    }

	private ItemView CreateItemView(ItemClient itemClient, ItemSlot slot)
	{
		ItemView itemView = (ItemView)Instantiate(ItemPrefab);
        if(!itemClient.ItemData.Types.Contains(Common.Resources.ItemType.Consumable) 
            || itemClient.ItemData.Types.Contains(Common.Resources.ItemType.Battle)
            || !itemClient.ItemData.CanUse)
        {
            DestroyImmediate(itemView.UseHandler);
        } 
        else 
        {
            DestroyImmediate(itemView.SellHandler);
        }

        if (!Tutorial.IsMainTutorialComplete())
        {
            DestroyImmediate(itemView.UseHandler);
            DestroyImmediate(itemView.SellHandler);
        }

        if (itemView.SellHandler)
        {
            itemView.EventSell += OnItemSell;
        }

        if (itemView.UseHandler)
        {
            itemView.EventUse += OnItemUse;
        }

        itemView.EventShowHint += OnShowHint;
        itemView.EventHideHint += OnHideHint;
        itemView.ItemClick += OnItemClick;

        slot.SetItem(itemView);
        itemView.SetItem(itemClient);
        return itemView;
	}

	public void OnDisable()
	{
        foreach (var equipmentSlot in EquipmentSlots)
        {
            equipmentSlot.EventChangeItem -= OnChangeEquipment;
        }

        foreach (ItemView item in _items)
        {
            item.EventShowHint -= OnShowHint;
            item.EventHideHint -= OnHideHint;
            item.EventSell -= OnItemSell;
        	item.ItemClick -= OnItemClick;
            item.EventUse -= OnItemUse;
            Destroy(item.gameObject);
        }
        _items.Clear();

        foreach (var slot in _slots)
        {
            Destroy(slot.gameObject);
        }
        _slots.Clear();
    }

    private void OnHPChanged(float value)
    {
        HP.text = Localization.GetString("GUI_INVENTORY_HP") + " - " + value.ToString("F0");
    }
    private void OnArmorChanged(float value)
    {
        Armor.text = Localization.GetString("GUI_INVENTORY_Armor") + " - " + value.ToString("F0");
    }

    private void OnMinDamageChanged(float value)
    {
        float max = DataManager.Instance.CurrentCharacter.MaxDamage;
        Attack.text = Localization.GetString("GUI_INVENTORY_Attack") + " - " + value.ToString("F0") + "-" + max.ToString("F0");
    }
    private void OnMaxDamageChanged(float value)
    {
        float min = DataManager.Instance.CurrentCharacter.MinDamage;
        Attack.text = Localization.GetString("GUI_INVENTORY_Attack") + " - " + min.ToString("F0") + 
            "-" + value.ToString("F0");
    }
    private void OnRegenChanged(float value)
    {
        Regen.text = Localization.GetString("GUI_INVENTORY_HP_Regen") + " - " + value.ToString("F0");
    }
    private void OnSpeedChanged(float value)
    {
        Speed.text = Localization.GetString("GUI_INVENTORY_SPEED") + " - " + value.ToString("F2");
    }
    private void OnSpellChanged(float value)
    {
        Spell.text = Localization.GetString("GUI_INVENTORY_Spell") + " - " + value.ToString("P0");
    }

    public override void OnCurrencyChanged()
    {
        Currency.text = DataManager.Instance.Currency.ToString();
    }

    public override void OnGoldChanged()
    {
        Gold.text = DataManager.Instance.Gold.ToString();

        if (DataManager.Instance.GetPowerUp(2) != null)
        {
            Gold.text += " (*)";
        }
    }

    public override void OnItemChanged()
    {
        var page = _currentPage;
        OnDisable();
        OnEnable();

        _currentPage = page;

        UpdateInventory();
    }

    public override void OnChestOpened(List<Common.DTO.Item> newItems)
    {
        var page = _currentPage;
        OnDisable();
        OnEnable();

        _currentPage = page;

        UpdateInventory();
    }

    public override void OnEquipChanged()
    {
        Dictionary<AffixClient, List<int>> dictionary = new Dictionary<AffixClient, List<int>>();
        List<ItemClient> items = DataManager.Instance.GetAllEquipedItems();

        foreach (var item in items)
        {
            foreach (var affix in item.Affixes)
            {
                KeyValuePair<AffixClient, List<int>> pair;
                if (affix.AffixData.Id == 16 || affix.AffixData.Id == 17)
                {
                    pair = dictionary.FirstOrDefault(a => a.Key.AffixData.Id == affix.AffixData.Id && affix.Values[1] == a.Value[1]);
                }
                else
                {
                    pair = dictionary.FirstOrDefault(a => a.Key.AffixData.Id == affix.AffixData.Id);
                }
                
                if (pair.Key != null)
                {
                    for (int i = 0; i < pair.Value.Count; i++)
                    {
                        switch (pair.Key.AffixDTO.Id)
                        {
                            // affixes with skills
                            case 16:
                            case 17:
                                if (i == 1)
                                {
                                    pair.Value[i] = affix.Values[i];
                                }
                                else
                                {
                                    pair.Value[i] += affix.Values[i];
                                }
                                break;
                            default:
                                if(pair.Key.AffixData.isMultiplicative)
                                {
                                    if(pair.Key.AffixData.isDecrease)
                                    {
                                        pair.Value[i] *= (1 - affix.Values[i]);
                                    }
                                    else
                                    {
                                        pair.Value[i] *= (1 + affix.Values[i]);
                                    }
                                }
                                else
                                {
                                    pair.Value[i] += affix.Values[i];
                                }
                                break;
                        }
                    }
                }
                else
                {
                    dictionary[affix] = affix.Values.ToList();
                }
            }
        }

        foreach(KeyValuePair<AffixClient,List <int>> pair in dictionary)
        {
            if (pair.Key.AffixData.isMultiplicative)
            {
                for (int i = 0; i < pair.Value.Count; i++)
                {
                    if (pair.Key.AffixData.isDecrease)
                    {
                        pair.Value[i] = 1 - pair.Value[i];
                    }
                    else
                    {
                        pair.Value[i] = pair.Value[i] + 1;
                    }
                }
                    
            }
        }

        List<KeyValuePair<AffixClient, List<int>>> sortList = dictionary.ToList();
        sortList = sortList.OrderByDescending(pair => pair.Key.AffixData.Id).ToList();

        string text = "";
        foreach (KeyValuePair<AffixClient, List<int>> entry in sortList)
        {
            if(entry.Key.IsValid(entry.Value) && !entry.Key.AffixData.isBaseAffix)
            {
                text += entry.Key.GetAffixStringByValues(entry.Value) + "\n";
            }
        }
        BonusesText.text = text;
    }

    public override void OnItemLost()
    {
        var page = _currentPage;
        OnDisable();
        OnEnable();

        _currentPage = page;

        UpdateInventory();
    }

    public void OnLeftClick()
    {
        if (LeftButton.activeInHierarchy)
        {
            _currentPage--;
            UpdateInventory();
        }
    }

    public void OnRightClick()
    {
        if (RightButton.activeInHierarchy)
        {
            _currentPage++;
            UpdateInventory();
        }
    }

    public void OnCharacteristicsClick()
    {
        Characteristics.SetActive(true);
        Bonuses.SetActive(false);
    }

    public void OnBonusesClick()
    {
        Characteristics.SetActive(false);
        Bonuses.SetActive(true);
    }

    private void UpdateInventory()
    {
        LeftButton.SetActive(_pageCount > 1 && _currentPage != 0);
        RightButton.SetActive(_pageCount > 1 && _currentPage != _pageCount - 1);
        PagingText.gameObject.SetActive(_pageCount > 1);

        PagingText.text = (_currentPage + 1).ToString() + "/" + _pageCount.ToString();

        for (int i = 0, n = _slots.Count; i < n; i++)
        {
            _slots[i].gameObject.SetActive(i >= _currentPage * ITEMS_ON_PAGE && i < (_currentPage + 1) * ITEMS_ON_PAGE);
        }
    }

    private void OnShowHint(ItemView itemView)
    {
        if(itemView.Slot.isAnyType)
        {
            var slot = EquipmentSlots.Find(s => s.SlotType == itemView.ItemClient.ItemData.ItemSlot);
            if(slot != null && slot.ItemView != null)
            {
                slot.ItemView.Hint.Show();
            }
        }
    }

    private void OnHideHint(ItemView itemView)
    {
        if (itemView.Slot.isAnyType)
        {
            var slot = EquipmentSlots.Find(s => s.SlotType == itemView.ItemClient.ItemData.ItemSlot);
            if (slot != null && slot.ItemView != null)
            {
                slot.ItemView.Hint.Hide();
            }
        }
    }

    private void OnItemSell(ItemView itemView)
	{
        if(itemView.ItemClient.ItemDTO.Rarity == Common.DTO.Rarity.Legendary)
        {
            UIManager.Instance.ShowConfirmation(itemView.ItemClient.ItemDTO.Id);
        }
        else
        {
            SocketManager.Instance.SellItem(itemView.ItemClient.ItemDTO.Id);
        }
	}

    private void OnItemUse(ItemView itemView)
    {
        SocketManager.Instance.UseItemRequest((int)DataManager.Instance.GetCurrentCharacterId(), itemView.ItemClient.ItemDTO.Id);
    }

    private void OnItemClick(ItemClient itemClient)
	{
		
	}

	private void OnChangeEquipment(ItemSlot itemSlot, ItemView newItem, ItemView oldItem)
	{
        if (oldItem != null)
        {
            SocketManager.Instance.UnequipItem(oldItem.ItemClient.ItemDTO.Id);
        }
        if (newItem != null)
		{
            SocketManager.Instance.EquipItem(DataManager.Instance.GetCurrentCharacterId(), newItem.ItemClient.ItemDTO.Id);
        }
	}

	public void OnOkClick()
    {
        UIManager.Instance.ShowScreen(GameScreen.ScreenNameEnum.Main);
    }
}
