﻿using UnityEngine;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine.UI;
using DG.Tweening;
using UnityEngine.EventSystems;

public class AbilityScreen : GameScreen
{
    private const long REFUND_ID = 27;
	public delegate void AbilitySelectAction(AbilityClient selectedAbilityClient);

    public List<AbilityTreeView> Trees = new List<AbilityTreeView>();
    public List<Button> Buttons = new List<Button>();

    public Text Info;
    public Button OkButton;
    public Text RefundPrice;

    private int _currentTree;


    public void OnEnable()
    { 
        foreach (AbilityTreeView tree in Trees)
        {
            tree.gameObject.SetActive(false);
        }

        foreach (Button button in Buttons)
        {
            button.image.sprite = button.spriteState.disabledSprite;
        }

        _currentTree = -1;
        UpdateCurrentTree(0, true, 0);
        UpdateInfo();

        UIManager.Instance.MainMenuView.ShowAbilities();

        SocketManager.Instance.GetShopItems();
    }

    public void OnDisable()
    {
        UIManager.Instance.MainMenuView.ShowMain();
    }

    public void OnRefundClick()
    {
        Debug.Log(DataManager.Instance.GetShopItemByBaseId(REFUND_ID).ShopItem.Id);
        SocketManager.Instance.BuyItem(DataManager.Instance.GetCurrentCharacterId(), DataManager.Instance.GetShopItemByBaseId(REFUND_ID).ShopItem.Id);
    }

    public void OnLeftClick()
    {
        UpdateCurrentTree(_currentTree == 0 ? Trees.Count - 1 : _currentTree - 1);
    }

    public void OnRightClick()
    {
        UpdateCurrentTree(_currentTree == Trees.Count - 1 ? 0 : _currentTree + 1, false);
    }

    public void OnTreeButtonClick(int index)
    {
        UpdateCurrentTree(index, index < _currentTree);
    }

    private void UpdateCurrentTree(int index, bool isLeft = true, float time = 1f)
    {
        if(index == _currentTree)
        {
            return;
        }

        int previos = _currentTree;
        _currentTree = index;

        foreach (AbilityTreeView tree in Trees)
        {
            tree.Rect.DOKill(true);
        }

        if (previos != -1)
        {
            Trees[previos].Rect.localEulerAngles = Vector3.zero;
            Trees[previos].Rect.DORotate(new Vector3(0, 0, isLeft ? - 60 : 60), time).OnComplete(() => Trees[previos].gameObject.SetActive(false));
            Buttons[previos].image.sprite = Buttons[previos].spriteState.disabledSprite;
        }
        
        Trees[index].gameObject.SetActive(true);
        Trees[index].Rect.localEulerAngles = new Vector3(0, 0, isLeft ? 60 : -60);
        Trees[index].Rect.DORotate(Vector3.zero, time);

        Buttons[index].image.sprite = Buttons[index].spriteState.highlightedSprite;
    }

    public override void OnFixedShop()
    {
        RefundPrice.text = DataManager.Instance.GetShopItemByBaseId(REFUND_ID).ShopItem.CurrencyPrice.ToString();
    }

    public override void OnAbilityChanged()
	{
        if(Tutorial.IsAbilityScreenPhase())
        {
            OkButton.gameObject.SetActive(true);
            Tutorial.NextPhase();
        }
		foreach (AbilityTreeView tree in Trees)
        {
            tree.UpdateTree();
        }
	}

    public override void OnCharacterLevelChanged()
    {
        UpdateInfo();
    }

    public override void OnApChanged()
    {
        UpdateInfo();
    }

    public override void OnItemChanged()
    {
        UpdateInfo();
    }

    public override void OnItemLost()
    {
        UpdateInfo();
    }

    private void UpdateInfo()
    {
        var count = 0;
        var item = DataManager.Instance.GetItemByBaseId(24);
        if(item != null)
        {
            count = item.ItemDTO.Quantity;
        }
        Info.text = String.Format(Localization.GetString("GUI_ABILITIES_INFO"), DataManager.Instance.CurrentCharacter.Level.ToString(), 
                                                                    DataManager.Instance.CurrentCharacter.AP.ToString());
    }

    public void OnOkClick()
    {
        UIManager.Instance.ShowScreen(GameScreen.ScreenNameEnum.Main);
    }
}
