﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class GameScreen : MonoBehaviour 
{
	public enum ScreenNameEnum {Login, Main, Ability, Battle, Shop, Inventory, Forge, Leaderboard, Achievement, KongGuest};
	
	public ScreenNameEnum ScreenName;

	public virtual void OnNewCharacter()
	{
		
	}

	public virtual void OnCharacterLevelChanged()
	{
		
	}

	public virtual void OnApChanged()
	{
		
	}

	public virtual void OnCurrencyChanged()
	{
		
	}

	public virtual void OnExperienceChanged()
	{
		
	}

	public virtual void OnGoldChanged()
	{
		
	}

	public virtual void OnRequiredExperienceChanged()
	{
		
	}

	public virtual void OnAbilityChanged()
	{
		
	}

    public virtual void OnAbilitySlotChanged()
    {

    }

    public virtual void OnEquipChanged()
    {

    }

    public virtual void OnItemChanged()
    {

    }

    public virtual void OnItemLost()
    {

    }

    public virtual void OnQuestChanged()
    {

    }

    public virtual void OnNewQuest()
    {

    }

    public virtual void OnNewItems()
    {

    }

    public virtual void OnQuestComplete()
    {

    }

    public virtual void OnFixedShop()
    {

    }

    public virtual void OnNewPowerUp(PowerUpClient powerUp)
	{
		
	}

    public virtual void OnPowerUpExpired(PowerUpClient powerUp)
    {

    }

    public virtual void OnChestOpened(List<Common.DTO.Item> newItems)
	{
		
	}
}
