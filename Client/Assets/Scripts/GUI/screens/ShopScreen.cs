﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public class ShopScreen : GameScreen 
{
    private static int ITEMS_ON_PAGE = 8;

    public ShopItemView ShopItemPrefab;
	public RectTransform ItemsContainer;
    public GameObject LeftButton;
    public GameObject RightButton;

    public Text Currency;
    public Text Gold;

    public List<ShopCategory> Categories;

    private List<ShopItemView> _items = new List<ShopItemView>();

    private int _currentCategory = -1;
    private int _pageCount;
    private int _currentPage;

    public void OnEnable()
	{
        OnCurrencyChanged();
        OnGoldChanged();

        foreach (ShopCategory category in Categories)
        {
            category.CategoryClick += OnCategoryClick;
        }

        _currentCategory = -1;
        SetCategory(0);

        SocketManager.Instance.GetShopItems();
    }

	public void OnDisable()
	{
        Categories[_currentCategory].SetActive(false);
        DestroyShopItems();

        foreach (ShopCategory category in Categories)
        {
            category.CategoryClick -= OnCategoryClick;
        }
    }

    public void SetButtonsInteractable(bool value)
    {
        foreach (ShopCategory category in Categories)
        {
            category.GetComponent<Button>().interactable = value;
        }
        LeftButton.GetComponent<Button>().interactable = value;
        RightButton.GetComponent<Button>().interactable = value;
    }

    private void DestroyShopItems()
    {
        foreach (ShopItemView shopItem in _items)
        {
            shopItem.ShopItemClick -= OnShopItemClick;
            shopItem.CurrencyItemClick -= OnCurrencyItemClick;
            Destroy(shopItem.gameObject);
        }
        _items.Clear();
    }

    public void SetCategory(int index)
    {
        if(_currentCategory == index)
        {
            return;
        }
        if(_currentCategory != -1)
        {
            Categories[_currentCategory].SetActive(false);
        }
        
        _currentCategory = index;
        Categories[_currentCategory].SetActive(true);

        _currentPage = 0;

        UpdateCategory();
    }

    public void UpdateCategory()
    {
        DestroyShopItems();

        if (Categories[_currentCategory].isCurrency)
        {
#if UNITY_EDITOR
            SetButtonsInteractable(false);
            SocketManager.Instance.GetKongShopItems();
#endif
            if (DataManager.Instance.Social == DataManager.SocialType.Kong)
            {
                SetButtonsInteractable(false);
                SocketManager.Instance.GetKongShopItems();
            }
            if (DataManager.Instance.IsOGZ)
            {
                SetButtonsInteractable(false);
                Application.ExternalEval(
                  @"
                    function onItemsCompleted(json) {
                        console.log(json.data);
                        if (json.success) {
                            var response = '';
                            for (var i = 0; i < json.data.length; i++)
                            {
                                response += '|' + json.data[i].id.toString() + ',';
                                switch('" + DataManager.Instance.Social + @"')
                                {
                                    case 'OK':
                                        response += json.data[i].price_tags.OKCRED.toString();
                                        break;
                                    case 'MM':
                                        response += json.data[i].price_tags.MMCRED.toString();
                                        break;
                                    case 'FB':
                                        response += json.data[i].price_tags.USD.toString();
                                        break;
                                    default:
                                        response += json.data[i].price_tags.VKVOTE.toString();
    
                                }
                                
                            }
                            response = response.substring(1);
                            SendMessage('" + gameObject.name + @"', 'OGZShopCallback', response);
                        }
                    }
                    OGZ.callAPI('items', {}, onItemsCompleted);"
                );
            }
        }
        else
        {
            UpdateShop();
        }
    }

    public void CreateCategory()
    {
        foreach (ShopItemClient shopItem in DataManager.Instance.GetAllShopItems())
        {
            if (Categories[_currentCategory].Items.Contains(shopItem.ShopItemData.Id) || 
                (Categories[_currentCategory].isDiscount && shopItem.ShopItem.Limited))
            {
                ShopItemView item = (ShopItemView)Instantiate(ShopItemPrefab, ItemsContainer, false);
                item.ShopItemClick += OnShopItemClick;
                item.SetShopItem(shopItem);
                _items.Add(item);
            }
        }
    }

    public void OGZShopCallback(string items)
    {
        Debug.Log("items " + items);
        DataManager.Instance.SetOGZItems(items);
        UpdateCurrencyShop();
    }

    public void UpdateShop()
    {
        DestroyShopItems();

        CreateCategory();

        _pageCount = (_items.Count - 1) / ITEMS_ON_PAGE + 1;

        SetButtonsInteractable(true);

        UpdatePage();
    }

    public void UpdateCurrencyShop()
    {
        DestroyShopItems();

        foreach (CurrencyItemClient currencyItem in DataManager.Instance.GetAllCurrencyItems())
        {
            ShopItemView item = (ShopItemView)Instantiate(ShopItemPrefab, ItemsContainer, false);
            item.CurrencyItemClick += OnCurrencyItemClick;
            item.SetCurrencyItem(currencyItem);
            _items.Add(item);
        }

        CreateCategory();

        _pageCount = (_items.Count - 1) / ITEMS_ON_PAGE + 1;

        SetButtonsInteractable(true);

        UpdatePage();
    }

    private void UpdatePage()
    {
        LeftButton.SetActive(_pageCount > 1 && _currentPage != 0);
        RightButton.SetActive(_pageCount > 1 && _currentPage != _pageCount - 1);

        var pos = ItemsContainer.anchoredPosition;
        pos.y = ItemsContainer.sizeDelta.y * _currentPage;
        ItemsContainer.anchoredPosition = pos;
    }

    private void OnCategoryClick(int index)
    {
        SetCategory(index);
    }

    public void OnLeftClick()
    {
        if (LeftButton.activeInHierarchy)
        {
            _currentPage--;
            UpdatePage();
        }
    }

    public void OnRightClick()
    {
        if (RightButton.activeInHierarchy)
        {
            _currentPage++;
            UpdatePage();
        }
    }

    private void OnShopItemClick(ShopItemClient shopItemClient)
	{
        SetButtonsInteractable(false);
        SocketManager.Instance.BuyItem(DataManager.Instance.GetCurrentCharacterId(), shopItemClient.ShopItem.Id);
        SocketManager.Instance.GetShopItems();
    }

    private void OnCurrencyItemClick(CurrencyItemClient currencyItemClient)
    {
        //SetButtonsInteractable(false);

        if(DataManager.Instance.Social == DataManager.SocialType.Kong)
        {
            Application.ExternalEval(
              @"
                function onPurchaseResult(result) {
                    console.log('onPurchaseResult');
                    if (result.success)
                    {
                        SendMessage('" + gameObject.name + @"', 'OnKongBuy', '" + currencyItemClient.ItemData.InternalId.ToString() + @"');
                    }

                }
            
                kongregate.mtx.purchaseItems(['" + currencyItemClient.ItemData.KongId + @"'], onPurchaseResult);"
            );
        }
        else if(DataManager.Instance.IsOGZ)
        {
            Application.ExternalEval(
              @"
                function onOGZPurchaseResult(success, data) {
                    console.log('onOGZPurchaseResult');
                    if (success)
                    {
                        SendMessage('" + gameObject.name + @"', 'OnOGZBuy', '" + currencyItemClient.ItemData.InternalId.ToString() + @"');
                    }

                }
                OGZ.addCallback('payment', onOGZPurchaseResult);
                OGZ.showPayment({item:'" + currencyItemClient.ItemData.OGZId + @"'});"
            );
        }
    }

    public void OnKongBuy(string id)
    {
        Debug.Log(id);

        Analytics.gua.sendEventHit("Shop", "Kong", id, (int)DataManager.Instance.GetCurrentCharacterId());
        SocketManager.Instance.UseKongItem(int.Parse(id));
        SocketManager.Instance.GetKongShopItems();
    }

    public void OnOGZBuy(string id)
    {
        Debug.Log(id);

        Analytics.gua.sendEventHit("Shop", "OGZ", id, (int)DataManager.Instance.GetCurrentCharacterId());
    }

    public void OnOkClick()
    {
    	UIManager.Instance.ShowScreen(GameScreen.ScreenNameEnum.Main);
    }

    public void OnBonusClick()
    {
        UIManager.Instance.ShowPopup(Popup.PopupNameEnum.ShopBonus);
    }

    public override void OnFixedShop()
    {
        if(Categories[_currentCategory].isCurrency)
        {
            UpdateCurrencyShop();
        }
        else
        {
            UpdateShop();
        }
    }

    public override void OnCurrencyChanged()
    {
        Currency.text = DataManager.Instance.Currency.ToString();
    }

    public override void OnGoldChanged()
    {
        Gold.text = DataManager.Instance.Gold.ToString();

        if (DataManager.Instance.GetPowerUp(2) != null)
        {
            Gold.text += " (*)";
        }
    }

    public override void OnChestOpened(List<Common.DTO.Item> newItems)
	{
        //UIManager.Instance.ShowPopup(Popup.PopupNameEnum.ChestOpen);
    }
}
