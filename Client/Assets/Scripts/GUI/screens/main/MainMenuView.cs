﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class MainMenuView : MonoBehaviour
{
    public GameObject AchivementsButton;
    public GameObject SettingsButton;
    public GameObject DailyButton;
    public GameObject ExitButton;

    public QuestView Quest;

    public void ShowMain()
    {
        gameObject.SetActive(true);
        AchivementsButton.SetActive(Tutorial.IsMainTutorialComplete());
        SettingsButton.SetActive(true);
        DailyButton.SetActive(false);
        ExitButton.SetActive(false);

        UpdateQuest();
    }

    public void ShowAbilities()
    {
        gameObject.SetActive(true);
        AchivementsButton.SetActive(false);
        SettingsButton.SetActive(false);
        DailyButton.SetActive(false);
        ExitButton.SetActive(false);
    }

    public void ShowBattle()
    {
        gameObject.SetActive(true);
        AchivementsButton.SetActive(false);
        SettingsButton.SetActive(true);
        DailyButton.SetActive(false);
        ExitButton.SetActive(true);
    }

    public void OnSettingsClick()
    {
        if(UIManager.Instance.Settings.gameObject.activeInHierarchy)
        {
            UIManager.Instance.HideSettings();
        }
        else
        {
            UIManager.Instance.ShowSettings();
        }
    }

    public void OnAchievementsClick()
    {
        if (UIManager.Instance.GetCurrentScreenName() == GameScreen.ScreenNameEnum.Achievement)
        {
            UIManager.Instance.ShowPreviosScreen();
        }
        else
        {
            UIManager.Instance.ShowScreen(GameScreen.ScreenNameEnum.Achievement);
        }
    }

    public void OnExitClick()
    {
        GameNetworkManager.Instance.StopClient();
        UIManager.Instance.ShowPreloader();
        ApplicationManager.Instance.ShowLobby(OnLoadSceneCallBack);
    }

    private void OnLoadSceneCallBack()
    {
        UIManager.Instance.ShowScreen(GameScreen.ScreenNameEnum.Main);
    }

    public void OnNewQuest()
    {
        UpdateQuest();
    }

    public void OnQuestChanged()
    {
        UpdateQuest();
    }

    public void OnQuestComplete()
    {
        UpdateQuest();
    }

    private void UpdateQuest()
    {
        Quest.SetQuest(DataManager.Instance.CurrentQuest);
    }
}
