﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class MainButtonView : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler, IPointerDownHandler, IPointerUpHandler
{
    [System.Serializable]
    public struct MainButtonState
    {
        public Vector2 ImagePosition;
        public Vector2 TextPosition;
        public Sprite Sprite;
        public Color TextColor;
        public Color OutlineColor;
    }

    public RectTransform TextTransform;
    public RectTransform ImageTransform;
    public Text Text;
    public Image Image;
    public Outline Outline;

    public MainButtonState IdleState;
    public MainButtonState PushState;
    public MainButtonState SelectState;

    public void OnPointerEnter(PointerEventData eventData)
    {
        SetState(SelectState);
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        SetState(IdleState);
    }

    public void OnPointerDown(PointerEventData eventData)
    {
        SetState(PushState);
    }

    public void OnPointerUp(PointerEventData eventData)
    {
        SetState(IdleState);
    }

    public void SetInteractive(bool interactive)
    {
        Color color;
        foreach (var image in GetComponentsInChildren<Image>())
        {
            image.raycastTarget = interactive;
            color = image.color;
            color.a = interactive ? 1 : 0.5f;
            image.color = color;
        }

        foreach (var text in GetComponentsInChildren<Text>())
        {
            text.raycastTarget = interactive;
            color = text.color;
            color.a = interactive ? 1 : 0.5f;
            text.color = color;
        }
    }

    private void SetState(MainButtonState state)
    {
        TextTransform.anchoredPosition = state.TextPosition;
        Image.sprite = state.Sprite;
        Text.color = state.TextColor;
        Outline.effectColor = state.OutlineColor;
        if(ImageTransform != null)
        {
            ImageTransform.anchoredPosition = state.ImagePosition;
        }
    }
}
