﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System;

public class QuestView : MonoBehaviour
{
    public Text QuestName;
    public Text QuestDescription;
    public Text Time;
    public Text Progress;

    public GameObject Hint;

    private QuestClient Quest;

    public void SetQuest(QuestClient quest)
    {
        CancelInvoke();
        Quest = quest;

        if (Quest != null)
        {
            QuestName.text = Localization.GetString(Quest.QuestData.NameKey);
            QuestDescription.text = Localization.GetString(Quest.QuestData.DescriptionKey);
            Progress.text = Quest.DailyQuestProgress.ToString() + " / " + Quest.QuestData.Threshold;

            InvokeRepeating("Repeating", 0, 1f);
        }
    }

    private void Repeating()
    {
        TimeSpan t = TimeSpan.FromSeconds(Quest.QuestExpireTime);
        Time.text = string.Format("{0:D2}:{1:D2}:{2:D2}", t.Hours, t.Minutes, t.Seconds);
    }

    private void TimerTick(object sender, EventArgs e)
    {
        
    }

    public void OnPoinerEnter()
    {
        if(Quest != null)
        {
            Hint.SetActive(true);
        }
    }

    public void OnPoinerExit()
    {
        Hint.SetActive(false);
    }
}
