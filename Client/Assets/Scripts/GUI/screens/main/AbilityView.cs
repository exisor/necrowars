﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class AbilityView : MonoBehaviour 
{
	public delegate void AbilityClickAction(int slot);

	public Image Image;
	public AbilityClient AbilityClient;

	public event AbilityClickAction EventAbilityClick;
	public event AbilityClickAction EventAbilityDown;
	public event AbilityClickAction EventAbilityUp;

	public void SetAbility(AbilityClient abilityClient)
	{
		AbilityClient = abilityClient;
        Image.enabled = abilityClient != null;

        if (abilityClient != null)
		{
			Image.sprite = AbilityClient.Ability.Sprite;
		} 
		else
		{
			Image.sprite = null;
		}
	}

	public void OnClick()
	{
		if(EventAbilityClick != null)
		{
			EventAbilityClick(transform.GetSiblingIndex());
		}
	}

	public void OnDown()
	{
		if(EventAbilityDown != null)
		{
			EventAbilityDown(transform.GetSiblingIndex());
		}
	}

	public void OnUp()
	{
		if(EventAbilityUp != null)
		{
			EventAbilityUp(transform.GetSiblingIndex());
		}
	}
}
