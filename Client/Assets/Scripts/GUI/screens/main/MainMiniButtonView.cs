﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using DG.Tweening;

public class MainMiniButtonView : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler, IPointerDownHandler, IPointerUpHandler
{
    public RectTransform ImageTransform;

    public void OnPointerEnter(PointerEventData eventData)
    {
        UIManager.Instance.IsPointerOverGUI = true;
        DOTween.Kill(ImageTransform);
        ImageTransform.DORotate(new Vector3(0, 0, -45f), 0.15f)
            .OnComplete(() => ImageTransform.DORotate(new Vector3(0, 0, 45f), 0.3f)
            .OnComplete(() => ImageTransform.DORotate(Vector3.zero, 0.15f)));
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        UIManager.Instance.IsPointerOverGUI = false;
        DOTween.Kill(ImageTransform);
        ImageTransform.DORotate(Vector3.zero, 0.2f);
    }

    public void OnPointerDown(PointerEventData eventData)
    {
        ImageTransform.DOScale(new Vector3(0.9f, 0.9f, 0.9f), 0.1f).
            OnComplete(() => ImageTransform.DOScale(Vector3.one, 0.1f));
    }

    public void OnPointerUp(PointerEventData eventData)
    {
        
    }
}
