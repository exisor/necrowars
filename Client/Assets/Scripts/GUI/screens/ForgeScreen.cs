﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using Common.Resources;
using System.Linq;

public class ForgeScreen : GameScreen
{
    private static int ITEMS_ON_PAGE = 16;
    private static readonly long[] FixedPriceIds = { 5, 6, 7, 8, 9, 10 };

    public ItemSlot ForgeSlot;
    public RectTransform AffixContainer;

    public ItemView ItemPrefab;
    public AffixView AffixPrefab;
    public ItemSlot ItemSlotPrefab;
    public RectTransform ItemSlotsContainer;

    public Text Currency;
    public Text Gold;
    public Text PagingText;
    public Text ForgeSlotBaseAffixText;
    public Text ForgeSlotItemNameText;
    public Text ForgeSlotItemLevelText;
    public Text ForgeSlotItemEnchantText;
    public Text ForgeSlotItemEnchantHintText;
    public Text EnchantCostText;
    public Text UpgradeCostText;
    public GameObject LeftButton;
    public GameObject RightButton;
    public GameObject HintContainer;

    public GameObject ForgeHints;
    public Text ForgeHintsButtonText;

    public Button EnchantButton;
    public Button UpgradeButton;
    public Sprite DisableButtonSprite;
    public Sprite DefaultButtonSprite;

    public AudioClip BrokeSound;

    private int _pageCount;
    private int _currentPage;
    private bool _hintsVisible;

    private List<ItemSlot> _slots = new List<ItemSlot>();
    private List<ItemView> _items = new List<ItemView>();
    public List<AffixView> Affixes = new List<AffixView>();

    public void OnEnable()
    {
        ForgeSlot.EventChangeItem += OnChangeForge;

        OnCurrencyChanged();
        OnGoldChanged();

        List<ItemClient> items = DataManager.Instance.GetItemsByTypes(new List<Common.Resources.ItemType>() { Common.Resources.ItemType.Equipment });
        _pageCount = (items.Count - 1) / ITEMS_ON_PAGE + 1;
        _currentPage = 0;

        UpdateForgeSlot();
        UpdateInventory();

        for (int i = 0; i < items.Count; i++)
        {
            ItemSlot itemSlot = (ItemSlot) Instantiate(ItemSlotPrefab, ItemSlotsContainer, false);
            _slots.Add(itemSlot);
            _items.Add(CreateItemView(items[i], itemSlot));
        }

        SetHintVisible(false);
        if (Tutorial.IsForgePhase())
        {
            SetHintVisible(true);
            Tutorial.NextPhase();
        }
    }

    private ItemView CreateItemView(ItemClient itemClient, ItemSlot slot)
    {
        ItemView itemView = (ItemView)Instantiate(ItemPrefab);
        DestroyImmediate(itemView.SellHandler);
        Destroy(itemView.UseHandler);
        itemView.ItemClick += OnItemClick;

        slot.SetItem(itemView);
        itemView.SetItem(itemClient);
        itemView.SetVisibleEquipedText(itemClient.Equiped);
        return itemView;
    }

    public void OnDisable()
    {
        ForgeSlot.EventChangeItem -= OnChangeForge;

        foreach (ItemView item in _items)
        {
            item.ItemClick -= OnItemClick;
            Destroy(item.gameObject);
        }

        _items.Clear();

        DestroyAffixes();

        foreach (var slot in _slots)
        {
            Destroy(slot.gameObject);
        }
        _slots.Clear();
    }

    public override void OnCurrencyChanged()
    {
        Currency.text = DataManager.Instance.Currency.ToString();
    }

    public override void OnGoldChanged()
    {
        Gold.text = DataManager.Instance.Gold.ToString();

        if (DataManager.Instance.GetPowerUp(2) != null)
        {
            Gold.text += " (*)";
        }
    }

    public void OnLeftClick()
    {
        if (LeftButton.activeInHierarchy)
        {
            _currentPage--;
            UpdateInventory();
        }
    }

    public void OnRightClick()
    {
        if (RightButton.activeInHierarchy)
        {
            _currentPage++;
            UpdateInventory();
        }
    }

    public void OnUpgradeClick()
    {
        if(ForgeSlot.ItemView != null)
        {
            SocketManager.Instance.UpgradeItem(DataManager.Instance.GetCurrentCharacterId(), ForgeSlot.ItemView.ItemClient.ItemDTO.Id);
        }
    }

    public void OnHintsClick()
    {
        SetHintVisible(!_hintsVisible);
    }

    private void SetHintVisible(bool value)
    {
        _hintsVisible = value;
        ForgeHints.SetActive(_hintsVisible);
        ForgeHintsButtonText.text = _hintsVisible ? Localization.GetString("GUI_FORGE_HIDE_HINTS") : Localization.GetString("GUI_FORGE_SHOW_HINTS");
    }

    public void OnEnchantClick()
    {
        if (ForgeSlot.ItemView != null)
        {
            SocketManager.Instance.EnchantItem(ForgeSlot.ItemView.ItemClient.ItemDTO.Id);
        }
    }

    private void UpdateInventory()
    {
        LeftButton.SetActive(_pageCount > 1 && _currentPage != 0);
        RightButton.SetActive(_pageCount > 1 && _currentPage != _pageCount - 1);
        PagingText.gameObject.SetActive(_pageCount > 1);

        PagingText.text = (_currentPage + 1).ToString() + "/" + _pageCount.ToString();

        var pos = ItemSlotsContainer.anchoredPosition;
        pos.y = ItemSlotsContainer.sizeDelta.y * _currentPage;
        ItemSlotsContainer.anchoredPosition = pos;
    }

    private void OnItemClick(ItemClient itemClient)
    {

    }

    private void OnChangeForge(ItemSlot itemSlot, ItemView newItem, ItemView oldItem)
    {
        if(oldItem != null)
        {
            oldItem.ItemClient.EventOnUpdate += UpdateForgeSlot;
        }
        if(newItem != null)
        {
            newItem.ItemClient.EventOnUpdate += UpdateForgeSlot;
        } 

        UpdateForgeSlot();
    }

    private void DestroyAffixes()
    {
        foreach (AffixView affixView in Affixes)
        {
            Destroy(affixView.gameObject);
        }

        Affixes.Clear();
    }

    private void UpdateForgeSlot()
    {
        DestroyAffixes();
        UpgradeButton.gameObject.SetActive(ForgeSlot.ItemView != null);
        EnchantButton.gameObject.SetActive(ForgeSlot.ItemView != null);

        if (ForgeSlot.ItemView != null)
        {
            var itemClient = ForgeSlot.ItemView.ItemClient;
            ForgeSlotItemNameText.text = itemClient.GetItemName();
            ForgeSlotItemLevelText.text = DataManager.Instance.CurrentCharacter.Level.ToString() + Localization.GetString("GUI_FORGE_LEVEL");
            ForgeSlotItemEnchantText.text = itemClient.GetEnchantString();

            HintContainer.SetActive(itemClient.ItemDTO.Enchant >= 3);
            ForgeSlotItemEnchantHintText.text = itemClient.GetEnchantHintString();

            var echantCost = FixedPriceIds.Contains(itemClient.ItemData.Id) ? new ItemUtilities.ComplexPrice(900, 0).Gold : 
                ItemUtilities.GetEnchantItemPrice(itemClient.ItemDTO.ItemLevel, itemClient.ItemDTO.Rarity, itemClient.ItemDTO.Enchant).Gold;
            var upgradeCost = FixedPriceIds.Contains(itemClient.ItemData.Id) ? new ItemUtilities.ComplexPrice(0, 1).Currency : 
                ItemUtilities.GetUpgradeItemPrice(itemClient.ItemDTO.ItemLevel, DataManager.Instance.CurrentCharacter.Level,
                itemClient.ItemDTO.Rarity, itemClient.ItemDTO.Enchant, itemClient.GetSumAffixesEnchant()).Currency;

            EnchantCostText.text = echantCost.ToString();
            UpgradeCostText.text = upgradeCost.ToString();

            EnchantButton.GetComponent<Image>().sprite = DataManager.Instance.Gold < echantCost ? DisableButtonSprite : DefaultButtonSprite;
            EnchantButton.interactable = DataManager.Instance.Gold >= echantCost;

            UpgradeButton.GetComponent<Image>().sprite = DataManager.Instance.Gold < upgradeCost ? DisableButtonSprite : DefaultButtonSprite;
            UpgradeButton.interactable = DataManager.Instance.Gold >= upgradeCost;

            foreach (AffixClient affix in itemClient.Affixes)
            {
                if (!affix.AffixData.isBaseAffix)
                {
                    AffixView affixView = (AffixView)Instantiate(AffixPrefab, AffixContainer, false);
                    affixView.SetAffix(itemClient, affix, affix.AffixData.CanEnchant);
                    Affixes.Add(affixView);
                }
                else
                {
                    ForgeSlotBaseAffixText.text = affix.Tooltip;
                }
            }
        }
        else
        {
            ForgeSlotItemNameText.text = "";
            ForgeSlotBaseAffixText.text = "";
        }
    }

    public override void OnItemLost()
    {
        SoundManager.PlaySoundUI(BrokeSound);

        ForgeSlot.ItemView.ItemClick -= OnItemClick;
        _items.Remove(ForgeSlot.ItemView);
        Destroy(ForgeSlot.ItemView.gameObject);

        ForgeSlot.SetItem(null);
    }

    public override void OnNewItems()
    {
        var page = _currentPage;
        OnDisable();
        OnEnable();

        _currentPage = page;
    }

    public void OnOkClick()
    {
        UIManager.Instance.ShowScreen(GameScreen.ScreenNameEnum.Main);
    }
}
