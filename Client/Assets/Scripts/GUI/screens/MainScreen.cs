﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using System;

public class MainScreen : GameScreen
{
    public Image Avatar;
	public Text Name;
	public Text Level;
	public Text AP;
	public Text Currency;
	public Text Experience;
    public Text Gold;

    public GameObject AbilitiesPlus;
    public GameObject ForgeHint;
    public GameObject DiscountHint;

    public RectTransform ExperienceBar;

    public GameObject ShareButton;
    public GameObject InviteButton;
    public MainButtonView ForgeButton;

    public RectTransform PowerUpsContainer;
    public PowerUpView PowerUpPrefab;

    public List<AbilityView> AbilityViews = new List<AbilityView>();

	private int _selectedSlot;
    private float _defaultExperienceBarWidth;
    private List<PowerUpView> _powerUps = new List<PowerUpView>();

    void Awake()
    {
        _defaultExperienceBarWidth = ExperienceBar.sizeDelta.x;
    }

	void OnEnable()
	{
        SetAvatar(DataManager.Instance.UserAvatar);
        Name.text = DataManager.Instance.UserName;
        UpdatePowerUps();
        OnCharacterLevelChanged();
		OnApChanged();
		OnCurrencyChanged();
		OnExperienceChanged();
		OnGoldChanged();
		OnRequiredExperienceChanged();

        if(DataManager.Instance.Social == DataManager.SocialType.Kong)
        {
            ShareButton.SetActive(false);
            InviteButton.SetActive(false);
        }

        for (int i = 0; i < AbilityViews.Count; i++)
		{
			AbilityViews[i].EventAbilityClick += OnAbilityClick;
			AbilityViews[i].SetAbility(DataManager.Instance.GetSelectedAbility(i));
		}

        SocketManager.Instance.GetShopItems();
    }

    public void SetAvatar(Sprite sprite)
    {
        Avatar.sprite = sprite;
    }

    void OnDisable()
    {
        for (int i = 0; i < AbilityViews.Count; i++)
        {
            AbilityViews[i].EventAbilityClick -= OnAbilityClick;
        }
    }

    public void OnShareClick()
    {
        Application.ExternalEval(
              @"
                OGZ.streamPost({message: 'Впечатывай врагов Рагнареком! Воскрешай вражеские армии! Прокачай свою Валькирию в безудержного терминатора!', title: 'Вальхалла ждет тебя!'});"
            );
    }

    public void OnInviteClick()
    {
        Application.ExternalEval(
              @"
                OGZ.showInvite({message :'Пригласить в Вальхаллу'});"
            );
    }

    private void OnAbilityClick(int slot)
	{
		_selectedSlot = slot;

		UIManager.Instance.GetPopup<AbilitySelectPopup>(Popup.PopupNameEnum.AbilitySelect).EventAbilitySelect += OnAbilitySelect;
		UIManager.Instance.ShowPopup(Popup.PopupNameEnum.AbilitySelect);
	}

	private void OnAbilitySelect(AbilityClient selectedAbilityClient)
	{
        UIManager.Instance.GetPopup<AbilitySelectPopup>(Popup.PopupNameEnum.AbilitySelect).EventAbilitySelect -= OnAbilitySelect;

		SocketManager.Instance.SelectAbility(DataManager.Instance.GetCurrentCharacterId(), selectedAbilityClient.Ability.Id, _selectedSlot);
	}

    public void OnForgeClick()
    {
        UIManager.Instance.ShowScreen(GameScreen.ScreenNameEnum.Forge);
    }

    public void OnAbilitiesClick()
    {
    	UIManager.Instance.ShowScreen(GameScreen.ScreenNameEnum.Ability);
    }

    public void OnShopClick()
    {
        UIManager.Instance.ShowScreen(GameScreen.ScreenNameEnum.Shop);
    }

    public void OnLeaderboardClick()
    {
        SocketManager.Instance.GetLeaderboard(DataManager.Instance.GetCurrentCharacterId());
    }

    public void OnInventoryClick()
    {
    	UIManager.Instance.ShowScreen(GameScreen.ScreenNameEnum.Inventory);
    }

	public void OnInBattleClick()
	{
        if (Tutorial.IsFirstBattle())
        {
            ApplicationManager.Instance.ShowTutorial();
        }
        else
        {
            Analytics.gua.sendEventHit("Battle", "StartEnteringBattle", "", (int)DataManager.Instance.GetCurrentCharacterId());
            SocketManager.Instance.InBattle(DataManager.Instance.GetCurrentCharacterId());
        }
	}

	private void OnLoadSceneCallBack()
    {
        GameNetworkManager.Instance.StartClient();
    }

	public override void OnCharacterLevelChanged()
	{
		Level.text = DataManager.Instance.CurrentCharacter.Level.ToString();
        UpdateExperiance();
        ForgeButton.SetInteractive(DataManager.Instance.CurrentCharacter.Level >= 50);
        ForgeHint.SetActive(DataManager.Instance.CurrentCharacter.Level < 50);
    }

    public override void OnAbilitySlotChanged()
    {
		for(int i = 0; i < AbilityViews.Count; i++)
		{
			AbilityViews[i].SetAbility(DataManager.Instance.GetSelectedAbility(i));
		}
    }

    public override void OnApChanged()
	{
		AP.text = "AP " + DataManager.Instance.CurrentCharacter.AP.ToString();
        AbilitiesPlus.SetActive(DataManager.Instance.CurrentCharacter.AP != 0);
	}

	public override void OnCurrencyChanged()
	{
		Currency.text = DataManager.Instance.Currency.ToString();
	}

	public override void OnExperienceChanged()
	{
        UpdateExperiance();
    }

	public override void OnGoldChanged()
	{
		Gold.text = DataManager.Instance.Gold.ToString();
	}

	public override void OnNewPowerUp(PowerUpClient powerUp)
	{
        UpdatePowerUps();
    }

    public override void OnPowerUpExpired(PowerUpClient powerUp)
    {
        UpdatePowerUps();
    }

    public override void OnFixedShop()
    {
        DiscountHint.SetActive(DataManager.Instance.GetAllShopItems().Find(s => s.ShopItem.Limited == true) != null);
    }

    public void UpdatePowerUps()
    {
        foreach (PowerUpView powerUp in _powerUps)
        {
            Destroy(powerUp.gameObject);
        }

        _powerUps = new List<PowerUpView>();
        foreach (PowerUpClient powerUp in DataManager.Instance.GetAllPowerUps())
        {
            PowerUpView powerUpView = (PowerUpView)Instantiate(PowerUpPrefab, PowerUpsContainer, false);
            powerUpView.SetPowerUp(powerUp);
            _powerUps.Add(powerUpView);
        }
    }

    public override void OnRequiredExperienceChanged()
	{
        UpdateExperiance();
    }

    private void UpdateExperiance()
    {
        Experience.text = DataManager.Instance.CurrentCharacter.Experience.ToString() + "/" +
            DataManager.Instance.CurrentCharacter.RequiredExperience.ToString();

        var sizeDelta = ExperienceBar.sizeDelta;
        float progress = (float) DataManager.Instance.CurrentCharacter.Experience / (float) DataManager.Instance.CurrentCharacter.RequiredExperience;
        sizeDelta.x = progress * _defaultExperienceBarWidth;
        ExperienceBar.sizeDelta = sizeDelta;

        if (DataManager.Instance.GetPowerUp(1) != null)
        {
            Experience.text += " (*)";
        }
    }
}
