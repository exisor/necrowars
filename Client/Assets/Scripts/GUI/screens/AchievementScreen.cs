﻿using UnityEngine;
using System.Collections.Generic;
using UnityEngine.UI;

public class AchievementScreen : GameScreen
{
    public AchievementItem ItemPrefab;
    public RectTransform Container;
    public GridLayoutGroup Grid;

    private List<AchievementItem> _items = new List<AchievementItem>();

    public void OnOkClick()
    {
        UIManager.Instance.ShowScreen(GameScreen.ScreenNameEnum.Main);
    }

    public void OnEnable()
    {
        var achievements = DataManager.Instance.GetAllAchievements();
        foreach (var achievement in achievements)
        {
            AchievementItem itemView = (AchievementItem)Instantiate(ItemPrefab, Container, false);
            itemView.SetAchievement(achievement);

            _items.Add(itemView);
        }

        var sizeDelta = Container.sizeDelta;
        sizeDelta.y = (Grid.spacing.y + Grid.cellSize.y) * _items.Count;
        Container.sizeDelta = sizeDelta;
    }

    public void OnDisable()
    {
        foreach (AchievementItem item in _items)
        {
            Destroy(item.gameObject);
        }
        _items.Clear();
    }
}
