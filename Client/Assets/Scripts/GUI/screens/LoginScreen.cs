﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class LoginScreen : GameScreen
{
	public InputField Login;
	public InputField Password;

    public void OnEnable()
    {
        Login.text = PlayerPrefs.GetString("last_login", "T3");
    }
	
	public void OnStartClick()
	{
        PlayerPrefs.SetString("last_login", Login.text);

        SocketManager.Instance.UserLogin = Login.text;
        SocketManager.Instance.UserName = Login.text;
        SocketManager.Instance.UserPassword = Password.text;
        SocketManager.Instance.Login();

        UIManager.Instance.ShowPreloader();
	}
}
