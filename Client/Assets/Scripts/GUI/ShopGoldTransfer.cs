﻿using UnityEngine;
using UnityEngine.UI;

public class ShopGoldTransfer : MonoBehaviour
{
    void Start()
    {
        GetComponent<Button>().onClick.AddListener(OnClick);
    }

    public void OnClick()
    {
        SocketManager.Instance.GetShopItems();
        UIManager.Instance.ShowScreen(GameScreen.ScreenNameEnum.Shop);
        //UIManager.Instance.GetScreen<ShopScreen>(GameScreen.ScreenNameEnum.Shop).SetCategory(1);
    }
}
