﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class LeaderboardItem : MonoBehaviour
{
    public delegate void ItemClickAction(Common.DTO.LeaderboardRecord record);

    public Text Position;
    public Text Name;
    public Text Points;
    public Image BG;

    public Color EvenColor;
    public Color OddColor;

    public bool ChangeGraphic = true;

    public event ItemClickAction ItemClick;

    public Common.DTO.LeaderboardRecord Record;

    public void SetRecord(Common.DTO.LeaderboardRecord record, bool isMyPlayer = false)
    {
        Record = record;

        Position.text = record.Position.ToString();
        if(isMyPlayer)
        {
            Name.text = record.PlayerName;
        }
        else
        {
            Name.text = record.PlayerName + Localization.GetString("GUI_VIEW_PROFILE");
        }
        
        Points.text = record.Points.ToString();

        if(ChangeGraphic)
        {
            Color bgColor = BG.color;

            if (transform.GetSiblingIndex() % 2 == 0)
            {
                bgColor.a = 0;
                Position.color = Name.color = Points.color = EvenColor;
            }
            else
            {
                bgColor.a = 1;
                Position.color = Name.color = Points.color = OddColor;
            }

            BG.color = bgColor;
        }


        /*
        Kills.text = record.Kills.ToString();
        Time.text = record.TopTime.ToString();
        Gold.text = record.TopGold.ToString();
        Army.text = record.ArmySize.ToString();*/
    }

    public void OnClick()
    {
        if (ItemClick != null)
        {
            ItemClick(Record);
        }
    }
}
