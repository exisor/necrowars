﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using System.Collections;
using System;

public class ViewHint : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
{
    public enum ViewHintState { Default, Left, Right };

    public RectTransform Rect;
	public Canvas ItemCanvas;

    public ViewHintState State = ViewHintState.Default;

    public event Action EventPointerEnter;
    public event Action EventPointerExit;

    private float _defaultMinX;
    private Vector2 _defaultAnchorPosition;

    public void Start()
    {
        _defaultAnchorPosition = Rect.anchoredPosition;
        _defaultMinX = Rect.anchorMin.x;
    }

    public void OnPointerEnter(PointerEventData eventData)
	{
		if(eventData.dragging)
		{
			return;
		}
		Show();

        if (EventPointerEnter != null)
        {
            EventPointerEnter();
        }
    }

	public void OnPointerExit(PointerEventData eventData)
	{
		if(eventData.dragging)
		{
			return;
		}
		Hide();

        if (EventPointerExit != null)
        {
            EventPointerExit();
        }
    }

	public void Show()
	{
		Rect.gameObject.SetActive(true);
        if(ItemCanvas != null)
        {
            ItemCanvas.overrideSorting = true;
        }

        Canvas.ForceUpdateCanvases();

        Rearrange();
    }

    public void Update()
    {
        Rearrange();
    }

    private void Rearrange()
    {
        Rect.anchoredPosition = _defaultAnchorPosition;

        Vector2 min = Rect.anchorMin;
        Vector2 max = Rect.anchorMax;
        Vector2 pivot = Rect.pivot;

        min.x = _defaultMinX;
        max.x = 1;
        pivot.x = 0;

        Rect.anchorMin = min;
        Rect.anchorMax = max;
        Rect.pivot = pivot;

        Vector3[] canvasCorners = new Vector3[4];
        UIManager.Instance.Canvas.GetComponent<RectTransform>().GetWorldCorners(canvasCorners);
        Vector3[] corners = new Vector3[4];
        Rect.GetWorldCorners(corners);

        Vector3 pos = Rect.position;
        if (corners[0].y < canvasCorners[0].y)
        {
            pos.y = corners[1].y - corners[0].y;
        }
        Rect.position = pos;

        if (State == ViewHintState.Default)
        {
            if (corners[2].x > canvasCorners[2].x)
            {
                min.x = 0;
                max.x = 1 - _defaultMinX;
                pivot.x = 1;
            }
            else if (corners[1].x < canvasCorners[1].x)
            {
                min.x = _defaultMinX;
                max.x = 1;
                pivot.x = 0;
            }
        }
        else if (State == ViewHintState.Left)
        {
            min.x = 0;
            max.x = 1 - _defaultMinX;
            pivot.x = 1;
        }
        else
        {
            min.x = _defaultMinX;
            max.x = 1;
            pivot.x = 0;
        }

        Rect.anchorMin = min;
        Rect.anchorMax = max;
        Rect.pivot = pivot;
    }

    public void Hide()
	{
		Rect.gameObject.SetActive(false);
        if (ItemCanvas != null)
        {
            ItemCanvas.overrideSorting = false;
        }
    }
}
