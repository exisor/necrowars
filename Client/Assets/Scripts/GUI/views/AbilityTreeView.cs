﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System;

public class AbilityTreeView : MonoBehaviour
{
    public RectTransform Rect;

    private Dictionary<long, AbilityBranchView> _branches = new Dictionary<long, AbilityBranchView>();

    public void OnEnable()
    {
        var branches = GetComponentsInChildren<AbilityBranchView>().ToList();
        foreach (AbilityBranchView view in branches)
        {
            view.SetAbility(DataManager.Instance.GetAbility(Convert.ToInt64(view.gameObject.name)));
            _branches[view.AbilityClient.Ability.Id] = view;
        }

        foreach (AbilityBranchView branch in _branches.Values.ToList())
        {
            branch.CreateChains(this);
            branch.UpdateAbility();
        }
    }

    public AbilityBranchView GetAbilityInTree(long id)
    {
        return _branches[id];
    }

    public void UpdateTree()
    {
        foreach (AbilityBranchView branch in _branches.Values.ToList())
        {
            branch.UpdateAbility();
        }
    }
}
