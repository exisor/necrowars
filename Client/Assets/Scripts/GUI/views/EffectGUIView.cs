﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using DG.Tweening;

public class EffectGUIView : MonoBehaviour
{
    public Image Image;
    public Text Hint;
    public _2dxFX_EdgeColor EdgeColor;

    public void Start()
    {
        DOTween.To(x => EdgeColor._Strength = x, 0f, 5f, 1f).OnComplete(() =>
                    DOTween.To(x => EdgeColor._Strength = x, 5f, 0f, 2f));
    }

    public void SetData(TargetEffect effect)
    {
        Image.sprite = effect.Sprite;
        Hint.text = Localization.GetString(effect.Hint);
    }
}
