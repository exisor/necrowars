﻿using UnityEngine;
using System.Collections;
using DG.Tweening;
using UnityEngine.UI;

public class PreloaderView : MonoBehaviour
{
    public RectTransform RingTransform;
    public Text HintText;
    public GameObject Hint;
    public LocalizationLibrary Hints;
    public GameObject CloseButton;

    void Awake()
    {
        Localization.AddChangeLanguageCallback(SetText);
    }

    void OnEnable()
    {
        TweenRing();

        SetText();

        CloseButton.SetActive(false);
    }

    private void SetText()
    {
        HintText.text = Localization.GetString(Hints.Datas[Random.Range(0, Hints.Datas.Count - 1)].Key).ToUpper();
    }

    private void TweenRing()
    {
        var rot = RingTransform.eulerAngles.z >= 0 ? Random.Range(-180f, 0f) : Random.Range(0f, 180f);
        RingTransform.DORotate(new Vector3(0, 0, rot), 2).OnComplete(() => TweenRing());
    }

    public void OnCloseClick()
    {
        UIManager.Instance.ShowScreen(GameScreen.ScreenNameEnum.Main);
    }

    public void ShowExitButton()
    {
        CloseButton.SetActive(true);
    }

    void OnDisable()
    {
        DOTween.Kill(RingTransform);
    }
}
