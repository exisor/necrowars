﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using System.Linq;
using System;
using UnityEngine.EventSystems;

public class AbilityBranchView : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
{
    private static float OFFSET = 3f;
	public delegate void AbilityClickAction(AbilityClient abilityClient);

	public AbilityClient AbilityClient;
    public RectTransform Rect;

    public Image Glow;
    public GameObject InBattleFrame;
    public GameObject Active;
    public GameObject Passive;
    public Image Image;
	public Text Name;
    public Text Description;
    public Text Level;
	public Button PassivePlusButton;
	public Button PassiveMinusButton;
    public Button ActivePlusButton;
    public Button ActiveMinusButton;

    public Sprite GlowUpgradeSprite;
    public Sprite GlowHoverSprite;
    public Sprite BarEmptySprite;
    public Sprite BarFullSprite;

    public List<Image> PassiveBars;
    public List<int> PassiveBarsLevels;
    public Image ActiveBar;

    public _2dxFX_GrayScale GrayScale;

    public bool CanUpgrade;

    public ChainView ChainPrefab;
    private List<ChainView> _chains = new List<ChainView>();

    public void SetAbility(AbilityClient abilityClient)
	{
		AbilityClient = abilityClient;
        InBattleFrame.gameObject.SetActive(DataManager.Instance.CurrentCharacter.SelectedAbilities.ContainsValue(AbilityClient));
        
        Name.text = AbilityClient.GetName();

		Image.sprite = AbilityClient.Ability.Sprite;
	}

    public void CreateChains(AbilityTreeView tree)
    {
        foreach (var chainData in AbilityClient.Ability.AbilityRequirements)
        {
            ChainView chain = (ChainView)Instantiate(ChainPrefab, transform.parent, false);
            chain.Init(tree.GetAbilityInTree(chainData.Id), tree.GetAbilityInTree(AbilityClient.Ability.Id), chainData);
            var start = chain.Start.Rect.anchoredPosition;
            var end = chain.End.Rect.anchoredPosition;

            chain.name = AbilityClient.Ability.Id.ToString();
            chain.Rect.SetAsFirstSibling();
            chain.Rect.anchoredPosition = start;

            var angle = Vector2.Angle(end - start, Vector2.right);
            if(start.y > end.y)
            {
                angle = -angle;
            }
            chain.Rect.localEulerAngles = new Vector3(0, 0, angle);

            float dis = Vector2.Distance(start, end);
            var sizeDelta = chain.Rect.sizeDelta;
            sizeDelta.x = dis - chain.Start.Rect.sizeDelta.x / OFFSET - chain.End.Rect.sizeDelta.x / OFFSET;
            chain.Rect.sizeDelta = sizeDelta;

            var pivot = chain.Rect.pivot;
            pivot.x = -(chain.Start.Rect.sizeDelta.x / OFFSET) / sizeDelta.x;
            chain.Rect.pivot = pivot;

            _chains.Add(chain);
        }
    }


    public void UpdateAbility()
    {
        Level.text = AbilityClient.Level.ToString();

        CanUpgrade = AbilityClient.CanUpgrade();

        Description.text = CanUpgrade ? AbilityClient.GetUpgradeDescription() : AbilityClient.GetDescription();

        for (int i = 0; i < PassiveBars.Count; i++)
        {
            PassiveBars[i].sprite = AbilityClient.Level >= PassiveBarsLevels[i] ? BarFullSprite : BarEmptySprite;
        }

        var refundCount = 0;
        var refund = DataManager.Instance.GetItemByBaseId(24);
        if (refund != null)
        {
            refundCount = refund.ItemDTO.Quantity;
        }

        GrayScale._EffectAmount = CanUpgrade ? 0 : 1;
        Active.gameObject.SetActive(AbilityClient.Ability.Type == Common.AbilityType.Active && CanUpgrade);
        Passive.gameObject.SetActive(AbilityClient.Ability.Type != Common.AbilityType.Active && CanUpgrade);

        ActiveBar.sprite = AbilityClient.Level == 0 ? BarEmptySprite : BarFullSprite;

        bool canPlus = (AbilityClient.Ability.isEndless || AbilityClient.Level == 0) && CanUpgrade && DataManager.Instance.CurrentCharacter.AP > 0;
        bool canMinus = (AbilityClient.Ability.isEndless || AbilityClient.Level == 1) && CanUpgrade && AbilityClient.Level != 0 && refundCount > 0;

        ActivePlusButton.gameObject.SetActive(canPlus);
        ActiveMinusButton.gameObject.SetActive(canMinus);
        PassivePlusButton.gameObject.SetActive(canPlus);
        PassiveMinusButton.gameObject.SetActive(canMinus);

        Glow.gameObject.SetActive(canPlus);

        foreach (var chain in _chains)
        {
            chain.UpdateChain();
        }
    }

    public void OnPointerEnter(PointerEventData eventData)
    {
        Glow.sprite = GlowHoverSprite;
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        Glow.sprite = GlowUpgradeSprite;
    }

    public void OnPlusClick()
	{
		if(Input.GetKey(KeyCode.LeftShift))
		{
			SocketManager.Instance.ChangeAbility(DataManager.Instance.GetCurrentCharacterId(),
				AbilityClient.Ability.Id, 50, false);
		}
		else
		{
			SocketManager.Instance.ChangeAbility(DataManager.Instance.GetCurrentCharacterId(),
				AbilityClient.Ability.Id, 1, false);
		}
		
	}

	public void OnMinusClick()
	{
		SocketManager.Instance.ChangeAbility(DataManager.Instance.GetCurrentCharacterId(),
			AbilityClient.Ability.Id, -1, false);
	}
	
}
