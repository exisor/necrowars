﻿using UnityEngine;
using System.Collections;

public class ChainView : MonoBehaviour
{
    public RectTransform Rect;
    public RectTransform Bottom;
    public RectTransform Top;

    public AbilityBranchView Start;
    public AbilityBranchView End;

    private Ability.AbilityRequirement _requirement;

    public void Init(AbilityBranchView start, AbilityBranchView end, Ability.AbilityRequirement requirement)
    {
        Start = start;
        End = end;

        _requirement = requirement;
    }

    public void UpdateChain()
    {
        var max = Bottom.anchorMax;
        max.x = (float) Start.AbilityClient.Level / (float) _requirement.Level;

        if (max.x > 1)
        {
            max.x = 1;
        }

        Bottom.anchorMax = max;
    }
}
