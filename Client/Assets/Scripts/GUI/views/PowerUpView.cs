﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class PowerUpView : MonoBehaviour
{
    public Image Image;
    public Text TimeText;

    private PowerUpClient _powerUpClient;

    public void SetPowerUp(PowerUpClient powerUp)
    {
        _powerUpClient = powerUp;
        Image.sprite = powerUp.PowerUpData.Sprite;

        InvokeRepeating("Repeating", 0, 1f);
    }

    private void Repeating()
    {
        //Debug.Log(string.Format("{0:D2}:{1:D2}", (int)_powerUpClient.PowerUp.Expires.TotalMinutes, (int)_powerUpClient.PowerUp.Expires.Seconds));
        if(_powerUpClient.PowerUp.Expires.TotalMinutes < 1)
        {
            TimeText.text = string.Format(Localization.GetString("GUI_SEC"), (int)_powerUpClient.PowerUp.Expires.TotalSeconds);
        }
        else
        {
            TimeText.text = string.Format(Localization.GetString("GUI_MIN"), (int)_powerUpClient.PowerUp.Expires.TotalMinutes);
        }
    }
}
