﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System;

public class ShopItemView : MonoBehaviour 
{
	public delegate void ShopItemClickAction(ShopItemClient shopItemClient);
    public delegate void CurrencyItemClickAction(CurrencyItemClient currencyItemClient);

    public Image Rarity;
    public Text RarityText;
    public Image Image;
	public ShopItemClient ShopItemClient;
    public CurrencyItemClient CurrencyItemClient;
    public Text BuyButtonText;
    public Text QuantityText;
    public Text Description;
    public Text Name;
    public Text Time;
    public Text Discount;
    public GameObject Coin;
    public GameObject Gem;
    public GameObject Currency;
    public Image CurrencyImage;

    public Sprite KongSprite;
    public Sprite VKSprite;

    public event ShopItemClickAction ShopItemClick;
    public event CurrencyItemClickAction CurrencyItemClick;

    public void SetShopItem(ShopItemClient shopItemClient)
	{
		ShopItemClient = shopItemClient;
        ShopItemClient.EventDiscountEnd += OnDiscountEnd;

        Rarity.sprite = ResourceManager.Instance.GetRarity(ShopItemClient.ShopItemData.Rarity).Sprite;
        RarityText.text = Localization.GetString(ResourceManager.Instance.GetRarity(ShopItemClient.ShopItemData.Rarity).NameKey);
        RarityText.color = ResourceManager.Instance.GetRarity(ShopItemClient.ShopItemData.Rarity).Color;
        Image.sprite = ShopItemClient.GetSprite();
		BuyButtonText.text = "";
        Description.text = ShopItemClient.GetItemDescription();
        Name.text = ShopItemClient.GetItemName();
        QuantityText.text = ShopItemClient.ShopItem.Quantity == 1 ? "" : StringUtils.FormatNumber(ShopItemClient.ShopItem.Quantity);
        Coin.SetActive(ShopItemClient.ShopItem.GoldPrice != 0);
        Gem.SetActive(!Coin.activeInHierarchy);
        Currency.SetActive(false);
        if (ShopItemClient.ShopItem.GoldPrice != 0)
		{
			BuyButtonText.text = StringUtils.FormatNumber(ShopItemClient.ShopItem.GoldPrice);
        }
		else if(ShopItemClient.ShopItem.CurrencyPrice != 0)
		{
			BuyButtonText.text = StringUtils.FormatNumber(ShopItemClient.ShopItem.CurrencyPrice);
        }

        Update();
    }

    public void SetCurrencyItem(CurrencyItemClient currencyItemClient)
    {
        Time.text = "";
        Discount.text = "";
        CurrencyItemClient = currencyItemClient;
        Rarity.sprite = ResourceManager.Instance.GetRarity(CurrencyItemClient.ItemData.Rarity).Sprite;
        RarityText.text = Localization.GetString(ResourceManager.Instance.GetRarity(CurrencyItemClient.ItemData.Rarity).NameKey);
        RarityText.color = ResourceManager.Instance.GetRarity(CurrencyItemClient.ItemData.Rarity).Color;
        Image.sprite = CurrencyItemClient.ItemData.Sprite;
        Description.text = CurrencyItemClient.GetItemDescription();
        Name.text = CurrencyItemClient.GetItemName();
        QuantityText.text = CurrencyItemClient.ItemData.Quantity == 1 ? "" : StringUtils.FormatNumber(CurrencyItemClient.ItemData.Quantity);
        Coin.SetActive(false);
        Gem.SetActive(false);
        Currency.SetActive(true);
        if(DataManager.Instance.Social == DataManager.SocialType.Kong)
        {
            CurrencyImage.sprite = KongSprite;
        }
        else if(DataManager.Instance.Social == DataManager.SocialType.VK)
        {
            CurrencyImage.sprite = VKSprite;
        }
        BuyButtonText.text = CurrencyItemClient.Price.ToString();
    }

    public void OnBuy()
	{
		if(CurrencyItemClick != null)
		{
            CurrencyItemClick(CurrencyItemClient);
        }
        else if(ShopItemClick != null)
        {
            ShopItemClick(ShopItemClient);
        }
	}

    private void OnDestroy()
    {
        if(ShopItemClient != null)
        {
            ShopItemClient.EventDiscountEnd -= OnDiscountEnd;
        }
    }

    private void OnDiscountEnd()
    {
        SocketManager.Instance.GetShopItems();
    }

    private void Update()
    {
        if(ShopItemClient != null)
        {
            int discount = 0;
            if(ShopItemClient.ShopItem.ExpiresInMinutes > 0 && ShopItemClient.ShopItem.Discount < 1 && ShopItemClient.ShopItem.Discount > 0)
            {
                discount = (int)((1 - ShopItemClient.ShopItem.Discount) * 100);
            }
            Discount.text = discount > 0 ? discount.ToString() + "%" : "";

            TimeSpan t = TimeSpan.FromMinutes(ShopItemClient.ShopItem.ExpiresInMinutes);
            Time.text = ShopItemClient.ShopItem.ExpiresInMinutes > 0 ? string.Format("{0:D2}:{1:D2}", t.Hours, t.Minutes) : "";
        }
    }
}
