﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class AchievementItem : MonoBehaviour
{
    public Text Name;
    public Text Description;
    public Text Tier;
    public Image Image;

    public Sprite CompleteSprite;
    public Sprite NonCompleteSprite;

    public ItemView ItemViewPrefab;
    public RectTransform ItemViewContainer;

    public AchievementClient AchievementClient;

    public void SetAchievement(AchievementClient achievementClient)
    {
        AchievementClient = achievementClient;

        Name.text = Localization.GetString(AchievementClient.AchievementData.NameKey);
        Description.text = Localization.GetString(AchievementClient.AchievementData.DescriptionKey).ToUpper();
        Image.sprite = AchievementClient.IsComplete() ? CompleteSprite : NonCompleteSprite;

        var step = AchievementClient.GetCurrentStep();

        ItemView itemView = (ItemView)Instantiate(ItemViewPrefab, ItemViewContainer, false);
        DestroyImmediate(itemView.DragHandler);
        DestroyImmediate(itemView.SellHandler);
        DestroyImmediate(itemView.UseHandler);

        itemView.SetItemResource(ResourceManager.Instance.GetItemById(step.RewardId), step.Quantity);

        if (AchievementClient.Achievement == null)
        {
            Tier.text = Localization.GetString("GUI_ACHIV_TIER") + " 1";
        }
        else
        {
            Tier.text = AchievementClient.GetTierString();

            if(AchievementClient.AchievementData.ShowProgress)
            {
                Tier.text += AchievementClient.GetProgressString();
            }
        }
    }
}
