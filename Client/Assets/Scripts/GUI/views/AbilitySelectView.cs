﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using System.Linq;
using System;

public class AbilitySelectView : MonoBehaviour 
{
	public delegate void AbilityClickAction(AbilityClient abilityClient);

	public AbilityClient AbilityClient;

	public Image Image;
	public Text Name;
    public Text Description;

    public Sprite DefaultSprite;

    public event AbilityClickAction EventAbilityClick;

	private bool _canUpgrade = true;

	public void SetAbility(AbilityClient abilityClient)
	{
		AbilityClient = abilityClient;

		Name.text = AbilityClient != null ? AbilityClient.GetName().ToUpper() : "";
        Description.text = AbilityClient != null ? AbilityClient.GetDescription() : "";

        Image.sprite = AbilityClient != null ? AbilityClient.Ability.Sprite : DefaultSprite;
    }

	public void OnAbilityClick()
	{
		if(EventAbilityClick != null)
		{
			EventAbilityClick(AbilityClient);
		}
	}
	
}
