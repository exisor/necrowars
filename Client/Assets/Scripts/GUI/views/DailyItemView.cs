﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System;

public class DailyItemView : MonoBehaviour
{
    public Image Rarity;
    public Text RarityText;
    public Image Image;
    public Image BackgroundImage;
    public Text Day;
    public Text Name;
    public Text Description;
    public Item ItemResource;
    public ViewHint Hint;
    public GameObject Arrow;

    public Sprite PreviosSprite;
    public Sprite CurrentSprite;
    public Sprite NextSprite;

    public void SetItemResource(Item item, int day)
    {
        ItemResource = item;

        Day.text = Localization.GetString("GUI_DAY") + day.ToString();

        Name.text = Localization.GetString(item.NameKey);
        Description.text = "\n" + Localization.GetString(item.DescriptionKey);

        Rarity.sprite = ResourceManager.Instance.GetRarity(ItemResource.Rarity).Sprite;
        RarityText.text = Localization.GetString(ResourceManager.Instance.GetRarity(ItemResource.Rarity).NameKey);
        RarityText.color = ResourceManager.Instance.GetRarity(ItemResource.Rarity).Color;
        Image.sprite = item.DefaultSprite;
    }

    public void SetCurrent()
    {
        Arrow.SetActive(false);
        BackgroundImage.sprite = CurrentSprite;
    }

    public void SetPrevios()
    {
        Arrow.SetActive(true);
        BackgroundImage.sprite = PreviosSprite;
    }

    public void SetNext()
    {
        Arrow.SetActive(false);
        BackgroundImage.sprite = NextSprite;
    }

    public void DisableHint()
    {
        Hint.Hide();
        Hint.enabled = false;
    }

    public void EnableHint()
    {
        Hint.enabled = true;
    }
}
