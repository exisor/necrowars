﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System;

public class ItemView : MonoBehaviour 
{
	public delegate void ItemClickAction(ItemClient itemClient);
	public delegate void ItemSellAction(ItemView itemView);
    public delegate void ItemUseAction(ItemView itemView);
    public delegate void ItemHintAction(ItemView itemView);

    public Image Rarity;
    public Text RarityText;
    public Image Image;
    public GameObject Root;
    public ItemClient ItemClient;
	public ViewHint Hint;
	public DragHandler DragHandler;
	public ItemSlot Slot;
	public SellHandler SellHandler;
    public UseHandler UseHandler;
    public Text Quantity;
    public Text Level;
    public Text Equiped;
    public Text Price;
    public Text Description;
    public Text Name;

    public event ItemClickAction ItemClick;
	public event ItemSellAction EventSell;
    public event ItemUseAction EventUse;
    public event ItemHintAction EventShowHint;
    public event ItemHintAction EventHideHint;

    public void Start()
	{
        if(DragHandler != null)
        {
            DragHandler.EventBeginDrag += OnBeginDrag;
            DragHandler.EventEndDrag += OnEndDrag;
        }

        if(Hint != null)
        {
            Hint.EventPointerEnter += OnShowHint;
            Hint.EventPointerExit += OnHideHint;
        }
        if (SellHandler != null)
        {
            SellHandler.EventSell += OnSell;
        }

        if (UseHandler != null)
        {
            UseHandler.EventUse += OnUse;
        }
    }

    public void OnDestroy()
    {
        if (ItemClient != null)
        {
            ItemClient.EventOnUpdate -= UpdateView;
        }
    }

	public void SetItem(ItemClient itemClient)
	{
        if(ItemClient != itemClient)
        {
            if(ItemClient != null)
            {
                ItemClient.EventOnUpdate -= UpdateView;
            }

            ItemClient = itemClient;

            ItemClient.EventOnUpdate += UpdateView;

            UpdateView();
        }
    }

    public void SetItemResource(Item item, int quantity = 1)
    {
        Quantity.text = quantity == 1 ? "" : quantity.ToString();

        Name.text = Localization.GetString(item.NameKey);
        Description.text = Localization.GetString(item.DescriptionKey);

        Rarity.sprite = ResourceManager.Instance.GetRarity(item.Rarity).Sprite;
        RarityText.text = Localization.GetString(ResourceManager.Instance.GetRarity(item.Rarity).NameKey);
        RarityText.color = ResourceManager.Instance.GetRarity(item.Rarity).Color;
        Image.sprite = item.DefaultSprite;

        Level.text = "";
    }

    public void SetFullStretch()
    {
        var rect = GetComponent<RectTransform>();
        rect.anchorMin = Vector2.zero;
        rect.anchorMax = Vector2.one;
        rect.offsetMin = Vector2.zero;
        rect.offsetMax = Vector2.zero;
    }

    public void SetVisibleEquipedText(bool value)
    {
        Equiped.gameObject.SetActive(value);
    }

    public void SetVisiblePriceText(bool value)
    {
        Price.gameObject.SetActive(value);
    }

    public void UpdateView()
    {
        Quantity.text = ItemClient.ItemDTO.Quantity == 1 ? "" : ItemClient.ItemDTO.Quantity.ToString();

        Name.text = ItemClient.GetItemName();
        Description.text = ItemClient.GetItemDescription();

        Rarity.sprite = ResourceManager.Instance.GetRarity(ItemClient.ItemDTO.Rarity).Sprite;
        RarityText.text = Localization.GetString(ResourceManager.Instance.GetRarity(ItemClient.ItemDTO.Rarity).NameKey);
        RarityText.color = ResourceManager.Instance.GetRarity(ItemClient.ItemDTO.Rarity).Color;

        Image.sprite = ItemClient.GetSprite();

        Level.text = ItemClient.ItemDTO.ItemLevel.ToString() + Localization.GetString("GUI_ITEM_LVL");

        Price.text = StringUtils.FormatNumber((Common.Resources.ItemUtilities.GetPrice(ItemClient.ItemDTO.ItemLevel, ItemClient.ItemDTO.Rarity).Gold * ItemClient.ItemDTO.Quantity));
    }

    public void SetItemResource(Item item)
    {
        Quantity.text = "";

        //Hint.SetItem(ItemClient);

        Image.sprite = item.DefaultSprite;

        Level.text = "";
    }

    public void SetSlot(ItemSlot slot)
	{
		Slot = slot;
        if(Slot != null && Hint != null)
        {
            Hint.State = Slot.HintState;
        }
        if (SellHandler != null)
        {
            UpdateSellButton();
        }
        if (UseHandler != null)
        {
            UseHandler.Show();
        }
    }

    public void OnShowHint()
    {
        if (EventShowHint != null)
        {
            EventShowHint(this);
            UpdateView();
        }
    }

    public void OnHideHint()
    {
        if (EventHideHint != null)
        {
            EventHideHint(this);
            UpdateView();
        }
    }

    public void DisableHint()
    {
        Hint.Hide();
        Hint.enabled = false;
    }

    public void EnableHint()
    {
        Hint.enabled = true;
    }

    public void OnBeginDrag()
	{
        DisableHint();

        if (SellHandler != null)
        {
            SellHandler.Hide();
        }
        if (UseHandler != null)
        {
            UseHandler.Hide();
        }
    }

	public void OnEndDrag()
	{
        EnableHint();

        if (SellHandler != null)
        {
            UpdateSellButton();
        }

        if (UseHandler != null)
        {
            UseHandler.Show();
        }

        UpdateEnchant();
    }

	public void OnClick()
	{
		if(ItemClick != null)
		{
			ItemClick(ItemClient);
		}
	}

	public void OnSell()
	{
		if(EventSell != null)
		{
			EventSell(this);
		}
	}

    public void OnUse()
    {
        if (EventUse != null)
        {
            EventUse(this);
        }
    }

    private void UpdateEnchant()
    {
        if (Slot.isEnchant)
        {
            DisableHint();
        }
        else
        {
            EnableHint();
        }
    }

    private void UpdateSellButton()
	{
		if(!Slot.isAnyType)
		{
			SellHandler.Hide();
		}
		else
		{
			SellHandler.Show();
		}
	}
}
