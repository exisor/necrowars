﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class AffixView : MonoBehaviour
{
    public Text AffixName;
    public Text Gold;

    public ItemClient Item;
    public AffixClient Affix;

    public Button EnchantButton;
    public Image EnchantButtonImage;

    public Sprite DisableButtonSprite;
    public Sprite DefaultButtonSprite;

    public void SetAffix(ItemClient item, AffixClient affix, bool canUpgrade)
    {
        Item = item;
        Affix = affix;
        EnchantButton.gameObject.SetActive(canUpgrade);

        AffixName.text = canUpgrade ? Affix.EnchantTooltip : Affix.Tooltip;

        var gold = Common.Resources.ItemUtilities.GetEnchantAffixPrice(item.ItemDTO.ItemLevel, item.ItemDTO.Rarity, affix.AffixDTO.Enchant).Gold;
        EnchantButtonImage.sprite = DataManager.Instance.Gold < gold ? DisableButtonSprite : DefaultButtonSprite;
        EnchantButton.interactable = DataManager.Instance.Gold >= gold;

        Gold.text = gold.ToString();
    }

    public void OnEnchantClick()
    {
        if(Affix.AffixDTO.Enchant >= Item.ItemDTO.Enchant)
        {
            var need = Item.ItemDTO.Enchant + 1;
            UIManager.Instance.ShowMessage(Popup.PopupNameEnum.Notification);
            UIManager.Instance.GetMessage<NotificationPopup>(Popup.PopupNameEnum.Notification).ShowMessage(
                string.Format(Localization.GetString("NeedEnchantError"), need.ToString()));
        }
        else
        {
            SocketManager.Instance.EnchantAffix(Item.ItemDTO.Id, Affix.AffixDTO.Id);
        }
    }
	
}
