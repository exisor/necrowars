﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public class AchievementRewardPopup : Popup
{
    public ItemView ItemPrefab;
    public RectTransform ItemsContainer;
    public Text Name;
    public Text Description;

    private List<ItemView> _items = new List<ItemView>();

    public void ShowAchievement(long achievementId, List<Common.DTO.Item> newItems)
    {
        OnDisable();

        AchievementClient achievement = DataManager.Instance.GetAchievementById(achievementId);

        Name.text = Localization.GetString(achievement.AchievementData.NameKey);
        Description.text = Localization.GetString(achievement.AchievementData.DescriptionKey);
        Description.text += "\n" + achievement.GetTierString();

        foreach (Common.DTO.Item item in newItems)
        {
            ItemView itemView = (ItemView)Instantiate(ItemPrefab, ItemsContainer, false);
            DestroyImmediate(itemView.DragHandler);
            DestroyImmediate(itemView.SellHandler);
            DestroyImmediate(itemView.UseHandler);
            itemView.SetItem(new ItemClient(item));
            _items.Add(itemView);
        }
    }

    public void OnDisable()
    {
        foreach (ItemView item in _items)
        {
            Destroy(item.gameObject);
        }
        _items.Clear();
    }


    public void OnOkClick()
    {
        UIManager.Instance.HideCurrentPopup();
    }
}
