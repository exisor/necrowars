﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class DailyRewardPopup : Popup
{
    public DailyItemView ItemPrefab;
    public RectTransform ItemsContainer;

    private List<DailyItemView> _items = new List<DailyItemView>();

    public void ShowItems(List<long> newItems, int current)
    {
        OnDisable();
        int day;
        for (int i = 0; i < newItems.Count; i++)
        {
            day = i + 1;
            DailyItemView itemView = (DailyItemView)Instantiate(ItemPrefab, ItemsContainer, false);
            itemView.SetItemResource(ResourceManager.Instance.GetItemById(newItems[i]), day);

            

            if(day == current)
            {
                itemView.SetCurrent();
            }
            else if (day < current)
            {
                itemView.SetPrevios();
            }
            else 
            {
                itemView.SetNext();
            }
            _items.Add(itemView);
        }
    }

    public void OnDisable()
    {
        foreach (DailyItemView item in _items)
        {
            Destroy(item.gameObject);
        }
        _items.Clear();
    }


    public void OnOkClick()
    {
        UIManager.Instance.HideCurrentPopup();
    }
}
