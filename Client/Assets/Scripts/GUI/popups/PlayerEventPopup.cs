﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public class PlayerEventPopup : Popup
{
    public Text Name;
    public Text Description;

    public ItemView ItemPrefab;
    public RectTransform ItemsContainer;

    private List<ItemView> _items = new List<ItemView>();

    public void ShowEvent(long eventId, List<Common.DTO.Item> newItems)
    {
        OnDisable();
        var playerEvent = ResourceManager.Instance.GetPlayerEventById(eventId);

        Name.text = Localization.GetString(playerEvent.NameKey);
        Description.text = Localization.GetString(playerEvent.DescriptionKey);

        foreach (Common.DTO.Item item in newItems)
        {
            ItemView itemView = (ItemView)Instantiate(ItemPrefab, ItemsContainer, false);
            DestroyImmediate(itemView.DragHandler);
            DestroyImmediate(itemView.SellHandler);
            DestroyImmediate(itemView.UseHandler);
            itemView.SetItem(new ItemClient(item));
            _items.Add(itemView);
        }
    }

    public void OnDisable()
    {
        foreach (ItemView item in _items)
        {
            Destroy(item.gameObject);
        }
        _items.Clear();
    }


    public void OnOkClick()
    {
        UIManager.Instance.HideCurrentPopup();
    }
}
