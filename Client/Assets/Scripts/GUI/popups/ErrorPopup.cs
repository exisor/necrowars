﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public class ErrorPopup : Popup
{
    public Text Message;

    public void OnOkClick()
    {
        UIManager.Instance.HideCurrentMessage();
        UIManager.Instance.ShowPreloader();
        SocketManager.Instance.Connect();
    }

    public void ShowMessage(string message)
    {
        Message.text = message;
    }
}
