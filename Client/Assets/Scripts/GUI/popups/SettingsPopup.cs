﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class SettingsPopup : Popup 
{
	public Slider MusicSlider;
	public Slider SoundSlider;
    public Slider GraphicsSlider;

    public override void OnEnable()
    {
        base.OnEnable();

        MusicSlider.value = SoundManager.GetMusicVolume();
        SoundSlider.value = SoundManager.GetSoundVolume();
        GraphicsSlider.value = 1 - ((float) ApplicationManager.Instance.FrameRate / (float) ApplicationManager.MIN_FRAMERATE);
    }

    public void ChangeGraphics(float value)
    {
        int framerate = (int) Mathf.Round((1 - value) * ApplicationManager.MIN_FRAMERATE);
        ApplicationManager.Instance.SetFrameRate(framerate);
    }

    public void OnOkClick()
    {
    	UIManager.Instance.HideSettings();
    }
}
