﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using ClientProtocol;
using System;
using DG.Tweening;
using UnityEngine.Events;

public class BattleEndPopup : Popup
{
    public Text Kills;
    public Text Time;
    public Text Exp;
    public Text Gold;
    public Text Chest;
    public Text CurrentLevel;
    public Text NextLevel;
    public RectTransform ExperienceBar;
    public Image ChestImage;

    public Button OkButton;
    public Button ChestButton;

    private BattleEndedEvent _message;
    private float _defaultExperienceBarWidth;

    private float _previosExperiance;
    private int _newLevel;
    private float _newExperience;
    private float _newRequiredExperience;

    void Awake()
    {
        _defaultExperienceBarWidth = ExperienceBar.sizeDelta.x;
    }

    public override void OnEnable()
    {
        base.OnEnable();

        UpdateXP();
    }

    public void OnDisable()
    {
        ExperienceBar.DOKill();
    }

    public void OnComplete()
	{
        if (_message.ChestRewardId == 0 && Tutorial.IsAbilityScreenPhase())
        {
            UIManager.Instance.ShowScreen(GameScreen.ScreenNameEnum.Ability);
        }
        else
        {
            UIManager.Instance.HideCurrentPopup();
        }
	}

    public void ShowInfo(BattleEndedEvent message)
    {
        if (Tutorial.IsRealBattleComplete())
        {
            Tutorial.NextPhase();
        }

        _message = message;

        Chest.gameObject.SetActive(message.ChestRewardId != 0);
        ChestImage.gameObject.SetActive(message.ChestRewardId != 0);
        ChestButton.gameObject.SetActive(message.ChestRewardId != 0);
        if (message.ChestRewardId != 0)
        {
            var chest = ResourceManager.Instance.GetChestById(message.ChestRewardId);
            Chest.text = Localization.GetString(chest.NameKey);
            if(message.ChestQuantity > 1)
            {
                Chest.text += " x" + message.ChestQuantity.ToString();
            }
            Chest.color = chest.RarityColor;
            ChestImage.sprite = chest.Sprite;
        }

        TimeSpan t = TimeSpan.FromSeconds(message.TimeAlive);
        Time.text = Localization.GetString("GUI_END_TIME") + string.Format("{0:D2}:{1:D2}:{2:D2}", t.Hours, t.Minutes, t.Seconds);
        Kills.text = Localization.GetString("GUI_END_KILLS") + message.Kills;
        Exp.text = Localization.GetString("GUI_END_EXP") + message.ExpGained;
        Gold.text = Localization.GetString("GUI_END_GOLD") + message.GoldGained;

        _previosExperiance = DataManager.Instance.CurrentCharacter.Experience;
        _newLevel = DataManager.Instance.CurrentCharacter.Level;
        _newExperience = _previosExperiance + message.ExpGained;
        _newRequiredExperience = DataManager.Instance.CurrentCharacter.RequiredExperience;
    }

    private void UpdateXP()
    {
        CurrentLevel.text = _newLevel.ToString();
        NextLevel.text = (_newLevel + 1).ToString();

        float from = _previosExperiance / _newRequiredExperience;
        float to = _newExperience / _newRequiredExperience;

        if (to > 1)
        {
            _previosExperiance = 0;
            to = 1;
            _newLevel++;
            _newExperience -= _newRequiredExperience;
            _newRequiredExperience = Common.Util.GetRequiredExperienceForLevel(_newLevel);

            AnimateXPBar(from, to, UpdateXP);
        }
        else
        {
            AnimateXPBar(from, to);
        }

        
    }

    private void AnimateXPBar(float from, float to, UnityAction callback = null)
    {
        var sizeDelta = ExperienceBar.sizeDelta;
        sizeDelta.x = from * _defaultExperienceBarWidth;
        ExperienceBar.sizeDelta = sizeDelta;
        sizeDelta.x = to * _defaultExperienceBarWidth;

        if (callback != null)
        {
            ExperienceBar.DOSizeDelta(sizeDelta, to - from).OnComplete(() => callback.Invoke());
        }
        else
        {
            ExperienceBar.DOSizeDelta(sizeDelta, to - from);
        }
    }

    /*
	public override void OnChestOpened(List<Common.DTO.Item> newItems)
	{
		UIManager.Instance.ShowPopup(Popup.PopupNameEnum.ChestOpen);
		UIManager.Instance.GetPopup<ChestOpenPopup>(Popup.PopupNameEnum.ChestOpen).ShowItems(newItems);
	}
    */
}
