﻿using UnityEngine;
using System.Collections.Generic;
using UnityEngine.UI;

public class ProfilePopup : Popup
{
    public ItemView ItemPrefab;

    public List<ItemSlot> EquipmentSlots = new List<ItemSlot>();
    public List<AbilitySelectView> AbilityViews = new List<AbilitySelectView>();
    private List<ItemView> _items = new List<ItemView>();

    public void OnDisable()
    {
        foreach (ItemView item in _items)
        {
            Destroy(item.gameObject);
        }
        _items.Clear();
    }

    public void ShowCharacter(string playerName, Common.DTO.Character Character, List<Common.DTO.Item> items)
    {
        for (int i = 0; i < Character.AbilitySlots.Count; i++)
        {
            var ability = Character.Abilities.Find(a => a.Id == Character.AbilitySlots[i].AbilityId);
            if (ability != null)
            {
                AbilityViews[i].SetAbility(new AbilityClient(ability));
            }
            else
            {
                AbilityViews[i].SetAbility(null);
                DestroyImmediate(AbilityViews[i].GetComponent<ViewHint>());
            } 
        }

        if (items != null)
        {
            foreach (var item in items)
            {
                ItemClient itemClient = new ItemClient(item);
                var slot = EquipmentSlots.Find(s => s.SlotType == itemClient.ItemData.ItemSlot);
                if(slot != null)
                {
                    ItemView itemView = (ItemView)Instantiate(ItemPrefab);
                    DestroyImmediate(itemView.SellHandler);
                    DestroyImmediate(itemView.DragHandler);
                    DestroyImmediate(itemView.UseHandler);
                    itemView.SetItem(itemClient);

                    slot.SetItem(itemView);

                    _items.Add(itemView);
                }
            }
        }
        
    }

    public void OnOkClick()
    {
        UIManager.Instance.HideCurrentPopup();
    }
}
