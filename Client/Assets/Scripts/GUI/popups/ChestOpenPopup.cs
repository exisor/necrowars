﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ChestOpenPopup : Popup 
{
	public ItemView ItemPrefab;
	public RectTransform ItemsContainer;

    public AudioClip ChestSound;
    public AudioClip LegendaryChestSound;

    private List<ItemView> _items = new List<ItemView>();

	public void ShowItems(long chestId, List<Common.DTO.Item> newItems)
	{
        OnDisable();

        OpenPopupSound = chestId == 4 ? LegendaryChestSound : ChestSound;


        foreach (Common.DTO.Item item in newItems)
        {
            var itemClient = new ItemClient(item);
            ItemView itemView = (ItemView) Instantiate(ItemPrefab, ItemsContainer, false);
            DestroyImmediate(itemView.DragHandler);

            if(itemClient.ItemData.Types.Contains(Common.Resources.ItemType.Instant) || itemClient.ItemData.StackingSize != 1 
                || itemClient.ItemDTO.Quantity != 1 || !Tutorial.IsMainTutorialComplete())
            {
                DestroyImmediate(itemView.SellHandler);
            }
            else
            {
                itemView.SellHandler.Show();
                itemView.EventSell += OnItemSell;
            }

            DestroyImmediate(itemView.UseHandler);
            itemView.SetItem(itemClient);
            
            _items.Add(itemView);
        }
	}

	public void OnDisable()
	{
		foreach (ItemView item in _items)
        {
            item.EventSell -= OnItemSell;
            Destroy(item.gameObject);
        }
        _items.Clear();
	}

    private void OnItemSell(ItemView itemView)
    {
        SocketManager.Instance.SellItem(itemView.ItemClient.ItemDTO.Id);
        itemView.Root.SetActive(false);
        Destroy(itemView.SellHandler);
    }


    public void OnOkClick()
    {
        if (Tutorial.IsAbilityScreenPhase())
        {
            UIManager.Instance.ShowScreen(GameScreen.ScreenNameEnum.Ability);
        }
        else
        {
            UIManager.Instance.HideCurrentPopup();
        }
    }
}
