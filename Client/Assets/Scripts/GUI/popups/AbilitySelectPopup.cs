﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System;

public class AbilitySelectPopup : Popup 
{
	public delegate void AbilitySelectAction(AbilityClient selectedAbilityClient);

	public AbilitySelectView AbilityBranchViewPrefab;
	public RectTransform AbilityContainer;
    public GameObject NoActiveSkillsText;

    public AudioClip SelectAbilitySound;

    public event AbilitySelectAction EventAbilitySelect;
    public event Action EventAbilityShow;

    private List<AbilitySelectView> _abilitiesViews = new List<AbilitySelectView>();

	public override void OnEnable()
	{
        base.OnEnable();

		List<AbilityClient> abilities = DataManager.Instance.GetAllAbilities().Where(ability => ability.Ability.isSelectable && ability.Level != 0).ToList();

        NoActiveSkillsText.SetActive(abilities.Count == 0);

        foreach (AbilityClient ability in abilities)
        {
            AbilitySelectView abilityView = (AbilitySelectView) Instantiate(AbilityBranchViewPrefab, AbilityContainer, false);
        	abilityView.EventAbilityClick += OnAbilityClick;
        	abilityView.SetAbility(ability);
        	_abilitiesViews.Add(abilityView);
        }

        if (EventAbilityShow != null)
        {
            EventAbilityShow();
        }
    }

	public void OnDisable()
	{
		foreach (AbilitySelectView abilityView in _abilitiesViews)
        {
        	Destroy(abilityView.gameObject);
        }
        _abilitiesViews.Clear();
	}

	public void OnOkClick()
    {
    	UIManager.Instance.HideCurrentPopup();
    }

    public List<AbilitySelectView> GetAbilityViews()
    {
        return _abilitiesViews;
    }

    private void OnAbilityClick(AbilityClient selectedAbilityClient)
	{
		if(EventAbilitySelect != null)
		{
			EventAbilitySelect(selectedAbilityClient);
		}

        SoundManager.PlaySoundUI(SelectAbilitySound);
        UIManager.Instance.HideCurrentPopup();
	}
}
