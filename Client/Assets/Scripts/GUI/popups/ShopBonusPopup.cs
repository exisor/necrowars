﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public class ShopBonusPopup : Popup
{
    public Text CurrencyBoughtText;

    public override void OnEnable()
    {
        base.OnEnable();

        CurrencyBoughtText.text = Localization.GetString("GUI_BONUS_SHOP_POINT") + DataManager.Instance.CurrencyBought;
    }

    public void OnOkClick()
    {
        UIManager.Instance.HideCurrentPopup();
    }
}
