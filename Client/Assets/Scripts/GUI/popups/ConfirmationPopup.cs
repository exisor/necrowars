﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public class ConfirmationPopup : Popup
{
    private long _itemId;

    public void OnYesClick()
    {
        SocketManager.Instance.SellItem(_itemId);
        UIManager.Instance.HideConfirmation();
    }

    public void OnNoClick()
    {
        UIManager.Instance.HideConfirmation();
    }

    public void SetItemId(long itemId)
    {
        _itemId = itemId;
    }
}
