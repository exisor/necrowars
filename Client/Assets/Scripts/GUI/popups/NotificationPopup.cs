﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public class NotificationPopup : Popup
{
    public Text Message;
    public Button ShopButton;

    public override void OnEnable()
    {
        base.OnEnable();

        SetVisibleShopButton(false);
    }

    public void SetVisibleShopButton(bool value)
    {
        ShopButton.gameObject.SetActive(value);
    }

    public void OnOkClick()
    {
        UIManager.Instance.HideCurrentMessage();
    }

    public void OnShopClick()
    {
        UIManager.Instance.ShowScreen(GameScreen.ScreenNameEnum.Shop);
        UIManager.Instance.HideCurrentMessage();
    }

    public void ShowMessage(string message)
    {
        Message.text = message;
    }
}
