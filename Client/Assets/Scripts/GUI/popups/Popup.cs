﻿using UnityEngine;
using System.Collections;
using DG.Tweening;

public class Popup : MonoBehaviour 
{
	public enum PopupNameEnum {ChestOpen, AbilitySelect, Error, Notification, DailyReward, AchievementReward, QuestReward, Settings, BattleEnd, Profile, PlayerEvent, Confirmation, ShopBonus, Offer };
	
	public PopupNameEnum PopupName;

    public AudioClip OpenPopupSound;

    public virtual void OnEnable()
    {
        SoundManager.PlaySoundUI(OpenPopupSound);

        transform.localScale = new Vector3(0.1f, 0.1f, 0.1f);
        transform.DOScale(1, 0.4f).SetEase(Ease.OutBack);
    }
}
