﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine.UI;

public class OfferPopup : Popup
{
    public ShopItemView ItemPrefab;
    public RectTransform ItemsContainer;
    public Text OfferText;

    private List<ShopItemView> _items = new List<ShopItemView>();

    public void ShowOffer(string offer, List<long> items)
    {
        OnDisable();

        OfferText.text = offer;

        List<ShopItemClient> shopItems = DataManager.Instance.GetAllShopItems().Where(item => items.Contains(item.ShopItem.BaseId)).ToList();
        foreach (ShopItemClient item in shopItems)
        {
            ShopItemView itemView = (ShopItemView)Instantiate(ItemPrefab, ItemsContainer, false);
            itemView.SetShopItem(item);
            itemView.ShopItemClick += OnShopItemClick;
            _items.Add(itemView);
        }
    }

    public void OnDisable()
    {
        foreach (ShopItemView item in _items)
        {
            Destroy(item.gameObject);
        }
        _items.Clear();
    }

    public void OnOkClick()
    {
        UIManager.Instance.HideCurrentPopup();
    }

    private void OnShopItemClick(ShopItemClient shopItemClient)
    {
        SocketManager.Instance.BuyItem(DataManager.Instance.GetCurrentCharacterId(), shopItemClient.ShopItem.Id);
        UIManager.Instance.HideCurrentPopup();
    }
}
