﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class TextLocalizator : MonoBehaviour
{
    public string Key;
    public LocalizationLibrary Library;
    void Start ()
    {
        SetText();
        Localization.AddChangeLanguageCallback(SetText);
    }
	
    private void SetText()
    {
        GetComponent<Text>().text = Localization.GetString(Key);
    }
}
