﻿using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;
using System;

public class DragAbilityScreen : MonoBehaviour, IBeginDragHandler, IDragHandler, IEndDragHandler 
{
	public event Action EventBeginDrag;
	public event Action EventEndDrag;

	private CanvasGroup _canvasGroup;

	private Vector2 offset;

	private RectTransform _canvasRect;
	private RectTransform _rect;

	public void Start()
	{
		_rect = GetComponent<RectTransform>();
		_canvasRect = UIManager.Instance.Canvas.GetComponent<RectTransform>();
	}

	public void OnBeginDrag (PointerEventData eventData)
	{
		if(EventBeginDrag != null)
		{
			EventBeginDrag();
		}

		offset = (Vector2)transform.position - eventData.position;

		_canvasGroup = gameObject.AddComponent<CanvasGroup>();
		_canvasGroup.blocksRaycasts = false;
	}

	public void OnDrag (PointerEventData eventData)
	{
		transform.position = eventData.position + offset;

		Vector3 anchPos = _rect.anchoredPosition;
		
		if(_rect.sizeDelta.x < _canvasRect.sizeDelta.x)
		{
			anchPos.x = 0;
		}
		if(_rect.sizeDelta.y < _canvasRect.sizeDelta.y)
		{
			anchPos.y = 0;
		}
		_rect.anchoredPosition = anchPos;
	}

	public void OnEndDrag (PointerEventData eventData)
	{
		Destroy(_canvasGroup);

		if(EventEndDrag != null)
		{
			EventEndDrag();
		}
	}

	public void Update()
	{
		Vector3 anchPos = _rect.anchoredPosition;
		if(_rect.sizeDelta.x > _canvasRect.sizeDelta.x)
		{
			if(anchPos.x < (_canvasRect.sizeDelta.x - _rect.sizeDelta.x) / 2)
			{
				anchPos.x = (_canvasRect.sizeDelta.x - _rect.sizeDelta.x) / 2;
			}
			else if(anchPos.x > (_rect.sizeDelta.x - _canvasRect.sizeDelta.x) / 2)
			{
				anchPos.x = (_rect.sizeDelta.x - _canvasRect.sizeDelta.x) / 2;
			}
		}
		
		if(_rect.sizeDelta.y > _canvasRect.sizeDelta.y)
		{
			if(anchPos.y < (_canvasRect.sizeDelta.y - _rect.sizeDelta.y) / 2)
			{
				anchPos.y = (_canvasRect.sizeDelta.y - _rect.sizeDelta.y) / 2;
			}
			else if(anchPos.y > (_rect.sizeDelta.y - _canvasRect.sizeDelta.y) / 2)
			{
				anchPos.y = (_rect.sizeDelta.y - _canvasRect.sizeDelta.y) / 2;
			}
		}
		
		_rect.anchoredPosition = anchPos;
	}
}