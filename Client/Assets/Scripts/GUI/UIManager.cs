﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using DG.Tweening;
using UnityEngine.Events;

[System.Serializable]
public class ChestOpenUnityEvent : UnityEvent<List<Common.DTO.Item>>{}

[System.Serializable]
public class PowerUpUnityEvent : UnityEvent<PowerUpClient>{}

public class UIManager : MonoBehaviour 
{
    public class PopupData
    {
        public Popup.PopupNameEnum PopupName;
        public UnityAction Callback;

        public PopupData(Popup.PopupNameEnum popupName, UnityAction callback)
        {
            PopupName = popupName;
            Callback = callback;
        }
    }

    public static UIManager Instance = null;

    public Canvas Canvas;

	public GameScreen[] Screens;

    public GameObject Preloader;
    public GameObject Background;
    public MainMenuView MainMenuView;

    public RectTransform MessagesContainer;
    public MessageView MessagePrefab;

    private Dictionary<GameScreen.ScreenNameEnum, GameScreen> _screensDictionary = new Dictionary<GameScreen.ScreenNameEnum, GameScreen>();
    private Dictionary<GameScreen.ScreenNameEnum, UnityEvent> _screenCallbacksDictionary = new Dictionary<GameScreen.ScreenNameEnum, UnityEvent>();
    private GameScreen _currentScreen;
    private GameScreen _previousScreen;

    public Popup[] Popups;
    public GameObject PopupBackground;
    private Dictionary<Popup.PopupNameEnum, Popup> _popupsDictionary = new Dictionary<Popup.PopupNameEnum, Popup>();
    public List<PopupData> _popupsStack = new List<PopupData>();

    public Popup Settings;
    public ConfirmationPopup Confirmation;

    public Popup[] MessagePopups;
    private Dictionary<Popup.PopupNameEnum, Popup> _messagePopupsDictionary = new Dictionary<Popup.PopupNameEnum, Popup>();
    private Popup _currentMessage;

    public bool IsPointerOverGUI = false;

    public UnityEvent EventNewCharacter;
    public UnityEvent EventCharacterLevelChanged;
    public UnityEvent EventApChanged;
    public UnityEvent EventCurrencyChanged;
    public UnityEvent EventExperienceChanged;
    public UnityEvent EventGoldChanged;
    public UnityEvent EventRequiredExperienceChanged;
    public UnityEvent EventAbilityChanged;
    public UnityEvent EventAbilitySlotChanged;
    public UnityEvent EventItemChanged;
    public UnityEvent EventEquipChanged;
    public UnityEvent EventItemLost;
    public UnityEvent EventNewQuest;
    public UnityEvent EventNewItems;
    public UnityEvent EventQuestChanged;
    public UnityEvent EventQuestComplete;
    public UnityEvent EventFixedShop;
    public PowerUpUnityEvent EventNewPowerUp;
    public PowerUpUnityEvent EventPowerUpExpired;
    public ChestOpenUnityEvent EventChestOpen;
	
	void Awake () 
	{
		Instance = this;
        DontDestroyOnLoad(transform.gameObject);

        foreach (GameScreen screen in Screens)
        {
            _screensDictionary[screen.ScreenName] = screen;
            _screenCallbacksDictionary[screen.ScreenName] = new UnityEvent();
        }

        foreach (Popup popup in Popups)
        {
            _popupsDictionary[popup.PopupName] = popup;
        }

        foreach (Popup messagePopup in MessagePopups)
        {
            _messagePopupsDictionary[messagePopup.PopupName] = messagePopup;
        }

        EventQuestChanged.AddListener(MainMenuView.OnQuestChanged);
        EventNewQuest.AddListener(MainMenuView.OnNewQuest);
        EventQuestComplete.AddListener(MainMenuView.OnQuestComplete);
    }

    void Start()
    {
        ShowPreloader();
    }

    public T GetScreen<T>(GameScreen.ScreenNameEnum screenName)
    {
    	return (T) Convert.ChangeType(_screensDictionary[screenName], typeof(T));
    }

    public T GetPopup<T>(Popup.PopupNameEnum popupName)
    {
        return (T) Convert.ChangeType(_popupsDictionary[popupName], typeof(T));
    }

    public T GetMessage<T>(Popup.PopupNameEnum popupName)
    {
        return (T)Convert.ChangeType(_messagePopupsDictionary[popupName], typeof(T));
    }

    public GameScreen.ScreenNameEnum? GetCurrentScreenName()
    {
    	if(_currentScreen != null)
    	{
    		return _currentScreen.ScreenName;
    	}
    	return null;
    }

    public Popup.PopupNameEnum? GetCurrentPopupName()
    {
        if(_popupsStack.Count != 0)
        {
            return _popupsStack[0].PopupName;
        }
        return null;
    }

    public void AddScreenCallback(GameScreen.ScreenNameEnum screenName, UnityAction callback)
    {
        if(GetCurrentScreenName() == screenName)
        {
            callback.Invoke();
        }
        else
        {
            _screenCallbacksDictionary[screenName].AddListener(callback);
        }
    }

    public void ShowPreloader()
    {
        HideAll();
        Preloader.SetActive(true);
    }

    public void ShowScreen(GameScreen.ScreenNameEnum newScreenName)
    {
        Preloader.SetActive(false);

        if (GetCurrentScreenName() == newScreenName)
    	{
    		return;
    	}
    	HideCurrentScreen();
        HideCurrentPopup();
        _currentScreen = _screensDictionary[newScreenName];

        Analytics.gua.sendEventHit("Interface", "Screens", newScreenName.ToString(), 0);

        Background.SetActive(newScreenName != GameScreen.ScreenNameEnum.Battle);

        EventNewCharacter.AddListener(_currentScreen.OnNewCharacter);
        EventCharacterLevelChanged.AddListener(_currentScreen.OnCharacterLevelChanged);
        EventApChanged.AddListener(_currentScreen.OnApChanged);
        EventCurrencyChanged.AddListener(_currentScreen.OnCurrencyChanged);
        EventExperienceChanged.AddListener(_currentScreen.OnExperienceChanged);
        EventGoldChanged.AddListener(_currentScreen.OnGoldChanged);
        EventRequiredExperienceChanged.AddListener(_currentScreen.OnRequiredExperienceChanged);
        EventAbilityChanged.AddListener(_currentScreen.OnAbilityChanged);
        EventNewPowerUp.AddListener(_currentScreen.OnNewPowerUp);
        EventPowerUpExpired.AddListener(_currentScreen.OnPowerUpExpired);
        EventChestOpen.AddListener(_currentScreen.OnChestOpened);
        EventAbilitySlotChanged.AddListener(_currentScreen.OnAbilitySlotChanged);
        EventItemLost.AddListener(_currentScreen.OnItemLost);
        EventEquipChanged.AddListener(_currentScreen.OnEquipChanged);
        EventItemChanged.AddListener(_currentScreen.OnItemChanged);
        EventQuestChanged.AddListener(_currentScreen.OnQuestChanged);
        EventNewQuest.AddListener(_currentScreen.OnNewQuest);
        EventNewItems.AddListener(_currentScreen.OnNewItems);
        EventQuestComplete.AddListener(_currentScreen.OnQuestComplete);
        EventFixedShop.AddListener(_currentScreen.OnFixedShop);

        _currentScreen.gameObject.SetActive(true);
        
        _screenCallbacksDictionary[newScreenName].Invoke();
        _screenCallbacksDictionary[newScreenName].RemoveAllListeners();
    }

    public void ShowPreviosScreen()
    {
        ShowScreen(_previousScreen.ScreenName);
    }

    public void ShowPopup(Popup.PopupNameEnum newPopupName, UnityAction callback = null)
    {
        Preloader.SetActive(false);

        _popupsStack.Add(new PopupData(newPopupName, callback));

        if(_popupsStack.Count == 1)
        {
            ShowFirstPopup();
        }
    }

    private void ShowFirstPopup()
    {
        _popupsDictionary[_popupsStack[0].PopupName].gameObject.SetActive(true);
        PopupBackground.gameObject.SetActive(true);
        if (_popupsStack[0].Callback != null)
        {
            _popupsStack[0].Callback.Invoke();
        }
    }

    public void ShowSettings()
    {
        Settings.gameObject.SetActive(true);
        PopupBackground.gameObject.SetActive(true);
    }

    public void HideSettings()
    {
        Settings.gameObject.SetActive(false);
        UpdatePopupBackground();
    }

    public void ShowConfirmation(long itemId)
    {
        Confirmation.SetItemId(itemId);
        Confirmation.gameObject.SetActive(true);
        PopupBackground.gameObject.SetActive(true);
    }

    public void HideConfirmation()
    {
        Confirmation.gameObject.SetActive(false);
        UpdatePopupBackground();
    }

    private void UpdatePopupBackground()
    {
        PopupBackground.gameObject.SetActive(_popupsStack.Count != 0 || Settings.gameObject.activeInHierarchy || Confirmation.gameObject.activeInHierarchy);
    }

    public void ShowMessage(Popup.PopupNameEnum newPopupName)
    {
        HideCurrentMessage();
        _currentMessage = _messagePopupsDictionary[newPopupName];
        _currentMessage.gameObject.SetActive(true);
    }

    public void HideAll()
    {
        HideCurrentScreen();
        HideCurrentPopup();
    }

    public void HideCurrentScreen()
    {
        if(_currentScreen != null)
        {
            EventNewCharacter.RemoveListener(_currentScreen.OnNewCharacter);
            EventCharacterLevelChanged.RemoveListener(_currentScreen.OnCharacterLevelChanged);
            EventApChanged.RemoveListener(_currentScreen.OnApChanged);
            EventCurrencyChanged.RemoveListener(_currentScreen.OnCurrencyChanged);
            EventExperienceChanged.RemoveListener(_currentScreen.OnExperienceChanged);
            EventGoldChanged.RemoveListener(_currentScreen.OnGoldChanged);
            EventRequiredExperienceChanged.RemoveListener(_currentScreen.OnRequiredExperienceChanged);
            EventAbilityChanged.RemoveListener(_currentScreen.OnAbilityChanged);
            EventNewPowerUp.RemoveListener(_currentScreen.OnNewPowerUp);
            EventChestOpen.RemoveListener(_currentScreen.OnChestOpened);
            EventAbilitySlotChanged.RemoveListener(_currentScreen.OnAbilitySlotChanged);
            EventItemLost.RemoveListener(_currentScreen.OnItemLost);
            EventEquipChanged.RemoveListener(_currentScreen.OnEquipChanged);
            EventItemChanged.RemoveListener(_currentScreen.OnItemChanged);
            EventPowerUpExpired.RemoveListener(_currentScreen.OnPowerUpExpired);
            EventQuestChanged.RemoveListener(_currentScreen.OnQuestChanged);
            EventNewQuest.RemoveListener(_currentScreen.OnNewQuest);
            EventQuestComplete.RemoveListener(_currentScreen.OnQuestComplete);
            EventNewItems.RemoveListener(_currentScreen.OnNewItems);
            EventFixedShop.RemoveListener(_currentScreen.OnFixedShop);

            _currentScreen.gameObject.SetActive(false);
        }

        _previousScreen = _currentScreen;
        _currentScreen = null;
    }

    public void HideCurrentPopup()
    {
        if(_popupsStack.Count != 0)
        {
            _popupsDictionary[_popupsStack[0].PopupName].gameObject.SetActive(false);
            _popupsStack.RemoveAt(0);
            PopupBackground.gameObject.SetActive(false);

            if (_popupsStack.Count != 0)
            {
                ShowFirstPopup();
            }
        }
    }

    public void HideCurrentMessage()
    {
        if (_currentMessage != null)
        {
            _currentMessage.gameObject.SetActive(false);
        }

        _currentMessage = null;
    }

    public void ShowMessage(string message, params string[] list)
    {
        MessageView messageView = (MessageView)Instantiate(MessagePrefab, MessagesContainer, false);
        messageView.Text.text = String.Format(Localization.GetString(message), list);
        messageView.transform.DORotate(Vector3.zero, 5f).OnComplete(() => Destroy(messageView.gameObject));
    }

    public void ShowNotification(ClientProtocol.ErrorCode message, params string[] list)
    {
        UIManager.Instance.ShowMessage(Popup.PopupNameEnum.Notification);
        UIManager.Instance.GetMessage<NotificationPopup>(Popup.PopupNameEnum.Notification).ShowMessage(String.Format(Localization.GetString(message.ToString()), list));

        if(message == ClientProtocol.ErrorCode.InsufficientResources && GetCurrentScreenName() != GameScreen.ScreenNameEnum.Shop)
        {
            UIManager.Instance.GetMessage<NotificationPopup>(Popup.PopupNameEnum.Notification).SetVisibleShopButton(true);
        }
    }
}
