﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System;

public class UseHandler : MonoBehaviour
{
    public Button UseButton;

    public event Action EventUse;

    public void OnDestroy()
    {
        Destroy(UseButton.gameObject);
    }

    public void Show()
    {
        UseButton.gameObject.SetActive(true);
    }

    public void Hide()
    {
        UseButton.gameObject.SetActive(false);
    }

    public void OnUse()
    {
        if (EventUse != null)
        {
            EventUse();
        }
    }
}
