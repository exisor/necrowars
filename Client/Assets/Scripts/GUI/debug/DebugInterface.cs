﻿using System;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

namespace Assets.Scripts.GUI.debug
{
    class DebugInterface : MonoBehaviour
    {
        private GameObject _content;

        [SerializeField] private CommandExecutor CommandExecutorPrefab;
        [SerializeField] private Button DebugButtonPrefab;
        [SerializeField] private InputField DebugInputFieldPrefab;

        [SerializeField] private string[] NetworkMessages;
        [SerializeField] private Transform CommandPanel;

        private CommandExecutor _currentCommand;
        void Awake()
        {
            DontDestroyOnLoad(gameObject);
            _content = transform.GetChild(0).gameObject;
            try
            {
                foreach (var networkMessage in NetworkMessages)
                {
                    var type = Type.GetType("ClientProtocol." + networkMessage + ", ClientProtocol");
                    if (type != null)
                    {
                        InstantiateCommand(type);
                    }
                    else
                    {
                        Debug.LogWarningFormat("Message {0} not found", networkMessage);
                    }
                }
            }
            catch (Exception e)
            {
                Debug.LogException(e);
            }
        }

        private void InstantiateCommand(Type networkMessageType)
        {
            var command = ((GameObject)Instantiate(CommandExecutorPrefab.gameObject, CommandPanel, false)).GetComponent<CommandExecutor>();

            command.MessageType = networkMessageType;
            // Create button
            var button = ((GameObject)Instantiate(DebugButtonPrefab.gameObject, command.transform, false)).GetComponent<Button>();
            var text = button.GetComponentInChildren<Text>();
            text.text = networkMessageType.Name;
            button.onClick.AddListener(command.Trigger);

            // instantiate input fields
            command.Args = networkMessageType.GetFields().Select(f =>
            {
                var inputField =
                    ((GameObject) Instantiate(DebugInputFieldPrefab.gameObject, command.transform, false))
                        .GetComponent<InputField>();
                ((Text) inputField.placeholder).text = f.Name;
                var argument = new CommandExecutor.CommandArgument {Input = inputField, Name = f.Name};
                return argument;
            }).ToArray();

            command.Selected += cmd => _currentCommand = cmd;
        }

        public void ToggleVisibility()
        {
            _content.SetActive(!_content.activeSelf);
        }

        public void ExecuteSelectedCommand()
        {
            if (_currentCommand != null)
            {
                _currentCommand.Trigger();
            }
        }
    }
}
