﻿using System;
using UnityEngine;
using UnityEngine.UI;

namespace Assets.Scripts.GUI.debug
{
    public class CommandExecutor : MonoBehaviour
    {
        public Type MessageType;
        [Serializable]
        public class CommandArgument
        {
            public string Name;
            public InputField Input;
        }

        public CommandArgument[] Args;

        void Start()
        {
            foreach (var commandArgument in Args)
            {
                commandArgument.Input.onValueChanged.AddListener(_ =>
                {
                    if (Selected != null) Selected(this);
                });
            }
        }
        public void Trigger()
        {
            object message;
            try
            {
                message = Activator.CreateInstance(MessageType);
            }
            catch (Exception e)
            {
                Debug.LogException(e);
                Debug.LogError("Cannot create object with such MessageName");
                return;
            }
            var type = message.GetType();
            {
                foreach (var commandArguments in Args)
                {
                    try
                    {
                        var field = type.GetField(commandArguments.Name);
                        object value;
                        if (field.FieldType.IsEnum)
                        {
                            value = Enum.Parse(field.FieldType, commandArguments.Input.text);
                        }
                        else
                        {
                            value = Convert.ChangeType(commandArguments.Input.text, field.FieldType);
                        }
                        field.SetValue(message, value);
                    }
                    catch (InvalidCastException e)
                    {
                        Debug.LogException(e);
                        Debug.LogErrorFormat("Cant cast value in {0} to type", commandArguments.Name);
                    }
                    catch (Exception e)
                    {
                        Debug.LogException(e);
                        Debug.LogErrorFormat("No such field with name {0}", commandArguments.Name);
                    }
                }
            }
            SocketManager.Instance.Send(message);
        }

        public event Action<CommandExecutor> Selected;
    }
}
