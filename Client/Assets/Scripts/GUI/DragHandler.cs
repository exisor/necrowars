﻿using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;
using System;

public class DragHandler : MonoBehaviour, IBeginDragHandler, IDragHandler, IEndDragHandler 
{
	public event Action EventBeginDrag;
	public event Action EventEndDrag;

	public Canvas ItemCanvas;

	private CanvasGroup _canvasGroup;
	private Vector3 _startPosition;
	private Transform _startParent;

	public void OnBeginDrag (PointerEventData eventData)
	{
		if(EventBeginDrag != null)
		{
			EventBeginDrag();
		}

		_canvasGroup = gameObject.AddComponent<CanvasGroup>();
		_canvasGroup.blocksRaycasts = false;

		_startPosition = transform.position;
		_startParent = transform.parent;

		if(ItemCanvas != null)
		{
			ItemCanvas.overrideSorting = true;
		}
	}

	public void OnDrag (PointerEventData eventData)
	{
		transform.position = eventData.position;
	}

	public void OnEndDrag (PointerEventData eventData)
	{
		if(transform.parent == _startParent)
		{
			transform.position = _startPosition;
		}

		Destroy(_canvasGroup);
		if(ItemCanvas != null)
		{
			ItemCanvas.overrideSorting = false;
		}

		if(EventEndDrag != null)
		{
			EventEndDrag();
		}
	}
}