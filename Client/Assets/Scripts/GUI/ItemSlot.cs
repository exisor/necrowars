﻿using UnityEngine;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.EventSystems;
using System;

public class ItemSlot : MonoBehaviour, IDropHandler 
{
	public delegate void SlotChangedAction(ItemSlot itemSlot, ItemView newItem, ItemView oldItem);

	public Common.Resources.ItemType SlotType;

	public bool isAnyType = true;
    public bool isEnchant = false;

    public AudioClip Sound;
    public ViewHint.ViewHintState HintState = ViewHint.ViewHintState.Default;

    public ItemView ItemView;
	public event Action EventOnDrop;
	public event SlotChangedAction EventChangeItem;
	
	public void SetItem(ItemView itemView)
	{
        ItemView oldItem = ItemView;

        ItemView = itemView;

		if(ItemView != null)
		{
			ItemView.transform.SetParent(transform, false);
            ItemView.SetFullStretch();
			ItemView.transform.localPosition = Vector3.zero;

			ItemView.SetSlot(this);
		}

		if(EventChangeItem != null)
		{
			EventChangeItem(this, ItemView, oldItem);
		}
	}

	public void OnDrop (PointerEventData eventData)
	{
		ItemView dropped = eventData.pointerDrag.GetComponent<ItemView>();
        if(dropped == null)
        {
            return;
        }
		if((isAnyType || SlotType == dropped.ItemClient.ItemData.ItemSlot) &&
			(dropped.Slot.isAnyType || ItemView == null || dropped.Slot.SlotType == ItemView.ItemClient.ItemData.ItemSlot))
		{
			dropped.Slot.SetItem(ItemView);
			SetItem(dropped);

            if(Sound != null)
            {
                SoundManager.PlaySoundUI(Sound);
            }

			if(EventOnDrop != null)
			{
				EventOnDrop();
			}
		}
		
	}
}
