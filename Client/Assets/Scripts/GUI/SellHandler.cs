﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System;

public class SellHandler : MonoBehaviour 
{
	public Button SellButton;
    public Text Price;

    public event Action EventSell;

	public void OnDestroy()
	{
		Destroy(SellButton.gameObject);
	}

	public void Show()
	{
        Price.gameObject.SetActive(true);
        SellButton.gameObject.SetActive(true);
	}

	public void Hide()
	{
        Price.gameObject.SetActive(false);
        SellButton.gameObject.SetActive(false);
	}
	
	public void OnSell()
	{
		if(EventSell != null)
		{
			EventSell();
		}
	}
}
