﻿using System.Collections.Generic;
using UnityEngine;

namespace Assets.Scripts
{
    public static class DictionaryUtils
    {
        public static TVal Get<TKey, TVal>(this Dictionary<TKey, TVal> dict, TKey key)
        {
            try
            {
                return dict[key];
            }
            catch (KeyNotFoundException)
            {
                Debug.LogErrorFormat("Not found item with id : {0}", key);
                //throw;
                return default(TVal);
            }
        }
    }
}
