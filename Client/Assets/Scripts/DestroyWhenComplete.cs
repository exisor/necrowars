﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(Animator))]
public class DestroyWhenComplete : MonoBehaviour
{
    private Animator _animator;

    void Start()
    {
        _animator = GetComponent<Animator>();
    }
    void Update()
    {
        if (_animator.GetCurrentAnimatorStateInfo(0).normalizedTime >= 1.0f)
        {
            Destroy(gameObject);
        }
    }
}
