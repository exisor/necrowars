﻿using UnityEngine;
using System.Collections;

public class TileRandomizer : MonoBehaviour {

	// Use this for initialization
	void Start () {
		SpriteRenderer[] sprites = GetComponentsInChildren<SpriteRenderer>();

		foreach (SpriteRenderer sprite in sprites)
        {
            sprite.flipX = Random.value > .5;
        }
	}
}
