using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace Assets.Scripts.Inputs
{
    public class Command : MonoBehaviour, ICommand
    {
        public bool Registered { get; set; }
        [SerializeField] private ContextType[] _contexts;
        [SerializeField] private KeyCode _keyCode;
        public IEnumerable<ContextType> Contexts
        {
            get { return _contexts.Length > 0 ? _contexts : (_contexts = new ContextType[] {Context.Current.Type}); }
        }

        public KeyCode Key
        {
            get { return _keyCode; }
        }

        public void KeyDown()
        {
            Down.Invoke();
        }

        public void KeyUp()
        {
            Up.Invoke();
        }
        
        void Awake()
        {
            Register();
        }

        void OnEnable()
        {
            Register();
        }

        void OnDestroy()
        {
            Unregister();
        }

        void OnDisable()
        {
            Unregister();
        }

        [SerializeField]
        private UnityEvent Down;
        [SerializeField]
        private UnityEvent Up;


        private void Register()
        {
            if (!Registered)
                InputManager.RegisterCommand(this);
        }

        private void Unregister()
        {
            if (Registered)
                InputManager.UnregisterCommand(this);
        }
    }
}