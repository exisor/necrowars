﻿using System.Collections.Generic;
using UnityEngine;

namespace Assets.Scripts.Inputs
{
    public interface ICommand
    {
        bool Registered { get; set; }
        IEnumerable<ContextType> Contexts { get; }
        KeyCode Key { get; }
        void KeyDown();
        void KeyUp();
    }
}
