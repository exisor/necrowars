﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Assets.Scripts.Inputs
{
    public class InputManager : MonoBehaviour
    {
        public static void RegisterCommand(ICommand command)
        {
            command.Registered = true;
            foreach (var contextType in command.Contexts)
            {
                _instance._commands[contextType][command.Key] = command;

                // adjust operative dictionary
                if (contextType == Context.Current)
                {
                    _instance._operativeDictionary[command.Key] = command;
                    _instance.UpdateOperativeList();
                }
                if (contextType == ContextType.Default)
                {
                    // Current context command has higher priority so if they exists we do nothing
                    if (_instance._commands[Context.Current][command.Key] != null)
                    {
                        _instance._operativeDictionary[command.Key] = command;
                        _instance.UpdateOperativeList();
                    }
                }
            }
        }

        public static void UnregisterCommand(ICommand command)
        {
            command.Registered = false;
            foreach (var contextType in command.Contexts)
            {
                _instance._commands[contextType].RemoveCommand(command);
                // adjust operative dictionary
                if (contextType == Context.Current || contextType == ContextType.Default)
                {
                    // if this command is currently operative
                    if (_instance._operativeDictionary[command.Key] == command)
                    {
                        // try find new command
                        var newCommand = _instance.FindCommandForKey(command.Key);
                        if (newCommand == null)
                        {
                            _instance._operativeDictionary.Remove(command.Key);
                            _instance.UpdateOperativeList();
                        }
                        else
                        {
                            _instance._operativeDictionary[newCommand.Key] = newCommand;
                            _instance.UpdateOperativeList();
                        }
                    }
                }
            }
        }

        private static InputManager _instance;

        private class CommandDictionary : Dictionary<KeyCode, ICommand>
        {
            private readonly Dictionary<KeyCode, List<ICommand>> _stackedCommands = new Dictionary<KeyCode, List<ICommand>>();

            public new ICommand this[KeyCode key]
            {
                get
                {
                    ICommand retval;
                    TryGetValue(key, out retval);
                    return retval;
                }
                set
                {
                    if (value == null) throw new ArgumentNullException("value");
                    // if command already bound put it to stack
                    ICommand boundCommand;
                    if (TryGetValue(key, out boundCommand))
                    {
                        List<ICommand> stack;
                        if (!_stackedCommands.TryGetValue(key, out stack))
                        {
                            stack = new List<ICommand>();
                            _stackedCommands[key] = stack;
                        }
                        stack.Add(boundCommand);
                    }
                    base[key] = value;
                }
            }

            public void RemoveCommand(ICommand command)
            {
                var key = command.Key;
                // if no command in currents with this key then they are just not exists
                ICommand activeCommand;
                if (!TryGetValue(key, out activeCommand))
                {
                    return;
                }
                // if not active then it should be in stack, remove it from there
                if (activeCommand != command)
                {
                    RemoveFromStack(command);
                    return;
                }

                // if active then pop last command from stack
                var poppedCommand = Pop(key);
                // if there's no stack just remove the key
                if (poppedCommand == null)
                {
                    Remove(key);
                    return;
                }
                base[key] = poppedCommand;
            }

            private ICommand Pop(KeyCode key)
            {
                List<ICommand> stack;
                if (!_stackedCommands.TryGetValue(key, out stack))
                {
                    return null;
                }
                var retval = stack.Last();
                stack.Remove(retval);
                if (stack.Count == 0)
                {
                    _stackedCommands.Remove(key);
                }
                return retval;
            }

            private void RemoveFromStack(ICommand command)
            {
                List<ICommand> stack;
                if (!_stackedCommands.TryGetValue(command.Key, out stack))
                {
                    return;
                }
                stack.Remove(command);
                if (stack.Count == 0)
                {
                    _stackedCommands.Remove(command.Key);
                }
            }
        }

        private readonly Dictionary<ContextType, CommandDictionary>
            _commands = new Dictionary<ContextType, CommandDictionary>();
        private readonly Dictionary<KeyCode, ICommand> _operativeDictionary = new Dictionary<KeyCode, ICommand>();
        private List<ICommand> _operativeList = new List<ICommand>();
        void Awake()
        {
            _instance = this;
            DontDestroyOnLoad(gameObject);
            foreach (ContextType context in Enum.GetValues(typeof(ContextType)))
            {
                _commands[context] = new CommandDictionary();
            }
            Context.Switched += ContextOnSwitched;
        }

        private void ContextOnSwitched()
        {
            RebuildDictionary();
        }

        private void RebuildDictionary()
        {
            _operativeDictionary.Clear();
            var currentContext = _commands[Context.Current];
            var defaultContext = _commands[ContextType.Default];

            var handlers = currentContext.Concat(defaultContext.Where(c => currentContext[c.Key] == null));

            foreach (var keyValuePair in handlers)
            {
                _operativeDictionary[keyValuePair.Key] = keyValuePair.Value;
            }
            UpdateOperativeList();
        }

        void Update()
        {
            if (Context.Current == null) return;
            if (Input.anyKeyDown)
            {
                foreach (var command in _operativeList)
                {
                    if (Input.GetKeyDown(command.Key))
                    {
                        command.KeyDown();
                    }
                    if (Input.GetKeyUp(command.Key))
                    {
                        command.KeyUp();
                    }
                }
            }
        }

        private ICommand FindCommandForKey(KeyCode code)
        {
            var retval = _commands[Context.Current][code];
            if (retval == null)
            {
                retval = _commands[ContextType.Default][code];
            }
            return retval;
        }

        private void UpdateOperativeList()
        {
            _operativeList = _operativeDictionary.Values.ToList();
        }

    }
}
