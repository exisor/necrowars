﻿using UnityEngine;
using System.Collections;

public class PerformanceTest : MonoBehaviour
{
    public GameObject[] Prefabs;

    public int Count = 100;

    private int _created = 0;
	
	void Start ()
    {
	    
	}
	
	
	void Update ()
    {
	    if(_created < Count)
        {
            var l = Count - _created;
            for(int i = 0; i < l; i++)
            {
                //Instantiate(Prefabs[Random.Range(0, Prefabs.Length)]);
                Instantiate(Prefabs[Random.Range(0, Prefabs.Length)], new Vector3(Random.Range(-5f, 5f), Random.Range(-4f, 4f), 0), Quaternion.identity);
                _created++;
            }
        }
	}
}
