﻿using UnityEngine;
using UnityEngine.Events;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.Networking;
using System;
using System.Reflection;
using System.Linq;
using System.IO;

public class GameNetworkManager : NetworkManager
{
	public static GameNetworkManager Instance = null; 
	
	[System.Serializable]
	public struct NetworkPrefab
	{
		public string Id;
		public GameObject Prefab;
		
		public NetworkHash128 NetworkHashId;
		
		public void ParseId()
		{
			NetworkHashId = NetworkHash128.Parse(Id);
		}
	}

	public string BattleToken;

    public NetworkPrefab[] NetworkPrefabs;
    private Dictionary<NetworkHash128, NetworkPrefab> _prefabDictionary = new Dictionary<NetworkHash128, NetworkPrefab>();

    public SocketManager Socket;

    void Awake () 
	{
		Instance = this;

        connectionConfig.MaxCombinedReliableMessageCount = 512;

        BattleToken = DataManager.Instance.BattleToken;
        networkAddress = DataManager.Instance.networkAddress;
        networkPort = DataManager.Instance.networkPort;

        #if !UNITY_EDITOR && UNITY_WEBGL
			useWebSockets = true;
#endif
    }

    void Start()
    {
        foreach (NetworkPrefab networkPrefab in NetworkPrefabs)
        {
            networkPrefab.ParseId();
            _prefabDictionary[networkPrefab.NetworkHashId] = networkPrefab;
            ClientScene.RegisterSpawnHandler(networkPrefab.NetworkHashId, Spawn, UnSpawn);
        }
    }

    public GameObject Spawn(Vector3 position, NetworkHash128 assetId)
    {
    	return (GameObject)Instantiate(_prefabDictionary[assetId].Prefab, position, Quaternion.identity);
    }

    public void UnSpawn(GameObject spawned)
    {
        Destroy(spawned);
    }

    public override void OnClientConnect(NetworkConnection conn)
	{
		base.OnClientConnect(conn);

        conn.logNetworkMessages = false;

        AuthorizePlayer request = new AuthorizePlayer();
		request.Message.CharacterId = DataManager.Instance.GetCurrentCharacterId();
		request.Message.Token = BattleToken;

		conn.RegisterHandler((short)ClientProtocol.CustomGameMessageType.PlayerAuthorized, OnPlayerAuthorized);

		conn.Send((short)request.MessageType, request);

        Debug.Log("OnClientConnect");
    }

	public void OnPlayerAuthorized(NetworkMessage netMsg)
	{
        Debug.Log("OnPlayerAuthorized");
		ClientScene.AddPlayer(0);
	}

	public override void OnStartClient(NetworkClient client)
	{
		base.OnStartClient(client);
	}

    public override void OnClientDisconnect(NetworkConnection conn)
    {
        base.OnClientDisconnect(conn);
        Debug.Log("Disconnect");

        UIManager.Instance.Preloader.GetComponent<PreloaderView>().ShowExitButton();
    }

    public override void OnClientError(NetworkConnection conn, int errorCode)
    {
        base.OnClientError(conn, errorCode);
        Debug.LogFormat("Error {0}", errorCode);
    }

    public override void OnDropConnection(bool success, string extendedInfo)
    {
        base.OnDropConnection(success, extendedInfo);
        Debug.LogFormat("Drop {0} {1}", success, extendedInfo);
    }
}
