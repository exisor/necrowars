﻿using UnityEngine;
using UnityEngine.Events;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.Networking;
using System;
using System.Reflection;
using System.Linq;
using System.IO;
using ClientProtocol;

public class SocketManager: MonoBehaviour
{
    public delegate void EventResponse(byte[] data);
    public static SocketManager Instance = null;

    private WebSocket _webSocket;

    public bool IsTest;
    public string MasterServer = "188.227.67.172:2020";
    public string LoginAddress;
    public string LobbyAddress;
    public long UserId;
    public string UserLogin;
    public string UserName;
    public string UserPassword;
    public string SessionKey;

    private Dictionary<string, EventResponse> _handlers = new Dictionary<string, EventResponse>();
    private string _socketAdress = "";
    private event Action EventSocketConnected;

    void Awake () 
    {
        Instance = this;

        /*var collectionTypes = Assembly.GetExecutingAssembly()
                .GetTypes()
                .Where(
                    type =>
                        !type.IsAbstract && !type.IsInterface &&
                        typeof(INetHandler).IsAssignableFrom(type)).ToList();
        var collection = collectionTypes.Select(c => Activator.CreateInstance(c) as INetHandler);
        */
        //_handlers = collection.ToDictionary(h => h.Key, h => h);

        var requestServerResponse = new RequestServerResponse();
        var UserLoginResponse = new UserLoginResponse();
        var UserAuthorizeResponse = new UserAuthorizeResponse();
        var PlayerDataResponse = new PlayerDataResponse();
        var CreatePlayerResponse = new CreatePlayerResponse();
        var FixedShopResponse = new FixedShopResponse();
        var BuyItemFromShopResponse = new BuyItemFromShopResponse();
        var KongregateItemListResponse = new KongregateItemListResponse();
        var KongregateUserItemsResponse = new KongregateUserItemsResponse();
        var NewCharacterEvent = new NewCharacterEvent();
        var AbilityChangedEvent = new AbilityChangedEvent();
        var LobbyLoggedInEvent = new LobbyLoggedInEvent();
        var CharacterLevelChangedEvent = new CharacterLevelChangedEvent();
        var ApChangedEvent = new ApChangedEvent();
        var CurrencyChangedEvent = new CurrencyChangedEvent();
        var CurrencyBoughtChangedEvent = new CurrencyBoughtChangedEvent();
        var ExperienceChangedEvent = new ExperienceChangedEvent();
        var GoldChangedEvent = new GoldChangedEvent();
        var RequiredExperienceChangedEvent = new RequiredExperienceChangedEvent();
        var ToBattleEvent = new ToBattleEvent();
        var ChestOpenEvent = new ChestOpenEvent();
        var NewItemsEvent = new NewItemsEvent();
        var ItemEquippedEvent = new ItemEquippedEvent();
        var ItemUnequippedEvent = new ItemUnequippedEvent();
        var ItemLostEvent = new ItemLostEvent();
        var NewPowerUpEvent = new NewPowerUpEvent();
        var Error = new Error();
        var ItemChangedEvent = new ItemChangedEvent();
        var AbilityLostEvent = new AbilityLostEvent();
        var NewAbilityEvent = new NewAbilityEvent();
        var AbilitySlotChangedEvent = new AbilitySlotChangedEvent();
        var LeaderboardResponse = new LeaderboardResponse();
        var CharacterProfileResponse = new CharacterProfileResponse();
        var TutorialPhaseChangedEvent = new TutorialPhaseChangedEvent();
        var PowerUpExpiredEvent = new PowerUpExpiredEvent();
        var InventoryCapacityChangedEvent = new InventoryCapacityChangedEvent();
        var DailyRewardsEvent = new DailyRewardsEvent();
        var AchievementProgressEvent = new AchievementProgressEvent();
        var AchievementRewardEvent = new AchievementRewardEvent();
        var QuestProgressEvent = new QuestProgressEvent();
        var QuestCompleteEvent = new QuestCompleteEvent();
        var NewQuestEvent = new NewQuestEvent();
        var BattleEndedEvent = new BattleEndedEvent();
        var KongregateUserAvatarResponse = new KongregateUserAvatarResponse();
        var EnchantItemResponse = new EnchantItemResponse();
        var SkillsResetEvent = new SkillsResetEvent();
        var NewPlayerEventEvent = new NewPlayerEventEvent();

        var Item = new Common.DTO.Item();
        var Ability = new Common.DTO.Ability();
        var AbilitySlot = new Common.DTO.AbilitySlot();
        var Character = new Common.DTO.Character();
        var FixedShop = new Common.DTO.FixedShop();
        var ItemAffix = new Common.DTO.ItemAffix();
        var Player = new Common.DTO.Player();
        var PowerUp = new Common.DTO.PowerUp();
        var ShopItem = new Common.DTO.ShopItem();
        var LeaderboardRecord = new Common.DTO.LeaderboardRecord();
        var Achievement = new Common.DTO.Achievement();
        var KongregateItem = new Common.DTO.KongregateItem();
        var EnchantResult = new Common.DTO.EnchantResult();

        _handlers["RequestServerResponse"] = NetHandler.Instance.RequestServerResponse;
        _handlers["UserLoginResponse"] = NetHandler.Instance.UserLoginResponse;
        _handlers["UserAuthorizeResponse"] = NetHandler.Instance.UserAuthorizeResponse;
        _handlers["PlayerDataResponse"] = NetHandler.Instance.PlayerDataResponse;
        _handlers["CreatePlayerResponse"] = NetHandler.Instance.CreatePlayerResponse;
        _handlers["FixedShopResponse"] = NetHandler.Instance.FixedShopResponse;
        _handlers["BuyItemFromShopResponse"] = NetHandler.Instance.BuyItemFromShopResponse;
        _handlers["KongregateItemListResponse"] = NetHandler.Instance.KongregateItemListResponse;
        _handlers["KongregateUserItemsResponse"] = NetHandler.Instance.KongregateUserItemsResponse;
        _handlers["NewCharacterEvent"] = NetHandler.Instance.NewCharacterEvent;
        _handlers["AbilityChangedEvent"] = NetHandler.Instance.AbilityChangedEvent;
        _handlers["LobbyLoggedInEvent"] = NetHandler.Instance.LobbyLoggedInEvent;
        _handlers["CharacterLevelChangedEvent"] = NetHandler.Instance.CharacterLevelChangedEvent;
        _handlers["ApChangedEvent"] = NetHandler.Instance.ApChangedEvent;
        _handlers["CurrencyChangedEvent"] = NetHandler.Instance.CurrencyChangedEvent;
        _handlers["CurrencyBoughtChangedEvent"] = NetHandler.Instance.CurrencyBoughtChangedEvent;
        _handlers["ExperienceChangedEvent"] = NetHandler.Instance.ExperienceChangedEvent;
        _handlers["GoldChangedEvent"] = NetHandler.Instance.GoldChangedEvent;
        _handlers["RequiredExperienceChangedEvent"] = NetHandler.Instance.RequiredExperienceChangedEvent;
        _handlers["ToBattleEvent"] = NetHandler.Instance.ToBattleEvent;
        _handlers["ChestOpenEvent"] = NetHandler.Instance.ChestOpenEvent;
        _handlers["NewItemsEvent"] = NetHandler.Instance.NewItemsEvent;
        _handlers["ItemEquippedEvent"] = NetHandler.Instance.ItemEquippedEvent;
        _handlers["ItemUnequippedEvent"] = NetHandler.Instance.ItemUnequippedEvent;
        _handlers["ItemLostEvent"] = NetHandler.Instance.ItemLostEvent;
        _handlers["NewPowerUpEvent"] = NetHandler.Instance.NewPowerUpEvent;
        _handlers["Error"] = NetHandler.Instance.Error;
        _handlers["ItemChangedEvent"] = NetHandler.Instance.ItemChangedEvent;
        _handlers["NewAbilityEvent"] = NetHandler.Instance.NewAbilityEvent;
        _handlers["AbilityLostEvent"] = NetHandler.Instance.AbilityLostEvent;
        _handlers["AbilitySlotChangedEvent"] = NetHandler.Instance.AbilitySlotChangedEvent;
        _handlers["LeaderboardResponse"] = NetHandler.Instance.LeaderboardResponse;
        _handlers["CharacterProfileResponse"] = NetHandler.Instance.CharacterProfileResponse;
        _handlers["TutorialPhaseChangedEvent"] = NetHandler.Instance.TutorialPhaseChangedEvent;
        _handlers["PowerUpExpiredEvent"] = NetHandler.Instance.PowerUpExpiredEvent;
        _handlers["InventoryCapacityChangedEvent"] = NetHandler.Instance.InventoryCapacityChangedEvent;
        _handlers["DailyRewardsEvent"] = NetHandler.Instance.DailyRewardsEvent;
        _handlers["AchievementProgressEvent"] = NetHandler.Instance.AchievementProgressEvent;
        _handlers["AchievementRewardEvent"] = NetHandler.Instance.AchievementRewardEvent;
        _handlers["NewQuestEvent"] = NetHandler.Instance.NewQuestEvent;
        _handlers["QuestCompleteEvent"] = NetHandler.Instance.QuestCompleteEvent;
        _handlers["QuestProgressEvent"] = NetHandler.Instance.QuestProgressEvent;
        _handlers["BattleEndedEvent"] = NetHandler.Instance.BattleEndedEvent;
        _handlers["KongregateUserAvatarResponse"] = NetHandler.Instance.KongregateUserAvatarResponse;
        _handlers["EnchantItemResponse"] = NetHandler.Instance.EnchantItemResponse;
        _handlers["SkillsResetEvent"] = NetHandler.Instance.SkillsResetEvent;
        _handlers["NewPlayerEventEvent"] = NetHandler.Instance.NewPlayerEventEvent;
    }

    public void Connect()
    {
        Open(MasterServer);
        Send<ClientProtocol.RequestServer>(new ClientProtocol.RequestServer { GameName = IsTest ? "Necrowars-test" : "Necrowars", ServerRole = Common.ServerRole.Login });
    }

    public void Open(string address)
    {
        StopCoroutine("OpenCoroutine");
        StartCoroutine(OpenCoroutine(address));
    }

    public void Close()
    {
        EventSocketConnected = null;
        _socketAdress = "";
        if (_webSocket != null)
        {
            _webSocket.Close();
            _webSocket = null;
        }
    }

    public void ShowError(ClientProtocol.ErrorCode message)
    {
        UIManager.Instance.HideAll();
        UIManager.Instance.ShowMessage(Popup.PopupNameEnum.Error);
        UIManager.Instance.GetMessage<ErrorPopup>(Popup.PopupNameEnum.Error).ShowMessage(Localization.GetString(message.ToString()));
    }

    IEnumerator OpenCoroutine(string address)
    {
        Close();

        _webSocket = new WebSocket(new Uri("wss://" + address));
        yield return StartCoroutine(_webSocket.Connect());

        _socketAdress = address;
        if (EventSocketConnected != null)
        {
            EventSocketConnected();
        }

        while (address == _socketAdress)
        {
            string reply = _webSocket.RecvString();
            if (reply != null)
            {
                Common.MessageInfo info = new Common.MessageInfo(reply);
                if (_handlers.ContainsKey(info.Key))
                {
                    _handlers[info.Key](info.Data);
                }
            }
            /*
            else if(_webSocket.error != null)
            {
                ShowError("Server is out");
                break;
            }*/
            yield return 0;
        }
    }

    public void Send<T>(T data)
    {
        string message = Common.ProtobufSerializer.MakeStringMessage(data);
        
        if(_socketAdress != "")
        {
            _webSocket.SendString(message);
        }
        else
        {
            EventSocketConnected += () => _webSocket.SendString(message);
        }
    }

    public void Login()
    {
        Open(LoginAddress);
        Send<ClientProtocol.UserLogin>(new ClientProtocol.UserLogin { Username = UserLogin });
    }

    public void KongregateAuthorizeRequest()
    {
        Open(LoginAddress);
        Send<ClientProtocol.KongregateAuthorizeRequest>(new ClientProtocol.KongregateAuthorizeRequest { UserId = System.Convert.ToInt64(UserLogin), Token = UserPassword });
    }

    public void OGZAuthorizeRequest()
    {
        Open(LoginAddress);
        Debug.Log("OGZAuthorizeRequest");
        Send<ClientProtocol.OGZLoginRequest>(new ClientProtocol.OGZLoginRequest { Id = System.Convert.ToInt64(UserLogin), SessionKey = UserPassword });
    }

    public void KongregateAvatarRequest()
    {
        Send<ClientProtocol.KongregateUserAvatarRequest>(new ClientProtocol.KongregateUserAvatarRequest { KongUserId = int.Parse(UserLogin) });
    }

    public void CheckPassword(string password)
    {
        Send<ClientProtocol.UserAuthorize>(new ClientProtocol.UserAuthorize { Username = UserLogin, EncryptedPassword = password });
    }

    public void GetLobbyServer(long userId, string sessionKey)
    {
        UserId = userId;
        SessionKey = sessionKey;
        Open(MasterServer);
        Send<ClientProtocol.RequestServer>(new ClientProtocol.RequestServer { GameName = IsTest ? "Necrowars-test" : "Necrowars", ServerRole = Common.ServerRole.Lobby });
    }

    public void LobbyLogin()
    {
        Open(LobbyAddress);
        Common.ClientTarget target = Common.ClientTarget.Unity;
#if !UNITY_EDITOR && UNITY_WEBGL
        target = Common.ClientTarget.WebGL;
#endif
        Send<ClientProtocol.LobbyLogin>(new ClientProtocol.LobbyLogin {UserId = UserId, SessionKey = SessionKey, Target = target });
    }

    public void GetPlayerData()
    {
        Send<ClientProtocol.PlayerDataRequest>(new ClientProtocol.PlayerDataRequest { } );
    }

    public void CreatePlayerData()
    {
        Send<ClientProtocol.CreatePlayerRequest>(new ClientProtocol.CreatePlayerRequest {Name = UserName });
    }

    public void CreateCharacterData()
    {
        Send<ClientProtocol.CreateCharacterRequest>(new ClientProtocol.CreateCharacterRequest { Id = 1000 });
    }

    public void CompleteTutorialPhase(int phase)
    {
        Send<ClientProtocol.TutorialPhaseCompletedRequest>(new ClientProtocol.TutorialPhaseCompletedRequest { PhaseNum = phase });
    }

    public void SelectAbility(long characterId, long abilityId, int slotId)
    {
        Debug.Log("SelectAbility");
        Common.DTO.AbilitySlot slot = new Common.DTO.AbilitySlot();
        slot.Slot = slotId;
        slot.AbilityId = abilityId;
        Send<ClientProtocol.SelectAbility>(new ClientProtocol.SelectAbility { Slot = slot, CharacterId = characterId});
    }

    public void ChangeAbility(long characterId, long abilityId, int levelDifference, bool spendCurrency)
    {
        Send<ClientProtocol.ChangeAbilityLevel>(new ClientProtocol.ChangeAbilityLevel { CharacterId = characterId, AbilityId = abilityId,
            LevelDifference = levelDifference, SpendCurrency = spendCurrency});
    }

    public void InBattle(long characterId)
    {
        Debug.Log("InBattle");
        Send<ClientProtocol.RequestBattle>(new ClientProtocol.RequestBattle { CharacterId = characterId});
    }

    public void GetShopItems()
    {
        Send<ClientProtocol.FixedShopRequest>(new ClientProtocol.FixedShopRequest {});
    }

    public void GetKongShopItems()
    {
        Debug.Log("GetKongShopItems");
        Send<ClientProtocol.KongregateItemListRequest>(new ClientProtocol.KongregateItemListRequest { });
    }

    public void UseKongItem(int itemId)
    {
        Send<ClientProtocol.ActivateKongregateItemRequest>(new ClientProtocol.ActivateKongregateItemRequest {Id = itemId, GameToken = UserPassword });
    }

    public void GetKongUserItems()
    {
        Send<ClientProtocol.KongregateUserItemsRequest>(new ClientProtocol.KongregateUserItemsRequest { });
    }

    public void GetLeaderboard(long characterId)
    {
        Send<ClientProtocol.LeaderboardsRequest>(new ClientProtocol.LeaderboardsRequest { CharacterId = characterId});
    }

    public void GetProfile(long characterId)
    {
        Send<ClientProtocol.CharacterProfileRequest>(new ClientProtocol.CharacterProfileRequest { CharacterId = characterId });
    }

    public void BuyItem(long characterId, long itemId)
    {
        Send<ClientProtocol.BuyItemFromShop>(new ClientProtocol.BuyItemFromShop { Id = itemId});
    }

    public void UpgradeItem(long characterId, long itemId)
    {
        Analytics.gua.sendEventHit("Forge", "EquipIncreased", itemId.ToString(), (int)characterId);
        Send<ClientProtocol.UpgradeItemRequest>(new ClientProtocol.UpgradeItemRequest { CharacterId = characterId, ItemId = itemId });
    }

    public void EnchantItem(long itemId)
    {
        Analytics.gua.sendEventHit("Forge", "EquipForged", itemId.ToString(), (int)DataManager.Instance.GetCurrentCharacterId());
        Send<ClientProtocol.EnchantItemRequest>(new ClientProtocol.EnchantItemRequest { ItemId = itemId });
    }

    public void EnchantAffix(long itemId, long affixId)
    {
        Analytics.gua.sendEventHit("Forge", "AffexForged", itemId.ToString(), (int)affixId);
        Send<ClientProtocol.EnchantAffixRequest>(new ClientProtocol.EnchantAffixRequest { ItemId = itemId, AffixId = affixId });
    }

    public void EquipItem(long characterId, long itemId)
    {
        Send<ClientProtocol.EquipItemRequest>(new ClientProtocol.EquipItemRequest { CharacterId = characterId, ItemId = itemId});
    }

    public void UnequipItem(long itemId)
    {
        Send<ClientProtocol.UnequipItemRequest>(new ClientProtocol.UnequipItemRequest { ItemId = itemId });
    }

    public void SellItem(long itemId)
    {
        Analytics.gua.sendEventHit("Shop", "Sell", itemId.ToString(), (int)DataManager.Instance.GetCurrentCharacterId());
        Send<ClientProtocol.SellItemRequest>(new ClientProtocol.SellItemRequest { Id = itemId});
    }

    public void SetAP(long characterId, int ap)
    {
        Send<SetCharacterAPRequest>(new ClientProtocol.SetCharacterAPRequest { CharacterId = characterId, AP = ap });
    }

    public void SetLevel(long characterId, int level)
    {
        Send<ClientProtocol.SetCharacterLevel>(new ClientProtocol.SetCharacterLevel { CharacterId = characterId, Level = level});
    }

    public void SetGold(long gold)
    {
        Send<ClientProtocol.SetGold>(new ClientProtocol.SetGold { Gold = gold});
    }

    public void SetCurrency(long currency)
    {
        Send<ClientProtocol.SetCurrency>(new ClientProtocol.SetCurrency { Currency = currency});
    }

    public void UseItemRequest(int characterId, long itemId)
    {
        Send<ClientProtocol.UseItemRequest>(new ClientProtocol.UseItemRequest { CharacterId = characterId, ItemId = itemId });
    }
}
