﻿using System;
using ClientProtocol;
using Common;
using UnityEngine.Networking;

public abstract class NetworkMessageWrapper<TMessage> : MessageBase
{
    public TMessage Message { get; private set; }
    public CustomGameMessageType MessageType { get; private set; }

    public NetworkMessageWrapper(TMessage message, CustomGameMessageType messageType)
    {
        Message = message;
        MessageType = messageType;
    }

    public override void Serialize(NetworkWriter writer)
    {
        writer.StartMessage((short)MessageType);
        writer.WriteBytesFull(ProtobufSerializer.SerializeBinary(Message));
        writer.FinishMessage();
    }

    public override void Deserialize(NetworkReader reader)
    {
        var source = reader.ReadBytesAndSize();
        ProtobufSerializer.Merge(source, Message);
    }
}