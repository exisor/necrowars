using ClientProtocol;

public class AuthorizePlayer : NetworkMessageWrapper<ClientProtocol.AuthorizeOnGameServer>
{
    public AuthorizePlayer() : base(new ClientProtocol.AuthorizeOnGameServer(), CustomGameMessageType.AuthorizePlayer)
    {
    }

    public AuthorizePlayer(ClientProtocol.AuthorizeOnGameServer message)
        : base(message, CustomGameMessageType.AuthorizePlayer)
    {
            
    }
}