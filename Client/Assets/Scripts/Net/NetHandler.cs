﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Security.Cryptography;
using System.Text;
using System;
using System.Collections;
using System.Collections.Generic;

public class NetHandler : MonoBehaviour
{
    public static NetHandler Instance = null;

    void Awake()
    {
        Instance = this;
    }

    public void RequestServerResponse(byte[] data)
    {
        var message = Common.ProtobufSerializer.Deserialize<ClientProtocol.RequestServerResponse>(data);
        if (message.Role == Common.ServerRole.Login)
        {
            SocketManager.Instance.LoginAddress = message.Address + ":" + message.Port.ToString();
            if (DataManager.Instance.Social == DataManager.SocialType.None)
            {
                UIManager.Instance.ShowScreen(GameScreen.ScreenNameEnum.Login);
            }
            else if (DataManager.Instance.Social == DataManager.SocialType.Kong)
            {
                SocketManager.Instance.KongregateAuthorizeRequest();
            }
            else if (DataManager.Instance.IsOGZ)
            {
                SocketManager.Instance.OGZAuthorizeRequest();
            }

            Debug.Log(SocketManager.Instance.LoginAddress);
        }
        else
        {
            SocketManager.Instance.LobbyAddress = message.Address + ":" + message.Port.ToString();
            Debug.Log(SocketManager.Instance.LobbyAddress);
            SocketManager.Instance.LobbyLogin();
        }
    }

    public void UserLoginResponse(byte[] data)
    {
        var message = Common.ProtobufSerializer.Deserialize<ClientProtocol.UserLoginResponse>(data);
        var sha = new SHA1CryptoServiceProvider();
        var passwordBytes = sha.ComputeHash(Encoding.UTF8.GetBytes(SocketManager.Instance.UserPassword));
        var saltBytes = Convert.FromBase64String(message.Key);
        var combined = new byte[passwordBytes.Length + saltBytes.Length];
        Buffer.BlockCopy(saltBytes, 0, combined, 0, saltBytes.Length);
        Buffer.BlockCopy(passwordBytes, 0, combined, saltBytes.Length, passwordBytes.Length);

        Debug.Log(message.Key);

        SocketManager.Instance.CheckPassword(Convert.ToBase64String(sha.ComputeHash(combined)));
    }

    public void UserAuthorizeResponse(byte[] data)
    {
        var message = Common.ProtobufSerializer.Deserialize<ClientProtocol.UserAuthorizeResponse>(data);
        Debug.Log(message.SessionKey);
        SocketManager.Instance.GetLobbyServer(message.Id, message.SessionKey);
    }

    public void LobbyLoggedInEvent(byte[] data)
    {
        SocketManager.Instance.GetPlayerData();

        if (DataManager.Instance.Social == DataManager.SocialType.Kong)
        {
            SocketManager.Instance.UseKongItem(0);
            SocketManager.Instance.KongregateAvatarRequest();
        }
    }

    public void PlayerDataResponse(byte[] data)
    {
        var message = Common.ProtobufSerializer.Deserialize<ClientProtocol.PlayerDataResponse>(data);
        Common.DTO.Player player = message.Data;

        if (player == null)
        {
            SocketManager.Instance.CreatePlayerData();
        }
        else
        {
            DataManager.Instance.SetPlayer(player);

            if (player.Characters.Count == 0)
            {
                SocketManager.Instance.CreateCharacterData();
            }
            else
            {
                ApplicationManager.Instance.Init();
            }
        }

        Debug.Log(player);
    }

    public void CreatePlayerResponse(byte[] data)
    {
        var message = Common.ProtobufSerializer.Deserialize<ClientProtocol.CreatePlayerResponse>(data);
        Common.DTO.Player player = message.Player;

        DataManager.Instance.SetPlayer(player);

        if (player.Characters.Count == 0)
        {
            SocketManager.Instance.CreateCharacterData();
        }
        else
        {
            ApplicationManager.Instance.Init();
        }
    }

    public void FixedShopResponse(byte[] data)
    {
        Debug.Log("FixedShopResponse");
        var message = Common.ProtobufSerializer.Deserialize<ClientProtocol.FixedShopResponse>(data);
        DataManager.Instance.SetShopItems(message.Shop.Items);

        UIManager.Instance.EventFixedShop.Invoke();
    }

    public void BuyItemFromShopResponse(byte[] data)
    {
        Debug.Log("BuyItemFromShopResponse");
        var message = Common.ProtobufSerializer.Deserialize<ClientProtocol.BuyItemFromShopResponse>(data);
        UIManager.Instance.ShowMessage("BuyItemFromShopResponse");
        Analytics.gua.sendEventHit("Shop", "Purchased", message.Item.BaseId.ToString(), (int)DataManager.Instance.GetCurrentCharacterId());
    }

    public void KongregateItemListResponse(byte[] data)
    {
        Debug.Log("KongregateItemListResponse");
        var message = Common.ProtobufSerializer.Deserialize<ClientProtocol.KongregateItemListResponse>(data);
        DataManager.Instance.SetKongItems(message.Items);
        UIManager.Instance.GetScreen<ShopScreen>(GameScreen.ScreenNameEnum.Shop).UpdateCurrencyShop();
    }

    public void KongregateUserItemsResponse(byte[] data)
    {
        var message = Common.ProtobufSerializer.Deserialize<ClientProtocol.KongregateUserItemsResponse>(data);
    }

    public void LeaderboardResponse(byte[] data)
    {
        var message = Common.ProtobufSerializer.Deserialize<ClientProtocol.LeaderboardResponse>(data);

        Debug.Log(message.Leaderboard);

        UIManager.Instance.ShowScreen(GameScreen.ScreenNameEnum.Leaderboard);
        UIManager.Instance.GetScreen<LeaderboardScreen>(GameScreen.ScreenNameEnum.Leaderboard)
            .ShowLeaderboard(message.Leaderboard);
    }

    public void CharacterProfileResponse(byte[] data)
    {
        var message = Common.ProtobufSerializer.Deserialize<ClientProtocol.CharacterProfileResponse>(data);

        UIManager.Instance.ShowPopup(Popup.PopupNameEnum.Profile);
        UIManager.Instance.GetPopup<ProfilePopup>(Popup.PopupNameEnum.Profile)
            .ShowCharacter(message.PlayerName, message.Character, message.Items);
    }

    public void TutorialPhaseChangedEvent(byte[] data)
    {
        var message = Common.ProtobufSerializer.Deserialize<ClientProtocol.TutorialPhaseChangedEvent>(data);
    }

    public void NewCharacterEvent(byte[] data)
    {
        var message = Common.ProtobufSerializer.Deserialize<ClientProtocol.NewCharacterEvent>(data);

        Common.DTO.Character character = message.Character;
        var noCharacter = DataManager.Instance.CurrentCharacter == null;
        Debug.Log(character);

        DataManager.Instance.AddCharacter(character);

        if (noCharacter)
        {
            ApplicationManager.Instance.Init();
        }
        UIManager.Instance.EventNewCharacter.Invoke();
    }

    public void AbilityChangedEvent(byte[] data)
    {
        var message = Common.ProtobufSerializer.Deserialize<ClientProtocol.AbilityChangedEvent>(data);

        CharacterClient character = DataManager.Instance.GetCharacter(message.CharacterId);

        AbilityClient ability = character.Abilities[message.AbilityId];

        ability.Level = message.Level;

        UIManager.Instance.EventAbilityChanged.Invoke();

        Analytics.gua.sendEventHit("Progress", "AbilityChangedEvent", message.AbilityId.ToString(),
            (int) DataManager.Instance.GetCurrentCharacterId());
    }

    public void CharacterLevelChangedEvent(byte[] data)
    {
        var message = Common.ProtobufSerializer.Deserialize<ClientProtocol.CharacterLevelChangedEvent>(data);

        DataManager.Instance.GetCharacter(message.CharacterId).Level = message.Level;
        DataManager.Instance.GetCharacter(message.CharacterId).RequiredExperience = Common.Util.GetRequiredExperienceForLevel(message.Level);

        UIManager.Instance.EventCharacterLevelChanged.Invoke();

        Analytics.gua.sendEventHit("Progress", "CharacterLevelChangedEvent", message.Level.ToString(),
            (int) DataManager.Instance.GetCurrentCharacterId());
    }

    public void ApChangedEvent(byte[] data)
    {
        var message = Common.ProtobufSerializer.Deserialize<ClientProtocol.ApChangedEvent>(data);

        DataManager.Instance.GetCharacter(message.CharacterId).AP = message.NewAp;

        UIManager.Instance.EventApChanged.Invoke();
    }

    public void CurrencyChangedEvent(byte[] data)
    {
        var message = Common.ProtobufSerializer.Deserialize<ClientProtocol.CurrencyChangedEvent>(data);

        DataManager.Instance.Currency = message.NewCurrency;

        UIManager.Instance.EventCurrencyChanged.Invoke();
    }

    public void ExperienceChangedEvent(byte[] data)
    {
        var message = Common.ProtobufSerializer.Deserialize<ClientProtocol.ExperienceChangedEvent>(data);

        DataManager.Instance.GetCharacter(message.CharacterId).Experience = message.Experience;

        UIManager.Instance.EventExperienceChanged.Invoke();
    }

    public void GoldChangedEvent(byte[] data)
    {
        var message = Common.ProtobufSerializer.Deserialize<ClientProtocol.GoldChangedEvent>(data);

        DataManager.Instance.Gold = message.NewGold;

        UIManager.Instance.EventGoldChanged.Invoke();
    }

    public void CurrencyBoughtChangedEvent(byte[] data)
    {
        var message = Common.ProtobufSerializer.Deserialize<ClientProtocol.CurrencyBoughtChangedEvent>(data);

        DataManager.Instance.CurrencyBought = (int) message.NewCurrencyBought;
    }

    public void RequiredExperienceChangedEvent(byte[] data)
    {
        var message = Common.ProtobufSerializer.Deserialize<ClientProtocol.RequiredExperienceChangedEvent>(data);

        //DataManager.Instance.GetCharacter(message.CharacterId).RequiredExperience = message.RequiredExperience;

        //UIManager.Instance.EventRequiredExperienceChanged.Invoke();
    }

    public void ToBattleEvent(byte[] data)
    {
        var message = Common.ProtobufSerializer.Deserialize<ClientProtocol.ToBattleEvent>(data);

        DataManager.Instance.BattleToken = message.BattleToken;
        DataManager.Instance.networkAddress = message.Address;
        DataManager.Instance.networkPort = message.Port;

        ApplicationManager.Instance.ShowBattle(OnLoadSceneCallBack);

        Analytics.gua.sendEventHit("Battle", "ToBattle", "", (int) DataManager.Instance.GetCurrentCharacterId());
    }

    public void BattleEndedEvent(byte[] data)
    {
        var message = Common.ProtobufSerializer.Deserialize<ClientProtocol.BattleEndedEvent>(data);
        Debug.Log("BattleEndedEvent");

        UIManager.Instance.GetPopup<BattleEndPopup>(Popup.PopupNameEnum.BattleEnd).ShowInfo(message);
        UIManager.Instance.AddScreenCallback(GameScreen.ScreenNameEnum.Main, BattleEndedCallback);

        Analytics.gua.sendEventHit("Battle", "BattleEndedEvent", message.ChestRewardId.ToString(),
            (int) DataManager.Instance.GetCurrentCharacterId());
    }

    private void BattleEndedCallback()
    {
        Debug.Log("BattleEndedEventPopup");
        UIManager.Instance.ShowPopup(Popup.PopupNameEnum.BattleEnd);
    }

    private void OnLoadSceneCallBack()
    {
        GameNetworkManager.Instance.StartClient();
        Debug.Log("StartClient");
    }

    public void ChestOpenEvent(byte[] data)
    {
        var message = Common.ProtobufSerializer.Deserialize<ClientProtocol.ChestOpenEvent>(data);
        Debug.Log("ChestOpenEvent");
        UIManager.Instance.EventChestOpen.Invoke(message.ItemsReceived);

        if (UIManager.Instance.GetCurrentScreenName() == GameScreen.ScreenNameEnum.Battle)
        {
            UIManager.Instance.AddScreenCallback(GameScreen.ScreenNameEnum.Main, () =>
            {
                UIManager.Instance.ShowPopup(Popup.PopupNameEnum.ChestOpen,
                    () =>
                        UIManager.Instance.GetPopup<ChestOpenPopup>(Popup.PopupNameEnum.ChestOpen)
                            .ShowItems(message.BaseId, message.ItemsReceived));
            });
        }
        else
        {
            UIManager.Instance.ShowPopup(Popup.PopupNameEnum.ChestOpen,
                () =>
                    UIManager.Instance.GetPopup<ChestOpenPopup>(Popup.PopupNameEnum.ChestOpen)
                        .ShowItems(message.BaseId, message.ItemsReceived));
        }

        Analytics.gua.sendEventHit("Chest", "ChestOpenEvent", message.BaseId.ToString(),
            DataManager.Instance.CurrentCharacter != null ? (int) DataManager.Instance.GetCurrentCharacterId() : 1);
    }

    public void AchievementRewardEvent(byte[] data)
    {
        var message = Common.ProtobufSerializer.Deserialize<ClientProtocol.AchievementRewardEvent>(data);
        Debug.Log("AchievementRewardEvent");

        if (UIManager.Instance.GetCurrentScreenName() == GameScreen.ScreenNameEnum.Battle)
        {
            UIManager.Instance.AddScreenCallback(GameScreen.ScreenNameEnum.Main, () =>
            {
                UIManager.Instance.ShowPopup(Popup.PopupNameEnum.AchievementReward, () => 
                    UIManager.Instance.GetPopup<AchievementRewardPopup>(Popup.PopupNameEnum.AchievementReward).ShowAchievement(message.AchievementId, message.Reward));
            });
        }
        else
        {
            UIManager.Instance.ShowPopup(Popup.PopupNameEnum.AchievementReward, () => 
                UIManager.Instance.GetPopup<AchievementRewardPopup>(Popup.PopupNameEnum.AchievementReward).ShowAchievement(message.AchievementId, message.Reward));
            
        }

        Analytics.gua.sendEventHit("Progress", "AchievementRewardEvent", message.AchievementId.ToString(),
            (int) DataManager.Instance.GetCurrentCharacterId());
    }

    public void AchievementProgressEvent(byte[] data)
    {
        var message = Common.ProtobufSerializer.Deserialize<ClientProtocol.AchievementProgressEvent>(data);
        Debug.Log("AchievementProgressEvent" + message.NewData.Id);
        DataManager.Instance.Achievements[message.NewData.Id].Achievement = message.NewData;
    }

    public void NewItemsEvent(byte[] data)
    {
        var message = Common.ProtobufSerializer.Deserialize<ClientProtocol.NewItemsEvent>(data);

        foreach (Common.DTO.Item item in message.Items)
        {
            DataManager.Instance.AddItem(item);
        }

        UIManager.Instance.EventNewItems.Invoke();
        Debug.Log("New Items");
    }

    public void NewAbilityEvent(byte[] data)
    {
        var message = Common.ProtobufSerializer.Deserialize<ClientProtocol.NewAbilityEvent>(data);

        DataManager.Instance.AddAbility(message.Ability);

        Debug.Log("NewAbilityEvent");
    }

    public void AbilitySlotChangedEvent(byte[] data)
    {
        var message = Common.ProtobufSerializer.Deserialize<ClientProtocol.AbilitySlotChangedEvent>(data);

        DataManager.Instance.SelectAbility(message.Slot.Slot, DataManager.Instance.GetAbility(message.Slot.AbilityId));
        UIManager.Instance.EventAbilitySlotChanged.Invoke();

        Debug.Log("AbilitySlotChangedEvent");

        Analytics.gua.sendEventHit("Abilities", "AbilitySlotChangedEvent", message.Slot.AbilityId.ToString(),
            (int) DataManager.Instance.GetCurrentCharacterId());
    }

    public void AbilityLostEvent(byte[] data)
    {
        var message = Common.ProtobufSerializer.Deserialize<ClientProtocol.AbilityLostEvent>(data);

        DataManager.Instance.LostAbility(message.Ability);
    }

    public void ItemEquippedEvent(byte[] data)
    {
        var message = Common.ProtobufSerializer.Deserialize<ClientProtocol.ItemEquippedEvent>(data);
        ItemClient newEquip = DataManager.Instance.GetItem(message.Id);
        DataManager.Instance.CurrentCharacter.Equip(newEquip);
        if (newEquip != null)
        {
            newEquip.Update(newEquip.ItemDTO);
        }
        UIManager.Instance.EventEquipChanged.Invoke();
    }

    public void ItemUnequippedEvent(byte[] data)
    {
        var message = Common.ProtobufSerializer.Deserialize<ClientProtocol.ItemUnequippedEvent>(data);

        ItemClient equip = DataManager.Instance.GetItem(message.Id);
        DataManager.Instance.CurrentCharacter.UnEquip(equip);
        if (equip != null)
        {
            equip.Update(equip.ItemDTO);
        }
        UIManager.Instance.EventEquipChanged.Invoke();
    }

    public void ItemChangedEvent(byte[] data)
    {
        var message = Common.ProtobufSerializer.Deserialize<ClientProtocol.ItemChangedEvent>(data);

        Common.DTO.Item itemDTO = message.Item;
        ItemClient item = DataManager.Instance.GetItem(itemDTO.Id);
        if (item != null)
        {
            item.Update(itemDTO);
        }

        Debug.Log("ChangeItem");
        UIManager.Instance.EventItemChanged.Invoke();
    }

    public void ItemLostEvent(byte[] data)
    {
        var message = Common.ProtobufSerializer.Deserialize<ClientProtocol.ItemLostEvent>(data);
        ItemClient item = DataManager.Instance.GetItem(message.Id);
        if(item.Equiped)
        {
            DataManager.Instance.CurrentCharacter.UnEquip(item);
        }
        
        DataManager.Instance.Items.Remove(message.Id);
        UIManager.Instance.EventItemLost.Invoke();
    }

    public void EnchantItemResponse(byte[] data)
    {
        var message = Common.ProtobufSerializer.Deserialize<ClientProtocol.EnchantItemResponse>(data);
        UIManager.Instance.ShowMessage("EnchantResult" + message.Result.ToString());
    }

    public void NewPowerUpEvent(byte[] data)
    {
        Debug.Log("NewPowerUpEvent");
        var message = Common.ProtobufSerializer.Deserialize<ClientProtocol.NewPowerUpEvent>(data);
        Debug.Log(string.Format("{0:D2}:{1:D2}:{2:D2}", message.PowerUp.Expires.Hours, message.PowerUp.Expires.Minutes, message.PowerUp.Expires.Seconds));
        var powerUp = DataManager.Instance.AddPowerUp(message.PowerUp);
        UIManager.Instance.EventNewPowerUp.Invoke(powerUp);
        UIManager.Instance.ShowMessage("NewPowerUpEvent");

        Analytics.gua.sendEventHit("PowerUp", "NewPowerUpEvent", message.PowerUp.BaseId.ToString(),
            (int) DataManager.Instance.GetCurrentCharacterId());
    }

    public void PowerUpExpiredEvent(byte[] data)
    {
        var message = Common.ProtobufSerializer.Deserialize<ClientProtocol.PowerUpExpiredEvent>(data);

        var powerUp = DataManager.Instance.RemovePowerUp(message.Id);
        UIManager.Instance.EventPowerUpExpired.Invoke(powerUp);
        UIManager.Instance.ShowMessage("PowerUpExpiredEvent");
    }

    public void InventoryCapacityChangedEvent(byte[] data)
    {
        var message = Common.ProtobufSerializer.Deserialize<ClientProtocol.InventoryCapacityChangedEvent>(data);

        DataManager.Instance.InventorySize = message.NewCapacity;

        Debug.Log("InventoryCapacityChangedEvent");
    }

    public void NewQuestEvent(byte[] data)
    {
        var message = Common.ProtobufSerializer.Deserialize<ClientProtocol.NewQuestEvent>(data);

        DataManager.Instance.SetQuest(message.BaseId, message.LifeTimeSeconds);
        UIManager.Instance.EventNewQuest.Invoke();
        Debug.Log("NewQuestEvent");
    }

    public void QuestProgressEvent(byte[] data)
    {
        var message = Common.ProtobufSerializer.Deserialize<ClientProtocol.QuestProgressEvent>(data);

        DataManager.Instance.CurrentQuest.DailyQuestProgress = message.Progress;
        UIManager.Instance.EventQuestChanged.Invoke();
        Debug.Log("QuestProgressEvent");
    }

    public void QuestCompleteEvent(byte[] data)
    {
        var message = Common.ProtobufSerializer.Deserialize<ClientProtocol.QuestCompleteEvent>(data);
        Analytics.gua.sendEventHit("Progress", "QuestCompleteEvent",
            DataManager.Instance.CurrentQuest.QuestData.Id.ToString(),
            (int) DataManager.Instance.GetCurrentCharacterId());

        DataManager.Instance.SetQuest(0);
        UIManager.Instance.EventQuestComplete.Invoke();

        UIManager.Instance.AddScreenCallback(GameScreen.ScreenNameEnum.Main, ()=> UIManager.Instance.ShowPopup(Popup.PopupNameEnum.QuestReward, ()=>
            UIManager.Instance.GetPopup<QuestRewardPopup>(Popup.PopupNameEnum.QuestReward).ShowItems(message.Reward)));

        Debug.Log("QuestCompleteEvent");
    }

    public void NewPlayerEventEvent(byte[] data)
    {
        var message = Common.ProtobufSerializer.Deserialize<ClientProtocol.NewPlayerEventEvent>(data);

        UIManager.Instance.AddScreenCallback(GameScreen.ScreenNameEnum.Main, () => UIManager.Instance.ShowPopup(Popup.PopupNameEnum.PlayerEvent, () =>
             UIManager.Instance.GetPopup<PlayerEventPopup>(Popup.PopupNameEnum.PlayerEvent).ShowEvent(message.EventId, message.Items)));

        Debug.Log("NewPlayerEventEvent");
    }

    public void DailyRewardsEvent(byte[] data)
    {
        var message = Common.ProtobufSerializer.Deserialize<ClientProtocol.DailyRewardsEvent>(data);

        Debug.Log("DailyRewardsEvent");
        UIManager.Instance.GetPopup<DailyRewardPopup>(Popup.PopupNameEnum.DailyReward)
            .ShowItems(message.Items, message.Current);
        UIManager.Instance.AddScreenCallback(GameScreen.ScreenNameEnum.Main, DailyRewardsCallback);
    }

    private void DailyRewardsCallback()
    {
        UIManager.Instance.ShowPopup(Popup.PopupNameEnum.DailyReward);
    }

    public void Error(byte[] data)
    {
        var message = Common.ProtobufSerializer.Deserialize<ClientProtocol.Error>(data);

        Debug.Log("Error " + message.Cmd + " " + message.Message.ToString());

        switch (message.Message)
        {
            case ClientProtocol.ErrorCode.AccessDenied:
            case ClientProtocol.ErrorCode.OperationFailed:
            case ClientProtocol.ErrorCode.ServerIsNotAvailable:
            case ClientProtocol.ErrorCode.SessionInvalid:
                SocketManager.Instance.ShowError(message.Message);
                break;
            case ClientProtocol.ErrorCode.NameInUse:
            case ClientProtocol.ErrorCode.InsufficientResources:
            case ClientProtocol.ErrorCode.EffectAlreadyApplied:
            case ClientProtocol.ErrorCode.ItemNotAvailable:
                UIManager.Instance.ShowNotification(message.Message);
                break;
            case ClientProtocol.ErrorCode.NotEnoughInventorySpace:
                UIManager.Instance.ShowNotification(message.Message, DataManager.Instance.InventorySize.ToString(), DataManager.Instance.GetAllUnequipedItems().Count.ToString());
                break;
        }
    }

    public void KongregateUserAvatarResponse(byte[] data)
    {
        var message = Common.ProtobufSerializer.Deserialize<ClientProtocol.KongregateUserAvatarResponse>(data);
        string url = message.Url.Replace("img.resize(width:50)", "img.resize(width:110)");
        StartCoroutine(LoadAvatar(url));
    }

    public IEnumerator LoadAvatar(string url)
    {
        var wwwAvatar = new WWW(url);
        yield return wwwAvatar;
        DataManager.Instance.UserAvatar = Sprite.Create(wwwAvatar.texture, new Rect(0, 0, wwwAvatar.texture.width, wwwAvatar.texture.height), new Vector2(0.5f, 0.5f), 100);
        UIManager.Instance.GetScreen<MainScreen>(GameScreen.ScreenNameEnum.Main).SetAvatar(DataManager.Instance.UserAvatar);
    }

    public void SkillsResetEvent(byte[] data)
    {
        UIManager.Instance.ShowMessage("SkillsResetMessage");
    }
}
