﻿using UnityEngine;
using System.Collections;
 
public class FPSDisplay : MonoBehaviour
{
	float deltaTime = 0.0f;
    float fps;
    string text;
    string userId;
    Rect rect;
    GUIStyle style = new GUIStyle();

    public string message = "";
 
	void Update()
	{
		deltaTime += (Time.deltaTime - deltaTime) * 0.1f;
	}
 
	void OnGUI()
	{
        int w = Screen.width, h = Screen.height;
        rect = new Rect(0, 0, w, h * 2 / 100);
        style.alignment = TextAnchor.UpperLeft;
        style.fontSize = h * 2 / 100;
        style.normal.textColor = new Color(0.0f, 0.0f, 0.5f, 1.0f);

        fps = 1.0f / deltaTime;
        if(SocketManager.Instance != null)
        {
            userId = SocketManager.Instance.UserId == 0 ? "" : string.Format("{0} ", SocketManager.Instance.UserId);
        }
        text = string.Format("{0}{1:0.} fps", userId, fps);
        //text += message;
		GUI.Label(rect, text, style);
	}
}