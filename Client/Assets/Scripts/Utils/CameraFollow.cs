﻿using UnityEngine;
using System.Collections;
using System;

public class CameraFollow : MonoBehaviour {
	public Transform Target;
	public Rect Border;

    public float Distance = 10.0f;
	public float Smooth = 5.0f;
    public float SmoothSize = 5.0f;
    public float DefaultSize;

    private float Duration = 0.2f;
    private float Speed = 1.0f;
    private float Magnitude = 0.5f;

    private Camera _camera;
    private float _size;

    void Awake()
    {
        _camera = GetComponent<Camera>();
        _size = DefaultSize = _camera.orthographicSize;
    }
	
	void LateUpdate () 
	{
		if(Target != null)
		{
			transform.position = Vector3.Lerp(transform.position, GetTargetPosition(), Time.deltaTime * Smooth);
            _camera.orthographicSize = Mathf.Lerp(_camera.orthographicSize, _size, Time.deltaTime * SmoothSize);
        }
	}
	
	public void UpdatePosition()
	{
		transform.position = GetTargetPosition();
	}

    public void SetQuantity(int quantity)
    {
        _size = DefaultSize;
        if(quantity > 70)
        {
            _size += ((float)Math.Sqrt(quantity - 70)) / 5;
        }
    }

    public void SetDefaultSize(float wheel)
    {
        DefaultSize -= wheel * 2;
        if(DefaultSize < 5)
        {
            DefaultSize = 5;
        }
        else if(DefaultSize > 10)
        {
            DefaultSize = 10;
        }
    }

    public Vector3 GetTargetPosition()
    {
        Vector3 targetPos = Target.position;
		targetPos.z = Distance;
			
		float screenAspect = (float)Screen.width / (float)Screen.height;
		float cameraHeight = Camera.main.orthographicSize * 2;
		Bounds bounds = new Bounds(
			targetPos,
			new Vector3(cameraHeight * screenAspect, cameraHeight, 0));
			
		if(bounds.center.x - bounds.extents.x < Border.x)
		{
			targetPos.x = Border.x + bounds.extents.x;
		} 
		else if(bounds.center.x + bounds.extents.x > Border.x + Border.width)
		{
			targetPos.x = Border.x + Border.width - bounds.extents.x;
		}
			
		if(bounds.center.y - bounds.extents.y < Border.y)
		{
			targetPos.y = Border.y + bounds.extents.y;
		}
		else if(bounds.center.y + bounds.extents.y > Border.y + Border.height)
		{
			targetPos.y = Border.y + Border.height - bounds.extents.y;
		}
		return targetPos;
    }

    public void PlayShake()
    {
        StartCoroutine(Shake());
        StartCoroutine(Shake());
    }

    IEnumerator Shake()
    {

        float elapsed = 0.0f;

        Vector3 originalCamPos = Camera.main.transform.position;
        float randomStart = UnityEngine.Random.Range(-1000.0f, 1000.0f);

        while (elapsed < Duration)
        {

            elapsed += Time.deltaTime;

            float percentComplete = elapsed / Duration;

            // We want to reduce the shake from full power to 0 starting half way through
            float damper = 1.0f - Mathf.Clamp(2.0f * percentComplete - 1.0f, 0.0f, 1.0f);

            // Calculate the noise parameter starting randomly and going as fast as speed allows
            float alpha = randomStart + Speed * percentComplete;

            // map noise to [-1, 1]
            float x = Util.Noise.GetNoise(alpha, 0.0f, 0.0f) * 2.0f - 1.0f;
            float y = Util.Noise.GetNoise(0.0f, alpha, 0.0f) * 2.0f - 1.0f;

            x *= Magnitude * damper;
            y *= Magnitude * damper;

            Camera.main.transform.position = new Vector3(originalCamPos.x + x, originalCamPos.y + y, originalCamPos.z);

            yield return null;
        }

        //Camera.main.transform.position = originalCamPos;
    }
}
