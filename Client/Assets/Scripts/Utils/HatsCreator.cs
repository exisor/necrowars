﻿using UnityEngine;
using System.Collections;

public class HatsCreator : MonoBehaviour
{
    public HatController[] Controllers;
    public string Name;
    public int Id;
    public Sprite Sprite;
    public Sprite ColorSprite;

    public void Create()
    {
	    foreach(var controller in Controllers)
        {
            GameObject hatGameObject = new GameObject();
            
            hatGameObject.transform.SetParent(controller.HatContainer.transform, false);
            hatGameObject.transform.localPosition = new Vector3(0, 0, -0.001f);

            var spriteHat = hatGameObject.AddComponent<SpriteRenderer>();
            spriteHat.sortingLayerName = "SortingByY";
            spriteHat.sprite = Sprite;

            hatGameObject.name = Name;
            hatGameObject.layer = 9;

            var hat = hatGameObject.AddComponent<Hat>();
            hat.Id = Id;
            hat.Image = spriteHat;
            

            if (ColorSprite != null)
            {
                GameObject subHatGameObject = new GameObject();
                subHatGameObject.transform.SetParent(hatGameObject.transform, false);
                subHatGameObject.transform.localPosition = new Vector3(0, 0, -0.001f);

                var spriteSub = subHatGameObject.AddComponent<SpriteRenderer>();
                spriteSub.sortingLayerName = "SortingByY";
                spriteSub.sprite = ColorSprite;

                subHatGameObject.name = Name + "Color";
                subHatGameObject.layer = 9;

                hat.ColorImage = spriteSub;
            }
        }
	}
	
	
}
