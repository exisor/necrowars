﻿using UnityEngine;
using System.Collections;

public class SortingObjectByY : MonoBehaviour
{
    public Transform View;

    private Vector3 _position;

	public void FixedUpdate()
	{
        _position = View.position;
        _position.z = transform.position.y;
        View.position = _position;
	}
}
