﻿
using System;

public class StringUtils
{
    public static string FormatNumber(int val)
    {
        var num = MaxThreeSignificantDigits(val);

        if (num >= 100000000)
            return (num / 1000000D).ToString("0.#M");
        if (num >= 1000000)
            return (num / 1000000D).ToString("0.##M");
        if (num >= 100000)
            return (num / 1000D).ToString("0K");
        if (num >= 100000)
            return (num / 1000D).ToString("0.#K");
        if (num >= 1000)
            return (num / 1000D).ToString("0.##K");
        return num.ToString("#,0");
    }


    internal static long MaxThreeSignificantDigits(int x)
    {
        int i = (int) Math.Log10(x);
        i = Math.Max(0, i - 2);
        i = (int)Math.Pow(10, i);
        return x / i * i;
    }
}
