﻿using UnityEngine;
using System.Collections.Generic;
using UnityEngine.UI;
using DG.Tweening;

public class BlinkChildrenEffect : MonoBehaviour
{
    private Image[] _images;
    private Text[] _texts;

    public float Delay = 0.5f;
    public float Time = 0f;
    public float MinAlpha = 0.5f;
    public float MaxAlpha = 1f;

    void OnEnable ()
    {
        _images = GetComponentsInChildren<Image>();
        _texts = GetComponentsInChildren<Text>();


        foreach (var image in _images)
        {
            TweenImage(image);
        }

        foreach (var text in _texts)
        {
            TweenText(text);
        }
    }

    private void TweenImage(Image image)
    {
        image.DOFade(image.color.a == MaxAlpha ? MinAlpha : MaxAlpha, Time).SetDelay(Delay).OnComplete(() => TweenImage(image));
    }

    private void TweenText(Text text)
    {
        text.DOFade(text.color.a == MaxAlpha ? MinAlpha : MaxAlpha, Time).SetDelay(Delay).OnComplete(() => TweenText(text));
    }

    void OnDisable ()
    {
        foreach (var image in _images)
        {
            DOTween.Kill(image);

            var imageColor = image.color;
            imageColor.a = 1;
            image.color = imageColor;
        }

        foreach (var text in _texts)
        {
            DOTween.Kill(text);

            var textColor = text.color;
            textColor.a = 1;
            text.color = textColor;
        }
    }
}
