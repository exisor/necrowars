﻿using UnityEngine;
using System.Collections;

public class MathUtils
{

    public static Vector3 GetPoint(Vector3 center, Vector3 end)
    {
        if (IsIntersect(0, 0, Screen.width, 0, center.x, center.y, end.x, end.y)) return Intersect(3, center, end) + new Vector3(0, 0, 10);
        else if (IsIntersect(Screen.width, 0, Screen.width, Screen.height, center.x, center.y, end.x, end.y)) return Intersect(0, center, end) + new Vector3(0, 0, 10);
        else if (IsIntersect(Screen.width, Screen.height, 0, Screen.height, center.x, center.y, end.x, end.y)) return Intersect(1, center, end) + new Vector3(0, 0, 10);
        else return Intersect(2, center, end) + new Vector3(0, 0, 10);
    }

    private static bool IsIntersect(float a1x, float a1y, float a2x, float a2y, float b1x, float b1y, float b2x, float b2y)
    {

        float d = (a1x - a2x) * (b2y - b1y) - (a1y - a2y) * (b2x - b1x);
        float da = (a1x - b1x) * (b2y - b1y) - (a1y - b1y) * (b2x - b1x);
        float db = (a1x - a2x) * (a1y - b1y) - (a1y - a2y) * (a1x - b1x);

        float ta = da / d;
        float tb = db / d;

        if ((0 <= ta) && (ta <= 1) && (0 <= tb) && (tb <= 1))
        {
            return true;
        }
        return false;
    }

    private static Vector3 Intersect(int edgeLine, Vector3 line2point1, Vector3 line2point2)
    {
        float[] A1 = { -Screen.height, 0, Screen.height, 0 };
        float[] B1 = { 0, -Screen.width, 0, Screen.width };
        float[] C1 = { -Screen.width * Screen.height, -Screen.width * Screen.height, 0, 0 };

        float A2 = line2point2.y - line2point1.y;
        float B2 = line2point1.x - line2point2.x;
        float C2 = A2 * line2point1.x + B2 * line2point1.y;

        float det = A1[edgeLine] * B2 - A2 * B1[edgeLine];

        return new Vector3((B2 * C1[edgeLine] - B1[edgeLine] * C2) / det, (A1[edgeLine] * C2 - A2 * C1[edgeLine]) / det, 0);
    }

    public static bool OnScreen(Vector2 input)
    {
        return !(input.x > Screen.width || input.x < 0 || input.y > Screen.height || input.y < 0);
    }
}
