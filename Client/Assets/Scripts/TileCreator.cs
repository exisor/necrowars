﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

public class TileCreator : MonoBehaviour
{
    public Texture2D HeigtMap;
    public GameObject Prefab;
    public GameObject PrefabTransition;

    public Transform StartTransform;

    public float Width;
    public float Height;
    public float HeightOffset = 0.1f;

    public Sprite[] Sprites;
    public Sprite[] SpritesTransition;


    public bool random;

    private List<List<int>> _heightmapData;
    private int _widthMap;
    private int _heightMap;
    public void Generate()
    {
        ApplyHeightmap();
        CreateTransitions();
        CreateTiles();

        
    }

    public void Clear()
    {
        var children = new List<GameObject>();
        foreach (Transform child in StartTransform) children.Add(child.gameObject);
        children.ForEach(child => DestroyImmediate(child));
    }

    private void CreateTiles()
    {
        for (int y = 0; y < _heightMap; y++)
        {

            for (int x = 0; x < _widthMap; x++)
            {
                GameObject go = Instantiate(Prefab);
                go.GetComponent<SpriteRenderer>().sprite = Sprites[_heightmapData[y][x]];
                //go.GetComponent<SpriteRenderer>().sprite = Sprites[Random.Range(0, Sprites.Length)];
                //go.GetComponent<SpriteRenderer>().flipX = Random.value > .5;
                go.transform.SetParent(StartTransform, false);

                Vector3 pos = Vector3.zero;
                pos.x = StartTransform.position.x + (x * Width);
                if ((y % 2) != 0)
                {
                    pos.x += Width / 2;
                }
                pos.y = StartTransform.position.y + (y * (Height / 2)) + (random ? Random.Range(-HeightOffset, HeightOffset) : 0);

                go.transform.position = pos;
            }
        }
    }

    private void CreateTransitions()
    {
        for (int y = 0; y < _heightMap; y++)
        {
            for (int x = 0; x < _widthMap; x++)
            {
                Vector2 left = new Vector2(x - 1, y + 1);
                Vector2 right = new Vector2(x, y + 1);
                Vector2 up = new Vector2(x, y + 2);
                if ((y % 2) != 0)
                {
                    left.x += 1;
                    right.x += 1;
                }
                CreateTransition(x, y, (int)left.x, (int)left.y, new Vector2(-Width * 0.25f, Height * 0.25f));
                CreateTransition(x, y, (int)right.x, (int)right.y, new Vector2(Width * 0.25f, Height * 0.25f), true);
            }
        }
    }

    private void CreateTransition(int currentX, int currentY, int comparisonX, int comparisonY, Vector2 offset, bool flip = false)
    {
        var row = _heightmapData.ElementAtOrDefault(comparisonY);

        if (row != null)
        {
            if (comparisonX >= 0 && comparisonX < _widthMap && row[comparisonX] != _heightmapData[currentY][currentX])
            {
                GameObject go = Instantiate(PrefabTransition);
                go.GetComponent<SpriteRenderer>().sprite = SpritesTransition[_heightmapData[currentY][currentX]];
                go.GetComponent<SpriteRenderer>().flipX = flip;
                go.transform.SetParent(StartTransform, false);

                Vector3 pos = Vector3.zero;
                pos.x = StartTransform.position.x + (currentX * Width) + offset.x;
                if ((currentY % 2) != 0)
                {
                    pos.x += Width / 2;
                }
                pos.y = StartTransform.position.y + (currentY * (Height / 2)) + offset.y;

                go.transform.position = pos;
            }
        }
    }

    private void ApplyHeightmap()
    {
        _widthMap = HeigtMap.width;
        _heightMap = HeigtMap.height;
        _heightmapData = new List<List<int>>();
        List<Color> mapColors = HeigtMap.GetPixels().ToList();
        Debug.Log(mapColors.Count);

        for (int y = 0; y < _heightMap; y++)
        {
            _heightmapData.Add(new List<int>());
            for (int x = 0; x < _widthMap; x++)
            {
                int index = y * _widthMap + x;
                if (mapColors[index].grayscale > 0.5)
                {
                    _heightmapData[y].Add(2);
                }
                else if(mapColors[index].grayscale > 0.1)
                {
                    _heightmapData[y].Add(1);
                }
                else
                {
                    _heightmapData[y].Add(0);
                }
            }
        }
    }

}
