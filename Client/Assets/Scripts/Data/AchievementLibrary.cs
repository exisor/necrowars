﻿using System.Collections.Generic;
using UnityEngine;
using System;

public class AchievementLibrary : ScriptableObject
{
    public TextAsset AchievementsXML;
    public List<Achievement> Achievements;
}
