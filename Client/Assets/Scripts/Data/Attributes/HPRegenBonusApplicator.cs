﻿using Assets.Scripts.Attributes;
using Common.Resources;

namespace Assets.Scripts.Data.Attributes
{
    public class HPRegenBonusApplicator : FloatBonusApplicator<FloatAttribute>
    {
        public HPRegenBonusApplicator(IAffixValues affixValues, BonusType type) : base(affixValues, type)
        {
        }

        public HPRegenBonusApplicator(IBonus<float> bonus) : base(bonus)
        {
        }

        #region Overrides of BonusApplicator<float,FloatAttribute>

        protected override FloatAttribute Target
        {
            get { return DataManager.Instance.CurrentCharacter.HPRegen; }
            set { DataManager.Instance.CurrentCharacter.HPRegen = value; }
        }

        protected override string ValueName
        {
            get { return "HP"; }
        }

        #endregion
    }
}