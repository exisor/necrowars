using System;
using Assets.Scripts.Attributes;
using Common.Resources;

namespace Assets.Scripts.Data.Attributes
{
    #region bonus applicator

    public abstract class BonusApplicator<TVal, TAttr> : IBonusApplicator
        where TVal : struct
        where TAttr : Attribute<TVal, TAttr>
    {
        private readonly IBonus<TVal> _bonus;
        protected readonly BonusType Type;
        protected BonusApplicator(IAffixValues affixValues, BonusType type)
        {
            Type = type;
            var value = affixValues.GetValue(ValueName);
            _bonus = GetBonus(value, type);
        }

        protected BonusApplicator(IBonus<TVal> bonus)
        {
            _bonus = bonus;
        }

        public void Apply()
        {
            if (_bonus == null) return;
            var target = Target;
            if (target == null) return;

            Target += _bonus;
        }

        public void Remove()
        {
            if (_bonus == null) return;
            var target = Target;
            if (target == null) return;

            Target -= _bonus;
        }
        protected abstract TAttr Target { get; set; }
        protected abstract string ValueName { get; }

        protected abstract IBonus<TVal> GetBonus(int val, BonusType type);
    }

    #endregion
}
