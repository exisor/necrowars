﻿using Assets.Scripts.Attributes;
using Common.Resources;

namespace Assets.Scripts.Data.Attributes
{
    public class HPBonusApplicator : FloatBonusApplicator<FloatAttribute>
    {
        public HPBonusApplicator(IAffixValues affixValues, BonusType type) : base(affixValues, type) { }
        public HPBonusApplicator(IBonus<float> bonus) : base(bonus) { }
        #region Overrides of BonusApplicator<FloatAttribute>

        protected override FloatAttribute Target
        {
            get { return DataManager.Instance.CurrentCharacter.MaxHP; }
            set { DataManager.Instance.CurrentCharacter.MaxHP = value; }
        }

        protected override string ValueName
        {
            get { return "HP"; }
        }


        #endregion
    }
}