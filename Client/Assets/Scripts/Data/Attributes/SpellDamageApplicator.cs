﻿using Assets.Scripts.Attributes;
using Common.Resources;

namespace Assets.Scripts.Data.Attributes
{
    public class SpellDamageApplicator : FloatBonusApplicator<MultiplierAttribute>
    {
        public SpellDamageApplicator(IAffixValues affixValues, BonusType type) : base(affixValues, type)
        {
        }

        public SpellDamageApplicator(IBonus<float> bonus) : base(bonus)
        {
        }

        #region Overrides of BonusApplicator<float,MultiplierAttribute>

        protected override MultiplierAttribute Target
        {
            get { return DataManager.Instance.CurrentCharacter.SpellDamage; }
            set { DataManager.Instance.CurrentCharacter.SpellDamage = value; }
        }

        protected override string ValueName
        {
            get { return "Damage"; }
        }

        #endregion
    }
}
