﻿using Assets.Scripts.Attributes;
using Common.Resources;

namespace Assets.Scripts.Data.Attributes
{
    public abstract class FloatBonusApplicator<T> : BonusApplicator<float, T> where T : FloatAttribute<T>
    {
        protected FloatBonusApplicator(IAffixValues affixValues, BonusType type) : base(affixValues, type) { }

        protected FloatBonusApplicator(IBonus<float> bonus) : base(bonus) { }
        #region Overrides of BonusApplicator<float,T>

        protected override IBonus<float> GetBonus(int val, BonusType type)
        {
            switch (type)
            {
                case BonusType.Fixed:
                    return new FixedFloatBonus(val);
                case BonusType.Percent:
                    return RelFloatBonus.FromPercent(val);
                case BonusType.Multiplier:
                    return RelFloatBonus.FromMultiplier(val);
                default:
                    return null;
            }
        }

        #endregion
    }
}