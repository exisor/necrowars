﻿namespace Assets.Scripts.Data.Attributes
{
    public interface IBonusApplicator
    {
        void Apply();
        void Remove();
    }
}