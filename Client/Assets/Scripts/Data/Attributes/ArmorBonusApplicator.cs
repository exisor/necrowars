﻿using Assets.Scripts.Attributes;
using Common.Resources;

namespace Assets.Scripts.Data.Attributes
{
    public class ArmorBonusApplicator : FloatBonusApplicator<ArmorAttribute>
    {
        public ArmorBonusApplicator(IAffixValues affixValues, BonusType type) : base(affixValues, type) { }
        public ArmorBonusApplicator(IBonus<float> bonus) : base(bonus) { }
        #region Overrides of BonusApplicator<ArmorAttribute>

        protected override ArmorAttribute Target
        {
            get { return DataManager.Instance.CurrentCharacter.Armor; }
            set { DataManager.Instance.CurrentCharacter.Armor = value; }
        }
        protected override string ValueName
        {
            get { return "Armour"; }
        }


        #endregion
    }
}