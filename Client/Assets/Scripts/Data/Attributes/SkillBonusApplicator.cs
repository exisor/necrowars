﻿using Common.Resources;

namespace Assets.Scripts.Data.Attributes
{
    public class SkillBonusApplicator : IntBonusApplicator<IntAttribute>
    {
        private readonly long _abilityId;
        public SkillBonusApplicator(IAffixValues affixValues, BonusType type) : base(affixValues, type)
        {
            _abilityId = affixValues.GetValue("AvailableSkills");
            
        }

        #region Overrides of BonusApplicator<int,IntAttribute>

        protected override IntAttribute Target
        {
            get
            {
                AbilityClient ability;
                DataManager.Instance.CurrentCharacter.Abilities.TryGetValue(_abilityId, out ability);
                if (ability == null) return null;
                return ability.BonusLevels;
            }
            set
            {
                AbilityClient ability;
                DataManager.Instance.CurrentCharacter.Abilities.TryGetValue(_abilityId, out ability);
                if (ability == null) return;
                ability.BonusLevels = value;
            }
        }

        protected override string ValueName
        {
            get { return "Skill"; }
        }

        #endregion
    }
}