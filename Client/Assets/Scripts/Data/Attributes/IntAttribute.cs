using Assets.Scripts.Attributes;

namespace Assets.Scripts.Data.Attributes
{
    public abstract class IntAttribute<T> : Attribute<int, T> where T : IntAttribute<T>
    {
        protected IntAttribute() : base() { }
        protected IntAttribute(int value) : base(value) { }
    }

    public class IntAttribute : IntAttribute<IntAttribute>
    {
        public IntAttribute() : base() { }
        public IntAttribute(int value) : base(value) { }
    }
}