﻿
using Assets.Scripts.Attributes;
using Common.Resources;

namespace Assets.Scripts.Data.Attributes
{
    public class AttackSpeedBonusApplicator : FloatBonusApplicator<AttackSpeedAttribute>
    {
        public AttackSpeedBonusApplicator(IAffixValues affixValues, BonusType type) : base(affixValues, type)
        {
        }

        public AttackSpeedBonusApplicator(IBonus<float> bonus) : base(bonus)
        {
        }

        #region Overrides of BonusApplicator<float,AttackSpeedAttribute>

        protected override AttackSpeedAttribute Target
        {
            get
            {
                return DataManager.Instance.CurrentCharacter.AttackSpeed;
            }
            set { DataManager.Instance.CurrentCharacter.AttackSpeed = value; }
        }

        protected override string ValueName
        {
            get { return "AttackSpeed"; }
        }

        #endregion
    }
}
