﻿using System;
using Assets.Scripts.Attributes;
using UnityEngine;

namespace Assets.Scripts.Data.Attributes
{
    public class FixedIntBonus : IBonus<int>
    {
        private readonly int _value;
        private int _counter;
        public FixedIntBonus(int value)
        {
            _value = value;
        }

        #region Implementation of IDisposable

        public void Dispose()
        {
            _counter--;
            if (_counter < 0)
            {
                Debug.LogWarningFormat("Disposed but not aquired!");
            }
        }

        #endregion

        #region Implementation of IBonus<int>

        public int Calc(int value)
        {
            return value + _value;
        }

        public void Aquire()
        {
            _counter++;
        }

        public int Priority { get { return 1; } }
        public event Action<IBonus<int>> Updated;

        #endregion
    }
}
