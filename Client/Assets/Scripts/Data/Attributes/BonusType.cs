﻿namespace Assets.Scripts.Data.Attributes
{
    public enum BonusType
    {
        Fixed,
        Percent,
        Multiplier
    }
}