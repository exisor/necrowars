﻿using Assets.Scripts.Attributes;
using Common.Resources;

namespace Assets.Scripts.Data.Attributes
{
    public class MinDamageBonusApplicator : FloatBonusApplicator<MinDamageAttribute>
    {
        public MinDamageBonusApplicator(IAffixValues affixValues, BonusType type) : base(affixValues, type)
        {
        }
        public MinDamageBonusApplicator(IBonus<float> bonus) : base(bonus) { }
        #region Overrides of BonusApplicator<float,MinDamageAttribute>

        protected override MinDamageAttribute Target
        {
            get { return DataManager.Instance.CurrentCharacter.MinDamage; }
            set { DataManager.Instance.CurrentCharacter.MinDamage = value; }
        }

        protected override string ValueName
        {
            get { return Type == BonusType.Fixed ? "DamageMin" : "Damage"; }
        }

        #endregion
    }

    public class MaxDamageBonusApplicator : FloatBonusApplicator<MaxDamageAttribute>
    {
        public MaxDamageBonusApplicator(IAffixValues affixValues, BonusType type) : base(affixValues, type)
        {
        }
        public MaxDamageBonusApplicator(IBonus<float> bonus ): base(bonus) { }
        #region Overrides of BonusApplicator<float,MaxDamageAttribute>

        protected override MaxDamageAttribute Target
        {
            get { return DataManager.Instance.CurrentCharacter.MaxDamage; }
            set { DataManager.Instance.CurrentCharacter.MaxDamage = value; }
        }

        protected override string ValueName
        {
            get { return Type == BonusType.Fixed ? "DamageMax" : "Damage"; }
        }

        #endregion
    }
}