using Assets.Scripts.Attributes;
using Common.Resources;

namespace Assets.Scripts.Data.Attributes
{
    public abstract class IntBonusApplicator<T> : BonusApplicator<int, T> where T : IntAttribute<T>
    {
        protected IntBonusApplicator(IAffixValues affixValues, BonusType type) : base(affixValues, type)
        {
        }

        #region Overrides of BonusApplicator<int,T>

        protected override IBonus<int> GetBonus(int val, BonusType type)
        {
            switch (type)
            {
                case BonusType.Fixed:
                    return new FixedIntBonus(val);
                case BonusType.Percent:
                    return null;
                case BonusType.Multiplier:
                    return null;
                default:
                    return null;
            }
        }

        #endregion
    }
}