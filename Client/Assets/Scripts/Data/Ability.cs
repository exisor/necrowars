﻿using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;
using System.Xml;
using System.Xml.Serialization;
using System.Linq;

[Serializable]
public class Ability
{
    [Serializable]
    public class AbilityRequirement
    {
        public long Id;
        public long Level;
    }

    public long Id;
    public Common.AbilityType Type;
    [XmlIgnoreAttribute]
    public string NameKey;
    [XmlIgnoreAttribute]
    public string DescriptionKey;
    [XmlIgnoreAttribute]
    public string DescriptionUpgradeKey;
    public bool isEndless = true;
    [XmlIgnoreAttribute]
    public bool isUpgradebale = true;
    [XmlIgnoreAttribute]
    public bool isSelectable = false;
    public List<Common.ClientValue> Values;
    public List<AbilityRequirement> AbilityRequirements;

    [XmlIgnoreAttribute]
    public Sprite Sprite;
    [XmlIgnoreAttribute]
    public CustomAbilityTooltip CustomLogic;
}
