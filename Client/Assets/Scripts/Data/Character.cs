﻿using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;
using System.Xml;
using System.Xml.Serialization;
using System.Linq;

[Serializable]
public class Character
{ 
    public long Id;
    public long BaseHP;
    public float HPCoefficent;
    public long BaseDamage;
    public float DamageCoefficent;
    public long BaseDamageSpeed;
    public long BaseSpeed;

    public List<long> AvailableAbilities;

    [XmlIgnoreAttribute]
    public string NameKey;
    [XmlIgnoreAttribute]
    public string DescriptionKey;
    [XmlIgnoreAttribute]
    public Sprite Sprite;
}
