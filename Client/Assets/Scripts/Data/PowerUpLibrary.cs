﻿using System.Collections.Generic;
using UnityEngine;
using System;

public class PowerUpLibrary : ScriptableObject 
{
    public List<PowerUp> PowerUps;

}
