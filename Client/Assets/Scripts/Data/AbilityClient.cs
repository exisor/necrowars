﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using System.Linq;
using Assets.Scripts.Attributes;
using Assets.Scripts.Data.Attributes;

public class AbilityClient 
{
	public Ability Ability;

    private long _level;
	public long Level { get {return _level;
	}
	    set
	    {
	        if (_level == value) return;
	        _level = value;
	        OnLevelChanged();
	    } }

    private readonly ILevelBonus _levelBonus;
    private void OnLevelChanged()
    {
        if (_levelBonus != null)
        {
            _levelBonus.Level = (int)TotalLevels;
        }
    }

    public AbilityClient(Common.DTO.Ability ability)
	{
		Ability = ResourceManager.Instance.GetAbilityById(ability.Id);
	    _levelBonus = GetLevelBonus(ability.Id);
        BonusLevels = new IntAttribute();
        BonusLevels.ValueChanged += BonusLevelsOnValueChanged;
		Level = ability.Level;
	    for (var i = 0; i < _applicators.Count; ++i)
	    {
	        _applicators[i].Apply();
	    }
	}

    private readonly List<IBonusApplicator> _applicators = new List<IBonusApplicator>();

    private ILevelBonus GetLevelBonus(long abilityId)
    {
        switch (abilityId)
        {
            // Valkirye hp and armor
            case 2:
            {
                var retval = new StraightLevelRelBonus(Ability.Values[0].Value);
                _applicators.Add(new HPBonusApplicator(retval));
                _applicators.Add(new ArmorBonusApplicator(retval));
                return retval;
            }
            // Valkirye damage
            case 6:
            {
                var retval = new StraightLevelRelBonus(Ability.Values[0].Value);
                _applicators.Add(new MinDamageBonusApplicator(retval));
                _applicators.Add(new MaxDamageBonusApplicator(retval));
                return retval;
            }
            // Valkirye aspd
            case 5:
            {
                var retval = new StraightLevelRelBonus(Ability.Values[0].Value);
                _applicators.Add(new AttackSpeedBonusApplicator(retval));
                return retval;
            }
            default:
                return null;
        }
    }

    private void BonusLevelsOnValueChanged(int i)
    {
        OnLevelChanged();
    }

    public string GetName()
    {
        return Localization.GetString(Ability.NameKey);
    }

    public string GetDescription()
    {
		try
		{
			if(Ability.CustomLogic != null)
			{
				return Ability.CustomLogic.GetTooltip(this);
			}
			return string.Format(Localization.GetString(Ability.DescriptionKey), Ability.Values.Select(v =>
				(v.Value * (v.LevelAddicted && Level != 0 ? Level : 1) * (v.IsPercent ? 100 : 1)).ToString("F2")).ToArray());
		}
		catch(Exception e){
			Debug.LogErrorFormat ("Exception in ability description. Ability id : {0}", Ability.Id);
			throw e;
		}
      
    }

    public string GetUpgradeDescription()
    {
		try
		{
			
        if (Ability.CustomLogic != null)
        {
            return Ability.CustomLogic.GetUpgradeTooltip(this);
        }
        List<float> values = Ability.Values.Select(v => (v.Value * (v.LevelAddicted && Level != 0 ? Level : 1) * (v.IsPercent ? 100 : 1))).ToList();
        values = values.Concat(Ability.Values.Select(v => (v.Value * (v.LevelAddicted ? Level + 1 : 1) * (v.IsPercent ? 100 : 1)))).ToList();
        return string.Format(Localization.GetString(Ability.DescriptionUpgradeKey), values.Select(v => v.ToString("F2")).ToArray());
		}
		catch(Exception e){
			Debug.LogErrorFormat ("Exception in ability description. Ability id : {0}", Ability.Id);
			throw e;
		}
	}

    public bool CanUpgrade()
    {
        if(!Ability.isUpgradebale)
        {
            return false;
        }
        foreach(var req in Ability.AbilityRequirements)
        {
            if(DataManager.Instance.GetAbility(req.Id).Level < req.Level)
            {
                return false;
            }
        }
        return true;
    }

    public IntAttribute BonusLevels;

    public long TotalLevels { get { return Level + BonusLevels; } }
}
