﻿using System;
using UnityEngine;

[Serializable]
public class CurrencyItem
{
    public long InternalId;
    public string KongId;
    public string OGZId;
    public Common.DTO.Rarity Rarity;
    public string NameKey;
    public string DescriptionKey;
    public Sprite Sprite;
    public int Quantity = 1;
}
