﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System;
using Assets.Scripts.Data.Attributes;
using Common.Resources;

public class ItemClient 
{
	public Item ItemData;
	public Common.DTO.Item ItemDTO;
	public List<AffixClient> Affixes = new List<AffixClient>();
    public List<string> AffixTooltips = new List<string>();

    private bool _equiped = false;
    public bool Equiped { get { return _equiped;
    }
        set
        {
            if (_equiped == value) return;
            _equiped = value;
            if (_equiped)
            {
                ApplyAffixBonuses();
            }
            else
            {
                RemoveAffixBonuses();
            }
        } }

    public event Action EventOnUpdate;

    private int _seed;

	public ItemClient(Common.DTO.Item item)
	{
        Update(item);

        ItemData = ResourceManager.Instance.GetItemById(ItemDTO.BaseId);

        foreach(var affix in ItemDTO.Affixes)
        {
            _seed = _seed ^ affix.Seed;
        }
    }

    public void Update(Common.DTO.Item item)
    {
        ItemDTO = item;

        var affixValues = DataManager.Instance.AffixManager.GetAffixValues(ItemDTO).ToList();
        Affixes = new List<AffixClient>();
        AffixTooltips = new List<string>();

        for (var i = 0; i < affixValues.Count; i++)
        {
            Common.DTO.ItemAffix AffixDTO = ItemDTO.Affixes[i];
            AffixClient affixClient = new AffixClient(ResourceManager.Instance.GetAffixById(AffixDTO.Id), AffixDTO, affixValues[i]);
            Affixes.Add(affixClient);
            if(affixClient.isVisible)
            {
                AffixTooltips.Add(affixClient.Tooltip);
            }
        }
        UpdateAffixes();
        if(EventOnUpdate != null)
        {
            EventOnUpdate();
        }
    }

    public Sprite GetSprite()
    {
        if(ItemData.Sprites.Length == 1)
        {
            return ItemData.Sprites[0];
        }
        else if(ItemData.Sprites.Length > 1)
        {
            return ItemData.Sprites[(int)ItemDTO.Rarity];
        }
        return null;
    }

    public string GetItemDescription()
    {
        if (ItemData.Types.Contains(ItemType.Consumable))
        {
            return "\n" + Localization.GetString(ItemData.DescriptionsKey[0]);
        }
        string tooltip = "";
        foreach (string affix in AffixTooltips)
        {
            tooltip += "\n" + affix;
        }
        if(ItemData.SetId != 0)
        {
            var set = ResourceManager.Instance.GetSetById(ItemData.SetId);
            int quantity = DataManager.Instance.GetEquipItemsBySetId(set.Id).Count;

            tooltip += "\n\n" + Localization.GetString(set.NameKey) + " (" + quantity.ToString() + "/" + set.ItemsCount.ToString() + ")\n";

            foreach (var bonus in set.Bonuses)
            {
                var affixResource = ResourceManager.Instance.GetAffixById(bonus.AffixId);
                string text = "(" + bonus.ItemsCount + ") " + Localization.GetString(affixResource.DescriptionKey);
                if (quantity >= bonus.ItemsCount)
                {
                    tooltip += "\n<color=#68bb00ff>" + text + "</color>";
                }
                else
                {
                    tooltip += "\n" + text;
                }
            }
        }

        if(tooltip == "" && ItemData.DescriptionsKey.Count > 0)
        {
            return "\n" + Localization.GetString(ItemData.DescriptionsKey[0]);
        }
        return tooltip;
    }

    public string GetItemName()
	{
		if(ItemData.Generic)
		{
			return Localization.GetItemString(_seed, ItemData.NameKey, ItemData.NameGender) + (ItemDTO.Enchant != 0 ? " +" + ItemDTO.Enchant.ToString() : "");
		}
        return Localization.GetString(ItemData.NameKey) + (ItemDTO.Enchant != 0 ? " +" + ItemDTO.Enchant.ToString() : "");
	}

    public string GetLevelString()
    {
        return ItemDTO.ItemLevel + Localization.GetString("GUI_FORGE_LEVEL");
    }

    public string GetEnchantString()
    {
        return Localization.GetString("GUI_FORGE_ENCHANT") + (ItemDTO.Enchant + 1).ToString();
    }

    public string GetEnchantHintString()
    {
        if (ItemDTO.Enchant < 3)
        {
            return "";
        }
        if (ItemDTO.Enchant < 5)
        {
            return Localization.GetString("GUI_FORGE_ENCHANT_HINT_1") + (ItemDTO.Enchant -1).ToString();
        }
        else if (ItemDTO.Enchant < 8)
        {
            return Localization.GetString("GUI_FORGE_ENCHANT_HINT_2");
        }
        else if (ItemDTO.Enchant < 10)
        {
            return Localization.GetString("GUI_FORGE_ENCHANT_HINT_3");
        }
        return Localization.GetString("GUI_FORGE_ENCHANT_HINT_4");
    }

    public int GetSumAffixesEnchant()
    {
        int sum = 0;
        Affixes.ForEach(a => sum += a.AffixDTO.Enchant);
        return sum;
    }

    private readonly List<IBonusApplicator> _applicators = new List<IBonusApplicator>();
    // call on equip
    private void ApplyAffixBonuses()
    {
        for (var i = 0; i < _applicators.Count; ++i)
        {
            _applicators[i].Apply();
        }
    }
    // call on unequip
    private void RemoveAffixBonuses()
    {
        for (var i = 0; i < _applicators.Count; ++i)
        {
            _applicators[i].Remove();
        }
    }
    // call on item update
    private void UpdateAffixes()
    {
        bool wasEquipped = Equiped;
        if (Equiped)
        {
            RemoveAffixBonuses();
        }
        _applicators.Clear();
        var affixValues = DataManager.Instance.AffixManager.GetAffixValues(ItemDTO).ToArray();
        for (var i = 0; i < affixValues.Length; ++i)
        {
            var av = affixValues[i];
            try
            {
                switch (av.Id)
                {
                    //fixed hp affix
                    case 1:
                    {
                        _applicators.Add(new HPBonusApplicator(av, BonusType.Fixed));
                        break;
                    }
                    // fixed armor affix
                    case 2:
                    {
                        _applicators.Add(new ArmorBonusApplicator(av, BonusType.Fixed));
                        break;
                    }
                    // armor percent
                    case 3:
                    {
                        _applicators.Add(new ArmorBonusApplicator(av, BonusType.Percent));
                        break;
                    }
                    // hp percent
                    case 4:
                    {
                        _applicators.Add(new HPBonusApplicator(av, BonusType.Percent));
                        break;
                    }
                    // hp regen
                    case 14:
                    {
                        _applicators.Add(new HPRegenBonusApplicator(av, BonusType.Fixed));
                        break;
                    }
                    // +n to passive skills
                    case 16:
                    {
                        _applicators.Add(new SkillBonusApplicator(av, BonusType.Fixed));
                        break;
                    }
                    // +n to active skills
                    case 17:
                    {
                        _applicators.Add(new SkillBonusApplicator(av, BonusType.Fixed));
                        break;
                    }
                    // min max damage
                    case 18:
                    {
                        _applicators.Add(new MinDamageBonusApplicator(av, BonusType.Fixed));
                        _applicators.Add(new MaxDamageBonusApplicator(av, BonusType.Fixed));
                        break;
                    }
                    // dmg percent
                    case 19:
                    {
                        _applicators.Add(new MinDamageBonusApplicator(av, BonusType.Percent));
                        _applicators.Add(new MaxDamageBonusApplicator(av, BonusType.Percent));
                        break;
                    }
                    // Attack Speed
                    case 22:
                    {
                        _applicators.Add(new AttackSpeedBonusApplicator(av, BonusType.Percent));
                        break;
                    }
                    // spell damage
                    case 26:
                    {
                        _applicators.Add(new SpellDamageApplicator(av, BonusType.Percent));
                        break;
                    }
                    // implicit armor
                    case 29:
                    {
                        _applicators.Add(new ArmorBonusApplicator(av, BonusType.Fixed));
                        break;
                    }
                    // implicit damage
                    case 30:
                    {
                        _applicators.Add(new MinDamageBonusApplicator(av, BonusType.Fixed));
                        _applicators.Add(new MaxDamageBonusApplicator(av, BonusType.Fixed));
                        break;
                    }
                } //endswitch
            }
            catch (Exception e)
            {
                Debug.LogException(e);
                Debug.LogErrorFormat("Affix id is {0}", av.Id);
            }
        }//endfor
        if (wasEquipped)
        {
            ApplyAffixBonuses();
        }
    }

}
