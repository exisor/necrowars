﻿using UnityEngine;
using System;
using System.Collections.Generic;
using System.Xml.Serialization;
using System.Linq;
using Common.Resources;

[Serializable]
public class Item
{
    public enum GenderType {Male, Female, Neutral, Plural};
    
    public long Id;
    public bool Generic;
    public Common.DTO.Rarity Rarity;
    [XmlIgnoreAttribute]
    public string NameKey;
    [XmlIgnoreAttribute]
    public List<string> DescriptionsKey;
    [XmlIgnoreAttribute]
    public GenderType NameGender;
    [XmlIgnoreAttribute]
    public Sprite[] Sprites;
    public List<ItemType> Types;
    public bool CanUse = true;
    public List<ItemAffixProbability> AffixProbabilities;
    public int StackingSize;
    public int SetId;
    public List<long> FixedAffixes;
    public CustomItemLogic CustomLogic;

    public string DescriptionKey { get{ return DescriptionsKey[0]; } }


    public ItemType? ItemSlot
    {
        get
        {
            if (!Types.Contains(ItemType.Equipment)) return null;
            return
                Types.FirstOrDefault(
                    t =>
                        t == ItemType.Body || t == ItemType.Boots || t == ItemType.Gloves || t == ItemType.Head ||
                        t == ItemType.Offhand || t == ItemType.Weapon || t == ItemType.VisualHelm);
        }
    }

    public Sprite DefaultSprite
    {
        get
        {
            if (Sprites.Length == 0) return null;
            return Sprites[0];
        }
    }
}
