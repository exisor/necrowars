﻿using UnityEngine;
using System.Collections;

public class GoldLogic : CustomItemLogic
{
    public override string GetShopDescription(ShopItemClient item)
    {
        if(item.ShopItem.Quantity < 5000000)
        {
            return Localization.GetString(item.ShopItemData.DescriptionsKey[1]);
        }
        else if (item.ShopItem.Quantity < 10000000)
        {
            return Localization.GetString(item.ShopItemData.DescriptionsKey[2]);
        }
        return Localization.GetString(item.ShopItemData.DescriptionsKey[3]);
    }

    public override Sprite GetShopSprite(ShopItemClient item)
    {
        if (item.ShopItem.Quantity < 5000000)
        {
            return item.ShopItemData.Sprites[1];
        }
        else if (item.ShopItem.Quantity < 10000000)
        {
            return item.ShopItemData.Sprites[2];
        }
        return item.ShopItemData.Sprites[3];
    }

}
