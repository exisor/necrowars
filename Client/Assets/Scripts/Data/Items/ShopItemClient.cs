﻿using UnityEngine;
using System.Collections;
using System;

public class ShopItemClient 
{
	public Item ShopItemData;
	public Common.DTO.ShopItem ShopItem;

    public event Action EventDiscountEnd;

	public ShopItemClient(Common.DTO.ShopItem shopItem)
	{
		ShopItem = shopItem;
        ShopItemData = ResourceManager.Instance.GetItemById(ShopItem.BaseId);

        DataManager.Instance.StartCoroutine(TimeTick());
    }

    public Sprite GetSprite()
    {
        if (ShopItemData.CustomLogic != null)
        {
            return ShopItemData.CustomLogic.GetShopSprite(this);
        }
        else if (ShopItemData.Sprites.Length != 0)
        {
            return ShopItemData.Sprites[0];
        }
        return null;
    }

    public string GetItemName()
    {
        return Localization.GetString(ShopItemData.NameKey);
    }

    public string GetItemDescription()
    {
        if(ShopItemData.CustomLogic != null)
        {
            return ShopItemData.CustomLogic.GetShopDescription(this);
        }
        else
        {
            return Localization.GetString(ShopItemData.DescriptionsKey[0]);
        }
    }

    private IEnumerator TimeTick()
    {
        while (ShopItem.ExpiresInMinutes > 0)
        {
            yield return new WaitForSeconds(60f);

            ShopItem.ExpiresInMinutes--;

            if (ShopItem.ExpiresInMinutes == 0 && EventDiscountEnd != null)
            {
                EventDiscountEnd();
            }
        }
    }

}
