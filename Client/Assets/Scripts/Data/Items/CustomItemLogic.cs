﻿using UnityEngine;
using System.Collections;

public class CustomItemLogic : ScriptableObject
{
    public virtual string GetShopDescription(ShopItemClient item)
    {
        return Localization.GetString(item.ShopItemData.DescriptionsKey[0]);
    }

    public virtual Sprite GetShopSprite(ShopItemClient item)
    {
        if (item.ShopItemData.Sprites.Length != 0)
        {
            return item.ShopItemData.Sprites[0];
        }
        return null;
    }

}
