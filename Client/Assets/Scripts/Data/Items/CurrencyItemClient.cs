﻿using UnityEngine;
using System.Collections;
using System.Globalization;

public class CurrencyItemClient
{
    public CurrencyItem ItemData;
    public Common.DTO.KongregateItem ItemKongDTO;
    public float Price;

    public CurrencyItemClient()
    {
    }
    public void SetKong(Common.DTO.KongregateItem kongItem)
    {
        ItemKongDTO = kongItem;
        ItemData = ResourceManager.Instance.GetKongItemById(kongItem.Identifier);
        Price = kongItem.Price;
    }

    public void SetOGZ(string item)
    {
        var info = item.Split(',');
        ItemData = ResourceManager.Instance.GetOGZItemById(info[0]);
        Price = float.Parse(info[1], CultureInfo.InvariantCulture.NumberFormat);
    }

    public string GetItemName()
    {
        return Localization.GetString(ItemData.NameKey);
    }

    public string GetItemDescription()
    {
        return Localization.GetString(ItemData.DescriptionKey);
    }
}
