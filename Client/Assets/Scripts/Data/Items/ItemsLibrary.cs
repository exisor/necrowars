﻿using System.Collections.Generic;
using System.Xml;
using System.IO;
using System.Xml.Serialization;
using UnityEngine;
using System;

public class ItemsLibrary : ScriptableObject 
{
    [System.Serializable]
    public struct ItemRarity
    {
        public Common.DTO.Rarity Rarity;
        public Sprite Sprite;
        public string NameKey;
        public Color Color;
    }

    public List<Item> Items;
    public List<CurrencyItem> CurrencyItems;
    public List<ItemRarity> Rarity;

    public string Save()
    {
        Debug.Log("Save Items");
        var serializer = new XmlSerializer(typeof(List<Item>));
        var stringWriter = new StringWriter();
        serializer.Serialize(stringWriter, Items);
        return stringWriter.ToString();
    }
}
