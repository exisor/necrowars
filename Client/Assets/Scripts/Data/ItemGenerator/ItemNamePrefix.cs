﻿using UnityEngine;
using System.Collections;
using System;

[Serializable]
public class ItemNamePrefix
{
	public string Female;
	public string Male;
	public string Neutral;
    public string Plural;
}
