﻿using System.Collections.Generic;
using UnityEngine;
using System;
using System.Linq;

public class ItemNamePrefixLibrary : ScriptableObject 
{
    public List<ItemNamePrefix> ItemNamePrefixs;

    public void Create(List<string> fileLines)
    {
        Debug.Log("fileLines");
    	ItemNamePrefixs = new List<ItemNamePrefix>();
        Debug.Log(fileLines[0][12]);
        foreach (string line in fileLines)
    	{
    		ItemNamePrefix prefix = new ItemNamePrefix();
            var words = line.Split('\t');
            
            int i = 0;
            foreach(string word in words)
            {
                if(word.Length > 1)
                {
                    if(i == 0)
                    {
                        prefix.Male = word.First().ToString().ToUpper() + word.Substring(1);
                    }
                    if (i == 1)
                    {
                        prefix.Female = word.First().ToString().ToUpper() + word.Substring(1);
                    }
                    if (i == 2)
                    {
                        prefix.Neutral = word.First().ToString().ToUpper() + word.Substring(1);
                    }
                    if (i == 3)
                    {
                        prefix.Plural = word.First().ToString().ToUpper() + word.Substring(1);
                    }
                    i++;
                }
            }
    		ItemNamePrefixs.Add(prefix);
    	}
    }
}
