﻿using System.Collections.Generic;
using UnityEngine;
using System;

public class ItemNameSuffixLibrary : ScriptableObject 
{
    public List<string> ItemNameSuffixs;

    public void Create(List<string> fileLines)
    {
    	ItemNameSuffixs = new List<string>();
    	foreach(string line in fileLines)
    	{
    		ItemNameSuffixs.Add(line);
    	}
    }
}
