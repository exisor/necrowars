﻿using System.Collections.Generic;
using UnityEngine;
using System;

public class SetLibrary : ScriptableObject
{
    public List<Set> Sets;
}
