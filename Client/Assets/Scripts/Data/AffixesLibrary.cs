﻿using System.Collections.Generic;
using UnityEngine;
using System;

public class AffixesLibrary : ScriptableObject 
{
    public List<Affix> Affixes;
}
