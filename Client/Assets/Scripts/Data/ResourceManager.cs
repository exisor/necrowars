﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Assets.Scripts;

public class ResourceManager : MonoBehaviour 
{
	public static ResourceManager Instance = null; 

	public AbilitiesLibrary AbilitiesLibrary;
	public CharactersLibrary CharactersLibrary;
	public ItemsLibrary ItemsLibrary;
	public PowerUpLibrary PowerUpLibrary;
	public ChestLibrary ChestLibrary;
	public AffixesLibrary AffixesLibrary;
    public AchievementLibrary AchievementLibrary;
    public QuestLibrary QuestLibrary;
    public PlayerEventLibrary PlayerEventLibrary;
    public SetLibrary SetLibrary;

    private Dictionary<long, Ability> _abilityDictionary = new Dictionary<long, Ability>();
	private Dictionary<long, Character> _characterDictionary = new Dictionary<long, Character>();
	private Dictionary<long, Item> _itemsDictionary = new Dictionary<long, Item>();
	private Dictionary<long, PowerUp> _powerUpsDictionary = new Dictionary<long, PowerUp>();
	private Dictionary<long, Chest> _chestsDictionary = new Dictionary<long, Chest>();
	private Dictionary<long, Affix> _affixesDictionary = new Dictionary<long, Affix>();
    private Dictionary<long, Achievement> _achievementsDictionary = new Dictionary<long, Achievement>();
    private Dictionary<long, Quest> _questsDictionary = new Dictionary<long, Quest>();
    private Dictionary<long, PlayerEvent> _eventsDictionary = new Dictionary<long, PlayerEvent>();
    private Dictionary<Common.DTO.Rarity, ItemsLibrary.ItemRarity> _rarityDictionary = new Dictionary<Common.DTO.Rarity, ItemsLibrary.ItemRarity>();
    private Dictionary<long, CurrencyItem> _currencyDictionary = new Dictionary<long, CurrencyItem>();
    private Dictionary<long, Set> _setsDictionary = new Dictionary<long, Set>();

    void Awake () 
	{
		Instance = this;

		foreach (Ability ability in AbilitiesLibrary.Abilities)
        {
            _abilityDictionary[ability.Id] = ability;
        }

        foreach (Character character in CharactersLibrary.Characters)
        {
            _characterDictionary[character.Id] = character;
        }

        foreach (Item item in ItemsLibrary.Items)
        {
            _itemsDictionary[item.Id] = item;
        }

        foreach (CurrencyItem currencyItem in ItemsLibrary.CurrencyItems)
        {
            _currencyDictionary[currencyItem.InternalId] = currencyItem;
        }

        foreach (ItemsLibrary.ItemRarity rarity in ItemsLibrary.Rarity)
        {
            _rarityDictionary[rarity.Rarity] = rarity;
        }

        foreach (PowerUp powerUp in PowerUpLibrary.PowerUps)
        {
            _powerUpsDictionary[powerUp.Id] = powerUp;
        }

        foreach (Chest chest in ChestLibrary.Chests)
        {
            _chestsDictionary[chest.Id] = chest;
        }

        foreach (Affix affix in AffixesLibrary.Affixes)
        {
            _affixesDictionary[affix.Id] = affix;
        }

        foreach (Achievement achievement in AchievementLibrary.Achievements)
        {
            _achievementsDictionary[achievement.Id] = achievement;
        }

        foreach (Quest quest in QuestLibrary.Quests)
        {
            _questsDictionary[quest.Id] = quest;
        }

        foreach (PlayerEvent playerEvent in PlayerEventLibrary.Events)
        {
            _eventsDictionary[playerEvent.Id] = playerEvent;
        }

        foreach (Set set in SetLibrary.Sets)
        {
            _setsDictionary[set.Id] = set;
        }
    }

	public Ability GetAbilityById(long id)
	{
		return _abilityDictionary.Get(id);
    }

	public Character GetCharacterById(long id)
	{
		return _characterDictionary.Get(id);
    }

	public Item GetItemById(long id)
	{
		return _itemsDictionary.Get(id);
    }

    public CurrencyItem GetKongItemById(string id)
    {
        return _currencyDictionary.Values.ToList().Find(a => a.KongId == id);
    }

    public CurrencyItem GetOGZItemById(string id)
    {
        return _currencyDictionary.Values.ToList().Find(a => a.OGZId == id);
    }

    public ItemsLibrary.ItemRarity GetRarity(Common.DTO.Rarity rarity)
    {
        return _rarityDictionary.Get(rarity);
    }

    public PowerUp GetPowerUpById(long id)
	{
		return _powerUpsDictionary.Get(id);
    }

	public Chest GetChestById(long id)
	{
		return _chestsDictionary.Get(id);
    }

	public Affix GetAffixById(long id)
	{
	    return _affixesDictionary.Get(id);
	}

    public Quest GetQuestById(long id)
    {
        return _questsDictionary.Get(id);
    }

    public PlayerEvent GetPlayerEventById(long id)
    {
        return _eventsDictionary.Get(id);
    }

    public Set GetSetById(long id)
    {
        return _setsDictionary.Get(id);
    }

    public Achievement GetAchievementById(long id)
    {
        return _achievementsDictionary.Get(id);
    }

    public List<Chest> GetAllChests()
	{
		return _chestsDictionary.Values.ToList();
	}

    public List<Achievement> GetAllAchievements()
    {
        return _achievementsDictionary.Values.ToList();
    }
}
