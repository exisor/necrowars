﻿using System.Collections.Generic;
using System.Xml;
using System.IO;
using System.Xml.Serialization;
using UnityEngine;
using System;

public class CharactersLibrary : ScriptableObject 
{
    public List<Character> Characters;

    public string Save()
    {
        Debug.Log("Save Characters");
        var serializer = new XmlSerializer(typeof(List<Character>));
        var stringWriter = new StringWriter();
        serializer.Serialize(stringWriter, Characters);
        return stringWriter.ToString();
    }

}
