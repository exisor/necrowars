﻿using UnityEngine;
using System;


[Serializable]
public class Quest
{
    public long Id;
    public string NameKey;
    public string DescriptionKey;

    public long Threshold;

}
