﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

public class ClientAffixResourceManager : Common.ResourceManager.AffixResourceManager
{

    public override void Load()
    {
        List<Common.Resources.Affix> loadedResources = Common.XmlSerializer.Deserialize<List<Common.Resources.Affix>>(DataManager.Instance.AffixXML.text);
        if (loadedResources != null)
        {
            _resources = loadedResources;
            _indexedResources = All
                .ToDictionary(item => item.Id, item => item);
        }
    }
}
