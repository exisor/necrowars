﻿using UnityEngine;
using System.Collections;

public class CustomAbilityTooltip : ScriptableObject
{
    public virtual string GetTooltip(AbilityClient ability)
    {
        return "";
    }

    public virtual string GetUpgradeTooltip(AbilityClient ability)
    {
        return "";
    }

}
