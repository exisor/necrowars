﻿using UnityEngine;
using System.Collections;

public class TinitusTooltip : CustomAbilityTooltip
{
    public override string GetTooltip(AbilityClient ability)
    {
        int chance = (int)((ability.Ability.Values[0].Value + (ability.Ability.Values[1].Value * ability.Level)) * 100);
        return string.Format(Localization.GetString(ability.Ability.DescriptionKey), chance.ToString("F2"));
    }

    public override string GetUpgradeTooltip(AbilityClient ability)
    {
        int chance = (int)((ability.Ability.Values[0].Value + (ability.Ability.Values[1].Value * ability.Level)) * 100);
        int chanceNew = (int)((ability.Ability.Values[0].Value + (ability.Ability.Values[1].Value * (ability.Level + 1))) * 100);
        return string.Format(Localization.GetString(ability.Ability.DescriptionUpgradeKey), chance.ToString("F2"), chanceNew.ToString("F2"));
    }

}
