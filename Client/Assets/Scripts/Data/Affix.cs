﻿using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;
using System.Linq;

[Serializable]
public class Affix
{
    public long Id;
    public string DescriptionKey;
    public string EnchantDescriptionKey;
    public List<string> Values;
    public CustomAffixTooltip CustomLogic;
    public bool CanEnchant = true;
    public bool isBaseAffix;
    public bool isMultiplicative;
    public bool isDecrease;
}
