﻿using UnityEngine;
using System.Collections;
using System;

[Serializable]
public class LocalizationData
{
    public string Key;
    [Multiline]
    public string RU;
    [Multiline]
    public string ENG;

    public LocalizationData Clone()
    {
        LocalizationData data = new LocalizationData();
        data.Key = Key;
        data.RU = RU;
        data.ENG = ENG;
        return data;
    }
}
