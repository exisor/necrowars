﻿using UnityEngine;
using System.Linq;
using System.Collections.Generic;

public class CustomAffixTooltip : ScriptableObject
{
    public virtual string GetTooltip<T>(AffixClient affix, IList<T> values, int enchant, string key)
        where T : struct 
    {
        return "";
    }

    public virtual bool IsValid<T>(IEnumerable<T> values)
        where T : struct
    {
        return values.Any(value => !value.Equals(default(T)));
    }
    
    public virtual List<int> CombineValues(List<int> values1, List<int> values2)
    {
        var values = new List<int>();
        for (int i = 0; i < values1.Count; i++)
        {
            values[i] = values1[i] + values2[i];
        }
        return values;
    }

}
