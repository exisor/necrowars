﻿using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

public class AddModToAbilityTooltip : CustomAffixTooltip
{

    public override string GetTooltip<T>(AffixClient affix, IList<T> values, int enchant, string key)
    {
        // assuming that second value is ability Id and first is value bonus
        var ability = ResourceManager.Instance.GetAbilityById(Convert.ToInt64(values[1]));
        // third can be enchant tooltip
        var valuesString = new object[] {values[0], Localization.GetString(ability.NameKey), values.Count > 2 ? values[2] : default(T)};
        return string.Format(Localization.GetString(key), valuesString);
    }
    public override bool IsValid<T>(IEnumerable<T> values) 
    {
        return !values.First().Equals(default(T));
    }

    public override List<int> CombineValues(List<int> values1, List<int> values2)
    {
        var values = new List<int>();
        values[0] = values1[0] * values2[0];
        values[1] = values1[1];
        return values;
    }
}
