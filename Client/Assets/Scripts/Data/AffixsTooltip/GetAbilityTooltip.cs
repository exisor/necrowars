﻿using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class GetAbilityTooltip : CustomAffixTooltip
{
    public override string GetTooltip<T>(AffixClient affix, IList<T> values, int enchant, string key)
    {
        Ability ability = ResourceManager.Instance.GetAbilityById(Convert.ToInt64(values[0]));
        return string.Format(Localization.GetString(key), Localization.GetString(ability.NameKey));
    }

    public override bool IsValid<T>(IEnumerable<T> values)
    {
        return true;
    }

    public override List<int> CombineValues(List<int> values1, List<int> values2)
    {
        return values1;
    }
}
