﻿using System.Collections.Generic;
using System.Xml;
using System.IO;
using System.Xml.Serialization;
using UnityEngine;
using System;

public class PlayerEventLibrary : ScriptableObject
{
    public List<PlayerEvent> Events;
}
