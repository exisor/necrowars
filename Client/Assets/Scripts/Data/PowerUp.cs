﻿using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;
using System.Linq;

[Serializable]
public class PowerUp
{
    public long Id;
    public string NameKey;
    public string DescriptionKey;
    public Sprite Sprite;
}
