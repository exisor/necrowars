﻿using System.Collections.Generic;
using System.Xml;
using System.IO;
using System.Xml.Serialization;
using UnityEngine;
using System;

public class AbilitiesLibrary : ScriptableObject 
{
    public TextAsset AbilitiesXML;
    public List<Ability> Abilities;

    public string Save()
    {
        Debug.Log("Save Abilities");
        var serializer = new XmlSerializer(typeof(List<Ability>));
        var stringWriter = new StringWriter();
        serializer.Serialize(stringWriter, Abilities);
        return stringWriter.ToString();
    }

}
