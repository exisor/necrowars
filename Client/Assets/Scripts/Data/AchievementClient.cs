﻿using UnityEngine;
using System.Collections;

public class AchievementClient
{
    public Achievement AchievementData;
    public Common.DTO.Achievement Achievement;

    public AchievementClient(Achievement achievementData, Common.DTO.Achievement achievement)
    {
        Achievement = achievement;
        AchievementData = achievementData;
    }

    public bool IsComplete()
    {
        if (Achievement == null)
        {
            return false;
        }
        for (int i = 0; i < AchievementData.Steps.Count; i++)
        {
            if (Achievement.Value < AchievementData.Steps[i].Threshold)
            {
                return false;
            }
        }
        return true;
    }

    public Achievement.AchievementStep GetCurrentStep()
    {
        if (Achievement == null)
        {
            return AchievementData.Steps[0];
        }
        for (int i = 0; i < AchievementData.Steps.Count; i++)
        {
            if (Achievement.Value < AchievementData.Steps[i].Threshold)
            {
                return AchievementData.Steps[i];
            }
        }
        return AchievementData.Steps[AchievementData.Steps.Count - 1];
    }

    public string GetTierString()
    {
        if(Achievement == null)
        {
            return Localization.GetString("GUI_ACHIV_TIER") + " 1";
        }
        else
        {
            for(int i = 0; i < AchievementData.Steps.Count; i++)
            {
                if (Achievement.Value < AchievementData.Steps[i].Threshold)
                {
                    return Localization.GetString("GUI_ACHIV_TIER") + " " + (i + 1).ToString();
                }
            }
            return "";
        }
    }

    public string GetProgressString()
    {
        if (Achievement == null)
        {
            return "";
        }
        else
        {
            for (int i = 0; i < AchievementData.Steps.Count; i++)
            {
                if (Achievement.Value < AchievementData.Steps[i].Threshold)
                {
                    return " " + Localization.GetString("GUI_ACHIV_PROGRESS") + " " + Achievement.Value.ToString() + " / " + AchievementData.Steps[i].Threshold.ToString();
                }
            }
            return "";
        }
    }

}
