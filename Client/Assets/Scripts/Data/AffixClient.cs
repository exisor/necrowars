﻿using System;
using System.Collections;
using UnityEngine;
using System.Linq;
using System.Collections.Generic;
using Common.Resources;
using Common.DTO;

public class AffixClient 
{
    public Affix AffixData;
    public ItemAffix AffixDTO;
    public List<int> Values;
    public List<double> DoubleValues;

    public string Tooltip;
    public string EnchantTooltip;
    public bool isVisible = true;

    private IAffixValues _values;

    public AffixClient(Affix affixData, ItemAffix affixDTO, IAffixValues values)
    {
        _values = values;
        AffixData = affixData;
        AffixDTO = affixDTO;

        Values = AffixData.Values.Select(v => values.GetValue(v)).ToList();
        DoubleValues = AffixData.Values.Select(v => values.GetFloatValue(v)).ToList();
        isVisible = IsValid(DoubleValues);

        List<IAffixValues> valuesList = new List<IAffixValues> { values };

        Tooltip = GetTooltip(valuesList, AffixDTO.Enchant, AffixData.DescriptionKey);

        IAffixValues enchantValues = _values.DeepClone();
        enchantValues.Enchant++;
        valuesList.Add(enchantValues);

        EnchantTooltip = GetTooltip(valuesList, AffixDTO.Enchant, AffixData.EnchantDescriptionKey);
    }

    public bool IsValid<T>(IEnumerable<T> values)
        where T : struct
    {
        return AffixData.CustomLogic == null ? 
            values.Any(value => !value.Equals(default(T)))
            : AffixData.CustomLogic.IsValid(values);
    }

    public string GetAffixStringByValues(List<int> valuesInt)
    {
        return GetAffixString(valuesInt, AffixDTO.Enchant, AffixData.DescriptionKey);
    }

    public List<int> CombineValues(List<int> values1, List<int> values2)
    {
        var values = new List<int>();
        if (AffixData.CustomLogic != null)
        {
            return AffixData.CustomLogic.CombineValues(values1, values2);
        }
        for (int i = 0; i < values1.Count; i++)
        {
            if(AffixData.isMultiplicative)
            {
                values[i] = values1[i] * values2[i];
            }
            else
            {
                values[i] = values1[i] + values2[i];
            }
            
        }
        return values;
    }

    private string GetTooltip(IEnumerable<IAffixValues> valuesList, int enchant, string key)
    {
        var values = new List<double>();
        foreach (var affixValues in valuesList)
        {
            values = values.Concat(AffixData.Values.Select(v => affixValues.GetFloatValue(v))).ToList();
        }
        
        return GetAffixString(values, enchant, key);
    }

    private string GetAffixString<T>(IList<T> values, int enchant, string key)
        where T : struct
    {
        string text = "";
        if (AffixData.CustomLogic == null)
        {
            text = string.Format(Localization.GetString(key), values.Select(v => (object)v).ToArray());
            if (!AffixData.isBaseAffix)
            {
                text += AffixDTO.Enchant == 0 ? "" : " +" + enchant;
            }
        }
        else
        {
            text = AffixData.CustomLogic.GetTooltip(this, values, enchant, key);
        }
        return text;
    }
}
