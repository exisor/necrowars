﻿using UnityEngine;
using System.Collections;
using System;

public class PowerUpClient 
{
	public PowerUp PowerUpData;
	public Common.DTO.PowerUp PowerUp;

	public PowerUpClient(Common.DTO.PowerUp powerUp)
	{
		PowerUp = powerUp;
		PowerUpData = ResourceManager.Instance.GetPowerUpById(PowerUp.BaseId);

        DataManager.Instance.StartCoroutine(TimeTick());
    }

    private IEnumerator TimeTick()
    {
        while (PowerUp.Expires.TotalSeconds > 0)
        {
            yield return new WaitForSeconds(1f);

            PowerUp.Expires = PowerUp.Expires.Add(TimeSpan.FromSeconds(-1));
        }
    }

}
