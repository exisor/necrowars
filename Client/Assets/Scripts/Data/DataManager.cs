﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

public class DataManager : MonoBehaviour 
{
	public static DataManager Instance = null;

    public enum SocialType { None, Kong, VK, OK, FB, MM };

    public List<CharacterClient> Characters = new List<CharacterClient>();
	public Dictionary<long, ShopItemClient> ShopItems = new Dictionary<long, ShopItemClient>();
    public Dictionary<long, CurrencyItemClient> CurrencyItems = new Dictionary<long, CurrencyItemClient>();
    public Dictionary<long, ItemClient> Items = new Dictionary<long, ItemClient>();
	public Dictionary<long, PowerUpClient> PowerUps = new Dictionary<long, PowerUpClient>();
    public Dictionary<long, AchievementClient> Achievements = new Dictionary<long, AchievementClient>();

    public CharacterClient CurrentCharacter;
    public QuestClient CurrentQuest;

    public string BattleToken;
    public SocialType Social = SocialType.None;
    public string networkAddress;
    public int networkPort;
	public string UserName;
    public Sprite UserAvatar;
    public long Gold;
    public long Currency;
    public int CurrencyBought;
    public int TutorialPhase;
    public int InventorySize;

    public TextAsset AffixXML;
    public ClientAffixResourceManager AffixManager;

    public bool IsOGZ
    {
        get 
        {
            return Social == SocialType.VK || Social == SocialType.OK || Social == SocialType.FB || Social == SocialType.MM;
        }
    }
	
	void Awake () 
	{
		Instance = this;

        AffixManager = new ClientAffixResourceManager();
	}

    public void SetShopItems(List<Common.DTO.ShopItem> items)
    {
        ShopItems = new Dictionary<long, ShopItemClient>();
        foreach (Common.DTO.ShopItem shopItem in items)
        {
            ShopItemClient shopItemClient = new ShopItemClient(shopItem);
            ShopItems[shopItem.Id] = shopItemClient;
        }
    }

    public void SetKongItems(List<Common.DTO.KongregateItem> items)
    {
        CurrencyItems = new Dictionary<long, CurrencyItemClient>();
        foreach (Common.DTO.KongregateItem item in items)
        {
            CurrencyItemClient itemClient = new CurrencyItemClient();
            itemClient.SetKong(item);
            CurrencyItems[itemClient.ItemData.InternalId] = itemClient;
        }
    }

    public void SetOGZItems(string items)
    {
        CurrencyItems = new Dictionary<long, CurrencyItemClient>();
        var info = items.Split('|');
        foreach (var item in info)
        {
            CurrencyItemClient itemClient = new CurrencyItemClient();
            itemClient.SetOGZ(item);
            CurrencyItems[itemClient.ItemData.InternalId] = itemClient;
        }
    }

    public void SetPlayer(Common.DTO.Player player)
	{
		UserName = player.Username;
		Gold = player.Gold;
        Currency = player.Currency;
        CurrencyBought = player.CurrencyBought;
        InventorySize = player.InventorySize;
        TutorialPhase = player.TutorialPhase;

        Tutorial.SetPhase(TutorialPhase);

        SetQuest(player.DailyQuestId, player.QuestExpireTime, player.DailyQuestProgress);

        foreach (Common.DTO.Item item in player.Items)
        {
            AddItem(item);
        }

        foreach (Common.DTO.PowerUp powerUp in player.PowerUps)
        {
            AddPowerUp(powerUp);
        }

        foreach (Achievement achievementData in ResourceManager.Instance.GetAllAchievements())
        {
            AddAchievement(achievementData, player.Achievements.Find(a => a.Id == achievementData.Id));
        }

        foreach (Common.DTO.Character character in player.Characters)
        {
        	AddCharacter(character);
        }

        if(Characters.Count > 0)
        {
            CurrentCharacter = Characters[0];
            Debug.Log(GetCurrentCharacterId());
        }
	}

	public long GetCurrentCharacterId()
	{
	    return CurrentCharacter.Id;
	}

    public void SetQuest(long id, int time = 0, long progress = 0)
    {
        CancelInvoke();

        if (id == 0)
        {
            CurrentQuest = null;
        }
        else
        {
            CurrentQuest = new QuestClient(id, time, progress);
        }
    }

    public ItemClient AddItem(Common.DTO.Item item)
	{
		ItemClient itemClient = new ItemClient(item);
        Items[item.Id] = itemClient;
		return itemClient;
	}

    public AbilityClient AddAbility(Common.DTO.Ability ability)
    {
        return CurrentCharacter.AddAbility(ability);
    }

    public void LostAbility(Common.DTO.Ability ability)
    {
        CurrentCharacter.LostAbility(ability);
    }

    public PowerUpClient AddPowerUp(Common.DTO.PowerUp powerUp)
	{
		PowerUpClient powerUpClient = new PowerUpClient(powerUp);
        PowerUps[powerUp.Id] = powerUpClient;
        return powerUpClient;
	}

    public AchievementClient AddAchievement(Achievement achievementData, Common.DTO.Achievement achievement = null)
    {
        AchievementClient achievementClient = new AchievementClient(achievementData, achievement);
        Achievements[achievementData.Id] = achievementClient;
        return achievementClient;
    }

    public PowerUpClient RemovePowerUp(long powerUpId)
    {
        Debug.Log(powerUpId);
        var powerUp = PowerUps[powerUpId];
        PowerUps.Remove(powerUpId);
        Debug.Log(GetAllPowerUps().Count);
        return powerUp;
    }

    public CharacterClient AddCharacter(Common.DTO.Character character)
    {
        CharacterClient characterClient = new CharacterClient();
		Characters.Add(characterClient);
        if (CurrentCharacter == null)
        {
            CurrentCharacter = characterClient;
        }
        characterClient.Init(character);
		return characterClient;
	}

	public CharacterClient GetCharacter(long CharacterId)
	{
		return Characters.Find(c => c.Id == CharacterId);
	}

	public List<AbilityClient> GetAllAbilities()
	{
		return CurrentCharacter.Abilities.Values.ToList();
	}

	public List<ShopItemClient> GetAllShopItems()
	{
		return ShopItems.Values.ToList();
	}

    public List<CurrencyItemClient> GetAllCurrencyItems()
    {
        return CurrencyItems.Values.ToList();
    }

    public List<AchievementClient> GetAllAchievements()
    {
        return Achievements.Values.ToList();
    }

    public AchievementClient GetAchievementById(long achievementId)
    {
        return Achievements[achievementId];
    }

    public ShopItemClient GetShopItemByBaseId(long itemId)
    {
        return ShopItems.Values.FirstOrDefault(item => item.ShopItemData.Id == itemId);
    }

    public List<ItemClient> GetAllItems()
	{
		return Items.Values.ToList();
	}

    public List<ItemClient> GetItemsByTypes(List<Common.Resources.ItemType> types)
    {
        return Items.Values.Where(item => item.ItemData.Types.Intersect(types).Any()).ToList();
    }

    public List<ItemClient> GetEquipItemsBySetId(long setId)
    {
        return GetAllEquipedItems().Where(item => item.ItemData.SetId == setId).ToList();
    }

    public List<ItemClient> GetAllUnequipedItems()
    {
        return Items.Values.Where(item => !item.Equiped && !item.ItemData.Types.Contains(Common.Resources.ItemType.Instant)).ToList();
    }

    public List<ItemClient> GetAllEquipedItems()
    {
        return Items.Values.Where(item => item.Equiped).ToList();
    }

    public AbilityClient GetAbility(long abilityId)
	{
        if(CurrentCharacter.Abilities.ContainsKey(abilityId))
        {
            return CurrentCharacter.Abilities[abilityId];
        }
		return null;
	}

	public AbilityClient GetSelectedAbility(int slotId)
	{
        return CurrentCharacter.SelectedAbilities[slotId];
    }

    public void SelectAbility(int slotId, AbilityClient ability)
    {
        CurrentCharacter.SelectedAbilities[slotId] = ability;
    }

    public ItemClient GetItem(long itemId)
	{
        if(Items.ContainsKey(itemId))
        {
            return Items[itemId];
        }
        return null;
    }

    public ItemClient GetItemByBaseId(long itemId)
    {
        return Items.Values.FirstOrDefault(item => item.ItemData.Id == itemId);
    }

    public PowerUpClient GetPowerUp(long powerUpId)
	{
		if(PowerUps.ContainsKey(powerUpId))
		{
			return PowerUps[powerUpId];
		}
		return null;
	}

    public List<PowerUpClient> GetAllPowerUps()
    {
        return PowerUps.Values.ToList();
    }
}
