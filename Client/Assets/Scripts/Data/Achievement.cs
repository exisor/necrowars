﻿using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;
using System.Linq;

[Serializable]
public class Achievement
{
    public long Id;
    public string NameKey;
    public string DescriptionKey;
    public bool ShowProgress = true;

    public List<AchievementStep> Steps;

    [Serializable]
    public struct AchievementStep
    {
        public int Threshold;
        public long RewardId;
        public int Quantity;
    }
}
