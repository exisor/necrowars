﻿using UnityEngine;
using System.Collections;
using System;

[Serializable]
public class Chest
{
    public long Id;
    public string NameKey;
    public string DescriptionKey;
    public Sprite Sprite;
    public int UnitsRequirement;
    public Color RarityColor;

    public long GetPointsRequirement(int level)
    {
        return Common.Util.GetChestPointsForLevel(level, Id);
    }
}
