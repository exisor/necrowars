﻿using UnityEngine;
using System.Collections;

public class QuestClient
{
    public Quest QuestData;
    public long DailyQuestProgress;
    public int QuestExpireTime;

    public QuestClient(long id, int time, long progress = 0)
    {
        QuestData = ResourceManager.Instance.GetQuestById(id);
        DailyQuestProgress = progress;
        QuestExpireTime = time;

        DataManager.Instance.StartCoroutine(TimeTick());
    }

    private IEnumerator TimeTick()
    {
        while (QuestExpireTime > 0)
        {
            yield return new WaitForSeconds(1f);

            QuestExpireTime--;
        }
    }
}
