﻿using UnityEngine;
using System;
using System.Collections.Generic;
using System.Xml.Serialization;
using System.Linq;
using Common.Resources;

[Serializable]
public class PlayerEvent
{
    public long Id;
    [XmlIgnoreAttribute]
    public string NameKey;
    [XmlIgnoreAttribute]
    public string DescriptionKey;
}
