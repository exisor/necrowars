﻿using System.Collections.Generic;
using UnityEngine;
using System;

public class QuestLibrary : ScriptableObject
{
    public TextAsset QuestsXML;
    public List<Quest> Quests;
}
