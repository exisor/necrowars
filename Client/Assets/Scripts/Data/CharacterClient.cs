﻿using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Common.Resources;
using Assets.Scripts.Attributes;

public class CharacterClient
{
    
	public Character CharacterData;
    [SerializeField]
	private Common.DTO.Character _character;

    public long Id { get { return _character.Id; } }
    public int Level
    {
        get
        {
            return _character.Level;
        }
        set
        {
            if (_character.Level == value) return;
            _character.Level = value;
            OnLevelChanged(value);
        }
    }

    public int AP
    {
        get { return _character.AP; }
        set { _character.AP = value; }
    }

    public long Experience
    {
        get { return _character.Experience; }
        set { _character.Experience = value; }
    }

    public long RequiredExperience
    {
        get { return _character.RequiredExperience; }
        set { _character.RequiredExperience = value; }
    }

    public Dictionary<long, AbilityClient> Abilities = new Dictionary<long, AbilityClient>();
	public Dictionary<long, AbilityClient> SelectedAbilities = new Dictionary<long, AbilityClient>();

    public Dictionary<ItemType?, ItemClient> EquippedItems = new Dictionary<ItemType?, ItemClient>();

	public CharacterClient()
	{
        MaxHP = new FloatAttribute(1000);
	    MaxHP += _hpBonus;

        Armor = new ArmorAttribute();

        MinDamage = new MinDamageAttribute();
	    MinDamage += _damageBonus;
        MaxDamage = new MaxDamageAttribute();
        MaxDamage += _damageBonus;

        AttackSpeed = new AttackSpeedAttribute(2f);
        SpellDamage = new MultiplierAttribute();
        HPRegen = new FloatAttribute();
    }
    // moved to init because all stats data need to have links to abilities & items & stats when applying
    public void Init(Common.DTO.Character character)
    {
        if (_character != null) throw new Exception("Character is already initialized");

        _character = character;
        OnLevelChanged(Level);
        CharacterData = ResourceManager.Instance.GetCharacterById(_character.BaseId);


        foreach (Common.DTO.Ability ability in character.Abilities)
        {
            AddAbility(ability);
        }

        foreach (long itemId in _character.EquippedItems)
        {
            ItemClient item = DataManager.Instance.GetItem(itemId);
            Equip(item);
        }

        foreach (var selectAbility in character.AbilitySlots)
        {
            if (Abilities.ContainsKey(selectAbility.AbilityId))
            {
                SelectedAbilities[selectAbility.Slot] = Abilities[selectAbility.AbilityId];
            }
            else
            {
                SelectedAbilities[selectAbility.Slot] = null;
            }
        }

    }
    public AbilityClient AddAbility(Common.DTO.Ability ability)
    {
        AbilityClient abilityClient = new AbilityClient(ability);
        Abilities[abilityClient.Ability.Id] = abilityClient;
        return abilityClient;
    }

    public void LostAbility(Common.DTO.Ability ability)
    {
        Abilities.Remove(ability.Id);
    }

    public ItemClient GetEquipByType(ItemType? type)
    {
        if (EquippedItems.ContainsKey(type))
        {
            return EquippedItems[type];
        }
        return null;
    }

    public void Equip(ItemClient item)
    {
        item.Equiped = true;

        EquippedItems[item.ItemData.ItemSlot] = item;
    }

    public void UnEquip(ItemClient item)
    {
        item.Equiped = false;

        EquippedItems.Remove(item.ItemData.ItemSlot);
    }

    public FloatAttribute MaxHP { get; set; }
    public ArmorAttribute Armor { get; set; }
    public MinDamageAttribute MinDamage { get; set; }
    public MaxDamageAttribute MaxDamage { get; set; }
    public AttackSpeedAttribute AttackSpeed { get; set; }

    // SpellDamage is multiplier
    // 1.0 means 100% damage 1.05% means 5% increase etc
    public MultiplierAttribute SpellDamage { get; set; }
    public FloatAttribute HPRegen { get; set; }


    private readonly StraightLevelRelBonus _hpBonus = new StraightLevelRelBonus(0.01f);
    private readonly StraightLevelRelBonus _damageBonus = new StraightLevelRelBonus(0.01f);

    protected void OnLevelChanged(int level)
    {
        _hpBonus.Level = level - 1;
        _damageBonus.Level = level - 1;
    }
}
