﻿using System.Collections.Generic;
using UnityEngine;
using System;

public class ChestLibrary : ScriptableObject 
{
    public List<Chest> Chests;
}
