﻿using UnityEngine;
using System.Collections.Generic;
using System;

[Serializable]
public class Set
{
    [Serializable]
    public class Bonus
    {
        public int AffixId;
        public int ItemsCount;
    }

    public long Id;
    public string NameKey;
    public int ItemsCount = 5;

    public List<Bonus> Bonuses;
}
