﻿using UnityEngine;
using Spine.Unity;

public class InvisbleSlotsPoseBehaviour : StateMachineBehaviour
{
    public string[] InvisibleSlotNames;

    public override void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        var skeletonAnimator = animator.gameObject.GetComponent<SkeletonAnimator>();
        skeletonAnimator.skeleton.SetToSetupPose();
        SetColor(0, skeletonAnimator);
    }

    public override void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        var skeletonAnimator = animator.gameObject.GetComponent<SkeletonAnimator>();
        skeletonAnimator.skeleton.SetToSetupPose();
        SetColor(1, skeletonAnimator);
    }

    private void SetColor(int alpha, SkeletonAnimator skeletonAnimator)
    {
        foreach (string slotname in InvisibleSlotNames)
        {
            foreach (Spine.Slot slot in skeletonAnimator.skeleton.slots)
            {
                if (slotname.Equals(slot.data.name))
                {
                    slot.A = alpha;
                }
            }
        }
    }

}