﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using System.Linq;
using UnityEngine.Events;

public class Localization : MonoBehaviour 
{
	public enum LanguageEnum {Ru, Eng};

	public static Localization Instance = null;

    public TextAsset NicksEn;
    public TextAsset NicksRu;

    public LocalizationLibrary[] Librarys;

	public ItemNamePrefixLibrary ItemNamePrefixLibraryENG;
	public ItemNamePrefixLibrary ItemNamePrefixLibraryRU;

	public ItemNameSuffixLibrary ItemNameSuffixLibraryENG;
	public ItemNameSuffixLibrary ItemNameSuffixLibraryRU;


	public LanguageEnum CurrentLanguage;
    public UnityEvent EventChangeLanguage;

    private Dictionary<LanguageEnum, Dictionary<string, string>> _localizationDictionary = new Dictionary<LanguageEnum, Dictionary<string, string>>();

	private Dictionary<LanguageEnum, List<ItemNamePrefix>> _itemsPreDictionary = new Dictionary<LanguageEnum, List<ItemNamePrefix>>();
	private Dictionary<LanguageEnum, List<string>> _itemsSufDictionary = new Dictionary<LanguageEnum, List<string>>();
    private Dictionary<LanguageEnum, List<string>> _nicks = new Dictionary<LanguageEnum, List<string>>();

    void Awake () 
	{
		Instance = this;

		foreach(LanguageEnum Language in Enum.GetValues(typeof(LanguageEnum)))
		{
			_localizationDictionary[Language] = new Dictionary<string, string>();
			foreach (LocalizationLibrary library in Librarys)
	        {
	            foreach (LocalizationData data in library.Datas)
		        {
		        	if(Language == LanguageEnum.Ru)
		        	{
		        		_localizationDictionary[Language][data.Key] = data.RU;
		        	}
		            else
		            {
		            	_localizationDictionary[Language][data.Key] = data.ENG;
		            }
		        }
	        }

	        if(Language == LanguageEnum.Ru)
		    {
		        _itemsPreDictionary[Language] = ItemNamePrefixLibraryRU.ItemNamePrefixs;
		        _itemsSufDictionary[Language] = ItemNameSuffixLibraryRU.ItemNameSuffixs;
                _nicks[Language] = CreateNicks(NicksRu);
		    }
		    else
		    {
		       	_itemsPreDictionary[Language] = ItemNamePrefixLibraryENG.ItemNamePrefixs;
		        _itemsSufDictionary[Language] = ItemNameSuffixLibraryENG.ItemNameSuffixs;
                _nicks[Language] = CreateNicks(NicksEn);
            }
		}
	}

    public static void SetCurrentLanguage(LanguageEnum language)
    {
        Instance.SetCurrentLanguageInternal(language);
    }

    public static void AddChangeLanguageCallback(UnityAction callback)
    {
        Instance.EventChangeLanguage.AddListener(callback);
    }

    public static string GetString(string key)
	{
		return Instance.GetStringInternal(key);
	}

	public static string GetItemString(int seed, string key, Item.GenderType gender)
	{
		return Instance.GetItemStringInternal(seed, key, gender);
	}

    public void SetCurrentLanguageInternal(LanguageEnum language)
    {
        if (CurrentLanguage != language)
        {
            CurrentLanguage = language;
            EventChangeLanguage.Invoke();
        }
    }

    public void SetLanguage(LanguageEnum language)
	{
		CurrentLanguage = language;
	}

	public string GetStringInternal(string key)
	{
		if(_localizationDictionary[CurrentLanguage].ContainsKey(key))
		{
			return _localizationDictionary[CurrentLanguage][key];
		}
		return "###" + key;
	}

	public string GetItemStringInternal(int seed, string key, Item.GenderType gender)
	{
		var random = new System.Random(seed);

		string mainString = GetStringInternal(key);
		string suffixString = _itemsSufDictionary[CurrentLanguage][random.Next(0, _itemsSufDictionary[CurrentLanguage].Count)];

		var prefix = _itemsPreDictionary[CurrentLanguage][random.Next(0, _itemsPreDictionary[CurrentLanguage].Count)];

		string prefixString = prefix.Male;
		if(gender == Item.GenderType.Female) prefixString = prefix.Female;
		else if(gender == Item.GenderType.Neutral) prefixString = prefix.Neutral;
        else if (gender == Item.GenderType.Plural) prefixString = prefix.Plural;

        return prefixString + " " + mainString + " " + suffixString;
	}

    public string GetNick()
    {
        return _nicks[CurrentLanguage][UnityEngine.Random.Range(0, _nicks[CurrentLanguage].Count)];
    }

    private List<string> CreateNicks(TextAsset nicksAsset)
    {
        return nicksAsset.text.Split("\n"[0]).ToList();

        /*
        for(int i = 0, n = nicks.Count; i < n; i++)
        {
            _nicks.Add(nicks[i]);
            
            _nicks.Add(nicks[i].ToLower());
            _nicks.Add(nicks[i] + UnityEngine.Random.Range(1, 100).ToString());
            _nicks.Add(nicks[i].ToLower() + UnityEngine.Random.Range(1, 100).ToString());
        }*/
    }
}
