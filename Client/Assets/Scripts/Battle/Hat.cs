﻿using UnityEngine;
using DG.Tweening;

public class Hat : MonoBehaviour
{
    public long Id;
    public SpriteRenderer Image;
    public SpriteRenderer ColorImage;

    public void SetColor(Color color)
    {
        if(ColorImage != null)
        {
            ColorImage.color = color;
        }
    }
}
