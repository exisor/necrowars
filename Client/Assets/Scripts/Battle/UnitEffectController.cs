﻿using UnityEngine;
using System.Collections;

public class UnitEffectController : MonoBehaviour
{
    public enum UnitEffectPosition { Center, Top, Down };

    public Transform EffectContainer;
    public Transform EffectContainerDown;
    public Transform EffectContainerTop;

    public void DestroyController()
    {
        EffectContainer.DetachChildren();
        EffectContainerDown.DetachChildren();
        EffectContainerTop.DetachChildren();
    }

    public void SetVisible(bool value)
    {
        EffectContainer.gameObject.SetActive(value);
        EffectContainerDown.gameObject.SetActive(value);
        EffectContainerTop.gameObject.SetActive(value);
    }

    public GameObject AddEffect(GameObject effectPrefab, UnitEffectPosition position = UnitEffectPosition.Center)
    {
        GameObject effect = PoolManager.Instance.GetEffect(effectPrefab.name);
        Transform container;
        if (position == UnitEffectPosition.Center)
        {
            container = EffectContainer;
        }
        else if(position == UnitEffectPosition.Down)
        {
            container = EffectContainerDown;
        }
        else
        {
            container = EffectContainerTop;
        }

        effect.transform.SetParent(container, false);
        effect.transform.localPosition = Vector3.zero;
        effect.transform.localScale = effectPrefab.transform.localScale;
        effect.SetActive(true);
        return effect;
    }
}
