﻿using UnityEngine;
using System.Collections;

public class OffscreenIndicator : MonoBehaviour
{
    private RectTransform _indicator;
    private RectTransform _canvasRect;

    void Awake ()
    {
        _indicator = UIManager.Instance.GetScreen<BattleScreen>(GameScreen.ScreenNameEnum.Battle).LeaderIndicator;
        _canvasRect = UIManager.Instance.Canvas.GetComponent<RectTransform>();
	}
	
	void Update ()
    {
        Vector3 targetPosOnScreen = Camera.main.WorldToScreenPoint(transform.position);
        _indicator.gameObject.SetActive(!MathUtils.OnScreen(targetPosOnScreen));

        if (!_indicator.gameObject.activeInHierarchy)
        {
            return;
        }

        Vector3 center = new Vector3(Screen.width / 2f, Screen.height / 2f, 0);
        float angle = Mathf.Atan2(targetPosOnScreen.y - center.y, targetPosOnScreen.x - center.x) * Mathf.Rad2Deg;
        if (angle < 0) angle = angle + 360;

        Vector2 ViewportPosition = Camera.main.ScreenToViewportPoint(MathUtils.GetPoint(center, targetPosOnScreen));
        Vector2 newPosition = new Vector2(
        ((ViewportPosition.x * _canvasRect.sizeDelta.x) - (_canvasRect.sizeDelta.x * 0.5f)),
        ((ViewportPosition.y * _canvasRect.sizeDelta.y) - (_canvasRect.sizeDelta.y * 0.5f)));

        if (Vector2.Distance(newPosition, _indicator.anchoredPosition) > 20)
        {
            _indicator.anchoredPosition = newPosition;
        }
        else
        {
            _indicator.anchoredPosition = Vector2.Lerp(_indicator.anchoredPosition, newPosition, Time.deltaTime * 15f);
        }
       
        _indicator.eulerAngles = new Vector3(0, 0, angle);
    }
}
