﻿using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.Events;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.Networking;
using UnityEngine.EventSystems;
using System;
using System.Linq;

public class GameManager : MonoBehaviour 
{		
	public static GameManager Instance = null;

    public List<Army> Armys = new List<Army>();

	public PlayerArmy PlayerArmy;
    public ArmyLeader PlayerLeader;
    public GameObject PlayerLeaderGO;

    public Vector2 TargetPosition;
	public CameraFollow CameraFollow;
    public int PlayerLeaderboardPosition;
    public event Action EventSetPlayerArmy;
    public event Action EventSetLeader;
    public Army TopArmy;

    protected List<Army> _sortArmys;
    private Dictionary<NetworkInstanceId, Dictionary<NetworkInstanceId, Unit>> NeedArmyUnits = new Dictionary<NetworkInstanceId, Dictionary<NetworkInstanceId, Unit>>();

    public virtual void Awake () 
	{
		Instance = this;
        UIManager.Instance.IsPointerOverGUI = false;

        ApplicationManager.Instance.LoadCallback();

        var circleColliders = GameObject.FindObjectsOfType<CircleCollider2D>();
        for(int i = 0, n = circleColliders.Length; i < n; i++)
        {
            Destroy(circleColliders[i]);
        }

        var capsuleColliders = GameObject.FindObjectsOfType<CapsuleCollider>();
        for (int i = 0, n = capsuleColliders.Length; i < n; i++)
        {
            Destroy(capsuleColliders[i]);
        }
    }

    public virtual void Start()
    {
        _sortArmys = Armys.Where(army => !army.Destroyed).OrderByDescending(army => army.Points).ToList();
    }

    public void AddArmy(Army army)
	{
		Armys.Add(army);
	}

	public void RemoveArmy(Army army)
	{
		Armys.Remove(army);
	}
	
	public void SetPlayerArmy(PlayerArmy army)
	{
		PlayerArmy = army;
		PlayerArmy.EventSetLeader += OnSetLeader;
        SetPlayerLeaderboardPosition();
        if (EventSetPlayerArmy != null)
		{
			EventSetPlayerArmy();
		}
    }

    protected virtual void SetPlayerLeaderboardPosition()
    {
        PlayerLeaderboardPosition = _sortArmys.IndexOf(PlayerArmy);
    }

    public void AddNeedArmyUnit(NetworkInstanceId oldArmyId, NetworkInstanceId armyId, Unit unit)
    {
        if (NeedArmyUnits.ContainsKey(oldArmyId))
        {
            NeedArmyUnits[oldArmyId].Remove(unit.netId);
        }
        if (!NeedArmyUnits.ContainsKey(armyId))
        {
            NeedArmyUnits[armyId] = new Dictionary<NetworkInstanceId, Unit>();
        }
        NeedArmyUnits[armyId][unit.netId] = unit;
    }

    public List<Unit> GetNeedArmyUnits(NetworkInstanceId armyId)
    {
        List<Unit> units = new List<Unit>();
        if (NeedArmyUnits.ContainsKey(armyId))
        {
            units = NeedArmyUnits[armyId].Values.ToList();
            NeedArmyUnits.Remove(armyId);
        }

        return units;
    }

    public virtual List<Army> GetLeaderboard(int count)
	{
		if(count > _sortArmys.Count)
		{
			return _sortArmys;
		}
		return _sortArmys.Take(count).ToList();
	}
	
	public void OnSetLeader()
	{
        PlayerLeader = PlayerArmy.Leader;
        PlayerLeaderGO = PlayerLeader.gameObject;

        var collider = PlayerLeaderGO.AddComponent<CircleCollider2D>();
        collider.isTrigger = true;
        collider.radius = 0.5f;

        PlayerArmy.Leader.EventDie += OnLeaderDie;
        TargetPosition = PlayerArmy.Leader.transform.position;
        CameraFollow.Target = PlayerArmy.Leader.transform;
		CameraFollow.UpdatePosition();

        Analytics.gua.sendEventHit("Battle", "EndEnteringBattle", "", (int)DataManager.Instance.GetCurrentCharacterId());
        UIManager.Instance.ShowScreen(GameScreen.ScreenNameEnum.Battle);

        if (EventSetLeader != null)
        {
            EventSetLeader();
        }
    }

	public void OnLeaderDie()
	{
        Debug.Log(PlayerArmy.Points);
        if (DataManager.Instance.Social == DataManager.SocialType.Kong)
        {
            Application.ExternalCall("kongregate.stats.submit", "points", PlayerArmy.Points);
        }

        StartCoroutine(ExitDelay());
        //ApplicationManager.Instance.ShowLobby(OnLoadSceneCallBack);
    }

    IEnumerator ExitDelay()
    {
        yield return new WaitForSeconds(2f);

        GameNetworkManager.Instance.StopClient();
        ApplicationManager.Instance.ShowLobby(OnLoadSceneCallBack);
    }

    private void OnLoadSceneCallBack()
    {
        UIManager.Instance.ShowScreen(GameScreen.ScreenNameEnum.Main);
    }
	
	public virtual void Update()
    {
        if (Time.frameCount % 30 == 0)
        {
            _sortArmys = Armys.Where(army => !army.Destroyed).OrderByDescending(army => army.Points).ToList();
            var old = TopArmy;
            if(_sortArmys.Count > 0)
            {
                TopArmy = _sortArmys[0];
            }
            if(old != null)
            {
                old.UpdateVisual();
            }
            if (TopArmy != null)
            {
                TopArmy.UpdateVisual();
            }

            if (PlayerArmy != null)
            {
                PlayerLeaderboardPosition = _sortArmys.IndexOf(PlayerArmy) + 1;
            }
        }

		if(PlayerArmy != null)
		{
            CameraFollow.SetQuantity(PlayerArmy.GetQuantity());
            CameraFollow.SetDefaultSize(Input.GetAxis("Mouse ScrollWheel"));

            if (PlayerArmy.Leader != null)
			{
				Vector3 pos = PlayerArmy.Leader.transform.position;
				if(Input.GetKey(KeyCode.S))
				{
					pos.y -= 3;
				}
				if(Input.GetKey(KeyCode.W))
				{
					pos.y += 3;
				}
				if(Input.GetKey(KeyCode.A))
				{
					pos.x -= 3;
				}
				if(Input.GetKey(KeyCode.D))
				{
					pos.x += 3;
				}
				if(pos != PlayerArmy.Leader.transform.position)
				{
					PlayerArmy.CmdMove(pos);
				}
			}
        }
	}
	
	public virtual void FixedUpdate()
	{
		if(PlayerArmy != null)
		{
			if(Input.GetButton("Fire1") && !UIManager.Instance.IsPointerOverGUI)
			{
				TargetPosition = (Vector2)Camera.main.ScreenToWorldPoint(Input.mousePosition);
			}
            PlayerArmy.CmdMove(TargetPosition);
        }
	}

    public virtual void CompleteTutorialPhase()
    {

    }

}
