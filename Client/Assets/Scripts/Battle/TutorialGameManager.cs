﻿using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.Events;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.Networking;
using UnityEngine.EventSystems;
using System;
using System.Linq;
using DG.Tweening;

public class TutorialGameManager : GameManager 
{
	public PlayerArmy PlayerArmyPrefab;
	public ArmyLeader LeaderPrefab;
    public HPPotion HPPotionPrefab;
    public List<Transform> UnitsTransform = new List<Transform>();
    public List<Unit> UnitsPrefabs = new List<Unit>();
    public List<Unit> Units = new List<Unit>();

	private List<Army> _fakeLeaderboard = new List<Army>();
    private bool _isResurrection = false;
    private bool _isCollision = false;
    private HPPotion _hpPotion;

    public override void Awake () 
	{
        Instance = this;
        ApplicationManager.Instance.LoadCallback();

        UIManager.Instance.ShowScreen(GameScreen.ScreenNameEnum.Battle);
        UIManager.Instance.GetScreen<BattleScreen>(GameScreen.ScreenNameEnum.Battle).SetDefaultView(false);
    }

	public override void Start () 
	{
        _sortArmys = new List<Army>();

        PlayerArmy = (PlayerArmy) Instantiate(PlayerArmyPrefab);
        PlayerArmy.IsPlayer = true;

		PlayerLeader = (ArmyLeader) Instantiate(LeaderPrefab);
        PlayerLeader.UpdateAlive(true);
        PlayerLeader.Quantity = 1;
        PlayerLeader.RotationDistance = 0.1f;
        PlayerLeader.Army = PlayerArmy;
        PlayerLeader.UpdateVisial(PlayerArmy.PlayerColor, 0);

        for (int i = 0; i < UnitsPrefabs.Count; i++)
        {
            var unit = (Unit)Instantiate(UnitsPrefabs[i]);
            unit.gameObject.SetActive(false);
            Units.Add(unit);
        }

        _hpPotion = (HPPotion)Instantiate(HPPotionPrefab);
        _hpPotion.Charges = 1;

        UIManager.Instance.GetScreen<BattleScreen>(GameScreen.ScreenNameEnum.Battle).UpdateHPBar(100, 100);
    }

	public override List<Army> GetLeaderboard(int count)
	{
		return _fakeLeaderboard;
	}

	public override void Update()
	{
		if(PlayerArmy != null)
		{
            if (Tutorial.GetPhase() == 2)
            {
                if(!_isCollision)
                {
                    if (Input.GetButton("Fire1") && !UIManager.Instance.IsPointerOverGUI)
                    {
                        TargetPosition = (Vector2)Camera.main.ScreenToWorldPoint(Input.mousePosition);
                    }
                    PlayerArmy.Move(TargetPosition);
                }
                else
                {
                    var enemyLeader = Units.Find(u => u is ArmyLeader);
                    PlayerLeader.OnAttack(enemyLeader.transform.position.x, enemyLeader.transform.position.y);

                    foreach (var unit in Units)
                    {
                        if(unit.Alive)
                        {
                            unit.OnAttack(PlayerLeader.transform.position.x, PlayerLeader.transform.position.y);
                        }
                    }
                }
            }

            if (Tutorial.GetPhase() == 3 && (Input.GetKeyDown(KeyCode.R) || Input.GetButtonDown("Fire2")) && !_isResurrection)
			{
                _isResurrection = true;
				foreach(var unit in Units)
				{
                    if(!(unit is ArmyLeader))
                    {
                        unit.UpdateAlive(true);
                        unit.OnResurrect();
                        unit.Army = PlayerArmy;
                        unit.UpdateVisial(PlayerArmy.PlayerColor, 0);
                    }
				}

                StartCoroutine(NextTutorialPhase());
			}
        }
	}

    public override void CompleteTutorialPhase()
    {
        if(Tutorial.GetPhase() == 1)
        {
            for(int i = 0; i < Units.Count; i++)
            {
                Units[i].gameObject.SetActive(true);
                Units[i].UpdateAlive(true);
                Units[i].UpdateVisial(PlayerArmy.EnemyColors[0], 0);
                Units[i].Quantity = 1;

                Units[i].AddEffect(Units[i].RessurrectFX);
                Units[i].gameObject.AddComponent<CircleCollider2D>();
                Units[i].gameObject.layer = 0;

                Units[i].transform.position = UnitsTransform[i].position;
                Units[i].transform.localScale = UnitsTransform[i].localScale;
            }

            PlayerLeader.gameObject.AddComponent<CircleCollider2D>();
            PlayerLeader.gameObject.AddComponent<TutorialCollision>();
            PlayerLeader.gameObject.layer = 0;
            Tutorial.NextPhase();
        }
        else if (Tutorial.GetPhase() == 2 && !_isCollision)
        {
            PlayerLeader.GetComponent<Rigidbody2D>().velocity = Vector2.zero;
            Destroy(PlayerLeader.GetComponent<TutorialCollision>());
            _isCollision = true;
            DOTween.To(x => UIManager.Instance.GetScreen<BattleScreen>(GameScreen.ScreenNameEnum.Battle).UpdateHPBar((int)x, 100), 100, 50, 4f);
            StartCoroutine(DyingTutorialPhase());
        }
        else if (Tutorial.GetPhase() == 4)
        {
            _hpPotion.Charges = 0;
            StartCoroutine(CompleteTutorial());
        }
    }

    protected override void SetPlayerLeaderboardPosition()
    {
        
    }

    IEnumerator DyingTutorialPhase()
    {
        yield return new WaitForSeconds(1f);

        foreach (var unit in Units)
        {
            unit.OnDie();
            unit.UpdateAlive(false);
            yield return new WaitForSeconds(0.5f);
        }

        Tutorial.NextPhase();
    }

    IEnumerator NextTutorialPhase()
    {
        yield return new WaitForSeconds(1f);

        _hpPotion.gameObject.AddComponent<TutorialHPPotion>();
        Tutorial.NextPhase();
    }

    IEnumerator CompleteTutorial()
    {
        DOTween.To(x => UIManager.Instance.GetScreen<BattleScreen>(GameScreen.ScreenNameEnum.Battle).UpdateHPBar((int)x, 100), 50, 100, 2f);
        yield return new WaitForSeconds(2f);

        ApplicationManager.Instance.ShowLobby(OnLoadSceneCallBack);
    }

    private void OnLoadSceneCallBack()
    {
        Tutorial.NextPhase();
        var message = new ClientProtocol.BattleEndedEvent();
        message.ChestRewardId = 4;

        UIManager.Instance.ShowScreen(GameScreen.ScreenNameEnum.Main);
        UIManager.Instance.GetPopup<BattleEndPopup>(Popup.PopupNameEnum.BattleEnd).ShowInfo(message);
        UIManager.Instance.ShowPopup(Popup.PopupNameEnum.BattleEnd);
    }

    public override void FixedUpdate()
	{
		
	}
}
