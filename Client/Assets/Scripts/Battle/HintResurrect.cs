﻿using UnityEngine;
using System.Collections;

public class HintResurrect : MonoBehaviour 
{
	public GameObject Hint;

	public void Show(bool visible)
	{
		Hint.SetActive(visible);
	}
}
