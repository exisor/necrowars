﻿using UnityEngine;
using UnityEngine.Networking;
using DG.Tweening;

public class PickupBonus : NetworkBehaviour
{
    public GameObject PickupEffectPrefab;
    [SyncVar(hook = "OnRadiusChanged")] private float _radius;

    #region Overrides of NetworkBehaviour

    public override void OnStartClient()
    {
        base.OnStartClient();
        OnRadiusChanged(_radius);
    }

    #endregion

    private void OnRadiusChanged(float value)
    {
        transform.localScale = new Vector3(value*4, value*4, 1f);
    }

    public override void OnNetworkDestroy()
    {        
        GameObject effect = (GameObject)Instantiate(PickupEffectPrefab, transform.position, transform.rotation);
        effect.transform.localScale = transform.localScale;
        effect.GetComponent<SpriteRenderer>().DOFade(0f, 1f);
        effect.transform.DOMoveY(transform.position.y + 1, 1f).OnComplete(() => Destroy(effect));
    }
}