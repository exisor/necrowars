﻿using UnityEngine;
using System.Collections;
using UnityEngine.Networking;

public class AbilityWithCharges : ActiveAbility
{
    [SyncVar]
    public int Charges;

    protected override void SetInSlot()
    {
        UIManager.Instance.GetScreen<BattleScreen>(GameScreen.ScreenNameEnum.Battle).SetConsumableAbilityInSlot(this);
    }

}
