﻿using UnityEngine;
using System.Collections;

public class Explosions : ActiveAbility 
{
    public AudioClip Sound;
    protected override void CreateSound(Vector2 targetPosition)
    {
        base.CreateSound(targetPosition);

        PlaySound(targetPosition, Sound);
    }
}
