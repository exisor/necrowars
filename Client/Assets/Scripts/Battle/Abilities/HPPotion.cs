﻿using UnityEngine;
using System.Collections;
using UnityEngine.Networking;

public class HPPotion : AbilityWithCharges
{
    [SyncVar]
    public float Heal;

    public AudioClip Sound;

    public override void OnUse(Vector2 targetPosition)
    {
        if(Tutorial.GetPhase() == 4)
        {
            TutorialGameManager.Instance.CompleteTutorialPhase();
        }
        else
        {
            base.OnUse(targetPosition);
        }
    }

    protected override void CreateSound(Vector2 targetPosition)
    {
        base.CreateSound(targetPosition);

        PlaySound(targetPosition, Sound, false);
    }
}
