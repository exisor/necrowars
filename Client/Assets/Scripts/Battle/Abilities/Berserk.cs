﻿using UnityEngine;
using System.Collections;

public class Berserk : ActiveAbility 
{
    public AudioClip Sound;

    public override void OnUse(Vector2 targetPosition)
    {
        Debug.Log("CmdCast " + Id.ToString());

        CmdCast(PlayerArmy.transform.position);
    }

    protected override void CreateSound(Vector2 targetPosition)
    {
        base.CreateSound(targetPosition);

        PlaySound(targetPosition, Sound);
    }
}
