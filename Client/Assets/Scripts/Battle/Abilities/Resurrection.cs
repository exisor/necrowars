﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.Networking;
using System.Linq;

public class Resurrection : ActiveAbility
{
    [SyncVar]
    private float _radius;

    private List<Unit> _units = new List<Unit>();

    public void FixedUpdate()
    {
        if (hasAuthority && ArmyLeader != null && Time.frameCount % 30 == 0)
        {
            foreach (Unit unit in _units)
            {
                if (unit != null)
                {
                    unit.HideHintResurrect();
                }
            }
            _units = Physics2D.OverlapCircleAll(ArmyLeader.transform.position, _radius, LayerMask.GetMask("Units")).ToList()
                .Select(c => c.GetComponent<Unit>()).ToList();
            foreach (Unit unit in _units)
            {
                unit.ShowHintResurrect();
            }
        }
    }

    public void Update()
    {
        if ((Input.GetKeyDown(KeyCode.R) || Input.GetButtonDown("Fire2")) && hasAuthority && CooldownTimer <= 0)
        {
            OnUse((Vector2)Camera.main.ScreenToWorldPoint(Input.mousePosition));
        }
    }
}