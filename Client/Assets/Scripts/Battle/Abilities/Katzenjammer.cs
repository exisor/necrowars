﻿using UnityEngine;
using System.Collections;
using UnityEngine.Networking;

public class Katzenjammer : ActiveAbility 
{
	[SyncVar] 
	private float _radius;
    [SyncVar] 
    private float _distance;

    public Transform AbilityRadiusPrefab;
	private Transform _abilityRadius;

	private bool _isPreUse = false;

	public override void OnPreUse()
	{
		if(CooldownTimer <= 0 && !IsCasting && !_isPreUse)
		{
			_abilityRadius = (Transform) Instantiate(AbilityRadiusPrefab, GetPosition(), Quaternion.identity);

			Vector3 scale = _abilityRadius.localScale;
			scale *= _radius;
			_abilityRadius.localScale = scale;

			_isPreUse = true;
		}
	}

	public void Update()
	{
		if(_isPreUse)
		{
			_abilityRadius.position = GetPosition();
		}
	}

	public override void OnUse(Vector2 targetPosition)
	{
		if(_isPreUse)
		{
			CmdCast((Vector2)_abilityRadius.position);
			Destroy(_abilityRadius.gameObject);
		}
		_isPreUse = false;
	}

	private Vector3 GetPosition()
	{
		Vector2 start = (Vector2)ArmyLeader.transform.position;
		Vector2 end = (Vector2)Camera.main.ScreenToWorldPoint(Input.mousePosition);
		float dis = Vector2.Distance(start, end);
		if(dis > _distance)
		{
			dis = _distance;
		}
		return start + (end - start).normalized * dis;
	}
}
