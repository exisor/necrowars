﻿using UnityEngine;
using System.Collections;

public class CooldownPotion : AbilityWithCharges
{
    public AudioClip Sound;

    protected override void CreateSound(Vector2 targetPosition)
    {
        base.CreateSound(targetPosition);

        PlaySound(targetPosition, Sound, false);
    }
}
