﻿using UnityEngine;
using System.Collections;
using UnityEngine.Networking;

public class Charge : ActiveAbility 
{
    [SyncVar] 
    private float _distance;

    public Transform AbilityPointPrefab;
    public AudioClip Sound;
    private Transform _abilityPoint;

	private bool _isPreUse = false;

	public override void OnPreUse()
	{
		if(CooldownTimer <= 0 && !IsCasting && !_isPreUse)
		{
			_abilityPoint = (Transform) Instantiate(AbilityPointPrefab, GetPosition(), Quaternion.identity);

			_isPreUse = true;
		}
	}

	public void Update()
	{
		if(_isPreUse)
		{
			_abilityPoint.position = GetPosition();
		}
	}

	public override void OnUse(Vector2 targetPosition)
	{
		if(_isPreUse)
		{
			CmdCast((Vector2)_abilityPoint.position);
			Destroy(_abilityPoint.gameObject);
		}
		_isPreUse = false;
	}

    protected override void CreateSound(Vector2 targetPosition)
    {
        base.CreateSound(targetPosition);

        PlaySound(targetPosition, Sound);
    }

    private Vector3 GetPosition()
	{
		Vector2 start = (Vector2)ArmyLeader.transform.position;
		Vector2 end = (Vector2)Camera.main.ScreenToWorldPoint(Input.mousePosition);
		float dis = Vector2.Distance(start, end);
		if(dis > _distance)
		{
			dis = _distance;
		}
		return start + (end - start).normalized * dis;
	}
}
