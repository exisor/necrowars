using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Networking;

public class ActiveAbility: NetworkAbility
{
    public enum AbilityType { Normal, Consumable };
    public delegate void EventUsedDelegate(Vector2 pos);

    //[SyncVar]
    protected float CastTimer;
    //[SyncVar]
    public float CooldownTimer;
    [SyncVar]
    protected bool IsCasting;
    [SyncVar] 
    public float Cooldown;
    [SyncVar]
    public int Slot = -1;
    [SyncEvent]
    public event EventUsedDelegate EventUsed;

    public AbilityType Type = AbilityType.Normal;
    public ArmyLeader ArmyLeader;
    public PlayerArmy PlayerArmy;

    public virtual void Awake()
    {        
        EventUsed += OnEventUsed;
    }

    public void Start()
    {
        UpdateSlot(Slot);

        if (GameManager.Instance.PlayerArmy == null)
        {
            GameManager.Instance.EventSetPlayerArmy += OnSetPlayerArmy;
        }
        else
        {
            OnSetPlayerArmy();
        }
    }

    [TargetRpc(channel = 2)]
    public void TargetCooldownChanged(NetworkConnection conn, float cooldown)
    {
        CooldownTimer = cooldown;
    }

    public void UpdateSlot(int slot)
    {
        Slot = slot;
        if (hasAuthority)
        {
            SetInSlot();
        }        
    }

    protected virtual void SetInSlot()
    {
        if (Slot != -1)
        {
            UIManager.Instance.GetScreen<BattleScreen>(GameScreen.ScreenNameEnum.Battle).SetAbilityInSlot(this);
        }
    }

    public void OnSetPlayerArmy()
    {
        PlayerArmy = GameManager.Instance.PlayerArmy;

        if (PlayerArmy.Leader == null)
        {
            PlayerArmy.EventSetLeader += OnSetLeader;
        }
        else
        {
            OnSetLeader();
        }
    }

    public void OnSetLeader()
    {
        ArmyLeader = PlayerArmy.Leader;
    }

    [Command(channel = 2)]
    public void CmdCast(Vector2 targetPosition)
    {
        
    }

    public virtual void OnPreUse()
    {

    }

    public virtual void OnUse(Vector2 targetPosition)
    {
        Debug.Log("CmdCast " + Id.ToString());
        
        CmdCast(targetPosition);
        Analytics.gua.sendEventHit("Battle", "SkillUsed", Id.ToString(), (int)DataManager.Instance.GetCurrentCharacterId());
    }

    private void OnEventUsed(Vector2 pos)
    {
        CreateEffect(pos);
        CreateSound(pos);
    }

    protected virtual void CreateEffect(Vector2 targetPosition)
    {
        
    }

    protected virtual void CreateSound(Vector2 targetPosition)
    {

    }

    protected void PlaySound(Vector2 targetPosition, AudioClip Sound, bool isPlayingOther = true)
    {
        SMSound sound;
        if (hasAuthority)
        {
            sound = SoundManager.PlaySound(Sound);
            if(sound.IsValid)
            {
                sound.Source.transform.SetParent(Camera.main.transform, false);
            }
        }
        else if (isPlayingOther)
        {
            Vector3 screenPoint = Camera.main.WorldToViewportPoint(targetPosition);
            bool onScreen = screenPoint.x > -0.2 && screenPoint.y > -0.2 && screenPoint.x < 1.2 && screenPoint.y < 1.2;
            if(!onScreen)
            {
                return;
            }
            sound = SoundManager.PlaySound(Sound);
            if (sound.IsValid)
            {
                sound.Source.transform.position = targetPosition;
            }
        }
    }
}