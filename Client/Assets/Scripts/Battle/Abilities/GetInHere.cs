﻿using UnityEngine;
using System.Collections;
using UnityEngine.Networking;

public class GetInHere : ActiveAbility 
{
	public delegate void EventSummonedDelegate(NetworkInstanceId networkId);
	public GameObject EffectPrefab;
	public GameObject SmokeEffectPrefab;

	[SyncEvent]
    public event EventSummonedDelegate EventSummoned;

    public override void Awake()
	{
		base.Awake();
		
		EventSummoned += OnEventSummoned;
	}

	public void OnEventSummoned(NetworkInstanceId networkId)
	{
		GameObject go = ClientScene.FindLocalObject(networkId);
		if(go != null)
		{
			GameObject effect = PoolManager.Instance.GetEffect(SmokeEffectPrefab.name);
			effect.transform.position = go.transform.position;
			effect.SetActive(true);
		}
	}
}
