﻿using UnityEngine;
using System.Collections;

public class AbilityBehavior : MonoBehaviour 
{
	public ActiveAbility ActiveAbility;
	public ArmyLeader ArmyLeader;
	public PlayerArmy PlayerArmy;
	
	void Start () 
	{
		if(GameManager.Instance.PlayerArmy == null)
		{
			GameManager.Instance.EventSetPlayerArmy += OnSetPlayerArmy;
		}
		else
		{
			OnSetPlayerArmy();
		}
	}

	public void OnSetPlayerArmy()
	{
		PlayerArmy = GameManager.Instance.PlayerArmy;
		
		if(PlayerArmy.Leader == null)
		{
			PlayerArmy.EventSetLeader += OnSetLeader;
		}
		else
		{
			OnSetLeader();
		}
	}

	public void OnSetLeader()
	{
		ArmyLeader = PlayerArmy.Leader;
	}

	public virtual void OnPreUse()
	{

	}

	public virtual void OnUse(Vector2 targetPosition)
	{

	}
}
