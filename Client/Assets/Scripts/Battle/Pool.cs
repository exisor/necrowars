﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

public class Pool : MonoBehaviour
{
	public GameObject PoolPrefab;

	public int StartPoolCount;

	public Stack<GameObject> PooledObjects = new Stack<GameObject>();
	
	void Start () 
	{
        //CreatePool();
    }

	public GameObject GetPoolObject()
	{
        /*
		if (PooledObjects.Count == 0) 
		{
            CreatePool();
		}
		return PooledObjects.Pop();*/
        GameObject go = (GameObject)Instantiate(PoolPrefab, transform, false);
        //go.name = PoolPrefab.name;
        return go;
    }

	public void ReturnToPool(GameObject go)
	{
        if(go != null)
        {
            Destroy(go);
            /*
            go.transform.SetParent(transform, false);
            PooledObjects.Push(go);
            go.SetActive(false);*/
        }
	}

    private void CreatePool()
    {
        for (int i = 0; i < StartPoolCount; i++)
        {
            PooledObjects.Push(CreateNewPooledObject());
        }
    }

    private GameObject CreateNewPooledObject()
	{
		GameObject go = (GameObject) Instantiate(PoolPrefab, transform, false);
		go.name = PoolPrefab.name;
		go.SetActive(false);
		return go;
	}
}
