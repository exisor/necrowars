﻿using UnityEngine;
using System.Collections.Generic;
using UnityEngine.Networking;

public class ArmorOfSheetEffect : TargetEffect 
{
    public GameObject StartEffectPrefab;

    protected override void CreateEffect(Unit unit)
	{
        base.CreateEffect(unit);
        unit.AddEffect(StartEffectPrefab);
    }
}
