﻿using UnityEngine;
using System.Collections.Generic;
using UnityEngine.Networking;
using System;

public class VikingTrapEffect : TargetEffect
{
    [SyncEvent]
    public event Action EventDetonate;

    public void Awake()
    {
        EventDetonate += OnEventDetonate;
    }

    protected override void CreateEffect(Unit unit)
    {
    }


    private void OnEventDetonate()
    {
        GameObject go = ClientScene.FindLocalObject(_targetId);

        if (go != null)
        {
            Unit = go.GetComponent<Unit>();

            GameObject effect = PoolManager.Instance.GetEffect(EffectPrefab.name);
            effect.transform.position = go.transform.position;
            effect.transform.localScale = new Vector3(0.5f, 0.5f, 0.5f);
            effect.SetActive(true);
        }
    }
}
