﻿using UnityEngine;
using System.Collections;

public class DrakkarEffect : GroundEffect
{
	public GameObject DrakkarPrefab;

	private GameObject _drakkar;

	public void Start()
	{
		_drakkar = PoolManager.Instance.GetEffect(DrakkarPrefab.name);
		_drakkar.SetActive(true);
		_drakkar.transform.position = transform.position;
	}

	public void Update()
	{
		Vector3 scale = _drakkar.transform.localScale;
		if(_drakkar.transform.position.x < transform.position.x)
		{
			scale.x = 1;
		}
		else
		{
			scale.x = -1;
		}
		_drakkar.transform.localScale = scale;

		_drakkar.transform.position = transform.position;
	}
	
	public override void OnNetworkDestroy()
	{
        PoolManager.Instance.ReturnEffect(DrakkarPrefab.name, _drakkar);

        Vector3 screenPoint = Camera.main.WorldToViewportPoint(transform.position);

        if(screenPoint.x > 0 && screenPoint.y > 0 && screenPoint.x < 1 && screenPoint.y < 1)
        {
            GameObject effect = PoolManager.Instance.GetEffect(EffectPrefab.name);
            effect.transform.position = transform.position;
            effect.SetActive(true);

            effect.transform.localScale = Vector3.one;

            GameManager.Instance.CameraFollow.PlayShake();
        }
	}
}
