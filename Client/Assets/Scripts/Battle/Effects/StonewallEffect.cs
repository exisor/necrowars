﻿using UnityEngine;
using System.Collections;

public class StonewallEffect : GroundEffect
{
    public GameObject StonewallPrefab;

    private GameObject _wall;

    public void Start()
    {
        _wall = PoolManager.Instance.GetEffect(StonewallPrefab.name);
        _wall.SetActive(true);
        _wall.transform.position = transform.position;
        _wall.transform.rotation = transform.rotation;

        GameObject effect = PoolManager.Instance.GetEffect(EffectPrefab.name);
        effect.transform.position = transform.position;
        effect.SetActive(true);

    }

    public void Update()
    {
        _wall.transform.position = transform.position;
        _wall.transform.rotation = transform.rotation;
    }

    public override void OnNetworkDestroy()
    {
        PoolManager.Instance.ReturnEffect(StonewallPrefab.name, _wall);
    }
}
