﻿using UnityEngine;
using System.Collections.Generic;
using UnityEngine.Networking;

public class ExplosionsEffect : TargetEffect
{
    protected override UnitEffectController.UnitEffectPosition EffectPosition
    {
        get { return UnitEffectController.UnitEffectPosition.Down; }
    }
}
