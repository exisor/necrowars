﻿using UnityEngine;
using System.Collections;
using UnityEngine.Networking;

public class TargetEffect : Effect 
{
	[SyncVar(hook = "UpdateTarget")] 
	protected NetworkInstanceId _targetId;
    protected GameObject _target;

    public Sprite Sprite;
    public string Hint;
    public Unit Unit;

    private GameObject _view;

    protected virtual UnitEffectController.UnitEffectPosition EffectPosition { get { return UnitEffectController.UnitEffectPosition.Center;} }

	private void UpdateTarget(NetworkInstanceId targetId)
	{
        _targetId = targetId;

        if(_target == null)
        {
            _target = ClientScene.FindLocalObject(_targetId);

            if(_target != null)
            {
                if(Sprite != null)
                {
                    if(GameManager.Instance.PlayerLeaderGO != null)
                    {
                        AddBarEffect();
                    }
                    else
                    {
                        GameManager.Instance.EventSetLeader += AddBarEffect;
                    }
                }

                Unit = _target.GetComponent<Unit>();
                CreateEffect(Unit);
            }            
        }
	}

    private void AddBarEffect()
    {
        if (_target == GameManager.Instance.PlayerLeaderGO)
        {
            UIManager.Instance.GetScreen<BattleScreen>(GameScreen.ScreenNameEnum.Battle).AddEffect(this);
        }
    }

	protected virtual void CreateEffect(Unit unit)
	{
	    if (EffectPrefab != null)
	    {
	        _view = unit.AddEffect(EffectPrefab, EffectPosition);
	    }
	}

    public void OnDestroy()
    {
        if (_target == GameManager.Instance.PlayerLeaderGO && Sprite != null && _target != null)
        {
            UIManager.Instance.GetScreen<BattleScreen>(GameScreen.ScreenNameEnum.Battle).RemoveEffect(this);
        }
    }

    public override void OnNetworkDestroy()
    {
        DestroyEffect();
    }

    protected virtual void DestroyEffect()
    {
        if (_view != null)
        {
            PoolManager.Instance.ReturnEffect(EffectPrefab.name, _view);
        }
    }
}
