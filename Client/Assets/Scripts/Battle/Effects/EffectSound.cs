﻿using UnityEngine;
using System.Collections;

public class EffectSound : MonoBehaviour
{
    public AudioClip Sound;

    void OnEnable ()
    {
        var sound = SoundManager.PlaySound(Sound);
        sound.AttachToObject(transform);
    }
}
