﻿using UnityEngine;

namespace Assets.Scripts.Battle.Effects
{
    [RequireComponent(typeof(Animator))]
    public class EffectPlayedController : MonoBehaviour
    {
        private Animator _animator;
        private bool _enabled;

        private bool Enabled
        {
            get { return _enabled; }
            set
            {
                if (value == _enabled) return;
                _enabled = value;
                _animator.SetBool("Played", value);
            }
        }

        void OnEnable()
        {
            Enabled = true;
        }

        void Awake()
        {
            _animator = GetComponent<Animator>();
            Enabled = true;
        }

        // Setting variables on disabled animator cause warnings
        void OnDisable()
        {
            //Enabled = false;
        }

        void OnDestroy()
        {
            //Enabled = false;
        }
        
    }
}
