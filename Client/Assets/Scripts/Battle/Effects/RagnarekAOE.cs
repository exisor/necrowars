﻿using UnityEngine;
using System.Collections;
using UnityEngine.Networking;

public class RagnarekAOE : GroundEffect
{
	[SyncVar] 
    private float _radius;

	public void Start()
	{
		
	}

    public override void OnNetworkDestroy()
    {
        GameObject effect = PoolManager.Instance.GetEffect(EffectPrefab.name);
        effect.transform.position = transform.position;
        effect.SetActive(true);

        Vector3 scale = effect.transform.localScale;
        scale *= _radius;
        effect.transform.localScale = scale;

        Vector3 screenPoint = Camera.main.WorldToViewportPoint(transform.position);

        if (screenPoint.x > 0 && screenPoint.y > 0 && screenPoint.x < 1 && screenPoint.y < 1)
        {
            GameManager.Instance.CameraFollow.PlayShake();
        }
    }
}
