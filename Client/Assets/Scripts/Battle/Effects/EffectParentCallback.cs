﻿using UnityEngine;
using System.Collections;

public class EffectParentCallback : MonoBehaviour
{
    public void OnComplete()
    {
        PoolManager.Instance.ReturnEffect(transform.parent.gameObject.name, transform.parent.gameObject);
    }
}
