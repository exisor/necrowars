﻿using UnityEngine;
using System.Collections;
using UnityEngine.Networking;

public class ChargeHit : GroundEffect
{
    public void Start()
    {

    }

    public override void OnNetworkDestroy()
    {
        GameObject effect = PoolManager.Instance.GetEffect(EffectPrefab.name);
        effect.transform.position = transform.position;
        effect.SetActive(true);
    }
}
