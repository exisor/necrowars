﻿using UnityEngine;

public class ValkyrieDamageRedirrect : TargetEffect
{
    public Sprite SetSprite;

    protected override void CreateEffect(Unit unit)
    {
        if(unit.Army != null)
        {
            SetImage();
        }
        else
        {
            unit.EventSetArmy.AddListener(SetImage);
        }
    }

    private void SetImage()
    {
        Unit.Army.PlayerSetImage.sprite = SetSprite;
        Unit.Army.PlayerSetImage.gameObject.SetActive(true);
    }

    protected override void DestroyEffect()
    {
        
    }
}
