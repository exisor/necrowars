﻿using UnityEngine;
using System.Collections.Generic;
using UnityEngine.Networking;

public class BerserkEffect : TargetEffect 
{
    public GameObject StartEffectPrefab;

    protected override UnitEffectController.UnitEffectPosition EffectPosition
    {
        get { return UnitEffectController.UnitEffectPosition.Top; }
    }

    protected override void CreateEffect(Unit unit)
	{
        base.CreateEffect(unit);
        unit.AddEffect(StartEffectPrefab);
    }
}
