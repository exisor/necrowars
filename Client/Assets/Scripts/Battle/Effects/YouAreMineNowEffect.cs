﻿using UnityEngine;
using System.Collections.Generic;
using UnityEngine.Networking;

public class YouAreMineNowEffect : TargetEffect 
{
	public delegate void EventResurrectedDelegate(NetworkInstanceId target);

	[SyncEvent]
    public event EventResurrectedDelegate EventResurrected;

    public void Awake()
	{
		EventResurrected += OnEventResurrected;
	}

	private void OnEventResurrected(NetworkInstanceId targetId)
	{
		_targetId = targetId;
	    GameObject go = ClientScene.FindLocalObject(_targetId);
		
		if(go != null)
		{
			Unit = go.GetComponent<Unit>();
            Unit.AddEffect(EffectPrefab);
        }
	}
}
