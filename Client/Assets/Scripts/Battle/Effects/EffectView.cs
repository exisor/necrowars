﻿using UnityEngine;
using System.Collections;

public class EffectView : MonoBehaviour 
{
    public void OnComplete()
	{
        PoolManager.Instance.ReturnEffect(gameObject.name, gameObject);
    }
}
