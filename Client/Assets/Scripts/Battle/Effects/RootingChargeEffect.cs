﻿using UnityEngine;
using System.Collections.Generic;
using UnityEngine.Networking;

public class RootingChargeEffect : TargetEffect 
{
    private GameObject _effect;

    protected override void CreateEffect(Unit unit)
	{
        _effect = unit.AddEffect(EffectPrefab, UnitEffectController.UnitEffectPosition.Down);
    }
}
