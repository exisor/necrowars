﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class PoolManager : MonoBehaviour 
{
	public static PoolManager Instance = null; 

	public Pool[] ShellsPool;
	public Pool[] EffectsPool;

	private Dictionary<string, Pool> _effectsDictionary = new Dictionary<string, Pool>();
    private Dictionary<string, Pool> _shellsDictionary = new Dictionary<string, Pool>();

    void Awake () 
	{
		Instance = this;

        foreach (Pool effect in EffectsPool)
        {
        	_effectsDictionary[effect.PoolPrefab.name] = effect;
        }

        foreach (Pool shell in ShellsPool)
        {
            _shellsDictionary[shell.PoolPrefab.name] = shell;
        }
    }

	public GameObject GetShell(string shell)
	{
		return _shellsDictionary[shell].GetPoolObject();
    }

	public GameObject GetEffect(string effect)
	{
		return _effectsDictionary[effect].GetPoolObject();
	}

	public void ReturnShell(string shell, GameObject go)
	{
        if(go != null)
        {
            Destroy(go);
        }
        
        //_shellsDictionary[shell].ReturnToPool(go);
    }

	public void ReturnEffect(string effect, GameObject go)
	{
        if (go != null)
        {
            Destroy(go);
        }
        //_effectsDictionary[effect].ReturnToPool(go);
    }
}
