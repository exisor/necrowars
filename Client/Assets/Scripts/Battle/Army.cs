﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.Networking;
using UnityEngine.Events;
using System;

public class Army : NetworkBehaviour 
{
	public delegate void EventCombineUnitsDelegate(NetworkInstanceId combineTo, NetworkInstanceId combineThis);

	public List<Unit> Units = new List<Unit>();
	
	public ArmyLeader Leader;

	public RectTransform PlayerNameTransform;
    public GameObject PlayerHPBar;
    public Image PlayerHPImage;
    public Image PlayerSetImage;

    public Color Color;
    public AudioClip PickupSound;

    public event Action EventSetLeader;
	public event Action EventArmyCreated;
	
	private bool _isCreated = false;

    #region Network state
    //[SyncVar]
    public int Provision;
    	[SyncVar(hook = "UpdateDestroyed")] 
    	public bool Destroyed;
    	[SyncVar(hook = "UpdatePlayerName")] 
    	public string PlayerName;
        [SyncVar] 
        public int Points;
        [SyncVar(hook = "UpdateHat")]
        public long HatId;
        [SyncVar]
        public int AliveUnits;
        [SyncVar]
        public int VikingsLimit;
        [SyncEvent]
        public event EventCombineUnitsDelegate EventCombineUnits;
    #endregion

    public virtual void Awake()
	{
	}
	
	public virtual void Start () 
	{
		_isCreated = true;

		GameManager.Instance.AddArmy(this);

		UpdatePlayerName(PlayerName);
		UpdateDestroyed(Destroyed);

        foreach(var unit in GameManager.Instance.GetNeedArmyUnits(netId))
        {
            AddUnitDeferred(unit);
        }
		
		if(EventArmyCreated != null)
		{
			EventArmyCreated();
		}
	}

    public override void OnNetworkDestroy()
	{
		GameManager.Instance.RemoveArmy(this);
	}

    public void UpdatePlayerName(string playerName)
	{
		PlayerName = GetPlayerName(playerName);

		PlayerNameTransform.GetComponentInChildren<Text>().text = PlayerName;
	}

    public void UpdateHat(long hatId)
    {
        HatId = hatId;
        UpdateVisual();
    }

    public void UpdateVisual()
    {
        foreach (Unit unit in Units)
        {
            unit.UpdateVisial(Color, HatId);
        }
    }

    protected virtual string GetPlayerName(string playerName)
    {
        return Localization.Instance.GetNick();
    }

	public void UpdateDestroyed(bool destroyed)
	{
		Destroyed = destroyed;
        foreach (Unit unit in Units)
        {
            if(unit == null)
            {
                gameObject.name = "GOVNO";
                Debug.Log(gameObject);
            }
            unit.UpdateColor();
        }
        //PlayerNameTransform.gameObject.SetActive(!Destroyed);
	}
	
	public void AddUnit(Unit unit)
	{
		if(_isCreated)
		{
			AddUnitDeferred(unit);
		}
		else
		{
			EventArmyCreated += ()=>AddUnitDeferred(unit);
		}
	}
	
	protected virtual void AddUnitDeferred(Unit unit)
	{
        unit.Army = this;
        unit.UpdateVisial(Color, HatId);
        unit.EventSetArmy.Invoke();
        Units.Add(unit);

        if (unit.UnitName == Unit.UnitNameEnum.Necromant)
		{
			Leader = (ArmyLeader) unit;
			if(EventSetLeader != null)
			{
				EventSetLeader();
			}
		}
	}
	
	public virtual void RemoveUnit(Unit unit)
	{
		Units.Remove(unit);
	}

    public int GetQuantity()
    {
        int quantity = 0;

        foreach (Unit unit in Units)
        {
            quantity += unit.Quantity;
        }

        return quantity;
    }

    [Command(channel = 2)]
	public void CmdMove(Vector2 targetPosition)
	{
		
	}
	
	[Command]
	public void CmdCreateArmy()
	{
		
	}

	[Command]
	public void CmdUseAbility(long id)
	{

	}

    [Command]
    public void CmdInteract(NetworkInstanceId netid)
    {
    }

    [TargetRpc]
    private void TargetPickupBonus(NetworkConnection target)
    {
        SoundManager.PlaySoundUI(PickupSound);
    }

    public void Update()
	{
        if (Leader != null)
		{
            PlayerNameTransform.gameObject.SetActive(Leader.IsVisible && !Destroyed);

            PlayerNameTransform.position = Leader.transform.position;

            UpdateHPBar(Leader.CurrentHp, Leader.MaxHp);
        }
        else
        {
            PlayerNameTransform.gameObject.SetActive(false);
        }

    }

    public void Move(Vector2 target)
    {
        foreach (Unit unit in Units)
        {
            unit.Move(target);
        }
    }

    public void UpdateHPBar(float current, float max)
    {
        PlayerHPImage.fillAmount = current / max;
    }
}
