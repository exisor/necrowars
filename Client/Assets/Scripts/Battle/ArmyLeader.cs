﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine.Networking;

public class ArmyLeader : Unit
{
    [SyncVar]
    public int CurrentHp;
    [SyncVar]
    public int MaxHp;
}
