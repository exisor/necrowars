﻿using UnityEngine;
using System.Collections;
using Assets.Scripts.Battle;
using UnityEngine.UI;

public class PlayerArmy : Army 
{
	public Color PlayerColor;
	public Color[] EnemyColors;

	public bool IsPlayer;

	public override void OnStartLocalPlayer()
	{
        Debug.Log("OnStartLocalPlayer");
	    IsPlayer = true;
	}

    protected override string GetPlayerName(string playerName)
    {
        return playerName;
    }

    public override void Start () 
	{
		if(IsPlayer)
		{
			Color = PlayerColor;
			GameManager.Instance.SetPlayerArmy(this);

            PlayerHPBar.SetActive(false);
		} 
		else
		{
			Color = EnemyColors[UnityEngine.Random.Range(0, EnemyColors.Length)];
		}
		
		base.Start();
	}
}
