﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine.Networking;
using UnityEngine.Audio;
using Spine.Unity;
using DG.Tweening;

public class SubUnit : MonoBehaviour 
{	
	public delegate void EventAttackDelegate(float x);
	public Unit.UnitNameEnum UnitName;

    public GameObject RessurrectFX;
    public AudioClip AttackSound;
	
	public bool Alive = true;
    public bool IsVisible = false;

    private Rigidbody2D _rigidbody;
	private Collider2D _collider;
	public Unit Unit;
	public Animator Animator;
	public SpriteRenderer[] SpriteRenderers;
	public string[] SlotNames;
	public HintResurrect HintResurrect;
	public MeshRenderer MeshRenderer;
	public SkeletonAnimator SkeletonAnimator;
    public Transform EffectContainer;

    private float RotationDistance = 0.05f;
	private float StayVelocity = 0.2f;
	private float previosPositionX;
    private float _baseScale;

    public virtual void Awake()
	{
		_rigidbody = GetComponent<Rigidbody2D>();
		_collider = GetComponent<Collider2D>();

        previosPositionX = transform.position.x;
        _baseScale = Animator.transform.localScale.x;

        //Animator.ForceStateNormalizedTime(UnityEngine.Random.Range(0.0f, 1.0f));
    }

    public void UpdateScale(int quantity)
    {
        float s = (float)Math.Sqrt(quantity) * _baseScale;
        Animator.transform.localScale = new Vector3(s, s, s);
    }

    public void UpdateAlive(bool alive)
	{
		Alive = alive;
		AnimatorSetBool("Alive", Alive);
		
		_collider.enabled = Alive;
	    _rigidbody.constraints = Alive ? RigidbodyConstraints2D.FreezeRotation : RigidbodyConstraints2D.FreezeAll;
	    SetSortingOrder(Alive ? "SortingByY" : "Death");

        if(Alive)
        {
            HintResurrect.Show(false);
        }
	}

	public void SetColor(Color color)
	{
		foreach(SpriteRenderer sprite in SpriteRenderers)
		{
			sprite.color = color;
		}

		if(SkeletonAnimator != null)
		{
			foreach (string slotname in SlotNames)
			{
	         	foreach (Spine.Slot slot in SkeletonAnimator.skeleton.slots)
	         	{
                    if (slotname.Equals(slot.data.name))
		            {
                        slot.R = color.r;
		                slot.G = color.g;
		                slot.B = color.b;
		                slot.A = color.a;
		            }
	         	}
	      	}
		}
	}

    public void DestroyMinion()
    {
        EffectContainer.DetachChildren();
    }

    public void SetVisible(bool visible)
    {
        IsVisible = visible;

        gameObject.SetActive(IsVisible);

        if(IsVisible)
        {
            EffectContainer.SetParent(transform);
        }
        else
        {
            EffectContainer.SetParent(Unit.transform);
            _rigidbody.velocity = Vector3.zero;
        }
        EffectContainer.localPosition = Vector3.zero;
        AnimatorSetBool("Alive", Alive);
    }

	public void SetSortingOrder(string layer)
	{
		if(MeshRenderer != null)
		{
			MeshRenderer.sortingLayerName = layer;
		}
		foreach(SpriteRenderer sprite in SpriteRenderers)
		{
			sprite.sortingLayerName = layer;
		}
	}
	
	public void OnDie()
	{
        AnimatorSetTrigger("Death");
	}
	
	public void OnResurrect()
	{
        AnimatorSetTrigger("Resurrect");
        
        AddEffect(RessurrectFX);
	}
	
	public void OnReceiveDamage()
	{
		
	}

	public GameObject AddEffect(GameObject effectPrefab)
	{
        GameObject effect = PoolManager.Instance.GetEffect(effectPrefab.name);
        effect.transform.SetParent(EffectContainer);
        effect.transform.localPosition = Vector3.zero;
        effect.SetActive(true);
        return effect;
	}
	
	public void OnAttack(float targetPositionX, float targetPositionY)
	{
        if (!IsVisible) return;
        if (!Animator.GetCurrentAnimatorStateInfo(0).IsName("Attack"))
        {
            AnimatorSetTrigger("Attack");
        }

        /*
		GameObject shell = PoolManager.Instance.GetShell();
		shell.SetActive(true);
		shell.transform.position = transform.position;

		Vector3 target = new Vector3(targetPositionX, targetPositionY, transform.position.z);
		float time = Vector3.Distance(target, transform.position) * 0.1f;

		shell.transform.DOMove(target, time).OnComplete( ()=> PoolManager.Instance.ReturnShell(shell));*/

	    //SoundManager.PlaySound(AttackSound);
		Vector3 scale = transform.localScale;
		if(transform.position.x < targetPositionX)
		{
			scale.x = -1;
		}
		else
		{
			scale.x = 1;
		}
		transform.localScale = scale;
	}
	
	public void Update()
	{
		if(!Alive || !IsVisible || Animator.GetCurrentAnimatorStateInfo(0).IsName("Attack")) return;

        AnimatorSetBool("Walk", Math.Abs(_rigidbody.velocity.x) > StayVelocity || Math.Abs(_rigidbody.velocity.y) > StayVelocity);

		Vector3 scale = transform.localScale;
		float posX = transform.position.x;
		
		if((posX > previosPositionX && scale.x == 1)||
			(posX < previosPositionX && scale.x == -1))
		{
			if(Math.Abs(posX - previosPositionX) > RotationDistance)
			{
				scale.x = -scale.x;
			}
		} 
		else
		{
			previosPositionX = posX;
		}
		transform.localScale = scale;
	}

    public void SetToPosition(Vector3 targetPosition)
    {
        transform.position = targetPosition;
    }

    private void AnimatorSetBool(string state, bool value)
    {
        if(IsVisible)
        {
            Animator.SetBool(state, value);
        }
    }

    private void AnimatorSetTrigger(string state)
    {
        if (IsVisible)
        {
            Animator.SetTrigger(state);
        }
    }
}
