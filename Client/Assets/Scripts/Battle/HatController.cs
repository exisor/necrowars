﻿using UnityEngine;
using System.Collections;
using Spine;
using System.Collections.Generic;
using Spine.Unity;
using DG.Tweening;

public class HatController : MonoBehaviour
{
    public string[] InvisibleSlotNames;
    public Hat TopHat;
    public Hat DefaultHat;
    public Hat[] Hats;
    public GameObject HatContainer;
    public Color Color;
    public SkeletonAnimator SkeletonAnimator;

    public float Time = 1f;
    public Vector3 Strength = new Vector3(0, 0.2f, 0);
    public int Vibrato = 6;
    public float Randomness = 0f;

    private Hat _currentHat;
    private Dictionary<long, Hat> _hatsDictionary = new Dictionary<long, Hat>();

    void Awake()
    {
        for(int i = 0, n = Hats.Length; i < n; i++)
        {
            _hatsDictionary[Hats[i].Id] = Hats[i];
            Hats[i].gameObject.SetActive(false);
        }

        TopHat.gameObject.SetActive(false);
        if (DefaultHat != null)
        {
            DefaultHat.gameObject.SetActive(false);
        }

        Shake();
    }

    private void Shake()
    {
        HatContainer.transform.DOShakePosition(Time, Strength, Vibrato, Randomness, false, false).OnComplete(Shake);
    }

    public void SetTopHat()
    {
        if(_currentHat != null)
        {
            _currentHat.gameObject.SetActive(false);
        }
        _currentHat = TopHat;
        _currentHat.gameObject.SetActive(true);
        _currentHat.SetColor(Color);
        SetDefaultHatVisible(false);
    }

    public void SetHat(long id)
    {
        if (_currentHat != null)
        {
            _currentHat.gameObject.SetActive(false);
        }
        _currentHat = _hatsDictionary[id];
        _currentHat.gameObject.SetActive(true);
        _currentHat.SetColor(Color);
        SetDefaultHatVisible(false);
    }

    public void SetDefault()
    {
        if (_currentHat != null)
        {
            _currentHat.gameObject.SetActive(false);
        }
        SetDefaultHatVisible(true);

        if (DefaultHat != null)
        {
            _currentHat = DefaultHat;
            _currentHat.gameObject.SetActive(true);
            _currentHat.SetColor(Color);
        }
    }

    public void SetVisible(bool visible)
    {
        HatContainer.gameObject.SetActive(visible);
    }

    private void SetDefaultHatVisible(bool visible)
    {
        if(SkeletonAnimator != null)
        {
            foreach (string slotname in InvisibleSlotNames)
            {
                foreach (Spine.Slot slot in SkeletonAnimator.skeleton.slots)
                {
                    if (slotname.Equals(slot.data.name))
                    {
                        slot.IsVisible = visible;
                    }
                }
            }
        }
    }

}
