﻿using System.Collections.Generic;
using UnityEngine;

public class AnimatorHelper
{
    private readonly Animator _a;
    private bool _visible;
    private readonly Dictionary<string, bool> _bools = new Dictionary<string, bool>();
    public AnimatorHelper(Animator a)
    {
        _a = a;
    }

    public void AnimatorSetBool(string state, bool value)
    {
        _bools[state] = value;
        if (Visible)
            _a.SetBool(state, value);
    }

    public void AnimatorSetTrigger(string state)
    {
        if (Visible)
            _a.SetTrigger(state);
    }

    public bool Visible
    {
        get { return _visible; }
        set
        {
            if (value == _visible) return;
            _visible = value;
            _a.enabled = value;
            if (value) FlushState();
        }
    }

    private void FlushState()
    {
        foreach (var kvp in _bools)
        {
            _a.SetBool(kvp.Key, kvp.Value);
        }
    }
}