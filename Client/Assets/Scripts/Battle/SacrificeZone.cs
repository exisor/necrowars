﻿using UnityEngine;
using DG.Tweening;
using UnityEngine.Networking;

public class SacrificeZone : NetworkBehaviour
{
    static private int NEED_ALIVE_UNITS = 75;
    public delegate void EventUnitSacrificedDelegate(Vector2 v);

    [SyncVar(hook = "UpdateRadius")]
    private float _radius;
    [SyncEvent]
    public event EventUnitSacrificedDelegate EventUnitSacrificed;

    public CircleCollider2D Collider;
    public NetworkIdentity NetworkIdentity;
    public GameObject EffectPrefab;
    public Transform Circle;

    private RectTransform _indicator;
    private GameObject _button;
    private GameObject _text;
    private RectTransform _canvasRect;

    private bool _inZone = false;

    void Awake()
    {
        _indicator = UIManager.Instance.GetScreen<BattleScreen>(GameScreen.ScreenNameEnum.Battle).SacrificeZoneIndicator;
        _button = UIManager.Instance.GetScreen<BattleScreen>(GameScreen.ScreenNameEnum.Battle).SacrificeButton;
        _text = UIManager.Instance.GetScreen<BattleScreen>(GameScreen.ScreenNameEnum.Battle).SacrificeText;
        _canvasRect = UIManager.Instance.Canvas.GetComponent<RectTransform>();
    }

    public void Start()
    {
        EventUnitSacrificed += OnSacrifice;

        UpdateRadius(_radius);

        UIManager.Instance.GetScreen<BattleScreen>(GameScreen.ScreenNameEnum.Battle).SacrificeZoneId = NetworkIdentity.netId;

        RotateCircle();
    }

    private void RotateCircle()
    {
        Circle.DOLocalRotate(new Vector3(-50, 0, Circle.localEulerAngles.z == 0 ? 180 : 0), 5).OnComplete(RotateCircle);
    }

    public override void OnNetworkDestroy()
    {
        EventUnitSacrificed -= OnSacrifice;
    }

    public void UpdateRadius(float radius)
    {
        _radius = radius;
        transform.localScale = new Vector3(_radius, _radius, _radius);
    }

    void OnTriggerEnter2D(Collider2D other)
    {
        _inZone = true;
    }

    void OnTriggerExit2D(Collider2D other)
    {
        _inZone = false;
    }

    public void OnSacrifice(Vector2 v)
    {
        GameObject effect = PoolManager.Instance.GetEffect(EffectPrefab.name);
        effect.transform.position = v;
        effect.SetActive(true);
    }

    [TargetRpc]
    public void TargetArmySacrificed(NetworkConnection conn, long exp, long gold, long points)
    {
        UIManager.Instance.ShowMessage("BattleSacrificeMessage", gold.ToString(), exp.ToString(), points.ToString());
    }

    void Update()
    {
        bool needAliveUnits = false;
        if(GameManager.Instance.PlayerArmy != null)
        {
            needAliveUnits = GameManager.Instance.PlayerArmy.AliveUnits >= NEED_ALIVE_UNITS;
        }
        _button.SetActive(_inZone && needAliveUnits);
        _text.SetActive(_inZone && !needAliveUnits);
        _indicator.gameObject.SetActive(!_inZone && needAliveUnits);

        if (_inZone)
        {
            return;
        }

        Vector3 targetPosOnScreen = Camera.main.WorldToScreenPoint(transform.position);
        Vector3 center = new Vector3(Screen.width / 2f, Screen.height / 2f, 0);
        float angle = Mathf.Atan2(targetPosOnScreen.y - center.y, targetPosOnScreen.x - center.x) * Mathf.Rad2Deg;
        if (angle < 0) angle = angle + 360;

        Vector2 ViewportPosition = Camera.main.ScreenToViewportPoint(MathUtils.GetPoint(center, targetPosOnScreen));
        Vector2 newPosition = new Vector2(
        ((ViewportPosition.x * _canvasRect.sizeDelta.x) - (_canvasRect.sizeDelta.x * 0.5f)),
        ((ViewportPosition.y * _canvasRect.sizeDelta.y) - (_canvasRect.sizeDelta.y * 0.5f)));

        if (Vector2.Distance(newPosition, _indicator.anchoredPosition) > 20)
        {
            _indicator.anchoredPosition = newPosition;
        }
        else
        {
            _indicator.anchoredPosition = Vector2.Lerp(_indicator.anchoredPosition, newPosition, Time.deltaTime * 15f);
        }

        _indicator.eulerAngles = new Vector3(0, 0, angle);
    }
}
