﻿using UnityEngine;
using System.Collections;
using System;
using System.Security;
using UnityEngine.Networking;
using UnityEngine.Events;
using DG.Tweening;

public class Unit : NetworkBehaviour 
{
    public enum DamageType {Normal, Unblockable, Spell};
	public enum UnitNameEnum {Necromant, Stoporom, Sokorokom, Bochka, Ovza, Kaban, Smolotom};
	public delegate void EventAttackDelegate(float x, float y);
        
	public UnitNameEnum UnitName;

	#region Network state
        [SyncVar(hook = "UpdateAlive")]
        public bool Alive;
		[SyncVar(hook = "UpdateArmy")]
        public NetworkInstanceId ArmyId;
        [SyncVar(hook = "UpdateQuantity")]
        public int Quantity;
        [SyncEvent]
        public event Action EventDie;
        [SyncEvent]
        public event EventAttackDelegate EventAttack;
        [SyncEvent]
        public event Action EventDamageReceived;
        [SyncEvent]
        public event Action EventResurrect;
    #endregion
    public UnityEvent EventSetArmy;

    public Army Army;
	public int AliveQuantity;
    public int TutorialSpeed = 200;
    public bool IsVisible = false;
    public bool IsRender = true;

    public GameObject RessurrectFX;
    public Animator Animator;
    public SpriteRenderer[] SpriteRenderers;
    public HintResurrect HintResurrect;
    public HatController HatController;
    public UnitEffectController UnitEffectController;
    public Transform ScalingContainer;

    public GameObject ShellPrefab;
    public Transform ShellStart;
    public float ShellSpeed = 0.2f;
    public bool IsScaleShell;

    public Color DeadColor;
    public Color DamageColor;

    private SpriteRenderer _spriteRenderer;
    private Rigidbody2D _rigidbody;
    private SortingObjectByY _sorting;
    private CircleCollider2D _collider;
	private NetworkTransform _networkTransform;

    public float RotationDistance = 0.05f;
    public float StayVelocity = 0.2f;
    private float _previosPositionX;
    private AnimatorHelper _anim;
    private Transform _viewTransform;
    private float _viewScaleX;
    private int _random;
    public virtual void Awake()
	{
        _anim = new AnimatorHelper(Animator);
		_rigidbody = GetComponent<Rigidbody2D>();
		_networkTransform = GetComponent<NetworkTransform>();
        _sorting = GetComponent<SortingObjectByY>();
        _viewTransform = Animator.gameObject.transform;
        _spriteRenderer = Animator.GetComponent<SpriteRenderer>();
        _viewScaleX = _viewTransform.localScale.x;

        _previosPositionX = transform.position.x;
        _random = UnityEngine.Random.Range(0, 5);
    }
	
	public void Start()
	{
        UpdateQuantity(Quantity);
        UpdateAlive(Alive);
		UpdateArmy(ArmyId);
        SetVisible(IsVisible);

        EventAttack += OnAttack;
		EventDie += OnDie;
		EventResurrect += OnResurrect;
        EventDamageReceived += OnReceiveDamage;
    }

	public override void OnNetworkDestroy()
	{
		EventAttack -= OnAttack;
		EventDie -= OnDie;
		EventResurrect -= OnResurrect;
        EventDamageReceived -= OnReceiveDamage;

        UnitEffectController.DestroyController();
        if (Army != null)
		{
			Army.RemoveUnit(this);
		}
	}

    public void SetVisible(bool visible)
    {
        IsVisible = visible && IsRender;

        Animator.gameObject.SetActive(IsVisible);
        UnitEffectController.SetVisible(IsVisible);
        _anim.Visible = IsVisible;
        _sorting.enabled = IsVisible;
    }

    public void UpdateQuantity(int quantity)
	{
		Quantity = quantity;

        float s = (float)Math.Sqrt(Quantity);
        ScalingContainer.localScale = new Vector3(s, s, s);
	}

	public void UpdateAlive(bool alive)
	{
		Alive = alive;

        _anim.AnimatorSetBool("Alive", Alive);

        _networkTransform.enabled = Alive;
        _rigidbody.constraints = Alive ? RigidbodyConstraints2D.FreezeRotation : RigidbodyConstraints2D.FreezeAll;
        SetSortingOrder(Alive ? "SortingByY" : "Death");

        UpdateColor();

        if (Alive)
        {
            HintResurrect.Show(false);
        }
        if (HatController != null)
        {
            HatController.SetVisible(Alive);
        }
    }

    public void UpdateColor()
    {
        Color color = !Alive && Army != null && !Army.Destroyed ? DeadColor : Color.white;
        _spriteRenderer.color = color;
    }

    public void UpdateVisial(Color color, long hatId)
    {
        UpdateColor();
        SetColor(color, SpriteRenderers);

        if (HatController != null)
        {
            HatController.Color = color;

            if (hatId != 0)
            {
                HatController.SetHat(hatId);
            }
            else if(Army != null && Army == GameManager.Instance.TopArmy)
            {
                HatController.SetTopHat();
            }
            else
            {
                HatController.SetDefault();
            }
        }
    }

    public void SetColor(Color color, SpriteRenderer[] sprites)
    {
        foreach (SpriteRenderer sprite in sprites)
        {
            sprite.color = color;
        }
    }

    public void SetSortingOrder(string layer)
    {
        foreach (SpriteRenderer sprite in SpriteRenderers)
        {
            sprite.sortingLayerName = layer;
        }
    }

    public void UpdateArmy(NetworkInstanceId armyId)
	{
	    GameObject go = ClientScene.FindLocalObject(armyId);

        if (Army != null)
        {
            Army.RemoveUnit(this);
        }

        if (go != null)
        {
            go.GetComponent<Army>().AddUnit(this);
        }
        else
        {
            GameManager.Instance.AddNeedArmyUnit(ArmyId, armyId, this);
        }

        ArmyId = armyId;
    }

	public GameObject AddEffect(GameObject effectPrefab, UnitEffectController.UnitEffectPosition position = UnitEffectController.UnitEffectPosition.Center)
	{
        return UnitEffectController.AddEffect(effectPrefab, position);
    }

	public void HideHintResurrect()
	{
        HintResurrect.Show(false);
    }

	public void ShowHintResurrect()
	{
        HintResurrect.Show(UnitName != UnitNameEnum.Necromant && Army != null && Army.Destroyed && !Alive);
    }
	
	public void OnDie()
	{
        if (Animator.GetCurrentAnimatorStateInfo(0).IsName("Diyng") ||
            Animator.GetCurrentAnimatorStateInfo(0).IsName("Die"))
        {
            return;
        }
        _anim.AnimatorSetTrigger("Death");
    }
	
	public void OnResurrect()
	{
        _anim.AnimatorSetTrigger("Resurrect");

        if(IsVisible)
        {
            AddEffect(RessurrectFX);
        }
    }
	
	public void OnReceiveDamage()
	{
        
    }
	
	public void OnAttack(float targetPositionX, float targetPositionY)
	{
        Vector3 scale = transform.localScale;
        if (transform.position.x < targetPositionX)
        {
            scale.x = -1;
        }
        else
        {
            scale.x = 1;
        }
        transform.localScale = scale;

        if (Animator.GetCurrentAnimatorStateInfo(0).IsName("Attack"))
        {
            return;
        }

        _anim.AnimatorSetTrigger("Attack");

        if (ShellPrefab != null && IsVisible)
        {
            GameObject shell = PoolManager.Instance.GetShell(ShellPrefab.name);
            shell.SetActive(true);
            if(ShellStart != null)
            {
                shell.transform.position = ShellStart.position;
            }
            else
            {
                shell.transform.position = transform.position;
            }

            float s = (float)Math.Sqrt(Quantity) * ShellPrefab.transform.localScale.x;
            shell.transform.localScale = new Vector3(s, s, s);

            Vector3 target = new Vector3(targetPositionX, targetPositionY, transform.position.z);
            Vector3 rot = shell.transform.localEulerAngles;
            rot.z += 90;
            float time = Vector3.Distance(target, transform.position) * ShellSpeed;

            shell.transform.DORotate(rot, time);
            if (IsScaleShell)
            {
                shell.transform.DOMove(target, time);
                s *= 2f;
                shell.transform.DOScale(new Vector3(s, s, s), 0.2f).SetDelay(time).OnComplete(() => PoolManager.Instance.ReturnShell(ShellPrefab.name, shell));
            }
            else
            {
                shell.transform.DOMove(target, time).OnComplete(() => PoolManager.Instance.ReturnShell(ShellPrefab.name, shell));
            }
        }
    }

    public void FixedUpdate()
    {
        if (Time.frameCount % 5 == _random)
        {
            Vector3 screenPoint = Camera.main.WorldToViewportPoint(transform.position);
            bool isVisible = screenPoint.x > -0.1 && screenPoint.y > -0.1 && screenPoint.x < 1.1 && screenPoint.y < 1.1;

            if (isVisible != IsVisible)
            {
                SetVisible(isVisible);
            }
        }

        if (!Alive || Animator.GetCurrentAnimatorStateInfo(0).IsName("Attack")) return;

        if(IsVisible)
        {
            _anim.AnimatorSetBool("Walk", Math.Abs(_rigidbody.velocity.x) > StayVelocity || Math.Abs(_rigidbody.velocity.y) > StayVelocity);

            if ((transform.position.x > _previosPositionX && transform.localScale.x == 1) ||
                (transform.position.x < _previosPositionX && transform.localScale.x == -1))
            {
                if (Math.Abs(transform.position.x - _previosPositionX) > RotationDistance)
                {
                    Vector3 scale = transform.localScale;
                    scale.x = -scale.x;
                    transform.localScale = scale;
                }
            }
            else
            {
                _previosPositionX = transform.position.x;
            }
        }
    }

    public void Move(Vector3 targetPosition)
    {
        targetPosition.z = transform.position.z;
        Vector3 direction = targetPosition - transform.position;
        _rigidbody.AddForce(direction * TutorialSpeed * Time.deltaTime, ForceMode2D.Force);
        //_rigidbody.velocity = direction * TutorialSpeed * Time.deltaTime;
    }
}
