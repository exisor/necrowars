﻿using UnityEngine;
using System.Collections;

public class LobbyManager : MonoBehaviour 
{
	public static LobbyManager Instance = null;
	
	void Awake () 
	{
		Instance = this;
		ApplicationManager.Instance.LoadCallback();
	}
}
