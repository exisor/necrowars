﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Tutorial11Phase : MonoBehaviour
{
    public Button OkButton;
    public GameObject Refund;

    public void OnEnable()
    {
        OkButton.onClick.AddListener(OnClick);
        Refund.SetActive(false);
    }

    public void OnDisable()
    {
        Refund.SetActive(true);
    }

    private void OnClick()
    {
        Tutorial.NextPhase();
        OkButton.onClick.RemoveListener(OnClick);
    }
}
