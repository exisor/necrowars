﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Tutorial10Phase : MonoBehaviour
{
    public Button OkButton;
    public GameObject Refund;
    public void OnEnable()
    {
        OkButton.gameObject.SetActive(false);
        Refund.SetActive(false);
    }

    public void OnDisable()
    {
        Refund.SetActive(true);
    }
}
