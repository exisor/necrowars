﻿using UnityEngine;
using System.Collections.Generic;
using UnityEngine.UI;

public class Tutorial12Phase : MonoBehaviour
{
    public RectTransform ForgeButton;

    public List<Button> BlockButtons = new List<Button>();

    public void OnEnable()
    {
        ForgeButton.gameObject.AddComponent<BlinkChildrenEffect>();

        ForgeButton.GetComponent<Button>().onClick.AddListener(OnClick);

        foreach (var block in BlockButtons)
        {
            block.interactable = false;
        }
    }

    public void OnDisable()
    {
        Destroy(ForgeButton.GetComponent<BlinkChildrenEffect>());

        foreach (var block in BlockButtons)
        {
            block.interactable = true;
        }
    }

    private void OnClick()
    {
        Tutorial.NextPhase();
        ForgeButton.GetComponent<Button>().onClick.RemoveListener(OnClick);
    }
}
