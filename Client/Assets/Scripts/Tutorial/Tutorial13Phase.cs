﻿using UnityEngine;
using System.Collections.Generic;
using UnityEngine.UI;

public class Tutorial13Phase : MonoBehaviour
{
    public ItemSlot ForgeSlot;

    public List<Button> BlockButtons = new List<Button>();

    private BlinkEffect _BlinkEffect;

    public void OnEnable()
    {
        ForgeSlot.EventChangeItem += OnChangeForge;
        _BlinkEffect = ForgeSlot.gameObject.AddComponent<BlinkEffect>();

        foreach (var block in BlockButtons)
        {
            block.gameObject.SetActive(false);
        }
    }

    public void OnDisable()
    {
        ForgeSlot.EventChangeItem -= OnChangeForge;

        foreach (var block in BlockButtons)
        {
            block.gameObject.SetActive(true);
        }
    }

    private void OnChangeForge(ItemSlot itemSlot, ItemView newItem, ItemView oldItem)
    {
        if (newItem != null)
        {
            Destroy(_BlinkEffect);
            Tutorial.NextPhase();
        }
    }
}
