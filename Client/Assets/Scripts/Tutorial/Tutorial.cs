﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

public class Tutorial : MonoBehaviour
{
    [Serializable]
    public class TutorialPhase
    {
        public List<GameObject> TutorialObjects = new List<GameObject>();

        public bool NeedSave = false;

        public GameScreen.ScreenNameEnum Screen = GameScreen.ScreenNameEnum.Main;

        public void SetVisible(bool visible)
        {
            foreach(var obj in TutorialObjects)
            {
                obj.SetActive(visible);
            }
        }
    }

    public static Tutorial Instance = null;

    public List<TutorialPhase> Phases = new List<TutorialPhase>();

    private TutorialPhase _currentPhase;
    private int _currentPhaseIndex = 0;

    void Awake()
    {
        Instance = this;
    }
    public static bool IsMainTutorialComplete()
    {
        return Instance.IsMainTutorailCompleteInternal();
    }

    public static int GetPhase()
    {
        return Instance.GetPhaseInternal();
    }

    public static void SetPhase(int phase)
    {
        if (phase == 5)
        {
            Instance.SetPhaseInternal(7);
        }
        else
        {
            Instance.SetPhaseInternal(phase);
        }
        //Instance.SetPhaseInternal(Instance.Phases.Count - 1);
        //Instance.SetPhaseInternal(0);
    }

    public static bool IsFirstBattle()
    {
        return Instance.GetPhaseInternal() == 0;
    }

    public static GameScreen.ScreenNameEnum GetGameScreen()
    {
        if (!Instance.IsCompleteInternal())
        {
            return Instance.Phases[GetPhase()].Screen;
        }
        return GameScreen.ScreenNameEnum.Main;
    }

    public static bool IsRealBattleComplete()
    {
        return Instance.GetPhaseInternal() == 10;
    }

    public static bool IsAbilityScreenPhase()
    {
        return Instance.GetPhaseInternal() == 11;
    }

    public static bool IsForgePhase()
    {
        return Instance.GetPhaseInternal() == 14;
    }

    public static void NextPhase()
    {
        Debug.Log("NextPhase");
        Instance.NextPhaseInternal();

        Analytics.gua.sendEventHit("Tutorial", "Phase", GetPhase().ToString(), (int)DataManager.Instance.GetCurrentCharacterId());
        if (Instance.IsCompleteInternal() || Instance.Phases[GetPhase()].NeedSave)
        {
            SocketManager.Instance.CompleteTutorialPhase(GetPhase());
        }
    }

    public bool IsMainTutorailCompleteInternal()
    {
        return _currentPhaseIndex >= 14;
    }

    public bool IsCompleteInternal()
    {
        return _currentPhaseIndex >= Phases.Count;
    }

    public int GetPhaseInternal()
    {
        return _currentPhaseIndex;
    }

    public void NextPhaseInternal()
    {
        SetPhaseInternal(_currentPhaseIndex + 1);
    }

    public void SetPhaseInternal(int index)
    {
        if (_currentPhase != null)
        {
            _currentPhase.SetVisible(false);
            _currentPhase = null;
        }

        _currentPhaseIndex = index;
        
        if (!IsCompleteInternal())
        {
            _currentPhase = Phases[_currentPhaseIndex];
            _currentPhase.SetVisible(true);
        }
    }
}
