﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using DG.Tweening;

public class Tutorial6Phase : MonoBehaviour
{
    public float EdgeColorStrength = 0.1f;
    public ItemSlot WeaponSlot;
    public Button OkButton;

    private BlinkEffect _BlinkEffect;
    public void OnEnable()
    {
        OkButton.gameObject.SetActive(false);
        WeaponSlot.EventChangeItem += OnChangeWeapon;
        _BlinkEffect = WeaponSlot.gameObject.AddComponent<BlinkEffect>();
    }

    private void OnChangeWeapon(ItemSlot itemSlot, ItemView newItem, ItemView oldItem)
    {
        WeaponSlot.EventChangeItem -= OnChangeWeapon;

        if (newItem != null)
        {
            Destroy(_BlinkEffect);
            OkButton.gameObject.SetActive(true);
            Tutorial.NextPhase();
        }
    }

}
