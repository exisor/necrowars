﻿using UnityEngine;
using System.Collections.Generic;
using UnityEngine.UI;

public class Tutorial15Phase : MonoBehaviour
{
    public ItemSlot ForgeSlot;
    public RectTransform EnchantButton;

    public List<Button> BlockButtons = new List<Button>();

    public void OnEnable()
    {
        ForgeSlot.EventChangeItem += OnChangeForge;

        EnchantButton.gameObject.AddComponent<BlinkChildrenEffect>();
        EnchantButton.GetComponent<Button>().onClick.AddListener(OnEnchantClick);

        foreach (var block in BlockButtons)
        {
            block.gameObject.SetActive(false);
        }
    }

    public void OnDisable()
    {
        ForgeSlot.EventChangeItem -= OnChangeForge;

        foreach (var block in BlockButtons)
        {
            block.gameObject.SetActive(true);
        }
    }

    private void OnChangeForge(ItemSlot itemSlot, ItemView newItem, ItemView oldItem)
    {
        if (newItem != null)
        {
            foreach (var block in BlockButtons)
            {
                block.gameObject.SetActive(false);
            }
        }
    }

    private void OnEnchantClick()
    {
        if (ForgeSlot.ItemView != null)
        {
            Destroy(EnchantButton.GetComponent<BlinkChildrenEffect>());
            Tutorial.NextPhase();
            EnchantButton.GetComponent<Button>().onClick.RemoveListener(OnEnchantClick);
        }
    }
}
