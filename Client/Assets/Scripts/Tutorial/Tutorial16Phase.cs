﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Tutorial16Phase : MonoBehaviour
{
    public Button OkButton;

    public void OnEnable()
    {
        OkButton.onClick.AddListener(OnClick);
    }

    private void OnClick()
    {
        Tutorial.NextPhase();
        OkButton.onClick.RemoveListener(OnClick);
    }
}
