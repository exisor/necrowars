﻿using UnityEngine;
using System.Collections;

public class TutorialCollision : MonoBehaviour
{

    void OnCollisionEnter2D(Collision2D coll)
    {
        TutorialGameManager.Instance.CompleteTutorialPhase();
    }
}
