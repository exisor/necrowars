﻿using UnityEngine;
using System.Collections.Generic;
using UnityEngine.UI;

public class Tutorial8Phase : MonoBehaviour
{
    public List<Button> BlockButtons = new List<Button>();
    public List<GameObject> AbilityButtons = new List<GameObject>();
    public GameObject BlockImage;
    public Transform AbilitysPanel;

    private int _siblingIndex;

    public void OnEnable()
    {
        UIManager.Instance.GetPopup<AbilitySelectPopup>(Popup.PopupNameEnum.AbilitySelect).EventAbilitySelect += OnAbilitySelect;
        UIManager.Instance.GetPopup<AbilitySelectPopup>(Popup.PopupNameEnum.AbilitySelect).EventAbilityShow += OnAbilityShow;

        foreach (var block in BlockButtons)
        {
            block.interactable = false;
        }

        foreach (var ability in AbilityButtons)
        {
            ability.AddComponent<BlinkChildrenEffect>();
        }

        BlockImage.gameObject.SetActive(true);

        _siblingIndex = AbilitysPanel.GetSiblingIndex();
        AbilitysPanel.SetAsLastSibling();
    }

    public void OnDisable()
    {
        foreach (var block in BlockButtons)
        {
            block.interactable = true;
        }

        foreach (var abilityView in UIManager.Instance.GetPopup<AbilitySelectPopup>(Popup.PopupNameEnum.AbilitySelect).GetAbilityViews())
        {
            Destroy(abilityView.gameObject.GetComponent<BlinkChildrenEffect>());
        }

        
    }

    private void OnAbilityShow()
    {
        UIManager.Instance.GetPopup<AbilitySelectPopup>(Popup.PopupNameEnum.AbilitySelect).EventAbilityShow -= OnAbilityShow;

        BlockImage.gameObject.SetActive(false);

        foreach (var ability in AbilityButtons)
        {
            Destroy(ability.GetComponent<BlinkChildrenEffect>());
        }

        foreach (var abilityView in UIManager.Instance.GetPopup<AbilitySelectPopup>(Popup.PopupNameEnum.AbilitySelect).GetAbilityViews())
        {
            abilityView.gameObject.AddComponent<BlinkChildrenEffect>();
        }
    }

    private void OnAbilitySelect(AbilityClient selectedAbilityClient)
    {
        AbilitysPanel.SetSiblingIndex(_siblingIndex);
        UIManager.Instance.GetPopup<AbilitySelectPopup>(Popup.PopupNameEnum.AbilitySelect).EventAbilitySelect -= OnAbilitySelect;

        Tutorial.NextPhase();
    }
}
