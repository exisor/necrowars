﻿using UnityEngine;
using System.Collections;

public class TutorialHPPotion : MonoBehaviour
{
    public void Start()
    {
        UIManager.Instance.GetScreen<BattleScreen>(GameScreen.ScreenNameEnum.Battle).SetConsumableAbilityInSlot(gameObject.GetComponent<HPPotion>());
    }
}
