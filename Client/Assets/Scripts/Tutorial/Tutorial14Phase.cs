﻿using UnityEngine;
using System.Collections.Generic;
using UnityEngine.UI;

public class Tutorial14Phase : MonoBehaviour
{
    public ItemSlot ForgeSlot;
    public RectTransform UpgradeButton;

    public List<Button> BlockButtons = new List<Button>();

    public void OnEnable()
    {
        UpgradeButton.gameObject.AddComponent<BlinkChildrenEffect>();
        UpgradeButton.GetComponent<Button>().onClick.AddListener(OnUpgradeClick);

        foreach (var block in BlockButtons)
        {
            block.gameObject.SetActive(false);
        }
    }

    public void OnDisable()
    {
        foreach (var block in BlockButtons)
        {
            block.gameObject.SetActive(true);
        }
    }

    private void OnUpgradeClick()
    {
        if (ForgeSlot.ItemView != null)
        {
            Destroy(UpgradeButton.GetComponent<BlinkChildrenEffect>());
            Tutorial.NextPhase();
            UpgradeButton.GetComponent<Button>().onClick.RemoveListener(OnUpgradeClick);
        }
    }
}
