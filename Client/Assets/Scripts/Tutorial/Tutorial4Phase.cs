﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Tutorial4Phase : MonoBehaviour
{
    public Button OkButton;
    public Button ClosePopupButton;
    public GameObject[] BlockImages;
    public void OnEnable()
    {
        OkButton.gameObject.SetActive(false);
        ClosePopupButton.gameObject.SetActive(false);

        foreach(var image in BlockImages)
        {
            image.SetActive(false);
            image.transform.SetParent(transform);
        }

        OkButton.onClick.AddListener(OnClick);
        ClosePopupButton.onClick.AddListener(OnClick);
    }

    public void OnOpenChest()
    {
        foreach (var image in BlockImages)
        {
            image.SetActive(true);
            image.transform.SetParent(transform.parent);
        }

        OkButton.transform.SetAsLastSibling();
        ClosePopupButton.transform.SetAsLastSibling();

        OkButton.gameObject.SetActive(true);
        ClosePopupButton.gameObject.SetActive(true);
        Tutorial.NextPhase();
    }

    private void OnClick()
    {
        for (int i = 0; i < BlockImages.Length; i++)
        {
            BlockImages[i].SetActive(false);
            BlockImages[i].transform.SetParent(transform);
            BlockImages[i].transform.SetSiblingIndex(i);
        }

        OkButton.onClick.RemoveListener(OnClick);
        ClosePopupButton.onClick.RemoveListener(OnClick);
    }
}
