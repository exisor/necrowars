﻿using UnityEngine;
using System.Collections.Generic;
using UnityEngine.UI;

public class Tutorial1Phase : MonoBehaviour
{
    public RectTransform BattleButton;

    public List<Button> BlockButtons = new List<Button>();

    private int _siblingIndex;

    public void OnEnable()
    {
        BattleButton.gameObject.AddComponent<BlinkChildrenEffect>();

        BattleButton.GetComponent<Button>().onClick.AddListener(OnClick);

        foreach(var block in BlockButtons)
        {
            block.interactable = false;
        }

        _siblingIndex = BattleButton.GetSiblingIndex();
        BattleButton.SetAsLastSibling();
    }

    public void OnDisable()
    {
        Destroy(BattleButton.GetComponent<BlinkChildrenEffect>());

        foreach (var block in BlockButtons)
        {
            block.interactable = true;
        }

        
    }

    private void OnClick()
    {
        BattleButton.SetSiblingIndex(_siblingIndex);
        Tutorial.NextPhase();
        BattleButton.GetComponent<Button>().onClick.RemoveListener(OnClick);
    }
}
