﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;

public class Tutorial17Phase : MonoBehaviour
{
    public GameObject Valkyrie;
    public GameObject ValkyrieAttack;

    public void OnClick()
    {
        StartCoroutine(NextTutorialPhase());

        Valkyrie.SetActive(false);
        ValkyrieAttack.SetActive(true);
    }

    IEnumerator NextTutorialPhase()
    {
        yield return new WaitForSeconds(0.9f);

        Tutorial.NextPhase();
        UIManager.Instance.MainMenuView.ShowMain();

        UIManager.Instance.ShowPopup(Popup.PopupNameEnum.Offer);
        UIManager.Instance.GetPopup<OfferPopup>(Popup.PopupNameEnum.Offer).ShowOffer(Localization.GetString("OFFER_START_PACK"), new List<long>() { 26 });
    }
}
