﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Tutorial5Phase : MonoBehaviour
{
    public Button ChestPopupButton;

    public void OnEnable()
    {
        ChestPopupButton.onClick.AddListener(OnClick);
    }

    public void OnClick()
    {
        Tutorial.NextPhase();
        UIManager.Instance.ShowScreen(GameScreen.ScreenNameEnum.Inventory);
        ChestPopupButton.onClick.RemoveListener(OnClick);
    }
}
