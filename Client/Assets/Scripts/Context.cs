﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using UnityEngine;
using Debug = UnityEngine.Debug;

namespace Assets.Scripts
{
    public enum ContextType
    {
        Default,
        Lobby,
        Battle,
        Debug,
        Inventory,
        MainScreen,
        Login,
        Abilities,
        Options,
        Shop,
        Forge,
        ChestPopup,
        AbilitySelect,
        Error,
        Leaderboard,
        Profile,
        Notification,
        DailyReward,
        Achievement,
        AchievementReward,
        QuestReward,
        PlayerEvent,
        Confirmation,
        ShopBonus,
        Offer
    }


    public enum ContextSwitch
    {
        New,
        Push,
        Pop
    }
    public class Context
    {
        private static Context _default;
        public static Context Default
        {
            get
            {
                return _default ?? (_default = new Context {Type = ContextType.Default});
            }
        }
        private static Context _current;
        private static readonly Stack<Context> _stackedContexts = new Stack<Context>();
        public static Context Current
        {
            get { return _current ?? Default; }
            private set { _current = value; }
        }

        public ContextType Type { get; private set; }

        public static void Switch(ContextType type, ContextSwitch switchType = ContextSwitch.New)
        {
            switch (switchType)
            {
                case ContextSwitch.New:
                    Current = new Context { Type = type };
                    _stackedContexts.Clear();
                    break;
                case ContextSwitch.Push:
                    Push(type);
                    break;
                case ContextSwitch.Pop:
                    Pop();
                    break;
            }
            OnSwitched();
        }

        public static void Push(ContextType type)
        {
            if (Current != null)
            {
                _stackedContexts.Push(Current);
            }
            Current = new Context { Type = type };
        }

        public static void Pop()
        {
            try
            {
                Current = _stackedContexts.Pop();
            }
            catch (Exception)
            {
                Current = Default;
            }
        }

        public static Context FindContext(ContextType type)
        {
            return _stackedContexts.FirstOrDefault(c => c.Type == type);
        }

        public static event Action Switched;

        private static void OnSwitched()
        {
            var handler = Switched;
            if (handler != null) handler();
        }

        public static implicit operator ContextType(Context context)
        {
            return context.Type;
        }
    }
}
