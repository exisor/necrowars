﻿using UnityEngine;
using Spine.Unity;

public class ResetPoseBehaviour : StateMachineBehaviour
{
   	public override void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
   	{
   		var skeletonAnimator = animator.gameObject.GetComponent<SkeletonAnimator>();

      	if (skeletonAnimator == null)
         	return;

        skeletonAnimator.skeleton.SetToSetupPose();
    }
   
}