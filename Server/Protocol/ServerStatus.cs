﻿using ProtoBuf;

namespace Protocol
{
    [ProtoContract]
    public enum ServerStatus : byte
    {
        Initializing = 0,
        Initialized,
        Starting, 
        Started,
        Error,
        ShuttingDown,
        Down
    }
}