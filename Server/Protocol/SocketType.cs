﻿
using ProtoBuf;

namespace Protocol
{
    [ProtoContract]
    public enum SocketType : byte
    {
        Tcp,
        WebSocket,
    }
}
