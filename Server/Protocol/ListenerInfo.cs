﻿using ProtoBuf;

namespace Protocol
{
    [ProtoContract]
    public class ListenerInfo : IListenerInfo
    {
        [ProtoMember(1)]
        public string Address { get; set; }
        [ProtoMember(2)]
        public int Port { get; set; }
        [ProtoMember(3)]
        public ListenerType Type { get; set; }
        [ProtoMember(4)]
        public int MaxClients { get; set; }
        [ProtoMember(5)]
        public string Name { get; set; }
        [ProtoMember(6)]
        public SocketType SocketType { get; set; }
    }
}