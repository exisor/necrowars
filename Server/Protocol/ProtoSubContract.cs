﻿using System;

namespace Protocol
{
    [AttributeUsage(AttributeTargets.Class, Inherited = false)]
    public sealed class ProtoSubContract : Attribute
    {
        public ProtoSubContract(int tag, int serverTag)
        {
            Tag = (tag << 4) + (serverTag & 0xf);
        }
        public int Tag { get; private set; }
    }
}
