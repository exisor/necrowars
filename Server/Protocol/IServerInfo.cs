﻿using System;
using System.Collections.Generic;

namespace Protocol
{
    public interface IServerInfo
    {
        string Name { get; }
        Guid Id { get; }
        ServerStatus Status { get; }
        int CurrentClients { get; }
        int PeerBuferSize { get; }
        List<ListenerInfo> Listeners { get; }
    }
}
