﻿using System;
using System.Collections.Generic;
using ProtoBuf;

namespace Protocol
{
    [ProtoContract]
    public class ServerInfo : IServerInfo
    {
        [ProtoMember(1)]
        public string Name { get; set; }
        [ProtoMember(2)]
        public Guid Id { get; set; }
        [ProtoMember(3)]
        public ServerStatus Status { get; set; }
        [ProtoMember(4)]
        public int CurrentClients { get; set; }
        [ProtoMember(5)]
        public int PeerBuferSize { get; set; }
        [ProtoMember(6)]
        public List<ListenerInfo> Listeners { get; set; }
    }
}