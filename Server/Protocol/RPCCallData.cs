﻿
using System;
using System.Threading.Tasks;
using ProtoBuf;

namespace Protocol
{
    [ProtoContract]
    [ProtoSubContract(1, Constants.COMMON_MESSAGE_SERVER_TAG)]
    public class RPCCallData : Message
    {
        [ProtoMember(1)]
        public Guid Id { get; set; }

        [ProtoMember(2)]
        public byte[] RpcData { get; set; }
    }
}
