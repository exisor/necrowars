using ProtoBuf;

namespace Protocol
{
    [ProtoContract]
    public enum ServerRole : byte
    {
        Master = 0,
        Login = 1,
        Lobby = 2,
    }
}