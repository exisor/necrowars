﻿using System;
using System.Collections.Concurrent;
using System.Linq;
using System.Reflection;
using ProtoBuf.Meta;

namespace Protocol
{
    public static class Serialization
    {
        private static readonly ConcurrentBag<string> AddedAssemblies = new ConcurrentBag<string>();
        private static readonly MetaType MessageMeta;

        static Serialization()
        {
            MessageMeta = RuntimeTypeModel.Default.Add(typeof (Message), true);
            AddMessagesFromAssembly(typeof(Message).Assembly);
        }
        public static void AddMessagesFromAssembly(Assembly assembly)
        {
            if (assembly == null) return;
            if (AddedAssemblies.Contains(assembly.FullName)) return;
            AddedAssemblies.Add(assembly.FullName);
            var assemblyTypes = assembly.GetTypes();
            
            var contracts = assemblyTypes
                .Where(t => t.IsDefined(typeof (ProtoSubContract), false) && t.BaseType == typeof(Message));
            foreach (var contract in contracts)
            {
                AddMessageContract(contract);
            }
        }

        public static void AddMessageContract(Type messageType)
        {
            var tag = messageType.GetCustomAttribute<ProtoSubContract>().Tag;
            var subtype =
                MessageMeta.GetSubtypes()
                    .FirstOrDefault(s => s.FieldNumber == tag && s.DerivedType.Type == messageType);
            if (subtype == null)
            {
                RuntimeTypeModel.Default.Add(messageType, true);
                MessageMeta.AddSubType(tag, messageType);
            }
        }
    }
}