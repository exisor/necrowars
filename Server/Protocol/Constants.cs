﻿using System.Reflection;

namespace Protocol
{
    public static class Constants
    {
        public const int COMMON_MESSAGE_SERVER_TAG = 0;

        public static Assembly PROTOCOL_ASSEMBLY => typeof (Constants).Assembly;
    }
}
