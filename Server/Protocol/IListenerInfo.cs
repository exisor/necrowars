﻿namespace Protocol
{
    public interface IListenerInfo
    {
        string Address { get; }
        int Port { get; }
        ListenerType Type { get; }
        int MaxClients { get; }
        string Name { get; }
        SocketType SocketType { get; }
    }
}
