﻿using ProtoBuf;

namespace Protocol
{
    [ProtoContract]
    public enum ListenerType : byte
    {
        Client,
        Server,
    }
}
