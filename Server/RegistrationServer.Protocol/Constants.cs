﻿using System.IO;
using System.Reflection;
using ProtoBuf;

namespace RegistrationServer.Protocol
{
    public static class Constants
    {
        public const int REGISTRATION_SERVER_TAG = 5;
        public static Assembly PROTOCOL_ASSEMBLY => Assembly.GetAssembly(typeof (Constants));

        public enum Messages
        {
            RegisterUserRequest = 1,
        }
    }
}
