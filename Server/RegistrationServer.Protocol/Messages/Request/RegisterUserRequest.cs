﻿using ProtoBuf;
using Protocol;

namespace RegistrationServer.Protocol.Messages.Request
{
    [ProtoContract]
    [ProtoSubContract((int)Constants.Messages.RegisterUserRequest, Constants.REGISTRATION_SERVER_TAG)]
    public class RegisterUserRequest : Message
    {
        [ProtoMember(1)]
        public string Username { get; set; }
        [ProtoMember(2)]
        public string Password { get; set; }
    }
}
