﻿using System.Threading.Tasks;
using BareBone.Base.Handlers;
using BareBone.Base.Peer;
using BareBone.Base.Pool;
using log4net;
using ManageServer.Logic.Modules;
using ManageServer.Protocol.Request;
using ManageServer.Protocol.Response;

namespace ManageServer.Handlers
{
    public class ManageAuthRequestHandler : ClientMessageHandler<ManageAuthRequest>
    {
        private readonly IAuthModule _authModule;
        private readonly IServerStatisticWatcher _statisticWatcher;
        private readonly MessagePool<AuthResultResponse> _authResultResponsePool = new MessagePool<AuthResultResponse>();
        public ManageAuthRequestHandler(ILog logger, IAuthModule authModule, IServerStatisticWatcher statisticWatcher) : base(logger)
        {
            _authModule = authModule;
            _statisticWatcher = statisticWatcher;
        }

        protected override async Task Handle(ManageAuthRequest message, ClientPeer peer)
        {
            using (var responsePooled = _authResultResponsePool.Get())
            {
                var response = responsePooled.Value;
                response.Success = _authModule.Authenticate(message.Password);
                _statisticWatcher.Subscribe(peer);
                await peer.Send(response);
            }
        }
    }

}
