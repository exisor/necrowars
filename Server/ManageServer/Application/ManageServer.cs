﻿using System.Collections.Generic;
using System.Reflection;
using System.Threading.Tasks;
using Autofac;
using BareBone.Base.ApplicationPlatform;
using BareBone.Base.Server;
using BareBone.Configuration;
using ManageServer.Logic.Modules;
using BareBone.Base.Timer;
using ManageServer.Protocol;

namespace ManageServer.Application
{
    public class ManageServer : ServerBase
    {
        private readonly IManageServerConfiguration _configuration;

        private IAuthModule _authModule;

        private ITimer _statisticUpdaterTimer;
        private IServerStatisticWatcher _serverStatisticWatcher;
        protected override void MapBindings(ContainerBuilder builder)
        {
            if (Platform == null)
            {
                Platform = ApplicationPlatform.RunningApplicationPlatform;
            }
            builder.RegisterType<AuthModule>().As<IAuthModule>().SingleInstance();
            
            builder.RegisterInstance(Platform).As<IApplicationPlatform>().SingleInstance();
            builder.RegisterType<ServerStatisticWatcher>().As<IServerStatisticWatcher>().SingleInstance();
        }

#pragma warning disable CS1998
        protected override async Task OnStart()
        {
            _authModule = Container.Resolve<IAuthModule>();
            _authModule.ChangePassword("");
            _serverStatisticWatcher = Container.Resolve<IServerStatisticWatcher>();
            if (_configuration != null)
            {
                _authModule.ChangePassword(_configuration.Password);
                _statisticUpdaterTimer = new Timer(_serverStatisticWatcher.Update, _configuration.TickRate);
                _statisticUpdaterTimer.Start();
            }
        }
#pragma warning restore CS1998

        protected override IEnumerable<Assembly> ProtocolAssemblies => new List<Assembly> { Constants.PROTOCOL_ASSEMBLY };

        public ManageServer(IServerConfiguration configuration) : base(configuration)
        {
            _configuration = configuration as IManageServerConfiguration;
        }

        public IApplicationPlatform Platform { get; set; }
    }
}
