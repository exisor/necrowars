﻿using System.Collections.Generic;
using System.Linq;
using BareBone.Base.ApplicationPlatform;
using BareBone.Base.Peer;
using ManageServer.Protocol.Event;
using Protocol;

namespace ManageServer.Logic.Modules
{
    class ServerStatisticWatcher : IServerStatisticWatcher
    {
        private readonly List<IPeer> _subscribers = new List<IPeer>();
        private readonly IApplicationPlatform _platform;
        private readonly ServerInfoUpdateEvent _message = new ServerInfoUpdateEvent();

        public ServerStatisticWatcher(IApplicationPlatform platform)
        {
            _platform = platform;
        }

        public void Update()
        {
            var snap = _platform.Servers.Cast<ServerInfo>().ToList();
            _message.ServerInfos = snap;
            foreach (var client in _subscribers)
            {
                client.Send(_message);
            }
        }

        public void Subscribe(IPeer peer)
        {
            peer.Disconnecting += () =>
            {
                _subscribers.Remove(peer);
            };
            _subscribers.Add(peer);
        }
        
    }
}