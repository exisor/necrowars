﻿namespace ManageServer.Logic.Modules
{
    internal class AuthModule : IAuthModule
    {
        private string _password;
        public void ChangePassword(string newPassword)
        {
            _password = newPassword;
        }

        public bool Authenticate(string password)
        {
            return password.Equals(_password);
        }
    }
}