﻿using BareBone.Base.Peer;

namespace ManageServer.Logic.Modules
{
    public interface IServerStatisticWatcher
    {
        void Update();
        void Subscribe(IPeer peer);
    }
}
