﻿namespace ManageServer.Logic.Modules
{
    public interface IAuthModule
    {
        void ChangePassword(string newPassword);
        bool Authenticate(string password);
    }
}
