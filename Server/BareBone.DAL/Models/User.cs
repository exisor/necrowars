﻿using FluentNHibernate.Mapping;

namespace Bitit.DAL.Models
{
    public class User
    {
        public virtual long Id { get; set; }
        public virtual string Username { get; set; }
        public virtual string Password { get; set; }
    }

    public class UserMap : ClassMap<User>
    {
        public UserMap()
        {
            Id(_ => _.Id);
            Map(_ => _.Username);
            Map(_ => _.Password);
            Table("users");
        }
    }
}
