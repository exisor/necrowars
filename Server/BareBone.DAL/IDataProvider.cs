﻿using System.Data;

namespace BareBone.DAL
{
    public interface IDataProvider
    {
        IDbConnection Connect();
    }
}
