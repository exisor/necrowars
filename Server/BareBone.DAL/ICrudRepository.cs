﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace BareBone.DAL
{
    public interface ICrudRepository<TData>
        where TData : class
    {
        IRepositoryItem<TData> Get();
        Task<IRepositoryItem<TData>> Get(long id);
        Task<IEnumerable<IRepositoryItem<TData>>> All { get; }
    }
}
