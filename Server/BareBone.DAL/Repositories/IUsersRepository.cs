﻿using Bitit.DAL.Models;

namespace Bitit.DAL.Repositories
{
    public interface IUsersRepository
    {
        void Add(User user);
        void Update(User user);
        void Remove(User user);
        User GetById(long id);
        User GetByName(string username);
    }

    public class UsersRepository : IUsersRepository
    {
        private readonly IDataProvider _data;

        public UsersRepository(IDataProvider data)
        {
            _data = data;
        }

        public void Add(User user)
        {
        }

        public void Update(User user)
        {
            throw new System.NotImplementedException();
        }

        public void Remove(User user)
        {
            throw new System.NotImplementedException();
        }

        public User GetById(long id)
        {
            throw new System.NotImplementedException();
        }

        public User GetByName(string username)
        {
            throw new System.NotImplementedException();
        }
    }
}
