﻿
using System;
using System.Threading.Tasks;

namespace BareBone.DAL
{
    public interface IRepositoryItem<TData> : IDisposable
    {
        TData Value { get; }
        Task Update();
        Task Insert();
        Task Delete();
    }
}
