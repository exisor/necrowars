using System.Collections.Generic;
using System.Threading.Tasks;

namespace BareBone.DAL
{
    public abstract class CrudRepository<TData> : ICrudRepository<TData> 
        where TData : class
    {
        private readonly IDataProvider _dataProvider;

        protected CrudRepository(IDataProvider dataProvider)
        {
            _dataProvider = dataProvider;

        }

        public IRepositoryItem<TData> Get()
        {
            throw new System.NotImplementedException();
        }

        public Task<IRepositoryItem<TData>> Get(long id)
        {
            throw new System.NotImplementedException();
        }

        public Task<IEnumerable<IRepositoryItem<TData>>> All { get; }
    }
}