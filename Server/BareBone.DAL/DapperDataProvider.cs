﻿using System.Data;
using Npgsql;

namespace BareBone.DAL
{
    public class DapperDataProvider : IDataProvider
    {
        private const string CONNECTION_STRING = @"Driver={PostgreSQL UNICODE};Server=127.0.0.1;Port=5432;Database=bitit;Uid=bitit;Pwd=bititpwd;";
        public IDbConnection Connect()
        {
            return new NpgsqlConnection(CONNECTION_STRING);
        }
    }
}