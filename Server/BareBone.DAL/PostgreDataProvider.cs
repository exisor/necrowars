﻿using System;
using System.IO;
using System.Reflection;
using FluentNHibernate.Cfg;
using FluentNHibernate.Cfg.Db;
using NHibernate;
using NHibernate.Tool.hbm2ddl;
using Environment = NHibernate.Cfg.Environment;

namespace Bitit.DAL
{
    public class PostgreDataProvider : IDataProvider
    {
        private readonly ISessionFactory _factory;
        public PostgreDataProvider()
        {
            _factory = Fluently.Configure()
                .ExposeConfiguration(c => c.SetProperty(Environment.WrapResultSets, "true"))
                .Database(PostgreSQLConfiguration.Standard
                    .ConnectionString(c =>
                        c.Host("localhost")
                            .Port(5432)
                            .Database("bitit")
                            .Username("bitit")
                            .Password("bititpwd")))
                .Mappings(m => m.FluentMappings.AddFromAssembly(Assembly.GetAssembly(typeof (PostgreDataProvider))))
                .ExposeConfiguration(c =>
                {
                    Action<string> updateExport = _ =>
                    {
                        using (var file = new FileStream(@"update.sql", FileMode.Append, FileAccess.Write))
                        using (var sw = new StreamWriter(file))
                        {
                            sw.Write(_);
                            sw.Close();
                        }
                    };
                    var update = new SchemaUpdate(c);
                    update.Execute(updateExport, true);
                })
                .BuildSessionFactory();
        }

        public ISession NewSession()
        {
            return _factory.OpenSession();
        }
    }
}