﻿using ProtoBuf;
using Protocol;

namespace ManageServer.Protocol.Response
{
    [ProtoContract]
    [ProtoSubContract(3, Constants.MANAGE_SERVER_TAG)]
    public class AuthResultResponse : Message
    {
        [ProtoMember(1)]
        public bool Success { get; set; }
    }
}