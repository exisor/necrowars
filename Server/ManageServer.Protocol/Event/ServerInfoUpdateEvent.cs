﻿using System.Collections.Generic;
using ProtoBuf;
using Protocol;

namespace ManageServer.Protocol.Event
{
    [ProtoContract]
    [ProtoSubContract(1, Constants.MANAGE_SERVER_TAG)]
    public class ServerInfoUpdateEvent : Message
    {
        [ProtoMember(1)]
        public List<ServerInfo> ServerInfos { get; set; }
    }
}