﻿
using ProtoBuf;
using Protocol;

namespace ManageServer.Protocol.Request
{
    [ProtoContract]
    [ProtoSubContract(2, Constants.MANAGE_SERVER_TAG)]
    public class ManageAuthRequest : Message
    {
        [ProtoMember(1)]
        public string Password { get; set; }
    }
}
