﻿

using System.Reflection;

namespace ManageServer.Protocol
{
    public static class Constants
    {
        public const int MANAGE_SERVER_TAG = 1;
        public static Assembly PROTOCOL_ASSEMBLY => Assembly.GetAssembly(typeof(Constants));
    }
}
