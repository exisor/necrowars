﻿using System.Linq;
using Assets.Scripts.Attributes;
using UnityEngine.Networking;

namespace Assets.Scripts
{
    public class ArmyLeader : Unit
    {
#pragma warning disable 414
#if SYNC_UNIT
        [SyncVar]
#endif
        private int _currentLeaderHp;

#if SYNC_UNIT
        [SyncVar] private int _maximumHp;
#endif
#pragma warning restore 414
        public MultiplierAttribute SkillDamage;

        #region Overrides of Unit

        protected override void Start()
        {
            MaxHP.ValueChanged += val => _maximumHp = (int)val;
            Army.UnitAdded += ArmyOnUnitAdded;
            base.Start();
        }

        private void ArmyOnUnitAdded(Unit _)
        {
            var applicableUnits = Army.Units.Where(u => u.Alive && !u.IsLeader).ToList();
            if (applicableUnits.Count > 1)
            {
                Quantity = (int)applicableUnits.Average(u => u.Quantity);
            }
        }

        #endregion

        public override bool IsLeader
        {
            get { return true; }
        }

        protected override void Die()
        {
            base.Die();
            Army.Destroyed = true;
        }

#region Overrides of Unit

        protected override float CurrentHp
        {
            get
            {
                return base.CurrentHp;
            }
            set
            {
                base.CurrentHp = value;
                _currentLeaderHp = (int)value;
            }
        }
        

        protected override void OnDead()
        {
            Army.UnitAdded -= ArmyOnUnitAdded;
            base.OnDead();
        }
        

        #endregion
    }
}