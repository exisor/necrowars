﻿using System;
using System.Collections.Generic;
using System.Linq;
using Assets.Scripts.Items;
using Common;
using Common.DTO;
using Common.Resources;
using UnityEngine;
using Ability = Common.DTO.Ability;
using Character = Common.DTO.Character;
using Item = Common.DTO.Item;
using Random = UnityEngine.Random;

namespace Assets.Scripts
{
    public class ArmiesManager : MonoBehaviour
    {
        public static ArmiesManager Instance { get; private set; }

        public List<FakeCharacter> FakeCharacters;

        public List<Army> Armies { get { return _armies; } }

#pragma warning disable 649
        [SerializeField] private GameField _gameField;
        [SerializeField] private List<Army> _armies;

        [SerializeField]
        private float _cleanup = 30.0f;
        [SerializeField] private ArmiesPool _armiesPool;
        [SerializeField] private int _minimum_upper_level_cap = 20;
        [SerializeField] private int _maximum_level_cap = 400;
        [SerializeField] private bool _bots_with_abilities = true;
#pragma warning restore 649
        private IItemAffixHandlerCollection _itemAffixHandlerCollection; 
        private readonly Common.ResourceManager.IAffixResourceManager _affixResourceManager = new AffixResourceManager();
        private readonly Common.ResourceManager.IAbilityResourceManager _abilityResourceManager = new AbilityResourceManager();
        private readonly Common.ResourceManager.IItemResourceManager _itemResourceManager = new ItemResourceManager();
        private readonly Common.ResourceManager.IItemSetsResourceManager _itemSetsResourceManager = new ItemSetsResourceManager();
        private int _topPlayerLevel;
        void Awake()
        {
            _topPlayerLevel = _minimum_upper_level_cap;
            _itemAffixHandlerCollection = new ItemAffixHandlerCollection();
            Instance = this;
        }
        void FixedUpdate()
        {
#if !FIXED_UPDATE
            for (var i = _armies.Count-1; i > 0; --i)
            {
                var a = _armies[i];
                a.ManagedUpdate();
                if (a.Destroyed && a.IsEmpty)
                {
                    _armies.Remove(a);
                    _armiesPool.DestroyArmy(a);
                    if (a.ArmyOwner == ArmyType.Player && a.Level == _topPlayerLevel)
                    {
                        var players = _armies.Where(aa => aa.ArmyOwner == ArmyType.Player && !aa.Destroyed).ToList();
                        _topPlayerLevel = Math.Max(_minimum_upper_level_cap, players.Count > 0 ? players.Max(aa => aa.Level) : 0);
                    }
                }
            }
#endif
        }
        private void Cleanup()
        {
            var destroyed = _armies.Where(a => a.Destroyed && a.IsEmpty).ToList();
            destroyed.ForEach(a =>
            {
                _armies.Remove(a);
                _armiesPool.DestroyArmy(a);
                if (a.ArmyOwner == ArmyType.Player && a.Level == _topPlayerLevel)
                {
                    var players = _armies.Where(aa => aa.ArmyOwner == ArmyType.Player && !aa.Destroyed).ToList();
                    _topPlayerLevel = Math.Max(_minimum_upper_level_cap, players.Count > 0 ? players.Max(aa => aa.Level) : 0);
                }
            });
        }

        public int GetRandomLevel()
        {
            return Random.Range(0, Math.Min(_topPlayerLevel, _maximum_level_cap));
        }
        public Army CreateAiArmy()
        {
            var level = GetRandomLevel();
            var abilities = new List<Ability>();
            // resurrection
            abilities.Add(new Ability {Id = 1, Level = 1});

            var actives = new List<Ability>();
            if (_bots_with_abilities)
            {
                // base passives
                abilities.AddRange(
                    Enumerable.Range(2, 6).Select(id => new Ability {Id = id, Level = Random.Range(0, level/8)}));
                // actives which are banned
                var banned = new long[] {38, 39, 40, 41, 42, 43, 44, 45, 28, 1, 30, 9, 11, 10, 46, 8};
                // pick three actives
                actives.AddRange(
                    _abilityResourceManager.All.Where(a => a.Type == AbilityType.Active && !banned.Contains(a.Id))
                        .OrderBy(
                            _ =>
                            {
                                if (_.Id == 31) return Random.Range(0, .5f);
                                return Random.Range(0f, 1f);
                            }).Take(3).Select(a => new Ability {Id = a.Id, Level = 1}));
                abilities.AddRange(actives);
                // add supports
                if (level > 50)
                {
                    actives.ForEach(a =>
                    {
                        abilities.AddRange(
                            _abilityResourceManager.GetAffectedAbilities(a.Id).Select(ability => new Ability
                            {
                                Id = ability.Id,
                                Level = Random.Range(0, level/8)
                            }));
                    });
                }
            }
            var i = 0;
            var character = new Character
            {
                Abilities = abilities,
                Level = level,
                AbilitySlots = actives.Select(a => new AbilitySlot
                {
                    AbilityId = a.Id,
                    Slot = i++,
                }).ToList()
            };
            var army = _armiesPool.CreateArmy(_gameField.GetRandomPos(), ArmyType.Ai, character);
            InitializeArmy(army, new NecroserverManager.CharacterData
            {
#if BOTS_WITH_ITEMS
                Items = new[] { 1, 2, 16, 17, 18, 19 }.Select(baseId =>
                      {
                          var retval = new Item
                          {
                              BaseId = baseId,
                              ItemLevel = character.Level,
                              Rarity = (Rarity)Math.Min(Random.Range((int)Rarity.Common, (int)Rarity.Legendary), Random.Range((int)Rarity.Common, (int)Rarity.Legendary))
                          };
                          var numberOfAffixes = 0;
                          switch (retval.Rarity)
                          {
                              case Rarity.Common:
                                  numberOfAffixes = 1;
                                  break;
                              case Rarity.Rare:
                                  numberOfAffixes = 2;
                                  break;
                              case Rarity.Epic:
                                  numberOfAffixes = 3;
                                  break;
                              case Rarity.Legendary:
                                  numberOfAffixes = 4;
                                  break;
                          }
                          var baseItem = _itemResourceManager.Get(retval.BaseId);
                          retval.Affixes = baseItem.AffixProbabilities.OrderByDescending(a => a.Weight * Random.Range(0f, 1f)).Select(prob =>
                              new ItemAffix
                              {
                                  Enchant = 0,
                                  Id = prob.AffixId,
                                  Seed = Random.Range(0, int.MaxValue)
                              }).Take(numberOfAffixes).ToList();
                          retval.Affixes.AddRange(baseItem.FixedAffixes.Select(id => new ItemAffix
                          {
                              Enchant = 0,
                              Id = id,
                              Seed = Random.Range(0, int.MaxValue)
                          }));
                          return retval;
                      }).ToList()
#else
                Items = new List<Item> ()
#endif
            });
            return army;
        }

        internal Army CreatePlayerArmy(NecroserverManager.CharacterData characterData)
        {
            var army = _armiesPool.CreateArmy(_gameField.GetRandomPos(), ArmyType.Player, characterData.Character);
            InitializeArmy(army, characterData);
            if (army.Level > _topPlayerLevel)
            {
                _topPlayerLevel = army.Level;
            }
            return army;
        }

        internal Army CreateFakePlayerArmy(FakeCharacter fakeCharacter)
        {
            var army = _armiesPool.CreateArmy(fakeCharacter.transform.position, ArmyType.Player, fakeCharacter.Character);
            InitializeArmy(army, new NecroserverManager.CharacterData {Character = fakeCharacter.Character, Items = new List<Item>(), PlayerName = ""});
            return army;
        }

        private void ApplySet(Army army, long id, int value)
        {
            var itemSet = _itemSetsResourceManager.Get(id);

            for (var i = 0; i < itemSet.Bonuses.Count; ++i)
            {
                var bonus = itemSet.Bonuses[i];
                if (bonus.ItemsCount <= value)
                {
                    var affix = _affixResourceManager.GetAffixValues(bonus.Affix);

                    _itemAffixHandlerCollection.ApplyAffix(army, affix);
                }
            }
        }

        private void InitializeArmy(Army army, NecroserverManager.CharacterData character)
        {
            var controller = army.GetComponent<ArmyPlayerController>();
            if (controller != null)
            {
                controller.CharacterData = character;
                controller.MmConnector = NecroserverManager.Instance.Client;
            }
            army.transform.SetParent(transform);
            army.SetHatId(0);
            army.VikingsLimit = 150;

            var sets = new Dictionary<long, int>();
            if (character.Items != null)
            {
                foreach (var item in character.Items)
                {
                    var itemBase = _itemResourceManager.Get(item.BaseId);
                    if (itemBase.Types.Contains(ItemType.VisualHelm))
                    {
                        army.SetHatId(item.BaseId);
                    }
                    var affixes = _affixResourceManager.GetAffixValues(item);
                    foreach (var affix in affixes)
                    {
                        _itemAffixHandlerCollection.ApplyAffix(army, affix);
                    }
                    if (itemBase.SetId != 0)
                    {
                        var oldval = 0;
                        sets.TryGetValue(itemBase.SetId, out oldval);
                        ++oldval;
                        sets[itemBase.SetId] = oldval;
                    }
                }
                foreach (var set in sets)
                {
                    ApplySet(army, set.Key, set.Value);
                }
            }
            army.Init();

            _armies.Add(army);
        }
    }
}
