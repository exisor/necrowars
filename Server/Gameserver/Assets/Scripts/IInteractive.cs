﻿namespace Assets.Scripts
{
    public interface IInteractive
    {
        void Interact(Army army);
    }
}
