﻿using UnityEngine;
using UnityEngine.Networking;

namespace Assets.Scripts.Zones
{
    public class SacrificeZone : NetworkBehaviour, IInteractive
    {
        [SyncVar][SerializeField]
        private float _radius;

        public delegate void EventUnitSacrificedDelegate(Vector2 v);

        [SyncEvent]
        public event EventUnitSacrificedDelegate EventUnitSacrificed;

        //public delegate void EventArmySacrificedDelegate(long exp, long gold, long points);

        //[SyncEvent]
        //public event EventArmySacrificedDelegate EventArmySacrificed;

        [SerializeField] private float _lifeTime;

        [SerializeField] private int _unitsRequired;
        [SerializeField]
        private int _unitsAliveLeft;
        [SerializeField]
        private float ExpMultiplier;
        [SerializeField]
        private float GoldMultiplier;
        [SerializeField]
        private float PointsMultiplier;

        private float _timer = 0;
        void Awake()
        {
            NetworkServer.Spawn(gameObject);
        }

        public void Interact(Army army)
        {
            var vDistance = army.Leader.pos - (Vector2)transform.position;
            if (vDistance.sqrMagnitude > _radius * _radius) return;
            if (army.AliveUnits < _unitsRequired) return;
            var controller = army.GetComponent<ArmyPlayerController>();

            var goldTotal = 0L;
            var expTotal = 0L;
            var pointsTotal = 0L;
            for (var i = army.Units.Count - 1; i >= 0 && army.AliveUnits >= _unitsAliveLeft; --i)
            {
                var unit = army.Units[i];
                if (unit.Alive && !unit.IsLeader)
                {
                    // sacrifice

                    Damage.Unblockable(army.Leader, unit, unit.HP);
                    var exp = (long)(unit.ExpReward.Value*ExpMultiplier);
                    var gold = (long)(unit.GoldReward.Value*GoldMultiplier);
                    var points = (int) (unit.GoldReward.Value*0.5f*PointsMultiplier);
                    controller.GrantExp(exp);
                    controller.GrantGold(gold);
                    army.Points += points;
                    goldTotal += gold;
                    expTotal += exp;
                    pointsTotal += points;
                    unit.OnReadyToCollect();
                    EventUnitSacrificed(unit.pos);
                }
            }
            TargetArmySacrificed(army.connectionToClient, expTotal, goldTotal, pointsTotal);
        }
        [TargetRpc]
        private void TargetArmySacrificed(NetworkConnection conn, long exp, long gold, long points)
        {
            
        }

        void FixedUpdate()
        {
            _timer += Time.deltaTime;
            if (_timer > _lifeTime)
            {
                NetworkServer.UnSpawn(gameObject);
                Destroy(gameObject);
            }
        }
    }
}
