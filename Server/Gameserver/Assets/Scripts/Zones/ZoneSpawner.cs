﻿using System;
using UnityEngine;
using Random = UnityEngine.Random;

namespace Assets.Scripts.Zones
{
    public class ZoneSpawner : MonoBehaviour
    {
        [SerializeField] private float _respawnCooldown;
        [SerializeField] private SacrificeZone SacrificeZonePrefab;
        [SerializeField] private GameField _field;

        [SerializeField] private Transform[] _spawnPositions;

        private float _timer;

        void Awake()
        {
            Spawn();
        }
        public void FixedUpdate()
        {
            _timer += Time.deltaTime;
            if (_timer >= _respawnCooldown)
            {
                _timer = 0;
                Spawn();
            }
        }

        private void Spawn()
        {
            Instantiate(SacrificeZonePrefab, _spawnPositions[Random.Range(0, _spawnPositions.Length)].position,
                Quaternion.identity);
        }
    }
}
