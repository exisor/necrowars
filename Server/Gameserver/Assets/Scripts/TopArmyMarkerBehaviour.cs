﻿using UnityEngine;

namespace Assets.Scripts
{
    public class TopArmyMarkerBehaviour : MonoBehaviour
    {
        public Army HostArmy;

        void FixedUpdate()
        {
            if (HostArmy == null) return;
            if (HostArmy.Leader == null) return;
            transform.position = HostArmy.Leader.pos;
        }
    }
}
