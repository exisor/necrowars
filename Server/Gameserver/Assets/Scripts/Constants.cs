﻿using UnityEngine;

namespace Assets.Scripts
{
    public static class Constants
    {
        public const float VECTOR_EPSILON = 0.1f;
        public static int UNITS_LAYER;

        public static int UNITS_LAYER_MASK;

        public static LayerMask NETWORK_LAYER_MASK;
    }
}
