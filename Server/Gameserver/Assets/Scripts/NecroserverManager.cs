﻿#if UNITY_5_6_OR_NEWER
#define EXPERIMENTAL
#endif
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using Common;
using Common.DTO;
using ServerProtocol;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.SceneManagement;
using UnityEngine.UI;


namespace Assets.Scripts
{
    public class NecroserverManager : NetworkManager
    {
        public static NecroserverManager Instance;

        private const string PATH_TO_CONFIG = "config.xml";

        private readonly Dictionary<string, CharacterData> _pendingCharacters = new Dictionary<string, CharacterData>();

        private readonly Dictionary<NetworkConnection, CharacterData> _connections = new Dictionary<NetworkConnection, CharacterData>();

        public struct CharacterData
        {
            public Character Character;
            public string PlayerName;
            public List<Item> Items;
        }
        public struct MMIPEndPoint
        {
            public string Address;
            public int Port;
        }

        public Connector Client { get; set; }
        public ServerConfiguration Config { get; set; }
        public bool Ready { get; set; }

        public MMIPEndPoint EndPoint { get; set; }
        [SerializeField] private Text _statusText;
        [SerializeField] private bool _isBalance;
        [SerializeField] private GameObject DummyPlayer;

#if EXPERIMENTAL
        [Flags]
        private enum lws_context_options : uint
        {
            LWS_SERVER_OPTION_REQUIRE_VALID_OPENSSL_CLIENT_CERT = (1 << 1) |
                     (1 << 12),
            LWS_SERVER_OPTION_SKIP_SERVER_CANONICAL_NAME = (1 << 2),
            LWS_SERVER_OPTION_ALLOW_NON_SSL_ON_SSL_PORT = (1 << 3) |
                     (1 << 12),
            LWS_SERVER_OPTION_LIBEV = (1 << 4),
            LWS_SERVER_OPTION_DISABLE_IPV6 = (1 << 5),
            LWS_SERVER_OPTION_DISABLE_OS_CA_CERTS = (1 << 6),
            LWS_SERVER_OPTION_PEER_CERT_NOT_REQUIRED = (1 << 7),
            LWS_SERVER_OPTION_VALIDATE_UTF8 = (1 << 8),
            LWS_SERVER_OPTION_SSL_ECDH = (1 << 9) |
                     (1 << 12),
            LWS_SERVER_OPTION_LIBUV = (1 << 10),
            LWS_SERVER_OPTION_REDIRECT_HTTP_TO_HTTPS = (1 << 11) |
                     (1 << 3) |
                     (1 << 12),
            LWS_SERVER_OPTION_DO_SSL_GLOBAL_INIT = (1 << 12),
            LWS_SERVER_OPTION_EXPLICIT_VHOSTS = (1 << 13),
            LWS_SERVER_OPTION_UNIX_SOCK = (1 << 14),
            LWS_SERVER_OPTION_STS = (1 << 15),

            /***** add new things just above ---^ *****/
        };

#endif
        private void Awake()
        {
            connectionConfig.MaxCombinedReliableMessageCount = 512;

            Instance = this;
            Constants.UNITS_LAYER = LayerMask.NameToLayer("units");
            Constants.UNITS_LAYER_MASK = LayerMask.GetMask("units");
            Constants.NETWORK_LAYER_MASK = LayerMask.GetMask("units", "effects");
            DontDestroyOnLoad(gameObject);
            Client = new Connector(this);
            // Load config
            try
            {
                var file = File.ReadAllText(PATH_TO_CONFIG);
                Config = XmlSerializer.Deserialize<ServerConfiguration>(file);
            }
            catch (Exception)
            {
                Config = new ServerConfiguration();
                File.WriteAllText(PATH_TO_CONFIG, XmlSerializer.Serialize(Config));
                throw;
            }
            connectionConfig.WebSocketReceiveBufferMaxSize = 64000;
#if EXTENDED_LOGGING
            Logging.Ability = Config.Log.Ability;
            Logging.Unit = Config.Log.Unit;
            Logging.Item = Config.Log.Item;
#endif

            networkPort = Config.LocalPort;
#if !UNITY_EDITOR
            useWebSockets = Config.UseWebsocket;
#endif
#if EXPERIMENTAL
            if (useWebSockets && Config.UseSSL)
            {
                Debug.Log("Using ssl");
                Debug.LogFormat("Cert path : {0}, key : {1} ca : {2}", Config.SSLCertPath, Config.SSLCertKeyPath, Config.SSLCAPath);
                if (!string.IsNullOrEmpty(Config.SSLCertPath)) connectionConfig.SSLCertFilePath = Config.SSLCertPath;
                if (!string.IsNullOrEmpty(Config.SSLCAPath)) connectionConfig.SSLCAFilePath = Config.SSLCAPath;
                if (!string.IsNullOrEmpty(Config.SSLCertKeyPath)) connectionConfig.SSLPrivateKeyFilePath = Config.SSLCertKeyPath;
                //connectionConfig.SSLServerOption = (uint)(
                //    lws_context_options.LWS_SERVER_OPTION_DISABLE_IPV6 |
                //    lws_context_options.LWS_SERVER_OPTION_PEER_CERT_NOT_REQUIRED |
                //    lws_context_options.LWS_SERVER_OPTION_DO_SSL_GLOBAL_INIT |
                //    lws_context_options.LWS_SERVER_OPTION_SSL_ECDH);
            }
#endif
            if (Config.ConnectToMatchmaker)
            {
                Client.Connect(Config.MatchmakerAddress, Config.MatchmakerPort);

                Debug.Log("Connected to master");
                Client.Send(ProtobufSerializer.MakeMessage(new ClientProtocol.RequestServer
                {
                    GameName = Debug.isDebugBuild ?
                    "Necrowars-test" :
                    "Necrowars",
                    ServerRole = ServerRole.Matchmaker
                }));
                StartCoroutine(WaitingForReady());
            }
            else
            {
                StartServer();
                if (_isBalance) SceneManager.LoadScene("Balance");
                else SceneManager.LoadScene(string.Format("Level{0}", Config.Level));
            }
            _statusText.text = "STARTED";
        }

        private IEnumerator WaitingForReady()
        {
            while (!Ready)
            {
                yield return new WaitForSeconds(1.0f);
            }

            Client.Disconnect();
            Client.Connect(EndPoint.Address, EndPoint.Port);
            var specs = Config.Specs.ToList();
            var message = new RegisterServerOnMM
            {
                Address = Config.GameserverAddress,
                Port = Config.GameserverPort,
                Level = Config.Level,
                Specs = specs,
            };
            Client.Send(ProtobufSerializer.MakeMessage( message));
            StartServer();
            if (_isBalance) SceneManager.LoadScene("Balance");
            else SceneManager.LoadScene(string.Format("Level{0}", Config.Level));
        }

        public override void OnClientConnect(NetworkConnection conn)
        {
            base.OnClientConnect(conn);
            Debug.Log("Client connect");
        }

        public override void OnClientError(NetworkConnection conn, int errorCode)
        {
            base.OnClientError(conn, errorCode);
            Debug.LogFormat("Client error : {0}", errorCode);
        }

        public override void OnClientDisconnect(NetworkConnection conn)
        {
            base.OnClientDisconnect(conn);
            Debug.LogFormat("Client disconnected");
        }

        public override void OnServerDisconnect(NetworkConnection conn)
        {
            base.OnServerDisconnect(conn);
            Debug.LogFormat("Disconnected");
        }

        public override void OnDropConnection(bool success, string extendedInfo)
        {
            base.OnDropConnection(success, extendedInfo);
            Debug.LogErrorFormat("OnDropConnection {0} {1}", success, extendedInfo);
        }


        public override void OnServerError(NetworkConnection conn, int errorCode)
        {
            base.OnServerError(conn, errorCode);
            Debug.LogErrorFormat("Error {0}", errorCode);
        }

        public override void OnServerRemovePlayer(NetworkConnection conn, PlayerController player)
        {
            base.OnServerRemovePlayer(conn, player);
            Debug.LogFormat("Remove player {0}", player.playerControllerId);
        }

        public override void OnServerConnect(NetworkConnection conn)
        {
            base.OnServerConnect(conn);
            if (!_isBalance)
                conn.RegisterHandler((short)ClientProtocol.CustomGameMessageType.AuthorizePlayer, AuthorizePlayerHandler);

            Debug.Log("Client connect");
        }

        private void AuthorizePlayerHandler(NetworkMessage netMsg)
        {
            var message = netMsg.ReadMessage<AuthorizePlayer>();
            Debug.Log("AuthorizePlayerHandler");
            if (AuthorizeCharacter(netMsg.conn, message.Message.Token, message.Message.CharacterId))
            {
                var msg = new PlayerAuthorized();
                netMsg.conn.Send((short) msg.MessageType, msg);
            }
            else
            {
                Debug.Log("Authorization failed");
            }
        }
        public override void OnServerAddPlayer(NetworkConnection conn, short playerControllerId)
        {
            if(_isBalance)
            {
                foreach (var fake in ArmiesManager.Instance.FakeCharacters)
                {
                    var army = ArmiesManager.Instance.CreateFakePlayerArmy(fake);
                    AddPlayerForConnection(army, conn, playerControllerId);
                }
            }
            else
            {
                CharacterData characterData;
                if (!_connections.TryGetValue(conn, out characterData))
                {
                    conn.Disconnect();
                    return;
                }
                var army = ArmiesManager.Instance.CreatePlayerArmy(characterData);
                army.PlayerName = characterData.PlayerName;
                AddPlayerForConnection(army, conn, playerControllerId);
            }
        }

        private void AddPlayerForConnection(Army army, NetworkConnection conn, short playerControllerId)
        {
            NetworkServer.AddPlayerForConnection(conn, army.gameObject, playerControllerId);
            conn.RegisterHandler(MsgType.Disconnect, OnPlayerDisconnects);
#if ABILITY_ENABLED
            foreach (var activeAbility in army.ActiveAbilities)
            {
                NetworkServer.SpawnWithClientAuthority(activeAbility.gameObject, conn);
            }
#endif
            Debug.Log("Player connect");
        }

        private void OnPlayerDisconnects(NetworkMessage netMsg)
        {
            try
            {
                if (netMsg.conn.playerControllers.Count == 0)
                {
                    Debug.Log("Somehow playerControllers is emty for disconnecting players");
                    return;
                }
                var army = netMsg.conn.playerControllers[0].gameObject.GetComponent<Army>();
                if (!army.Destroyed)
                {
                    Damage.Unblockable(army.Leader, army.Leader, army.Leader.HP);
                    if (army.Leader.Alive) throw new Exception("ArmyLeader is alive");
                }
                NetworkServer.ReplacePlayerForConnection(netMsg.conn, Instantiate(DummyPlayer), army.playerControllerId);
            }
            catch (Exception e)
            {
                Debug.LogException(e);
            }
            finally
            {
                NetworkServer.DestroyPlayersForConnection(netMsg.conn);
            }
        }

        public void ConnectCharacter(string token, CharacterData character)
        {
            _pendingCharacters[token] = character;
        }

        public bool AuthorizeCharacter(NetworkConnection connection, string token, long characterId)
        {
            CharacterData pendingCharacter;
            if (!_pendingCharacters.TryGetValue(token, out pendingCharacter))
            {
                Debug.LogFormat("Character is not authorized : {0}", characterId);
                return false;
            }
            if (pendingCharacter.Character.Id != characterId)
            {
                Debug.LogFormat("Character is not authorized : {0}", characterId);
                return false;
            }
            _pendingCharacters.Remove(token);

            _connections[connection] = pendingCharacter;

            return true;
        }

        public class Connector
        {
            private const int READ_BUFFER_SIZE = 65536;


            private TcpClient client;
            private readonly byte[] _readBuffer = new byte[READ_BUFFER_SIZE];
            private readonly NecroserverManager _necroserverManager;

            public Connector(NecroserverManager manager)
            {
                _necroserverManager = manager;
            }

            public void Connect(string address, int port)
            {
                try
                {
                    client = new TcpClient(address, port) {ReceiveBufferSize = READ_BUFFER_SIZE, ReceiveTimeout = 1000, };
                    client.GetStream().BeginRead(_readBuffer, 0, READ_BUFFER_SIZE, DoRead, null);
                }
                catch (Exception ex)
                {
                    Debug.LogException(ex);
                }
            }

            private void DoRead(IAsyncResult ar)
            {
                try
                {
                    if (!client.Connected) return;
                    var bytesRead = client.GetStream().EndRead(ar);
                    if (bytesRead < 1)
                    {
                        Debug.Log("Read 0 bytes. Server close");
                        return;
                    }
                    var s = Encoding.ASCII.GetString(_readBuffer, 0, bytesRead);
                    Debug.Log(s);
                    var messageStrings = s.Split(new[] {ProtobufSerializer.PROTOCOL_DELIMETER},
                        StringSplitOptions.RemoveEmptyEntries);
                    foreach (var messageString in messageStrings)
                    {
                        try
                        {
                            var message = new MessageInfo(messageString);
                            ProcessCommands(message);
                        }
                        catch (Exception e)
                        {
                            Debug.LogException(e);
                        }
                    }
                }
                catch (Exception e)
                {
                    Debug.LogException(e);
                }
                finally
                {
                    client.GetStream().BeginRead(_readBuffer, 0, READ_BUFFER_SIZE, DoRead, null);
                }
            }

            // Process the command received from the server, and take appropriate action.
            private void ProcessCommands(MessageInfo message)
            {
                switch (message.Key)
                {
                    case "RequestServerResponse":
                        HandleRequestServerResponse(
                            ProtobufSerializer.Deserialize<ClientProtocol.RequestServerResponse>(
                                message.Data));
                        break;
                    case "Error":
                        throw new Exception(
                            ProtobufSerializer.Deserialize<ClientProtocol.Error>(message.Data).Message.ToString());
                    case "UserEnteringBattleEvent":
                        HandleUserEnteringBattleEvent(
                            ProtobufSerializer.Deserialize<ServerProtocol.UserEnteringBattleEvent>(message.Data));
                        break;
                    default:
                        throw new Exception(string.Format("Unknown message : {0} {1}", message.Key, message.Data));
                }
            }

            private void HandleUserEnteringBattleEvent(UserEnteringBattleEvent message)
            {
                _necroserverManager.ConnectCharacter(message.Token, new CharacterData {Character = message.Character, PlayerName = message.PlayerName, Items = message.Items});
            }

            public void Send(byte[] data)
            {
                client.GetStream().Write(data, 0, data.Length);
            }

            public void Send<T>(T message)
            {
                Send(ProtobufSerializer.MakeMessage(message));
            }

            public void Disconnect()
            {
                client.Close();
            }

            private void HandleRequestServerResponse(ClientProtocol.RequestServerResponse response)
            {
                Debug.LogFormat("{0} {1} {2}", response.Role, response.Address, response.Port);
                _necroserverManager.EndPoint = new MMIPEndPoint
                {
                    Address = response.Address,
                    Port = response.Port
                };
                _necroserverManager.Ready = true;
            }
        }
    }
}
