﻿using UnityEngine;

namespace Assets.Scripts.Pool
{
    public class GameobjectPool<T> : Pool<T> where T : MonoBehaviour, IPooledObject
    {
        public GameobjectPool(int initialSize, GameObject go) : base(initialSize, () =>
        {
            var retval = Object.Instantiate(go);
            return retval.GetComponent<T>();
        })
        {
        }
    }
}