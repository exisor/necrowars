﻿namespace Assets.Scripts.Pool
{
    public interface IPool<T>
        where T : IPooledObject
    {
        T Get();
        void Return(T o);
    }
}
