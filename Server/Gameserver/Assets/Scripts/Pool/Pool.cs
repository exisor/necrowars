﻿using System;
using System.Collections.Generic;

namespace Assets.Scripts.Pool
{
    public class Pool<T> : IPool<T> where T : IPooledObject
    {
        private readonly Stack<T> _stack;
        private readonly Func<T> _allocator;
        public Pool(int initialSize, Func<T> allocator)
        {
            _allocator = allocator;
            _stack = new Stack<T>(initialSize);
            for (var i = 0; i < initialSize; ++i)
            {
                _stack.Push(allocator());
            }
        }
        public T Get()
        {
            var retval = _stack.Count > 0 ? _stack.Pop() : _allocator();
            retval.Initialize();
            return retval;
        }

        public void Return(T o)
        {
            o.Cleanup();
            _stack.Push(o);
        }
    }
}