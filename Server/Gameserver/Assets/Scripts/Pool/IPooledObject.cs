﻿namespace Assets.Scripts.Pool
{
    public interface IPooledObject
    {
        void Initialize();
        void Cleanup();
    }
}