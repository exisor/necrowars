﻿namespace Assets.Scripts.Effects
{
    public class ValkyrieDamageRedirrect : TargetEffect
    {
        protected override bool HasConstantDuration
        {
            get { return true; }
        }

        protected override void Enable()
        {
            Target.ReceivingDamage += TargetOnReceivingDamage;
        }

        private void TargetOnReceivingDamage(ref Damage damage)
        {
            if (damage.Type == DamageType.Unblockable) return;
            if (Target.Army.AliveUnits <= 1) return;

            Unit newTarget = null;
            for (var i = 0; i < Target.Army.Units.Count; i++)
            {
                newTarget = Target.Army.Units[i];
                if (newTarget.Alive && !newTarget.IsLeader) break;
            }
            Damage.Redirrect(newTarget, damage);
            damage.Amount = 0;
        }

        protected override void Disable()
        {
            Target.ReceivingDamage -= TargetOnReceivingDamage;
        }
    }
}