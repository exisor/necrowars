namespace Assets.Scripts.Effects
{
    public class StunEffect : TargetEffect
    {
        protected override void Enable()
        {
            Target.MoveActive = false;
            Target.AttackActive = false;
        }

        protected override void Disable()
        {
            Target.MoveActive = true;
            Target.AttackActive = true;
        }
    }
}