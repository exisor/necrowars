﻿using System;
using Assets.Scripts.Attributes;

namespace Assets.Scripts.Effects
{
    public class ValhallaShieldEffect : TargetEffect
    {
        public FloatAttribute HP;
        private float _currentHP;
        protected override void Enable()
        {
            _currentHP = HP;
            Target.ReceivingDamage += TargetOnReceivingDamage;
        }

        private void TargetOnReceivingDamage(ref Damage damage)
        {
            if (damage.Type == DamageType.Unblockable) return;
            damage.Amount -= _currentHP;
            if (damage.Amount < 0)
            {
                _currentHP = -damage.Amount;
                damage.Amount = 0;
                return;
            }
            _currentHP = 0;
        }

        protected override void Disable()
        {
            Target.ReceivingDamage -= TargetOnReceivingDamage;
        }
    }
}
