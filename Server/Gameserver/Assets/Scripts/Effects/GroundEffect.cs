﻿using UnityEngine;

namespace Assets.Scripts.Effects
{
    public abstract class GroundEffect : Effect
    {
        public Army Owner { get; set; }
    }
}
