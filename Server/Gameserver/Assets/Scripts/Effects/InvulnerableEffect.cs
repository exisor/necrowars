﻿namespace Assets.Scripts.Effects
{
    public class InvulnerableEffect : TargetEffect
    {
        protected override bool HasConstantDuration
        {
            get { return true; }
        }

        protected override void Enable()
        {
            Target.CanBeTarget = false;
            Target.AttackActive = false;
        }

        protected override void Disable()
        {
            Target.CanBeTarget = true;
            Target.AttackActive = true;
        }
    }
}
