namespace Assets.Scripts.Effects
{
    public enum EffectType
    {
        Positive,
        Negative,
    }
}