﻿using Assets.Scripts.Attributes;
using UnityEngine;

namespace Assets.Scripts.Effects
{
    public class ChargeEffect : TargetEffect
    {
        protected override bool HasConstantDuration
        {
            get { return true; }
        }

        public Vector2 MoveTarget;
        public float Speed;
        protected override void OnUpdate()
        {
            Target.transform.position = Vector2.MoveTowards(Target.transform.position, MoveTarget, Speed*Time.deltaTime);
        }
    }
}
