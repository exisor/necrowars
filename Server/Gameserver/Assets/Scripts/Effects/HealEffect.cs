﻿using Assets.Scripts.Attributes;

namespace Assets.Scripts.Effects
{
    public class HealEffect : TargetEffect
    {
        public FixedFloatBonus RegenAmount;

        protected override void Enable()
        {
            Target.HPRegen += RegenAmount;
        }

        protected override void Disable()
        {
            Target.HPRegen -= RegenAmount;
        }
    }
}