﻿using Assets.Scripts.Attributes;

namespace Assets.Scripts.Effects
{
    public class SlowEffect : TargetEffect
    {
        [Common.ClientValueTag(IsPercent = true)]
        public RelFloatBonus SpeedPenalty;
        protected override void Enable()
        {
            Target.MoveSpeed += SpeedPenalty;
        }

        protected override void Disable()
        {
            Target.MoveSpeed -= SpeedPenalty;
        }
    }
}