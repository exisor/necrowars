using UnityEngine.Networking;

namespace Assets.Scripts.Effects
{
    public abstract class TargetEffect : Effect
    {
#pragma warning disable 414
#if SYNC_EFFECT
        [SyncVar]
#endif
        private NetworkInstanceId _targetId;
#pragma warning restore 414
        protected virtual bool StayOnDeath { get { return false; } }
        protected virtual bool HasConstantDuration { get { return false; } }
        private Unit _target;

        public Unit Target
        {
            get { return _target; }
            set
            {
                if (value.Effects.ContainsKey(GetType()))
                {
                    CleanEffect();
                    return;
                }

                if (Type == EffectType.Negative && value.Effects.ContainsKey(typeof (ValhallaEffect)))
                {
                    CleanEffect();
                    return;
                }

                _target = value;
                _target.ReadyToCollect += TargetOnReadyToCollect;
                if (!StayOnDeath)
                {
                    _target.Dead += TargetOnDead;
                }

                if (Type == EffectType.Negative)
                {
                    if (!HasConstantDuration)
                    Duration *= _target.ReduceNegativeEffectDuration;
                }
                else
                {
                    if (!HasConstantDuration)
                        Duration *= _target.IncreasePositiveEffectDuration;
                }

                DurationTimer = Duration;

                _targetId = _target.GetComponent<NetworkIdentity>().netId;
                transform.SetParent(_target.transform);

                _target.Effects[GetType()] = this;
            }
        }

        private void TargetOnDead(Unit unit)
        {
            CleanEffect();
        }

        protected override void OnCleanEffect()
        {
            if (Target != null)
            {
                Target.ReadyToCollect -= TargetOnReadyToCollect;
                Target.Dead -= TargetOnDead;
                Target.Effects.Remove(GetType());
            }
        }

        private void TargetOnReadyToCollect(Unit unit)
        {
            CleanEffect();
        }
    }
}