﻿using Assets.Scripts.Attributes;

namespace Assets.Scripts.Effects
{
    public class AmplifyDamageEffect : TargetEffect
    {
        [Common.ClientValueTag(IsPercent = true)]
        public FloatAttribute AmplifyDamage;

        private RelFloatBonus _bonus;

        protected override void Enable()
        {
            _bonus = new RelFloatBonus(AmplifyDamage);
            Target.AmplifyIncomingDamage += _bonus;
        }

        protected override void Disable()
        {
            Target.AmplifyIncomingDamage -= _bonus;
        }
    }
}
