﻿using System;
using System.Linq;
using Assets.Scripts.Attributes;
using UnityEngine;
using UnityEngine.Networking;

namespace Assets.Scripts.Effects
{
    public class VikingTrapEffect : TargetEffect
    {
        private Army _owner;

        [Common.ClientValueTag]
        public float DetonateTime;
        private float _detonateTimer;

        public float Speed;

        [Common.ClientValueTag(IsPercent = true)]
        public FloatAttribute Multiplier;

        [Common.ClientValueTag]
        public float Radius;
        public float Force;

        private float _damage;

        protected override bool StayOnDeath
        {
            get { return true; }
        }

        protected override void Enable()
        {
            _owner = Target.Army;
            _damage = Damage.Unblockable(Target, Target, Target.HP);
            Target.CanBeTarget = false;
            Target.Army = null;
            Target.Resurrected += TargetOnResurrected;
        }

        private void TargetOnResurrected(Unit unit)
        {
            Target.Resurrected -= TargetOnResurrected;
            Target.MoveActive = false;
            Target.AttackActive = false;
            _detonateTimer = DetonateTime;
        }

        protected override void OnUpdate()
        {
            if (Target.Army == null) return;
            if (_owner == null || _owner.Leader == null || _owner.Destroyed)
            {
                CleanEffect();
                return;
            }

            Target.transform.localPosition = Vector2.MoveTowards(Target.transform.localPosition, Target.Army.Leader, Speed * Time.deltaTime);

            _detonateTimer -= Time.deltaTime;
            if (_detonateTimer <= 0)
            {
                var targets =
                    Physics2D.OverlapCircleAll(Target, Radius, LayerMask.GetMask("units"))
                        .Select(t => t.GetComponent<Unit>())
                        .Where(u => u.Alive && u.Army == Target.Army)
                        .ToList();
                Damage.Spell(_owner.Leader, targets, _damage * Multiplier);
                EventDetonate();
                CleanEffect();
            }
        }

        protected override void Disable()
        {
            Target.Resurrected -= TargetOnResurrected;
            Damage.Unblockable(Target, Target, Target.HP);
            Target.OnReadyToCollect();
        }

        [SyncEvent]
        public event Action EventDetonate;
    }
}
