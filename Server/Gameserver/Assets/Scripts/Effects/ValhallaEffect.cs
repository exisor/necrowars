﻿using System.Linq;

namespace Assets.Scripts.Effects
{
    public class ValhallaEffect : TargetEffect
    {
        protected override void Enable()
        {
            foreach (var effect in Target.Effects.Values.ToList().Where(e => e.Type == EffectType.Negative))
            {
                effect.CleanEffect();
            }
        }
    }
}
