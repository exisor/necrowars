﻿using Assets.Scripts.Attributes;

namespace Assets.Scripts.Effects
{
    public class MoveSpeedEffect : TargetEffect
    {
        public FloatAttribute Amount;

        private RelFloatBonus _bonus; 
        protected override void Enable()
        {
            _bonus = new RelFloatBonus(Amount);
            Target.MoveSpeed += _bonus;
        }

        protected override void Disable()
        {
            Target.MoveSpeed -= _bonus;
        }
    }
}