﻿
using System.Linq;
using Assets.Scripts.Attributes;
using UnityEngine;
using UnityEngine.Networking;

namespace Assets.Scripts.Effects
{
    public class RagnarekAOE : GroundEffect
    {
#pragma warning disable 414
#if SYNC_EFFECT
        [SyncVar]
#endif
        private float _radius;
#pragma warning restore 414

        public FloatAttribute Radius;
        public FloatAttribute SkillDamage;
        [Common.ClientValueTag(LevelAddicted = false, IsPercent = true)]
        public float DamageMultiplier;
        protected override void Enable()
        {
            _radius = Radius;
            Radius.ValueChanged += val => _radius = val;
        }

        protected override void Disable()
        {
            SkillDamage += Owner.Leader.RollDamage();
            SkillDamage += RelFloatBonus.FromMultiplier(DamageMultiplier);

            var targets =
                Physics2D.OverlapCircleAll(transform.position, Radius, LayerMask.GetMask("units"))
                    .Select(t => t.GetComponent<Unit>())
                    .Where(u => u.Alive);
            foreach (var target in targets)
            {
                Damage.Spell(Owner.Leader, target, SkillDamage);
            }
        }
    }
}
