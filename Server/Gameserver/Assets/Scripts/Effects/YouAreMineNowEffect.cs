﻿using UnityEngine;
using UnityEngine.Networking;

namespace Assets.Scripts.Effects
{
    public class YouAreMineNowEffect : TargetEffect
    {
        public float Chance;
        protected override void Enable()
        {
            Target.Kill += TargetOnKill;
        }

        private void TargetOnKill(Unit source, Unit target)
        {
            if (target.IsLeader) return;
            if (Target.Army.AliveUnits >= Target.Army.VikingsLimit) return;
            if (Random.Range(0f, 1f) >= Chance) return;
            target.Army = Target.Army;
            target.Resurrect();
            target.Heal();
#if SYNC_EFFECT
            EventResurrected(target.netId);
#endif
        }

        protected override void Disable()
        {
            Target.Kill -= TargetOnKill;
        }
#if SYNC_EFFECT
        public delegate void EventResurrectedDelegate(NetworkInstanceId target);
        [SyncEvent]
        public event EventResurrectedDelegate EventResurrected;
#endif
    }
}
