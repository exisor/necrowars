using Assets.Scripts.Attributes;

namespace Assets.Scripts.Effects
{
    public class AttackEffect : TargetEffect
    {
        [Common.ClientValueTag(IsPercent = true)]
        public FloatAttribute Amount;

        private RelFloatBonus _bonus; 
        protected override void Enable()
        {
            _bonus = new RelFloatBonus(Amount);
            Target.MinDamage += _bonus;
            Target.MaxDamage += _bonus;
        }

        protected override void Disable()
        {
            Target.MinDamage -= _bonus;
            Target.MaxDamage -= _bonus;
        }
    }
}