﻿using System;
using Assets.Scripts.Attributes;
using UnityEngine;
using UnityEngine.Networking;

namespace Assets.Scripts.Effects
{
    public abstract class Effect :
#if SYNC_EFFECT
        NetworkBehaviour
#else
        MonoBehaviour
#endif
    {
        public static T Create<T>(T prototype) where T : Effect
        {
            return ((GameObject)Instantiate(prototype.gameObject, new Vector3(-100, -100, 0), Quaternion.identity)).GetComponent<T>();
        }
        [Common.ClientValueTag(LevelAddicted = false)]
        public FloatAttribute Duration;

        public EffectType Type;
        protected float DurationTimer;

        protected bool _enabled;
        public bool _infinite = false;

        public void CleanEffect()
        {
            OnCleanEffect();
            if (_enabled)
            {
                Disable();
                if (Disabled != null) Disabled(this);
            }
#if SYNC_EFFECT
            NetworkServer.UnSpawn(gameObject);
#endif
            Destroy(gameObject);
        }

        void Start()
        {
#if SYNC_EFFECT
            NetworkServer.Spawn(gameObject);
#endif
            DurationTimer = Duration;
            Enable();
            if (Enabled != null) Enabled(this);
            _enabled = true;
        }
        void FixedUpdate()
        {
            if (!_infinite)
            {
                DurationTimer -= Time.deltaTime;
                if (DurationTimer <= 0.0f)
                {
                    CleanEffect();
                    return;
                }
            }
            OnUpdate();
        }

        protected virtual void Enable() { }
        protected virtual void Disable() { }
        protected virtual void OnUpdate() { }

        protected virtual void OnCleanEffect() { }

        public event Action<Effect> Enabled;
        public event Action<Effect> Disabled;
    }
}
