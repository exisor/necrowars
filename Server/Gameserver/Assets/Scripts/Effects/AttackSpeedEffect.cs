﻿using Assets.Scripts.Attributes;

namespace Assets.Scripts.Effects
{
    public class AttackSpeedEffect : TargetEffect
    {
        public FloatAttribute Amount;

        private RelFloatBonus _bonus; 
        protected override void Enable()
        {
            _bonus = new RelFloatBonus(Amount);
            Target.AttackSpeed += _bonus;
        }

        protected override void Disable()
        {
            Target.AttackSpeed -= _bonus;
        }
    }
}