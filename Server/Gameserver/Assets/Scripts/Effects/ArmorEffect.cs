﻿using Assets.Scripts.Attributes;

namespace Assets.Scripts.Effects
{
    public class ArmorEffect : TargetEffect
    {
        [Common.ClientValueTag]
        public FloatAttribute Amount;
        [Common.ClientValueTag(IsPercent = true)]
        public FloatAttribute RelativeAmount;
        private FixedFloatBonus _bonus;
        private RelFloatBonus _relBonus;
        protected override void Enable()
        {
            _bonus = new FixedFloatBonus(Amount);
            _relBonus = new RelFloatBonus(RelativeAmount);
            Target.Armor += _bonus;
            Target.Armor += _relBonus;
        }

        protected override void Disable()
        {
            Target.Armor -= _bonus;
            Target.Armor -= _relBonus;
        }
    }
}