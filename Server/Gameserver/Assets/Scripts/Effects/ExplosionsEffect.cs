﻿using System;
using System.Collections.Generic;
using System.Linq;
using Assets.Scripts.Attributes;
using UnityEngine;

namespace Assets.Scripts.Effects
{
    public class ExplosionsEffect : TargetEffect
    {
        [Common.ClientValueTag]
        public FloatAttribute Radius;
        [Common.ClientValueTag(IsPercent = true)]
        public MultiplierAttribute RangeMultiplier;

        private RelFloatBonus _rangeBonus;

        private float _storedRadius;
        private float _aggroRange;
        protected override void Enable()
        {
            _leader = (ArmyLeader) Target;
            _rangeBonus = RelFloatBonus.FromMultiplier(RangeMultiplier);
            _leader.AggroRange += _rangeBonus;
            _leader.DamageDealt += OnAttack;

        }

        private ArmyLeader _leader;
        private void OnAttack(Unit source, Unit target, float damage)
        {
            var targets =
                Physics2D.OverlapCircleAll(target.transform.position, Radius, LayerMask.GetMask("units"))
                    .Select(t => t.GetComponent<Unit>()).Where(t => t.Army != _leader.Army && t.Alive).ToList();
            foreach (var unit in targets)
            {
                Damage.Spell(_leader, unit, damage);
            }
            OnSplash(targets);
        }

        protected override void Disable()
        {
            Target.DamageDealt -= OnAttack;
            Target.AggroRange -= _rangeBonus;
        }

        public event Action<List<Unit>> Splash;

        protected virtual void OnSplash(List<Unit> obj)
        {
            var handler = Splash;
            if (handler != null) handler(obj);
        }
    }
}