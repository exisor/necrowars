using System;
using System.Collections.Generic;
using System.Linq;
using Assets.Scripts.Attributes;
using UnityEngine;

namespace Assets.Scripts.Effects
{
    [RequireComponent(typeof(BoxCollider2D))]
    public class StoneWallEffect : GroundEffect
    {
        [Common.ClientValueTag(IsPercent = true)]
        public FloatAttribute Multiplier;

        [Common.ClientValueTag]
        public float Width;
        [Common.ClientValueTag]
        public float Height;

        public float DistanceFromCollider;
        private float _tickTimer = 0;

        private BoxCollider2D _collider;
        private Vector2 downleft;
        private Vector2 upright;
        protected override void Enable()
        {
            _collider = GetComponent<BoxCollider2D>();
            _collider.size = new Vector2(Width, Height);
            Vector3 worldposition = transform.TransformPoint(0, 0, 0);
            downleft = worldposition +
                       Quaternion.Euler(transform.eulerAngles)*
                       new Vector2(-_collider.size.x/2 - DistanceFromCollider, -_collider.size.y/2 - DistanceFromCollider);
            upright = worldposition +
                      Quaternion.Euler(transform.eulerAngles)*
                      new Vector2(_collider.size.x/2 + DistanceFromCollider, _collider.size.y/2 + DistanceFromCollider);
        }

        protected override void OnUpdate()
        {
            _tickTimer -= Time.deltaTime;
            if (_tickTimer <= 0)
            {
                _tickTimer = 1;
                if (Owner == null) return;
                if (Owner.Leader == null) return;

                var targets = Physics2D.OverlapAreaAll(downleft, upright, LayerMask.GetMask("units"))
                    .Select(t => t.GetComponent<Unit>()).Where(u => u.Army != Owner && u.Alive).ToList();
                Damage.Spell(Owner.Leader, targets, Owner.Leader.RollDamage() * Multiplier);
                if (Tick != null)
                {
                    Tick(this, targets);
                }
            }
        }

        public event Action<Effect, List<Unit>> Tick;
    }
}