﻿namespace Assets.Scripts.Effects
{
    public class RootEffect : TargetEffect
    {
        protected override void Enable()
        {
            Target.MoveActive = false;
        }

        protected override void Disable()
        {
            Target.MoveActive = true;
        }
    }
}