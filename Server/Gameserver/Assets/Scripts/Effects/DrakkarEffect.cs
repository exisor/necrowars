﻿using System;
using System.Collections.Generic;
using System.Linq;
using Assets.Scripts.Attributes;
using UnityEngine;

namespace Assets.Scripts.Effects
{
    public class DrakkarEffect : GroundEffect
    {
        [Common.ClientValueTag(IsPercent = true)]
        public MultiplierAttribute DamageMod;
        public Vector2 MoveTarget;
        [Common.ClientValueTag]
        public float Speed;

        [Common.ClientValueTag]
        public FloatAttribute Radius;
        
        public FloatAttribute SkillDamage;

        private static readonly Collider2D[] TemporaryTargets = new Collider2D[50];
        protected override void Enable()
        {
            SkillDamage *= DamageMod;
            transform.position = Owner.Leader;
        }

        #region Overrides of Effect

        protected override void OnUpdate()
        {
            if (!Owner.Leader.Alive)
            {
                CleanEffect();
            }
            if (Vector2.Distance(transform.position, MoveTarget) > Constants.VECTOR_EPSILON)
            {
                transform.position = Vector2.MoveTowards(transform.position, MoveTarget, Speed * Time.deltaTime);
            }
        }

        #endregion

        protected override void Disable()
        {
            if (Owner.Leader.Alive)
            {
                var hits = Physics2D.OverlapCircleNonAlloc(transform.position, Radius, TemporaryTargets,
                        Constants.UNITS_LAYER_MASK);
                for (var i = 0; i < hits; ++i)
                {
                    var u = TemporaryTargets[i].GetComponent<Unit>();
                    if (!u.Alive || u.Army == Owner) continue;

                    Damage.Spell(Owner.Leader, u, SkillDamage);
                }
            }
        }
    }
}
