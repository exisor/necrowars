﻿
using System;

namespace Assets.Scripts
{
    [Serializable]
    public enum DamageType : byte
    {
        Normal,
        Unblockable,
        Spell,
    }
}
