﻿using System;
using System.Collections.Generic;
using System.Linq;
using Assets.Scripts.Attributes;
using Assets.Scripts.Effects;
using UnityEngine;
using UnityEngine.Networking;
using Random = UnityEngine.Random;

namespace Assets.Scripts
{
    [RequireComponent(typeof (Rigidbody2D))]
    [RequireComponent(typeof (Collider2D))]
    public class Unit : NetworkBehaviour
    {
        #region const

        private const float ATTACK_SPEED_MULTIPLIER = 1.0f;
        private const float MIN_SQUARE_DISTANCE_TO_LEADER = 2.0f;
        private const float LEADER_ATTRACTION_SPEED_DELIMETER = 4f;
        

        #endregion

        #region RemoteEventsDelegates
#if SYNC_UNIT
        public delegate void EventAttackDelegate(Vector2 target);
#endif

#endregion

#region editor interface

#pragma warning disable 649
        [SerializeField] private long _id;
        [SerializeField] private float _corspePersist;
        [SerializeField] private float _resurrectionTime = 0.5f;

        protected virtual float CurrentHp
        {
            get { return _currentHp; }
            set { _currentHp = value; }
        }

        // New Attributes

        [Header("HP")]
        public FloatAttribute MaxHP;
        public StraightLevelRelBonus QuantityHpBonus;
        public StraightLevelRelBonus LevelHpBonus;
        public FloatAttribute HPRegen;
        public MultiplierAttribute SpellDamageDefence;
        [Header("Speeds")] public AttackSpeedAttribute AttackSpeed;
        public FloatAttribute MoveSpeed;
        [Header("Damage")] public MinDamageAttribute MinDamage;
        public MaxDamageAttribute MaxDamage;
        public StraightLevelRelBonus QuantityDamageBonus;
        public StraightLevelRelBonus LevelDamageBonus;

        public ArmorAttribute Armor;

        public FloatAttribute ConsumesHunger;
        public StraightLevelRelBonus LevelHungerBonus;
        public StraightLevelRelBonus QuantityHungerBonus;

        public FloatAttribute HungerLeech;
        public FloatAttribute HPLeech;

        public MultiplierAttribute AmplifyIncomingDamage;
        [Header("Gold")] public FloatAttribute GoldReward;
        public StraightLevelRelBonus LevelGoldBonus;
        public StraightLevelRelBonus QuantityGoldBonus;

        [Header("Exp")] public FloatAttribute ExpReward;

        public ExpLevelRelBonus LevelExpBonus;
        public StraightLevelRelBonus QuantityExpBonus;  
        [Header("Misc")]
        public MultiplierAttribute ReduceNegativeEffectDuration;
        public MultiplierAttribute IncreasePositiveEffectDuration;
        [Space(10.0f)] 
        public FloatAttribute AggroRange;

        public AggroRangeQuantityBonus AggroRangeQuantityBonus;
        public int AggroRate = 5;


        private int _aggroTries;
#pragma warning restore 649

#endregion

#region private members

        private Rigidbody2D _rigidbody;
        private CircleCollider2D _collider;
        private Army _army;
        private float _baseColliderRadius;

        private Unit _currentTarget;
        private Vector2 _moveTarget;
        private float _attackTimer;
        private float _corpseTimer;
        private float _cantMoveTimer;

        private bool _readyToCollect = false;

        #endregion

#region Network state
#if SYNC_UNIT
        [SyncVar]
#endif
        private bool _alive = true;
#pragma warning disable 414
#if SYNC_UNIT
        [SyncVar]
#endif
        private NetworkInstanceId _armyId;
#pragma warning restore 414
        [SerializeField]
#if SYNC_UNIT
        [SyncVar] 
#endif
        private int _quantity = 1;
#if SYNC_UNIT
        [SyncEvent]
        public event Action EventDie;

        [SyncEvent]
        public event EventAttackDelegate EventAttack;

        [SyncEvent]
        public event Action EventDamageReceived;
        [SyncEvent]
        public event Action EventResurrect;
#endif
#endregion

#region Unity events 

        void Awake()
        {
            _rigidbody = GetComponent<Rigidbody2D>();
            _collider = GetComponent<CircleCollider2D>();
        }
        protected virtual void Start()
        {
            _moveTarget = transform.localPosition;
            _baseColliderRadius = _collider.radius;

            MaxHP += QuantityHpBonus;
            MaxHP += LevelHpBonus;
            MinDamage += QuantityDamageBonus;
            MaxDamage += QuantityDamageBonus;
            MinDamage += LevelDamageBonus;
            MaxDamage += LevelDamageBonus;

            ConsumesHunger += LevelHungerBonus;
            ConsumesHunger += QuantityHungerBonus;

            GoldReward += LevelGoldBonus;
            GoldReward += QuantityGoldBonus;

            ExpReward += LevelExpBonus;
            ExpReward += QuantityExpBonus;

            AggroRange.ValueChanged += val => _square_range = val*val;
            AggroRange += AggroRangeQuantityBonus;

            AggroRangeQuantityBonus.Quantity = Quantity;

            MaxHP.ValueChanged += hp => CurrentHp = (int)Math.Min(CurrentHp, hp);
            Heal();
            MoveActive = true;
            AttackActive = true;
            CanBeTarget = true;
#if EXTENDED_LOGGING
            Logging.Log(this, "Unit created. Id : {0}, MaxHP : {1}, Damage : {2}-{3}. HPRegen : {4}", Id, MaxHP, MinDamage, MaxDamage, HPRegen);
            MaxHP.ValueChanged += val => Logging.Log(this, "MaxHP changed : {0}", val);
            HPRegen.ValueChanged += val => Logging.Log(this, "HPRegen changed : {0}", val);
#endif
        }

#if FIXED_UPDATE
        void FixedUpdate()
#else
        public void ManagedUpdate()
#endif
        {
            pos = transform.localPosition;
            _rigidbody.velocity = Vector2.zero;
            UpdateCooldowns();
            if (!Alive)
            {
                _corpseTimer += Time.deltaTime;
                if (_corpseTimer > _corspePersist)
                {
                    OnReadyToCollect();
                }
                return;
            }
            if (HPRegen > 0 && CurrentHp < MaxHP)
            {
                Heal(HPRegen*Time.deltaTime);
            }
            if (CanAttack)
            {
                MakeAttack();
            }
            var moved = CanMove;
            if (moved)
            {
                MakeMove();
            }
            else
            {
                FixInPosition();
            }
            _moved = moved;
            if (Math.Abs(pos.x) > GameField.FieldWidth / 2  || Math.Abs(pos.y) > GameField.FieldHeight / 2)
            {
                Damage.Unblockable(this, this, CurrentHp);
            }
        }

        private void ConsumeProvision()
        {
            Army.Provision -= ConsumesHunger*Time.deltaTime;
        }

        void OnDestroy()
        {
            Army = null;
        }

#endregion
        

#region logic

        private void FixInPosition()
        {
            _rigidbody.constraints = RigidbodyConstraints2D.FreezeAll;
        }

        private bool _moved;

        private static readonly Collider2D[] TemporaryTargets = new Collider2D[50];
        // this method is called a lot of times per frame so all vector math intentionally manual
        private void MakeMove()
        {
            if (!_moved)
            {
                _rigidbody.constraints = RigidbodyConstraints2D.FreezeRotation;
            }
            float speed = MoveSpeed;
            // velocity to movetarget
            var x = _moveTarget.x - pos.x;
            var y = _moveTarget.y - pos.y;
            var square_speed = speed*speed;
            var m = Mathf.Sqrt(square_speed / (x * x + y * y));
            var velocity = new Vector2(x * m, y * m);

            if (!IsLeader)
            {
                // distance to leader
                var leaderPos = Army.Leader.pos;
                var x_leader_distance = leaderPos.x - pos.x;
                var y_leader_distance = leaderPos.y - pos.y;
                var m_leader_distance = (x_leader_distance*x_leader_distance) + (y_leader_distance*y_leader_distance);

                if (m_leader_distance > MIN_SQUARE_DISTANCE_TO_LEADER)
                {
                    var new_magnitude = Mathf.Sqrt(square_speed / m_leader_distance)/LEADER_ATTRACTION_SPEED_DELIMETER;
                    velocity = new Vector2(velocity.x + x_leader_distance * new_magnitude, velocity.y + y_leader_distance*new_magnitude);
                }
            }
            _rigidbody.velocity = velocity;
        }

        private bool CanMove
        {
            get
            {
                if (Army == null) return false;
                if (Army.Destroyed) return false;
                if (IsAttacking) return false;
                if (!MoveActive) return false;
                if (_cantMoveTimer > 0) return false;
                var x = pos.x - _moveTarget.x;
                var y = pos.y - _moveTarget.y;
                var m = x*x + y*y;
                if (m < Constants.VECTOR_EPSILON) return false;
                return true;
            }
        }

        private void MakeAttack()
        {
            _attackTimer += ATTACK_SPEED_MULTIPLIER/AttackSpeed;
            var damage = Damage.Attack(this, _currentTarget);
            Vector2 t = _currentTarget.pos;
            if (HungerLeech > 0)
            {
                Army.Provision += damage*HungerLeech;
            }
            if (HPLeech > 0)
            {
                Heal(damage*HPLeech);
            }
            OnDamageDealt(_currentTarget, damage);
#if SYNC_UNIT
            EventAttack(t);
#endif
        }

        protected virtual void Die()
        {
            Alive = false;
            _currentTarget = null;
            _moveTarget = transform.localPosition;
            CurrentHp = 0;
#if SYNC_UNIT
            EventDie();
#endif
            OnDead();
        }

        private void UpdateCooldowns()
        {
            if (_attackTimer > 0) UpdateAttackCooldown();
            if (_cantMoveTimer > 0) UpdateCantMoveCooldown();
            if (_aggroTries > 0) -- _aggroTries;
        }

        private void UpdateCantMoveCooldown()
        {
            _cantMoveTimer -= Time.deltaTime;
        }

        private void UpdateAttackCooldown()
        {
            _attackTimer -= Time.deltaTime;
        }

        private bool CanAttack
        {
            get
            {
                if (!AttackActive) return false;
                if (!HasValidTarget() && !SetCurrentTarget())
                {
                    return false;
                }
                return _attackTimer <= 0.0f;
            }
        }

        private float _square_range;
        private bool HasValidTarget()
        {
            if (_currentTarget == null) return false;
            if (_currentTarget.Army == Army ||
                !_currentTarget.CanBeTarget ||
                !_currentTarget.Alive ||
                _currentTarget.Collected)
            {
                _currentTarget = null;
                return false;
            }
            var x = pos.x - _currentTarget.pos.x;
            var y = pos.y - _currentTarget.pos.y;
            if (x*x + y*y > _square_range)
            {
                _currentTarget = null;
                return false;
            }
            return true;
        }

        private bool SetCurrentTarget()
        {
            if (_aggroTries > 0) return false;
            _aggroTries = AggroRate;
            _currentTarget = null;
            var targetsCount = Physics2D.OverlapCircleNonAlloc(pos, AggroRange, TemporaryTargets, Constants.UNITS_LAYER_MASK);
            for (var i = 0; i < targetsCount; ++i)
            {
                var u = TemporaryTargets[i].GetComponent<Unit>();
                if (u.Army != Army && u.Alive && u.CanBeTarget && !u.Collected && _currentTarget == null)
                {
                    _currentTarget = u;
                }
                TemporaryTargets[i] = null;
            }
            return _currentTarget != null;
        }

    private bool IsAttacking { get { return _attackTimer > 0.0f; } }
#endregion
#region public interface
        [NonSerialized]
        public Vector2 pos;
        public bool Collected { get { return _readyToCollect; } }
        public int Quantity
        {
            get { return _quantity; }
            set
            {
                _quantity = value;
                if (_collider != null)
                {
                    _collider.radius = (float) Math.Sqrt(_quantity)*_baseColliderRadius;
                }
                AggroRangeQuantityBonus.Quantity = _quantity;

                QuantityHpBonus.Level = value - 1;
                QuantityDamageBonus.Level = value - 1;
                QuantityHungerBonus.Level = value - 1;
            }
        }
        public Army Army
        {
            get { return _army; }
            set
            {
                if (value == _army) return;
                if (_army != null)
                {
                    _army.RemoveUnit(this);
                    transform.SetParent(_army.transform.parent, true);
                }
                _army = value;
                if (value == null)
                {
                    _armyId = NetworkInstanceId.Invalid;
                    return;
                }
                _army.AddUnit(this);
                if (_readyToCollect) return;
                _armyId = _army.GetComponent<NetworkIdentity>().netId;
                transform.SetParent(_army.transform);
            }
        }
        public bool Alive
        {
            get { return _alive; }
            private set
            {
                if (_alive == value) return;
                _alive = value;
                _corpseTimer = 0f;
                _collider.isTrigger = !_alive;
                if (Army != null) Army.AliveChanged(value);
            }
        }
        public void SetMoveTarget(Vector2 target)
        {
            _moveTarget = target + ((Vector2) Army.Leader.transform.localPosition - (Vector2) transform.localPosition);
        }
        public float ReceiveDamage(Damage damage)
        {
            if (!CanBeTarget && damage.Type != DamageType.Unblockable) return 0;
            EventDamageReceived();
            OnReceivingDamage(ref damage);
            CurrentHp -= damage.Amount;
            if (CurrentHp <= 0)
            {
                if (IsLeader)
                {
                    foreach (var unit in Army.Units.Where(u => u.Alive && !u.IsLeader).ToList())
                    {
                        damage.DamageSource.OnKill(unit);
                    }
                }
                Die();
                
                damage.DamageSource.OnKill(this);
            }
            return damage.Amount;
        }

        public void Heal(float hp)
        {
            CurrentHp += hp;
            if (CurrentHp > MaxHP) CurrentHp = MaxHP;
            if (CurrentHp > 0f)
            {
                Alive = true;
            }
#if EXTENDED_LOGGING
            Logging.Log(this, "Healing unit by : {0}. Current hp : {1}", hp, CurrentHp);
#endif
        }

        public float RollDamage()
        {
            return Random.Range(MinDamage, MaxDamage);
        }

        public void Heal()
        {
            CurrentHp = MaxHP;
            Alive = true;
        }

        public virtual bool IsLeader
        {
            get { return false; }
        }

        public long Id
        {
            get { return _id; }
        }

        public void Resurrect()
        {
            _cantMoveTimer = _resurrectionTime;
#if SYNC_UNIT
            EventResurrect();
#endif
            OnResurrected();
        }

        public virtual void OnReadyToCollect()
        {
            _readyToCollect = true;
            ReadyToCollect(this);
        }

        public float HP
        {
            get { return CurrentHp; }
        }

        public event Action<Unit> ReadyToCollect;

        public delegate void ReceivingDamageDelegate(ref Damage damage);
        public event ReceivingDamageDelegate ReceivingDamage;
        public event Action<Unit, Unit, float> DamageDealt;
        public event Action<Unit, Unit> Kill;

        public bool Invulnerable { get; set; }

#endregion

        protected virtual void OnDamageDealt(Unit target, float damage)
        {
            var handler = DamageDealt;
            if (handler != null) handler(this, target, damage);
        }

        public virtual void OnKill(Unit u)
        {
            if (u.Army == Army) return;
            if (Army != null)
            {
                Army.OnKill(u);
            }
            var handler = Kill;
            if (handler != null) handler(this, u);
        }

        public bool AttackActive { get; set; }
        public bool MoveActive { get; set; }

        private readonly Dictionary<Type, TargetEffect> _effects = new Dictionary<Type, TargetEffect>();
        [SerializeField]
        private float _currentHp;
        public Dictionary<Type, TargetEffect> Effects { get { return _effects; } }

        public static implicit operator Vector3(Unit unit)
        {
            return unit.transform.localPosition;
        }
        public static implicit operator Vector2(Unit unit)
        {
            return unit.transform.localPosition;
        }

        protected virtual void OnReceivingDamage(ref Damage obj)
        {
            var handler = ReceivingDamage;
            if (handler != null) handler(ref obj);
        }

        public bool CanBeTarget { get; set; }
        public event Action<Unit> Dead;

        protected virtual void OnDead()
        {
            var handler = Dead;
            if (handler != null) handler(this);
        }

        public event Action<Unit> Resurrected;

        protected virtual void OnResurrected()
        {
            var handler = Resurrected;
            if (handler != null) handler(this);
        }

        public static Vector2 operator -(Unit lvalue, Vector2 rvalue)
        {
            return (Vector2)lvalue - rvalue;
        }

        public static bool operator ==(Unit l, Unit r)
        {
            return ReferenceEquals(l, r);
        }
        public static bool operator !=(Unit l, Unit r)
        {
            return !ReferenceEquals(l, r);
        }

        public override bool Equals(object o)
        {
            return ReferenceEquals(this, o);
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
    }
}