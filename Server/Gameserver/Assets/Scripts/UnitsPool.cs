﻿using System;
using System.Collections.Generic;
using System.Linq;
using Assets.Scripts.DTO;
using UnityEngine;
using UnityEngine.Networking;

namespace Assets.Scripts
{
    public class UnitsPool : MonoBehaviour
    {
        public static UnitsPool Instance = null; 

        private Dictionary<long, GameObject> _units;

        public void Awake ()
        {
            Instance = this;
            _units = UnityEngine.Resources.LoadAll("Prefabs/Units").Select(r => ((GameObject)r).GetComponent<Unit>())
                .Where(u => u != null)
                .ToDictionary(u => u.Id, u => u.gameObject);
        }

        public Unit CreateUnit(Vector2 position, UnitData unitData)
        {
            var prefab = _units[unitData.Id];
            var go = (GameObject)Instantiate(prefab, position, Quaternion.identity);
            NetworkServer.Spawn(go);
            var retval = go.GetComponent<Unit>();
            retval.ReadyToCollect += UnitOnReadyToCollect;
            return retval;
        }

        private void UnitOnReadyToCollect(Unit unit)
        {
            DestroyUnit(unit);
        }

        public void DestroyUnit(Unit unit)
        {
            unit.Army = null;
            NetworkServer.UnSpawn(unit.gameObject);
            Destroy(unit.gameObject);
        }
    }
}

