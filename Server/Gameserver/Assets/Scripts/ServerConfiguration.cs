﻿using System;
using System.Collections.Generic;
using Common;

namespace Assets.Scripts
{
    [Serializable]
    public class ServerConfiguration
    {
        [Serializable]
        public class LoggingConfig
        {
            public bool Ability;
            public bool Unit;
            public bool Item;
        }

        public string MatchmakerAddress = string.Empty;
        public int MatchmakerPort = 0;

        public bool UseWebsocket = false;
        public string GameserverAddress = string.Empty;
        public int GameserverPort = 0;

        public int Level = 1;

        public bool ConnectToMatchmaker;
        public List<BattleSpecEnum> Specs = new List<BattleSpecEnum>();
        public int LocalPort = 7777;

        public LoggingConfig Log = new LoggingConfig();

        public float SpawnRate = 3f;

        public bool UseSSL = false;
        public string SSLCertPath = "";
        public string SSLCAPath = "";
        public string SSLCertKeyPath = "";
    }
}
