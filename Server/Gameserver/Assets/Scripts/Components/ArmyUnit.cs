﻿namespace Assets.Scripts.Components
{
    public class ArmyUnit : Component
    {
        public Army Army { get; set; }

        #region Overrides of Component

        public override void Cleanup()
        {
            base.Cleanup();
            Army = null;
        }

        #endregion
    }
}
