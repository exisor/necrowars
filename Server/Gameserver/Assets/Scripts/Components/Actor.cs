﻿#if NEW_NETWORK
using System;
using UnityEngine;
using UnityEngine.Networking;

namespace Assets.Scripts.Components
{
    [RequireComponent(typeof(Movement))]
    [RequireComponent(typeof(Attack))]
    [RequireComponent(typeof(AttackTarget))]
    public class UnitActor : NetworkBehaviour, IPooledObject
    {
        [SerializeField] private long _id;
        [SerializeField] private float _corpsePersist;

        private Movement _movement;
        private Attack _attack;
        private AttackTarget _attackTarget;

        private float _corpseTimer;

        void Awake()
        {
            _movement = GetComponent<Movement>();
            _attack = GetComponent<Attack>();
            _attackTarget = GetComponent<AttackTarget>();
        }

        void FixedUpdate()
        {
            if (!_attackTarget.Alive)
            {
                _corpseTimer += Time.deltaTime;
                if (_corpseTimer > _corpsePersist)
                {
                    OnReadyToCollect();
                }
                return;
            }
            // ConsumeProvision
            if (_attack.Enabled)
            {
                MakeAttack();
            }
            if (_movement.Enabled)
            {
                _movement.MakeMove();
            }
        }

        private void MakeAttack()
        {

        }

#region Implementation of IPooledObject

        public void Initialize()
        {
            _movement.Initialize();
            _attack.Initialize();
            _attackTarget.Initialize();
        }

        public void Cleanup()
        {
            _movement.Cleanup();
            _attack.Cleanup();
            _attackTarget.Cleanup();
        }

        public event Action<IPooledObject> ReadyToCollect;
        protected virtual void OnReadyToCollect()
        {
            var handler = ReadyToCollect;
            if (handler != null) handler(this);
        }

#endregion

        public static implicit operator Movement(UnitActor actor)
        {
            return actor._movement;
        }

        public static implicit operator Attack(UnitActor actor)
        {
            return actor._attack;
        }

        public static implicit operator AttackTarget(UnitActor actor)
        {
            return actor._attackTarget;
        }

    }
}
#endif