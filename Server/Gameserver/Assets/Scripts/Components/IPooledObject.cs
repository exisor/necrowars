﻿using System;

namespace Assets.Scripts.Components
{
    public interface IPooledObject
    {
        void Initialize();
        void Cleanup();
        event Action<IPooledObject> ReadyToCollect;
    }
}
