﻿#if NEW_NETWORK
using System;
using Assets.Scripts.Attributes;
using UnityEngine;

namespace Assets.Scripts.Components
{
    [RequireComponent(typeof(Attack))]
    [RequireComponent(typeof(AttackTarget))]
    [RequireComponent(typeof(ArmyUnit))]
    public class Leech : MonoBehaviour
    {
        public FloatAttribute HPLeech;
        public FloatAttribute HungerLeech;

        private Attack _attack;
        private AttackTarget _attackTarget;
        private ArmyUnit _armyUnit;

        void Awake()
        {
            _attack = GetComponent<Attack>();
            _attackTarget = GetComponent<AttackTarget>();
            _armyUnit = GetComponent<ArmyUnit>();

            _attack.Attacking += AttackOnAttacking;
        }

        private void AttackOnAttacking(Unit unit, float damage)
        {
            _attackTarget.Heal(damage * HPLeech);
            _armyUnit.Army.Provision += damage*HungerLeech;
        }
    }
}
#endif