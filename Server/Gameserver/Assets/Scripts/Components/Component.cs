﻿using System;
using UnityEngine;

namespace Assets.Scripts.Components
{
    public abstract class Component : MonoBehaviour, IPooledObject
    {
        private int _disables = 0;

        public virtual bool Enabled
        {
            get
            {
                return _disables == 0;
            }
        }

        public void Enable()
        {
            --_disables;
            if (_disables < 0) throw new InvalidOperationException("More enables then disables");
            if(Enabled) OnEnable();
        }

        public void Disable()
        {
            var notify = Enabled;
            ++_disables;
            if (notify) OnDisable();
        }

        public virtual void Initialize()
        {
            _disables = 0;
            OnEnable();
        }

        public virtual void Cleanup()
        {
            OnDisable();
        }
        public event Action<IPooledObject> ReadyToCollect;

        protected void OnReadyToCollect()
        {
            if (ReadyToCollect != null)
            {
                ReadyToCollect(this);
            }
        }

        protected virtual void OnEnable() { }
        protected virtual void OnDisable() { }
    }
}
