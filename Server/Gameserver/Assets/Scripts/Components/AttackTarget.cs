﻿using Assets.Scripts.Attributes;
using UnityEngine;

namespace Assets.Scripts.Components
{
    public class AttackTarget : Component
    {
        private const float ARMOR_VALUE_DELIMETER = 1000f;

        [SerializeField]
        private float _currentHP;

        public FloatAttribute MaxHP;
        public MinMaxFloatAttribute Armor;
        public MultiplierAttribute SpellDefence;
        public MultiplierAttribute AmplifyIncomingDamage;
        public FloatAttribute HPRegen;

        void Awake()
        {
            MaxHP.ValueChanged += MaxHpOnValueChanged;
        }

        void FixedUpdate()
        {
            if (Alive && HPRegen > 0)
            {
                Heal(HPRegen * Time.deltaTime);
            }
        }

        private void MaxHpOnValueChanged(float maxHp)
        {
            if (_currentHP > maxHp) CurrentHP = maxHp;
        }

        public float CurrentHP
        {
            get { return _currentHP; }
            set
            {
                _currentHP = value;
                Alive = _currentHP > 0;
            }
        }

        public float ReceiveDamage(DamageStruct damage)
        {
            float totalDamage = damage;
            if (damage != DamageType.Unblockable)
            {
                // mitigate with armor
                totalDamage /= 1f + (Armor + ARMOR_VALUE_DELIMETER);
                // amplify with multiplier
                totalDamage *= AmplifyIncomingDamage;
            }
            if (damage == DamageType.Spell)
            {
                totalDamage *= SpellDefence;
            }
            _currentHP -= totalDamage;
            return totalDamage;
        }

        public void Heal(float value)
        {
            CurrentHP += value;
            if (CurrentHP > MaxHP) CurrentHP = MaxHP;
        }

        public void Heal()
        {
            CurrentHP = MaxHP;
        }

        public bool Alive { get; private set; }

        #region Overrides of Component

        public override void Cleanup()
        {
            base.Cleanup();
            Alive = false;
            Armor.Reset();
            SpellDefence.Reset();
            AmplifyIncomingDamage.Reset();
            HPRegen.Reset();
        }

        public override void Initialize()
        {
            base.Initialize();
            Alive = true;
            Heal();
        }

        #endregion
    }
}
