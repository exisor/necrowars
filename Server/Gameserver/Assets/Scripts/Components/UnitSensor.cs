﻿#if NEW_NETWORK
using Assets.Scripts.Abilities;
using Assets.Scripts.Attributes;
using UnityEngine;

namespace Assets.Scripts.Components
{
    public class UnitSensor : TargetSensor
    {
        private ArmyUnit _host;
        private readonly Unit[] _target = new Unit[1];
        [SerializeField] private int _targetUpdatePeriod;

        private int _aggroTries;

        public FloatAttribute Range;

        private float _squareRange;

        void Awake()
        {
            _host = GetComponent<ArmyUnit>();
            Range.ValueChanged += RangeOnValueChanged;
            RangeOnValueChanged(Range);
        }

        private void RangeOnValueChanged(float f)
        {
            _squareRange = f*f;
        }


        void FixedUpdate()
        {
            if (_aggroTries > 0)
            {
                --_aggroTries;
            }
        }


#region Overrides of TargetSensor

        public override Unit[] Targets
        {
            get { return _target; }
        }


        public override Unit FirstTarget
        {
            get
            {
                if (TargetIsValid(_target[0]))
                {
                    return _target[0];
                }
                UpdateTarget();
                return _target[0];
            }
        }

        private bool TargetIsValid(Unit target)
        {
            if (target == null) return false;
            if (!target.AttackTarget.Alive) return false;
            if (target.ArmyUnit.Army == _host.Army) return false;
            if (Vector2Utilities.SquareDistance(gameObject.transform.position, target.Movement) > _squareRange)
                return false;

            return true;
        }

        private void UpdateTarget()
        {
            _target[0] = null;
            if (_aggroTries > 0) return;
            _aggroTries = _targetUpdatePeriod;
            foreach (var target in Physics2D.OverlapCircleAll(gameObject.transform.position, Range, Constants.UNITS_LAYER_MASK))
            {
                var unit = target.GetComponent<Unit>();
                if (!TargetIsValid(unit)) continue;
                _target[0] = unit;
            }
        }

#endregion

#region Overrides of Component

        public override void Cleanup()
        {
            base.Cleanup();
            _aggroTries = 0;
            _target[0] = null;
            Range.Reset();
        }

#endregion
    }
}
#endif