using Assets.Scripts.Attributes;

namespace Assets.Scripts.Components
{
    public struct DamageStruct
    {
        public float Amount;
        public DamageType Type;

        public DamageStruct(float amount, DamageType type)
        {
            Amount = amount;
            Type = type;
        }

        public static implicit operator float(DamageStruct damage)
        {
            return damage.Amount;
        }

        public static implicit operator DamageType(DamageStruct damage)
        {
            return damage.Type;
        }
    }
}