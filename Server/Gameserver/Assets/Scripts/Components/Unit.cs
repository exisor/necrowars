﻿#if NEW_NETWORK
using UnityEngine;

namespace Assets.Scripts.Components
{
    [RequireComponent(typeof(Movement))]
    [RequireComponent(typeof(Attack))]
    [RequireComponent(typeof(AttackTarget))]
    [RequireComponent(typeof(UnitSensor))]
    [RequireComponent(typeof(ArmyUnit))]
    public class Unit : Component
    {
        private Movement _movement;
        private Attack _attack;
        private AttackTarget _attackTarget;
        private UnitSensor _unitSensor;
        private ArmyUnit _armyUnit;
        void Awake()
        {
            _attackTarget = GetComponent<AttackTarget>();
            _attack = GetComponent<Attack>();
            _movement = GetComponent<Movement>();
            _unitSensor = GetComponent<UnitSensor>();
            _armyUnit = GetComponent<ArmyUnit>();
        }

        public Movement Movement { get { return _movement; } }
        public Attack Attack { get { return _attack; } }
        public AttackTarget AttackTarget { get { return _attackTarget; } }
        public UnitSensor UnitSensor { get { return _unitSensor; } }
        public ArmyUnit ArmyUnit { get { return _armyUnit; } }

        #region helpers
        public static implicit operator Movement(Unit u)
        {
            return u.Movement;
        }
        public static implicit operator Attack(Unit u)
        {
            return u.Attack;
        }
        public static implicit operator AttackTarget(Unit u)
        {
            return u.AttackTarget;
        }
        public static implicit operator UnitSensor(Unit u)
        {
            return u.UnitSensor;
        }
        public static implicit operator ArmyUnit(Unit u)
        {
            return u.ArmyUnit;
        }
        #endregion
    }

}
#endif