﻿#if NEW_NETWORK
using System;
using Assets.Scripts.Attributes;
using UnityEngine;
using Random = UnityEngine.Random;

namespace Assets.Scripts.Components
{
    [RequireComponent(typeof(TargetSensor))]
    public class Attack : Component
    {
        [SerializeField]
        private bool _attackBlockMovement;
        [SerializeField]
        private DamageType _damageType;

        public MinMaxFloatAttribute MinDamage;
        public MinMaxFloatAttribute MaxDamage;
        public MinMaxFloatAttribute AttackSpeed;

        private float _attackTimer;
        private Movement _movement;
        private TargetSensor _targetSensor;
        void Awake()
        {
            _movement = GetComponent<Movement>();
            _targetSensor = GetComponent<TargetSensor>();
        }

        void FixedUpdate()
        {
            if (_attackTimer > 0)
            {
                _attackTimer -= Time.deltaTime;
                if (_movement != null && _attackBlockMovement && _attackTimer <= 0)
                {
                    _movement.Enable();
                }
            }
            else
            {
                var target = _targetSensor.FirstTarget;
                if (target != null)
                {
                    MakeAttack(target);
                }
            }
        }

        public void MakeAttack(Unit unit)
        {
            _attackTimer = 1/AttackSpeed;
            if (_attackBlockMovement && _movement != null) _movement.Disable();
            var damage = new DamageStruct(Random.Range(MinDamage, MaxDamage), _damageType);
            var damageDealt = unit.AttackTarget.ReceiveDamage(damage);
            OnAttacking(unit, damageDealt);
            if (!unit.AttackTarget.Alive)
            {
                OnKill(unit);
            }
        }

#region Overrides of Component

        public override bool Enabled
        {
            get { return base.Enabled && (_attackTimer <= 0); }
        }

        public override void Cleanup()
        {
            base.Cleanup();
            MinDamage.Reset();
            MaxDamage.Reset();
            AttackSpeed.Reset();
        }

#endregion

        public event Action<Unit, float> Attacking;
        public event Action<Unit> Kill;
        protected virtual void OnAttacking(Unit obj, float damage)
        {
            if (Attacking != null) Attacking(obj, damage);
        }

        protected virtual void OnKill(Unit obj)
        {
            if (Kill != null) Kill(obj);
        }
    }
}
#endif