﻿using System.Collections.Generic;

namespace Assets.Scripts.Components
{
    public abstract class TargetSensor : Component
    {
        public abstract Unit[] Targets { get; }
        public abstract Unit FirstTarget { get; }
    }
}
