﻿#if NEW_NETWORK
using UnityEngine;

namespace Assets.Scripts.Components
{
    public class Corpse : Component
    {
        [SerializeField] private float _corpsePersist;

        private float _corpseTimer;

        #region Overrides of Component

        protected override void OnEnable()
        {
            _corpseTimer = 0;
        }

        #endregion
    }
}
#endif