﻿#if NEW_NETWORK
using Assets.Scripts.Abilities;
using Assets.Scripts.Attributes;
using UnityEngine;

namespace Assets.Scripts.Components
{
    [RequireComponent(typeof (Rigidbody2D))]
    public class Movement : Component
    {
        private const float ATTRACTION_TARGET_MINIMAL_DISTANCE = 5.0f;

        private Transform _attractionTarget;
        private Vector2 _moveTarget;
        private Rigidbody2D _rigidbody;

        public FloatAttribute AttractionSpeed;
        public FloatAttribute Speed;

        private void Awake()
        {
            _rigidbody = GetComponent<Rigidbody2D>();
        }

        public void SetMoveTarget(Vector2 moveTarget)
        {
            _moveTarget = moveTarget;
        }

        // like 15% of frame
        public void MakeMove()
        {
            var velocity = Vector2.zero;
            // if distance is very close then do not move to target
            if (Vector2Utilities.SquareDistance(this, _moveTarget) > Constants.VECTOR_EPSILON)
            {
                velocity += (_moveTarget - this).ChangeMagnitude(Speed);
            }
            // if no attraction target or distance is too small then do not move towards
            if (_attractionTarget != null &&
                Vector2Utilities.SquareDistance(this, _attractionTarget.position) > ATTRACTION_TARGET_MINIMAL_DISTANCE
                && AttractionSpeed != 0)
            {
                velocity += ((Vector2) _attractionTarget.position - this).ChangeMagnitude(AttractionSpeed);
            }
            _rigidbody.velocity = velocity;
        }

        public void SetAttractionTarget(Transform attractionTransform)
        {
            _attractionTarget = attractionTransform;
        }

        public static implicit operator Vector2(Movement movement)
        {
            return movement.gameObject.transform.position;
        }

#region Overrides of Component

        public override void Initialize()
        {
            base.Initialize();
            _moveTarget = this;
        }


        public override void Cleanup()
        {
            base.Cleanup();
            Speed.Reset();
            AttractionSpeed.Reset();
            _attractionTarget = null;
            _moveTarget = Vector2.zero;
        }

        protected override void OnEnable()
        {
            _rigidbody.constraints = RigidbodyConstraints2D.FreezeRotation;
        }


        protected override void OnDisable()
        {
            _rigidbody.constraints = RigidbodyConstraints2D.FreezeAll;
        }

#endregion
    }
}
#endif