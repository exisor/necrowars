﻿
using Assets.Scripts.Attributes;
using UnityEngine;

namespace Assets.Scripts
{
    public class AttributeTestBehaviour : MonoBehaviour
    {
        public FloatAttributeS attribute;
        public FloatBonusS bonus;

        public void AddBonus()
        {
            attribute.AddBonus(bonus);
        }

        public void RemoveBonus()
        {
            attribute.RemoveBonus(bonus);
        }
    }
}
