﻿#if NEW_NETWORK
using UnityEngine.Networking;

namespace Assets.Scripts.Network
{
    public class Server
    {
        private static Server _instance;

        public static void Start()
        {

        }

        public static void Initialize(ConnectionConfig config, int maxConnections)
        {
            _instance = new Server();
            _instance.InitializeInternal(config, maxConnections);
        }

        private HostTopology _hostTopology;

        private void InitializeInternal(ConnectionConfig config, int maxConnections)
        {
            NetworkTransport.Init();

            _hostTopology = new HostTopology(config, maxConnections);
            var globalconfig = new GlobalConfig();
        }
    }
}
#endif