﻿#if NEW_NETWORK
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

namespace Assets.Scripts.Network
{
    public class FieldCell
    {
        public const float SQUARE_VELOCITY_THRESHOLD = 0.1f;

        private readonly List<Rigidbody2D> _rigidObjects = new List<Rigidbody2D>();
        private readonly List<NetworkConnection> _observers = new List<NetworkConnection>();

        private bool _observed;

        public bool Observed
        {
            get { return _observed; }
            private set
            {
                if (_observed == value) return;
                _observed = value;
                if (_observed)
                {
                    OnObserveStarted();
                }
                else
                {
                    OnObserveFinished();
                }
            }
        }

        public void Subscribe(NetworkConnection connection)
        {
            _observers.Add(connection);
            Observed = true;
        }

        public void Unsubscribe(NetworkConnection connection)
        {
            _observers.Remove(connection);
            if (_observers.Count == 0)
            {
                Observed = false;
            }
        }

        private void OnObserveStarted()
        {
            
        }


        private void OnObserveFinished()
        {
            
        }

        void Update()
        {
            for (var i = 0; i < _rigidObjects.Count; ++i)
            {
                var rigidToUpdate = _rigidObjects[i];
                if (rigidToUpdate.velocity.sqrMagnitude < SQUARE_VELOCITY_THRESHOLD) continue;


            }

            for (var i = 0; i < _observers.Count; ++i)
            {
                var observer = _observers[i];
                //observer.
            }
        }
    }
}
#endif