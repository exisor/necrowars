﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

namespace Assets.Scripts.Network
{
    [RequireComponent(typeof(CircleCollider2D))]
    public class NetworkObserver : NetworkBehaviour
    {
        [SerializeField] private int _radius = 50;
        [SerializeField] private float _updateInterval = 2f;

        private float _timer;
        private CircleCollider2D _collider;

        private NetworkConnection _connection;
        void Start()
        {
            _connection = transform.parent.GetComponent<ArmyLeader>().Army.connectionToClient;
            if (_connection == null)
            {
                Debug.Log("No connection to client");
                Destroy(gameObject);
                return;
            }

            _collider = GetComponent<CircleCollider2D>();

            _collider.radius = _radius;
        }
        void FixedUpdate()
        {
            _timer += Time.deltaTime;
            if (_timer >= _updateInterval)
            {
                _timer = 0;
                Observe();
            }
        }

        private void Observe()
        {
            for (var i = 0; i < _visibleObjects.Count; ++i)
            {
                _visibleObjects[i].Observe(_connection);
            }
            _visibleObjects.Clear();
            for (var i = 0; i < _missingObjects.Count; ++i)
            {
                var o = _missingObjects[i];
                if (o != null) 
                {
                    o.Miss(_connection);
                }
            }
            _missingObjects.Clear();
        }

        private readonly List<NetworkVisibleObject> _visibleObjects = new List<NetworkVisibleObject>();
        private readonly List<NetworkVisibleObject> _missingObjects = new List<NetworkVisibleObject>();
        void OnTriggerEnter2D(Collider2D c)
        {
            var o = c.GetComponent<NetworkVisibleObject>();
            if (o != null)
            {
                _visibleObjects.Add(o);
            }
        }

        void OnTriggerExit2D(Collider2D c)
        {
            var o = c.GetComponent<NetworkVisibleObject>();
            if (o != null)
            {
                if (_visibleObjects.Remove(o)) return;

                _missingObjects.Add(o);
            }
        }
    }
}
