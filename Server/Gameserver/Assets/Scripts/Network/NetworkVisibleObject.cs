﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

namespace Assets.Scripts.Network
{
    [RequireComponent(typeof(NetworkIdentity))]
    public class NetworkVisibleObject : NetworkBehaviour
    {
        private NetworkIdentity _identity;
        private readonly List<NetworkConnection> _observers = new List<NetworkConnection>();
        private readonly HashSet<NetworkConnection> _connections = new HashSet<NetworkConnection>();

        void Awake()
        {
            _identity = GetComponent<NetworkIdentity>();
        }

        public void Observe(NetworkConnection conn)
        {
            if (_connections.Contains(conn)) return;
            _observers.Add(conn);
            _connections.Add(conn);
            RebuildObservers();
        }

        public void Miss(NetworkConnection conn)
        {
            _observers.Remove(conn);
            _connections.Remove(conn);
            RebuildObservers();
        }
        public override bool OnRebuildObservers(HashSet<NetworkConnection> observers, bool initialize)
        {
            for (var i = _observers.Count - 1; i >=0 ; --i)
            {
                var o = _observers[i];
                if (!o.isConnected)
                {
                    _observers.Remove(o);
                    continue;
                }
                observers.Add(o);
            }
            return true;
        }

        private void RebuildObservers()
        {

            if (_identity.isServer)
                _identity.RebuildObservers(false);
        }

        public override bool OnCheckObserver(NetworkConnection conn)
        {
            return _observers.Contains(conn);
        }

        
        public override void OnStartServer()
        {
            base.OnStartServer();
            RebuildObservers();
        }

        public override void OnNetworkDestroy()
        {
            _observers.Clear();
            RebuildObservers();
        }

    }
}
