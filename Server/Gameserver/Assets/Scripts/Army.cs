﻿using System;
using System.Collections.Generic;
using System.Linq;
using Assets.Scripts.Abilities;
using Assets.Scripts.Items;
using Assets.Scripts.Pickups;
using UnityEngine;
using UnityEngine.Networking;
using Random = UnityEngine.Random;

namespace Assets.Scripts
{
    public class Army : NetworkBehaviour
    {
        #region private members
#if COMBINE_UNITS
        private readonly Dictionary<long, List<Unit>> _unitsDictionary = new Dictionary<long, List<Unit>>();
#endif
        private readonly List<Unit> _units = new List<Unit>();
        private const float POINTS_INCREASE_TIME_INTERVAL = 5f;
        private const float POINTS_INCREASE_PERCENT = 0.002f;
        private const float POINTS_INCREASE_COMMON_UNIT = 0.003f;
        private const float POINTS_INCREASE_LEADER = 0.02f;
#if ABILITY_ENABLED
        private readonly List<ActiveAbility> _activeAbilities = new List<ActiveAbility>();
        private readonly List<PassiveAbility> _passiveAbilities = new List<PassiveAbility>(); 
#endif
        private readonly List<MinionsBonus> _minionsBonuses = new List<MinionsBonus>(); 
        private int _level;

        private const float MAX_INPUT_TIMEOUT = 3f;
        private float _inputTimer = 0;
        private const float SQUARE_DISTANCE_THRESHOLD_FOR_EXTENDING_TARGET = 100f;
#endregion
#region Editor interface
        [SerializeField]
        private int _unitsCountTreshold = 25;
        public ArmyType ArmyOwner;
        private float _timer;
#endregion
#region Network delegates
        public delegate void EventCombineUnitsDelegate(NetworkInstanceId combineTo, NetworkInstanceId combineThis);
#endregion
#region Network interface
        [SerializeField]
        //[SyncVar]
        private int _provision;
#if SYNC_ARMY
        [SyncVar]
#endif
        private bool _destroyed;
#if SYNC_ARMY
        [SyncVar]
#endif
        public string PlayerName;
#if SYNC_ARMY
        [SyncVar]
#endif
        private int _points;
        [SyncVar]
        private long _hatId;
        [SerializeField]
        [SyncVar]
        private int _aliveUnits;
        [SyncVar]
        public int VikingsLimit;

        public void SetHatId(long id)
        {
            _hatId = id;
        }
        public int Points
        {
            get
            {
                return _points;
            }
            set
            {
                _points = value;
                TopArmyBehaviour.Instance.PointsUpdate(this);
            }
        }

        public DateTime StartTime;
        public DateTime? DeadTime;
#if SYNC_ARMY
        [SyncEvent]
        public event EventCombineUnitsDelegate EventCombineUnits;
#endif

        [Command(channel = 2)]
        public void CmdMove(Vector2 target)
        {
            if (Leader == null) return;
            var squareDistance = (target - Leader.pos).sqrMagnitude;
            var newTarget = (squareDistance > SQUARE_DISTANCE_THRESHOLD_FOR_EXTENDING_TARGET)
                ? (target - Leader.pos).normalized*1000f
                : target;
            OrderMove(newTarget);
        }
#if DEBUG
        [Command]
        public void CmdCreateArmy()
        {
            ArmiesManager.Instance.CreateAiArmy();
        }
#endif
#endregion
#region public interface
#if FIXED_UPDATE
        void FixedUpdate()
#else
        public void ManagedUpdate()
#endif
        {
            if (!Destroyed)
            {
                _inputTimer += Time.deltaTime;
                _timer += Time.deltaTime;
                if (_timer > POINTS_INCREASE_TIME_INTERVAL)
                {
                    _timer = 0;
                    Points += (int) Math.Min(Points*POINTS_INCREASE_PERCENT, 1000);
                }
                if (_inputTimer > MAX_INPUT_TIMEOUT)
                {
                    if (Leader != null)
                    {
                        OrderMove(Leader.pos);
                    }
                }
            }
#if !FIXED_UPDATE
            for (var i = 0; i < _units.Count; ++i)
            {
                _units[i].ManagedUpdate();
            }
            if (!Destroyed)
            {
                for (var i = 0; i < _activeAbilities.Count; ++i)
                {
                    _activeAbilities[i].ManagedUpdate();
                }
            }
#endif
        }
        public void AddUnit(Unit unit)
        {
            _units.Add(unit);
            if (unit.Alive)
                _aliveUnits++;
#if COMBINE_UNITS
            List<Unit> storedUnits;
            if (!_unitsDictionary.TryGetValue(unit.Id, out storedUnits))
            {
                storedUnits = new List<Unit>();
                _unitsDictionary[unit.Id] = storedUnits;
            }
            storedUnits.Add(unit);
            if (_units.Count(u => u.Alive) > _unitsCountTreshold)
            {
                Combine(unit);
            }
#endif
            if (unit.IsLeader) Leader = (ArmyLeader)unit;
            ApplyBonuses(unit);
            if (UnitAdded != null) UnitAdded(unit);
        }
        private void ApplyBonuses(Unit unit, bool levelOnly = false)
        {
            unit.LevelDamageBonus.Level = Level - 1;
            var maxHp = unit.MaxHP;
            unit.LevelHpBonus.Level = Level - 1;
            var hpDiff = unit.MaxHP - maxHp;
            if (hpDiff > 0)
            {
                unit.Heal(unit.MaxHP - maxHp);
            }
            unit.LevelGoldBonus.Level = Level;
            unit.LevelHungerBonus.Level = Level - 1;
            unit.LevelExpBonus.Level = Level;
            if (!levelOnly)
            { _minionsBonuses.ForEach(b => b.ApplyBonus(unit));}

        }
        public void RemoveUnit(Unit unit)
        {
            _units.Remove(unit);
            if (unit.Alive) _aliveUnits--;
#if COMBINE_UNITS
            _unitsDictionary[unit.Id].Remove(unit);
#endif
            if (Leader == unit) Leader = null;
            RemoveBonuses(unit);
            if (UnitRemoved != null) UnitRemoved(unit);
        }
        private void RemoveBonuses(Unit unit)
        {
            unit.LevelDamageBonus.Level = 0;
            unit.LevelHpBonus.Level = 0;
            unit.LevelHungerBonus.Level = 0;
            unit.LevelExpBonus.Level = 0;
            unit.LevelGoldBonus.Level = 0;
            _minionsBonuses.ForEach(b => b.RemoveBonus(unit));
        }

        public int AliveUnits { get { return _aliveUnits; } set
        {
            if (value == _aliveUnits) return;
            _aliveUnits = value;
            if (AliveUnitsChanged != null) AliveUnitsChanged(value);
        } }
        public ArmyLeader Leader { get; private set; }
        public bool Destroyed
        {
            get { return _destroyed; }
            set
            {
                _destroyed = value;
                if (_destroyed)
                {
                    DeadTime = DateTime.Now;
                    for (var i = 0; i < _units.Count; ++i)
                    {
                        var u = _units[i];
                        if (u.Alive) Damage.Unblockable(u, u, u.MaxHP);
                    }
                    _aliveUnits = 0;
                    TopArmyBehaviour.Instance.NotifyDestroyed(this);
                }
                else
                {
                    StartTime = DateTime.Now;
                    DeadTime = null;
                }
            }
        }
        public void Clear()
        {
            _units.ForEach(u =>
            {
                u.OnReadyToCollect();
            });
#if COMBINE_UNITS
            _unitsDictionary.Clear();
#endif
            _units.Clear();
#if ABILITY_ENABLED
            _activeAbilities.ToList().ForEach(a => RemoveAbility(a));
            _passiveAbilities.ToList().ForEach(a => RemoveAbility(a));
#endif
            Kill = null;
            AliveUnitsChanged = null;
            PickedUpBonus = null;
            UnitAdded = null;
            UnitRemoved = null;
        }
        public void Init()
        {
            Destroyed = false;
            Points = 0;
            _pickups = 0;
#if ABILITY_ENABLED
            for (var i = _passiveAbilities.Count - 1; i >=0; --i)
            {
                _passiveAbilities[i].Enable();
            }
#endif
        }
        public void OrderMove(Vector2 target)
        {
            _inputTimer = 0;
            if (Destroyed) return;
            for (var i = 0; i < _units.Count; ++i)
            {
                var u = _units[i];
                if (u.Alive) u.SetMoveTarget(target);
            }
        }
        public bool IsEmpty
        {
            get { return _units.Count == 0; }
        }
        public event Action<Unit> Kill;
        public event Action<Unit> UnitAdded;
        public event Action<Unit> UnitRemoved;
        public virtual void OnKill(Unit obj)
        {
#if DEBUG
            if (obj == null) throw new ArgumentNullException("obj");
            if (obj.Army == null)
                throw new ArgumentNullException("obj->Army");
#endif

            Points += (int) (obj.GoldReward.Value * 0.5f + Math.Min(obj.Army.Points * (obj.IsLeader ? POINTS_INCREASE_LEADER : POINTS_INCREASE_COMMON_UNIT), Points/3f));
            var handler = Kill;
            if (handler != null) handler(obj);
        }
#if ABILITY_ENABLED
        public void AddAbility(ActiveAbility ability)
        {
            ability.transform.SetParent(transform, false);
            ability.Army = this;
            _activeAbilities.Add(ability);
        }

        public void AddAbility(PassiveAbility ability)
        {
            ability.transform.SetParent(transform, false);
            ability.Army = this;
            _passiveAbilities.Add(ability);
        }

        public void RemoveAbility(ActiveAbility ability)
        {
            _activeAbilities.Remove(ability);
            ability.Army = null;
            if (ArmyOwner == ArmyType.Player)
            {
                NetworkServer.UnSpawn(ability.gameObject);
            }
            Destroy(ability.gameObject);
        }

        public void RemoveAbility(PassiveAbility ability)
        { 
            ability.Disable();
            _passiveAbilities.Remove(ability);
            Destroy(ability.gameObject);
        }
#endif
        public void AddMinionBonus(MinionsBonus bonus)
        {
            _minionsBonuses.Add(bonus);
            foreach (var unit in Units.Where(u => !u.IsLeader))
            {
                bonus.ApplyBonus(unit);
            }
        }

        public void RemoveMinionBonus(MinionsBonus bonus)
        {
            _minionsBonuses.Remove(bonus);
            foreach (var unit in Units.Where(u => !u.IsLeader))
            {
                bonus.RemoveBonus(unit);
            }
        }
#if ABILITY_ENABLED
        public List<ActiveAbility> ActiveAbilities { get { return _activeAbilities; } }
        public IEnumerable<PassiveAbility> PassiveAbilities { get { return _passiveAbilities; } }
#endif
        public List<Unit> Units { get { return _units; } }
        #endregion
#region logic
#if COMBINE_UNITS
        private void Combine(Unit unit)
        {
            var minQuantity = 99999;
            Unit pair = null;
            foreach (var list in _unitsDictionary)
            {
                var ordered = list.Value.Where(x => x.Alive).OrderBy(x => x.Quantity).ToArray();
                if (ordered.Length < 2) continue;
                var takenMinimumFromList = ordered[0].Quantity + ordered[1].Quantity;
                if (takenMinimumFromList >= minQuantity) continue;

                minQuantity = takenMinimumFromList;
                pair = ordered[0];
                unit = ordered[1];
            }

            pair.Quantity += unit.Quantity;
#if SYNC_ARMY
            EventCombineUnits(pair.netId, unit.netId);
#endif
            pair.Heal(unit.MaxHP);
            unit.OnReadyToCollect();
        }
#endif
#endregion
#region GameLogic

        // Level
        public int Level
        {
            get { return _level; }
            set
            {
                _level = value; 
                _units.ForEach(u =>
                {
                    ApplyBonuses(u, true);
                });
                Provision = 1000.0f + (_level * 10.0f);
            }
        }

        public float Provision
        {
            get { return _provision; }
            set
            {
                //_provision = (int)value;
                //if (_provision < 0.0f) ConsumeUnit();
            }
        }

        private void ConsumeUnit()
        {
            var unit = _units.Where(u => u.Alive && !u.IsLeader).OrderBy(u => u.HP).FirstOrDefault();
            if (unit != null)
            {
                var damage = Damage.Unblockable(Leader, unit, unit.HP);
                if (damage < 0 || damage > 10000) return;
                Provision += damage;
            }
        }

#endregion

        public static Army operator +(Army army, MinionsBonus minionBonus)
        {
            army.AddMinionBonus(minionBonus);
            return army;
        }

        public static Army operator -(Army army, MinionsBonus minionBonus)
        {
            army.RemoveMinionBonus(minionBonus);
            return army;
        }

        public static bool operator ==(Army l, Army r)
        {
            return ReferenceEquals(l, r);
        }
        public static bool operator !=(Army l, Army r)
        {
            return !ReferenceEquals(l, r);
        }

        public override bool Equals(object o)
        {
            return ReferenceEquals(this, o);
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }

        private int _pickups;
        public void ConsumeBonus(PickupBonus pickupBonus)
        {
            _pickups++;
#if QUANTITY_ENABLED
            if (_pickups%2 == 0)
            {
                var unit = _units.Where(u => u.Alive && !u.IsLeader).OrderBy(_ => Random.Range(0f, 1f)).FirstOrDefault();
                if (unit != null)
                {
                    unit.Quantity++;
                }
            }
#endif
            Points += (int)pickupBonus.PointsBonus.Value;
            OnPickedUpBonus(pickupBonus);
            if (ArmyOwner == ArmyType.Player)
            {
                TargetPickupBonus(connectionToClient);
            }
        }

        protected void OnPickedUpBonus(PickupBonus b)
        {
            if (PickedUpBonus != null)
            {
                PickedUpBonus(b);
            }
        }
        public event Action<PickupBonus> PickedUpBonus;

        [TargetRpc]
        private void TargetPickupBonus(NetworkConnection target)
        {    
        }

        [Command]
        public void CmdInteract(NetworkInstanceId netid)
        {
            var o = NetworkServer.FindLocalObject(netid);
            if (o == null) return;
            var interactive = o.GetComponent<IInteractive>();
            if (interactive == null) return;

            interactive.Interact(this);
        }

        public void AliveChanged(bool value)
        {
            if (value) AliveUnits++;
            else AliveUnits--;
        }

        public event Action<int> AliveUnitsChanged;
    }
}
