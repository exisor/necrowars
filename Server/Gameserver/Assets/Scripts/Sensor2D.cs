﻿using System;
using UnityEngine;

namespace Assets.Scripts
{
    [RequireComponent(typeof(CircleCollider2D))]
    public class Sensor2D : MonoBehaviour
    {

        private Collider2D _collider;
        void Awake()
        {
            _collider = GetComponent<CircleCollider2D>();
        }

        void OnTriggerEnter2D(Collider2D other)
        {
            var unit = other.GetComponent<Unit>();
            OnEnter(unit);
        }

        void OnTriggerExit2D(Collider2D other)
        {
            var unit = other.GetComponent<Unit>();
            OnLeave(unit);
        }

        public event Action<Unit> Enter;
        public event Action<Unit> Leave;

        protected virtual void OnEnter(Unit obj)
        {
            var handler = Enter;
            if (handler != null) handler(obj);
        }

        protected virtual void OnLeave(Unit obj)
        {
            var handler = Leave;
            if (handler != null) handler(obj);
        }

        void OnDisable()
        {
            _collider.enabled = false;
        }

        void OnEnable()
        {
            _collider.enabled = true;
        }
    }
}
