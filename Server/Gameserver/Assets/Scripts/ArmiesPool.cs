﻿using System;
using System.Collections.Generic;
using System.Linq;
using Assets.Scripts.Abilities;
using Assets.Scripts.DTO;
using Common.Resources;
using UnityEngine;
using UnityEngine.Networking;
#if ABILITY_ENABLED
using Ability = Assets.Scripts.Abilities.Ability;
#endif
using Random = UnityEngine.Random;

namespace Assets.Scripts
{
    public class ArmiesPool : MonoBehaviour
    {
        [Serializable]
        public class ArmySetup
        {
            public ArmyType Type;
            public ArmyData Data;
            public GameObject Prefab;
        }

        [SerializeField] private UnitsPool _unitsPool;
        [SerializeField] private ArmySetup[] _armiesSetup;
#if ABILITY_ENABLED
        [SerializeField] private Ability[] _abilities;
        [SerializeField] private PassiveAbility[] _passiveAbilities;
#endif
        private readonly Dictionary<ArmyType, Stack<GameObject>> _pooledObjects = new Dictionary<ArmyType, Stack<GameObject>>
        {
            { ArmyType.Ai, new Stack<GameObject>()},
            { ArmyType.Player, new Stack<GameObject>()},
        };
        public static ArmiesPool Instance { get; private set; }

        void Awake()
        {
            Instance = this;
        }
        public Army CreateArmy(Vector2 position, ArmyType type, Common.DTO.Character data)
        {
            var armySetup = _armiesSetup.Where(a => a.Type == type).OrderBy(a => Random.Range(0, 100)).FirstOrDefault();
            if (armySetup == null) throw new NullReferenceException();

            var armyObject = GetArmyFromPool(armySetup);

            // InitializeArmy

            armyObject.transform.position = Vector3.zero;
            armyObject.SetActive(true);
            
            var retval = armyObject.GetComponent<Army>();


            NetworkServer.Spawn(armyObject);

            // create units
            var leader = _unitsPool.CreateUnit(position, armySetup.Data.ArmyLeader);

            InitializeUnit(retval, leader);

            armySetup.Data.Units.ForEach(u => InitializeUnit(retval, _unitsPool.CreateUnit(position.Jitter(), u)));
#if ABILITY_ENABLED
            // resurrection
            var resAbilityData = data.Abilities.FirstOrDefault(a => a.Id == 1);
            if (resAbilityData != null)
            {
                var res = CreateAbility(data.Abilities.First());
                retval.AddAbility(res);
                if (type == ArmyType.Ai)
                {
                    NetworkServer.Spawn(res.gameObject);
                }
            }

            // other abilities
            data.AbilitySlots
                .Where(slot => slot.AbilityId != 0)
                .Select(slot =>
                {
                    var abilityInSlot = CreateAbility(data.Abilities.First(ability => ability.Id == slot.AbilityId));
                    abilityInSlot.SetSlotNumber(slot.Slot);
                    return abilityInSlot;
                })
                .ToList()
                .ForEach(a =>
                {
                    if (type == ArmyType.Ai)
                    {
                        NetworkServer.Spawn(a.gameObject);
                    }
                    retval.AddAbility(a);
                });
            foreach (var ability in _passiveAbilities.Select(passiveAbility => data.Abilities.FirstOrDefault(a => a.Id == passiveAbility.Id)).Where(ability => ability != null && ability.Level > 0))
            {
                retval.AddAbility(CreatePassiveAbility(ability));
            }
#endif
            retval.Level = data.Level;
            return retval;
        }

        private void InitializeUnit(Army army, Unit unit)
        {
            unit.Army = army;
        }

        private GameObject GetArmyFromPool(ArmySetup setup)
        {
            if (setup.Type != ArmyType.Player)
            {
                var pool = _pooledObjects[setup.Type];
                if (pool.Count > 0) return pool.Pop();
            }

            var army = (GameObject)Instantiate(setup.Prefab, transform);
            army.SetActive(false);
            return army;
        }

        public void DestroyArmy(Army army)
        {
            if (army.ArmyOwner == ArmyType.Player)
            {
                NetworkServer.UnSpawn(army.gameObject);
                Destroy(army.gameObject);
                return;
            }

            army.Clear();
            army.gameObject.SetActive(false);

            NetworkServer.UnSpawn(army.gameObject);
            SendArmyToPool(army);
        }

        private void SendArmyToPool(Army army)
        {
            _pooledObjects[army.ArmyOwner].Push(army.gameObject);
            army.transform.SetParent(transform);
        }
#if ABILITY_ENABLED
        public ActiveAbility CreateAbility(Common.DTO.Ability data)
        {
            var retval = Instantiate(_abilities.First(a => a.Id == data.Id).gameObject).GetComponent<ActiveAbility>();
            return retval;
        }

        private PassiveAbility CreatePassiveAbility(Common.DTO.Ability data)
        {
            var retval = Instantiate(_passiveAbilities.First(a => a.Id == data.Id).gameObject).GetComponent<PassiveAbility>();
            retval.Level = data.Level;
            return retval;
        }
#endif
    }
}
