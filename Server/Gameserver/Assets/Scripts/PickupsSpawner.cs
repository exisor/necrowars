﻿using System.Collections.Generic;
using Assets.Scripts.Pickups;
using UnityEngine;
using UnityEngine.Networking;

namespace Assets.Scripts
{

    public class PickupsSpawner : MonoBehaviour
    {
        [SerializeField] private GameField _field;
        [SerializeField] private GameObject _pickupPrefab;
        public PickupBonus[] Pickups;
        [SerializeField]
        private float _step = 1;

        void Awake()
        {
            Spawn();
        }

        public void Spawn()
        {
            if (Pickups != null)
            {
                for (var i = Pickups.Length - 1; i >= 0; --i)
                {
                    DestroyImmediate(Pickups[i].gameObject);
                }
            }
            var list = new List<PickupBonus>();
            for (var x = -_field.Width/2.05f; x < _field.Width / 2.05f; x += _step)
            {
                for (var y = -_field.Height/2.05f; y < _field.Height / 2.05f; y += _step)
                {
                    var p =
                        ((GameObject) Instantiate(_pickupPrefab, new Vector2(x, y), Quaternion.identity))
                            .GetComponent<PickupBonus>();
                    p.Jitter = _step / 2;
                    list.Add(p);
                }
            }
            Pickups = list.ToArray();
        }

        void FixedUpdate()
        {
            for (int i = 0; i < Pickups.Length; ++i)
            {
                var p = Pickups[i];
                if (!p.Enabled)
                {
                    p.ManagedUpdate();
                }
            }
        }
    }
}
