﻿using ClientProtocol;

namespace Assets.Scripts
{
    public class PlayerAuthorized : NetworkMessageWrapper<ClientProtocol.AuthorizePlayerOnGameServerEvent>
    {
        public PlayerAuthorized() : base(new ClientProtocol.AuthorizePlayerOnGameServerEvent(), CustomGameMessageType.PlayerAuthorized)
        {
        }

        public PlayerAuthorized(ClientProtocol.AuthorizePlayerOnGameServerEvent message)
            : base(message, CustomGameMessageType.PlayerAuthorized)
        {
            
        }
    }
}