﻿using System;
using System.Linq;
using Assets.Scripts.Effects;
using Assets.Scripts.Network;
using Assets.Scripts.Pickups;
using ServerProtocol;
using UnityEngine;

namespace Assets.Scripts
{
    public class ArmyPlayerController : MonoBehaviour
    {
        [SerializeField] private Army _army;
        [SerializeField] private GameObject _playerSensorPrefab;

        private NecroserverManager.CharacterData _characterData;
        private int _armySize;
        private long _exp;
        private long _gold;
        private int _kills;

        private bool _destroyed;

        [SerializeField] private InvulnerableEffect InvulnerableEffectPrefab;

        void Start()
        {
            _army.Kill += ArmyOnKill;
            _army.Leader.Dead += LeaderOnDead;
            _army.UnitAdded += ArmyOnUnitAdded;
            _army.PickedUpBonus += ArmyOnPickedUpBonus;
            UpdateTopArmySize();
            foreach (var armyUnit in _army.Units)
            {
                var netObj = armyUnit.GetComponent<NetworkVisibleObject>();
                netObj.Observe(_army.connectionToClient);
                var e = Effect.Create(InvulnerableEffectPrefab);
                e.Target = armyUnit;
            }
            Instantiate(_playerSensorPrefab, _army.Leader.transform, false);
        }

        private void ArmyOnPickedUpBonus(PickupBonus pickupBonus)
        {
            GrantExp((long)pickupBonus.ExpBonus.Value);
            GrantGold((long)pickupBonus.GoldBonus.Value);
        }

        private void ArmyOnUnitAdded(Unit unit)
        {
            UpdateTopArmySize();
        }

        private void UpdateTopArmySize()
        {
            _armySize = Math.Max(_armySize, _army.Units.Where(u => u.Alive).Sum(u => u.Quantity));
        }

        private void LeaderOnDead(Unit unit)
        {
            if (_destroyed) return;

            _destroyed = true;
            _army.Leader.Dead -= LeaderOnDead;
            MmConnector.Send(new BattleEndedForPlayer
            {
                CharacterId = CharacterData.Character.Id,
                PlayerId = CharacterData.Character.UserId,
                Points = _army.Points,
                TimeAlive = (int)((_army.DeadTime ?? DateTime.Now) - _army.StartTime).TotalSeconds,
                ArmySize = _armySize,
                ExpGained = _exp,
                GoldGained = _gold,
                Kills = _kills,
                Top = _top
            });
        }

        private void ArmyOnKill(Unit unit)
        {
            if (unit.Army == _army) return;
            _kills++;
            GrantExp((long)unit.ExpReward.Value);
            GrantGold((long)unit.GoldReward.Value);
        }

        public NecroserverManager.CharacterData CharacterData
        {
            get { return _characterData; }
            set
            {
                _characterData = value;
                _army.Level = _characterData.Character.Level;
            }
        }

        public NecroserverManager.Connector MmConnector { get; set; }

        public void GrantExp(long exp)
        {
            _exp += exp;
        }

        public void GrantGold(long gold)
        {
            _gold +=  gold;
        }

        public void ConsumeItem(long baseId)
        {
            MmConnector.Send(new ConsumeItemEvent
            {
                ItemBaseId = baseId,
                PlayerId = CharacterData.Character.UserId
            });
        }

        private bool _top = false;
        public void Top()
        {
            _top = true;
        }

        void OnDestroy()
        {
            _army.Kill -= ArmyOnKill;
            _army.UnitAdded -= ArmyOnUnitAdded;
            _army.PickedUpBonus -= ArmyOnPickedUpBonus;

            LeaderOnDead(_army.Leader);
        }
    }
}
