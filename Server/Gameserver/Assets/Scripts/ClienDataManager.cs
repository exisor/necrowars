﻿#if ABILITY_ENABLED
using UnityEngine;
using System.IO;
using System.Xml.Serialization;
using System.Collections.Generic;
using Assets.Scripts.Abilities;
using System.Reflection;
using System.Linq;
using Assets.Scripts.Attributes;
using System;

public class ClienDataManager : MonoBehaviour
{
    public List<Ability> AbilityPrefabs = new List<Ability>();
    public List<PassiveAbility> PassiveAbilityPrefabs = new List<PassiveAbility>();

    public string SaveAbilities()
    {
        Debug.Log("Save Abilities");
        List<Common.ClientData> abilities = new List<Common.ClientData>();

        Common.ClientData clientData;
        foreach (var ability in AbilityPrefabs)
        {
            clientData = new Common.ClientData();
            clientData.Id = ability.Id;
            clientData.Values = new List<Common.ClientValue>();

            CreateClientData(ability, clientData);

            abilities.Add(clientData);
        }

        foreach (var passiveAbility in PassiveAbilityPrefabs)
        {
            clientData = new Common.ClientData();
            clientData.Id = passiveAbility.Id;
            clientData.Values = new List<Common.ClientValue>();

            CreateClientData(passiveAbility, clientData);

            abilities.Add(clientData);
        }

        var serializer = new XmlSerializer(typeof(List<Common.ClientData>));
        var stringWriter = new StringWriter();
        serializer.Serialize(stringWriter, abilities);
        return stringWriter.ToString();
    }

    private void CreateClientData(object obj, Common.ClientData clientData)
    {
        FieldInfo[] fields = obj.GetType().GetFields(BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance);

        foreach (FieldInfo field in fields)
        {
            var valueTag = field.GetCustomAttributes(typeof(Common.ClientValueTag), true).FirstOrDefault() as Common.ClientValueTag;
            
            if (valueTag != null)
            {
                float value = Math.Abs(GetFloatValue(field.GetValue(obj)));
                clientData.Values.Add(
                        CreateClientValue(value, obj.ToString() + "_" + field.Name, valueTag.LevelAddicted, valueTag.IsPercent));
            }

            var objectTag = field.GetCustomAttributes(typeof(Common.ClientObjectTag), true).FirstOrDefault() as Common.ClientObjectTag;
            if (objectTag != null)
            {
                CreateClientData(field.GetValue(obj), clientData);
            }
        }
    }

    private float GetFloatValue(object raw)
    {
        IAttribute<float> attribute = raw as IAttribute<float>;
        if (attribute != null)
        {
            return attribute.Value;
        }

        StraightLevelRelBonus straightLevelRelBonus = raw as StraightLevelRelBonus;
        if (straightLevelRelBonus != null)
        {
            FieldInfo field = straightLevelRelBonus.GetType().GetField("_modifier", BindingFlags.NonPublic | BindingFlags.Instance);
            
            return (float) field.GetValue(straightLevelRelBonus);
        }

        StraightLevelFixedBonus straightLevelFixedBonus = raw as StraightLevelFixedBonus;
        if (straightLevelFixedBonus != null)
        {
            FieldInfo field = straightLevelFixedBonus.GetType().GetField("_modifier", BindingFlags.NonPublic | BindingFlags.Instance);

            return (float)field.GetValue(straightLevelFixedBonus);
        }
        RelFloatBonus floatBonus = raw as RelFloatBonus;
        if (floatBonus != null)
        {
            FieldInfo field = floatBonus.GetType().GetField("Value", BindingFlags.NonPublic | BindingFlags.Instance);

            return (float)field.GetValue(floatBonus);
        }
        FixedFloatBonus fixedFloatBonus = raw as FixedFloatBonus;
        if (fixedFloatBonus != null)
        {
            FieldInfo field = fixedFloatBonus.GetType().GetField("Value", BindingFlags.NonPublic | BindingFlags.Instance);

            return (float)field.GetValue(fixedFloatBonus);
        }

        return (float)raw;
    }

    private Common.ClientValue CreateClientValue(float floatValue, string name, bool levelAddicted,bool isPercent)
    {
        var value = new Common.ClientValue();
        value.Value = floatValue;
        value.Name = name;
        value.LevelAddicted = levelAddicted;
        value.IsPercent = isPercent;
        return value;
    }
}
#endif