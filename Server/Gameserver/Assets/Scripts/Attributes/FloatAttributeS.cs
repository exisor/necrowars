﻿using System;
using System.Collections.Generic;
using System.Globalization;
using UnityEngine;

namespace Assets.Scripts.Attributes
{
    [Serializable]
    public struct FloatAttributeS : IAttributeStruct<float, FloatBonusS>
    {
        private bool _initialized;
        [SerializeField]
        private List<FloatBonusS> _bonuses;
        [SerializeField]
        private float _value;
        [SerializeField]
        private float _currentValue;

        [SerializeField] private bool _hasMaxValue;
        [SerializeField] private float _maxValue;
        [SerializeField] private bool _hasMinValue;
        [SerializeField] private float _minValue;
        public FloatAttributeS(float val)
        {
            _bonuses = new List<FloatBonusS>();
            _value = val;
            _currentValue = val;
            _initialized = true;
            ValueChanged = null;
            _hasMaxValue = false;
            _maxValue = float.NaN;
            _hasMinValue = false;
            _minValue = float.NaN;
        }
        public float Value
        {
            get
            {
                if (!_initialized)
                {
                    _currentValue = _value;
                    _initialized = true;
                }
                if (_hasMinValue && _currentValue < _minValue) return _minValue;
                if (_hasMaxValue && _currentValue > _maxValue) return _maxValue;
                return _currentValue;
            }
        }

        public void AddBonus(FloatBonusS bonus)
        {
            _bonuses.AddWithPriority<FloatBonusS, int>(bonus);
            UpdateValue();
        }

        private void UpdateValue()
        {
            _currentValue = _value;
            for (var i = 0; i < _bonuses.Count; ++i)
            {
                _currentValue = _bonuses[i].Calc(_currentValue);
            }
            if (ValueChanged != null) ValueChanged(_currentValue);
        }

        public void RemoveBonus(FloatBonusS bonus)
        {
            var removed = false;
            for (var i = 0; i < _bonuses.Count; ++i)
            {
                if (bonus == _bonuses[i])
                {
                    _bonuses.RemoveAt(i);
                    removed = true;
                    break;
                }
            }
            if (!removed)
            {
                Debug.LogWarningFormat("Trying to remove bonus which is not applied : {0}", bonus);
            }
            else
            {
                UpdateValue();
            }
        }

        public static implicit operator float(FloatAttributeS b)
        {
            return b.Value;
        }

        public event Action<float> ValueChanged;

        public override string ToString()
        {
            return Value.ToString(CultureInfo.CurrentCulture);
        }
    }
}