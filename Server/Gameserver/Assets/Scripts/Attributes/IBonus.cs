using System;

namespace Assets.Scripts.Attributes
{
    public interface IBonus<T>
        where T : struct 
    {
        int Priority { get; }
        T Calc(T value);
        void Aquire(IAttribute<T> attribute);
        void Dispose(IAttribute<T> attribute);
        T GetValue();
    }
}