using System;
using UnityEngine;

namespace Assets.Scripts.Attributes
{
    [Serializable]
    public class MinMaxFloatAttribute<T> : FloatAttribute<T> where T : MinMaxFloatAttribute<T>
    {
        [SerializeField] private float _min;
        [SerializeField] private float _max;
        [SerializeField] private bool _hasMinValue;
        [SerializeField] private bool _hasMaxValue;

        public MinMaxFloatAttribute() : this(0)
        {
        }

        public MinMaxFloatAttribute(float value, float min = float.NaN, float max = float.NaN) : base(value)
        {
            if (!float.IsNaN(min))
            {
                _min = min;
                _hasMinValue = true;
            }
            if (!float.IsNaN(max))
            {
                _max = max;
                _hasMaxValue = true;
            }
        }

        public override float Value
        {
            get
            {
                var value = base.Value;
                if (_hasMinValue && value < _min) return _min;
                if (_hasMaxValue && value > _max) return _max;
                return value;
            }
        }
    }

    [Serializable]
    public class MinMaxFloatAttribute : MinMaxFloatAttribute<MinMaxFloatAttribute>
    {

        public MinMaxFloatAttribute(float value, float min = float.NaN, float max = float.NaN) : base(value, min, max)
        {
            
        }
    }
}