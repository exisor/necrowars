﻿using System;
using UnityEngine;

namespace Assets.Scripts.Attributes
{
    [Serializable]
	public class SpawnUnitInfo
	{
		public long UnitId;

		public int MinCount;
		public int MinDelimiter;
		public int MaxCount;
		public int MaxDelimiter;

		public StraightLevelRelBonus LevelAttackBonus;

		private int _level;
		public int Level
        {
            get { return _level; }
            set
            {
                _level = value;
                LevelAttackBonus.Level = _level;
            }
        }
	}
}
