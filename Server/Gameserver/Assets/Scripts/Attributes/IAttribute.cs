using System;

namespace Assets.Scripts.Attributes
{
    public interface IAttribute<T>
        where T : struct
    {
        T Value { get; }
        event Action<T> ValueChanged;
        void BonusChanged(IBonus<T> bonus);
    }
}