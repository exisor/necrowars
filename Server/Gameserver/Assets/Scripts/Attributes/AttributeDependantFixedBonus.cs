﻿using System;

namespace Assets.Scripts.Attributes
{
    public class AttributeDependantFixedBonus<T> : AddedFloatBonus 
        where T : FloatAttribute<T>
    {
        private readonly float _rate;
        private readonly FloatAttribute<T> _attribute;
        public AttributeDependantFixedBonus(float value, FloatAttribute<T> attribute) : base(attribute * value)
        {
            _rate = value;
            _attribute = attribute;
            _attribute.ValueChanged += AttributeOnValueChanged;
        }

        private void AttributeOnValueChanged(float value)
        {
            Value = value*_rate;
        }

        protected override void Free()
        {
            _attribute.ValueChanged -= AttributeOnValueChanged;
        }
    }
}
