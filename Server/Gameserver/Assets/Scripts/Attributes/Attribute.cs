﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace Assets.Scripts.Attributes
{
    [Serializable]
    public class Attribute<T, TDerived> : IAttribute<T>
        where T : struct
        where TDerived : Attribute<T, TDerived>
    {
        private readonly List<IBonus<T>> _bonuses = new List<IBonus<T>>(5);
        [SerializeField] private T _currentValue;
        private bool _initialized;

        [SerializeField] protected T _value;

        protected Attribute()
        {
        }

        protected Attribute(T initialValue)
        {
            _value = initialValue;
        }

        public virtual T DefaultValue
        {
            get { return _value; }
        }

        public virtual T Value
        {
            get
            {
                if (!_initialized)
                    UpdateValue();
                return _currentValue;
            }
        }

        public event Action<T> ValueChanged;

        public void BonusChanged(IBonus<T> bonus)
        {
            BonusOnUpdated(bonus);
        }

        protected void AddBonus(IBonus<T> bonus)
        {
#if DEBUG
            if (_bonuses.Contains(bonus))
                Debug.LogWarningFormat("Bonus added twice! Are you sure this is intended?");
#endif
            // reducing memory allocations
            var index = 0;
            for (; index < _bonuses.Count; ++index)
                if (_bonuses[index].Priority > bonus.Priority)
                    break;
            _bonuses.Insert(index, bonus);
            bonus.Aquire(this);
            UpdateValue();
        }

        private void BonusOnUpdated(IBonus<T> bonus)
        {
            UpdateValue();
        }

        protected void RemoveBonus(IBonus<T> bonus)
        {
            _bonuses.Remove(bonus);
            bonus.Dispose(this);
            UpdateValue();
        }

        protected void UpdateValue()
        {
            _initialized = true;
            _currentValue = _value;

#if DEBUG
            if (!IsSorted(_bonuses)) throw new InvalidOperationException("Bonuses is not sorted");
#endif
            for (var i = 0; i < _bonuses.Count; ++i)
                _currentValue = _bonuses[i].Calc(_currentValue);
            OnValueChanged(_currentValue);
        }

#if DEBUG
        private static bool IsSorted(IList<IBonus<T>> bonuses)
        {
            for (var i = 0; i < bonuses.Count - 1; ++i)
                if (bonuses[i].Priority > bonuses[i + 1].Priority) return false;
            return true;
        }
#endif

        public void Reset()
        {
            for (var i = _bonuses.Count - 1; i >= 0; --i)
                RemoveBonus(_bonuses[i]);
        }

        protected virtual void OnValueChanged(T obj)
        {
            var handler = ValueChanged;
            if (handler != null) handler(obj);
        }

        public static implicit operator T(Attribute<T, TDerived> attr)
        {
            return attr.Value;
        }

        public static TDerived operator +(Attribute<T, TDerived> attribute, IBonus<T> bonus)
        {
            attribute.AddBonus(bonus);
            return (TDerived) attribute;
        }

        public static TDerived operator -(Attribute<T, TDerived> attribute, IBonus<T> bonus)
        {
            attribute.RemoveBonus(bonus);
            return (TDerived) attribute;
        }

        public override string ToString()
        {
            return Value.ToString();
        }
    }
}