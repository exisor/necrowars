﻿namespace Assets.Scripts.Attributes
{
    public interface IBonusStruct<T> : IPriorited<int>
        where T : struct
    {
        T Calc(T val);
    }
}
