using System;

namespace Assets.Scripts.Attributes
{
    [Serializable]
    public class MaxDamageAttribute : MinMaxFloatAttribute<MaxDamageAttribute>
    {
        private const float MINIMUM_DAMAGE = 20;
        public MaxDamageAttribute() : base(0, min: MINIMUM_DAMAGE) { }
    }
}