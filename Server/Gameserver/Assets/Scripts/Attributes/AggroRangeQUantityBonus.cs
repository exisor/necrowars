﻿using System;
using UnityEngine;

namespace Assets.Scripts.Attributes
{
    [Serializable]
    public class AggroRangeQuantityBonus : RelFloatBonus
    {
        private int _quantity;

        [SerializeField] private float _modifier;

        public AggroRangeQuantityBonus() : this(1.0f)
        { }

        public AggroRangeQuantityBonus(float modifier)
            : base(0.0f)
        {
            _modifier = modifier;
        }

        public int Quantity
        {
            get { return _quantity; }
            set
            {
                _quantity = value;
                Value = (float)Math.Pow(_quantity - 1, 0.3)*_modifier;
                OnUpdated();
            }
        }
    }
}
