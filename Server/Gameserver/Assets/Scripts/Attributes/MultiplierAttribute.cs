using System;

namespace Assets.Scripts.Attributes
{
    [Serializable]
    public class MultiplierAttribute : FloatAttribute<MultiplierAttribute>
    {
        public MultiplierAttribute() : base(1f) { }
    }
}