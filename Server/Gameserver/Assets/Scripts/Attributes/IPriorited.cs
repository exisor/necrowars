﻿using System;

namespace Assets.Scripts.Attributes
{
    public interface IPriorited<T> where T : IComparable
    {
        T Priority { get; }
    }
}