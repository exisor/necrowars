﻿using System;
using UnityEngine;

namespace Assets.Scripts.Attributes
{
    [Serializable]
    public struct FloatBonusS : IBonusStruct<float>
    {
        public enum Priorty : int
        {
            PRIORITY_FIXED = 0,
            PRIORITY_RELATIVE = 1,
            PRIORITY_ADDED_BONUS = 2
        }
        public FloatBonusS(float v, BonusType t, Priorty priority
#if DEBUG
            , string s
#endif
        )
        {
            value = v;
            type = t;
            _priority = priority;
#if DEBUG
            source = s;
#endif
        }


#if DEBUG
        public string source;
#endif

        public float value;
        public BonusType type;
        [SerializeField]
        private Priorty _priority;

        public int Priority
        {
            get { return (int)_priority; }
        }

        public float Calc(float val)
        {
            switch (type)
            {
                case BonusType.Constant:
                    return value;
                case BonusType.Additive:
                    return val + value;
                case BonusType.Multiplicative:
                    return val*(1 + value);
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }

        public static bool operator ==(FloatBonusS l, FloatBonusS r)
        {
            return l.value == r.value && l.Priority == r.Priority && l.type == r.type;
        }

        public static bool operator !=(FloatBonusS l, FloatBonusS r)
        {
            return !(l == r);
        }

        public override bool Equals(object obj)
        {
            return false;
        }

        public override int GetHashCode()
        {
            return 0;
        }

        public override string ToString()
        {
            return string.Format("value : {0}, type : {1}, priority : {2} ", value, type, _priority)
#if DEBUG
                   + source
#endif
                ;
        }

        public static FloatBonusS Fixed(float value
#if DEBUG
            ,string source
#endif
            )
        {
            return new FloatBonusS(value, BonusType.Additive, Priorty.PRIORITY_FIXED
#if DEBUG
                , source
#endif
            );
        }
        public static FloatBonusS Relative(float value
#if DEBUG
            , string source
#endif
            )
        {
            return new FloatBonusS(value, BonusType.Multiplicative, Priorty.PRIORITY_RELATIVE
#if DEBUG
                , source
#endif
            );
        }
        public static FloatBonusS AddedBonus(float value
#if DEBUG
            , string source
#endif
            )
        {
            return new FloatBonusS(value, BonusType.Additive, Priorty.PRIORITY_ADDED_BONUS
#if DEBUG
                , source
#endif
            );
        }

        public FloatBonusS WithLevel(int level)
        {
            return new FloatBonusS(value * level, type, _priority
#if DEBUG
                , source
#endif
                );
        }
    }
}