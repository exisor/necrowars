using System;

namespace Assets.Scripts.Attributes
{
    [Serializable]
    public class MinDamageAttribute : MinMaxFloatAttribute<MinDamageAttribute>
    {
        private const float MINIMUM_DAMAGE = 10;
        
        public MinDamageAttribute() : base(0, min: MINIMUM_DAMAGE) { }
    }
}