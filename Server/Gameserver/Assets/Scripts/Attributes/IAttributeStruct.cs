﻿namespace Assets.Scripts.Attributes
{
    public interface IAttributeStruct<out TVal, in TBonus> where TVal : struct where TBonus : struct, IBonusStruct<TVal>
    {
        TVal Value { get; }
        void AddBonus(TBonus bonus);
        void RemoveBonus(TBonus bonus);
    }
}