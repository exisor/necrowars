using System;
using System.Collections.Generic;
using UnityEngine;

namespace Assets.Scripts.Attributes
{
    public abstract class FloatBonus : IBonus<float>
    {
        private int _aquireCount;
        public abstract int Priority { get;}
        public abstract float Calc(float value);
        private List<IAttribute<float>> _attributes = new List<IAttribute<float>>(1);

        private void Aquire()
        {
            ++_aquireCount;
        }

        public void Aquire(IAttribute<float> attribute)
        {
            Aquire();
            _attributes.Add(attribute);
        }

        public void Dispose(IAttribute<float> attribute)
        {
            _attributes.Remove(attribute);
            Dispose();
        }

        protected void OnUpdated()
        {
            for (var i = 0; i < _attributes.Count; ++i)
            {
                _attributes[i].BonusChanged(this);
            }
        }
        public abstract float GetValue();

        private void Dispose()
        {
            --_aquireCount;
            if (_aquireCount == 0)
            {
                Free();
            }
            if (_aquireCount < 0)
            {
                Debug.LogWarningFormat("Disposed but not aquired!");
            }
        }

        protected virtual void Free() { }
    }
}