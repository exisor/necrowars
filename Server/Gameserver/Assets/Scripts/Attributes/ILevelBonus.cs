﻿namespace Assets.Scripts.Attributes
{
    public interface ILevelBonus
    {
        int Level { get; set; }
    }
}
