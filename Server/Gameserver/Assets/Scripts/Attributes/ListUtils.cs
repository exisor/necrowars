﻿using System;
using System.Collections.Generic;

namespace Assets.Scripts.Attributes
{
    public static class ListUtils
    {
        public static void AddWithPriority<TValue, TPriority>(this List<TValue> list, TValue value)
            where TPriority : IComparable
            where TValue : struct, IPriorited<TPriority>
        {
            var index = 0;
            for (; index < list.Count; ++index)
                if (list[index].Priority.CompareTo(value.Priority) > 0)
                    break;
            list.Insert(index, value);
        }

        public static void RemoveWithPriority<TValue, TPriority>(this List<TValue> list, TValue value)
            where TPriority : IComparable
            where TValue : struct, IPriorited<TPriority>
        {
            list.Remove(value);
        }
    }
}