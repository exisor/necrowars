﻿namespace Assets.Scripts.Attributes
{
    public class FixedFloatBonus : FloatBonus
    {
        protected float Value;
        public FixedFloatBonus(float value)
        {
            Value = value;
        }

        public override int Priority
        {
            get { return 1; }
        }

        public override float Calc(float value)
        {
            return value += Value;
        }

        public override float GetValue()
        {
            return Value;
        }
    }

    public class AddedFloatBonus : FixedFloatBonus
    {
        public AddedFloatBonus(float value) : base(value)
        {
        }

        public override int Priority
        {
            get { return 10000; }
        }
    }
}
