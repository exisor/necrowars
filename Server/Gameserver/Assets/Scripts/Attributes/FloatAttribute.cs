using System;

namespace Assets.Scripts.Attributes
{
    [Serializable]
    public class FloatAttribute<T> : Attribute<float, T> where T : FloatAttribute<T>
    {
        public FloatAttribute() : base() { }
        public FloatAttribute(float value) : base(value) { }

        public static T operator *(FloatAttribute<T> attribute, IAttribute<float> multiplier)
        {
#if DEBUG
            if (attribute == null) throw new ArgumentNullException("attribute");
            if (multiplier == null) throw new ArgumentNullException("multiplier");
#endif
            attribute += RelFloatBonus.FromMultiplier(multiplier.Value);
            return (T) attribute;
        }

        public static T operator +(FloatAttribute<T> attribute, float value)
        {
            attribute._value += value;
            attribute.UpdateValue();
            return (T) attribute;
        }
    }

    [Serializable]
    public class FloatAttribute : FloatAttribute<FloatAttribute>
    {
        public FloatAttribute() : base() { }
        public FloatAttribute(float value) : base(value) { }
    }
}