﻿using System;

namespace Assets.Scripts.Attributes
{
    [Serializable]
    public class ExpLevelRelBonus : StraightLevelRelBonus
    {
        public override int Level
        {
            get
            {
                return _level;
            }
            set
            {
                _level = value;
                Value = (float)Math.Pow(_level, _modifier);
            }
        }
    }
}
