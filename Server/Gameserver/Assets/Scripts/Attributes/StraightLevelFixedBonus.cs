﻿using System;
using UnityEngine;

namespace Assets.Scripts.Attributes
{
    [Serializable]
    public class StraightLevelFixedBonus : FixedFloatBonus, ILevelBonus
    {
        private int _level;
        [SerializeField]
        private float _modifier;

        public StraightLevelFixedBonus() : this(0.0f) { }

        public StraightLevelFixedBonus(float modifier) : base(0.0f)
        {
            _modifier = modifier;
        }

        public int Level
        {
            get { return _level; }
            set
            {
                _level = value;
                Value = _level * _modifier;
                OnUpdated();
            }
        }
    }
}