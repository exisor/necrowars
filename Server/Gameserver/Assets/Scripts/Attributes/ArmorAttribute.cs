using System;

namespace Assets.Scripts.Attributes
{
    [Serializable]
    public class ArmorAttribute : MinMaxFloatAttribute<ArmorAttribute>
    {
        private const float MINIMUM_ARMOR = 0;
        public ArmorAttribute() : base(0, min: MINIMUM_ARMOR) { }
    }
}