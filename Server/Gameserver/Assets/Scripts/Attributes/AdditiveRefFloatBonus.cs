using System;
using System.Collections.Generic;

namespace Assets.Scripts.Attributes
{
    [Obsolete("Not supported")]
    public class AdditiveRefFloatBonus : FloatBonus
    {
        private readonly List<RelFloatBonus> _relFloatBonuses = new List<RelFloatBonus>();
        private float _value = 0.0f;

        public AdditiveRefFloatBonus()
        {
            
        }
        public AdditiveRefFloatBonus(IEnumerable<RelFloatBonus> bonuses)
        {
            foreach (var relFloatBonus in bonuses)
            {
                Add(relFloatBonus);
            }
        }

        public override int Priority
        {
            get { return 100; }
        }

        public override float Calc(float value)
        {
            return value*(1.0f + _value);
        }

        public override float GetValue()
        {
            return _value;
        }

        public void Add(RelFloatBonus bonus)
        {
            _relFloatBonuses.Add(bonus);
            _value += bonus.Calc(1.0f) - 1.0f;
            OnUpdated();
        }

        public void Remove(RelFloatBonus bonus)
        {
            _relFloatBonuses.Remove(bonus);
            _value -= bonus.Calc(1.0f) - 1.0f;
            OnUpdated();
        }
    }
}