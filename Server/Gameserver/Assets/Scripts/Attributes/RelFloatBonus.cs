using System;
using UnityEngine;

namespace Assets.Scripts.Attributes
{
    [Serializable]
    public class RelFloatBonus : FloatBonus
    {
        public static RelFloatBonus FromMultiplier(float value)
        {
            return new RelFloatBonus(value - 1);
        }

        public static RelFloatBonus FromPercent(float value)
        {
            return new RelFloatBonus(value / 100f);
        }

        [SerializeField]
        protected float Value;

        public RelFloatBonus()
        {
            
        }
        public RelFloatBonus(float value)
        {
            Value = value;
        }
        public override int Priority
        {
            get { return 100; }
        }
        public override float Calc(float value)
        {
            return value*(1 + Math.Max(Value, -1));
        }

        public override float GetValue()
        {
            return Value;
        }
    }

    public class MutableRelFloatBonus : RelFloatBonus
    {
        public void SetValue(float value)
        {
            Value = value;
            OnUpdated();
        }
    }
}