﻿using System;
using UnityEngine;

namespace Assets.Scripts.Attributes
{
    [Serializable]
    public class StraightLevelRelBonus : RelFloatBonus, ILevelBonus
    {
        protected int _level;
        [SerializeField]
        protected float _modifier;

        public StraightLevelRelBonus() : this(0.0f) { }

        public StraightLevelRelBonus(float modifier) : base(0.0f)
        {
            _modifier = modifier;
        }

        public virtual int Level
        {
            get { return _level; }
            set
            {
                _level = value;
                Value = _level * _modifier;
                OnUpdated();
            }
        }
    }
}
