﻿namespace Assets.Scripts.Attributes
{
    public enum BonusType
    {
        Constant,
        Additive,
        Multiplicative,
    }
}