using System;

namespace Assets.Scripts.Attributes
{
    [Serializable]
    public class AttackSpeedAttribute : MinMaxFloatAttribute<AttackSpeedAttribute>
    {
        private const float MINIMUM_ATTACK_SPEED = 0.1f;

        public AttackSpeedAttribute() : this(0.0f) { }

        public AttackSpeedAttribute(float value) : base(value, min: MINIMUM_ATTACK_SPEED) { }
    }
}