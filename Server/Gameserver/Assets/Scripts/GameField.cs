﻿using UnityEngine;

namespace Assets.Scripts
{
    public class GameField : MonoBehaviour
    {
        public float Width;
        public float Height;
        private static GameField _instance;
        public static float FieldWidth { get { return _instance.Width; } }
        public static float FieldHeight { get { return _instance.Height; } }
        void Start()
        {
            _instance = this;
            const float thickness = 5;

            var top =
                gameObject.AddComponent<BoxCollider2D>();

            top.offset = new Vector2(0, (Height + thickness) / 2);
            top.size = new Vector2(Width * 2, thickness + 0.5f);

            var right =
                gameObject.AddComponent<BoxCollider2D>();

            right.offset = new Vector2((Width + thickness) / 2, 0);
            right.size = new Vector2(thickness + 0.5f, Height * 2);

            var bottom =
                gameObject.AddComponent<BoxCollider2D>();
            bottom.offset = new Vector2(0, - (Height + thickness) / 2);
            bottom.size = new Vector2(Width * 2, thickness + 0.5f);

            var left =
                gameObject.AddComponent<BoxCollider2D>();
            left.offset = new Vector2(-(Width + thickness) / 2, 0);
            left.size = new Vector2(thickness + 0.5f, Height * 2);

        }

        public Vector2 GetRandomPos(float offset = 0.2f)
        {
            Vector2 pos;
            var o = offset + 2;
            pos.x = Random.Range(-Width / o, Width / o);
            pos.y = Random.Range(-Height / o, Height / o);
            return pos;
        }
    }
}
