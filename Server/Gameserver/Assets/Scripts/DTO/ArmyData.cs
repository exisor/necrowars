﻿using System;
using System.Collections.Generic;

namespace Assets.Scripts.DTO
{
    [Serializable]
    public class ArmyData
    {
        public ArmyLeaderData ArmyLeader;
        public List<UnitData> Units;
    }
}
