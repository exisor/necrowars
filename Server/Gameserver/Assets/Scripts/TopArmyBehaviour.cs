﻿using System.Linq;
using JetBrains.Annotations;
using UnityEngine;
using UnityEngine.Networking;

namespace Assets.Scripts
{
    public class TopArmyBehaviour : MonoBehaviour
    {
        public static TopArmyBehaviour Instance;
        
        [SerializeField] private TopArmyMarkerBehaviour _topArmyMarkerPrefab;

        private TopArmyMarkerBehaviour _runtimeMarker;

        private Army _top;
        private float _topPoints;

        void Awake()
        {
            Instance = this;
            _runtimeMarker =
                ((GameObject) Instantiate(_topArmyMarkerPrefab.gameObject, transform)).GetComponent<TopArmyMarkerBehaviour>();
            NetworkServer.Spawn(_runtimeMarker.gameObject);
#if DEBUG
            var identity = _runtimeMarker.GetComponent<NetworkIdentity>();
            Debug.LogFormat("TopArmyMarkerBehaviour netId : {0}, assetId : {1}, isServer : {2}, enabled : {3}, active : {4}", identity.netId, identity.assetId, identity.isServer, _runtimeMarker.enabled, _runtimeMarker.gameObject.activeInHierarchy);
#endif
        }

        private void UpdateTop()
        {
            var newTop = ArmiesManager.Instance.Armies.Where(a => !a.Destroyed).OrderByDescending(a => a.Points).FirstOrDefault();
            if (newTop != _top)
            {
                SetTop(newTop);
            }
        }

        public void PointsUpdate(Army army)
        {
            if (_top == null) SetTop(army);
            if (army.Points > _topPoints)
            {
                SetTop(army);
            }
        }

        private void SetTop([CanBeNull]Army army)
        {
            if (_top == army) return;
            _top = army;
            if (_top == null) UpdateTop();
            if (army != null) _topPoints = army.Points;
            _runtimeMarker.HostArmy = army;

            if (_top == null || _top.ArmyOwner == ArmyType.Ai) return;
            _top.GetComponent<ArmyPlayerController>().Top();
        }

        public void NotifyDestroyed(Army army)
        {
            if (army != _top) return;

            UpdateTop();
        }
    }
}
