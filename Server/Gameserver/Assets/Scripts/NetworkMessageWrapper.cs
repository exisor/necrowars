﻿using ClientProtocol;
using Common;
using UnityEngine.Networking;

namespace Assets.Scripts
{
    public abstract class NetworkMessageWrapper<TMessage> : MessageBase
    {
        public TMessage Message { get; private set; }
        public CustomGameMessageType MessageType { get; private set; }

        public NetworkMessageWrapper(TMessage message, CustomGameMessageType messageType)
        {
            Message = message;
            MessageType = messageType;
        }

        public override void Serialize(NetworkWriter writer)
        {
            writer.StartMessage((short)MessageType);
            writer.WriteBytesFull(ProtobufSerializer.SerializeBinary(Message));
            writer.FinishMessage();
        }

        public override void Deserialize(NetworkReader reader)
        {
            var source = reader.ReadBytesAndSize();
            Message = ProtobufSerializer.Deserialize<TMessage>(source);
        }
    }
}