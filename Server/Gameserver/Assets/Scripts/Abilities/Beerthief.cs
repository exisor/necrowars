﻿#if ABILITY_ENABLED
using System;
using Assets.Scripts.Attributes;
using Assets.Scripts.Effects;

namespace Assets.Scripts.Abilities
{
    [Obsolete("Ability inactive at the moment")]
    public class Beerthief : SupportAbility<YouAreMineNow>
    {
        [Common.ClientValueTag]
        public StraightLevelRelBonus BeerBonus;
        protected override long MainAbilityId
        {
            get { return 30; }
        }

        protected override void OnEnabled()
        {
            BeerBonus.Level = Level;
            MainAbility.Buffed+= MainAbilityOnBuffed;
        }

        private void MainAbilityOnBuffed(YouAreMineNowEffect youAreMineNowEffect)
        {
        }

        protected override void OnDisabled()
        {
            MainAbility.Buffed -= MainAbilityOnBuffed;
        }
    }
}
#endif