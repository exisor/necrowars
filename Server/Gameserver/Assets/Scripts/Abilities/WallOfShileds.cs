#if ABILITY_ENABLED
using System;
using System.Linq;
using Assets.Scripts.Attributes;
using Assets.Scripts.Effects;
using UnityEngine;

namespace Assets.Scripts.Abilities
{
    public class WallOfShileds : ActiveAbility
    {
        [Common.ClientObjectTag]
        public ArmorEffect ArmorEffect;
        protected override Vector2 Use(Vector2 _)
        {
            foreach (var unit in Army.Units.Where(u => u.Alive))
            {
                var effect = Effect.Create(ArmorEffect);
                effect.Target = unit;
                OnUsed(unit);
            }
            return Army.Leader;
        }

        public event Action<Unit> Used;

        protected virtual void OnUsed(Unit obj)
        {
            var handler = Used;
            if (handler != null) handler(obj);
        }
    }
}
#endif