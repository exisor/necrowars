﻿#if ABILITY_ENABLED
using System;
using Assets.Scripts.Attributes;
using Assets.Scripts.Effects;

namespace Assets.Scripts.Abilities
{
    public class ExplosionsDamage : SupportAbility<Explosions>
    {
        [Common.ClientValueTag(IsPercent = true, LevelAddicted = true)]
        public StraightLevelRelBonus DamageBonus;

        protected override long MainAbilityId
        {
            get { return 11; }
        }

        protected override void OnEnabled()
        {
            DamageBonus.Level = Level;
            MainAbility.Buffed += MainAbilityOnBuffed;
#if EXTENDED_LOGGING
            Logging.Log(this, "Enabled. Level : {0}, DamageBonus : {1}", Level, DamageBonus.GetValue());
            DamageBonus.Updated += bonus => Logging.Log(this, "Damage bonus changed : {0}", bonus.GetValue());
#endif
        }

        private void MainAbilityOnBuffed(ExplosionsEffect explosionsEffect)
        {
            explosionsEffect.Disabled += ExplosionsEffectOnDisabled;
            explosionsEffect.Target.MinDamage += DamageBonus;
            explosionsEffect.Target.MaxDamage += DamageBonus;
#if EXTENDED_LOGGING
            Logging.Log(this, "Procced. DamageBonus : {0}. New Damage : {1}-{2}", DamageBonus.GetValue(), explosionsEffect.Target.MinDamage, explosionsEffect.Target.MaxDamage);
#endif
        }

        private void ExplosionsEffectOnDisabled(Effect effect)
        {
            var targetEffect = (TargetEffect) effect;
            targetEffect.Disabled -= ExplosionsEffectOnDisabled;
            targetEffect.Target.MinDamage -= DamageBonus;
            targetEffect.Target.MaxDamage -= DamageBonus;
#if EXTENDED_LOGGING
            Logging.Log(this, "Buff off. DamageBonus : {0}. New Damage : {1}-{2}", DamageBonus.GetValue(), targetEffect.Target.MinDamage, targetEffect.Target.MaxDamage);
#endif
        }

        protected override void OnDisabled()
        {
#if EXTENDED_LOGGING
            Logging.Log(this, "Disabled");
#endif
            MainAbility.Buffed -= MainAbilityOnBuffed;
        }
    }
}
#endif