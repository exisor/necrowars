﻿#if ABILITY_ENABLED

using System;
using Assets.Scripts.Attributes;
using Assets.Scripts.Effects;
using UnityEngine;
using UnityEngine.Networking;

namespace Assets.Scripts.Abilities
{
    public class Ragnarek : ActiveAbility, ISkillRadius, ISkillRange
    {
        [Common.ClientValueTag]
        public FloatAttribute Distance;
        [Common.ClientValueTag]
        public FloatAttribute Radius;

#pragma warning disable 414
        [SyncVar]
        private float _distance;
        [SyncVar]
        private float _radius;
#pragma warning restore 414
        [Common.ClientObjectTag]
        public RagnarekAOE RagnarekAOE;

        protected override void Start()
        {
            base.Start();
            _radius = Radius;
            Radius.ValueChanged += val => _radius = val;
            _distance = Distance;
            Distance.ValueChanged += val => _distance = val;
        }

        protected override Vector2 Use(Vector2 target)
        {
            var effect = Effect.Create(RagnarekAOE);
            effect.Radius += Radius;

            var actualTarget = target.ClampTarget(Army.Leader, Distance);
            effect.transform.localPosition = actualTarget;
            effect.Owner = Army;
            OnCast(effect);
            return actualTarget;

        }

        public event Action<RagnarekAOE> Cast;

        protected virtual void OnCast(RagnarekAOE obj)
        {
            var handler = Cast;
            if (handler != null) handler(obj);
        }

        public FloatAttribute SkillRadius { get { return Radius; }set { Radius = value; } }
        public float SkillRange { get { return Distance; } }
    }
}

#endif