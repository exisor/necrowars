﻿#if ABILITY_ENABLED
using System;
using Assets.Scripts.Attributes;
using Assets.Scripts.Effects;

namespace Assets.Scripts.Abilities
{
    public class Mushroomism : SupportAbility<Berserk>
    {
        [Common.ClientValueTag(IsPercent = true, LevelAddicted = true)]
        public StraightLevelFixedBonus AttackSpeedBonus;
        [Common.ClientObjectTag]
        public AttackSpeedEffect AttackSpeedEffect;
        protected override long MainAbilityId
        {
            get { return 12; }
        }

        protected override void OnEnabled()
        {
            AttackSpeedBonus.Level = Level;
            MainAbility.Buffed += MainAbilityOnBuffed;
        }

        private void MainAbilityOnBuffed(AttackEffect attackEffect)
        {
            if (attackEffect.Target == null) return;
            var effect = Effect.Create(AttackSpeedEffect);
            effect.Amount += AttackSpeedBonus;
            effect.Target = attackEffect.Target;
        }

        protected override void OnDisabled()
        {
            MainAbility.Buffed -= MainAbilityOnBuffed;
        }
    }
}
#endif