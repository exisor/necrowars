#if ABILITY_ENABLED
using System;
using Assets.Scripts.Attributes;
using Assets.Scripts.Items;

namespace Assets.Scripts.Abilities
{
    public class Axery : PassiveAbility
    {
        [Common.ClientValueTag(IsPercent = true, LevelAddicted = true)]
        public StraightLevelRelBonus VikingDamageBonus;

        private MinionDamageBonus _bonus;
        public override void Enable()
        {
            VikingDamageBonus.Level = Level;
#if EXTENDED_LOGGING
            Logging.Log(this, "Enabled. Level : {0}, Viking Damage Bonus : {1}", Level, VikingDamageBonus.GetValue());
            VikingDamageBonus.Updated +=
                bonus => Logging.Log(this, "Viking Damage Bonus changed : {0}", bonus.GetValue());
#endif
            _bonus = new MinionDamageBonus(VikingDamageBonus);
            Army.AddMinionBonus(_bonus);
        }

        public override void Disable()
        {
#if EXTENDED_LOGGING
            Logging.Log(this, "Disabled");
#endif
            Army.RemoveMinionBonus(_bonus);
        }
    }
}
#endif