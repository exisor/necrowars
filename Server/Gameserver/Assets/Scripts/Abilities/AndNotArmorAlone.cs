﻿#if ABILITY_ENABLED
using Assets.Scripts.Attributes;
using Assets.Scripts.Effects;

namespace Assets.Scripts.Abilities
{
    /// <summary>
    /// Spirit Weakness
    /// Ослабление духа
    /// </summary>
    public class AndNotArmorAlone : SupportAbility<ArmorOfShit>
    {
        [Common.ClientObjectTag]
        public AmplifyDamageEffect AmplifyDamageEffect;
        [Common.ClientValueTag(IsPercent = true)]
        public StraightLevelRelBonus MoreDamage;
        protected override long MainAbilityId
        {
            get { return 26; }
        }

        protected override void OnEnabled()
        {
            MoreDamage.Level = Level;
#if EXTENDED_LOGGING
            Logging.Log(this, "Enabled. Level : {0}, MainAbility : {1}, MoreDamage : {2}", Level, MainAbility.netId, MoreDamage.GetValue());
            MoreDamage.Updated += bonus => Logging.Log(this, "MoreDamage value changed : {0}", bonus.GetValue());
#endif
            MainAbility.Debuff += MainAbilityOnDebuff;
        }

        private void MainAbilityOnDebuff(ArmorEffect armorEffect)
        {
#if EXTENDED_LOGGING
            Logging.Log(this, "Procced");
#endif
            if (armorEffect.Target == null) return;
            var effect = Effect.Create(AmplifyDamageEffect);
            effect.AmplifyDamage += MoreDamage;
            effect.Target = armorEffect.Target;
#if EXTENDED_LOGGING
            Logging.Log(this, "Target : {0}, MoreDamage : {1}, AmplifyDamage : {2}", effect.Target.netId, MoreDamage, effect.AmplifyDamage);
#endif
        }

        protected override void OnDisabled()
        {
#if EXTENDED_LOGGING
            Logging.Log(this, "Disabled.");
#endif
            MainAbility.Debuff -= MainAbilityOnDebuff;
        }
    }
}
#endif