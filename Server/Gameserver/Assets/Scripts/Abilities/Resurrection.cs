#if ABILITY_ENABLED

using System.Linq;
using Assets.Scripts.Attributes;
using UnityEngine;
using UnityEngine.Networking;

namespace Assets.Scripts.Abilities
{
    

    public class Resurrection : ActiveAbility, ISkillRadius
    {
#pragma warning disable 414
        [SyncVar]
        private float _radius;
#pragma warning restore 414
        [Common.ClientValueTag]
        public FloatAttribute Radius;


        protected override void Start()
        {
            base.Start();

            _radius = Radius;
            Radius.ValueChanged += val => _radius = val;
        }
        private static readonly Collider2D[] TemporaryTargets = new Collider2D[500];
        protected override Vector2 Use(Vector2 _)
        {
            var targetsCount = Physics2D.OverlapCircleNonAlloc(Army.Leader.pos, Radius, TemporaryTargets, Constants.UNITS_LAYER_MASK);
            for (var i = 0; i < targetsCount; ++i)
            {
                var t = TemporaryTargets[i].GetComponent<Unit>();
                // capacity lock
                if (!t.Collected && !t.Alive && !t.IsLeader && (t.Army == null || t.Army.Destroyed) && Army.AliveUnits < Army.VikingsLimit)
                {
                    t.Army = Army;
                    t.Resurrect();
                    t.Heal();
                }
                TemporaryTargets[i] = null;
            }
            return Army.Leader;
        }

        public FloatAttribute SkillRadius
        {
            get { return Radius; } set { Radius = value; }
        }
    }
}

#endif