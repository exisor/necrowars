#if ABILITY_ENABLED
using System;
using Assets.Scripts.Attributes;
using Assets.Scripts.Effects;

namespace Assets.Scripts.Abilities
{
    public class ShieldAndBeer : SupportAbility<WallOfShileds>
    {
        [Common.ClientObjectTag]
        public HealEffect HealEffect;
        [Common.ClientValueTag(LevelAddicted = true)]
        public StraightLevelFixedBonus HealBonus;
        protected override long MainAbilityId
        {
            get { return 8; }
        }

        protected override void OnEnabled()
        {
            HealBonus.Level = Level;
            MainAbility.Used += MainAbilityOnUsed;
        }

        private void MainAbilityOnUsed(Unit unit)
        {
            var effect = Effect.Create(HealEffect);
            effect.RegenAmount = HealBonus;
            effect.Target = unit;
        }

        protected override void OnDisabled()
        {
            MainAbility.Used -= MainAbilityOnUsed;
        }
    }
}
#endif