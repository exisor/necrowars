﻿#if ABILITY_ENABLED
using System;
using Assets.Scripts.Effects;
using UnityEngine;

namespace Assets.Scripts.Abilities
{
    [Obsolete("Not used anymore")]
    public class YouAreMineNow : ActiveAbility
    {
        [Common.ClientObjectTag]
        public YouAreMineNowEffect BuffEffect;
        protected override Vector2 Use(Vector2 _)
        {
            var effect = Effect.Create(BuffEffect);
            effect.Target = Army.Leader;
            OnBuffed(effect);
            return Army.Leader;
        }

        public event Action<YouAreMineNowEffect> Buffed;

        protected virtual void OnBuffed(YouAreMineNowEffect obj)
        {
            var handler = Buffed;
            if (handler != null) handler(obj);
        }
    }
}
#endif