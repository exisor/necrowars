#if ABILITY_ENABLED

using Assets.Scripts.Attributes;
using Assets.Scripts.Items;

namespace Assets.Scripts.Abilities
{
    public class OvercomePain : PassiveAbility
    {
        // ID 3
        [Common.ClientValueTag(IsPercent = true, LevelAddicted = true)]
        public StraightLevelRelBonus VikingHPBonus;
        private MinionHPBonus _bonus;

        public override void Enable()
        {
            VikingHPBonus.Level = Level;
            _bonus = new MinionHPBonus(VikingHPBonus);
            Army.AddMinionBonus(_bonus);
        }

        public override void Disable()
        {
            Army.RemoveMinionBonus(_bonus);
        }
    }
}

#endif