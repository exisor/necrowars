﻿using Assets.Scripts.Attributes;

namespace Assets.Scripts.Abilities
{
    public interface ISkillRange
    {
        float SkillRange { get; }
    }
}
