#if ABILITY_ENABLED
using UnityEngine;
using UnityEngine.Networking;

namespace Assets.Scripts.Abilities
{
    public class HPPotion : AbilityWithCharges
    {

        protected override Vector2 Use(Vector2 target)
        {
            Army.Leader.Heal(Heal);
            return Army.Leader;
        }
        [SyncVar]
        public float Heal;
    }
}
#endif