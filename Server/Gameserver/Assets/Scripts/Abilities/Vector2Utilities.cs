﻿using UnityEngine;

namespace Assets.Scripts.Abilities
{
    public static class Vector2Utilities
    {
        public static Vector2 ClampTarget(this Vector2 target, Vector2 origin, float distance)
        {
            var direction = target - origin;
            if (direction.magnitude < distance) return target;
            direction.Normalize();
            direction *= distance;
            return origin + direction;
        }

        public static Vector2 Jitter(this Vector2 vector, float value = 1.0f)
        {
            return vector + new Vector2(Random.Range(-value, value), Random.Range(-value, value));
        }

        public static float SquareDistance(Vector2 lvalue, Vector2 rvalue)
        {
            return (lvalue - rvalue).sqrMagnitude;
        }
        
    }
}
