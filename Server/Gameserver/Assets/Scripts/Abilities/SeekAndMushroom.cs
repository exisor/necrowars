﻿#if ABILITY_ENABLED
using System;
using System.Linq;
using Assets.Scripts.Attributes;
using Assets.Scripts.Effects;
using UnityEngine;

namespace Assets.Scripts.Abilities
{
    public class SeekAndMushroom : SupportAbility<Charge>, ISkillRadius
    {
        [Common.ClientValueTag]
        public FloatAttribute Radius;
        [Common.ClientValueTag(IsPercent = true, LevelAddicted = true)]
        public StraightLevelRelBonus DamageBonus;

        public ChargeDamageEffect ChargeDamageGroundEffect;
        protected override long MainAbilityId
        {
            get { return 10; }
        }

        protected override void OnEnabled()
        {
            DamageBonus.Level = Level;
            MainAbility.LeaderCharged += MainAbilityOnLeaderCharged;
        }

        private void MainAbilityOnLeaderCharged(ChargeEffect chargeEffect)
        {
            chargeEffect.Disabled += ChargeEffectOnDisabled;
        }

        private void ChargeEffectOnDisabled(Effect effect)
        {
            var targetEffect = (TargetEffect) effect;

            targetEffect.Disabled -= ChargeEffectOnDisabled;

            var leader = (ArmyLeader)targetEffect.Target;

            var targets = Physics2D.OverlapCircleAll(leader, Radius,
                LayerMask.GetMask("units")).Select(t => t.GetComponent<Unit>()).Where(u => u.Army != leader.Army && u.Alive);
            var baseDamage = new FloatAttribute(leader.RollDamage());
            baseDamage += DamageBonus;
            foreach (var target in targets)
            {
                Damage.Spell(leader,target, baseDamage);
            }

            Effect.Create(ChargeDamageGroundEffect).transform.localPosition = leader;
        }

        protected override void OnDisabled()
        {
            MainAbility.LeaderCharged -= MainAbilityOnLeaderCharged;
        }

        public FloatAttribute SkillRadius
        {
            get { return Radius; } set { Radius = value; }
        }
    }
}
#endif