﻿#if ABILITY_ENABLED
using Assets.Scripts.Attributes;

namespace Assets.Scripts.Abilities
{
    public class DrakkarRadius : SupportAbility<Drakkar>
    {
        [Common.ClientValueTag(IsPercent = true, LevelAddicted = true)]
        public StraightLevelRelBonus RadiusBonus;
        protected override long MainAbilityId
        {
            get { return 13; }
        }

        protected override void OnEnabled()
        {
            RadiusBonus.Level = Level;
            MainAbility.Radius += RadiusBonus;
#if EXTENDED_LOGGING
            Logging.Log(this, "Enabled. Level : {0}, Radius : {1}", Level, RadiusBonus.GetValue());
            RadiusBonus.Updated += bonus => Logging.Log(this, "RadiusBonus changed : {0}", bonus.GetValue());
#endif
        }

        protected override void OnDisabled()
        {
#if EXTENDED_LOGGING
            Logging.Log(this, "Disabled");
#endif
            MainAbility.Radius -= RadiusBonus;
        }
    }
}
#endif