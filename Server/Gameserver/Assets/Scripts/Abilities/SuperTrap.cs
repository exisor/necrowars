﻿#if ABILITY_ENABLED
using System;
using Assets.Scripts.Attributes;
using Assets.Scripts.Effects;

namespace Assets.Scripts.Abilities
{
    public class SuperTrap : SupportAbility<Kamikaze>
    {
        [Common.ClientValueTag(IsPercent = true, LevelAddicted = true)]
        public StraightLevelRelBonus DamageBonus;
        protected override long MainAbilityId
        {
            get { return 47; }
        }

        protected override void OnEnabled()
        {
            MainAbility.Casted += MainAbilityOnCasted;
        }

        private void MainAbilityOnCasted(VikingTrapEffect vikingTrapEffect)
        {
            vikingTrapEffect.Multiplier += DamageBonus;
        }

        protected override void OnDisabled()
        {
            MainAbility.Casted -= MainAbilityOnCasted;
        }
    }
}
#endif