﻿#if ABILITY_ENABLED
using System;
using System.Collections.Generic;
using Assets.Scripts.Attributes;
using Assets.Scripts.Effects;
using UnityEngine;

namespace Assets.Scripts.Abilities
{
    public class SoakWall : SupportAbility<StoneWall>
    {
        public float Power;
        [Common.ClientValueTag(IsPercent = true, LevelAddicted = true)]
        public StraightLevelRelBonus DamageBonus;
        protected override long MainAbilityId
        {
            get { return 46; }
        }

        protected override void OnEnabled()
        {
            MainAbility.OnSpawned += MainAbilityOnOnSpawned; 
        }

        private void MainAbilityOnOnSpawned(StoneWallEffect stoneWallEffect)
        {
            stoneWallEffect.Multiplier += DamageBonus;
            stoneWallEffect.Tick += StoneWallEffectOnTick;
            stoneWallEffect.Disabled += StoneWallEffectOnDisabled;
        }

        private void StoneWallEffectOnTick(Effect wall, List<Unit> units)
        {
            foreach (var unit in units)
            {
                unit.GetComponent<Rigidbody2D>().AddForce(((Vector2)unit - (Vector2)wall.transform.localPosition).normalized * Power);
            }
        }

        private void StoneWallEffectOnDisabled(Effect effect)
        {
            ((StoneWallEffect) effect).Tick -= StoneWallEffectOnTick;
        }

        protected override void OnDisabled()
        {
            MainAbility.OnSpawned -= MainAbilityOnOnSpawned;
        }
    }
}
#endif