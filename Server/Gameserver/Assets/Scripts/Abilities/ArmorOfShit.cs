﻿#if ABILITY_ENABLED
using System;
using System.Linq;
using Assets.Scripts.Attributes;
using Assets.Scripts.Effects;
using UnityEngine;
using UnityEngine.Networking;

namespace Assets.Scripts.Abilities
{
    /// <summary>
    /// Curse of the Dead
    /// Проклятие мертвецов
    /// </summary>
    public class ArmorOfShit : ActiveAbility, ISkillRadius, ISkillRange
    {
        [Common.ClientValueTag(IsPercent = false)]
        public FloatAttribute Radius;
        [Common.ClientValueTag(IsPercent = false)]
        public FloatAttribute Distance;
#pragma warning disable 0414
        [SyncVar]
        private float _radius;
        [SyncVar]
        private float _distance;
#pragma warning restore 0414
        [Common.ClientObjectTag]
        public ArmorEffect ArmorDebuff;

        protected override void Start()
        {
            base.Start();

            _radius = Radius;
            Radius.ValueChanged += val => _radius = val;

            _distance = Distance;
            Distance.ValueChanged += val => _distance = val;
#if EXTENDED_LOGGING
            Logging.Log(this, "Radius : {0}, Distance : {1}", Radius, Distance);
            Radius.ValueChanged += val => Logging.Log(this, "Radius changed : {0}", val);
            Distance.ValueChanged += val => Logging.Log(this, "Distance changed {0}", val);
#endif
        }

        protected override Vector2 Use(Vector2 target)
        {
            var actualTarget = target.ClampTarget(Army.Leader, Distance);
#if EXTENDED_LOGGING
            Logging.Log(this, "Clamping target from {0} to {1}", target, actualTarget);
#endif
            var targets = Physics2D.OverlapCircleAll(actualTarget,
                Radius, LayerMask.GetMask("units")).Select(t => t.GetComponent<Unit>()).Where(u => u.Army != Army && u.Alive);
            foreach (var unit in targets)
            {
#if EXTENDED_LOGGING
                Logging.Log(this, "Hit target {0} in {1}", unit.netId, unit.transform.localPosition);
#endif
                var effect = Effect.Create(ArmorDebuff);
                effect.Target = unit;
                OnDebuff(effect);
            }
            return actualTarget;
        }

        public event Action<ArmorEffect> Debuff;

        protected virtual void OnDebuff(ArmorEffect obj)
        {
            var handler = Debuff;
            if (handler != null) handler(obj);
        }

        public FloatAttribute SkillRadius { get { return Radius; } set { Radius = value; } }

        #region Implementation of ISkillRange

        public float SkillRange { get { return Distance; }}

        #endregion
    }
}
#endif