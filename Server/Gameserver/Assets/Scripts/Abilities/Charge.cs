#if ABILITY_ENABLED
using System;
using System.Linq;
using Assets.Scripts.Attributes;
using Assets.Scripts.Effects;
using UnityEngine;
using UnityEngine.Networking;

namespace Assets.Scripts.Abilities
{
    public class Charge : ActiveAbility, ISkillRange
    {
        [Common.ClientValueTag]
        public FloatAttribute Distance;

        [SyncVar]
        private float _distance;

        [Common.ClientObjectTag]
        public ChargeEffect ChargeEffect;

        protected override void Start()
        {
            base.Start();

            _distance = Distance;
            Distance.ValueChanged += val => _distance = val;

#if EXTENDED_LOGGING
            Logging.Log(this, "Distance : {0}", Distance);
            Distance.ValueChanged += val => Logging.Log(this, "Distance value changed : {0}", val);
#endif
        }

        protected override Vector2 Use(Vector2 target)
        {
            var fixedTarget = target.ClampTarget(Army.Leader, Distance);

            foreach (var unit in Army.Units.Where(u => u.Alive))
            {
                var t = fixedTarget.Jitter();
                unit.SetMoveTarget(t);
                var effect = Effect.Create(ChargeEffect);
                effect.MoveTarget = t;
                effect.Target = unit;
                if (unit.IsLeader)
                {
                    OnLeaderCharged(effect);
                }
#if EXTENDED_LOGGING
                Logging.Log(this, "Used on target : {0} at {1}. MoveTarget : {2}", unit.netId, unit.transform.localPosition, t);
#endif
            }
            return fixedTarget;
        }

        public event Action<ChargeEffect> LeaderCharged;

        protected virtual void OnLeaderCharged(ChargeEffect obj)
        {
            var handler = LeaderCharged;
            if (handler != null) handler(obj);
        }

        #region Implementation of ISkillRange

        public float SkillRange { get { return _distance; } }

        #endregion
    }
}
#endif