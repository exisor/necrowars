﻿
#if ABILITY_ENABLED
using System;
using System.Linq;
using Assets.Scripts.Attributes;
using Assets.Scripts.Effects;
using UnityEngine;
using UnityEngine.Networking;

namespace Assets.Scripts.Abilities
{
    [Obsolete("Skill is not usable")]
    public class Katzenjammer : ActiveAbility, ISkillRadius
    {
        [Common.ClientValueTag]
        public FloatAttribute Radius;

        [Common.ClientValueTag]
        public FloatAttribute Distance;

#pragma warning disable 414
        [SyncVar]
        private float _radius;
        [SyncVar]
        private float _distance;
#pragma warning restore 414

        [Common.ClientObjectTag]
        public AttackEffect AttackDebuff;

        protected override void Start()
        {
            base.Start();

            _radius = Radius;
            Radius.ValueChanged += val => _radius = val;

            _distance = Distance;
            Distance.ValueChanged += val => _distance = val;
        }

        protected override Vector2 Use(Vector2 target)
        {
            var actualTarget = target.ClampTarget(Army.Leader, Distance);
            var targets = Physics2D.OverlapCircleAll(actualTarget,
                Radius, LayerMask.GetMask("units"))
                .Select(t => t.GetComponent<Unit>())
                .Where(u => u.Army != Army && u.Alive);
            foreach (var unit in targets)
            {
                var effect = Effect.Create(AttackDebuff);
                effect.Target = unit;
                OnDebuff(AttackDebuff);
            }
            return actualTarget;
        }

        public event Action<AttackEffect> Debuff;

        protected virtual void OnDebuff(AttackEffect obj)
        {
            var handler = Debuff;
            if (handler != null) handler(obj);
        }

        public FloatAttribute SkillRadius { get {return Radius;} set { Radius = value; } }
    }
}


#endif