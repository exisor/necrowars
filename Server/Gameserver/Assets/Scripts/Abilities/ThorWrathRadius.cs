#if ABILITY_ENABLED
using Assets.Scripts.Attributes;

namespace Assets.Scripts.Abilities
{
    public class ThorWrathRadius : SupportAbility<ThorWrath>
    {
        [Common.ClientValueTag(IsPercent = true, LevelAddicted = true)]
        public StraightLevelRelBonus RadiusBonus;
        protected override long MainAbilityId
        {
            get { return 9; }
        }

        protected override void OnEnabled()
        {
            RadiusBonus.Level = Level;
            MainAbility.Radius += RadiusBonus;
        }

        protected override void OnDisabled()
        {
            MainAbility.Radius -= RadiusBonus;
        }
    }
}
#endif