#if ABILITY_ENABLED
using Assets.Scripts.Attributes;
using UnityEngine;
using UnityEngine.Networking;

namespace Assets.Scripts.Abilities
{
    public abstract class ActiveAbility : Ability
    {
        public delegate void EventUsedDelegate(Vector2 pos);
        protected float CastTimer;
        [SyncVar]
        protected bool IsCasting;
        [Common.ClientValueTag(LevelAddicted = false)]
        public FloatAttribute Cooldown;
        [Common.ClientValueTag(LevelAddicted = false)]
        public FloatAttribute CastTime;

        private NetworkConnection _playerConnection;
#pragma warning disable 649, 414
        [SyncVar]
        private float _cooldown;
        [SyncVar][SerializeField]
        private int _slot = -1;
#pragma warning restore 649, 414
        private float _cooldownTimer;
        public float CooldownTimer
        {
            get { return _cooldownTimer; }
            set
            {
                _cooldownTimer = value;
                if (_playerConnection != null)
                {
                    TargetCooldownChanged(_playerConnection, _cooldownTimer);
                }
            }
        }

        protected virtual void Start()
        {
            _cooldown = Cooldown;
            Cooldown.ValueChanged += val => _cooldown = val;

            if (localPlayerAuthority)
            {
                var identity = GetComponent<NetworkIdentity>();
                _playerConnection = identity.clientAuthorityOwner;
            }

#if EXTENDED_LOGGING
            Logging.Log(this, "Cooldown : {0}, CastTime : {1}", Cooldown, CastTime);
            Cooldown.ValueChanged += val => Logging.Log(this, "Cooldown changed : {0}", val);

#endif
        }

#if FIXED_UPDATE
        void FixedUpdate()
#else
        public void ManagedUpdate()
#endif
        {
            if (Army.Destroyed)
            {
                enabled = false;
                return;
            }
            UpdateCooldowns();
            if (IsCasting && CastTimer <= 0.0f)
            {
                Cast();
            }
        }

        protected virtual void UpdateCooldowns()
        {
            if (IsCasting)
            {
                // update cast timer
                CastTimer -= Time.deltaTime;
            }
            if (OnCooldown)
            {
                CooldownTimer -= Time.deltaTime;
            }
        }

        public virtual bool CanCast()
        {
            if (IsCasting || OnCooldown) return false;
            return true;
        }

        public virtual void BeginCast()
        {
            if (!CanCast()) return;
            IsCasting = true;
            CastTimer = CastTime;
            OnBeginCasting();
#if EXTENDED_LOGGING
            Logging.Log(this, "Begin casting from position :{0}", Army.Leader.transform.localPosition);
#endif
        }

        protected virtual void OnBeginCasting()
        {
        }

        private void Cast()
        {
            IsCasting = false;
            CastTimer = 0.0f;
            CooldownTimer = Cooldown;
            var actualTarget = Use(_currentTarget);
            EventUsed(actualTarget);
#if EXTENDED_LOGGING
            Logging.Log(this, "Casted in target : [{0}] from [{1}]", actualTarget, Army.Leader.transform.localPosition);
#endif
        }

        protected abstract Vector2 Use(Vector2 target);

        protected bool OnCooldown
        {
            get { return CooldownTimer > 0.0f; }
        }
        [SyncEvent]
        public event EventUsedDelegate EventUsed;
        private Vector2 _currentTarget;

        [Command(channel = 2)]
        public void CmdCast(Vector2 target)
        {
            CastToPoint(target);
        }

        public void CastToPoint(Vector2 target)
        {
            _currentTarget = target;
#if EXTENDED_LOGGING
            Logging.Log(this, "Casting to point : [{0}]", target);
#endif
            BeginCast();
        }

        public void SetSlotNumber(int value)
        {
            _slot = value;
#if EXTENDED_LOGGING
            Logging.Log(this, "Slot number changed: [{0}]", value);
#endif
        }

        [TargetRpc(channel = 2)]
        private void TargetCooldownChanged(NetworkConnection conn, float cooldown)
        {
            
        }
    }
}
#endif