#if ABILITY_ENABLED
using Assets.Scripts.Attributes;
using Assets.Scripts.Items;
using UnityEngine.UI;

namespace Assets.Scripts.Abilities
{
    // ID 4
    public class Drunkenrage : PassiveAbility
    {
        [Common.ClientValueTag(IsPercent = true, LevelAddicted = true)]
        public StraightLevelRelBonus VikingAttackSpeedBonus;
        private MinionAttackSpeedBonus _bonus;
        public override void Enable()
        {
            VikingAttackSpeedBonus.Level = Level;
                 
            _bonus = new MinionAttackSpeedBonus(VikingAttackSpeedBonus);
#if EXTENDED_LOGGING
            Logging.Log(this, "Enabled, Level : {0}, AttackSpeedBonus : {1}", Level, VikingAttackSpeedBonus.GetValue());
#endif
            Army.AddMinionBonus(_bonus);
            
        }

        public override void Disable()
        {
#if EXTENDED_LOGGING
            Logging.Log(this, "Disabled");
#endif
            Army.RemoveMinionBonus(_bonus);
        }
    }
}
#endif