﻿#if ABILITY_ENABLED
using System;
using Assets.Scripts.Attributes;
using Assets.Scripts.Effects;

namespace Assets.Scripts.Abilities
{
    public class FullPants : SupportAbility<ThorWrath>
    {
        [Common.ClientObjectTag]
        public SlowEffect SlowEffect;
        [Common.ClientValueTag(IsPercent = false, LevelAddicted = true)]
        public StraightLevelFixedBonus SlowBonus;
        protected override long MainAbilityId
        {
            get { return 9; }
        }

        protected override void OnEnabled()
        {
            SlowBonus.Level = Level;
            MainAbility.TargetStunned += TargetStunned;
        }

        private void TargetStunned(StunEffect stunEffect)
        {
            stunEffect.Disabled += StunEffectOnDisabled;
        }

        private void StunEffectOnDisabled(Effect stunTargetEffect)
        {
            stunTargetEffect.Disabled -= StunEffectOnDisabled;
            var target = ((TargetEffect)stunTargetEffect).Target;

            if (!target.Alive) return;
            var effect = Effect.Create(SlowEffect);
            effect.Duration += SlowBonus;
            effect.Target = target;
        }

        protected override void OnDisabled()
        {
            MainAbility.TargetStunned -= TargetStunned;
        }
    }
}
#endif