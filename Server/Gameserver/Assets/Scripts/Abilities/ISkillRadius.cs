﻿using Assets.Scripts.Attributes;

namespace Assets.Scripts.Abilities
{
    public interface ISkillRadius
    {
        FloatAttribute SkillRadius { get; set; }
    }
}
