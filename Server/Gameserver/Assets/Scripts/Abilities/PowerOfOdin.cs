#if ABILITY_ENABLED

using Assets.Scripts.Attributes;

namespace Assets.Scripts.Abilities
{
    // id : 2
    public class PowerOfOdin : PassiveAbility
    {
        [Common.ClientValueTag(IsPercent = true, LevelAddicted = true)]
        public StraightLevelRelBonus HPBonus;
        [Common.ClientValueTag(IsPercent = true, LevelAddicted = true)]
        public StraightLevelRelBonus ArmorBonus;

        public override void Enable()
        {
            HPBonus.Level = Level;
            ArmorBonus.Level = Level;
            Army.Leader.MaxHP += HPBonus;
            Army.Leader.Armor += ArmorBonus;
        }

        public override void Disable()
        {
            if (Army.Leader == null) return;
            Army.Leader.MaxHP -= HPBonus;
            Army.Leader.Armor -= ArmorBonus;
        }
    }
}

#endif