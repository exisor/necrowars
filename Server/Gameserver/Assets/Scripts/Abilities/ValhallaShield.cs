﻿#if ABILITY_ENABLED
using Assets.Scripts.Attributes;
using Assets.Scripts.Effects;

namespace Assets.Scripts.Abilities
{
    public class ValhallaShield : SupportAbility<Valhalla>
    {
        [Common.ClientValueTag(LevelAddicted = true)]
        public StraightLevelFixedBonus HPBonus;

        [Common.ClientObjectTag]
        public ValhallaShieldEffect ValhallaShieldEffect;

        protected override long MainAbilityId
        {
            get { return 29; }
        }

        protected override void OnEnabled()
        {
            HPBonus.Level = Level;
            MainAbility.Buff += MainAbilityOnBuff;
        }

        private void MainAbilityOnBuff(ValhallaEffect valhallaEffect)
        {
            if (valhallaEffect.Target == null) return;
            var effect = Effect.Create(ValhallaShieldEffect);
            effect.HP += HPBonus;
            effect.Target = valhallaEffect.Target;
        }

        protected override void OnDisabled()
        {
            MainAbility.Buff -= MainAbilityOnBuff;
        }
    }
}
#endif