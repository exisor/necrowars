﻿using Assets.Scripts.Attributes;

#if ABILITY_ENABLED


namespace Assets.Scripts.Abilities
{

    public class LifeGivingViking : SupportAbility<GetInHere>
    {
        [Common.ClientValueTag(IsPercent = true, LevelAddicted = true)]
        public StraightLevelRelBonus HPPercent;
        #region Overrides of SupportAbility<GetInHere>

        protected override long MainAbilityId
        {
            get { return 27; }
        }

        protected override void OnEnabled()
        {
            HPPercent.Level = Level;
            MainAbility.Summoned += MainAbilityOnSummoned;
        }

        private void MainAbilityOnSummoned(Unit unit)
        {
            Army.Leader.Heal(unit.MaxHP * HPPercent.GetValue());
        }

        protected override void OnDisabled()
        {
            MainAbility.Summoned -= MainAbilityOnSummoned;
        }

        #endregion
    }
}
#endif