﻿#if ABILITY_ENABLED
using Assets.Scripts.Attributes;
using Assets.Scripts.Effects;

namespace Assets.Scripts.Abilities
{
    public class BiteTheShield : SupportAbility<Berserk>
    {
        [Common.ClientValueTag(IsPercent = true, LevelAddicted = true)]
        public StraightLevelRelBonus DamageBonus;
        protected override long MainAbilityId
        {
            get { return 12; }
        }

        protected override void OnEnabled()
        {
            DamageBonus.Level = Level;
            MainAbility.Buffed += MainAbilityOnBuffed;
#if EXTENDED_LOGGING
            Logging.Log(this, "Enabled. Level : {0}, DamageBonus : {1}", Level, DamageBonus.GetValue());
            DamageBonus.Updated += bonus => Logging.Log(this, "Damage bonus changed : {0}", bonus.GetValue());
#endif
        }

        private void MainAbilityOnBuffed(AttackEffect attackEffect)
        {
#if EXTENDED_LOGGING
            Logging.Log(this, "Procced");
#endif
            if (attackEffect.Target == null) return;
            attackEffect.Disabled +=AttackEffectOnDisabled;

            attackEffect.Target.MinDamage += DamageBonus;
            attackEffect.Target.MaxDamage += DamageBonus;

#if EXTENDED_LOGGING
            Logging.Log(this, "DamageBonus : {0}, Damage : {1} - {2}", DamageBonus.GetValue(), attackEffect.Target.MinDamage, attackEffect.Target.MaxDamage);
#endif
        }

        private void AttackEffectOnDisabled(Effect effect)
        {
            var targetEffect = (TargetEffect) effect;

            targetEffect.Disabled -= AttackEffectOnDisabled;

            targetEffect.Target.MinDamage -= DamageBonus;
            targetEffect.Target.MaxDamage -= DamageBonus;
#if EXTENDED_LOGGING
            Logging.Log(this, "Buff off. DamageBonus : {0}, Damage : {1} - {2}", DamageBonus.GetValue(), targetEffect.Target.MinDamage, targetEffect.Target.MaxDamage);
#endif
        }

        protected override void OnDisabled()
        {
#if EXTENDED_LOGGING
            Logging.Log(this, "Disabled");
#endif
            MainAbility.Buffed -= MainAbilityOnBuffed;
        }
    }
}
#endif