#if ABILITY_ENABLED
using System;
using System.Linq;
using UnityEngine;
using UnityEngine.Networking;

namespace Assets.Scripts.Abilities
{
    public abstract class AbilityWithCharges : ActiveAbility
    {
        [SyncVar]
        private int _charges;
        public int Charges { get {return _charges;} set { _charges = value; } }
        [SerializeField]
        private int _itemBaseId;

        private ArmyPlayerController _controller;

        protected override void Start()
        {
            base.Start();
            _controller = Army.GetComponent<ArmyPlayerController>();
            if (_controller == null)
            {
                Army.RemoveAbility(this);
                throw new InvalidOperationException("Bots cannot use consumables");
            }
            var item = _controller.CharacterData.Items.First(i => i.BaseId == _itemBaseId);
            Charges = item.Quantity;

#if EXTENDED_LOGGING
            Logging.Log(this, "ItemBaseId : {0}, Charges : {1}", _itemBaseId, Charges);
#endif
        }

        public override bool CanCast()
        {
            if (Charges <= 0) return false;
            return base.CanCast();
        }

        protected override void OnBeginCasting()
        {
            --Charges;
#if EXTENDED_LOGGING
            Logging.Log(this, "Charge consumed. Charges left : {0}", Charges);
#endif
            _controller.ConsumeItem(_itemBaseId);
        }
    }
}
#endif