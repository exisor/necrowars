#if ABILITY_ENABLED
using Assets.Scripts.Attributes;

namespace Assets.Scripts.Abilities
{
    public class LockiCunning : PassiveAbility
    {
        [Common.ClientValueTag(IsPercent = true, LevelAddicted = true)]
        public StraightLevelRelBonus AttackSpeedBonus;
        public override void Enable()
        {
            AttackSpeedBonus.Level = Level;
            Army.Leader.AttackSpeed += AttackSpeedBonus;
        }

        public override void Disable()
        {
            if (Army.Leader == null) return;
            Army.Leader.AttackSpeed += AttackSpeedBonus;
        }
    }
}

#endif