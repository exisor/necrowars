﻿
#if ABILITY_ENABLED
using UnityEngine.Networking;

namespace Assets.Scripts.Abilities
{
    public abstract class Ability :
        NetworkBehaviour
    {
        [SyncVar]
        public long Id;

        private Army _army;

        public Army Army
        {
            get { return _army; }
            set
            {
                _army = value;
#if EXTENDED_LOGGING
                if (value != null)
                Logging.Log(this, "Ability id:{0} created for Army : [{1}]", Id, _army.netId);
#endif
            }
        }
    }
}
#endif