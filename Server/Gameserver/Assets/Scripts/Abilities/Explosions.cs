﻿#if ABILITY_ENABLED
using System;
using Assets.Scripts.Attributes;
using Assets.Scripts.Effects;
using UnityEngine;

namespace Assets.Scripts.Abilities
{
    public class Explosions : ActiveAbility, ISkillRadius
    {
        [Common.ClientObjectTag]
        public ExplosionsEffect ExplosionsEffect;
        [Common.ClientValueTag]
        public FloatAttribute Radius;

        protected override Vector2 Use(Vector2 _)
        {
            var effect = Effect.Create(ExplosionsEffect);
            effect.Radius += Radius;
            effect.Target = Army.Leader;
#if EXTENDED_LOGGING
            Logging.Log(this, "Explosions used. Radius : {0}. Effect raidus : {1}. Target : {2} at {3}", Radius, effect.Radius, effect.Target.netId, effect.Target.transform.localPosition);
#endif
            if (effect.Target != null)
            {
                OnBuffed(effect);
            }
            return Army.Leader;
        }

        public event Action<ExplosionsEffect> Buffed;

        protected virtual void OnBuffed(ExplosionsEffect obj)
        {
            var handler = Buffed;
            if (handler != null) handler(obj);
        }

        public FloatAttribute SkillRadius { get {return Radius; } set { Radius = value; } }
    }
}

#endif