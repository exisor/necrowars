﻿#if ABILITY_ENABLED
using System;
using System.Linq;
using Assets.Scripts.Effects;
using UnityEngine;
using Random = UnityEngine.Random;

namespace Assets.Scripts.Abilities
{
    public class Kamikaze : ActiveAbility
    {
        [Common.ClientObjectTag]
        public VikingTrapEffect Trap;
        protected override Vector2 Use(Vector2 target)
        {
            var targetUnit = Army.Units.Where(u => !u.IsLeader).OrderBy(_ => Random.Range(0f, 1f)).FirstOrDefault();
            if (targetUnit == null) return Army.Leader;

            var effect = Effect.Create(Trap);
            effect.Target = targetUnit;
            if (Casted != null)
            {
                Casted(effect);
            }
            return targetUnit;
        }

        public event Action<VikingTrapEffect> Casted;
    }
}

#endif