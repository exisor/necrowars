﻿#if ABILITY_ENABLED
using System;
using System.Collections.Generic;
using System.Linq;
using Assets.Scripts.Attributes;
using Assets.Scripts.Effects;

namespace Assets.Scripts.Abilities
{
    public class Tinnitus : SupportAbility<Explosions>
    {
        [Common.ClientValueTag(IsPercent = true, LevelAddicted = true)]
        public FloatAttribute StunChance;
        [Common.ClientValueTag(IsPercent = true)]
        public StraightLevelFixedBonus StunBonus;
        [Common.ClientObjectTag]
        public StunEffect StunEffect;
        protected override long MainAbilityId
        {
            get { return 11; }
        }

        protected override void OnEnabled()
        {
            StunBonus.Level = Level;
            StunChance += StunBonus;
            MainAbility.Buffed += MainAbilityOnBuffed;
        }

        private void MainAbilityOnBuffed(ExplosionsEffect explosionsEffect)
        {
            explosionsEffect.Disabled += ExplosionsEffectOnDisabled;

            explosionsEffect.Splash += ExplosionsEffectOnSplash;
        }

        private void ExplosionsEffectOnSplash(List<Unit> units)
        {
            foreach (var unit in units)
            {
                if (unit == null) continue;
                if (!Proc()) continue;
                var effect = Effect.Create(StunEffect);
                effect.Target = unit;
            }
        }

        private void ExplosionsEffectOnDisabled(Effect targetEffect)
        {
            targetEffect.Disabled -= ExplosionsEffectOnDisabled;

            ((ExplosionsEffect) targetEffect).Splash -= ExplosionsEffectOnSplash;
        }

        protected override void OnDisabled()
        {
            MainAbility.Buffed -= MainAbilityOnBuffed;
        }

        private bool Proc()
        {
            return StunChance > UnityEngine.Random.Range(0.0f, 1.0f);
        }
    }
}
#endif