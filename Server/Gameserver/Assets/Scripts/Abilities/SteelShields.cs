﻿#if ABILITY_ENABLED
using System;
using Assets.Scripts.Attributes;

namespace Assets.Scripts.Abilities
{
    public class SteelShields : SupportAbility<WallOfShileds>
    {
        [Common.ClientValueTag(LevelAddicted = true, IsPercent = true)]
        public StraightLevelRelBonus ArmorBonus;
        protected override void OnEnabled()
        {
            ArmorBonus.Level = Level;
            MainAbility.ArmorEffect.Amount += ArmorBonus;
        }

        protected override void OnDisabled()
        {
            MainAbility.ArmorEffect.Amount -= ArmorBonus;
        }

        protected override long MainAbilityId
        {
            get { return 8; }
        }
    }
}
#endif