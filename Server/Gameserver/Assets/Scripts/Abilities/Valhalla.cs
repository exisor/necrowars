﻿#if ABILITY_ENABLED


using System;
using System.Linq;
using Assets.Scripts.Effects;
using UnityEngine;

namespace Assets.Scripts.Abilities
{
    public class Valhalla : ActiveAbility
    {
        [Common.ClientObjectTag]
        public ValhallaEffect ValhallaEffect;
        protected override Vector2 Use(Vector2 _)
        {
            foreach (var unit in Army.Units.Where(u => u.Alive))
            {
                var effect = Effect.Create(ValhallaEffect);
                effect.Target = unit;
                OnBuff(effect);
            }
            return Army.Leader;
        }

        public event Action<ValhallaEffect> Buff;

        protected virtual void OnBuff(ValhallaEffect obj)
        {
            var handler = Buff;
            if (handler != null) handler(obj);
        }
    }
}
#endif