#if ABILITY_ENABLED
using System;
using System.Linq;

namespace Assets.Scripts.Abilities
{
    public abstract class SupportAbility<T> : PassiveAbility
        where T : ActiveAbility
    {
        [NonSerialized]
        protected T MainAbility;
        public override void Enable()
        {
            MainAbility = Army.ActiveAbilities.FirstOrDefault(a => a.Id == MainAbilityId) as T;
            if (MainAbility == null)
            {
                Army.RemoveAbility(this);
                return;
            }
            OnEnabled();
        }

        public override void Disable()
        {
            if (MainAbility == null) return;
            OnDisabled();
        }

        protected abstract long MainAbilityId { get; }
        protected abstract void OnEnabled();
        protected abstract void OnDisabled();
    }
}
#endif