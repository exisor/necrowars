﻿#if ABILITY_ENABLED
using System.Linq;
using Assets.Scripts.Attributes;
using Assets.Scripts.Effects;
using UnityEngine;

namespace Assets.Scripts.Abilities
{
    public class RootingCharge : SupportAbility<Charge>, ISkillRadius
    {
        [Common.ClientValueTag]
        public FloatAttribute Radius;
        [Common.ClientValueTag]
        public StraightLevelFixedBonus DurationBonus;
        [Common.ClientObjectTag]
        public RootEffect RootEffect;

        protected override long MainAbilityId
        {
            get { return 10; }
        }

        protected override void OnEnabled()
        {
            DurationBonus.Level = Level;
            MainAbility.LeaderCharged += MainAbilityOnLeaderCharged;
        }

        private void MainAbilityOnLeaderCharged(ChargeEffect chargeEffect)
        {
            chargeEffect.Disabled += ChargeEffectOnDisabled;
        }

        private void ChargeEffectOnDisabled(Effect effect)
        {
            var targetEffect = (TargetEffect) effect;
            targetEffect.Disabled -= ChargeEffectOnDisabled;

            var targets = Physics2D.OverlapCircleAll(targetEffect.Target.transform.localPosition, Radius,
                LayerMask.GetMask("units")).Select(t => t.GetComponent<Unit>());
            foreach (var unit in targets.Where(t => t.Army != targetEffect.Target.Army && t.Alive))
            {
                var rootEffect = Effect.Create(RootEffect);
                rootEffect.Duration += DurationBonus;
                rootEffect.Target = unit;
            }
        }

        protected override void OnDisabled()
        {
            MainAbility.LeaderCharged -= MainAbilityOnLeaderCharged;
        }

        public FloatAttribute SkillRadius
        {
            get { return Radius; } set { Radius = value; }
        }
    }
}
#endif