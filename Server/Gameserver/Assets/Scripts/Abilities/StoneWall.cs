#if ABILITY_ENABLED

using System;
using Assets.Scripts.Attributes;
using Assets.Scripts.Effects;
using UnityEngine;
using UnityEngine.Networking;

namespace Assets.Scripts.Abilities
{
    public class StoneWall : ActiveAbility, ISkillRange
    {
        [Common.ClientObjectTag]
        public StoneWallEffect WallEffect;
        [Common.ClientValueTag]
        public FloatAttribute Distance;

        [SyncVar]
        private float _distance;

        protected override void Start()
        {
            base.Start();
            _distance = Distance;
            Distance.ValueChanged += val => _distance = val;
        }

        protected override Vector2 Use(Vector2 target)
        {
            var point = target.ClampTarget(Army.Leader, Distance);
            var effect = Effect.Create(WallEffect);
            
            effect.Owner = Army;
            effect.transform.localPosition = point;
            // no rotation of wall
            // var angle = Vector2.Angle(Vector2.up, point - Army.Leader) * (point.x < Army.Leader.transform.position.x ? -1 : 1);
            // effect.transform.Rotate(Vector3.back, angle);
            if (OnSpawned != null)
            {
                OnSpawned(effect);
            }
            return point;

        }

        public event Action<StoneWallEffect> OnSpawned;

        #region Implementation of ISkillRange

        public float SkillRange { get { return _distance; } }

        #endregion
    }
}

#endif