#if ABILITY_ENABLED
using System;
using Assets.Scripts.DTO;
using UnityEngine;
using UnityEngine.Networking;
using Random = UnityEngine.Random;

namespace Assets.Scripts.Abilities
{
    public class GetInHere : ActiveAbility
    {
        public delegate void EventSummonedDelegate(NetworkInstanceId networkId);

        public UnitData[] UnitDatas;
        protected override Vector2 Use(Vector2 _)
        {
            if (Army.AliveUnits >= 150) return Army.Leader.pos;
            var data = UnitDatas[Random.Range(0, UnitDatas.Length)];
            var unit = UnitsPool.Instance.CreateUnit(((Vector2) Army.Leader.transform.localPosition).Jitter(), data);
            unit.Army = Army;
            EventSummoned(unit.netId);
            if (Summoned != null)
            {
                Summoned(unit);
            }
            return unit.pos;
        }

        [SyncEvent]
        public event EventSummonedDelegate EventSummoned;

        public event Action<Unit> Summoned;
    }
}

#endif