﻿#if ABILITY_ENABLED
using System;
using Assets.Scripts.Attributes;
using Assets.Scripts.Effects;

namespace Assets.Scripts.Abilities
{
    public class Bdysh : SupportAbility<Drakkar>
    {
        [Common.ClientValueTag(IsPercent = true, LevelAddicted = true)]
        public StraightLevelRelBonus DamageBonus;
        protected override long MainAbilityId
        {
            get { return 13; }
        }

        protected override void OnEnabled()
        {
            DamageBonus.Level = Level;
            MainAbility.Casted += MainAbilityOnCasted;
#if EXTENDED_LOGGING
            Logging.Log(this, "Enabled. Level : {0}, DamageBonus : {1}", Level, DamageBonus.GetValue());
            DamageBonus.Updated += bonus => Logging.Log(this, "Damage bonus changed : {0}", bonus.GetValue());
#endif
        }

        private void MainAbilityOnCasted(DrakkarEffect drakkarEffect)
        {
            drakkarEffect.SkillDamage += DamageBonus;
#if EXTENDED_LOGGING
            Logging.Log(this, "Procced. Drakkar skill damage : {0}", drakkarEffect.SkillDamage);
#endif
        }

        protected override void OnDisabled()
        {
#if EXTENDED_LOGGING
            Logging.Log(this, "Disabled");
#endif
            MainAbility.Casted -= MainAbilityOnCasted;
        }
    }
}
#endif