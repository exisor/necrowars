#if ABILITY_ENABLED
using System;
using System.Linq;
using Assets.Scripts.Attributes;
using Assets.Scripts.Effects;
using UnityEngine;
using UnityEngine.Networking;

namespace Assets.Scripts.Abilities
{
    public class Drakkar : ActiveAbility , ISkillRadius, ISkillRange
    {
        [Common.ClientValueTag]
        public FloatAttribute Distance;
		[Common.ClientValueTag]
        public FloatAttribute Radius;
#pragma warning disable 414
        [SyncVar]
        private float _distance;
        [SyncVar]
        private float _radius;
#pragma warning restore 414
        [Common.ClientObjectTag]
        public DrakkarEffect DrakkarEffect;
        protected override void Start()
        {
            base.Start();
            _radius = Radius;
            Radius.ValueChanged += val => _radius = val;
            _distance = Distance;
            Distance.ValueChanged += val => _distance = val;
#if EXTENDED_LOGGING
            Logging.Log(this, "Drakkar.Start. Distance : {0}, Radius : {1}", Distance, Radius);
            Radius.ValueChanged += val => Logging.Log(this, "Radius changed : {0}", val);
            Distance.ValueChanged += val => Logging.Log(this, "Distance changed : {0}", val);
#endif
        }

        protected override Vector2 Use(Vector2 target)
        {
            var maxHP = 0f;
            Unit maxUnit = null;
            foreach (var unit in Army.Units.Where(u => u.Alive && !u.IsLeader))
            {
                if (unit.HP < maxHP) continue;
                maxHP = unit.HP;
                maxUnit = unit;
            }

            if (maxUnit == null) return Army.Leader;

#if EXTENDED_LOGGING
            Logging.Log(this, "Unit selected for shooting. Id: {0} at {1} with hp {2}", maxUnit.netId, maxUnit.transform.localPosition, maxHP);
#endif

            var damage = Damage.Unblockable(Army.Leader, maxUnit, maxHP) / (float)Math.Sqrt(maxUnit.Quantity);

            var effect = Effect.Create(DrakkarEffect);
            effect.Radius += Radius;
            effect.Owner = Army;
            var actualTarget = target.ClampTarget(Army.Leader, Distance);
            effect.MoveTarget = actualTarget;
            effect.SkillDamage += damage;
#if EXTENDED_LOGGING
            Logging.Log(this, "Drakkar ground effect created. Radius : {0}, MoveTarget : {1}. Base Damage : {2} Total Damage : {3}", effect.Radius, effect.MoveTarget, damage, effect.SkillDamage);
#endif
            OnCasted(effect);
            return actualTarget;
        }

        public event Action<DrakkarEffect> Casted;

        protected virtual void OnCasted(DrakkarEffect obj)
        {
            var handler = Casted;
            if (handler != null) handler(obj);
        }

        public FloatAttribute SkillRadius
        {
            get { return Radius; } set { Radius = value; }
        }

        #region Implementation of ISkillRange

        public float SkillRange { get { return Distance; } }

        #endregion
    }
}
#endif