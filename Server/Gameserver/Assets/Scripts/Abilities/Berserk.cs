#if ABILITY_ENABLED
using System;
using System.Linq;
using Assets.Scripts.Effects;
using UnityEngine;

namespace Assets.Scripts.Abilities
{
    public class Berserk : ActiveAbility
    {
        [Common.ClientObjectTag]
        public AttackEffect AttackEffect;
        protected override Vector2 Use(Vector2 _)
        {
            foreach (var unit in Army.Units.Where(u => u.Alive))
            {
#if EXTENDED_LOGGING
                Logging.Log(this, "Used on target : {0} in {1}", unit.netId, unit.transform.localPosition);
#endif
                var effect = Effect.Create(AttackEffect);
                effect.Target = unit;
                OnBuffed(effect);
            }
            return Army.Leader;
        }

        public event Action<AttackEffect> Buffed;

        protected virtual void OnBuffed(AttackEffect obj)
        {
            var handler = Buffed;
            if (handler != null) handler(obj);
        }
    }
}
#endif