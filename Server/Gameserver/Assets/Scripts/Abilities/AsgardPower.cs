
#if ABILITY_ENABLED
using Assets.Scripts.Attributes;

namespace Assets.Scripts.Abilities
{
    public class AsgardPower : PassiveAbility
    {
        [Common.ClientValueTag(IsPercent = true, LevelAddicted = true)]
        public StraightLevelRelBonus ValkyrieAttackBonus;
        public override void Enable()
        {
            ValkyrieAttackBonus.Level = Level;
#if EXTENDED_LOGGING
            Logging.Log(this, "Enabled. Level : {0}, AttackBonus : {1}", Level, ValkyrieAttackBonus.GetValue());
            ValkyrieAttackBonus.Updated +=
                bonus => Logging.Log(this, "Valkyrie Attack Bonus changed : {0}", bonus.GetValue());
#endif
            Army.Leader.MinDamage += ValkyrieAttackBonus;
            Army.Leader.MaxDamage += ValkyrieAttackBonus;
        }

        public override void Disable()
        {
#if EXTENDED_LOGGING
            Logging.Log(this, "Disabled");
#endif
            if (Army.Leader == null) return;
            Army.Leader.MinDamage -= ValkyrieAttackBonus;
            Army.Leader.MaxDamage -= ValkyrieAttackBonus;
        }
    }
}
#endif