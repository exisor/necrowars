#if ABILITY_ENABLED

using UnityEngine;

namespace Assets.Scripts.Abilities
{
    public abstract class PassiveAbility : MonoBehaviour

    {
        public long id;
        public long Id { get { return id; } }
        public Army Army { get; set; }
        public int Level { get; set; }
        public abstract void Enable();
        public abstract void Disable();
    }
}

#endif