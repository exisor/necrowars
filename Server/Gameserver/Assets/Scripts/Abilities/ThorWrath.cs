#if ABILITY_ENABLED
using System;
using System.Linq;
using Assets.Scripts.Attributes;
using Assets.Scripts.Effects;
using UnityEngine;
using UnityEngine.Networking;

namespace Assets.Scripts.Abilities
{
    public class ThorWrath : ActiveAbility, ISkillRadius, ISkillRange
    {
        [Common.ClientValueTag]
        public FloatAttribute Radius;
        [Common.ClientValueTag]
        public FloatAttribute Distance;

        [Common.ClientObjectTag]
        public StunEffect StunEffect;

#pragma warning disable 414
        [SyncVar]
        private float _radius;
        [SyncVar]
        private float _distance;
#pragma warning restore 414

        protected override void Start()
        {
            base.Start();

            _radius = Radius;
            Radius.ValueChanged += val => _radius = val;

            _distance = Distance;
            Distance.ValueChanged += val => _distance = val;
        }

        protected override Vector2 Use(Vector2 target)
        {
            var actualTarget = target.ClampTarget(Army.Leader, Distance);
            var targets = Physics2D.OverlapCircleAll(actualTarget, Radius, LayerMask.GetMask("units")).Select(t => t.GetComponent<Unit>());
            foreach (var unit in targets.Where(u => u.Army != Army && u.Alive))
            {
                var effect = Effect.Create(StunEffect);
                effect.Target = unit;

                if (TargetStunned != null) TargetStunned(effect);
            }
            return actualTarget;
        }

        public event Action<StunEffect> TargetStunned;
        public FloatAttribute SkillRadius { get { return Radius; } set { Radius = value; } }

        #region Implementation of ISkillRange

        public float SkillRange { get { return Distance; } }

        #endregion
    }
}
#endif