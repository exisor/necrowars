﻿#if ABILITY_ENABLED
using System;
using Assets.Scripts.Attributes;
using Assets.Scripts.Effects;

namespace Assets.Scripts.Abilities
{
    [Obsolete("Skill is not usable")]
    public class Vomit : SupportAbility<Katzenjammer>
    {
        [Common.ClientObjectTag]
        public AttackSpeedEffect AttackSpeedDebuff;
        [Common.ClientValueTag]
        public StraightLevelRelBonus Amount;

        protected override long MainAbilityId
        {
            get { return 28; }
        }

        protected override void OnEnabled()
        {
            Amount.Level = Level;
            MainAbility.Debuff += MainAbilityOnDebuff;
        }

        private void MainAbilityOnDebuff(AttackEffect attackEffect)
        {
            var effect = Effect.Create(AttackSpeedDebuff);
            effect.Amount += Amount;
            effect.Target = attackEffect.Target;
        }

        protected override void OnDisabled()
        {
            MainAbility.Debuff -= MainAbilityOnDebuff;
        }
    }
}
#endif