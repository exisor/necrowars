#if ABILITY_ENABLED
using System.Linq;
using UnityEngine;

namespace Assets.Scripts.Abilities
{
    public class CooldownPotion : AbilityWithCharges
    {
        protected override Vector2 Use(Vector2 target)
        {
            foreach (var ability in Army.ActiveAbilities.Where(ability => ability != this))
            {
#if EXTENDED_LOGGING
                Logging.Log(this, "Resetting ability : {0} cooldown timer", ability.netId);
#endif
                ability.CooldownTimer = 0;
            }
            return Army.Leader;
        }
    }
}
#endif