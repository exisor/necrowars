﻿#if ABILITY_ENABLED
using System;
using Assets.Scripts.Attributes;
using Assets.Scripts.Effects;

namespace Assets.Scripts.Abilities
{
    public class Vschi : SupportAbility<Ragnarek>
    {
        [Common.ClientValueTag(IsPercent = true, LevelAddicted = true)]
        public StraightLevelRelBonus DamageBonus;
        protected override long MainAbilityId
        {
            get { return 31; }
        }

        protected override void OnEnabled()
        {
            DamageBonus.Level = Level;
            MainAbility.Cast+=MainAbilityOnCast;
        }

        private void MainAbilityOnCast(RagnarekAOE ragnarekAoe)
        {
            ragnarekAoe.SkillDamage += DamageBonus;
        }

        protected override void OnDisabled()
        {
            MainAbility.Cast -= MainAbilityOnCast;
        }
    }
}
#endif