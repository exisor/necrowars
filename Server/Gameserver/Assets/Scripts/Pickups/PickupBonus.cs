﻿using Assets.Scripts.Abilities;
using Assets.Scripts.Attributes;
using UnityEngine;
using UnityEngine.Networking;

namespace Assets.Scripts.Pickups
{
    [RequireComponent(typeof(CircleCollider2D))]
    public class PickupBonus : NetworkBehaviour
    {
        private CircleCollider2D _collider;
        public Vector2 Center;
        private int Level
        {
            get { return _level; }
            set
            {
                _level = value;
                LevelGoldBonus.Level = _level;
                LevelExpBonus.Level = _level;
                LevelPointsBonus.Level = _level;
                _radius = LevelToRadius(_level);
                _collider.radius = _radius;
            }
        }

        public bool Enabled;
        public float Jitter;
        public FloatAttribute GoldBonus;
        public StraightLevelRelBonus LevelGoldBonus;
        public MutableRelFloatBonus RandomGoldMult = new MutableRelFloatBonus();
        public FloatAttribute ExpBonus;
        public ExpLevelRelBonus LevelExpBonus;
        public MutableRelFloatBonus RandomExpMult = new MutableRelFloatBonus();
        public FloatAttribute PointsBonus;
        public StraightLevelRelBonus LevelPointsBonus;
        public MutableRelFloatBonus RandomPointsMult = new MutableRelFloatBonus();
       
        private int _level;
        private float _respawnTimer;
        [SerializeField] private float _respawnTime = 15f;
        [SyncVar] private float _radius;

        private bool _initialized;

        void Awake()
        {
            _collider = GetComponent<CircleCollider2D>();
            _collider.isTrigger = true;
            Center = transform.localPosition;
            PointsBonus += LevelPointsBonus;
            GoldBonus += LevelGoldBonus;
            ExpBonus += LevelExpBonus;
            GoldBonus += RandomGoldMult;
            ExpBonus += RandomExpMult;
            PointsBonus += RandomPointsMult;
            _initialized = true;
        }

        public float LevelToRadius(int level)
        {
            const float BASE_RADIUS = 0.1f;
            return BASE_RADIUS * (1 +  Mathf.Sqrt(f: level/10));
        }

        void OnTriggerEnter2D(Collider2D c)
        {
            var u = c.GetComponent<Unit>();
            if (u.Army == null) return;
            u.Army.ConsumeBonus(this);
            Disable();
        }

        private void Disable()
        {
            NetworkServer.UnSpawn(gameObject);
            Enabled = false;
            gameObject.SetActive(false);
        }

        public void ManagedUpdate()
        {
            if (!_initialized) return;
            _respawnTimer -= Time.deltaTime;
            if (_respawnTimer <= 0)
            {
                TryEnable();
                _respawnTimer = _respawnTime;
            }
        }

        private static readonly Collider2D[] TemporaryTargets = new Collider2D[2];
        private void TryEnable()
        {
            Level = ArmiesManager.Instance.GetRandomLevel();
            var t = Center.Jitter(Jitter);
            if (Physics2D.OverlapCircleNonAlloc(t, _radius, TemporaryTargets) > 0)
            {
                TemporaryTargets[0] = null;
                TemporaryTargets[1] = null;
                return;
            }
            transform.localPosition = t;
            RandomExpMult.SetValue(Random.Range(-1f, 0f));
            RandomGoldMult.SetValue(Random.Range(-1f, 0f));
            RandomPointsMult.SetValue(Random.Range(-1f, 0f));
            gameObject.SetActive(true);
            Enabled = true;
            NetworkServer.Spawn(gameObject);
        }
    }
}
