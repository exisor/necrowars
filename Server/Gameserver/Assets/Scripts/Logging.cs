﻿#if EXTENDED_LOGGING
using Assets.Scripts.Abilities;
using Assets.Scripts.Items;
using UnityEngine;
using UnityEngine.Networking;

namespace Assets.Scripts
{
    public static class Logging
    {
        public static bool Unit;
        public static bool Ability;
        public static bool Item;

        public static void Log(object caller, string format, params object[] args)
        {
            if ((caller is Ability || caller is PassiveAbility)
                && !Ability) return;

            if (caller is Unit && !Unit) return;

            if (caller is IItemAffixHandler && !Item) return;

            var netId = NetworkInstanceId.Invalid;

            var networkBehaviour = caller as NetworkBehaviour;
            if (networkBehaviour != null)
            {
                netId = networkBehaviour.netId;
            }

            Debug.LogFormat("[{0}:{1}] : {2}", caller.GetType(), netId, string.Format(format, args));
        }
    }
}
#endif