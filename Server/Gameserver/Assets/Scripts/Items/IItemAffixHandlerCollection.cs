using Common.Resources;

namespace Assets.Scripts.Items
{
    public interface IItemAffixHandlerCollection
    {
        void ApplyAffix(Army army, IAffixValues affix);
    }
}