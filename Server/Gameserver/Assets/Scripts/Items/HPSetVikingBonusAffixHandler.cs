using Common.Resources;

namespace Assets.Scripts.Items
{
    public class HPSetVikingBonusAffixHandler : IItemAffixHandler
    {
        // 2 items hp bonus to vikings + 100%

        public long Id { get { return 44; } }
        public void ApplyAffix(Army army, IAffixValues affix)
        {
            var hp = affix.GetValue("HP");
            army += new MinionHPBonus(hpPercent: (float)hp);
        }
    }
}