﻿using Common.Resources;

namespace Assets.Scripts.Items
{
    public interface IItemAffixHandler
    {
        long Id { get; }
        void ApplyAffix(Army army, IAffixValues affix);
    }
}