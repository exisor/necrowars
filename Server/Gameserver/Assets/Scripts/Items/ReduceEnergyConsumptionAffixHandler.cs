using Assets.Scripts.Attributes;
using Common.Resources;

namespace Assets.Scripts.Items
{
    public class ReduceEnergyConsumptionAffixHandler : IItemAffixHandler
    {
        public long Id
        {
            get { return 13; }
        }

        public void ApplyAffix(Army army, IAffixValues affix)
        {
            var energy = RelFloatBonus.FromPercent(- affix.GetValue("Energy"));
            army += new ReduceEnergyConsumptionBonus(energy);
        }
    }
}