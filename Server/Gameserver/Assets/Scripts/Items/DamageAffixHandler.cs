using Assets.Scripts.Attributes;
using Common.Resources;

namespace Assets.Scripts.Items
{
    public class DamageAffixHandler : IItemAffixHandler
    {
        public long Id { get { return 19; } }
        public void ApplyAffix(Army army, IAffixValues affix)
        {
            var damage = RelFloatBonus.FromPercent(affix.GetValue("Damage"));

            army.Leader.MinDamage += damage;
            army.Leader.MaxDamage += damage;
        }
    }
}