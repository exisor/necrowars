using Assets.Scripts.Attributes;

namespace Assets.Scripts.Items
{
    public class MinionHPBonus : MinionsBonus
    {
        private readonly FloatBonus _bonus;

        public MinionHPBonus(FloatBonus bonus)
        {
            _bonus = bonus;
        }
        public MinionHPBonus(float hpPercent) : this(RelFloatBonus.FromPercent(hpPercent)) { }

        public MinionHPBonus(int fixedHP) : this(new FixedFloatBonus(fixedHP)) { }
        protected override void ApplyBonusInternal(Unit unit)
        {
            unit.MaxHP += _bonus;
        }

        protected override void RemoveBonusInternal(Unit unit)
        {
            unit.MaxHP -= _bonus;
        }
    }
}