﻿using System;
using System.Collections.Generic;
using System.Linq;
using Common.Resources;

namespace Assets.Scripts.Items
{
    public class ItemAffixHandlerCollection : IItemAffixHandlerCollection
    {
        private readonly Dictionary<long, IItemAffixHandler> _affixHandlers;

        public ItemAffixHandlerCollection()
        {
            _affixHandlers = GetType()
                .Assembly
                .GetTypes()
                .Where(t => typeof (IItemAffixHandler).IsAssignableFrom(t) && !t.IsInterface && !t.IsAbstract)
                .Select(t => Activator.CreateInstance(t) as IItemAffixHandler)
                .ToDictionary(key => key.Id, val => val);
        }

        public void ApplyAffix(Army army, IAffixValues affix)
        {
            IItemAffixHandler handler;
            if (_affixHandlers.TryGetValue(affix.Id, out handler))
            {
                handler.ApplyAffix(army, affix);
            }
        }
    }
}
