using Common.Resources;

namespace Assets.Scripts.Items
{
    public class FixedDamageAffixHandler : IItemAffixHandler
    {
        public long Id { get { return 18; } }
        public void ApplyAffix(Army army, IAffixValues affix)
        {
            var minDamage = affix.GetValue("DamageMin");
            var maxDamage = affix.GetValue("DamageMax");

            army.Leader.MinDamage += minDamage;
            army.Leader.MaxDamage += maxDamage;
        }
    }
}