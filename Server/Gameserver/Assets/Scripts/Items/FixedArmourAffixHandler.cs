using Common.Resources;

namespace Assets.Scripts.Items
{
    public class FixedArmourAffixHandler : IItemAffixHandler
    {
        public long Id { get { return 2; } }
        public void ApplyAffix(Army army, IAffixValues affix)
        {
            var armour = affix.GetValue("Armour");
            army.Leader.Armor += armour;
        }
    }
}