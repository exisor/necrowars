using Assets.Scripts.Attributes;
using Common.Resources;

namespace Assets.Scripts.Items
{
    public class IncreaseAOEDamageAffixHandler : IItemAffixHandler
    {
        public long Id
        {
            get { return 26; }
        }

        public void ApplyAffix(Army army, IAffixValues affix)
        {
            var damage = affix.GetValue("Damage");
            army.Leader.SkillDamage += new RelFloatBonus(damage);
        }
    }
}