using Assets.Scripts.Attributes;

namespace Assets.Scripts.Items
{
    public class MinionAttackSpeedBonus : MinionsBonus
    {
        private readonly RelFloatBonus _bonus;

        public MinionAttackSpeedBonus(RelFloatBonus bonus)
        {
            _bonus = bonus;
        }

        public MinionAttackSpeedBonus(int percent) : this(RelFloatBonus.FromPercent(percent)) { }
        public MinionAttackSpeedBonus(float value) : this(new RelFloatBonus(value)) { }
        protected override void ApplyBonusInternal(Unit unit)
        {
            unit.AttackSpeed += _bonus;
        }

        protected override void RemoveBonusInternal(Unit unit)
        {
            unit.AttackSpeed -= _bonus;
        }
    }
}