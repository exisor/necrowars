using Assets.Scripts.Attributes;
using Common.Resources;

namespace Assets.Scripts.Items
{
    public class HPSetValkyrieBonusAffixHandler : IItemAffixHandler
    {
        // 2 items hp bonus to vikings + 100%

        public long Id { get { return 45; } }
        public void ApplyAffix(Army army, IAffixValues affix)
        {
            var hp = affix.GetValue("HP");
            army.Leader.MaxHP += RelFloatBonus.FromPercent(hp);
        }
    }
}