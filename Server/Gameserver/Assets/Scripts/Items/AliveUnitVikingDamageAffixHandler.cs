using Common.Resources;

namespace Assets.Scripts.Items
{
    public class AliveUnitVikingDamageAffixHandler : IItemAffixHandler
    {

        // 2 items set bonus.
        // for every alive viking get +1% damage
        public long Id { get { return 40; } }
        public void ApplyAffix(Army army, IAffixValues affix)
        {
            var damage = affix.GetValue("Damage");

            army += new MinionDamageBonus(new AliveUnitsRelFloatBonus(army, (float)damage / 100.0f));
        }
    }
}