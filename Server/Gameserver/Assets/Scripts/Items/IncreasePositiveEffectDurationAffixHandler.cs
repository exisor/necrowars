using Assets.Scripts.Attributes;
using Common.Resources;

namespace Assets.Scripts.Items
{
    public class IncreasePositiveEffectDurationAffixHandler : IItemAffixHandler
    {
        public long Id { get { return 12; } }
        public void ApplyAffix(Army army, IAffixValues affix)
        {
            var duration = RelFloatBonus.FromPercent(affix.GetValue("Duration"));
            army += new MinionIncreasePositiveEffectDurationBonus(duration);
            army.Leader.IncreasePositiveEffectDuration += duration;
        }
    }
}