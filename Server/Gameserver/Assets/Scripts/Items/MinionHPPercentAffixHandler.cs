using Common.Resources;

namespace Assets.Scripts.Items
{
    public class MinionHPPercentAffixHandler : IItemAffixHandler
    {
        public long Id { get { return 5; } }
        public void ApplyAffix(Army army, IAffixValues affix)
        {
            var hp = affix.GetValue("HP");
            army += new MinionHPBonus(hpPercent:(float)hp);
        }
    }
}