﻿
namespace Assets.Scripts.Items
{
    public abstract class MinionsBonus
    {
        public void ApplyBonus(Unit unit)
        {
            if (unit.IsLeader) return;
            ApplyBonusInternal(unit);
        }
        protected abstract void ApplyBonusInternal(Unit unit);

        public void RemoveBonus(Unit unit)
        {
            if (unit.IsLeader) return;
            RemoveBonusInternal(unit);
        }

        protected abstract void RemoveBonusInternal(Unit unit);
    }
}

