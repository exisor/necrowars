﻿using Assets.Scripts.Attributes;

namespace Assets.Scripts.Items
{
    public class MinionReduceNegativeEffectDurationBonus : MinionsBonus
    {
        private readonly FloatBonus _bonus;

        public MinionReduceNegativeEffectDurationBonus(FloatBonus bonus)
        {
            _bonus = bonus;
        }

        public MinionReduceNegativeEffectDurationBonus(float value)
        {
            _bonus = new FixedFloatBonus(value);   
        }

        protected override void ApplyBonusInternal(Unit unit)
        {
            unit.ReduceNegativeEffectDuration += _bonus;
        }

        protected override void RemoveBonusInternal(Unit unit)
        {
            unit.ReduceNegativeEffectDuration -= _bonus;
        }
    }
}
