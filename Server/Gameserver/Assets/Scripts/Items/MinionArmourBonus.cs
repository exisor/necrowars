﻿using Assets.Scripts.Attributes;

namespace Assets.Scripts.Items
{
    public class MinionArmourBonus : MinionsBonus
    {
        private readonly FloatBonus _bonus;

        public MinionArmourBonus(FloatBonus bonus)
        {
            _bonus = bonus;
        }

        public MinionArmourBonus(int armour)
        {
            _bonus = new FixedFloatBonus(armour);
        }

        public MinionArmourBonus(float percent)
        {
            _bonus = RelFloatBonus.FromPercent(percent);
        }

        protected override void ApplyBonusInternal(Unit unit)
        {
            unit.Armor += _bonus;
        }

        protected override void RemoveBonusInternal(Unit unit)
        {
            unit.Armor -= _bonus;
        }
    }
}