using Common.Resources;

namespace Assets.Scripts.Items
{
    public class MinionArmourOfMasterAffixHandler : IItemAffixHandler
    {
        public long Id
        {
            get { return 7; }
        }

        public void ApplyAffix(Army army, IAffixValues affix)
        {
            var armour = affix.GetValue("Armour");
            army += new MasterArmorToMinion(percent: armour, leader: army.Leader);
        }
    }
}