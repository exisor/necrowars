using Common.Resources;

namespace Assets.Scripts.Items
{
    public class AliveUnitVikingArmorAffixHandler : IItemAffixHandler
    {

        // 2 items set bonus.
        // for every alive viking get +1% armour
        public long Id { get { return 42; } }
        public void ApplyAffix(Army army, IAffixValues affix)
        {
            var armour = affix.GetValue("Armour");

            army += new MinionArmourBonus(new AliveUnitsRelFloatBonus(army, (float)armour / 100.0f));
        }
    }
}