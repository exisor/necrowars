using System.Linq;
using Common.Resources;

namespace Assets.Scripts.Items
{
    public class PassiveSkillAffixHandler : IItemAffixHandler
    {
        public long Id { get { return 16; } }
        public void ApplyAffix(Army army, IAffixValues affix)
        {
#if ABILITY_ENABLED

            var skillId = affix.GetValue("AvailableSkills");
            var skillPoint = affix.GetValue("Skill");

            var ability = army.PassiveAbilities.FirstOrDefault(a => a.Id == skillId);
            if (ability == null) return;

            ability.Level += skillPoint;
#endif
        }
    }
}