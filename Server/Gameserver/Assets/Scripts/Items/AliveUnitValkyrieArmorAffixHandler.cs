using Common.Resources;

namespace Assets.Scripts.Items
{
    public class AliveUnitValkyrieArmorAffixHandler : IItemAffixHandler
    {
        // 5 items set bonus.
        // for every alive viking valkyrie get +1% armour
        public long Id { get { return 43; } }
        public void ApplyAffix(Army army, IAffixValues affix)
        {
            var armour = affix.GetValue("Armour");

            army.Leader.Armor += new AliveUnitsRelFloatBonus(army, (float)armour / 100.0f);
        }
    }
}