using Common.Resources;

namespace Assets.Scripts.Items
{
    public class MinionArmourAffixHandler : IItemAffixHandler
    {
        public long Id { get { return 9; } }
        public void ApplyAffix(Army army, IAffixValues affix)
        {
            var armour = affix.GetValue("Armour");
            army += new MinionArmourBonus(percent:(float)armour);
        }
    }
}