using Common.Resources;

namespace Assets.Scripts.Items
{
    public class ImplicitArmorAffixHandler : IItemAffixHandler
    {
        public long Id { get { return 29; } }
        public void ApplyAffix(Army army, IAffixValues affix)
        {
            var armor = affix.GetValue("Armour");

            army.Leader.Armor += armor;
        }
    }
}