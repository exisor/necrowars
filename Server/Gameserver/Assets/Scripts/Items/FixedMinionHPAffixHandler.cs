using Common.Resources;

namespace Assets.Scripts.Items
{
    public class FixedMinionHPAffixHandler : IItemAffixHandler
    {
        public long Id
        {
            get { return 6; }
        }

        public void ApplyAffix(Army army, IAffixValues affix)
        {
            var hp = affix.GetValue("HP");
            army += new MinionHPBonus(fixedHP:hp);
        }
    }
}