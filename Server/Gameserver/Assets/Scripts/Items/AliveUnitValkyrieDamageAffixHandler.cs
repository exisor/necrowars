using Common.Resources;

namespace Assets.Scripts.Items
{
    public class AliveUnitValkyrieDamageAffixHandler : IItemAffixHandler
    {
        // 5 items set bonus
        // for every alive viking valkyrie get +1% dmg
        public long Id { get { return 41; } }
        public void ApplyAffix(Army army, IAffixValues affix)
        {
            var damage = affix.GetValue("Damage");

            var bonus = new AliveUnitsRelFloatBonus(army, (float) damage/100.0f);

            army.Leader.MinDamage += bonus;
            army.Leader.MaxDamage += bonus;
        }
    }
}