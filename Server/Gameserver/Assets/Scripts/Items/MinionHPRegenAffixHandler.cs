using Common.Resources;

namespace Assets.Scripts.Items
{
    public class MinionHPRegenAffixHandler : IItemAffixHandler
    {
        public long Id
        {
            get { return 15; }
        }

        public void ApplyAffix(Army army, IAffixValues affix)
        {
            var hp = affix.GetValue("HP");
            army += new MinionHPRegenBonus(hp);
        }
    }
}