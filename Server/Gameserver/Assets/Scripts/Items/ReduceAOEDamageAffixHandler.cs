using Assets.Scripts.Attributes;
using Common.Resources;

namespace Assets.Scripts.Items
{
    public class ReduceAOEDamageAffixHandler : IItemAffixHandler
    {
        public long Id { get { return 10; } }

        public void ApplyAffix(Army army, IAffixValues affix)
        {
            var damageReduce = affix.GetValue("Damage");
            army.Leader.SpellDamageDefence += RelFloatBonus.FromPercent(- damageReduce);
        }
    }
}