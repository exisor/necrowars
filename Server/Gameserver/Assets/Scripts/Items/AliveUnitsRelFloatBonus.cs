using Assets.Scripts.Attributes;

namespace Assets.Scripts.Items
{
    public class AliveUnitsRelFloatBonus : MutableRelFloatBonus
    {
        private readonly float _multiplier;
        private readonly Army _host;
        public AliveUnitsRelFloatBonus(Army army, float multiplier)
        {
            _multiplier = multiplier;
            _host = army;
            _host.AliveUnitsChanged += OnAliveUnitChanged;
            OnAliveUnitChanged(army.AliveUnits);
        }
        protected override void Free()
        {
            _host.AliveUnitsChanged -= OnAliveUnitChanged;
        }

        void OnAliveUnitChanged(int value)
        {
            SetValue((float) value* _multiplier);
        }
    }
}