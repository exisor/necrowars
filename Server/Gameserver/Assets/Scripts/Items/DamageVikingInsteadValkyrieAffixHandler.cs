using Assets.Scripts.Effects;
using Common.Resources;
using UnityEngine;

namespace Assets.Scripts.Items
{
    public class DamageVikingInsteadValkyrieAffixHandler : IItemAffixHandler
    {
        private readonly ValkyrieDamageRedirrect _prefab = Resources.Load<GameObject>("Prefabs/Effects/ValkyrieDamageRedirrect").GetComponent<ValkyrieDamageRedirrect>();
        public long Id { get
        {
            return 47;
        } }
        public void ApplyAffix(Army army, IAffixValues affix)
        {
            var effect = Effect.Create(_prefab);
            effect.Target = army.Leader;
        }
    }

    public class YouAreMinePassiveAffixHandler : IItemAffixHandler
    {
        private readonly YouAreMineNowEffect _prefab = Resources.Load<GameObject>("Prefabs/Effects/YouAreMineEffect").GetComponent<YouAreMineNowEffect>();
        public long Id { get { return 48; } }
        public void ApplyAffix(Army army, IAffixValues affix)
        {
            var value = (float)affix.GetFloatValue("Chance")/100f;
            var effect = Effect.Create(_prefab);
            effect.Chance = value;
            effect.Target = army.Leader;
        }
    }
}