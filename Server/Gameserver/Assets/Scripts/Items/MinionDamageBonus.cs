using Assets.Scripts.Attributes;

namespace Assets.Scripts.Items
{
    public class MinionDamageBonus : MinionsBonus
    {
        private readonly RelFloatBonus _bonus;

        public MinionDamageBonus(RelFloatBonus bonus)
        {
            _bonus = bonus;
        }
        public MinionDamageBonus(float value) : this(new RelFloatBonus(value)) { }

        public MinionDamageBonus(int percent) : this(RelFloatBonus.FromPercent(percent)) { }
        protected override void ApplyBonusInternal(Unit unit)
        {
            unit.MinDamage += _bonus;
            unit.MaxDamage += _bonus;
        }

        protected override void RemoveBonusInternal(Unit unit)
        {
            unit.MinDamage -= _bonus;
            unit.MaxDamage -= _bonus;
        }
    }
}