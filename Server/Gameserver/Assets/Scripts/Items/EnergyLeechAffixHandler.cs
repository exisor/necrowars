using Common.Resources;

namespace Assets.Scripts.Items
{
    public class EnergyLeechAffixHandler : IItemAffixHandler
    {
        public long Id
        {
            get { return 25; }
        }

        public void ApplyAffix(Army army, IAffixValues affix)
        {
            var leech = affix.GetValue("Leech")/100f;
            army.Leader.HungerLeech += leech;
        }
    }
}