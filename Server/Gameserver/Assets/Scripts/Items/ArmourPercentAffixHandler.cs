using Assets.Scripts.Attributes;
using Assets.Scripts.Effects;
using Common.Resources;

namespace Assets.Scripts.Items
{
    public class ArmourPercentAffixHandler : IItemAffixHandler
    {
        public long Id { get { return 3; } }
        public void ApplyAffix(Army army, IAffixValues affix)
        {
            var armour = affix.GetValue("Armour");
            army.Leader.Armor += RelFloatBonus.FromPercent(armour);
        }
    }
}