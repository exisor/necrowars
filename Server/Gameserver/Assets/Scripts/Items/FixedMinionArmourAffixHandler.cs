using Common.Resources;

namespace Assets.Scripts.Items
{
    public class FixedMinionArmourAffixHandler : IItemAffixHandler
    {
        public long Id
        {
            get { return 8; }
        }

        public void ApplyAffix(Army army, IAffixValues affix)
        {
            var armour = affix.GetValue("Armour");
            army += new MinionArmourBonus(armour:armour);
        }
    }
}