using Assets.Scripts.Attributes;
using Common.Resources;

namespace Assets.Scripts.Items
{
    public class DecreaseCooldownAffixHandler : IItemAffixHandler
    {
        public long Id {get { return 28; } }
        public void ApplyAffix(Army army, IAffixValues affix)
        {
#if ABILITY_ENABLED

            var cooldown = RelFloatBonus.FromPercent(- affix.GetValue("Cooldown"));

            foreach (var skill in army.ActiveAbilities)
            {
                skill.Cooldown += cooldown;
            }
#endif
        }
    }
}