﻿using Assets.Scripts.Attributes;

namespace Assets.Scripts.Items
{
    public class ReduceEnergyConsumptionBonus : MinionsBonus
    {
        private readonly RelFloatBonus _bonus;

        public ReduceEnergyConsumptionBonus(RelFloatBonus bonus)
        {
            _bonus = bonus;
        }
        protected override void ApplyBonusInternal(Unit unit)
        {
            unit.ConsumesHunger += _bonus;
        }

        protected override void RemoveBonusInternal(Unit unit)
        {
            unit.ConsumesHunger -= _bonus;
        }
    }
}