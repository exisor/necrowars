using Common.Resources;

namespace Assets.Scripts.Items
{
    public class LifeLeechAffixHandler : IItemAffixHandler
    {
        public long Id
        {
            get { return 24; }
        }

        public void ApplyAffix(Army army, IAffixValues affix)
        {
            var leech = affix.GetValue("Leech") / 100f;

            army.Leader.HPLeech += leech;
        }
    }
}