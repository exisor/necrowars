using Common.Resources;

namespace Assets.Scripts.Items
{
    public class FixedHPAffixHandler : IItemAffixHandler
    {
        public long Id { get { return 1; } }
        public void ApplyAffix(Army army, IAffixValues affix)
        {
            var hp = affix.GetValue("HP");
            army.Leader.MaxHP += hp;
        }
    }
}