using Common.Resources;

namespace Assets.Scripts.Items
{
    public class ArmyLimitAffixHandler : IItemAffixHandler
    {
        public long Id { get { return 46; } }

        public void ApplyAffix(Army army, IAffixValues affix)
        {
            var bonus = affix.GetValue("VikingsLimit");
            army.VikingsLimit += bonus;
        }
    }
}