using Assets.Scripts.Attributes;

namespace Assets.Scripts.Items
{
    public class MinionIncreasePositiveEffectDurationBonus : MinionsBonus
    {
        private readonly FloatBonus _bonus;

        public MinionIncreasePositiveEffectDurationBonus(FloatBonus bonus)
        {
            _bonus = bonus;
        }

        protected override void ApplyBonusInternal(Unit unit)
        {
            unit.IncreasePositiveEffectDuration += _bonus;
        }

        protected override void RemoveBonusInternal(Unit unit)
        {
            unit.IncreasePositiveEffectDuration -= _bonus;
        }
    }
}