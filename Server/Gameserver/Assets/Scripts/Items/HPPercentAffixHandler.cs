using Assets.Scripts.Attributes;
using Common.Resources;

namespace Assets.Scripts.Items
{
    public class HPPercentAffixHandler : IItemAffixHandler
    {
        public long Id
        {
            get { return 4; }
        }

        public void ApplyAffix(Army army, IAffixValues affix)
        {
            var hp = affix.GetValue("HP");
            army.Leader.MaxHP += RelFloatBonus.FromPercent(hp);
        }
    }
}