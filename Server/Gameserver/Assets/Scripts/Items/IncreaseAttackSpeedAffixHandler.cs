using Assets.Scripts.Attributes;
using Common.Resources;

namespace Assets.Scripts.Items
{
    public class IncreaseAttackSpeedAffixHandler : IItemAffixHandler
    {
        public long Id
        {
            get { return 22; }
        }

        public void ApplyAffix(Army army, IAffixValues affix)
        {
            var aspd = affix.GetValue("AttackSpeed");

            army.Leader.AttackSpeed += RelFloatBonus.FromPercent(aspd);
        }
    }
}