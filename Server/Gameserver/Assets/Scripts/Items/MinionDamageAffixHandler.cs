using Common.Resources;

namespace Assets.Scripts.Items
{
    public class MinionDamageAffixHandler : IItemAffixHandler
    {
        public long Id { get { return 20; } }
        public void ApplyAffix(Army army, IAffixValues affix)
        {
            var damage = affix.GetValue("Damage");

            army += new MinionDamageBonus(percent: damage);
        }
    }
}