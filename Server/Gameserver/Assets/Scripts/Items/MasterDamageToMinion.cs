using Assets.Scripts.Attributes;

namespace Assets.Scripts.Items
{
    public class MasterDamageToMinion : MinionsBonus
    {
        public MasterDamageToMinion(int percent, ArmyLeader leader) : this(percent / 100f, leader) { }
        public MasterDamageToMinion(float value, ArmyLeader leader)
        {
            _minDamageBonus = new AttributeDependantFixedBonus<MinDamageAttribute>(value, leader.MinDamage);
            _maxDamageBonus = new AttributeDependantFixedBonus<MaxDamageAttribute>(value, leader.MaxDamage);
        }

        private readonly AttributeDependantFixedBonus<MinDamageAttribute> _minDamageBonus;
        private readonly AttributeDependantFixedBonus<MaxDamageAttribute> _maxDamageBonus;
        protected override void ApplyBonusInternal(Unit unit)
        {
            unit.MinDamage += _minDamageBonus;
            unit.MaxDamage += _maxDamageBonus;
        }

        protected override void RemoveBonusInternal(Unit unit)
        {
            unit.MinDamage -= _minDamageBonus;
            unit.MaxDamage -= _maxDamageBonus;
        }
    }
}