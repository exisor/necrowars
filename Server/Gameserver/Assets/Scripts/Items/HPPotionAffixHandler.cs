using Assets.Scripts.Abilities;
using Common.Resources;
using Ability = Common.DTO.Ability;

namespace Assets.Scripts.Items
{
    public class HPPotionAffixHandler : IItemAffixHandler
    {
        public long Id { get { return 37; } }
        public void ApplyAffix(Army army, IAffixValues affix)
        {
#if ABILITY_ENABLED

            var abilityId = affix.GetValue("Skill");
            var hp = affix.GetValue("HP");
            var ability = (HPPotion)ArmiesPool.Instance.CreateAbility(new Ability {Level = 1, Id = abilityId});
            ability.Heal = hp;
            army.AddAbility(ability);
#endif
        }
    }
}