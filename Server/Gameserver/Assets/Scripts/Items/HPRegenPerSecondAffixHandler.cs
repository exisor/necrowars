using Common.Resources;

namespace Assets.Scripts.Items
{
    public class HPRegenPerSecondAffixHandler : IItemAffixHandler
    {
        public long Id
        {
            get { return 14; }
        }

        public void ApplyAffix(Army army, IAffixValues affix)
        {
            var hp = affix.GetValue("HP");
            army.Leader.HPRegen += hp;
#if EXTENDED_LOGGING
            Logging.Log(this, "Added HPRegen : {0}, Total : {1}", hp, army.Leader.HPRegen);
#endif
        }
    }
}