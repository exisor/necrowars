using Common.Resources;

namespace Assets.Scripts.Items
{
    public class IncreaseMinionAttackSpeedAffixHandler : IItemAffixHandler
    {
        public long Id { get { return 23; } }
        public void ApplyAffix(Army army, IAffixValues affix)
        {
            var aspd = affix.GetValue("AttackSpeed");
            army += new MinionAttackSpeedBonus(aspd);
        }
    }
}