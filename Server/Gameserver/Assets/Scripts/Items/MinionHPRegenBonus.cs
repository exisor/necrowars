using Assets.Scripts.Attributes;

namespace Assets.Scripts.Items
{
    public class MinionHPRegenBonus : MinionsBonus
    {
        private readonly FixedFloatBonus _bonus;

        public MinionHPRegenBonus(float value)
        {
            _bonus = new FixedFloatBonus(value);
        }
        protected override void ApplyBonusInternal(Unit unit)
        {
            unit.HPRegen += _bonus;
        }

        protected override void RemoveBonusInternal(Unit unit)
        {
            unit.HPRegen -= _bonus;
        }
    }
}