using Assets.Scripts.Attributes;

namespace Assets.Scripts.Items
{
    public class MasterArmorToMinion : MinionArmourBonus
    {
        public MasterArmorToMinion(float value, ArmyLeader leader) : base(new AttributeDependantFixedBonus<ArmorAttribute>(value, leader.Armor))
        {
        }

        public MasterArmorToMinion(int percent, ArmyLeader leader) : this(percent / 100f, leader)
        {
        }
    }
}