using System.Linq;
using Assets.Scripts.Abilities;
using Assets.Scripts.Attributes;
using Common.Resources;

namespace Assets.Scripts.Items
{
    public class IncreaseAOERadiusAffixHandler : IItemAffixHandler
    {
        public long Id
        {
            get { return 27; }
        }

        public void ApplyAffix(Army army, IAffixValues affix)
        {
#if ABILITY_ENABLED

            var radius = RelFloatBonus.FromPercent(affix.GetValue("AOE"));
            foreach (var skill in army.ActiveAbilities.OfType<ISkillRadius>().Concat(army.PassiveAbilities.OfType<ISkillRadius>()))
            {
                skill.SkillRadius += radius;
            }
#endif
        }
    }
}