using Common.Resources;

namespace Assets.Scripts.Items
{
    public class MinionDamageOfMasterAffixHandler : IItemAffixHandler
    {
        public long Id
        {
            get { return 21; }
        }

        public void ApplyAffix(Army army, IAffixValues affix)
        {
            var damage = affix.GetValue("Damage");

            army += new MasterDamageToMinion(percent: damage, leader: army.Leader);
        }
    }
}