using Common.Resources;
using Ability = Common.DTO.Ability;

namespace Assets.Scripts.Items
{
    public class CDPotionAffixHandler : IItemAffixHandler
    {
        public long Id { get { return 38; } }
        public void ApplyAffix(Army army, IAffixValues affix)
        {
#if ABILITY_ENABLED

            var abilityId = affix.GetValue("Skill");
            var ability = ArmiesPool.Instance.CreateAbility(new Ability { Level = 1, Id = abilityId });

            army.AddAbility(ability);
#endif
        }
    }
}