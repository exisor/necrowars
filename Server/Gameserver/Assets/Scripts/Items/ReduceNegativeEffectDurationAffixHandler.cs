using Assets.Scripts.Attributes;
using Common.Resources;

namespace Assets.Scripts.Items
{
    public class ReduceNegativeEffectDurationAffixHandler : IItemAffixHandler
    {
        public long Id { get { return 11; } }
        public void ApplyAffix(Army army, IAffixValues affix)
        {
            var duration = RelFloatBonus.FromPercent(- affix.GetValue("Duration"));
            army += new MinionReduceNegativeEffectDurationBonus(duration);
            army.Leader.ReduceNegativeEffectDuration += duration;
        }
    }
}