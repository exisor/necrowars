﻿using System;
using System.Linq;
using Assets.Scripts.Abilities;
using UnityEngine;
using UnityEngine.Networking;
using Random = UnityEngine.Random;

namespace Assets.Scripts
{
    [RequireComponent(typeof(Army))]
    public class ArmyAIController : MonoBehaviour
    {
        private Army _army;

#pragma warning disable 649
        [SerializeField] private float _changeOrderAfterSeconds;
#pragma warning restore 649

        private float _timeSinceLastOrder;
        void Start()
        {
            _army = GetComponent<Army>();
            _army.PlayerName = "NPC " + Random.Range(1, 999);
        }

        void FixedUpdate()
        {
            if (_army.Destroyed)
            {
                return;
            }
            _timeSinceLastOrder += Time.deltaTime;
            if (_timeSinceLastOrder > _changeOrderAfterSeconds)
            {
                _timeSinceLastOrder = 0;

                // get normalized destination
                var newDestination = Vector2.zero.Jitter();
                // increase magnitude by 1000
                OrderMove(newDestination * 1000.0f);
            }
#if ABILITY_ENABLED
            for (var i = 0; i < _army.ActiveAbilities.Count; ++i)
            {
                var activeAbility = _army.ActiveAbilities[i];
                if (activeAbility is Resurrection && _army.Units.Count > 15) continue;
                if (activeAbility.CanCast())
                {
                    var target = FindTarget(activeAbility);
                    if (target.HasValue)
                    {
                        activeAbility.CastToPoint(target.Value.Jitter());
                    }
                }
            }
#endif
        }

        private static readonly Collider2D[] TemporaryTargets = new Collider2D[50];
        private Vector2? FindTarget(ActiveAbility ability)
        {
            var withRange = ability as ISkillRange;
            if (withRange == null) return _army.Leader.pos;

            var hits = Physics2D.OverlapCircleNonAlloc(_army.Leader.pos, withRange.SkillRange, TemporaryTargets,
                Constants.UNITS_LAYER_MASK);
            Vector2? retval = null;
            for (var i = 0; i < hits; ++i)
            {
                var u = TemporaryTargets[i].GetComponent<Unit>();
                if (u.Army != _army && u.Army != null && u.Army.Leader != null && !retval.HasValue)
                {
                    retval = u.Army.Leader.pos;
                }
                TemporaryTargets[i] = null;
            }
            return retval;
        }
        private void OrderMove(Vector2 target)
        {
            _army.OrderMove(target);
        }

        private Vector2 GetHungerVector()
        {
            throw new NotImplementedException();
        }

        private Vector2 GetAfraidVector()
        {
            throw new NotImplementedException();
        }
    }
}
