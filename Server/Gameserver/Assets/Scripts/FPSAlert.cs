﻿
using UnityEngine;
using UnityEngine.Networking;

namespace Assets.Scripts
{
    public static class SERVER
    {
        public static bool OVERLOAD;
    }
    class FPSAlert : MonoBehaviour
    {
        private int frames = 0;
        void Update()
        {
            if (Time.deltaTime > Time.fixedDeltaTime)
            {
                Alert();
            }
            else
            {
                Clear();
            }
        }

        private void Clear()
        {
            if (!SERVER.OVERLOAD) return;
            SERVER.OVERLOAD = false;
            Debug.LogFormat("Server overload passed. Users : {0}, frames : {1}", NetworkServer.connections.Count, frames);
            frames = 0;
        }
        
        private void Alert()
        {
            frames++;
            if (SERVER.OVERLOAD) return;
            SERVER.OVERLOAD = true;
            Debug.LogErrorFormat("Server overload. DeltaTime : {0}", Time.deltaTime);
        }
    }
}
