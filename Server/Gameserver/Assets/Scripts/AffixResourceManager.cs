﻿namespace Assets.Scripts
{
    public class AffixResourceManager : Common.ResourceManager.AffixResourceManager
    {
        protected override string FileName
        {
            get
            {
                return
#if UNITY_EDITOR
                 "Assets/Plugins/" +
#endif
                 base.FileName;
            }
        }
    }

    public class ItemResourceManager : Common.ResourceManager.ItemResourceManager
    {
        protected override string FileName
        {
            get
            {
                return
#if UNITY_EDITOR
                 "Assets/Plugins/" +
#endif
                 base.FileName;
            }
        }
    }

    public class AbilityResourceManager : Common.ResourceManager.AbilityResourceManager
    {
        protected override string FileName
        {
            get
            {
                return
#if UNITY_EDITOR
                 "Assets/Plugins/" +
#endif
                 base.FileName;
            }
        }
    }

    public class ItemSetsResourceManager : Common.ResourceManager.ItemSetsResourceManager
    {
        protected override string FileName
        {
            get
            {
                return
#if UNITY_EDITOR
                 "Assets/Plugins/" +
#endif
                 base.FileName;
            }
        }
    }
}
