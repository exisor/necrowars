﻿
using System.Collections.Generic;
using Assets.Scripts.Effects;

namespace Assets.Scripts
{
    public struct Damage
    {
        private const float ARMOR_VALUE_DELIMETER = 1000.0f;
        public float Amount;
        public readonly DamageType Type;
        public readonly Unit DamageSource;
        public readonly Unit DamageTarget;

        public static float Redirrect(Unit to, Damage damage)
        {
            return to.ReceiveDamage(new Damage(damage.DamageSource, to, DamageType.Unblockable, damage.Amount));
        }
        public static float Unblockable(Unit source, Unit target, float amount)
        {
            return target.ReceiveDamage(new Damage(source, target, DamageType.Unblockable, amount));
        }

        public static float Attack(Unit source, Unit target)
        {
            return target.ReceiveDamage(new Damage(source, target));
        }

        public static float Spell(ArmyLeader caster, Unit target, float amount)
        {
            return target.ReceiveDamage(new Damage(caster, target, DamageType.Spell, amount*caster.SkillDamage));
        }

        public static void Spell(ArmyLeader caster, List<Unit> targets, float amount)
        {
            for (var i = 0; i < targets.Count; ++i)
            {
                Spell(caster, targets[i], amount);
            }
        }

        public Damage(Unit source, Unit target, DamageType type, float amount)
        {
            DamageTarget = target;
            DamageSource = source;
            Type = type;
            Amount = RecountForUnit(DamageTarget, amount, type);
        }

        public Damage(Unit source, Unit target)
        {
            DamageSource = source;
            Type = DamageType.Normal;
            DamageTarget = target;
            Amount = RecountForUnit(DamageTarget, source.RollDamage(), Type);
        }

        private static float MitigateWithArmor(float armorValue, float damage)
        {
            return damage / (armorValue/ARMOR_VALUE_DELIMETER + 1.0f);
        }

        private static float MitigateWithReduceAoe(float value, float damage)
        {
            return damage * value;
        }

        private static float AmplifyDamage(float value, float damage)
        {
            return damage * value;
        }
        private static float RecountForUnit(Unit target, float damage, DamageType type)
        {
            if (type == DamageType.Unblockable) return damage;
            damage = MitigateWithArmor(target.Armor, damage);

            if (type == DamageType.Spell)
            {
                damage = MitigateWithReduceAoe(target.SpellDamageDefence, damage);
            }
            damage = AmplifyDamage(target.AmplifyIncomingDamage, damage);
            return damage;
        }
    }
}
