﻿using System.Linq;
using UnityEngine;

namespace Assets.Scripts
{
    public class NeutralArmySpawner : MonoBehaviour
    {
        [SerializeField] private GameField _gameField;
        [SerializeField] private ArmiesManager _armiesManager;
        [SerializeField] private float _spawnInterval = 3;
        [SerializeField] private int _armyThreshold = 20;
        private float _spawnCooldown;
        void Start()
        {
            _spawnInterval = NecroserverManager.Instance.Config.SpawnRate;
            if (_spawnInterval == 0) return;
            Enumerable.Range(0, 150).ToList().ForEach(_ => Spawn());
        }

        void FixedUpdate()
        {
            if (_spawnInterval == 0) return;
            _spawnCooldown -= Time.deltaTime;
            if (_spawnCooldown < 0.0f && _armiesManager.Armies.Count < _armyThreshold)
            {
                Spawn();
            }
        }

        private void Spawn()
        {
            _spawnCooldown = _spawnInterval;
            if (!SERVER.OVERLOAD)
            _armiesManager.CreateAiArmy();
        }
    }
}
