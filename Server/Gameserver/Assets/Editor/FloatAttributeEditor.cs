using Assets.Scripts.Attributes;
using ClientProtocol;
using UnityEditor;
using UnityEngine;

namespace Assets.Editor
{
    [CustomPropertyDrawer(typeof(FloatAttribute))]
    [CustomPropertyDrawer(typeof(AttackSpeedAttribute))]
    [CustomPropertyDrawer(typeof(ArmorAttribute))]
    [CustomPropertyDrawer(typeof(MinDamageAttribute))]
    [CustomPropertyDrawer(typeof(MaxDamageAttribute))]
    [CustomPropertyDrawer(typeof(MultiplierAttribute))]
    [CanEditMultipleObjects]
    public class FloatAttributeEditor : PropertyDrawer
    {
        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
        {
            EditorGUI.BeginProperty(position, label, property);
            var indent = EditorGUI.indentLevel;
            EditorGUI.indentLevel = 0;
            position = EditorGUI.PrefixLabel(position, GUIUtility.GetControlID(FocusType.Passive), label);
            var rect = new Rect(position.x, position.y, position.width / 2.0f - 5.0f, position.height);
            var currentRect = new Rect(position.x + position.width / 2.0f + 5.0f, position.y, position.width / 2.0f - 5.0f, position.height);

            EditorGUI.PropertyField(rect, property.FindPropertyRelative("_value"), GUIContent.none);
            EditorGUI.PropertyField(currentRect, property.FindPropertyRelative("_currentValue"), GUIContent.none);
            EditorGUI.indentLevel = indent;

            EditorGUI.EndProperty();
        }
    }
    //[CustomPropertyDrawer(typeof(FloatAttributeS))]
    [CanEditMultipleObjects]
    public class FloatAttributeSEditor : PropertyDrawer
    {
        private bool fold;
        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
        {
            const int checkbox_offset = 16;
            var height = base.GetPropertyHeight(property, label);
            EditorGUI.BeginProperty(position, label, property);
            var indent = EditorGUI.indentLevel;
            EditorGUI.indentLevel = 0;

            position = EditorGUI.PrefixLabel(position, GUIUtility.GetControlID(FocusType.Passive), label);
            var rect = new Rect(position.x, position.y, position.width / 2.0f - 5.0f, height);
            EditorGUI.PropertyField(rect, property.FindPropertyRelative("_value"), GUIContent.none);
            if (EditorApplication.isPlaying)
            {
                var currentRect = new Rect(position.x + position.width / 2.0f + 5.0f, position.y, position.width / 2.0f - 5.0f, height);
                EditorGUI.PropertyField(currentRect, property.FindPropertyRelative("_currentValue"), GUIContent.none);
            }

            // ReSharper disable once AssignmentInConditionalExpression
            if (
                fold =
                    EditorGUI.Toggle(
                        new Rect(position.width - checkbox_offset - 2, position.y, checkbox_offset, height),
                        new GUIContent("Restrictions"), fold)) 
            {

                EditorGUI.PropertyField(
                    new Rect(14, position.y + height + 2, checkbox_offset, height),
                    property.FindPropertyRelative("_hasMinValue"), new GUIContent("Min"));
                EditorGUI.PropertyField(
                    new Rect(position.x + checkbox_offset, position.y + height + 2, position.width - checkbox_offset,
                        height),
                    property.FindPropertyRelative("_minValue"), GUIContent.none);
                EditorGUI.PropertyField(
                    new Rect(14, position.y + (height + 2) * 2, checkbox_offset, height),
                    property.FindPropertyRelative("_hasMaxValue"), new GUIContent("Max"));

                EditorGUI.PropertyField(
                    new Rect(position.x + checkbox_offset, position.y + (height + 2) * 2,
                        position.width - checkbox_offset, height),
                    property.FindPropertyRelative("_maxValue"), GUIContent.none);
            }

            if (EditorApplication.isPlaying)
            {
                EditorGUILayout.PropertyField(
                    property.FindPropertyRelative("_bonuses"), new GUIContent("Bonuses"));
            }

            EditorGUI.indentLevel = indent;
            EditorGUI.EndProperty();
        }

        public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
        {
            return !fold ? base.GetPropertyHeight(property, label) : base.GetPropertyHeight(property, label) * 3 + 4;
        }

    }
    //[CustomPropertyDrawer(typeof(FloatBonusS))]
    [CanEditMultipleObjects]
    public class FloatBonusSEditor : PropertyDrawer
    {
        private bool fold;
        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
        {
            var height = base.GetPropertyHeight(property, label);
            EditorGUI.BeginProperty(position, label, property);
            var indent = EditorGUI.indentLevel;
            EditorGUI.indentLevel = 0;

            position = EditorGUI.PrefixLabel(position, GUIUtility.GetControlID(FocusType.Passive), label);
            var rect = new Rect(position.x, position.y, position.width / 2.0f - 5.0f, height);
            EditorGUI.PropertyField(rect, property.FindPropertyRelative("value"), GUIContent.none);

            // ReSharper disable once AssignmentInConditionalExpression
            if (fold = EditorGUILayout.Foldout(fold, new GUIContent("More")))
            {
                EditorGUI.PropertyField(
                    new Rect(14, position.y + height + 2, position.width * 2, height),
                    property.FindPropertyRelative("type"), new GUIContent("Type"));
                EditorGUI.PropertyField(
                    new Rect(14, position.y +(height + 2) * 2, position.width * 2, height), 
                    property.FindPropertyRelative("_priority"), new GUIContent("Priority"));
                EditorGUI.PropertyField(
                    new Rect(position.x, position.y + (height + 2) * 3, position.width * 2 - 8, height), 
                    property.FindPropertyRelative("source"), GUIContent.none);
            }
            EditorGUI.indentLevel = indent;
            EditorGUI.EndProperty();
        }

        public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
        {
            return !fold ? base.GetPropertyHeight(property, label) : base.GetPropertyHeight(property, label) * 4 + 6;
        }
    }
}