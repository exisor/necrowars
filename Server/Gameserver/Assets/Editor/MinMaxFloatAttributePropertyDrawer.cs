using UnityEditor;
using UnityEngine;

namespace Assets.Editor
{
    [CustomPropertyDrawer(typeof(Scripts.Attributes.MinMaxFloatAttribute))]
    [CanEditMultipleObjects]
    public class MinMaxFloatAttributePropertyDrawer : PropertyDrawer
    {
        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
        {
            var height = base.GetPropertyHeight(property, label);
            const int checkbox_offset = 16;

            EditorGUI.BeginProperty(position, label, property);
            var indent = EditorGUI.indentLevel;
            EditorGUI.indentLevel = 0;
            position = EditorGUI.PrefixLabel(position, GUIUtility.GetControlID(FocusType.Passive), label);
            var rect = new Rect(position.x, position.y, position.width / 2.0f - 5.0f, height);
            var currentRect = new Rect(position.x + position.width / 2.0f + 5.0f, position.y, position.width / 2.0f - 5.0f, height);

            EditorGUI.PropertyField(rect, property.FindPropertyRelative("_value"), GUIContent.none);
            EditorGUI.PropertyField(currentRect, property.FindPropertyRelative("_currentValue"), GUIContent.none);
            EditorGUI.PropertyField(
                new Rect(14, position.y + height + 2, checkbox_offset, height),
                property.FindPropertyRelative("_hasMinValue"), new GUIContent("Min"));
            EditorGUI.PropertyField(
                new Rect(position.x + checkbox_offset, position.y + height + 2, position.width - checkbox_offset, height),
                property.FindPropertyRelative("_min"), GUIContent.none);
            EditorGUI.PropertyField(
                new Rect(14, position.y + (height + 2) * 2, checkbox_offset, height),
                property.FindPropertyRelative("_hasMaxValue"), new GUIContent("Max"));
            EditorGUI.PropertyField(
                new Rect(position.x + checkbox_offset, position.y + (height + 2) * 2, position.width - checkbox_offset, height),
                property.FindPropertyRelative("_max"), GUIContent.none);
            EditorGUI.indentLevel = indent;

            EditorGUI.EndProperty();
        }

        public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
        {
            return base.GetPropertyHeight(property, label) * 3 + 4;
        }
    }
}