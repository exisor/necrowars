using Assets.Scripts.Attributes;
using UnityEditor;

namespace Assets.Editor
{
    [CustomPropertyDrawer(typeof (StraightLevelFixedBonus))]
    [CustomPropertyDrawer(typeof(AggroRangeQuantityBonus))]
    [CanEditMultipleObjects]
    public class StraightLevelFixedBonusEditor : ModifierPropertyDrawer
    {
        
    }


}