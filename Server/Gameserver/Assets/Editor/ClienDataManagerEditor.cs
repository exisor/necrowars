﻿#if ABILITY_ENABLED

using UnityEngine;
using System.Collections;
using UnityEditor;
using System.IO;
using System.Xml.Serialization;

[CustomEditor(typeof(ClienDataManager))]
public class ClienDataManagerEditor : Editor
{
    public override void OnInspectorGUI()
    {
        DrawDefaultInspector();

        ClienDataManager data = (ClienDataManager)target;
        if (GUILayout.Button("Parse To XML"))
        {
            File.WriteAllText("../../Client/Assets/Plugins/Resources/abilitiesDescription.xml", data.SaveAbilities());
            AssetDatabase.SaveAssets();
            AssetDatabase.Refresh();
        }
    }
}
#endif