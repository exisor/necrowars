﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Assets.Scripts;
using UnityEditor;
using UnityEngine;

namespace Assets.Editor
{
    [CustomEditor(typeof(ArmyPlayerController))]
    public class KillPlayerButtonEditor : UnityEditor.Editor
    {
        public override void OnInspectorGUI()
        {
            DrawDefaultInspector();

            if (GUILayout.Button("Kill"))
            {
                var army = ((ArmyPlayerController) target).GetComponent<Army>();
                Damage.Unblockable(army.Leader, army.Leader, army.Leader.MaxHP);
            }
        }
    }

    [CustomEditor(typeof(PickupsSpawner))]
    public class SpawnPickupsItemsEditor : UnityEditor.Editor
    {
        public override void OnInspectorGUI()
        {
            DrawDefaultInspector();

            if (GUILayout.Button("Spawn"))
            {
                var spawner = ((PickupsSpawner)target).GetComponent<PickupsSpawner>();
                spawner.Spawn();
            }
        }
    }
}
