using Assets.Scripts.Attributes;
using UnityEditor;

namespace Assets.Editor
{
    [CustomPropertyDrawer(typeof(StraightLevelRelBonus))]
    [CanEditMultipleObjects]
    public class StraightLevelRelBonusEditor : ModifierPropertyDrawer
    {
    }
}