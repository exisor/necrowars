﻿using UnityEngine;
using System.Collections;
using UnityEngine.Networking;

[ExecuteInEditMode]
public class AssetId : MonoBehaviour
{
    public NetworkIdentity obj;
    public string id;

    void Start()
    {
    	obj = GetComponent<NetworkIdentity>();
    }
	// Use this for initialization
	void Update ()
	{
        if (obj != null)
	    id = obj.assetId.ToString();
	}
}
