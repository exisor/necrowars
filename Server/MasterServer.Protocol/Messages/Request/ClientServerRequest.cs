﻿using ProtoBuf;
using Protocol;

namespace MasterServer.Protocol.Messages.Request
{
    [ProtoContract]
    [ProtoSubContract((int)Constants.Messages.ClientServerRequest, Constants.MASTER_SERVER_TAG)]
    public class ClientServerRequest : Message, IClientMessage
    {
        [ProtoMember(1)]
        public string Game { get; set; }
        [ProtoMember(2)]
        public ServerRole Role { get; set; }
    }
}
