﻿using ProtoBuf;
using Protocol;

namespace MasterServer.Protocol.Messages.Request
{
    [ProtoContract]
    [ProtoSubContract((int)Constants.Messages.ServerRegisterRequest, Constants.MASTER_SERVER_TAG)]
    public class ServerRegisterRequest : Message
    {
        [ProtoMember(1)]
        public string GameName { get; set; }
        [ProtoMember(2)]
        public ServerRole Role { get; set; }
        [ProtoMember(3)]
        public string Address { get; set; }
        [ProtoMember(4)]
        public int Port { get; set; }
    }
}
