﻿using ProtoBuf;
using Protocol;

namespace MasterServer.Protocol.Messages.Request
{
    [ProtoContract]
    [ProtoSubContract((int)Constants.Messages.FreeGameServerRequest, Constants.MASTER_SERVER_TAG)]
    public class FreeGameServerRequest : Message
    {
        [ProtoMember(1)]
        public string Game { get; set; }
    }
}
