﻿using System;
using ProtoBuf;
using Protocol;

namespace MasterServer.Protocol.Messages.Event
{
    [ProtoContract]
    [ProtoSubContract((int)Constants.Messages.UserLoggedInEvent, (int)Constants.MASTER_SERVER_TAG )]
    public class UserLoggedInEvent : Message
    { 
        [ProtoMember(1)]
        public long UserId { get; set; }
        [ProtoMember(2)]
        public string Token { get; set; }
        [ProtoMember(3)]
        public string Address { get; set; }
    }
}
