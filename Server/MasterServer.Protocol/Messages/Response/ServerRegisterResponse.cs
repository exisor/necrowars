﻿using ProtoBuf;
using Protocol;

namespace MasterServer.Protocol.Messages.Response
{
    [ProtoContract]
    [ProtoSubContract((int)Constants.Messages.ServerRegisterResponse, Constants.MASTER_SERVER_TAG)]
    public class ServerRegisterResponse : Message
    {
        [ProtoMember(1)]
        public bool Success { get; set; }
    }
}
