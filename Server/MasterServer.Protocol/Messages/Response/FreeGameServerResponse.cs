﻿using ProtoBuf;
using Protocol;

namespace MasterServer.Protocol.Messages.Response
{
    [ProtoContract]
    [ProtoSubContract((int) Constants.Messages.FreeGameServerResponse, Constants.MASTER_SERVER_TAG)]
    public class FreeGameServerResponse : Message
    {
        [ProtoContract]
        public enum Error : byte
        {
            OK = 0,
            GameIsNotRegistered = 1,
            AllServersIsBusy = 2,
        }
        [ProtoMember(1)]
        public string Address { get; set; }
        [ProtoMember(2)]
        public int Port { get; set; }
        [ProtoMember(3)]
        public Error Status { get; set; }
    }
}