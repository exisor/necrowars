﻿
using ProtoBuf;
using Protocol;

namespace MasterServer.Protocol.Messages.Response
{
    [ProtoContract]
    [ProtoSubContract((int)Constants.Messages.ClientServerRequestResponse, Constants.MASTER_SERVER_TAG)]
    public class ClientServerRequestResponse : Message, IClientMessage
    {
        [ProtoMember(1)]
        public string Address { get; set; }
        [ProtoMember(2)]
        public int Port { get; set; }
        [ProtoMember(3)]
        public ClientServerRequestResponseError Error { get; set; }
    }

    [ProtoContract]
    public enum ClientServerRequestResponseError : byte
    {
        OK = 0,
        GameIsNotRegistered = 1,
        ServerIsNotAvailable = 2,
    }
}
