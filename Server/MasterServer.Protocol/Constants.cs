﻿using System.Reflection;

namespace MasterServer.Protocol
{
    public static class Constants
    {
        public const int MASTER_SERVER_TAG = 2;
        public static Assembly PROTOCOL_ASSEMBLY => Assembly.GetAssembly(typeof(Constants));

        public enum Messages : int
        {
            ServerRegisterRequest = 1,
            ServerRegisterResponse = 2,
            ClientServerRequest = 3,
            ClientServerRequestResponse = 4,
            UserLoggedInEvent = 5,
            FreeGameServerRequest = 6,
            FreeGameServerResponse = 7,

        }
    }
}
