using BareBone.Base.Server;
using BareBone.Configuration;
using NSubstitute;
using NUnit.Framework;

namespace BareBone.UnitTest
{
    [TestFixture]
    public class TestServerFactory
    {
        [Test]
        public void ManageServerCreate()
        {
            var configuration = Substitute.For<IServerConfiguration>();
            configuration.Name.Returns("Manage");
            configuration.Type.Returns("ManageServer.Application.ManageServer, ManageServer");
            var factory = new ServerFactory(new ConsoleLogger());
            var server = factory.CreateServer(configuration);
            Assert.NotNull(server);
            Assert.IsAssignableFrom<ManageServer.Application.ManageServer>(server);
        }

        [Test]
        public void TypeNotFoundExceptionCreateServer()
        {
            var configuration = Substitute.For<IServerConfiguration>();
            configuration.Name.Returns("test");
            var factory = new ServerFactory(new ConsoleLogger());
            Assert.Null(factory.CreateServer(configuration));
        }
    }
}