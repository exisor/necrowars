﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BareBone.Base.ApplicationPlatform;
using NUnit.Framework;

namespace BareBone.UnitTest
{

    [TestFixture]
    public class ConfigurationTests
    {
        [Test]
        public async Task PlatformConfigurationLoad()
        {
            using (AppConfig.Change("Configs\\LoadSmokeTest.config"))
            {
                var platform = new ApplicationPlatform();
                await platform.Init();
                var configuration = platform.Configuration;
                Assert.NotNull(configuration);
                Assert.NotNull(configuration.ServerConfigurations);
                platform.Shutdown();
            }
        }

        [Test]
        public async Task ManageServerConfigLoad()
        {
            using (AppConfig.Change("Configs\\ManageServer.config"))
            {
                var platform = new ApplicationPlatform();
                await platform.Init();
                var manageServerConfiguration =
                    platform.Configuration.ServerConfigurations.FirstOrDefault(config => config.Name == "Manage");
                Assert.NotNull(manageServerConfiguration);
                platform.Shutdown();
            }
        }

        [Test]
        public async Task ManageServerCreated()
        {
            using (AppConfig.Change("Configs\\ManageServer.config"))
            {
                var platform = new ApplicationPlatform();
                await platform.Init();
                var manageServer = platform.Servers.FirstOrDefault(serverInfo => serverInfo.Name == "Manage");
                Assert.NotNull(manageServer);
                platform.Shutdown();
            }
        }

        [Test]
        public async Task CreateServer()
        {
            using (AppConfig.Change("Configs\\ManageServer.config"))
            {
                var platform = new ApplicationPlatform();
                await platform.Init();
                await platform.Run();
                platform.CreateServer(platform.Configuration.ServerConfigurations.First(it => it.Name == "Manage")).Wait();
                await platform.RunServer("Manage");
                Assert.AreEqual(1, platform.Servers.Count());
                platform.Shutdown();
            }
        }

        [Test]
        public void ShutdownUnknownServerFail()
        {
            using (AppConfig.Change("LoadSmokeTest.config"))
            {
                var platform = new ApplicationPlatform();
                Assert.IsAssignableFrom(typeof(KeyNotFoundException),
                    Assert.Catch(() => platform.ShutdownServer("Manage")));
            }
        }

        [Test]
        public async Task ShutdownNotRunningServerFail()
        {
            using (AppConfig.Change("Configs\\ManageServer.config"))
            {
                var platform = new ApplicationPlatform();
                await platform.Init();
                Assert.IsAssignableFrom(typeof(InvalidOperationException),
                   Assert.Catch(() => platform.ShutdownServer("Manage")));
            }
        }

        [Test]
        public void RunUnknownServerFail()
        {
            using (AppConfig.Change("LoadSmokeTest.config"))
            {
                var platform = new ApplicationPlatform();
                Assert.IsAssignableFrom(typeof(KeyNotFoundException),
                    Assert.CatchAsync(async () => await platform.RunServer("Manage")));
            }
        }

        [Test]
        public async Task RunUnitializedServer()
        {
            using (AppConfig.Change("Configs\\ManageServer.config"))
            {
                var platform = new ApplicationPlatform();
                await platform.Init();
                await platform.Run();
                Assert.IsAssignableFrom(typeof(InvalidOperationException),
                   Assert.CatchAsync(async () => await platform.RunServer("Manage")));
                platform.Shutdown();
            }
        }
        [Test]
        public async Task ShutdownNamedServerTest()
        {
            using (AppConfig.Change("Configs\\ManageServer.config"))
            {
                var platform = new ApplicationPlatform();
                await platform.Init();
                await platform.Run();
                platform.ShutdownServer("Manage");
                platform.Shutdown();
            }
        }

        [Test]
        public async Task FailedToCreateServerFromConfig()
        {
            using (AppConfig.Change("Configs\\FailedToCreateServer.config"))
            {
                var platform = new ApplicationPlatform();
                await platform.Init();
                await platform.Run();
                Assert.AreEqual(0, platform.Servers.Count());
            }
        }
    }
}