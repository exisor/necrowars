﻿//using System;
//using System.Linq;
//using System.Net.Sockets;
//using System.Security.Cryptography;
//using System.Text;
//using System.Threading.Tasks;
//using BareBone.Base.ApplicationPlatform;
//using BareBone.Base.DAL;
//using BareBone.Base.Serialization;
//using MasterServer.Protocol.Messages.Request;
//using MasterServer.Protocol.Messages.Response;
//using NUnit.Framework;
//using Protocol;
//using Constants = Protocol.Constants;

//namespace BareBone.UnitTest
//{
//    [TestFixture]
//    public class BigIntegrationTests
//    {
//        private readonly IDataProvider _db = new DapperDataProvider(@"Server = 192.168.1.57; Port=5432;Database=SuricateGames;Uid=bitit;Pwd=bititpwd;");
//        private const string USERNAME = "ManagerMasterLoginLobbyChainTest";
//        private const string PASSWORD = "testPassword";
//        [SetUp]
//        public async Task Setup()
//        {
//            var userRepository = new UserRepository(_db);
//            await
//                userRepository.CreateUser(new User
//                {
//                    Password = PASSWORD,
//                    Username = USERNAME
//                });
//        }

//        [Test
//            , Timeout(5000)
//            ]
//        public async Task ManagerMasterLoginLobbyChainTest()
//        {
//            using (AppConfig.Change("Configs\\ManagerMasterLoginLobbyChainTest.config"))
//            {
//                var application = ApplicationPlatform.RunningApplicationPlatform;
//                await application.Init();
//                await application.Run();
//                Assert.AreEqual(4, application.Servers.Count());
//                Assert.IsTrue(application.Servers.All(server => server.Status == ServerStatus.Started));

//                var client = new TcpClient();
//                var masterServer =
//                    application.Servers.First(s => s.Name == "Master")
//                        .Listeners.First(l => l.Type == ListenerType.Client);
//                await client.ConnectAsync(masterServer.Address, masterServer.Port);
//                Assert.True(client.Connected);
//                var stream = client.GetStream();
//                var serializer = new Serializer();
//                serializer.AddMessagesFromAssembly(Constants.PROTOCOL_ASSEMBLY);
//                serializer.AddMessagesFromAssembly(LoginServer.Protocol.Constants.PROTOCOL_ASSEMBLY);
//                serializer.AddMessagesFromAssembly(MasterServer.Protocol.Constants.PROTOCOL_ASSEMBLY);
//                serializer.AddMessagesFromAssembly(LobbyServer.Protocol.Constants.PROTOCOL_ASSEMBLY);
//                // need delays because of server and client sharing same threadpool in test
//                // login server request
//                var loginServerResponse = new ClientServerRequestResponse();
//                var loginServerRequest = new ClientServerRequest {Game = "Bitit", Role = ServerRole.Login};
//                await Task.Delay(100);

//                serializer.Serialize(stream, loginServerRequest);
//                // response
//                serializer.Deserialize(stream, loginServerResponse);
//                if (loginServerResponse.Error == ClientServerRequestResponseError.OK)
//                    Assert.AreEqual(ClientServerRequestResponseError.OK, loginServerResponse.Error);
//                Assert.NotNull(loginServerResponse.Address);
//                Assert.AreNotEqual(0, loginServerResponse.Port);
//                // disconnect and connect to login
//                client.Close();
//                client = new TcpClient();
//                await client.ConnectAsync(loginServerResponse.Address, loginServerResponse.Port);
//                Assert.True(client.Connected);
//                stream = client.GetStream();
//                // send request with username for encryption key
//                var requestEncryptionKey = new ClientLoginRequest {Username = USERNAME};
//                serializer.Serialize(stream, requestEncryptionKey);
//                var encryptionKeyResponse = new ClientLoginResponse();
//                serializer.Deserialize(stream, encryptionKeyResponse);
//                Assert.NotNull(encryptionKeyResponse.Key);
//                // encrypt password
//                var sha = new SHA1CryptoServiceProvider();
//                var passwordBytes = sha.ComputeHash(Encoding.UTF8.GetBytes(PASSWORD));
//                var saltBytes = Convert.FromBase64String(encryptionKeyResponse.Key);
//                var combined = new byte[passwordBytes.Length + saltBytes.Length];
//                Buffer.BlockCopy(saltBytes, 0, combined, 0, saltBytes.Length);
//                Buffer.BlockCopy(passwordBytes, 0, combined, saltBytes.Length, passwordBytes.Length);
//                // sending encrypted password and trying to authorize
//                var authorizeRequest = new ClientAuthorizeRequest
//                {
//                    EncryptedPass = Convert.ToBase64String(sha.ComputeHash(combined)),
//                    Username = USERNAME
//                };
//                serializer.Serialize(stream, authorizeRequest);
//                var authorizeResponse = new ClientAuthorizeResponse();
//                serializer.Deserialize(stream, authorizeResponse);
//                Assert.NotNull(authorizeResponse.Token);
//                Assert.NotNull(authorizeResponse.GameServerAddress);
//                Assert.NotNull(authorizeResponse.GameServerPort);
//                client.Close();
//                client = new TcpClient();
//                await client.ConnectAsync(authorizeResponse.GameServerAddress, authorizeResponse.GameServerPort);
//                stream = client.GetStream();
//                // Connecting to lobby
//                Assert.True(client.Connected);
//                // Authorize on lobby
//                var gameAuthRequest = new AuthorizeUserRequest
//                {
//                    Token = authorizeResponse.Token,
//                    UserId = authorizeResponse.UserId
//                };
//                serializer.Serialize(stream, gameAuthRequest);
//                var gameAuthResponse = new AuthorizeUserResponse();
//                serializer.Deserialize(stream, gameAuthResponse);
//                Assert.AreEqual(AuthorizeUserResponseResult.OK, gameAuthResponse.Result);
//                // getting data

//                client.Close();
//                application.Shutdown();
//            }
//        }
//        [TearDown]
//        public async Task Teardown()
//        {
//            var userRepository = new UserRepository(_db);
//            var user = await userRepository.GetByUsername(USERNAME);
//            await userRepository.Delete(user);
//        }
//    }
//}
