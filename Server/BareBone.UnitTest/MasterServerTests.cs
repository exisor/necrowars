﻿using System;
using System.Linq;
using System.Net.Sockets;
using System.Threading.Tasks;
using BareBone.Base.ApplicationPlatform;
using MasterServer.Protocol;
using MasterServer.Protocol.Messages.Request;
using MasterServer.Protocol.Messages.Response;
using NUnit.Framework;
using ProtoBuf;
using Protocol;

namespace BareBone.UnitTest
{
    [TestFixture]
    public class MasterServerTests
    {
        [Test]
        public async Task ConfigurationTest()
        {
            using (AppConfig.Change("Configs\\MasterServerConfig.config"))
            {
                var platform = new ApplicationPlatform();
                await platform.Init();
                await platform.Run();
                var manageServer = platform.Servers.FirstOrDefault(server => server.Name == "Manage");
                Assert.NotNull(manageServer);
                var masterServer = platform.Servers.FirstOrDefault(server => server.Name == "Master");
                Assert.NotNull(masterServer);
                platform.Shutdown();
            }
        }

        [Test, Timeout(5000)]
        public async Task RegisterLoginServer()
        {
            using (AppConfig.Change("Configs\\MasterServerConfig.config"))
            {
                var platform = ApplicationPlatform.NewApplicationPlatform;
                await platform.Init();
                await platform.Run();
                var client = new TcpClient();
                var config = platform.Servers.First(s => s.Name == "Master");
                var serverListener = config.Listeners.First(l => l.Type == ListenerType.Server);
                await client.ConnectAsync(serverListener.Address, serverListener.Port);
                Assert.True(client.Connected);
                var stream = client.GetStream();
                var serializer = new Base.Serialization.Serializer();
                serializer.AddMessagesFromAssembly(typeof(ServerRegisterRequest).Assembly);
                var authRequest = new ServerRegisterRequest {GameName = "Bitit", Role = ServerRole.Login};
                serializer.Serialize(stream, authRequest);
                var resposne = new ServerRegisterResponse();
                serializer.Deserialize(stream, resposne);
                Assert.True(resposne.Success);
                platform.Shutdown();
                client.Close();
            }
        }
    }
}
