﻿using Bitit.DAL;
using NUnit.Framework;

namespace BareBone.UnitTest
{
    [TestFixture]
    public class DBTest
    {
        [Test]
        public void ConnectionTest()
        {
            var db = new PostgreDataProvider();
            using (var session = db.NewSession())
            {
                Assert.NotNull(session);
            }
        }
    }
}
