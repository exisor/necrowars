﻿//using System.Net.Sockets;
//using System.Threading.Tasks;
//using BareBone.Base.ApplicationPlatform;
//using BareBone.Base.Serialization;
//using LobbyServer.Protocol.Messages.Request;
//using LobbyServer.Protocol.Messages.Response;
//using NUnit.Framework;

//namespace BareBone.UnitTest
//{
//    [TestFixture]
//    public class LobbyTest
//    {
//        [Test,
//            Timeout(5000)]
//        public async Task SmokeTest()
//        {
//            using (AppConfig.Change("Configs\\LobbyTest.config"))
//            {
//                var application = ApplicationPlatform.RunningApplicationPlatform;
//                await application.Init();
//                await application.Run();
//                using (var client = new TcpClient())
//                {
//                    await client.ConnectAsync("127.0.0.1", 2023);
//                    using (var stream = client.GetStream())
//                    {
//                        var serializer = new Serializer();
//                        serializer.AddMessage(typeof (AuthorizeUserRequest));
//                        serializer.AddMessage(typeof (AuthorizeUserResponse));
//                        serializer.Serialize(stream, new AuthorizeUserRequest {Token = "asd", UserId = 0});
//                        var response = new AuthorizeUserResponse();
//                        serializer.Deserialize(stream, response);
//                        Assert.AreEqual(response.Result, AuthorizeUserResponseResult.InvalidUserId);
//                    }
//                }
//                application.Shutdown();
//            }
//        }
//    }
//}
