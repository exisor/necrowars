﻿using System.Collections.Generic;
using System.IO;
using System.Reflection;
using System.Threading.Tasks;
using BareBone.Base.Handlers;
using BareBone.Base.Peer;
using BareBone.Base.Serialization;
using log4net;
using MasterServer.Protocol;
using MasterServer.Protocol.Messages.Request;
using NSubstitute;
using NUnit.Framework;
using ProtoBuf;
using ProtoBuf.Meta;
using Protocol;
using Serializer = ProtoBuf.Serializer;

namespace BareBone.UnitTest
{
    [TestFixture]
    public class SerializationTest
    {
        private class TestMessageCollection : MessageHandlersCollection<IPeer>
        {
            public TestMessageCollection(IEnumerable<IMessageHandler<IPeer>> handlers, ILog log, ISerializer serializer) : base(handlers, log, serializer)
            {
            }
        }

        [ProtoContract]
        [ProtoSubContract(128, 0)]
        public class TestMessage : Message
        {
            [ProtoMember(1)]
            public string TestString { get; set; }
        }

        [Test]
        public async Task TestMessageHandlerCollection()
        {
            var handler = Substitute.For<IMessageHandler<IPeer>>();
            var code = typeof (TestMessage).GetCustomAttribute<ProtoSubContract>().Tag;
            Assert.AreEqual(128<< 4 + 0, code);
            var serializer = new Base.Serialization.Serializer();
            serializer.AddMessage(typeof(TestMessage));
            handler.MessageCode.Returns(code);
            handler.MessageType.Returns(typeof (TestMessage));
            var msg = new TestMessage();
            var bytesBuffer = new byte[1024];
            var ms = new MemoryStream(bytesBuffer);
            serializer.Serialize(ms, msg);
            var collection = new TestMessageCollection(new List<IMessageHandler<IPeer>> {handler}, new ConsoleLogger(), serializer);
            await collection.Handle(bytesBuffer, 0, bytesBuffer[0] + 4, null);
            await handler.Received().Handle(Arg.Any<Message>(), null);
        }

        [Test]
        public void ServerRegisterRequestTest()
        {
            var msg = new ServerRegisterRequest {GameName = "test", Role = ServerRole.Login};
            Serialization.AddMessageContract(msg.GetType());
            var stream = new MemoryStream();
            Serializer.SerializeWithLengthPrefix(stream, msg, PrefixStyle.Fixed32);
            stream.Position = 0;
            var deser = Serializer.DeserializeWithLengthPrefix<ServerRegisterRequest>(stream, PrefixStyle.Fixed32);
            Assert.AreEqual(msg.Role, deser.Role);
            Assert.AreEqual(msg.GameName, deser.GameName);
        }
    }
}
