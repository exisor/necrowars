using System.Collections.Generic;
using System.Linq;
using System.Net.Sockets;
using System.Threading.Tasks;
using BareBone.Base.ApplicationPlatform;
using BareBone.Configuration;
using ManageServer.Protocol.Event;
using ManageServer.Protocol.Request;
using ManageServer.Protocol.Response;
using NSubstitute;
using NUnit.Framework;
using Protocol;

namespace BareBone.UnitTest
{
    [TestFixture]
    public class TestManageServer
    {
        private IServerConfiguration ConfigureManageServer()
        {
            var configuration = Substitute.For<IServerConfiguration>();
            configuration.Name.Returns("Manage");
            var listenerConfig = Substitute.For<IListenerConfiguration>();
            listenerConfig.ListenerType.Returns(ListenerType.Client);
            listenerConfig.Address.Returns("127.0.0.1");
            listenerConfig.Port.Returns(2023);
            configuration.ListenerConfigurations.Returns(new List<IListenerConfiguration> {listenerConfig});
            configuration.Type.Returns("ManageServer.Application.ManageServer, ManageServer");
            configuration.PeerBuferSize.Returns(1024);
            return configuration;
        }


        [Test, Timeout(1000)]
        public async Task TestManageServerPort()
        {
            var config = ConfigureManageServer();
            var server = new ManageServer.Application.ManageServer(config);
            var platform = Substitute.For<IApplicationPlatform>();
            platform.Servers.Returns(new List<IServerInfo> {server.Info});
            server.Platform = platform;
            await server.Init();
            Assert.AreEqual(ServerStatus.Initialized, server.Info.Status);
            await server.Run();
            Assert.AreEqual(ServerStatus.Started, server.Info.Status);
            Assert.AreEqual(0, server.Info.CurrentClients);
            // check port
            var client = new TcpClient();
            await client.ConnectAsync(config.ListenerConfigurations.First().Address, config.ListenerConfigurations.First().Port);
            await Task.Delay(100);
            Assert.AreEqual(true, client.Connected);
            Assert.AreEqual(1, server.Info.CurrentClients);
            server.Shutdown();
            Assert.AreEqual(ServerStatus.Down, server.Info.Status);
        }

        [Test]
        public async Task LoadPlatformTypedConfig()
        {
            using (AppConfig.Change("Configs\\ManageServerTyped.config"))
            {
                var platform = new ApplicationPlatform();
                await platform.Init();
                await platform.Run();
                var manageServer = platform.Servers.FirstOrDefault(server => server.Name == "Manage");
                Assert.NotNull(manageServer);
                platform.Shutdown();
            }
        }


        [Test
            , Timeout(3000)
            ]
        public async Task ManageServerAuthenticate()
        {
            using (AppConfig.Change("Configs\\ManageServerTyped.config"))
            {
                var platform = ApplicationPlatform.RunningApplicationPlatform;
                await platform.Init();
                await platform.Run();
                var client = new TcpClient();
                var config =
                    platform.Configuration.ServerConfigurations.FirstOrDefault(it => it.Name == "Manage") as
                        ManageServerConfiguration;
                Assert.NotNull(config);
                await client.ConnectAsync(config.ListenerConfigurations.First().Address, config.ListenerConfigurations.First().Port);
                Assert.True(client.Connected);
                var stream = client.GetStream();
                var serializer = new Base.Serialization.Serializer();
                serializer.AddMessagesFromAssembly(typeof(ManageAuthRequest).Assembly);
                var requestAuthMesage = new ManageAuthRequest {Password = config.Password};
                serializer.Serialize(stream, requestAuthMesage);
                var authresponsemessage = new AuthResultResponse();
                serializer.Deserialize(stream, authresponsemessage);
                Assert.True(authresponsemessage.Success);
                var statisticMessage = new ServerInfoUpdateEvent();
                serializer.Deserialize(stream,
                    statisticMessage);
                Assert.AreEqual(1, statisticMessage.ServerInfos.Count);
                var stat = statisticMessage.ServerInfos.First();
                var server = platform.Servers.FirstOrDefault(it => it.Name == "Manage");
                Assert.NotNull(server);
                Assert.AreEqual(stat.Id, server.Id);
                foreach (var found in stat.Listeners.Select(listenerInfo => server.Listeners.FirstOrDefault(it => it.Address == listenerInfo.Address &&
                                                                                                                  it.MaxClients == listenerInfo.MaxClients &&
                                                                                                                  it.Port == listenerInfo.Port)))
                {
                    Assert.NotNull(found);
                }
                Assert.AreEqual(1, stat.CurrentClients);
                Assert.AreEqual(stat.PeerBuferSize, server.PeerBuferSize);
                Assert.AreEqual(ServerStatus.Started, stat.Status);

                client.Close();
                platform.Shutdown();
            }
        }
    }
}