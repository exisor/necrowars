﻿using System.Threading.Tasks;
using BareBone.Base.Timer;
using NUnit.Framework;

namespace BareBone.UnitTest
{
    [TestFixture]
    public class TimerTest
    {

        [Test, Timeout(2000)]
        public async Task TestingTimerSkippingFrames()
        {
            var counter = 0;
            var timer = new Timer(()=>
            {
                ++counter;
                Task.Delay(500).Wait();
            }, 3);
            timer.Start();
            await Task.Delay(1000);
            timer.Stop();
            Assert.AreEqual(2, counter);
            Assert.AreEqual(1, timer.SkippedFrames);
        }

        [Test, Timeout(2000)]
        public async Task TestingTimerNormal()
        {
            var counter = 0;
            var timer = new Timer(() =>
            {
                ++counter;
            }, 3);
            timer.Start();
            await Task.Delay(1000);
            timer.Stop();
            Assert.AreEqual(3, counter);
            Assert.AreEqual(0, timer.SkippedFrames);
        }
    }
}
