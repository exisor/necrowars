﻿using System.Collections.Generic;
using System.Net.Sockets;
using System.Reflection;
using System.Threading.Tasks;
using Autofac;
using BareBone.Base.Server;
using BareBone.Configuration;
using NSubstitute;
using NUnit.Framework;
using Protocol;

namespace BareBone.UnitTest
{
    [TestFixture]
    public class ServerBaseTest
    {

        private class InitContainerExceptionServer : ServerBase
        {
            public InitContainerExceptionServer(IServerConfiguration configuration) : base(configuration)
            {
            }

            protected override void MapBindings(ContainerBuilder builder)
            {
                throw new System.NotImplementedException();
            }

#pragma warning disable CS1998
            protected override async Task OnStart()
            {
            }
#pragma warning restore CS1998
            protected override IEnumerable<Assembly> ProtocolAssemblies { get; }
        }
        [Test]
        public void InitContainerException()
        {
            var config = Substitute.For<IServerConfiguration>();
            var server = new InitContainerExceptionServer(config);
            Assert.CatchAsync(server.Init);
        }

        [Test]
        public async Task RunTwiceExceptionFail()
        {
            var config = Substitute.For<IServerConfiguration>();
            var listener = Substitute.For<IListenerConfiguration>();
            config.Name.Returns("test");
            listener.Address.Returns("127.0.0.1");
            listener.MaxClients.Returns(1);
            listener.Port.Returns(2020);
            config.ListenerConfigurations.Returns(new List<IListenerConfiguration> {listener});
            var server1 = new ManageServer.Application.ManageServer(config);
            var server2 = new ManageServer.Application.ManageServer(config);
            await server1.Init();
            await server2.Init();
            await server1.Run();
            Assert.ThrowsAsync<SocketException>(server2.Run); 
            Assert.AreEqual(ServerStatus.Error, server2.Info.Status);
            server1.Shutdown();
            server2.Shutdown();
        }
    }
}
