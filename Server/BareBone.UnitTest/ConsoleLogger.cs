using System;
using log4net;
using log4net.Core;

namespace BareBone.UnitTest
{
    public class ConsoleLogger : ILog
    {
        public ILogger Logger { get; }
        public void Debug(object message)
        {
            throw new System.NotImplementedException();
        }

        public void DebugFormat(string format, params object[] args)
        {
            throw new System.NotImplementedException();
        }

        public void DebugFormat(string format, object arg0)
        {
            throw new System.NotImplementedException();
        }

        public void DebugFormat(string format, object arg0, object arg1)
        {
            throw new System.NotImplementedException();
        }

        public void DebugFormat(string format, object arg0, object arg1, object arg2)
        {
            throw new System.NotImplementedException();
        }

        public void Info(object message)
        {
            throw new System.NotImplementedException();
        }
        

        public void InfoFormat(string format, params object[] args)
        {
            throw new System.NotImplementedException();
        }

        public void InfoFormat(string format, object arg0)
        {
            throw new System.NotImplementedException();
        }

        public void InfoFormat(string format, object arg0, object arg1)
        {
            throw new System.NotImplementedException();
        }

        public void InfoFormat(string format, object arg0, object arg1, object arg2)
        {
            throw new System.NotImplementedException();
        }
      

        public void Warn(object message)
        {
            throw new System.NotImplementedException();
        }
        

        public void WarnFormat(string format, params object[] args)
        {
            throw new System.NotImplementedException();
        }

        public void WarnFormat(string format, object arg0)
        {
            throw new System.NotImplementedException();
        }

        public void WarnFormat(string format, object arg0, object arg1)
        {
            throw new System.NotImplementedException();
        }

        public void WarnFormat(string format, object arg0, object arg1, object arg2)
        {
            throw new System.NotImplementedException();
        }
        

        public void Error(object message)
        {
            Console.Error.WriteLine(message);
        }
        

        public void ErrorFormat(string format, params object[] args)
        {
            Error(string.Format(format, args));
        }

        public void ErrorFormat(string format, object arg0)
        {

            Error(string.Format(format, arg0));
        }

        public void ErrorFormat(string format, object arg0, object arg1)
        {
            Error(string.Format(format, arg0, arg1));
        }

        public void ErrorFormat(string format, object arg0, object arg1, object arg2)
        {
            Error(string.Format(format, arg0, arg1, arg2));
        }
        

        public void Fatal(object message)
        {
            throw new System.NotImplementedException();
        }
        

        public void FatalFormat(string format, params object[] args)
        {
            throw new System.NotImplementedException();
        }

        public void FatalFormat(string format, object arg0)
        {
            throw new System.NotImplementedException();
        }

        public void FatalFormat(string format, object arg0, object arg1)
        {
            throw new System.NotImplementedException();
        }

        public void FatalFormat(string format, object arg0, object arg1, object arg2)
        {
            throw new System.NotImplementedException();
        }
        
        public bool IsDebugEnabled { get; }
        public bool IsInfoEnabled { get; }
        public bool IsWarnEnabled { get; }
        public bool IsErrorEnabled { get; }
        public bool IsFatalEnabled { get; }

        public void FatalFormat(IFormatProvider provider, string format, params object[] args)
        {
            throw new System.NotImplementedException();
        }

        public void Fatal(object message, Exception exception)
        {
            throw new System.NotImplementedException();
        }

        public void ErrorFormat(IFormatProvider provider, string format, params object[] args)
        {
            throw new System.NotImplementedException();
        }

        public void Error(object message, Exception exception)
        {
            Console.Error.WriteLine(message);
            Console.Error.WriteLine("{0}\n{1}", exception.Message, exception.StackTrace);
        }

        public void WarnFormat(IFormatProvider provider, string format, params object[] args)
        {
            throw new System.NotImplementedException();
        }

        public void Warn(object message, Exception exception)
        {
            throw new System.NotImplementedException();
        }

        public void InfoFormat(IFormatProvider provider, string format, params object[] args)
        {
            throw new System.NotImplementedException();
        }

        public void Info(object message, Exception exception)
        {
            throw new System.NotImplementedException();
        }

        public void DebugFormat(IFormatProvider provider, string format, params object[] args)
        {
            throw new System.NotImplementedException();
        }

        public void Debug(object message, Exception exception)
        {
            throw new System.NotImplementedException();
        }
    }
}