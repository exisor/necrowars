﻿using System;
using System.Collections.Generic;
using System.Linq;
using ClientProtocol;
using Common;

namespace MatchmakerServer
{
    public class ServerBalancer
    {
        private readonly Dictionary<int, ServersList> _servers = new Dictionary<int, ServersList>();
        private readonly Dictionary<int, ServersList> _unityClientServers = new Dictionary<int, ServersList>();
        public void Register(Session session)
        {
            ServersList val;
            var targetContainer = session.Specs.Contains(BattleSpecEnum.UnityClient) ? _unityClientServers : _servers;
            if (!targetContainer.TryGetValue(session.Level, out val))
            {
                val = new ServersList();
                targetContainer[session.Level] = val;
            }
            val.Add(session);
        }
        public Session GetServerForLevel(int level, List<BattleSpecEnum> specs)
        {
            var targetContainer = specs.Contains(BattleSpecEnum.UnityClient) ? _unityClientServers : _servers;
            if (targetContainer.Count == 0) return null;
            var ordered = targetContainer.OrderBy(t => t.Key);
            foreach (var serversList in ordered)
            {
                if (serversList.Key >= level) return serversList.Value.Get();
            }
            return ordered.Last().Value.Get();
        }
    }

    public class ServersList
    {
        private readonly List<Session> _sessions = new List<Session>();
        public void Add(Session info)
        {
            info.Disconnected += InfoOnDisconnected;
            info.UsersChanged += InfoOnUsersChanged;
            _sessions.Add(info);
            Sort();
        }

        private void InfoOnUsersChanged(Session session, int i)
        {
            Sort();
        }

        private void InfoOnDisconnected(Session session)
        {
            session.Disconnected -= InfoOnDisconnected;
            session.UsersChanged -= InfoOnUsersChanged;
            _sessions.Remove(session);
            Sort();
        }

        private void Sort()
        {
            _sessions.Sort((info1, info2) => info1.Users == info2.Users ? 0 :
            info1.Users > info2.Users ? 1 : -1);
        }

        private int counter;
        public Session Get()
        {
            if (_sessions.Count == 0) return null;
            return _sessions[Math.Abs(counter++)%_sessions.Count];
        }
    }
}
