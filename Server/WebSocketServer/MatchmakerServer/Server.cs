﻿using System;
using System.Net;
using Common;
using ServerCommon;
using SuperSocket.Common;

namespace MatchmakerServer
{
    public class Server : ServerBase<Session, Server>
    {
        private const string IP = "127.0.0.1";
        private const int PORT = 2021;
        public override EndPoint MasterEndPoint => new IPEndPoint(IPAddress.Parse(IP), PORT);
        private EndPoint _clientEndPoint;
        public override EndPoint ClientEndPoint => _clientEndPoint;
        protected override ServerRole Role => ServerRole.Matchmaker;
        protected override string Game =>
#if DEBUG
            "Necrowars-test";
#else
            "Necrowars";
#endif
        public ServerBalancer Balancer { get; }= new ServerBalancer();
        public override bool Start()
        {
            var endpointStrings = Config.Options.GetValue("broadcastAddress").Split(new[] { ":" }, StringSplitOptions.None);
            _clientEndPoint =
                CreateEndpoint(new EndPointInfo {address = endpointStrings[0], port = int.Parse(endpointStrings[1])});
            return base.Start();
        }
    }
}
