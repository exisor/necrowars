﻿
using Common;
using SuperSocket.SocketBase.Command;
using SuperSocket.SocketBase.Protocol;

namespace MatchmakerServer.S2S
{
    public class BattleEndedForPlayer : StringCommandBase<Session>
    {
        public override void ExecuteCommand(Session session, StringRequestInfo requestInfo)
        {
            var message = ProtobufSerializer.Deserialize<ServerProtocol.BattleEndedForPlayer>(requestInfo.Body);
            var lobby = session.CharacterLobbys[message.CharacterId];
            lobby.Users--;
            lobby.Send(ProtobufSerializer.MakeStringMessage(message));
        }
    }
}
