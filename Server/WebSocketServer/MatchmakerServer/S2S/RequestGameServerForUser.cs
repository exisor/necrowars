﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using ClientProtocol;
using Common;
using Common.DTO;
using SuperSocket.SocketBase.Command;
using SuperSocket.SocketBase.Protocol;

namespace MatchmakerServer.S2S
{
    public class RequestGameServerForUser : StringCommandBase<Session>
    {
        private readonly RandomNumberGenerator _rng = new RNGCryptoServiceProvider();
        public override void ExecuteCommand(Session session, StringRequestInfo requestInfo)
        {
            var message = ProtobufSerializer.Deserialize<ServerProtocol.RequestGameServerForUser>(requestInfo.Body);
            session.Logger.InfoFormat("Requesting server for spec : {0}, and level : {1}", string.Concat(message.Specs.Select(s => s.ToString())), message.SelectedCharacter.Level);
            var gameServer = session.Server.Balancer.GetServerForLevel(message.SelectedCharacter.Level, message.Specs);
            if (gameServer == null)
            {
                session.Logger.InfoFormat("Gameserver is not found");
                session.Send(ProtobufSerializer.MakeStringMessage(new ClientProtocol.Error
                {
                    Cmd = GetType().Name,
                    Message = ErrorCode.ServerIsNotAvailable,
                }));
                return;
            }
            var token = GetUniqueToken();
            gameServer.UserLobbys[message.SelectedCharacter.UserId] = session;

            gameServer.CharacterLobbys[message.SelectedCharacter.Id] = session;
            session.Logger.InfoFormat("Token generated : {0}", token);
            gameServer.Send(ProtobufSerializer.MakeStringMessage(new ServerProtocol.UserEnteringBattleEvent
            {
                Character = message.SelectedCharacter,
                Token = token,
                PlayerName = message.PlayerName,
                Items = message.Items
            }));
            session.Logger.Info("UserEnteringBattle");

            session.Send(ProtobufSerializer.MakeStringMessage(new ToBattleEvent
            {
                Address = gameServer.Address,
                CharacterId = message.SelectedCharacter.Id,
                BattleToken = token,
                Port = gameServer.Port,
            }));

            session.Logger.Info("ToBattleEvent");
        }

        private string GetUniqueToken()
        {
            var bytes = new byte[32];
            _rng.GetBytes(bytes);
            return Convert.ToBase64String(bytes);
        }
    }
}
