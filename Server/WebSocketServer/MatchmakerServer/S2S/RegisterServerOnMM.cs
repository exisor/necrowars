﻿
using System.Linq;
using Common;
using SuperSocket.SocketBase.Command;
using SuperSocket.SocketBase.Protocol;

namespace MatchmakerServer.S2S
{
    public class RegisterServerOnMM : StringCommandBase<Session>
    {
        public override void ExecuteCommand(Session session, StringRequestInfo requestInfo)
        {
            var message = ProtobufSerializer.Deserialize<ServerProtocol.RegisterServerOnMM>(requestInfo.Body);
            session.Level = message.Level;
            session.Specs = message.Specs.ToList();
            session.Address = message.Address;
            session.Port = message.Port;
            session.Logger.InfoFormat("Registering : {0}:{1} Level : {2}. Specs: {3}", session.Address, session.Port, session.Level, string.Concat(session.Specs.Select(s => s.ToString())));
            session.Server.Balancer.Register(session);
            session.IsGame = true;
        }
    }
}
