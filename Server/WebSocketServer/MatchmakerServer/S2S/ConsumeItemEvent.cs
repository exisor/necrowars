﻿using Common;
using SuperSocket.SocketBase.Command;
using SuperSocket.SocketBase.Protocol;

namespace MatchmakerServer.S2S
{
    public class ConsumeItemEvent : StringCommandBase<Session>
    {
        public override void ExecuteCommand(Session session, StringRequestInfo requestInfo)
        {
            var message = ProtobufSerializer.Deserialize<ServerProtocol.ConsumeItemEvent>(requestInfo.Body);
            session.UserLobbys[message.PlayerId].Send(ProtobufSerializer.MakeStringMessage(message));
        }
    }
}
