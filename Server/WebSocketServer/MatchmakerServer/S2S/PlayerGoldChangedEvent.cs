﻿using Common;
using SuperSocket.SocketBase.Command;
using SuperSocket.SocketBase.Protocol;

namespace MatchmakerServer.S2S
{
    public class PlayerGoldChangedEvent : StringCommandBase<Session>
    {
        public override void ExecuteCommand(Session session, StringRequestInfo requestInfo)
        {
            var message = ProtobufSerializer.Deserialize<ServerProtocol.PlayerGoldChangedEvent>(requestInfo.Body);
            session.UserLobbys[message.PlayerId].Send(ProtobufSerializer.MakeStringMessage(message));
        }
    }
}
