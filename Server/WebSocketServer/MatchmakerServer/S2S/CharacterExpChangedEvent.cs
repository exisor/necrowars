﻿using Common;
using SuperSocket.SocketBase.Command;
using SuperSocket.SocketBase.Protocol;

namespace MatchmakerServer.S2S
{
    public class CharacterExpChangedEvent : StringCommandBase<Session>
    {
        public override void ExecuteCommand(Session session, StringRequestInfo requestInfo)
        {
            var message = ProtobufSerializer.Deserialize<ServerProtocol.CharacterExpChangedEvent>(requestInfo.Body);
            session.CharacterLobbys[message.CharacterId].Send(ProtobufSerializer.MakeStringMessage(message));
        }
    }
}
