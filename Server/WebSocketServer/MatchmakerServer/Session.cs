﻿using System;
using System.Collections.Generic;
using Common;
using SuperSocket.SocketBase;
using SuperSocket.SocketBase.Protocol;

namespace MatchmakerServer
{
    public class Session : AppSession<Session>
    {
        public Server Server => (Server) AppServer;
        private int _users;
        public string Address { get; set; }
        public int Port { get; set; }
        public int Level { get; set; }

        public readonly Dictionary<long, Session> CharacterLobbys = new Dictionary<long, Session>();
        public readonly Dictionary<long, Session> UserLobbys = new Dictionary<long, Session>();
        public bool IsGame { get; set; }
        public List<BattleSpecEnum> Specs { get; set; }
        public int Users
        {
            get { return _users;
            }
            set
            {
                if (value == _users) return;
                _users = value;
                UsersChanged?.Invoke(this, value);
                Logger.Info($"Users changed on server : {Address}:{Port}. Users : {_users}");
            }
        }

        public event Action<Session> Disconnected;
        public event Action<Session, int> UsersChanged;
        protected override void OnSessionClosed(CloseReason reason)
        {
            Disconnected?.Invoke(this);
            base.OnSessionClosed(reason);
        }

        protected override void HandleUnknownRequest(StringRequestInfo requestInfo)
        {
        }
    }
}
