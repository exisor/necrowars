﻿
using System;
using System.IO;
using System.Net;
using ClientProtocol;
using Common;
using LoginServer.Data;
using Newtonsoft.Json.Linq;
using SuperSocket.SocketBase.Protocol;
using SuperSocket.WebSocket.SubProtocol;

namespace LoginServer.Commands
{
    public class OGZLoginRequest : SubCommandBase<WebSession>
    {
        // PLACEHOLDER CHECK IT
        private const string AUTHORIZE_FORMAT = "https://gateway.overgamez.com/api/apps/55/users/current/?access_token={0}";

        public override async void ExecuteCommand(WebSession session, SubRequestInfo requestInfo)
        {
            try
            {
                var message = ProtobufSerializer.Deserialize<ClientProtocol.OGZLoginRequest>(requestInfo.Body);

                var request =
                    WebRequest.CreateHttp(
                        new Uri(string.Format(AUTHORIZE_FORMAT, message.SessionKey)));
                request.Method = "GET";

                JObject jobj;

                using (var restResponse = await request.GetResponseAsync())
                using (var stream = restResponse.GetResponseStream())
                using (var reader = new StreamReader(stream))
                {
                    var responseData = reader.ReadToEnd();
                    jobj = JObject.Parse(responseData);
                }

                var success = (bool) jobj["success"];
                if (!success)
                {
                    session.Send(ProtobufSerializer.MakeStringMessage(new Error(this, ErrorCode.AccessDenied)));
                    session.Close();
                    return;
                }
                var jdata = jobj["data"];
                
                var uid = (long)jdata["uid"];
                if (uid != message.Id)
                {
                    session.Send(ProtobufSerializer.MakeStringMessage(new Error(this, ErrorCode.AccessDenied)));
                    session.Close();
                    return;
                }
                var userId = "OGZ_" + uid;
                var user = await session.Server.UserRepository.GetByUsername(userId);
                if (user == null)
                {
                    // create user
                    user = new User { Password = "", Username = userId };
                    await session.Server.UserRepository.CreateUser(user);
                }
                var sessionKey = message.SessionKey;
                var response = new UserAuthorizeResponse
                {
                    Id = user.Id,
                    SessionKey = sessionKey
                };
                var ev = new ServerProtocol.UserLoggedIn
                {
                    Game = Server.GameName,
                    Id = user.Id,
                    SessionKey = sessionKey,
                    OGZId = uid,
                };

                session.Logger.Info($"New user logged in. Game : {ev.Game} Id : {ev.Id}, SessionKey : {sessionKey}, KongId : {ev.KongregateId}, OGZId : {ev.OGZId}");
                session.Server.S2S.MasterConnection.Send(ProtobufSerializer.MakeStringMessage(ev));

                session.Send(ProtobufSerializer.MakeStringMessage(response));
                session.Close();
            }
            catch (Exception e)
            {
                session.Logger.Error($"{e.Message} {e.StackTrace}");
            }
        }
    }
}
