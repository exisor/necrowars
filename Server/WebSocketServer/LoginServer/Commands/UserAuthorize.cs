﻿using ClientProtocol;
using Common;
using SuperSocket.WebSocket.SubProtocol;

namespace LoginServer.Commands
{
    public class UserAuthorize : SubCommandBase<WebSession>
    {
        public override async void ExecuteCommand(WebSession session, SubRequestInfo requestInfo)
        {
            var message = ProtobufSerializer.Deserialize<ClientProtocol.UserAuthorize>(requestInfo.Body);
            var sessionKey = await session.Server.Auth.Authorize(message.Username, message.EncryptedPassword);
            if (sessionKey == null)
            {
                session.Send(ProtobufSerializer.MakeStringMessage(new Error(this, ErrorCode.AccessDenied)));
                session.Close();
                return;
            }
            session.UserId = (await session.Server.UserRepository.GetByUsername(message.Username)).Id;
            var response = new UserAuthorizeResponse
            {
                Id = session.UserId,
                SessionKey = sessionKey
            };

            session.Server.S2S.MasterConnection.Send(ProtobufSerializer.MakeStringMessage(new ServerProtocol.UserLoggedIn
            {
                Game = Server.GameName,
                Id = session.UserId,
                SessionKey = sessionKey,
            }));

            session.Send(ProtobufSerializer.MakeStringMessage(response));
            session.Close();
        }
    }
}
