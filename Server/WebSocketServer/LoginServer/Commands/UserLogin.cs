﻿using ClientProtocol;
using Common;
using SuperSocket.SocketBase;
using SuperSocket.WebSocket.SubProtocol;

namespace LoginServer.Commands
{
    public class UserLogin : SubCommandBase<WebSession>
    {
        public override async void ExecuteCommand(WebSession session, SubRequestInfo requestInfo)
        {
            var message = ProtobufSerializer.Deserialize<ClientProtocol.UserLogin>(requestInfo.Body);
            var key = await session.Server.Auth.RequestKey(message.Username);
            if (key == null)
            {
                session.Send(ProtobufSerializer.MakeStringMessage(new Error(this, ErrorCode.AccessDenied)));
                session.Close(CloseReason.ApplicationError);
                return;
            }
            session.Send(ProtobufSerializer.MakeStringMessage(new ClientProtocol.UserLoginResponse
            {
                Key = key
            }));
        }
    }
}
