﻿using System;
using System.IO;
using System.Net;
using System.Net.Security;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using ClientProtocol;
using Common;
using LoginServer.Data;
using Newtonsoft.Json.Linq;
using ServerProtocol;
using SuperSocket.WebSocket.SubProtocol;

namespace LoginServer.Commands
{
    public class KongregateAuthorizeRequest : SubCommandBase<WebSession>
    {
        private readonly Uri _authorizeUri = new Uri("https://api.kongregate.com/api/authenticate.json");
        private const string API_KEY = "9d984ea4-f48c-4b7a-86c3-2d170361c6b2";

        private const string REQUEST_FORMAT = @"{{""api_key"":""{0}"",""user_id"":{1},""game_auth_token"":""{2}""}}";
        public KongregateAuthorizeRequest()
        {
            ServicePointManager.ServerCertificateValidationCallback = AcceptAllCerts;
        }

        private bool AcceptAllCerts(object sender, X509Certificate certificate, X509Chain chain,
            SslPolicyErrors sslPolicyErrors)
        {
            return true;
        }

        #region Overrides of SubCommandBase<WebSession>

        public override async void ExecuteCommand(WebSession session, SubRequestInfo requestInfo)
        {
            try
            {
                var message = ProtobufSerializer.Deserialize<ClientProtocol.KongregateAuthorizeRequest>(requestInfo.Body);

                session.Info($"KongregateAuthorizeRequest : userId : {message.UserId}, sessionKey : {message.Token}");

                var request = WebRequest.CreateHttp(_authorizeUri);
                request.ContentType = "application/json";
                request.Method = "POST";
                request.AllowWriteStreamBuffering = true;
                var requestString = MakeRequestString(message.UserId, message.Token);
                var data = Encoding.ASCII.GetBytes(requestString);
                session.Info($"Request string : {requestString}. Data : {data}");
                request.ContentLength = data.Length;
                using (var stream = request.GetRequestStream())
                {
                    stream.Write(data, 0, data.Length);
                }
                using (var restResponse = await request.GetResponseAsync())
                using (var stream = restResponse.GetResponseStream())
                using (var reader = new StreamReader(stream))
                {
                    var responseData = reader.ReadToEnd();
                    session.Info($"Response data : {responseData}");
                    var jobj = JObject.Parse(responseData);
                    var success = (bool)jobj["success"];
                    session.Info($"Success : {success}");
                    if (success)
                    {
                        // authorized
                        var username = jobj["username"]; // not needed atm

                        // transform this to db_username with kong prefix
                        var kongId = (long) jobj["user_id"];
                        var userId = "kong_" + kongId; 
                        var user = await session.Server.UserRepository.GetByUsername(userId);
                        if (user == null)
                        {
                            // create user
                            user = new User {Password = "", Username = userId};
                            await session.Server.UserRepository.CreateUser(user);
                        }
                        var sessionKey = message.Token;
                        var response = new UserAuthorizeResponse
                        {
                            Id = user.Id,
                            SessionKey = sessionKey
                        };
                        var ev = new ServerProtocol.UserLoggedIn
                        {
                            Game = Server.GameName,
                            Id = user.Id,
                            SessionKey = sessionKey,
                            KongregateId = kongId,
                        };

                        session.Logger.Info($"New user logged in. Game : {ev.Game} Id : {ev.Id}, SessionKey : {sessionKey}, KongId : {ev.KongregateId}");
                        session.Server.S2S.MasterConnection.Send(ProtobufSerializer.MakeStringMessage(ev));

                        session.Send(ProtobufSerializer.MakeStringMessage(response));
                        session.Close();
                    }
                    else
                    {
                        session.Send(ProtobufSerializer.MakeStringMessage(new Error(this, ErrorCode.AccessDenied)));
                        session.Info($"ErrorCode : {jobj["error"]}. Error : {jobj["error_description"]}");
                        session.Close();
                    }
                }
            }
            catch (Exception e)
            {
                session.Logger.Error($"{e.Message} {e.StackTrace}");
            }
        }

        #endregion

        private string MakeRequestString(long userId, string token)
        {
            return string.Format(REQUEST_FORMAT, API_KEY, userId, token);
        }
    }
}
