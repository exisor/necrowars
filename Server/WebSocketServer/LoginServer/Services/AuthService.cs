using System;
using System.Collections.Concurrent;
using System.Security.Cryptography;
using System.Threading.Tasks;
using LoginServer.Data;

namespace LoginServer.Logic.Auth
{
    public class AuthService : IAuthService
    {
        private readonly IUserRepository _userRepository;
        private readonly ConcurrentDictionary<string, string> _pendingUsers = new ConcurrentDictionary<string, string>();
        private readonly SHA1 _sha = new SHA1CryptoServiceProvider();
        private readonly RandomNumberGenerator _rng = new RNGCryptoServiceProvider();

        public AuthService(IUserRepository userRepository)
        {
            _userRepository = userRepository;
        }

        public async Task<string> RequestKey(string username)
        {
            var user = await _userRepository.GetByUsername(username);
            if (user == null) return null;
            var retval = GenerateUnique();
            _pendingUsers[username] = retval;
            return retval;
        }

        public async Task<string> Authorize(string username, string encryptedPassword)
        {
            string pending;
            if (!_pendingUsers.TryRemove(username, out pending))
            {
                return null;
            }
            var user = await _userRepository.GetByUsername(username);

            // if user password is empty then it should use automatic authorizations using host services
            if (string.IsNullOrEmpty(user.Password)) return null;

            var pendingBytes = Convert.FromBase64String(pending);
            var passwordBytes = Convert.FromBase64String(user.Password);
            var combined = new byte[pendingBytes.Length + passwordBytes.Length];
            Buffer.BlockCopy(pendingBytes, 0, combined, 0, pendingBytes.Length);
            Buffer.BlockCopy(passwordBytes, 0, combined, pendingBytes.Length, passwordBytes.Length);
            var hashwithsalt = _sha.ComputeHash(combined);
            if (!encryptedPassword.Equals(Convert.ToBase64String(hashwithsalt)))
            {
                return null;
            }
            return GenerateUnique();
        }

        private string GenerateUnique()
        {
            var bytes = new byte[32];
            _rng.GetBytes(bytes);
            return Convert.ToBase64String(bytes);
        }
    }
}