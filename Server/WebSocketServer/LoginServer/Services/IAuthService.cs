﻿using System.Threading.Tasks;

namespace LoginServer.Logic.Auth
{
    public interface IAuthService
    {
        Task<string> RequestKey(string username);
        Task<string> Authorize(string username, string encryptedPassword);
    }
}
