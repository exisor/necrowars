﻿using DAL;
using LoginServer.Data;
using LoginServer.Logic.Auth;
using SuperSocket.SocketBase;
using SuperSocket.WebSocket;

namespace LoginServer
{
    public class WebServer : WebSocketServer<WebSession>
    {
        public const string DB_CONNECTION_STRING = @"Server=192.168.0.104;Port=5432;Database=Login;Uid=exisor;Pwd=Zaebali1;";

        public IAuthService Auth { get; private set; }
        public IUserRepository UserRepository { get; private set; }
        public Server S2S { get; private set; }
        protected override void OnStarted()
        {
            base.OnStarted();
            S2S = (Server)Bootstrap.GetServerByName("Login Server (Tcp)");

            var connectionString = Config.Options.Get("connection");
            if (string.IsNullOrEmpty(connectionString))
            {
                connectionString = DB_CONNECTION_STRING;
            }

            var dataProvider = new DapperDataProvider(connectionString);

            UserRepository = new UserRepository(dataProvider);
            Auth = new AuthService(UserRepository);

            // REST Server Callbacks


        }
    }
}