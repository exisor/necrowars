﻿using SuperSocket.WebSocket;

namespace LoginServer
{
    public class WebSession : WebSocketSession<WebSession>
    {
        public WebServer Server => (WebServer) AppServer;

        public long UserId { get; set; }

        public void Info(string data)
        {
            Logger.Info(data);
        }
    
    }
}