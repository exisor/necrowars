﻿using System;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using Dapper.FastCrud;
using DAL;

namespace LoginServer.Data
{
    public class UserRepository : CrudRepository<User>, IUserRepository
    {
        public UserRepository(IDataProvider dataProvider) :
            base(dataProvider)
        { }

        public async Task<User> GetByUsername(string username)
        {
            User retval = null;
            await DataProvider.Transaction(async _ =>
            {
                retval = (await _.FindAsync<User>(statement => statement
                    .Where($"{nameof(User.Username):C}=@Username")
                    .WithParameters(new User { Username = username }))).FirstOrDefault();
            });
            return retval;
        }

        public async Task<bool> CreateUser(User user)
        {
            var exist = await GetByUsername(user.Username);
            if (exist != null) return false;
            var passwordBytes = Encoding.UTF8.GetBytes(user.Password);
            var sha = new SHA1CryptoServiceProvider();
            user.Password = Convert.ToBase64String(sha.ComputeHash(passwordBytes));
            await Insert(user);
            return true;
        }
    }
}