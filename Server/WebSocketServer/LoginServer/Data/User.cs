﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using DAL;

namespace LoginServer.Data
{

    [Table("users")]
    public partial class User : IIdentifiedData
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Column("id")]
        public virtual long Id { get; set; }
        [Column("username")]
        public virtual string Username { get; set; }
        [Column("password")]
        public virtual string Password { get; set; }
    }
}
