﻿using System.Threading.Tasks;
using DAL;

namespace LoginServer.Data
{
    public interface IUserRepository : ICrudRepository<User>
    {
        Task<User> GetByUsername(string username);
        Task<bool> CreateUser(User user);
    }
}
