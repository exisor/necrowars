﻿using System;
using System.Net;
using Common;
using ServerCommon;
using SuperSocket.ClientEngine;
using SuperSocket.SocketBase;

namespace LoginServer
{
    public class Server : ServerBase<Session, Server>
    {
        private const string IP = "127.0.0.1";
        private const int PORT = 2021;

        public const string GameName =
#if DEBUG
            "Necrowars-test";
#else
            "Necrowars";
#endif
        public Session MasterConnection => MasterSession;

        public WebServer WebServer { get; private set; }

        public override EndPoint MasterEndPoint => new IPEndPoint(IPAddress.Parse(IP), PORT);
        private EndPoint _clientEndPoint;
        public override EndPoint ClientEndPoint => _clientEndPoint;
        protected override ServerRole Role => ServerRole.Login;
        protected override string Game => GameName;

        protected override void OnStarted()
        {
            var endpointStrings = Config.Options.GetValue("broadcastAddress").Split(new[] { ":" }, StringSplitOptions.None);
            _clientEndPoint = CreateEndpoint(new EndPointInfo {port = int.Parse(endpointStrings[1]) ,address = endpointStrings[0] });
            WebServer = (WebServer)Bootstrap.GetServerByName("Login Server (Web)");
            base.OnStarted();
        }
    }
}
