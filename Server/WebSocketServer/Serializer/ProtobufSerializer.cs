﻿using System;
using System.IO;
using System.Text;
using ProtoBuf;

namespace Common
{
    public static class ProtobufSerializer
    {
        public const string PROTOCOL_DELIMETER = "\r\n";

        public static string Serialize<T>(T data)
        {
            return Convert.ToBase64String(SerializeBinary(data));
        }

        public static byte[] SerializeBinary<T>(T data)
        {
            using (var ms = new MemoryStream())
            {
                Serializer.Serialize(ms, data);
                return ms.ToArray();
            }
        }

        public static T Deserialize<T>(string data)
        {
            return Deserialize<T>(Convert.FromBase64String(data));
        }

        public static T Deserialize<T>(byte[] data)
        {
            using (var ms = new MemoryStream(data))
            {
                return Serializer.Deserialize<T>(ms);
            }
        }

        public static object Deserialize(Type type, string data)
        {
            using (var ms = new MemoryStream(Convert.FromBase64String(data)))
            {
                return Serializer.Deserialize(type, ms);
            }
        }
        public static byte[] MakeMessage<T>(T message)
        {
            return Encoding.ASCII.GetBytes(MakeStringMessage(message));
        }

        public static string MakeStringMessage<T>(T message)
        {
            return $"{typeof (T).Name} {Serialize(message)}{PROTOCOL_DELIMETER}";
        }

        public static void Merge<T>(byte[] data, T target)
        {
            if (data == null) return;
            using (var ms = new MemoryStream(data))
            {
                Serializer.Merge(ms, target);
            }
        }
    }
}
