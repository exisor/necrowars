﻿using NUnit.Framework;
using System.Data;
using System.Threading.Tasks;
using DAL;
using LoginServer.Data;

namespace LoginServer.Test
{
    [TestFixture]
    public class TestClass
    {
        [Test]
        public void DB()
        {
            var db = new DapperDataProvider(WebServer.DB_CONNECTION_STRING);
            using (var connection = db.Connect())
            {
                connection.Open();
                Assert.AreEqual(ConnectionState.Open, connection.State);
            }
        }

        //[Test]
        public async Task CreateUser()
        {
            var db = new DapperDataProvider(WebServer.DB_CONNECTION_STRING);
            var userRepository = new UserRepository(db);
            await userRepository.CreateUser(new User
            {
                Username = "1-3",
                Password = "test",
            });
        }
    }
}
