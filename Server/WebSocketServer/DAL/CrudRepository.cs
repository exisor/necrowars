using System.Threading.Tasks;
using Dapper.FastCrud;

namespace DAL
{
    public abstract class CrudRepository<TData> : ICrudRepository<TData> 
        where TData : class, IIdentifiedData, new()
    {
        protected readonly IDataProvider DataProvider;
        protected CrudRepository(IDataProvider dataProvider)
        {
            DataProvider = dataProvider;
        }

        public virtual async Task<TData> Get(long id)
        {
            TData retval = null;
            await DataProvider.Transaction(async _ =>
            {
                retval = await _.GetAsync(new TData {Id = id});
            });
            return retval;
        }

        public TData GetSync(long id)
        {
            TData retval = null;
            DataProvider.Transaction(_ =>
            {
                retval = _.Get(new TData { Id = id });
            });
            return retval;
        }

        public Task Insert(TData data)
        {
            return DataProvider.Transaction(_ => _.InsertAsync(data));
        }

        public Task Update(TData data)
        {
            return DataProvider.Transaction(_ => _.UpdateAsync(data));
        }

        public Task Delete(TData data)
        {
            return DataProvider.Transaction(_ => _.DeleteAsync(data));
        }

        public void UpdateSync(TData data)
        {
            DataProvider.Transaction(_ => _.Update(data));
        }

        public void InsertSync(TData data)
        {
            DataProvider.Transaction(_ => _.Insert(data));
        }

        public void DeleteSync(TData data)
        {
            DataProvider.Transaction(_ => _.Delete(data));
        }
    }
}