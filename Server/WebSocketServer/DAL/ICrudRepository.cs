﻿using System.Threading.Tasks;

namespace DAL
{
    public interface ICrudRepository<TData>
        where TData : class, IIdentifiedData
    {
        Task<TData> Get(long id);
        TData GetSync(long id);
        Task Insert(TData data);
        Task Update(TData data);
        Task Delete(TData data);
        void UpdateSync(TData data);
        void InsertSync(TData data);
        void DeleteSync(TData data);
    }

    public interface IIdentifiedData
    {
        long Id { get; set; }
    }
}
