﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Threading.Tasks;
using Dapper.FastCrud;
using Npgsql;

namespace DAL
{
    public class DapperDataProvider : IDataProvider
    {
        private readonly string _connectionString;

        public DapperDataProvider(string connectionString)
        {
            _connectionString = connectionString;
            OrmConfiguration.DefaultDialect = SqlDialect.PostgreSql;
        }

        public DapperDataProvider(string host, int port, string database, string username, string password)
        {
            Dapper.DefaultTypeMap.MatchNamesWithUnderscores = true;
            var sb = new NpgsqlConnectionStringBuilder
            {
                Host = host,
                Port = port,
                Database = database,
                Username = username,
                Password = password
            };

            _connectionString = sb.ConnectionString;
        }
        public IDbConnection Connect()
        {
            return new NpgsqlConnection(_connectionString);
        }

        public async Task Transaction(Func<IDbConnection, Task> executeFunc)
        {
            try
            {
                using (var conn = Connect())
                {
                    conn.Open();

                    await executeFunc(conn);
                }
            }
            catch (TimeoutException)
            {
                throw new Exception($"{GetType()} got SQLtimeout exception.");
            }
            catch (SqlException)
            {
                throw new Exception($"{GetType()} got SQL exception");
            }
        }

        public void Transaction(Action<IDbConnection> executeAction)
        {
            try
            {
                using (var conn = Connect())
                {
                    conn.Open();

                    executeAction(conn);
                }
            }
            catch (TimeoutException)
            {
                throw new Exception($"{GetType()} got SQLtimeout exception.");
            }
            catch (SqlException)
            {
                throw new Exception($"{GetType()} got SQL exception");
            }
        }
    }
}