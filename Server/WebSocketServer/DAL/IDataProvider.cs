﻿using System;
using System.Data;
using System.Threading.Tasks;

namespace DAL
{
    public interface IDataProvider
    {
        IDbConnection Connect();
        Task Transaction(Func<IDbConnection, Task> executeFunc);
        void Transaction(Action<IDbConnection> executeAction);
    }
}
