﻿namespace Common
{
    public interface IIdentified
    {
        long Id { get; }
    }
}
