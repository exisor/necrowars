﻿using System.Collections.Generic;
using Common.Resources;

namespace Common.ResourceManager
{
    public interface IAbilityResourceManager : IResourceManager<Common.Resources.Ability>
    {
        IEnumerable<Ability> GetAffectedAbilities(long id);
    }
}
