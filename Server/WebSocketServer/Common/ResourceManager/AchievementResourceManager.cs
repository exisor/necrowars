﻿using Common.Resources;

namespace Common.ResourceManager
{
    public class AchievementResourceManager : ResourceManager<Achievement>, IAchievementResourceManager
    {
        #region Overrides of ResourceManager<Achievement>

        protected override string FileName => "Resources\\achievements.xml";

        #endregion
    }
}