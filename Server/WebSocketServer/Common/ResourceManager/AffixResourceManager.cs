﻿using System.Collections.Generic;
using System.Linq;
using Common.Resources;
using Item = Common.DTO.Item;

namespace Common.ResourceManager
{
    public class AffixResourceManager : ResourceManager<Common.Resources.Affix>, IAffixResourceManager
    {
        protected override string FileName => "resources/affixes.xml";
        public IEnumerable<IAffixValues> GetAffixValues(Item item)
        {
            return item.Affixes.Select(a =>
            {
                var retval = ((AffixValues)Get(a.Id)).Clone();

                retval.Enchant = a.Enchant;
                retval.Level = item.ItemLevel;
                retval.SetSeed(a.Seed);
                return retval;
            });
        }

        public IAffixValues GetAffixValues(long id)
        {
            var retval = ((AffixValues) Get(id)).Clone();

            retval.Enchant = 0;
            retval.Level = 1;
            retval.SetSeed(0);
            return retval;
        }
    }
}