﻿using System.Linq;
using Common.Resources;

namespace Common.ResourceManager
{
    public class KongregateShopResourceManager : ResourceManager<KongregateShopItem>,IKongregateShopResourceManager
    {
        #region Overrides of ResourceManager<KongregateShopItem>

        protected override string FileName => "resources/kongregateShop.xml";

        #endregion

        #region Implementation of IKongregateShopResourceManager

        public KongregateShopItem GetByKongId(string kongregateId)
        {
            return All.FirstOrDefault(i => i.KongregateId == kongregateId);
        }

        #endregion
    }
}