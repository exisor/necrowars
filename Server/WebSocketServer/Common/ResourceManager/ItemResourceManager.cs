namespace Common.ResourceManager
{
    public class ItemResourceManager : ResourceManager<Common.Resources.Item>, IItemResourceManager
    {
        protected override string FileName => "Resources\\items.xml";
    }
}