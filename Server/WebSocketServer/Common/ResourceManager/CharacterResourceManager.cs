﻿using Common.Resources;

namespace Common.ResourceManager
{
    public class CharacterResourceManager : ResourceManager<Character>, ICharacterResourceManager
    {
        protected override string FileName => "Resources\\characters.xml";
    }
}