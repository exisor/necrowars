﻿using System.Linq;
using Common.Resources;

namespace Common.ResourceManager
{
    public class CurrencyBonusRewardResourceManager : ResourceManager<CurrencyBonusReward>, ICurrencyBonusRewardResourceManager
    {
        protected override string FileName => "Resources/currencyBonusRewards.xml";
        public int CycleLength => All.OrderBy(item => item.Threshold).LastOrDefault()?.Threshold ?? 0;
    }
}