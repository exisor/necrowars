﻿namespace Common.ResourceManager
{
    public class ChestResourceManager : ResourceManager<Common.Resources.Chest>, IChestResourceManager
    {
        protected override string FileName => "resources/chest.xml";
    }
}