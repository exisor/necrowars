﻿using Common.Resources;

namespace Common.ResourceManager
{
    public class DailyRewardResourceManager : ResourceManager<DailyReward>, IDailyRewardResourceManager
    {
        #region Overrides of ResourceManager<DailyReward>

        protected override string FileName => "resources/dailyRewards.xml";

        #endregion
    }

    public interface IDailyRewardResourceManager : IResourceManager<DailyReward>
    {
    }
}