﻿using Common.Resources;

namespace Common.ResourceManager
{
    public interface IDailyQuestResourceManager : IResourceManager<DailyQuest>
    {
    }
}
