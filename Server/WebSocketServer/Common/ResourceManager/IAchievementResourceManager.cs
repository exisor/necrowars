﻿using Common.Resources;

namespace Common.ResourceManager
{
    public interface IAchievementResourceManager : IResourceManager<Achievement>
    {
    }
}
