﻿using Common.Resources;

namespace Common.ResourceManager
{
    public interface ICurrencyBonusRewardResourceManager : IResourceManager<CurrencyBonusReward>
    {
        int CycleLength { get; }
    }
}
