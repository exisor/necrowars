﻿using System.Collections.Generic;
using Common.Resources;

namespace Common.ResourceManager
{
    public interface IAffixResourceManager : IResourceManager<Affix>
    {
        IEnumerable<IAffixValues> GetAffixValues(DTO.Item item);
        IAffixValues GetAffixValues(long id);
    }
}
