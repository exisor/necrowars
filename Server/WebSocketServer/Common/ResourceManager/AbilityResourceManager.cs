﻿using System.Collections.Generic;
using Common.Resources;

namespace Common.ResourceManager
{
    public class AbilityResourceManager : ResourceManager<Ability>, IAbilityResourceManager
    {
        private readonly Dictionary<long, List<Ability>> _affectedAbilities;

        protected override string FileName => "Resources\\abilities.xml";

        public AbilityResourceManager() :base()
        {
            _affectedAbilities = new Dictionary<long, List<Ability>>();
            foreach (var ability in All)
            {
                foreach (var abilityRequirement in ability.AbilityRequirements)
                {
                    List<Ability> affectedList;
                    if (!_affectedAbilities.TryGetValue(abilityRequirement.Id, out affectedList))
                    {
                        affectedList = new List<Ability>();
                        _affectedAbilities[abilityRequirement.Id] = affectedList;
                    }
                    affectedList.Add(ability);
                }
            }
        }
        public IEnumerable<Ability> GetAffectedAbilities(long id)
        {
            List<Ability> ability;
            return !_affectedAbilities.TryGetValue(id, out ability) ? new List<Ability>() : ability;
        }
    }
}