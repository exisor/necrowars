﻿using Common.Resources;

namespace Common.ResourceManager
{
    public class ShopItemsResourceManager : ResourceManager<ShopItem>, IShopItemsResourceManager
    {
        protected override string FileName => "resources/shop.xml";
    }
}