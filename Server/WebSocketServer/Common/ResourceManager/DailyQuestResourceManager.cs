﻿using Common.Resources;

namespace Common.ResourceManager
{
    public class DailyQuestResourceManager : ResourceManager<DailyQuest>, IDailyQuestResourceManager
    {
        #region Overrides of ResourceManager<DailyQuest>

        protected override string FileName => "resources/dailyQuests.xml";

        #endregion
    }
}