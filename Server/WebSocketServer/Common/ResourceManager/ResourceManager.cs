﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection.Emit;

namespace Common.ResourceManager
{
    public abstract class ResourceManager<TResource> : IResourceManager<TResource> where TResource : class, IIdentified
    {
        protected Dictionary<long, TResource> _indexedResources;
        protected List<TResource> _resources;
        protected ResourceManager()
        {
            Load();
        }  
        public TResource Get(long id)
        {
            if (_indexedResources == null) return Default;
            TResource retval;
            if (!_indexedResources.TryGetValue(id, out retval))
            {
                return Default;
            }
            return retval;
        }
        public TResource this[long id] => Get(id);
        public IEnumerable<TResource> All => _resources;
        public TResource Default { get; protected set; }
        protected abstract string FileName { get; }
        protected virtual bool IsList => true;
        public virtual void Load()
        {
            if (IsList)
            {
                LoadFromFile<List<TResource>>();
            }
            else
            {
                LoadFromFile<TResource>();
            }
        }
        private void LoadFromFile<T>()
        {
            var asset = File.ReadAllText(FileName);
            T loadedResources;
            try
            {
                loadedResources = XmlSerializer.Deserialize<T>(asset);
            }
            catch (Exception e)
            {
                throw new Exception($"Can't load resource : {FileName}. Error : {e.Message}" );
            }
            var listOfResources = loadedResources as List<TResource>;
            if (listOfResources != null)
            {
                _resources = listOfResources;
                _indexedResources = All
                    .ToDictionary(item => item.Id, item => item);
                return;
            }
            var singleResource = loadedResources as TResource;
            if (singleResource != null)
            {
                Default = singleResource;
                _resources = new List<TResource> { singleResource };
            }
            throw new Exception("Cannot properly load resource file! File is corrupt");
        }
    }
}