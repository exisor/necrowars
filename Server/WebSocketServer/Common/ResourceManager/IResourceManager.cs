﻿using System.Collections.Generic;

namespace Common.ResourceManager
{
    public interface IResourceManager
    {
        void Load();
    }
    public interface IResourceManager<TResource> : IResourceManager
        where TResource : IIdentified
    {
        TResource Get(long id);
        IEnumerable<TResource> All { get; }
        TResource Default { get; }
    }
}
