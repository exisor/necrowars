﻿using Common.Resources;

namespace Common.ResourceManager
{
    public interface IItemSetsResourceManager : IResourceManager<ItemSet>
    {
    }

    public class ItemSetsResourceManager : ResourceManager<ItemSet>, IItemSetsResourceManager
    {
        protected override string FileName => "resources/itemSets.xml";
    }
}
