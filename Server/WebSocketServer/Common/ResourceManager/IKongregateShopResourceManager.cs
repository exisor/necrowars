﻿using System.Security.Cryptography.X509Certificates;
using Common.Resources;

namespace Common.ResourceManager
{
    public interface IKongregateShopResourceManager : IResourceManager<KongregateShopItem>
    {
        KongregateShopItem GetByKongId(string kongregateId);
    }
}

