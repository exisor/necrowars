﻿using Common.Resources;

namespace Common.ResourceManager
{
    public interface IShopItemsResourceManager : IResourceManager<ShopItem>
    {
    }
}
