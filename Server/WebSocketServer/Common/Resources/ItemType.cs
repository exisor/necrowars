namespace Common.Resources
{
    public enum ItemType : byte
    {
        Equipment,
        Weapon,
        Body,
        Consumable,
        Instant,
        Battle,
        Lobby,
        Head,
        Gloves,
        Boots,
        Offhand,
        OneTime,
        VisualHelm
    }
}