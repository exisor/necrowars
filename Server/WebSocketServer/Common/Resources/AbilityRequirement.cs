﻿using System;

namespace Common.Resources
{
    [Serializable]
    public class AbilityRequirement
    {
        public long Id { get; set; }
        public int Level { get; set; }
    }
}
