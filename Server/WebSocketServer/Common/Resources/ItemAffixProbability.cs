﻿using System;

namespace Common.Resources
{
    [Serializable]
    public class ItemAffixProbability
    {
        public long AffixId { get; set; }
        public float Weight { get; set; }
    }
}
