﻿using System;
using System.Collections.Generic;
using Common.DTO;
namespace Common.Resources
{
    [Serializable]
    public struct ItemSetBonus
    {
        public int ItemsCount;
        public long Affix;
    }
    [Serializable]
    public class ItemSet : IIdentified
    {
        public long Id { get; set; }
        public List<ItemSetBonus> Bonuses { get; set; } = new List<ItemSetBonus>();
    }
    [Serializable]
    public class Item : IIdentified
    {
        public long Id { get; set; }
        public List<ItemType> Types { get; set; } 
        public List<ItemAffixProbability> AffixProbabilities { get; set; }
        public int StackSize { get; set; }
        public Rarity Rarity { get; set; }
        public List<long> FixedAffixes { get; set; }
        public int OverrideRarityAffixCount { get; set; } = -1;
        public List<int> LevelSteps { get; set; } = new List<int>();
        public long SetId { get; set; } = 0;
    }
}