using System;

namespace Common.Resources
{
    [Serializable]
    public class ConstantValueCalculator : ValueCalculator
    {
        public int Constant { get; set; }
        public override ValueCalculator Clone()
        {
            var retval = new ConstantValueCalculator
            {
                Constant = Constant
            };
            CloneInternal(retval);
            return retval;
        }

        protected override double UpdateValue()
        {
            return Constant;
        }
    }
}