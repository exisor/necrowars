﻿using System;
using System.Collections.Generic;

namespace Common.Resources
{
    [Serializable]
    public class Ability : IIdentified
    {
        public long Id { get; set; }
        public AbilityType Type { get; set; }
        public float Cooldown { get; set; }
        public float Duration { get; set; }
        public float CastTime { get; set; }
        public float Radius { get; set; }
        public List<AbilityRequirement> AbilityRequirements { get; set; }
        public bool isEndless { get; set; }

        public Ability()
        {
            AbilityRequirements = new List<AbilityRequirement>();
        }
    }
}
