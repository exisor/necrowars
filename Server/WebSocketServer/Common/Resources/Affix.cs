﻿using System;
using System.Xml.Serialization;

namespace Common.Resources
{
    [Serializable]
    [XmlInclude(typeof (AffixValues))]
    public class Affix : IIdentified
    {
        public long Id { get; set; }
        public int MinimumItemLevel { get; set; }
    }
}
