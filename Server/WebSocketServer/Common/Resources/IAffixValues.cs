﻿namespace Common.Resources
{
    public interface IAffixValues : IIdentified
    {
        int Level { get; set; }
        int Enchant { get; set; }
        void SetSeed(int seed);
        int GetValue(string name);
        double GetFloatValue(string name);
        IAffixValues Clone();
        IAffixValues DeepClone();
    }
}