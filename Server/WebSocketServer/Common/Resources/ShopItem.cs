﻿using System;
using System.Xml.Serialization;

namespace Common.Resources
{

    [Serializable]
    public class DayOfWeekLimited : Limited
    {
        public DayOfWeek DayOfWeek { get; set; }
        public override DateTime GetStartTime()
        {
            var now = DateTime.Today;
            var endTimeSpan = TimeSpan.FromMinutes(Length);
            var start = (DayOfWeek <= now.DayOfWeek)
                ? now.Subtract(TimeSpan.FromDays((int) now.DayOfWeek - (int) DayOfWeek))
                : now.Subtract(TimeSpan.FromDays(7 - ((int) DayOfWeek - (int) now.DayOfWeek)));
            if (DateTime.Now < start + endTimeSpan) return start;
            return start + TimeSpan.FromDays(7);
        }
    }

    [Serializable]
    public class DayOfYearLimited : Limited
    {
        public DateTime Date { get; set; }
        public override DateTime GetStartTime()
        {
            return Date;
        }
    }

    [Serializable]
    [XmlInclude(typeof(DayOfWeekLimited))]
    [XmlInclude(typeof(DayOfYearLimited))]
    public abstract class Limited
    {
        public abstract DateTime GetStartTime();
        // Length in Minutes
        public double Length { get; set; }

        [XmlIgnore]
        public bool Ongoing
        {
            get
            {
                var now = DateTime.Now;
                var start = GetStartTime();
                return now < start + TimeSpan.FromMinutes(Length) && now >= start;
            }
        }

        public long ExpiresInMinutes
        {
            get
            {
                if (!Ongoing) return 0;
                return (long)(GetStartTime() + TimeSpan.FromMinutes(Length) - DateTime.Now).TotalMinutes;
            }
        }
    }
    [Serializable]
    public class ShopItem : IIdentified
    {
        public long Id { get; set; }
        public long ItemId { get; set; }
        public int GoldPrice { get; set; }
        public int CurrencyPrice { get; set; }
        public int Quantity { get; set; }
        public bool OneTime { get; set; }
        public int Level { get; set; } = 0;
        public Limited Limited { get; set; }
        public long Overrides { get; set; }
    }
}
