using System;
using System.Collections.Generic;
using System.Linq;

namespace Common.Resources
{
    [Serializable]
    public class RandomFromCollectionValueCalculator : ValueCalculator
    {
        public List<int> Ids { get; set; }
        private int _id;
        public override ValueCalculator Clone()
        {
            var retval = new RandomFromCollectionValueCalculator
            {
                Ids = Ids.ToList()
            };
            CloneInternal(retval);
            return retval;
        }

        protected override void InitializeRandomInternal(Random random)
        {
            var index = random.Next(Ids.Count);
            _id = Ids[index];
        }

        protected override double UpdateValue()
        {
            return _id;
        }
    }
}