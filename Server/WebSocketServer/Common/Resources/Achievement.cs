﻿using System;
using System.Collections.Generic;

namespace Common.Resources
{
    [Serializable]
    public class Achievement : IIdentified
    {

        #region Implementation of IIdentified
        public long Id { get; set; }
        #endregion

        public List<AchievementStep> Steps { get; set; }
    }

    [Serializable]
    public class AchievementStep
    {
        public int Threshold { get; set; }
        public long RewardId { get; set; }
        public int Quantity { get; set; }
    }
}
