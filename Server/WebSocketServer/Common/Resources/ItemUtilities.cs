﻿using System;
using System.CodeDom;
using Common.DTO;

namespace Common.Resources
{
    public static class ItemUtilities
    {
        public struct ComplexPrice
        {
            public readonly int Gold;
            public readonly int Currency;

            public static explicit operator int(ComplexPrice price)
            {
                return price.Gold;
            }

            public ComplexPrice(int gold, int currency)
            {
                Gold = gold;
                Currency = currency;
            }

            public static ComplexPrice operator +(ComplexPrice lvalue, ComplexPrice rvalue)
            {
                return new ComplexPrice(lvalue.Gold + rvalue.Gold, lvalue.Currency + rvalue.Currency);
            }

            public static ComplexPrice operator -(ComplexPrice lvalue, ComplexPrice rvalue)
            {
                return new ComplexPrice(lvalue.Gold - rvalue.Gold, lvalue.Currency - rvalue.Currency);
            }

            public static ComplexPrice operator +(ComplexPrice lvalue, int value)
            {
                return new ComplexPrice(lvalue.Gold + value, lvalue.Currency);
            }

            public static ComplexPrice operator -(ComplexPrice lvalue, int value)
            {
                return new ComplexPrice(lvalue.Gold - value, lvalue.Currency);
            }

            public static ComplexPrice operator *(ComplexPrice lvalue, int value)
            {
                return new ComplexPrice(lvalue.Gold * value, lvalue.Currency * value);
            }

            public static ComplexPrice operator *(ComplexPrice lvalue, double value)
            {
                return new ComplexPrice((int)(lvalue.Gold * value), (int) (lvalue.Currency * value));
            }
        }
        /// <summary>
        /// Calculate item selling price
        /// </summary>
        /// <param name="itemLevel">Level of item</param>
        /// <param name="rarity">Rarity of item. Rarity.Random is not allowed</param>
        /// <returns>Item selling price</returns>
        public static ComplexPrice GetPrice(int itemLevel, Rarity rarity)
        {
            if (rarity == Rarity.Random) throw new ArgumentException(nameof(rarity));

            return new ComplexPrice((int)(45*itemLevel*RarityMod(rarity)), 0);
        }
        private static float RarityMod(Rarity rarity)
        {
            switch (rarity)
            {
                case Rarity.Common:
                    return 0.8f;
                case Rarity.Rare:
                    return 1.0f;
                case Rarity.Epic:
                    return 3.5f;
                case Rarity.Legendary:
                    return 8.0f;
                case Rarity.Unique:
                    return 10.0f;
                default:
                    throw new ArgumentOutOfRangeException(nameof(rarity), rarity, null);
            }
        }
        /// <summary>
        /// Calculates enchant affix price
        /// </summary>
        /// <param name="itemLevel">Level of item</param>
        /// <param name="rarity">Specific item rartiy. Rarity.Random is not allowed</param>
        /// <param name="affixCurrentEnchant">Current affix enchant level</param>
        /// <returns>Price for affix's next level enchant</returns>
        public static ComplexPrice GetEnchantAffixPrice(int itemLevel, Rarity rarity, int affixCurrentEnchant)
        {
            return GetPrice(itemLevel, rarity) * (1 + Math.Pow(affixCurrentEnchant, 2.5));
        }
        /// <summary>
        /// Calculates enchant price
        /// </summary>
        /// <param name="itemLevel">Level of item</param>
        /// <param name="rarity">Specific item rartiy. Rarity.Random is not allowed</param>
        /// <param name="currentEnchantLevel">Current item enchant level</param>
        /// <returns>Price for item net level enchant</returns>
        public static ComplexPrice GetEnchantItemPrice(int itemLevel, Rarity rarity, int currentEnchantLevel)
        {
            return GetPrice(itemLevel, rarity) * (2 + Math.Pow(currentEnchantLevel, 1.5));
        }
        /// <summary>
        /// Calculates upgrade price for item
        /// </summary>
        /// <param name="itemLevel">Level of item</param>
        /// <param name="characterLevel">Level of character</param>
        /// <param name="rarity">Item rarity</param>
        /// <param name="itemEnchant">Level of item enchant (without affixes, only base item)</param>
        /// <param name="sumOfAffixesEnchant">Sum of levels of all item affixes. Without base item enchant level. ex: (sumOfAffixesEnchant : item.Affixes.Sum(a => a.Enchant))</param>
        /// <returns></returns>
        public static ComplexPrice GetUpgradeItemPrice(int itemLevel, int characterLevel, Rarity rarity, int itemEnchant,
            int sumOfAffixesEnchant)
        {
            return new ComplexPrice(
#if UPGRADE_WITH_GOLD
                (int) (GetPrice(characterLevel, rarity).Gold * (2 + Math.Pow(itemEnchant, 1.5)) * (1 + Math.Pow(sumOfAffixesEnchant, 2.5)))
#else
                0
#endif
                , characterLevel - itemLevel);
        }
        
    }
}
