﻿
using System;

namespace Common.Resources
{
    [Serializable]
    public class CurrencyBonusReward : IIdentified
    {
        public long Id { get; set; }
        public int Threshold { get; set; }
        public long ItemBaseId { get; set; }
        public int Quantity { get; set; }
    }
}
