﻿using System;

namespace Common.Resources
{
    [Serializable]
    public class DailyReward : IIdentified
    {
        #region Implementation of IIdentified
        public long Id { get; set; }
        #endregion
        public long ItemId { get; set; }
        public int Quantity { get; set; }
    }
}
