﻿using System;

namespace Common.Resources
{
    [Serializable]
    public class DailyQuest : IIdentified
    {
        #region Implementation of IIdentified

        public long Id { get; set; }

        #endregion

        public long RewardId { get; set; }

        public int Quantity { get; set; }
        public int Threshold { get; set; }
    }
}
