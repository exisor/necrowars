using System;

namespace Common.Resources
{
    [Serializable]
    public class ExponentValueCalculator : ValueCalculator
    {
        private double _quality;
        private double _softCap = 1;
        public double Multiplier { get; set; } = 1;

        public double SoftCap
        {
            get { return _softCap; }
            set
            {
                // ReSharper disable once CompareOfFloatsByEqualityOperator
                if (value == 0) throw new DivideByZeroException("SoftCap cannot be 0");
                _softCap = value;
            }
        }
        public double Exponent { get; set; } = 1;
        public double FixedBonus { get; set; } = 0;
        public int StartLevel { get; set; } = 0;
        public int MaxValue { get; set; } = int.MaxValue;

        public double MinQuality { get; set; } = 0.5;
        public double QualityRange { get; set; } = 0.5;
        public double QualityPerEnchant { get; set; } = 0.1;

        public override ValueCalculator Clone()
        {
            var retval = new ExponentValueCalculator
            {
                Multiplier = Multiplier,
                SoftCap = SoftCap,
                Exponent = Exponent,
                FixedBonus = FixedBonus,
                MaxValue = MaxValue,
                StartLevel = StartLevel,
                MinQuality = MinQuality,
                QualityRange = QualityRange,
                QualityPerEnchant = QualityPerEnchant
            };
            CloneInternal(retval);
            return retval;
        }

        protected override double UpdateValue()
        {
            return Math.Truncate(Math.Min(Multiplier*Math.Pow((double) (Level + Enchant - StartLevel)/ SoftCap, Exponent) + FixedBonus, MaxValue) + Epsilon) * Quality;
        }

        protected override void InitializeRandomInternal(Random random)
        {
            _quality = MinQuality + QualityRange*random.NextDouble();
        }
        private double Quality => _quality + QualityPerEnchant * Enchant;
    }
}