﻿using System;
using System.Collections.Generic;

namespace Common.Resources
{
    [Serializable]
    public class Character : IIdentified
    {
        public long Id { get; set; }
        public List<long> AvailableAbilities { get; set; }
    }
}
