namespace Common.Resources
{
    public interface IValue
    {
        int Value { get; }
        double FloatValue { get; }
    }
}