using System;
using System.Xml.Serialization;

namespace Common.Resources
{
    [Serializable, XmlInclude(typeof(ExponentValueCalculator)), XmlInclude(typeof(RandomFromCollectionValueCalculator)), XmlInclude(typeof(ConstantValueCalculator))]
    public abstract class ValueCalculator : IValue
    {

        protected const float Epsilon = 0.00001f;

        #region members
        private bool _initialized;
        private int _enchant;
        private int _level = 1;
        #endregion
        #region IValue

        [XmlIgnore]
        public int Value => (int) (FloatValue + double.Epsilon);
        #endregion
        #region public friend interface
        [XmlIgnore]
        public int Level
        {
            get { return _level; }
            set
            {
                _level = value; 
                OnUpdate();
            }
        }
        [XmlIgnore]
        public double FloatValue { get; private set; }

        [XmlIgnore]
        public int Enchant
        {
            get { return _enchant; }
            set
            {
                _enchant = value; 
                OnUpdate();
            }
        }

        public void InitializeRandom(Random random)
        {
            if (_initialized) throw new InvalidOperationException("Already initialized");
            InitializeRandomInternal(random);
            _initialized = true;
            OnUpdate();
        }

        protected virtual void InitializeRandomInternal(Random random) { }
        public abstract ValueCalculator Clone();
        #endregion
        #region protected members
        protected abstract double UpdateValue();

        protected void CloneInternal(ValueCalculator other)
        {
            other.Name = Name;
        }
        #endregion
        #region serializable data
        [XmlAttribute]
        public string Name { get; set; }
        #endregion
        #region helpers
        private void OnUpdate()
        {
            if (!_initialized) return;
            FloatValue = UpdateValue();
        }
        #endregion
    }
}