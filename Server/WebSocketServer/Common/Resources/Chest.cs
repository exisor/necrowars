﻿
using System;
using System.Collections.Generic;
using Common.DTO;

namespace Common.Resources
{
    [Serializable]
    public class Chest : IIdentified
    {
        public long Id { get; set; }
        public List<ChestItemProbability> ChestItems { get; set; }
        public int NumberOfItems { get; set; }
    }

    [Serializable]
    public class ChestItemProbability
    {
        public long ItemId { get; set; }
        public float Weight { get; set; }
        public Rarity Rarity { get; set; }
        public int QuantityMin { get; set; } = 1;
        public int QuantityMax { get; set; } = 1;
    }
}
