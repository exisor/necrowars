﻿using System;

namespace Common.Resources
{
    [Serializable]
    public class KongregateShopItem : IIdentified
    {
        public long Id { get; set; }
        public string KongregateId { get; set; }
        public long ItemBaseId { get; set; }
        public int Quantity { get; set; }
        public int KongregateCurrencyPrice { get; set; }
    }
}