﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Serialization;

namespace Common.Resources
{
    [Serializable]
    public class AffixValues : Affix, IAffixValues
    {
        #region members
        private Random _rnd;
        private int _seed;

        private int _level = 1;
        private int _enchant = 0;
        #endregion
        #region IAffixValues
        [XmlIgnore]
        public int Level
        {
            get { return _level; }
            set
            {
                _level = value;
                Values.ForEach(vc => vc.Level = value);
            }
        }

        [XmlIgnore]
        public int Enchant
        {
            get { return _enchant; }
            set
            {
                _enchant = value; 
                Values.ForEach(vc => vc.Enchant = value);
            }
        }
        public void SetSeed(int seed)
        {
            _seed = seed;
            _rnd = new Random(_seed);
            InitRandom(_rnd);
        }

        public int GetValue(string name)
        {
            var value = this[name];
            if (value == null) return 0;
            else return value.Value;
        }

        public double GetFloatValue(string name)
        {
            var value = this[name];
            if (value == null) return 0;
            else return value.FloatValue;
        }

        #endregion
        protected virtual void InitRandom(Random rnd)
        {
            Values.ForEach(vc => vc.InitializeRandom(rnd));
        }
        #region serializable data
        public List<ValueCalculator> Values { get; set; } = new List<ValueCalculator>();
        #endregion  

        public ValueCalculator this[string name]
        {
            get { return Values.First(v => v.Name == name); }
        }
        public IAffixValues Clone()
        {
            return new AffixValues
            {
                Id = Id,
                MinimumItemLevel = MinimumItemLevel,
                Values = Values.Select(v => v.Clone()).ToList()
            };
        }
        public IAffixValues DeepClone()
        {
            var retval = Clone();
            retval.SetSeed(_seed);
            retval.Enchant = Enchant;
            retval.Level = Level;
            return retval;
        }
    }
}
