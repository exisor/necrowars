﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Common
{
    [AttributeUsage(AttributeTargets.Field)]
    public class ClientValueTag: Attribute
    {
        public bool LevelAddicted { get; set; } = true;
        public bool IsPercent { get; set; } = false;
    }
    [AttributeUsage(AttributeTargets.Field)]
    public class ClientObjectTag : Attribute
    {
    }
    [Serializable]
    public struct ClientValue
    {
        public string Name;
        public float Value;
        public bool LevelAddicted;
        public bool IsPercent;
    }
    [Serializable]
    public class ClientData
    {
        public long Id;
        public List<ClientValue> Values;
    }
}
