﻿using System.IO;

namespace Common
{
    public static class XmlSerializer
    {
        public static string Serialize<T>(T data)
        {
            var serializer = new System.Xml.Serialization.XmlSerializer(typeof (T));
            using (var writer = new StringWriter())
            {
                serializer.Serialize(writer, data);
                return writer.ToString();
            }
        }

        public static T Deserialize<T>(string data)
        {
            var serializer = new System.Xml.Serialization.XmlSerializer(typeof (T));
            using (var reader = new StringReader(data))
            {
                return (T)serializer.Deserialize(reader);
            }
        }
    }
}
