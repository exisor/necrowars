﻿namespace Common
{
    public enum ServerRole : byte
    {
        Master,
        Login,
        Lobby,
        Game,
        Matchmaker,
    }
}