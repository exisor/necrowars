﻿using static System.Math;
namespace Common
{
    public static class Util
    {
        public static long GetRequiredExperienceForLevel(int level)
        {
            const double MULTIPLIER = 100;
            const double SOFT_CAP_LVL = 400;
            var dLevel = (double)level;
            return (long)(Pow(dLevel, 2 + Sqrt(dLevel / SOFT_CAP_LVL)) * MULTIPLIER / Log(dLevel + 1.0));
        }

        public static long GetChestPointsForLevel(int level, long chestBaseId)
        {
            const int LEVEL_ADDED = 5;

            const int MINUMUM_UNITS = 20;
            const int MID_UNITS = 200;
            const int HIGH_UNITS = 600;
            const int TOP_UNITS = 1200;

            const int UNITS_VALUE = 4;

            const int MINIMUM_REWARD_THRESHOLD = MINUMUM_UNITS * UNITS_VALUE;
            const int MID_REWARD_THRESHOLD = MID_UNITS * UNITS_VALUE;
            const int HIGH_REWARD_THRESHOLD = HIGH_UNITS * UNITS_VALUE;
            const int TOP_REWARD_THRESHOLD = TOP_UNITS * UNITS_VALUE;

            var multiplier = 0;
            switch (chestBaseId)
            {
                // Common
                case 1:
                    multiplier = MINIMUM_REWARD_THRESHOLD;
                    break;
                case 2:
                    multiplier = MID_REWARD_THRESHOLD;
                    break;
                case 3:
                    multiplier = HIGH_REWARD_THRESHOLD;
                    break;
                case 4:
                    multiplier = TOP_REWARD_THRESHOLD;
                    break;
            }

            return (LEVEL_ADDED + level) * multiplier;
        }
    }
}
