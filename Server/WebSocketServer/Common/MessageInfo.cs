﻿using System;

namespace Common
{
    public class MessageInfo
    {
        public MessageInfo(string data)
        {
            var split = data.Split(new[] {" "}, StringSplitOptions.None);
            Key = split[0];
            Data = Convert.FromBase64String(split[1]);
        }

        public string Key { get; private set; }
        public byte[] Data { get; private set; }
    }
}
