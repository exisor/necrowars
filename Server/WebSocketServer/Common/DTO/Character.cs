﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
#if Client
using ProtoBuf;
#endif

namespace Common.DTO
{
    [Serializable]
#if Client
    [ProtoContract]
#else
    [DataContract]
#endif
    public class Character
    {
#if Client
        [ProtoMember(1)]
#else
        [DataMember(Order = 1)]
#endif
        public long Id;
#if Client
        [ProtoMember(2)]
#else
        [DataMember(Order = 2)]
#endif
        public int Level;
#if Client
        [ProtoMember(3)]
#else
        [DataMember(Order = 3)]
#endif
        public long Experience;
#if Client
        [ProtoMember(4)]
#else
        [DataMember(Order = 4)]
#endif
        public long BaseId;
#if Client
        [ProtoMember(5)]
#else
        [DataMember(Order = 5)]
#endif
        public List<Ability> Abilities = new List<Ability>();
#if Client
        [ProtoMember(6)]
#else
        [DataMember(Order = 6)]
#endif
        public List<AbilitySlot> AbilitySlots = new List<AbilitySlot>();
#if Client
        [ProtoMember(7)]
#else
        [DataMember(Order = 7)]
#endif
        public int AP;
#if Client
        [ProtoMember(8)]
#else
        [DataMember(Order = 8)]
#endif
        public long RequiredExperience;
#if Client
        [ProtoMember(9)]
#else
        [DataMember(Order = 9)]
#endif
        public int APRefunded;
#if Client
        [ProtoMember(10)]
#else
        [DataMember(Order = 10)]
#endif
        public long UserId;
        // inventory
#if Client
        [ProtoMember(11)]
#else
        [DataMember(Order = 11)]
#endif
        public List<long> EquippedItems = new List<long>();
    }
}
