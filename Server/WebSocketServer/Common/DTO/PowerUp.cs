﻿using System;
using System.Runtime.Serialization;
#if Client
using ProtoBuf;
#endif

namespace Common.DTO
{
#if Client
    [ProtoContract]
#else
    [DataContract]
#endif
    public class PowerUp
    {
#if Client
        [ProtoMember(1)]
#else
        [DataMember(Order = 1)]
#endif
        public long Id;
#if Client
        [ProtoMember(2)]
#else
        [DataMember(Order = 2)]
#endif
        public long BaseId;
#if Client
        [ProtoMember(3)]
#else
        [DataMember(Order = 3)]
#endif
        public TimeSpan Expires;
    }
}
