using System.Runtime.Serialization;
#if Client
using ProtoBuf;
#endif

namespace Common.DTO
{
#if Client
    [ProtoContract]
#else
    [DataContract]
#endif
    public enum Rarity : byte
    {
        Common,
        Rare,
        Epic,
        Legendary,
        Random,
        Unique,
    }
}