﻿using System.Collections.Generic;
using System.Runtime.Serialization;
#if Client
using ProtoBuf;
#endif

namespace Common.DTO
{
#if Client
    [ProtoContract]
#else
    [DataContract]
#endif
    public class KongregateShop
    {
#if Client
        [ProtoMember(1)]
#else
        [DataMember(Order = 1)]
#endif
        public List<KongregateShopItem> Items = new List<KongregateShopItem>();
    }
}
