﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
#if Client
using ProtoBuf;
#endif

namespace Common.DTO
{
    [Serializable]
#if Client
    [ProtoContract]
#else
    [DataContract]
#endif
    public class Player
    {
#if Client
        [ProtoMember(1)]
#else
        [DataMember(Order = 1)]
#endif
        public string Username;
#if Client
        [ProtoMember(2)]
#else
        [DataMember(Order = 2)]
#endif
        public long Gold;
#if Client
        [ProtoMember(3)]
#else
        [DataMember(Order = 3)]
#endif
        public long Currency;
#if Client
        [ProtoMember(4)]
#else
        [DataMember(Order = 4)]
#endif
        public List<Character> Characters = new List<Character>();
#if Client
        [ProtoMember(5)]
#else
        [DataMember(Order = 5)]
#endif
        public List<Item> Items = new List<Item>();
#if Client
        [ProtoMember(6)]
#else
        [DataMember(Order = 6)]
#endif
        public List<PowerUp> PowerUps = new List<PowerUp>();
#if Client
        [ProtoMember(7)]
#else
        [DataMember(Order = 7)]
#endif
        public int InventorySize;
#if Client
        [ProtoMember(8)]
#else
        [DataMember(Order = 8)]
#endif
        public int TutorialPhase;
#if Client
        [ProtoMember(9)]
#else
        [DataMember(Order = 9)]
#endif
        public List<Achievement> Achievements = new List<Achievement>();
#if Client
        [ProtoMember(10)]
#else
        [DataMember(Order = 10)]
#endif
        public long DailyQuestId;
#if Client
        [ProtoMember(11)]
#else
        [DataMember(Order = 11)]
#endif
        public long DailyQuestProgress;
#if Client
        [ProtoMember(12)]
#else
        [DataMember(Order = 12)]
#endif
        public int QuestExpireTime;

#if Client
        [ProtoMember(13)]
#else
        [DataMember(Order = 13)]
#endif
        public int CurrencyBought;
    }
}
