﻿#if Client
using ProtoBuf;
#endif
using System.Runtime.Serialization;

namespace Common.DTO
{
#if Client
    [ProtoContract]
#else
    [DataContract]
#endif
    public class ItemAffix
    {
#if Client
        [ProtoMember(1)]
#else
        [DataMember(Order = 1)]
#endif
        public long Id;
#if Client
        [ProtoMember(2)]
#else
        [DataMember(Order = 2)]
#endif
        public int Seed;
#if Client
        [ProtoMember(3)]
#else
        [DataMember(Order = 3)]
#endif
        public int Enchant;
    }
}
