﻿using System;
using System.Runtime.Serialization;
#if Client
using ProtoBuf;
#endif
namespace Common.DTO
{
    [Serializable]
#if Client
    [ProtoContract]
#else
    [DataContract]
#endif
    public class KongregateItem
    {
#if Client
        [ProtoMember(1)]
#else
        [DataMember(Order = 1)]
#endif
        public long Id;

#if Client
        [ProtoMember(2)]
#else
        [DataMember(Order = 2)]
#endif
        public string Identifier;

#if Client
        [ProtoMember(3)]
#else
        [DataMember(Order = 3)]
#endif
        public string Name;

#if Client
        [ProtoMember(4)]
#else
        [DataMember(Order = 4)]
#endif
        public string Description;

#if Client
        [ProtoMember(5)]
#else
        [DataMember(Order = 5)]
#endif
        public int Price;
#if Client
        [ProtoMember(6)]
#else
        [DataMember(Order = 6)]
#endif
        public int RemainingUses;
    }
}
