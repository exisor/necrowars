﻿using System.Runtime.Serialization;
#if Client
using ProtoBuf;
#endif
namespace Common.DTO
{

#if Client
    [ProtoContract]
#else
    [DataContract]
#endif
    public class KongregateShopItem
    {
#if Client
        [ProtoMember(1)]
#else
        [DataMember(Order = 1)]
#endif
        public string Id;
#if Client
        [ProtoMember(2)]
#else
        [DataMember(Order = 2)]
#endif
        public long ItemBaseId;
#if Client
        [ProtoMember(3)]
#else
        [DataMember(Order = 3)]
#endif
        public int Quantity;
#if Client
        [ProtoMember(4)]
#else
        [DataMember(Order = 4)]
#endif
        public int KongregateCurrencyPrice;
    }
}