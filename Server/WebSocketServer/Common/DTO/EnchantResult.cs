﻿using System.Runtime.Serialization;
#if Client
using ProtoBuf;
#endif

namespace Common.DTO
{
#if Client
    [ProtoContract]
#else
    [DataContract]
#endif
    public enum EnchantResult
    {
        Success,
        LevelDecreased,
        LevelZero,
        Broken,
    }
}
