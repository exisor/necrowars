﻿using System.Runtime.Serialization;
#if Client
using ProtoBuf;
#endif

namespace Common.DTO
{
#if Client
    [ProtoContract]
#else
    [DataContract]
#endif
    public class ShopItem
    {
#if Client
        [ProtoMember(1)]
#else
        [DataMember(Order = 1)]
#endif
        public long BaseId;
#if Client
        [ProtoMember(2)]
#else
        [DataMember(Order = 2)]
#endif
        public int GoldPrice;
#if Client
        [ProtoMember(3)]
#else
        [DataMember(Order = 3)]
#endif
        public int CurrencyPrice;
#if Client
        [ProtoMember(4)]
#else
        [DataMember(Order = 4)]
#endif
        public long Id;
#if Client
        [ProtoMember(5)]
#else
        [DataMember(Order = 5)]
#endif
        public int Quantity;
#if Client
        [ProtoMember(6)]
#else
        [DataMember(Order = 6)]
#endif
        public int Level;
#if Client
        [ProtoMember(7)]
#else
        [DataMember(Order = 7)]
#endif
        public bool Limited;
#if Client
        [ProtoMember(8)]
#else
        [DataMember(Order = 8)]
#endif
        public long ExpiresInMinutes;
#if Client
        [ProtoMember(9)]
#else
        [DataMember(Order = 9)]
#endif
        public double Discount;
    }
}
