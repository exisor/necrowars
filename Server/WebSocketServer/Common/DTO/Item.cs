﻿using System.CodeDom;
using System.Collections.Generic;
using System.Runtime.Serialization;
#if Client
using ProtoBuf;
#endif

namespace Common.DTO
{
#if Client
    [ProtoContract]
#else
    [DataContract]
#endif
    public class Item
    {
#if Client
        [ProtoMember(1)]
#else
        [DataMember(Order = 1)]
#endif
        public long Id;
#if Client
        [ProtoMember(2)]
#else
        [DataMember(Order = 2)]
#endif
        public long BaseId;
#if Client
        [ProtoMember(3)]
#else
        [DataMember(Order = 3)]
#endif
        public int ItemLevel;
#if Client
        [ProtoMember(4)]
#else
        [DataMember(Order = 4)]
#endif
        public Rarity Rarity;
#if Client
        [ProtoMember(5)]
#else
        [DataMember(Order = 5)]
#endif
        public List<ItemAffix> Affixes = new List<ItemAffix>();

#if Client
        [ProtoMember(6)]
#else
        [DataMember(Order = 6)]
#endif
        public int Enchant;
#if Client
        [ProtoMember(7)]
#else
        [DataMember(Order = 7)]
#endif
        public int Quantity;

        public static Item operator +(Item lvalue, Item rvalue)
        {
            lvalue.Quantity += rvalue.Quantity;
            return lvalue;
        }
    }
}