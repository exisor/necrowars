﻿using System;
using System.Runtime.Serialization;
#if Client
using ProtoBuf;
#endif

namespace Common.DTO
{
    [Serializable]
#if Client
    [ProtoContract]
#else
    [DataContract]
#endif
    public class AbilitySlot
    {
        
        public static AbilitySlot Empty(int slot)
        {
            return new AbilitySlot {Slot = slot, AbilityId = 0};
        }

#if Client
        [ProtoMember(1)]
#else
        [DataMember(Order = 1)]
#endif
        public int Slot;
#if Client
        [ProtoMember(2)]
#else
        [DataMember(Order = 2)]
#endif
        public long AbilityId;
    }
}
