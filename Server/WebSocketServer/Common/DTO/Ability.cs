﻿using System;
using System.Runtime.Serialization;
#if Client
using ProtoBuf;
#endif

namespace Common.DTO
{
    [Serializable]
#if Client
    [ProtoContract]
#else
    [DataContract]
#endif
    public class Ability
    {
#if Client
        [ProtoMember(1)]
#else
        [DataMember(Order = 1)]
#endif
        public long Id;
#if Client
        [ProtoMember(2)]
#else
        [DataMember(Order = 2)]
#endif
        public int Level;
    }
}
