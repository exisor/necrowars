﻿using System;
using System.Runtime.Serialization;
#if Client
using ProtoBuf;
#endif

namespace Common.DTO
{
    [Serializable]
#if Client
    [ProtoContract]
#else
    [DataContract]
#endif
    public class LeaderboardRecord
    {
#if Client
        [ProtoMember(1)]
#else
        [DataMember(Order = 1)]
#endif
            public long CharacterId;

#if Client
        [ProtoMember(2)]
#else
        [DataMember(Order = 2)]
#endif
        public string PlayerName;

#if Client
        [ProtoMember(3)]
#else
        [DataMember(Order = 3)]
#endif
        public int ArmySize;
#if Client
        [ProtoMember(4)]
#else
        [DataMember(Order = 4)]
#endif
        public int TopTime;

#if Client
        [ProtoMember(5)]
#else
        [DataMember(Order = 5)]
#endif
        public long TopGold;

#if Client
        [ProtoMember(6)]
#else
        [DataMember(Order = 6)]
#endif
        public int Kills;

#if Client
        [ProtoMember(7)]
#else
        [DataMember(Order = 7)]
#endif
        public int Points;
#if Client
        [ProtoMember(8)]
#else
        [DataMember(Order = 8)]
#endif
            public DateTime Date;
#if Client
        [ProtoMember(9)]
#else
        [DataMember(Order = 9)]
#endif
        public int Position;
    }
}
