using System.Runtime.Serialization;
#if Client
using ProtoBuf;
#endif

namespace Common
{
#if Client
    [ProtoContract]
#else
    [DataContract]
#endif
    public enum BattleSpecEnum
    {
        WebGL,
        UnityClient,
    }
}