﻿using System.Runtime.Serialization;

namespace ServerProtocol
{
    [DataContract]
    public class CharacterExpChangedEvent
    {
        [DataMember(Order = 1)]
        public long CharacterId;
        [DataMember(Order = 2)]
        public long CurrentExp;

        [DataMember(Order = 3)] public long UserId;
    }
}
