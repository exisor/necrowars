﻿using Common;
using System.Runtime.Serialization;

namespace ServerProtocol
{
    [DataContract]
    public class RegisterServerOnMaster
    {
        [DataMember(Order = 1)]
        public string Game;
        [DataMember(Order = 2)]
        public ServerRole Role;

        [DataMember(Order = 3)] public string Address;
        [DataMember(Order = 4)] public int Port;
    }
}
