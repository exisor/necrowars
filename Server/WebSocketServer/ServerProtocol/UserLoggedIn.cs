﻿using System.Runtime.Serialization;

namespace ServerProtocol
{
    [DataContract]
    public class UserLoggedIn
    {
        [DataMember(Order = 1)] public long Id;
        [DataMember(Order = 2)] public string SessionKey;
        [DataMember(Order = 3)] public string Game;
        [DataMember(Order = 4)] public long KongregateId;
        [DataMember(Order = 5)] public long OGZId;
    }
}
