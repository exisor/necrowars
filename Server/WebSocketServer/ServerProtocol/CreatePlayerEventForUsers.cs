﻿using System.Collections.Generic;
using System.Runtime.Serialization;
using Common.DTO;

#if Client
using ProtoBuf;
#endif
namespace ServerProtocol
{
    [DataContract]
    public class CreatePlayerEventForUsers
    {
        [DataMember(Order = 1)] public long EventId;
        [DataMember(Order = 2)] public List<long> ItemBaseIds = new List<long>();
        [DataMember(Order = 3)] public List<long> UserIds = new List<long>();
    }
}
