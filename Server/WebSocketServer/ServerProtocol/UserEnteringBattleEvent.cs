﻿using System.Collections.Generic;
using System.Runtime.Serialization;
using Common.DTO;

namespace ServerProtocol
{
    [DataContract]
    public class UserEnteringBattleEvent
    {
        [DataMember(Order = 1)] public string Token;
        [DataMember(Order = 2)] public Common.DTO.Character Character;
        [DataMember(Order = 3)] public string PlayerName;
        [DataMember(Order = 4)] public List<Item> Items;
    }
}
