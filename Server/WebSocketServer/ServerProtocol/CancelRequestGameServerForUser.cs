﻿
using System.Runtime.Serialization;

namespace ServerProtocol
{
    [DataContract]
    public class CancelRequestGameServerForUser
    {
        [DataMember(Order = 1)] public long CharacterId;

    }
}
