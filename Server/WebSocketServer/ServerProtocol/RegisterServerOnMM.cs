﻿using System.Collections.Generic;
using System.Runtime.Serialization;
using Common;

namespace ServerProtocol
{
    [DataContract]
    public class RegisterServerOnMM
    {
        [DataMember(Order = 1)] public string Address;
        [DataMember(Order = 2)] public int Port;
        [DataMember(Order = 3)] public int Level;
        [DataMember(Order = 4)] public List<BattleSpecEnum> Specs;
    }
}
