﻿
using System.Runtime.Serialization;
#if Client
using ProtoBuf;
#endif

namespace ServerProtocol
{
    [DataContract]
    public class BattleEndedForPlayer
    {
        [DataMember(Order = 1)] public long CharacterId;
        [DataMember(Order = 2)] public long PlayerId;
        [DataMember(Order = 3)] public long Points;
        [DataMember(Order = 4)] public int TimeAlive;
        [DataMember(Order = 5)] public long ExpGained;
        [DataMember(Order = 6)] public long GoldGained;
        [DataMember(Order = 7)] public int Kills;
        [DataMember(Order = 8)] public int ArmySize;
        [DataMember(Order = 9)] public bool Top;
    }
}
