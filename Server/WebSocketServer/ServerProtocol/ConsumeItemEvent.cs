﻿using System.Runtime.Serialization;

namespace ServerProtocol
{
    [DataContract]
    public class ConsumeItemEvent
    {
        [DataMember(Order = 1)] public long PlayerId;
        [DataMember(Order = 2)] public long ItemBaseId;
    }
}
