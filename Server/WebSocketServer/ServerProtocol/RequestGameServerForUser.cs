﻿using System.Collections.Generic;
using System.Runtime.Serialization;
using Common;
using Common.DTO;

namespace ServerProtocol
{
    [DataContract]
    public class RequestGameServerForUser
    {
        [DataMember(Order = 1)] public Common.DTO.Character SelectedCharacter;
        [DataMember(Order = 2)] public string PlayerName;
        [DataMember(Order = 3)] public List<Item> Items;
        [DataMember(Order = 4)] public List<BattleSpecEnum> Specs;
    }
}
