﻿#if DEBUG
using System.Runtime.Serialization;
#if Client
using ProtoBuf;
#endif
namespace ClientProtocol
{
#if Client
    [ProtoContract]
#else
    [DataContract]
#endif
    public class TestCurrencyBoughtBonus
    {
#if Client
        [ProtoMember(1)]
#else
        [DataMember(Order = 1)]
#endif
        public int CurrencyBought;
    }
}
#endif