﻿#if Client
using ProtoBuf;
#endif
using System.Runtime.Serialization;
using Common;

namespace ClientProtocol
{
#if Client
    [ProtoContract]
#else
    [DataContract]
#endif
    public class UserAuthorize
    {
#if Client
        [ProtoMember(1)]
#else
        [DataMember(Order = 1)]
#endif
        public string Username;
#if Client
        [ProtoMember(2)]
#else
        [DataMember(Order = 2)]
#endif
        public string EncryptedPassword;
    }
}
