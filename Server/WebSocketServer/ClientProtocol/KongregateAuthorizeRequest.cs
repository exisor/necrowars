﻿using System.Runtime.Serialization;
#if Client
using ProtoBuf;
#endif

namespace ClientProtocol
{
#if Client
    [ProtoContract]
#else
    [DataContract]
#endif
    public class KongregateAuthorizeRequest
    {
#if Client
        [ProtoMember(1)]
#else
        [DataMember(Order = 1)]
#endif
        public long UserId;
#if Client
        [ProtoMember(2)]
#else
        [DataMember(Order = 2)]
#endif
        public string Token;
    }
}
