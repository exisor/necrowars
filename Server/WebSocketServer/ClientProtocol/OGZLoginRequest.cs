﻿using System.Runtime.Serialization;
#if Client
using ProtoBuf;
#endif

namespace ClientProtocol
{
#if Client
    [ProtoContract]
#else
    [DataContract]
#endif

    public class OGZLoginRequest
    {
#if Client
        [ProtoMember(1)]
#else
        [DataMember(Order = 1)]
#endif
        public long Id;
#if Client
        [ProtoMember(2)]
#else
        [DataMember(Order = 2)]
#endif
        public string SessionKey;
    }
}
