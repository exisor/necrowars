﻿using System.Collections.Generic;
using System.Runtime.Serialization;
using Common.DTO;

#if Client
using ProtoBuf;
#endif
namespace ClientProtocol
{
#if Client
    [ProtoContract]
#else
    [DataContract]
#endif
    public class NewPlayerEventEvent
    {
#if Client
        [ProtoMember(1)]
#else
        [DataMember(Order = 1)]
#endif
        public long EventId;
#if Client
        [ProtoMember(2)]
#else
        [DataMember(Order = 2)]
#endif
        public List<Common.DTO.Item> Items { get; set; } = new List<Item>();
    }
}
