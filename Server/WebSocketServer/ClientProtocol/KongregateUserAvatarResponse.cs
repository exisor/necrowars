﻿#if Client
using ProtoBuf;
#endif
using System.Runtime.Serialization;

namespace ClientProtocol
{
#if Client
    [ProtoContract]
#else
    [DataContract]
#endif
    public class KongregateUserAvatarResponse
    {
#if Client
        [ProtoMember(1)]
#else
        [DataMember(Order = 1)]
#endif
        public string Url;
    }
}