﻿#if Client
using ProtoBuf;
#else
using System.Runtime.Serialization;
#endif

namespace ClientProtocol
{
#if DEBUG
#if Client
    [ProtoContract]
#else
    [DataContract]
#endif
    public class SetSkillForCharacterRequest
    {
        #if Client
        [ProtoMember(1)]
        #else
        [DataMember(Order = 1)]
        #endif
        public long SkillId;

        #if Client
        [ProtoMember(2)]
        #else
        [DataMember(Order = 2)]
        #endif
        public int Level;

        #if Client
        [ProtoMember(3)]
        #else
        [DataMember(Order = 3)]
        #endif
        public long CharacterId;
    }
#endif
}
