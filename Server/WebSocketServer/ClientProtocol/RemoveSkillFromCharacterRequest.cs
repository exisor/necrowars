﻿#if Client
using ProtoBuf;
#else
using System.Runtime.Serialization;
#endif

namespace ClientProtocol
{
#if DEBUG
#if Client
    [ProtoContract]
#else
    [DataContract]
#endif
    public class RemoveSkillFromCharacterRequest
    {
        #if Client
        [ProtoMember(1)]
        #else
        [DataMember(Order = 1)]
        #endif
        public long SkillId;
        #if Client
        [ProtoMember(3)]
        #else
        [DataMember(Order = 3)]
        #endif
        public long CharacterId;
    }
#endif
}