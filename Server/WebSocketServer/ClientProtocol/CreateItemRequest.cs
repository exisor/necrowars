﻿#if DEBUG
#if Client
using ProtoBuf;
#else
using System.Runtime.Serialization;
#endif
using Common.DTO;

namespace ClientProtocol
{
#if Client
    [ProtoContract]
#else
    [DataContract]
#endif
    public class CreateItemRequest
    {

    #if Client
            [ProtoMember(1)]
    #else
            [DataMember(Order = 1)]
    #endif
            public long BaseId;
    #if Client
            [ProtoMember(2)]
    #else
            [DataMember(Order = 2)]
    #endif
            public int ItemLevel;
    #if Client
            [ProtoMember(3)]
    #else
            [DataMember(Order = 3)]
    #endif
            public Rarity Rarity;

    }
}

#endif