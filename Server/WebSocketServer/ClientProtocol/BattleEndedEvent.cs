﻿#if Client
using ProtoBuf;
#else
using System.Runtime.Serialization;
#endif

namespace ClientProtocol
{
#if Client
    [ProtoContract]
#else
    [DataContract]
#endif
    public class BattleEndedEvent
    {
#if Client
        [ProtoMember(1)]
#else
        [DataMember(Order = 1)]
#endif
        public long Points;
#if Client
        [ProtoMember(2)]
#else
        [DataMember(Order = 2)]
#endif
        public int TimeAlive;
#if Client
        [ProtoMember(3)]
#else
        [DataMember(Order = 3)]
#endif
        public long ExpGained;
#if Client
        [ProtoMember(4)]
#else
        [DataMember(Order = 4)]
#endif
        public long GoldGained;
#if Client
        [ProtoMember(5)]
#else
        [DataMember(Order = 5)]
#endif
        public int Kills;
#if Client
        [ProtoMember(6)]
#else
        [DataMember(Order = 6)]
#endif
        public int ArmySize;
#if Client
        [ProtoMember(7)]
#else
        [DataMember(Order = 7)]
#endif
        public long ChestRewardId;
#if Client
        [ProtoMember(8)]
#else
        [DataMember(Order = 8)]
#endif
        public int ChestQuantity;

    }
}
