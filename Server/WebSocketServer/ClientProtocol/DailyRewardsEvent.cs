﻿
using System.Collections.Generic;
using System.Runtime.Serialization;

#if Client
using ProtoBuf;
#endif
namespace ClientProtocol
{
#if Client
    [ProtoContract]
#else
    [DataContract]
#endif
    public class DailyRewardsEvent
    {
#if Client
        [ProtoMember(1)]
#else
        [DataMember(Order = 1)]
#endif
        public List<long> Items;
#if Client
        [ProtoMember(2)]
#else
        [DataMember(Order = 2)]
#endif
        public int Current;
    }
}
