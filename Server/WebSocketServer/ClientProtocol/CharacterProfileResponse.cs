﻿using System.Collections.Generic;
using System.Runtime.Serialization;
using Common.DTO;

#if Client
using ProtoBuf;
#endif
namespace ClientProtocol
{
#if Client
    [ProtoContract]
#else
    [DataContract]
#endif
    public class CharacterProfileResponse
    {
#if Client
        [ProtoMember(1)]
#else
        [DataMember(Order = 1)]
#endif
        public Character Character;

#if Client
        [ProtoMember(2)]
#else
        [DataMember(Order = 2)]
#endif
        public string PlayerName;

#if Client
        [ProtoMember(3)]
#else
        [DataMember(Order = 3)]
# endif
        public List<Item> Items { get; set; }
    }
}