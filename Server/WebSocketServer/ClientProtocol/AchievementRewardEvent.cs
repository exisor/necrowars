﻿using System.Collections.Generic;
using System.Runtime.Serialization;
using Common.DTO;

#if Client
using ProtoBuf;
#endif

namespace ClientProtocol
{
#if Client
    [ProtoContract]
#else
    [DataContract]
#endif
    public class AchievementRewardEvent 
    {
#if Client
        [ProtoMember(1)]
#else
        [DataMember(Order = 1)]
#endif
        public List<Item> Reward;
#if Client
        [ProtoMember(2)]
#else
        [DataMember(Order = 2)]
#endif
        public long AchievementId;
    }
}
