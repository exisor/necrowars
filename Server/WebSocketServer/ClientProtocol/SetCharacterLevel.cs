﻿#if Client
using ProtoBuf;
#endif
using System.Runtime.Serialization;
using Common;

#if DEBUG
namespace ClientProtocol
{
#if Client
    [ProtoContract]
#else
    [DataContract]
#endif
    public class SetCharacterLevel
    {
#if Client
        [ProtoMember(1)]
#else
        [DataMember(Order = 1)] 
#endif
        public long CharacterId;
#if Client
        [ProtoMember(2)]
#else
        [DataMember(Order = 2)] 
#endif
        public int Level;
    }
}
#endif