﻿namespace ClientProtocol
{
    public enum CustomGameMessageType : short
    {
        AuthorizePlayer = 1000,
        PlayerAuthorized = 1001,
        AccessDenied = 1002,
    }
}
