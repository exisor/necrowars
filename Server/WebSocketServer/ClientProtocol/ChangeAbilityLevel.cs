﻿#if Client
using ProtoBuf;
#endif
using System.Runtime.Serialization;
using Common;

namespace ClientProtocol
{
#if Client
    [ProtoContract]
#else
    [DataContract]
#endif
    public class ChangeAbilityLevel
    {
#if Client
        [ProtoMember(1)]
#else
        [DataMember(Order = 1)]
#endif
        public long AbilityId;
#if Client
        [ProtoMember(2)]
#else
        [DataMember(Order = 2)]
#endif
        public int LevelDifference;
#if Client
        [ProtoMember(3)]
#else
        [DataMember(Order = 3)]
#endif
        public long CharacterId;
#if Client
        [ProtoMember(4)]
#else
        [DataMember(Order = 4)]
#endif
        public bool SpendCurrency;
    }
}
