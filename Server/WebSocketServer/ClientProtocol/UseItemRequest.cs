﻿
#if Client
using ProtoBuf;
#else
using System.Runtime.Serialization;
#endif

namespace ClientProtocol
{
#if Client
    [ProtoContract]
#else
    [DataContract]
#endif
    public class UseItemRequest
    {
#if Client
        [ProtoMember(1)]
#else
        [DataMember(Order = 1)]
#endif
        public long ItemId;
#if Client
        [ProtoMember(2)]
#else
        [DataMember(Order = 2)]
#endif
        public int CharacterId;
    }
}
