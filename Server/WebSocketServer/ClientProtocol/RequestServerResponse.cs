﻿#if Client
using ProtoBuf;
#endif
using System.Runtime.Serialization;
using Common;

namespace ClientProtocol
{
#if Client
    [ProtoContract]
#else
    [DataContract]
#endif
    public class RequestServerResponse
    {
#if Client
        [ProtoMember(1)]
#else
        [DataMember(Order = 1)]
#endif
        public string Address;
#if Client
        [ProtoMember(2)]
#else
        [DataMember(Order = 2)]
#endif
        public int Port;
#if Client
        [ProtoMember(3)]
#else
        [DataMember(Order = 3)]
#endif
        public ServerRole Role;
    }
}
