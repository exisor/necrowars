﻿#if Client
using ProtoBuf;
#endif
using System.Runtime.Serialization;
using Common;

namespace ClientProtocol
{
#if Client
    [ProtoContract]
#else
    [DataContract]
#endif
    public class NewPowerUpEvent
    {
#if Client
        [ProtoMember(1)]
#else
        [DataMember(Order = 1)]
#endif
        public Common.DTO.PowerUp PowerUp;
    }
}
