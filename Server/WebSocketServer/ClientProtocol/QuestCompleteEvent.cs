﻿using System.Collections.Generic;
using System.Runtime.Serialization;
using Common.DTO;

#if Client
using ProtoBuf;
#endif
namespace ClientProtocol
{
#if Client
    [ProtoContract]
#else
    [DataContract]
#endif
    public class QuestCompleteEvent
    {
#if Client
        [ProtoMember(1)]
#else
        [DataMember(Order = 1)]
#endif
        public List<Item> Reward;
    }
}
