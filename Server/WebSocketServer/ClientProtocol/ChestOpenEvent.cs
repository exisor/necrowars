﻿#if Client
using ProtoBuf;
#endif
using System.Collections.Generic;
using System.Runtime.Serialization;
using Common;
using Common.DTO;

namespace ClientProtocol
{
#if Client
    [ProtoContract]
#else
    [DataContract]
#endif
    public class ChestOpenEvent
    {
#if Client
        [ProtoMember(1)]
#else
        [DataMember(Order = 1)]
#endif
        public long BaseId;
#if Client
        [ProtoMember(2)]
#else
        [DataMember(Order = 2)]
#endif
        public List<Item> ItemsReceived;
    }
}
