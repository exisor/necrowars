﻿#if Client
using ProtoBuf;
#endif
using System.Runtime.Serialization;
using Common;
using Common.DTO;

namespace ClientProtocol
{
#if Client
    [ProtoContract]
#else
    [DataContract]
#endif
    public class FixedShopResponse
    {
#if Client
        [ProtoMember(1)]
#else
        [DataMember(Order = 1)]
#endif
        public FixedShop Shop;
    }
}
