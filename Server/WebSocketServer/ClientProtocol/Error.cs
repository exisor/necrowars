﻿#if Client
using ProtoBuf;
#endif
using System.Runtime.Serialization;
using Common;

namespace ClientProtocol
{
#if Client
    [ProtoContract]
#else
    [DataContract]
#endif
    public class Error
    {
        public Error()
        {
            
        }

        public Error(object caller) : this()
        {
            Cmd = caller.GetType().Name;
        }

        public Error(object caller, ErrorCode code) : this(caller)
        {
            Message = code;
        }
#if Client
        [ProtoMember(1)]
#else
        [DataMember(Order = 1)]
#endif
        public ErrorCode Message;
#if Client
        [ProtoMember(2)]
#else
        [DataMember(Order = 2)]
#endif
        public string Cmd;
    }

    [DataContract]
    public enum ErrorCode : int
    {
        AccessDenied = 1,

        SessionInvalid = 2,
        ServerIsNotAvailable = 3,
        InvalidDataInRequest = 4,
        DatabaseError = 5,

        NameInUse = 6,
        CharacterNotExists,
        PlayerNotExists,
        NoEmptyCharacterSlots,
        IncorrectAbilitySlot,
        AbilityNotAvailable,
        InsufficientResources,
        OperationFailed,
        ItemNotAvailable,
        NotEnoughInventorySpace,
        EffectAlreadyApplied,
        CharacterLevelIsTooSmall
    }
}
