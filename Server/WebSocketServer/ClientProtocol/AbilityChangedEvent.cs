﻿using System.Runtime.Serialization;
#if Client
using ProtoBuf;
#endif
namespace ClientProtocol
{
#if Client
    [ProtoContract]
#else
    [DataContract]
#endif
    public class AbilityChangedEvent
    {
#if Client
        [ProtoMember(1)]
#else
        [DataMember(Order = 1)] 
#endif
        public long AbilityId;
#if Client
        [ProtoMember(2)]
#else
        [DataMember(Order = 2)]
#endif
        public int Level;

#if Client
        [ProtoMember(3)]
#else
        [DataMember(Order = 3)] 
#endif
        public long CharacterId;
    }
}
