﻿#if Client
using ProtoBuf;
#endif
using System.Runtime.Serialization;
using Common;

#if DEBUG
 
namespace ClientProtocol
{
#if Client
    [ProtoContract]
#else
    [DataContract]
#endif
    public class SetGold
    {
#if Client
        [ProtoMember(1)]
#else
        [DataMember(Order = 1)] 
#endif
        public long Gold;
    }
}
#endif