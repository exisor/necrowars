﻿#if Client
using ProtoBuf;
#endif
using System.Collections.Generic;
using System.Runtime.Serialization;
using Common;
using Common.DTO;

namespace ClientProtocol
{
#if Client
    [ProtoContract]
#else
    [DataContract]
#endif
    public class NewItemsEvent
    {
#if Client
        [ProtoMember(1)]
#else
        [DataMember(Order = 1)]
#endif
        public List<Item> Items = new List<Item>();
    }
}
