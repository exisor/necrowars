﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using MasterServer.REST;
using MasterServer.REST.Handlers;

namespace MasterServer.Test
{
    [TestFixture]
    public class RESTTest
    {
        [Test]
        public async Task BaseListener()
        {
            var restServer = new RESTServer(new HttpsListener(new RequestHandlersCollection(new List<IRequestHandler>
            {
                new ErrorRequestHandler()
            })));
            restServer.Start(new IPEndPoint(IPAddress.Parse("127.0.0.1"), 8098));
            await Task.Delay(50000);
            restServer.Stop();
        }
    }
}
