﻿using System;
using System.IO;
using System.Threading.Tasks;
using Common;
using Newtonsoft.Json;

namespace LogConverter
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                string inputFile = "";
                string outputFile = "";
                if (args.Length < 1 || args.Length > 2) throw new ArgumentException("Incorrect arguments\n usage : LogConverter input.log output.log[optional]");
                if (args.Length == 1)
                {
                    inputFile = args[0];
                    var lastPoint = inputFile.LastIndexOf(".", StringComparison.CurrentCulture);

                    outputFile = inputFile.Substring(0, lastPoint) + "_output" + inputFile.Substring(lastPoint);
                }
                if (args.Length == 2)
                {
                    inputFile = args[0];
                    outputFile = args[1];
                }
                if (!File.Exists(inputFile)) throw new ArgumentException($"File '{inputFile} not exists'");
                using (var input = File.OpenRead(inputFile))
                using (var output = File.Create(outputFile))
                {
                    var converter = new LogConverter(input, output);
                    Console.WriteLine("Starting coversion\n");
                    converter.Convert();
                    while (!converter.Finished)
                    {
                        Task.Delay(30).Wait();
                        Console.Write($"\rProcessing line : [{converter.CurrentLine}]");
                    }
                    Console.WriteLine("\nFinished");
                    Console.ReadKey();
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                Console.ReadKey();
            }
        }
    }

    public class LogConverter
    {
        private readonly Stream _input;
        private readonly Stream _output;
        private bool _completed;
        private int _line = 0;
        private const string DELIMETER = "Sending data : [";
        private const string INCOMING_DELIMETER = "Incoming request : [";
        public LogConverter(Stream input, Stream output)
        {
            _input = input;
            _output = output;
        }

        public void Convert()
        {
            if (_completed) throw new InvalidOperationException("Invalid operation");
            _completed = false;
            Task.Run(async () =>
            {
                await ConvertInternal();
                _completed = true;
            }).ConfigureAwait(false);
        }

        private async Task ConvertInternal()
        {
            using (var r = new StreamReader(_input))
            using(var w = new StreamWriter(_output))
            {
                while (!r.EndOfStream)
                {
                    _line++;
                    var line = await r.ReadLineAsync();
                    if (line.Contains(DELIMETER))
                    {
                        line = DecodeLine(line, DELIMETER);
                    }
                    if (line.Contains(INCOMING_DELIMETER))
                    {
                        line = DecodeLine(line, INCOMING_DELIMETER);
                    }
                    await w.WriteAsync(line + Environment.NewLine);
                }
            }
        }

        private string DecodeLine(string line, string delimeter)
        {
            var index = line.IndexOf(delimeter, StringComparison.CurrentCulture);
            var data = line.Substring(index + delimeter.Length);
            var splitData = data.Split(' ');

            if (splitData.Length != 2) throw new InvalidDataException();

            var messageName = splitData[0];

            var messageType = Type.GetType($"ClientProtocol.{messageName},ClientProtocol");
            if (messageType == null) throw new InvalidDataException($"Unknown message : {messageName}");

            var messageData = splitData[1];
            var messageObject = ProtobufSerializer.Deserialize(messageType, messageData);

            return line.Substring(0, index + delimeter.Length) + Environment.NewLine + messageType + Environment.NewLine + GetObjectDescription(messageObject);
        }

        private string GetObjectDescription(object data)
        {
            return JsonConvert.SerializeObject(data, Formatting.Indented);
        }

        public bool Finished => _completed;
        public int CurrentLine => _line;
    }
}
