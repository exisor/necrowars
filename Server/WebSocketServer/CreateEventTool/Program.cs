﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using Common;
using Newtonsoft.Json.Linq;

namespace CreateEventTool
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                var connectionString = args[0];
                var messageId = long.Parse(args[1]);
                var items = args[2];
                var userIds = new List<long>();
                for (var i = 3; i < args.Length; ++i)
                {
                    userIds.Add(long.Parse(args[i]));
                }
                AddMessage(connectionString, messageId, items, userIds);
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                Console.WriteLine("[=============================ERROR===============================]");
                PrintUsage();
            }
        }

        private static void AddMessage(string connectionString, long messageId, string items, List<long> userIds)
        {
            var split = connectionString.Split(':');
            var c = new TcpClient();
            c.Connect(split[0], int.Parse(split[1]));
            var jitems = (JArray)JObject.Parse(items)["items"];
            var itemList = jitems.Select(jitem => (long) jitem["id"]).ToList();
            using (var s = c.GetStream())
            {
                var datastring = ProtobufSerializer.MakeStringMessage(new ServerProtocol.CreatePlayerEventForUsers
                {
                    EventId = messageId, 
                    ItemBaseIds = itemList,
                    UserIds = userIds,
                });
                var data = Encoding.ASCII.GetBytes(datastring);
                s.Write(data, 0, data.Length);
            }
            c.Close();
        }

        private static void PrintUsage()
        {
            Console.WriteLine("USAGE : CreateEventTool [string connection_string] [long messageId] [json-string items] userids...");
        }
    }
}
