﻿using System;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using Common;
using ServerProtocol;
using SuperSocket.SocketBase;
using SuperSocket.SocketBase.Protocol;

namespace ServerCommon
{
    public abstract class ServerBase<TSession, TServer> : AppServer<TSession>
        where TServer : ServerBase<TSession, TServer>
        where TSession : AppSession<TSession, StringRequestInfo>, new()
    {
        public abstract EndPoint MasterEndPoint { get; }
        public abstract EndPoint ClientEndPoint { get; }
        protected TSession MasterSession;
        protected abstract ServerRole Role { get; }
        protected abstract string Game { get; }
        protected override async void OnStarted()
        {
            base.OnStarted();
            var masterEndPointString = Config.Options.Get("masterEndpoint");
            EndPoint masterEndpoint;
            if (string.IsNullOrEmpty(masterEndPointString))
            {
                masterEndpoint = MasterEndPoint;
            }
            else
            {
                var split = masterEndPointString.Split(new string[] {":"}, StringSplitOptions.None);

                masterEndpoint = new IPEndPoint(IPAddress.Parse(split[0]), int.Parse(split[1]));
            }
            MasterSession = (TSession)(await ((IActiveConnector) this).ActiveConnect(masterEndpoint)).Session;
            Logger.Info("Connected to master server");

            var endpointInfo = GetEndpointInfo(ClientEndPoint);

            MasterSession.Send(ProtobufSerializer.MakeStringMessage(new RegisterServerOnMaster
            {
                Game = Game,
                Address = endpointInfo.address,
                Port = endpointInfo.port,
                Role = Role,
            }));
        }

        protected struct EndPointInfo
        {
            public string address;
            public int port;
        }

        private EndPointInfo GetEndpointInfo(EndPoint endpoint)
        {
            var retval = new EndPointInfo();
            var ipendpoint = ClientEndPoint as IPEndPoint;
            if (ipendpoint != null)
            {
                retval.address = ipendpoint.Address.ToString();
                retval.port = ipendpoint.Port;
                return retval;
            }
            var dnsendpoint = ClientEndPoint as DnsEndPoint;
            if (dnsendpoint != null)
            {
                retval.address = dnsendpoint.Host;
                retval.port = dnsendpoint.Port;
                return retval;
            }
            return retval;
        }

        protected EndPoint CreateEndpoint(EndPointInfo info)
        {
            EndPoint retval;
            IPAddress ipaddress;
            if (IPAddress.TryParse(info.address, out ipaddress))
            {
                retval = new IPEndPoint(ipaddress, info.port);
            }
            else
            {

                retval = new DnsEndPoint(info.address, info.port, AddressFamily.InterNetwork);
            }
            return retval;
        }

    }
}
