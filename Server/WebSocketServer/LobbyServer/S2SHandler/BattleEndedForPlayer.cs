﻿using System;
using System.Collections.Generic;
using System.Linq;
using Autofac;
using Common;
using LobbyServer.BL;
using LobbyServer.Models;
using LobbyServer.Services;
using SuperSocket.SocketBase.Command;
using SuperSocket.SocketBase.Protocol;
using Chest = LobbyServer.Models.Chest;
using static Common.Util;

namespace LobbyServer.S2SHandler
{
    public class BattleEndedForPlayer : StringCommandBase<Session>
    {
        private readonly PlayersService _players = WebServer.Context.Resolve<PlayersService>();
        private readonly Leaderboard _leaderboard = WebServer.Context.Resolve<Leaderboard>();

        private readonly DefferedCommand<OpenChestForCharacterCommand> OpenChest = new DefferedCommand<OpenChestForCharacterCommand>();

        private readonly DefferedCommand<LevelupCharacterCommand> LevelUp = new DefferedCommand<LevelupCharacterCommand>();
        private readonly DefferedCommand<SetCharacterExperienceCommand> SetExp = new DefferedCommand<SetCharacterExperienceCommand>();
        private readonly DefferedCommand<ProgressAchievementCommand> Achievement = new DefferedCommand<ProgressAchievementCommand>();
#if DAILY_QUESTS
        private readonly DefferedCommand<ProgressDailyQuestCommand> DailyQuest = new DefferedCommand<ProgressDailyQuestCommand>();
#endif
        private readonly DefferedCommand<SetPlayerGoldCommand> SetPlayerGold = new DefferedCommand<SetPlayerGoldCommand>();
        private readonly DefferedCommand<SetCharacterExperienceCommand> SetExperience = new DefferedCommand<SetCharacterExperienceCommand>();
        private readonly DefferedCommand<LevelupOGZNotifyCommand> NotifyLvlupOGZ = new DefferedCommand<LevelupOGZNotifyCommand>();
        private const long CHEST_COMMON_ID = 1;
        private const long CHEST_RARE_ID = 2;
        private const long CHEST_EPIC_ID = 3;
        private const long CHEST_LEGENDARY_ID = 4;

        private const long ACHIEVEMENT_UNITS_KILLED_ID = 3;
        private const long ACHIEVEMENT_ARMY_SIZE = 4;
        private const long ACHIEVEMENT_TOP_PLAYER = 7;

        private const long DAILY_QUEST_KILLS_ID = 1;
        public override async void ExecuteCommand(Session session, StringRequestInfo requestInfo)
        {
            try
            {
                var message = ProtobufSerializer.Deserialize<ServerProtocol.BattleEndedForPlayer>(requestInfo.Body);

                await _leaderboard.PostResult(message);

                var playerSession = _players[message.PlayerId] ?? await OfflineWebSession.Get(message.PlayerId);

                var character = playerSession.Player.Characters[message.CharacterId];
                if (character == null)
                {
                    // should never happen
                    return;
                }

                // Gold
                var boostedGold = message.GoldGained;
                if (playerSession.Player.Effects.HasEffect(EffectType.GoldPotion))
                {
                    boostedGold *= 2;
                }

                // Exp
                var boostedExp = message.ExpGained;
                if (playerSession.Player.Effects.HasEffect(EffectType.ExpPotion))
                {
                    boostedExp *= 2;
                }
                var totalexp = character.Experience + boostedExp;

                var chests = new List<IChest>();
                var chestBaseId = 0L;
                if (message.Points >= GetChestPointsForLevel(character.Level, CHEST_LEGENDARY_ID))
                {
                    var pointsForOneChest = GetChestPointsForLevel(character.Level, CHEST_LEGENDARY_ID);
                    var quantity = (int)(message.Points/pointsForOneChest);
                    chests.AddRange(Enumerable.Range(0, quantity).Select(_ => Chest.New(CHEST_LEGENDARY_ID, character.Level)));
                    chestBaseId = CHEST_LEGENDARY_ID;
                }
                else if (message.Points >= GetChestPointsForLevel(character.Level, CHEST_EPIC_ID))
                {
                    chests.Add(Chest.New(CHEST_EPIC_ID, character.Level));
                    chestBaseId = CHEST_EPIC_ID;
                }
                else if (message.Points >= GetChestPointsForLevel(character.Level, CHEST_RARE_ID))
                {
                    chests.Add(Chest.New(CHEST_RARE_ID, character.Level));
                    chestBaseId = CHEST_RARE_ID;
                }
                else if (message.Points >= GetChestPointsForLevel(character.Level, CHEST_COMMON_ID))
                {
                    chests.Add(Chest.New(CHEST_COMMON_ID, character.Level));
                    chestBaseId = CHEST_COMMON_ID;
                }

                playerSession.Send(new ClientProtocol.BattleEndedEvent
                {
                    ArmySize = message.ArmySize,
                    ExpGained = boostedExp,
                    GoldGained = boostedGold,
                    Kills = message.Kills,
                    Points = message.Points,
                    TimeAlive = message.TimeAlive,
                    ChestRewardId = chestBaseId,
                    ChestQuantity = chests.Count
                });
                bool levelup = false;
                while (totalexp > character.RequiredExperience)
                {
                    totalexp -= character.RequiredExperience;

                    await LevelUp.Command.Execute(playerSession, character);
                    levelup = true;
                }
                await SetExperience.Command.Execute(playerSession, character, totalexp);
                await SetPlayerGold.Command.Execute(playerSession, playerSession.Player, playerSession.Player.Gold + boostedGold);
                // tutorial
                if (character.Level == 1)
                {
                    await LevelUp.Command.Execute(playerSession, character);
                    await SetExp.Command.Execute(playerSession, character, 0);
                }

                foreach (var chest in chests)
                {
                    await OpenChest.Command.Execute(playerSession, playerSession.Player, character, chest);
                }

                // achievements
                // Units killed

                await
                    Achievement.Command.Execute(playerSession, playerSession.Player, ACHIEVEMENT_UNITS_KILLED_ID,
                        message.Kills);

                // Army size

                var armySizeAchievement = playerSession.Player.Achievements[ACHIEVEMENT_ARMY_SIZE];
                if (armySizeAchievement == null)
                {
                    await
                        Achievement.Command.Execute(playerSession, playerSession.Player, ACHIEVEMENT_ARMY_SIZE,
                            message.ArmySize);
                }
                else if (armySizeAchievement.Value < message.ArmySize)
                {
                    await
                        Achievement.Command.Execute(playerSession, playerSession.Player, ACHIEVEMENT_ARMY_SIZE,
                            message.ArmySize - armySizeAchievement.Value);
                }

                // Top player
                if (message.Top)
                {
                    await Achievement.Command.Execute(playerSession, playerSession.Player, ACHIEVEMENT_TOP_PLAYER, 1);
                }

                // daily quests
#if DAILY_QUESTS
                // kills
                if (playerSession.Player.QuestId == DAILY_QUEST_KILLS_ID)
                {
                    await DailyQuest.Command.Execute(playerSession, message.Kills);
                }
#endif

                if (levelup && playerSession.Metadata.OGZId > 0)
                {
                    await NotifyLvlupOGZ.Command.Execute(playerSession, character.Level);
                }

            }
            catch (Exception e)
            {
                session.Logger.Error($"{e.Message}\n{e.StackTrace}");
            }
        }
    }
}
