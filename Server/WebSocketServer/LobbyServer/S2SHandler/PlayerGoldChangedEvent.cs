﻿
using Autofac;
using Common;
using LobbyServer.BL;
using LobbyServer.Models;
using LobbyServer.Services;
using SuperSocket.SocketBase.Command;
using SuperSocket.SocketBase.Protocol;

namespace LobbyServer.S2SHandler
{
    public class PlayerGoldChangedEvent : StringCommandBase<Session>
    {
        private readonly PlayersService _players = WebServer.Context.Resolve<PlayersService>();
        private readonly SetPlayerGoldCommand SetPlayerGold = WebServer.Context.Resolve<SetPlayerGoldCommand>();
        public override async void ExecuteCommand(Session session, StringRequestInfo requestInfo)
        {
            var message = ProtobufSerializer.Deserialize<ServerProtocol.PlayerGoldChangedEvent>(requestInfo.Body);
            var playerSession = _players[message.PlayerId] ?? await OfflineWebSession.Get(message.PlayerId);
            var player = playerSession.Player;
            var boostedGold = message.NewGold;
            if (player.Effects.HasEffect(EffectType.GoldPotion))
            {
                boostedGold *= 2;
            }
            await SetPlayerGold.Execute(playerSession, player, player.Gold + boostedGold);
        }
    }
}
