﻿using Autofac;
using Common;
using LobbyServer.Services;
using SuperSocket.SocketBase.Command;
using SuperSocket.SocketBase.Protocol;

namespace LobbyServer.S2SHandler
{
    public class ToBattleEvent : StringCommandBase<Session>
    {
        private readonly BattleManager _battleManager = WebServer.Context.Resolve<BattleManager>();
        public override void ExecuteCommand(Session session, StringRequestInfo requestInfo)
        {
            var message = ProtobufSerializer.Deserialize<ClientProtocol.ToBattleEvent>(requestInfo.Body);
            var userSession = _battleManager.BattleFound(message.CharacterId);
            userSession?.Send(message);
        }
    }
}
