﻿using System;
using System.Collections.Generic;
using System.Linq;
using Autofac;
using Common;
using LobbyServer.BL;
using LobbyServer.Data;
using LobbyServer.Services;
using SuperSocket.SocketBase.Command;
using SuperSocket.SocketBase.Protocol;

namespace LobbyServer.S2SHandler
{
    public class CreatePlayerEventForUsers : StringCommandBase<Session>
    {
        private readonly PlayersService _playersService = WebServer.Context.Resolve<PlayersService>();
        private readonly IPlayerEventsRepository _playerEventsRepository = WebServer.Context.Resolve<IPlayerEventsRepository>();
        private readonly DefferedCommand<CreateItemForUserCommand> CreateItem = new DefferedCommand<CreateItemForUserCommand>();
        private readonly DefferedCommand<ApplyPlayerEventCommand> ApplyEvent = new DefferedCommand<ApplyPlayerEventCommand>();
        public override async void ExecuteCommand(Session session, StringRequestInfo requestInfo)
        {
            try
            {
                var message = ProtobufSerializer.Deserialize<ServerProtocol.CreatePlayerEventForUsers>(requestInfo.Body);
                foreach (var messageUserId in message.UserIds)
                {
                    try
                    {
                        var playerSession = _playersService[messageUserId];
                        if (playerSession == null)
                        {
                            playerSession = await OfflineWebSession.Get(messageUserId);
                        }
                        var character = playerSession.Player.Characters.FirstOrDefault();
                        if (character == null) continue;

                        var level = character.Level;
                        var itemTemplates =
                            message.ItemBaseIds.Select(
                                id => new CreateItemForUserCommand.ItemTemplate(id, level, 1));
                        var items = new List<Common.DTO.Item>();
                        foreach (var itemTemplate in itemTemplates)
                        {
                            await
                                CreateItem.Command.Execute(playerSession, playerSession.Player, itemTemplate,
                                    item => items.Add(item.DTO));
                        }
                        var ev = new PlayerEvent
                        {
                            Items = ProtobufSerializer.SerializeBinary(items),
                            MessageId = message.EventId,
                            UserId = messageUserId
                        };
                        if (playerSession is OfflineWebSession)
                        {
                            await _playerEventsRepository.Insert(ev);
                        }
                        else
                        {
                            await ApplyEvent.Command.Execute(playerSession, playerSession.Player, ev);
                        }
                    }
                    catch (Exception e)
                    {
                        session.Logger.Error(e);
                    }
                }
            }
            catch (Exception e)
            {
                session.Logger.Error(e);
            }
        }
    }
}
