﻿using System.Linq;
using Autofac;
using Common;
using LobbyServer.BL;
using LobbyServer.Data;
using LobbyServer.Models;
using LobbyServer.Services;
using SuperSocket.SocketBase.Command;
using SuperSocket.SocketBase.Protocol;

namespace LobbyServer.S2SHandler
{
    public class CharacterExpChangedEvent : StringCommandBase<Session>
    {
        private readonly PlayersService _players = WebServer.Context.Resolve<PlayersService>();
        private readonly DefferedCommand<LevelupCharacterCommand> Levelup = new DefferedCommand<LevelupCharacterCommand>();

        private readonly DefferedCommand<SetCharacterExperienceCommand> SetExperience = new DefferedCommand<SetCharacterExperienceCommand>();

        public override async void ExecuteCommand(Session session, StringRequestInfo requestInfo)
        {
            var message = ProtobufSerializer.Deserialize<ServerProtocol.CharacterExpChangedEvent>(requestInfo.Body);
            var playerSession = _players[message.UserId] ?? await OfflineWebSession.Get(message.UserId);

            var player = playerSession.Player;
            var character = player.Characters[message.CharacterId];
            var boostedExp = message.CurrentExp;
            if (player.Effects.HasEffect(EffectType.ExpPotion))
            {
                boostedExp *= 2;
            }
            var totalexp = character.Experience + boostedExp;
            while (totalexp > character.RequiredExperience)
            {
                totalexp -= character.RequiredExperience;

                await Levelup.Command.Execute(playerSession, character);
            }
            await SetExperience.Command.Execute(playerSession, character, totalexp);
        }
    }
}
