﻿using Common;
using SuperSocket.SocketBase.Command;
using SuperSocket.SocketBase.Protocol;

namespace LobbyServer.S2SHandler
{
    public class RequestServerResponse : StringCommandBase<Session>
    {
        public override async void ExecuteCommand(Session session, StringRequestInfo requestInfo)
        {
            var message = ProtobufSerializer.Deserialize<ClientProtocol.RequestServerResponse>(requestInfo.Body);
            switch (message.Role)
            {
                case ServerRole.Matchmaker:
                    await session.Server.ConnectToMM(message.Address, message.Port);
                    break;
                default:
                    return;
            }
        }
    }
}
