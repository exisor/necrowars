﻿using System.Linq;
using Autofac;
using Common;
using LobbyServer.BL;
using LobbyServer.Services;
using SuperSocket.SocketBase.Command;
using SuperSocket.SocketBase.Protocol;

namespace LobbyServer.S2SHandler
{
    public class ConsumeItemEvent : StringCommandBase<Session>
    {
        private readonly PlayersService _players = WebServer.Context.Resolve<PlayersService>();

        private readonly DefferedCommand<ChangeItemQuantityCommand> ChangeItemQuantity = new DefferedCommand<ChangeItemQuantityCommand>();
        private readonly DefferedCommand<DestroyItemCommand> DestroyItem = new DefferedCommand<DestroyItemCommand>();
        public override async void ExecuteCommand(Session session, StringRequestInfo requestInfo)
        {
            var message = ProtobufSerializer.Deserialize<ServerProtocol.ConsumeItemEvent>(requestInfo.Body);


            var playerSession = _players[message.PlayerId] ?? await OfflineWebSession.Get(message.PlayerId);

            var itemToConsume = playerSession.Player.Inventory.FirstOrDefault(i => i.BaseId == message.ItemBaseId);
            if (itemToConsume == null)
            {
                session.Logger.Error("Consume non existent item!");
                return;
            }

            await ChangeItemQuantity.Command.Execute(playerSession, itemToConsume, itemToConsume.Quantity - 1);
            if (itemToConsume.Quantity <= 0)
            {
                await DestroyItem.Command.Execute(playerSession, playerSession.Player.Inventory, itemToConsume);
            }
        }
    }
}
