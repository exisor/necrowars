﻿using Autofac;
using Autofac.Features.Metadata;
using Common;
using LobbyServer.Data;
using LobbyServer.Services;
using SuperSocket.SocketBase;
using SuperSocket.SocketBase.Command;
using SuperSocket.SocketBase.Protocol;

namespace LobbyServer.S2SHandler
{
    public class UserLoggedIn : StringCommandBase<Session>
    {
        private readonly PlayersService _playersService = WebServer.Context.Resolve<PlayersService>();
        public override void ExecuteCommand(Session session, StringRequestInfo requestInfo)
        {
            var message = ProtobufSerializer.Deserialize<ServerProtocol.UserLoggedIn>(requestInfo.Body);

            if (message.Id == 0)
            {
                session.Logger.Error("S2S.UserLoggedIn : User Id cannot be null");
                return;
            }

            if (message.Game != session.Server.GameName)
            {
                session.Logger.Error($"S2S.UserLoggedIn : incorrect game in request : {message.Game}");
                return;
            }

            if (string.IsNullOrEmpty(message.SessionKey))
            {
                session.Logger.Error("Session key cannot be null or empty");
                return;
            }
            if (message.KongregateId != 0)
            {
                session.Logger.Info($"Kong user id in message : {message.KongregateId}");
            }
            if (message.OGZId != 0)
            {
                session.Logger.Info($"OGZId : {message.OGZId}");
            }
            _playersService[message.Id]?.Close(CloseReason.ServerClosing);
            var info = new UserService.SessionInfo
            {
                sessonKey = message.SessionKey,
                metadata = {KongregateId = message.KongregateId, OGZId = message.OGZId}
            };
            session.Logger.Info($"Registering new session for id : {message.Id} with key : {message.SessionKey} | KongId : {info.metadata.KongregateId}");
            ((Server)session.AppServer).Users.CreateSession(message.Id, info);
        }
    }
}
