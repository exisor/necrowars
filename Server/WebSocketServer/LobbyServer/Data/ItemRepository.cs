﻿using System.Collections.Generic;
using System.Linq;
using Dapper;
using Dapper.FastCrud;
using DAL;

namespace LobbyServer.Data
{
    public class ItemRepository : CrudRepository<Item>, IItemRepository
    {
        public ItemRepository(IDataProvider dataProvider) : base(dataProvider)
        {
        }

        public List<Item> GetEquippedCharacterItems(long id)
        {
            List<Item> retval = null;
            DataProvider.Transaction(_ =>
            {
                retval = _.Find<Item>(builder => builder.Where($"character_id=@CharacterId").WithParameters(new {CharacterId = id})).ToList();
            });
            return retval;
        }
    }
}