﻿using System.Collections.Generic;
using System.Runtime.InteropServices;
using System.Threading.Tasks;
using DAL;

namespace LobbyServer.Data
{
    public class PlayerEventsRepository : CrudRepository<PlayerEvent>, IPlayerEventsRepository
    {
        public PlayerEventsRepository(IDataProvider dataProvider) : base(dataProvider)
        {
        }

    }
}