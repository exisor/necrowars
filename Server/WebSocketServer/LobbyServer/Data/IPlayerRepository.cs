﻿using System.Threading.Tasks;
using DAL;

namespace LobbyServer.Data
{
    public interface IPlayerRepository : ICrudRepository<Player>
    {
        Task Update(Common.DTO.Player dto, long id);
    }
}
