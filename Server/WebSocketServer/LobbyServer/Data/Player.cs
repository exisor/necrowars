﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using DAL;

namespace LobbyServer.Data
{
    [Table("users")]
    public partial class Player : IIdentifiedData
    {
        [Key]
        [Column("id")]
        public virtual long Id { get; set; }
        [Column("gold")]
        public virtual long Gold { get; set; }
        [Column("currency")]
        public virtual long Currency { get; set; }
        [Column("username")]
        public virtual string Username { get; set; }
        [Column("AdditionalInventorySlots")]
        public virtual int AdditionalInventorySlots { get; set; }
        [Column("TutorialPhase")]
        public virtual int TutorialPhase { get; set; }
        [Column("LastLogin")]
        public virtual DateTime LastLogin { get; set; }
        [Column("DailyRound")]
        public virtual int DailyRound { get; set; }
        [Column("AchievementsData")]
        public virtual byte[] AchievementsData { get; set; }
        [Column("QuestProgress")]
        public virtual int QuestProgress { get; set; }
        [Column("LastUpdate")] 
        public virtual DateTime LastUpdate { get; set; }
        [Column("CurrencyBought")]
        public virtual int CurrencyBought { get; set; }
        public virtual IEnumerable<Character> Characters { get; set; }
        public virtual IEnumerable<Item> Inventory { get; set; }
        public virtual IEnumerable<Effect> Effects { get; set; }
        public virtual IEnumerable<PlayerEvent> Events { get; set; }
    }
}
