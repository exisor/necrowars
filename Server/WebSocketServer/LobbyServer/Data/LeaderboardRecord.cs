﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using DAL;

namespace LobbyServer.Data
{
    [Table("leaderboards")]
    public partial class LeaderboardRecord : IIdentifiedData
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Column("id")]
        public virtual long Id { get; set; }

        [Column("character_id")] // foreign key
        public virtual long CharacterId { get; set; }

        [Column("army_size")]
        public virtual int ArmySize { get; set; }

        [Column("top_time")]
        public virtual int TopTime { get; set; }

        [Column("top_gold")]
        public virtual long TopGold { get; set; }

        [Column("kills")]
        public virtual int Kills { get; set; }

        [Column("points")]
        public virtual int Points { get; set; }

        [Column("date")]
        public virtual DateTime Date { get; set; }
    }
}
