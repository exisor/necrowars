﻿using DAL;

namespace LobbyServer.Data
{
    public class GameDatabaseProvider : DapperDataProvider
    {
        private const string HOST =
#if DEBUG
            "192.168.0.104"
#else
            "192.168.10.1"
#endif
            ;

        private const int PORT = 5432;
        private const string DATABASE = "necro";
        private const string USERNAME =
#if DEBUG
            "exisor"
#else
            "exisor"
#endif
            ;
        private const string PASSWORD = "Zaebali1";
        public GameDatabaseProvider() : base($@"Server={HOST};Port={PORT};Database={DATABASE};Uid={USERNAME};Pwd={PASSWORD};")
        {
        }

        public GameDatabaseProvider(string host, int port, string database, string username, string password) : base(host, port, database, username, password)
        {
        }
    }
}
