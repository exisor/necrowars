﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using DAL;

namespace LobbyServer.Data
{
    [Table("effects")]
    public class Effect : IIdentifiedData
    {
        [Column("id")]
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long Id { get; set; }
        [Column("user_id")]
        public long UserId { get; set; } // Foreign key
        /// <summary>
        /// EffectBase is casted <value>EffectType</value> enum
        /// </summary>
        [Column("effect_base")]
        public long EffectBase { get; set; }
        [Column("started")]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public DateTime Started { get; set; }
        // Duration in SECONDS

        /// <summary>
        /// Duration in seconds
        /// </summary>
        [Column("duration")]
        public long Duration { get; set; }

        public TimeSpan Estimated =>  Started + TimeSpan.FromSeconds(Duration) - DateTime.Now;
    }
}
