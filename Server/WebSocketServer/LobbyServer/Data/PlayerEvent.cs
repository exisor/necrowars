﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using DAL;

namespace LobbyServer.Data
{
    [Table("playerevents")]
    public partial class PlayerEvent : IIdentifiedData
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Column("id")]
        public virtual long Id { get; set; }
        [Column("userid")]
        public virtual long UserId { get; set; } // foreign key
        [Column("messageid")]
        public virtual long MessageId { get; set; }
        [Column("items")]
        public virtual byte[] Items { get; set; } // foreign key
    }
}
