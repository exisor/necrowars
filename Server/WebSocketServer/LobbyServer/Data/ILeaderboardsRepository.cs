﻿
using System.Collections.Generic;
using System.Threading.Tasks;
using DAL;

namespace LobbyServer.Data
{
    public interface ILeaderboardsRepository : ICrudRepository<LeaderboardRecord>
    {
        Task<List<LeaderboardRecord>> GetAll();
        Task<LeaderboardRecord> GetCharacterRecord(long characterId);
    }
}
