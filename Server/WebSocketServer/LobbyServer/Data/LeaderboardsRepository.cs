﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Dapper;
using Dapper.FastCrud;
using DAL;

namespace LobbyServer.Data
{
    public class LeaderboardsRepository : CrudRepository<LeaderboardRecord>, ILeaderboardsRepository
    {
        public LeaderboardsRepository(IDataProvider dataProvider) : base(dataProvider)
        {
        }

        public async Task<List<LeaderboardRecord>> GetAll()
        {
            List<LeaderboardRecord> retval = null;
            await DataProvider.Transaction(async conn =>
            {
                retval = (await conn.FindAsync<LeaderboardRecord>()).ToList();
            });
            return retval;
        }

        public async Task<LeaderboardRecord> GetCharacterRecord(long characterId)
        {
            var cmd = @"SELECT * FROM leaderboards WHERE character_id = @Id;";
            LeaderboardRecord retval = null;
            await DataProvider.Transaction(async conn =>
            {
                retval = (await conn.QueryAsync<LeaderboardRecord>(cmd, new {Id = characterId})).FirstOrDefault();
            });
            return retval;
        }
    }
}