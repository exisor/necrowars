﻿using DAL;

namespace LobbyServer.Data
{
    public class CharacterRepository : CrudRepository<Character>, ICharacterRepository
    {
        public CharacterRepository(IDataProvider dataProvider) : base(dataProvider)
        {
        }
    }
}