﻿using System.Collections.Generic;
using System.Threading.Tasks;
using DAL;

namespace LobbyServer.Data
{
    public interface IPlayerEventsRepository : ICrudRepository<PlayerEvent>
    {
    }

    public class StubPlayerEventsRepository : IPlayerEventsRepository
    {
        public async Task<PlayerEvent> Get(long id)
        {
            return null;
        }

        public PlayerEvent GetSync(long id)
        {
            return null;
        }

        public Task Insert(PlayerEvent data)
        {
            return Task.CompletedTask;
        }

        public Task Update(PlayerEvent data)
        {
            return Task.CompletedTask;
        }

        public Task Delete(PlayerEvent data)
        {
            return Task.CompletedTask;
        }

        public void UpdateSync(PlayerEvent data)
        {
        }

        public void InsertSync(PlayerEvent data)
        {
        }

        public void DeleteSync(PlayerEvent data)
        {
        }
    }
}
