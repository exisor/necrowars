﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using DAL;

namespace LobbyServer.Data
{
    [Table("characters")]
    public partial class Character : IIdentifiedData
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Column("id")]
        public virtual long Id { get; set; }
        [Column("user_id")]
        public virtual long UserId { get; set; } // foreign key
        [Column("base_id")]
        public virtual long BaseId { get; set; }
        [Column("level")]
        public virtual int Level { get; set; }
        [Column("experience")]
        public virtual long Experience { get; set; }
        [Column("ap")]
        public virtual int Ap { get; set; }
        [Column("ability_data")]
        public virtual byte[] AbilityData { get; set; }
        [Column("selected_abilities")]
        public virtual byte[] SelectedAbilities { get; set; }
        [Column("ap_refunded")]
        public virtual int APRefunded { get; set; }
    }
}
