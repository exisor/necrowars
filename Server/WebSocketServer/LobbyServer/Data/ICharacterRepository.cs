﻿using DAL;

namespace LobbyServer.Data
{
    public interface ICharacterRepository : ICrudRepository<Character>
    {
    }
}
