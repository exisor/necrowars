﻿using DAL;

namespace LobbyServer.Data
{
    public interface IEffectRepository : ICrudRepository<Effect>
    {

    }
}
