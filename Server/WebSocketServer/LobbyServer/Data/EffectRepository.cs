﻿using DAL;

namespace LobbyServer.Data
{
    public class EffectRepository : CrudRepository<Effect>, IEffectRepository
    {
        public EffectRepository(IDataProvider dataProvider) : base(dataProvider)
        {
        }
    }
}