﻿using System.Linq;
using System.Threading.Tasks;
using Common;
using Dapper;
using DAL;

namespace LobbyServer.Data
{
    public class PlayerRepository : CrudRepository<Player>, IPlayerRepository
    {
        private readonly ICharacterRepository _characterRepository;
        private readonly IItemRepository _itemRepository;
        private readonly IEffectRepository _effectRepository;
        private readonly IPlayerEventsRepository _playerEventsRepository;
        public PlayerRepository(IDataProvider dataProvider, ICharacterRepository characterRepository, IItemRepository itemRepository, IEffectRepository effectRepository, IPlayerEventsRepository playerEventsRepository) : base(dataProvider)
        {
            _characterRepository = characterRepository;
            _itemRepository = itemRepository;
            _effectRepository = effectRepository;
            _playerEventsRepository = playerEventsRepository;
        }

        public Task Update(Common.DTO.Player dto, long id)
        {
            var player = new Player
            {
                Currency = dto.Currency,
                Gold = dto.Gold,
                Id = id,
                Username = dto.Username,
            };
            return Update(player);
        }

        public override async Task<Player> Get(long id)
        {
            Player retval = null;
            await DataProvider.Transaction(async _ =>
            {
                using (var query = await _.QueryMultipleAsync(
                    "SELECT * FROM users WHERE id = @id; SELECT id FROM characters WHERE user_id = @id; SELECT id FROM items WHERE owner_id = @id; SELECT id FROM effects WHERE user_id = @id;"
+ "SELECT id FROM playerevents WHERE userid = @id;"
                        ,
                    new Player {Id = id}))
                {
                    retval = await query.ReadSingleOrDefaultAsync<Player>();
                    if (retval == null) return;
                    retval.Characters =
                        (await query.ReadAsync<Character>())
                            .Select(c => _characterRepository.GetSync(c.Id))
                            .ToList();
                    retval.Inventory =
                        (await query.ReadAsync<Item>())
                            .Select(i => _itemRepository.GetSync(i.Id))
                            .ToList();
                    retval.Effects =
                        (await query.ReadAsync<Effect>())
                            .Select(e => _effectRepository.GetSync(e.Id))
                            .ToList();
                    retval.Events =
                        (await query.ReadAsync<PlayerEvent>()).Select(e => _playerEventsRepository.GetSync(e.Id))
                            .ToList();
                }
            });
            return retval;
        }
    }
}
