﻿using System.Collections.Generic;
using DAL;

namespace LobbyServer.Data
{
    public interface IItemRepository : ICrudRepository<Item>
    {
        List<Data.Item> GetEquippedCharacterItems(long id);
    }
}
