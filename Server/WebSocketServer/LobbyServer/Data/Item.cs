﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using DAL;

namespace LobbyServer.Data
{
    [Table("items")]
    public partial class Item : IIdentifiedData
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Column("id")]
        public virtual long Id { get; set; }
        [Column("base_id")]
        public virtual long BaseId { get; set; }
        [Column("itemlevel")]
        public virtual int Itemlevel { get; set; }
        [Column("rarity")]
        public virtual short Rarity { get; set; }
        [Column("owner_id")]
        public virtual long OwnerId { get; set; } // Foreign key
        [Column("character_id")]
        public virtual long? CharacterId { get; set; } // Foreign key
        [Column("affixes")]
        public virtual byte[] AffixData { get; set; }
        [Column("enchant")]
        public virtual int Enchant { get; set; }
        [Column("quantity")]
        public virtual int Quantity { get; set; }
    }
}
