﻿using System;
using System.Threading;
using System.Threading.Tasks;
using Autofac;
using ClientProtocol;
using Common;
using LobbyServer.Models;
using LobbyServer.Services;
using SuperSocket.SocketBase;
using SuperSocket.WebSocket;
using SuperSocket.WebSocket.Protocol;
using SuperSocket.WebSocket.SubProtocol;

namespace LobbyServer
{
    public class Syncronization 
    {
        private long _sync;

        public Task<SyncronizationTask> Aquire()
        {
            return  Task.Factory.StartNew(() =>
            {
                var s = new SpinWait();
                while (_sync > 0)
                {
                    s.SpinOnce();
                }
                Interlocked.Increment(ref _sync);
                return new SyncronizationTask(this);
            });
        }

        public void Release()
        {
            Interlocked.Decrement(ref _sync);
        }
    }

    public class SyncronizationTask : IDisposable
    {
        private readonly Syncronization _s;
        public SyncronizationTask(Syncronization s)
        {
            _s = s;
        }

        public void Dispose()
        {
            _s.Release();
        }
    }
    public class WebSession : WebSocketSession<WebSession>
    {
        public struct SessionMetadata
        {
            public long KongregateId;
            public long OGZId;
        }

        public Syncronization Sync { get; } = new Syncronization();
        public ClientTarget Target { get; set; }

        private readonly PlayersService _players = WebServer.Context.Resolve<PlayersService>();
        public WebServer Server => (WebServer) AppServer;

        public long? UserId
        {
            get { return _userId; }
            set
            {
                _userId = value;
            }
        }

        public SessionMetadata Metadata { get; set; }

        private long? _userId;
        public IPlayer Player { get; set; }
        public void Error(object caller, ErrorCode code)
        {
            Logger.Error($"Error from : {RemoteEndPoint} when executing {caller.GetType().Name}: Code :{code}");
            Send(new Error(caller, code));
        }

        public void Fatal(object caller, ErrorCode code)
        {
            Error(caller, code);
            Close(CloseReason.ApplicationError);
        }

        public virtual void Send<T>(T obj)
            where T : class
        {
            var data = ProtobufSerializer.MakeStringMessage(obj);
            Logger.Info($"Sending data : [{data}] to {this.RemoteEndPoint}");
            base.Send(data);
        }

        protected override void HandleUnknownRequest(IWebSocketFragment requestInfo)
        {
            Close();
        }

        protected override void OnSessionClosed(CloseReason reason)
        {
            _players.UnRegisterOnlinePlayer(this);
            Disconnected?.Invoke(this);
            base.OnSessionClosed(reason);
        }

        public event Action<WebSession> Disconnected;

        public void LogMessage(SubRequestInfo message)
        {
            Logger.Info($"Incoming request : [{message.Key} {message.Body}{Environment.NewLine}] from {this.RemoteEndPoint}");
        }
    }
} 