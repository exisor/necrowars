﻿using System.Threading.Tasks;
using LobbyServer.BL;

namespace LobbyServer.Commands
{
#if DEBUG
    public class SetCurrency : AuthorizedCommand<ClientProtocol.SetCurrency>
    {
        private readonly DefferedCommand<SetPlayerCurrencyCommand> SetPlayerCurrency = new DefferedCommand<SetPlayerCurrencyCommand>();
        protected override async Task Execute(WebSession session, ClientProtocol.SetCurrency message)
        {
            await SetPlayerCurrency.Command.Execute(session, session.Player, message.Currency);
        }
    }
#endif
}
