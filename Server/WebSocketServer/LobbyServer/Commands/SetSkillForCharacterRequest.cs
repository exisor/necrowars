﻿using System.Threading.Tasks;
using ClientProtocol;
using LobbyServer.BL;
using LobbyServer.Models;

namespace LobbyServer.Commands
{
#if DEBUG
    public class SetSkillForCharacterRequest : AuthorizedCommand<ClientProtocol.SetSkillForCharacterRequest>
    {
        private readonly DefferedCommand<AddSkillForCharacterCommand> AddSkill = new DefferedCommand<AddSkillForCharacterCommand>();
        private readonly DefferedCommand<SetCharacterAbilityLevelCommand> SetAbilityLevel = new DefferedCommand<SetCharacterAbilityLevelCommand>();
        protected override async Task Execute(WebSession session, ClientProtocol.SetSkillForCharacterRequest message)
        {
            var character = session.Player.Characters[message.CharacterId];
            if (character == null)
            {
                session.Error(this, ErrorCode.AbilityNotAvailable);
                return;
            }

            var skill = character.Abilities[message.SkillId];
            if (skill == null)
            {
                await
                    AddSkill.Command.Execute(session, character,
                        new Ability(new Common.DTO.Ability {Id = message.SkillId, Level = message.Level}));
            }
            else
            {
                await SetAbilityLevel.Command.Execute(session, skill, character, message.Level);
            }
        }
    }
#endif
}
