﻿using System.Threading.Tasks;
using ClientProtocol;
using LobbyServer.BL;

namespace LobbyServer.Commands
{
    public class UseItemRequest : AuthorizedCommand<ClientProtocol.UseItemRequest>
    {
        private readonly DefferedCommand<UseInstantItemCommand> UseItem = new DefferedCommand<UseInstantItemCommand>();
        protected override async Task Execute(WebSession session, ClientProtocol.UseItemRequest message)
        {
            var character = session.Player.Characters[message.CharacterId];
            if (character == null)
            {
                session.Error(this, ErrorCode.CharacterNotExists);
                return;
            }
            var item = session.Player.Inventory[message.ItemId];

            if (item == null)
            {
                session.Error(this, ErrorCode.ItemNotAvailable);
                return;
            }

            await UseItem.Command.Execute(session, session.Player, character, item);
        }
    }
}
