﻿using System;
using System.Linq;
using System.Threading.Tasks;
using ClientProtocol;
using Common.DTO;
using Common.Resources;
using LobbyServer.BL;
using LobbyServer.Models;

namespace LobbyServer.Commands
{
    public class EnchantAffixRequest : AuthorizedCommand<ClientProtocol.EnchantAffixRequest>
    {
        private readonly DefferedCommand<SetPlayerGoldCommand> SetGold = new DefferedCommand<SetPlayerGoldCommand>();
        private readonly DefferedCommand<SetAffixEnchantCommand> SetAffixEnchant = new DefferedCommand<SetAffixEnchantCommand>();
        protected override async Task Execute(WebSession session, ClientProtocol.EnchantAffixRequest message)
        {
            var item = session.Player.Inventory[message.ItemId];
            if (item == null)
            {
                session.Error(this, ErrorCode.ItemNotAvailable);
                return;
            }

            var affix = item.Affixes.FirstOrDefault(a => a.Id == message.AffixId);
            if (affix == null)
            {
                session.Error(this, ErrorCode.ItemNotAvailable);
                return;
            }

            if (affix.Enchant + 1 > item.Enchant)
            {
                session.Error(this, ErrorCode.ItemNotAvailable);
                return;
            }

            var price = GetPrice(item, affix);

            if (session.Player.Gold < price)
            {
                session.Error(this, ErrorCode.InsufficientResources);
                return;
            }

            await SetGold.Command.Execute(session, session.Player, session.Player.Gold - price);
            await SetAffixEnchant.Command.Execute(session, item, affix.Id, affix.Enchant + 1);
        }

        private static int GetPrice(IItem item, ItemAffix affix)
        {
            return ItemUtilities.GetEnchantAffixPrice(item.ItemLevel, item.Rarity, affix.Enchant).Gold;
        }
    }
}
