﻿
using System.Threading.Tasks;
using LobbyServer.BL;

namespace LobbyServer.Commands
{
#if DEBUG
    public class SetGold : AuthorizedCommand<ClientProtocol.SetGold>
    {
        private readonly DefferedCommand<SetPlayerGoldCommand> SetPlayerGold = new DefferedCommand<SetPlayerGoldCommand>();
        protected override async Task Execute(WebSession session, ClientProtocol.SetGold message)
        {
            await SetPlayerGold.Command.Execute(session, session.Player, message.Gold);
        }
    }
#endif
}
