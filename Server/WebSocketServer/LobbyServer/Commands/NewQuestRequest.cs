﻿#if DAILY_QUEST

using System.Threading.Tasks;
using LobbyServer.BL;

namespace LobbyServer.Commands
{
    public class NewQuestRequest : AuthorizedCommand<ClientProtocol.NewQuestRequest>
    {
        private readonly DefferedCommand<UpdateDailyQuestCommand> UpdateDailyQuest = new DefferedCommand<UpdateDailyQuestCommand>();
#region Overrides of AuthorizedCommand<NewQuestRequest>

        protected override Task Execute(WebSession session, ClientProtocol.NewQuestRequest message)
        {
            return UpdateDailyQuest.Command.Execute(session);
        }

#endregion
    }
}
#endif