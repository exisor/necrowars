﻿using System.Linq;
using System.Threading.Tasks;
using Common.DTO;
using Common.ResourceManager;

namespace LobbyServer.Commands
{
    public class KongregateShopRequest : AuthorizedCommand<ClientProtocol.KongregateShopRequest>
    {
        private readonly IKongregateShopResourceManager _shop = GetInstance<IKongregateShopResourceManager>();

        #region Overrides of AuthorizedCommand<KongregateShopRequest>

        protected override async Task Execute(WebSession session, ClientProtocol.KongregateShopRequest message)
        {
            session.Send(new ClientProtocol.KongregateShopResponse
            {
                Shop = new KongregateShop
                {
                    Items = _shop.All.Select(i => new Common.DTO.KongregateShopItem
                    {
                        Id = i.KongregateId,
                        ItemBaseId = i.ItemBaseId,
                        KongregateCurrencyPrice = i.KongregateCurrencyPrice,
                        Quantity = i.Quantity
                    }).ToList()
                }
            });
        }

        #endregion
    }
}
