﻿using System;
using System.Threading.Tasks;
using Autofac;
using ClientProtocol;
using Common;
using LobbyServer.BL;
using LobbyServer.Services;
using SuperSocket.WebSocket.SubProtocol;

namespace LobbyServer.Commands
{
    public class LobbyLogin : SubCommandBase<WebSession>
    {
        private readonly PlayersService _players = WebServer.Context.Resolve<PlayersService>();
        private readonly DefferedCommand<SetSessionUserIdCommand> SetSessionUserId = new DefferedCommand<SetSessionUserIdCommand>();
        public override async void ExecuteCommand(WebSession session, SubRequestInfo requestInfo)
        {
            try
            {
                var message = ProtobufSerializer.Deserialize<ClientProtocol.LobbyLogin>(requestInfo.Body);
                var users = session.Server.S2S.Users;
                var haveSession = users.RequestSession(message.UserId, message.SessionKey);
                // success
                if (haveSession != null)
                {
                    users.DisposeSession(message.UserId);
                    await SetSessionUserId.Command.Execute(session, message.UserId);
                    session.Metadata = haveSession.metadata;
                    session.Logger.Info($"User logged in. Id : {message.UserId}, KongId : {session.Metadata.KongregateId}, OGZId : {session.Metadata.OGZId}");
                    session.Target = message.Target;
                    _players.RegisterOnlinePlayer(session);
                    session.Send(new LobbyLoggedInEvent());
                }
                else
                {
                    session.Fatal(this, ErrorCode.SessionInvalid);
                }
            }
            catch (Exception e)
            {
                session.Logger.Error(e);
                session.Fatal(this, ErrorCode.OperationFailed);
            }
        }
    }
}
