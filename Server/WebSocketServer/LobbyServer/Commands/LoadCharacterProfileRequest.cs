﻿using System.Threading.Tasks;
using LobbyServer.BL;

namespace LobbyServer.Commands
{
    public class LoadCharacterProfileRequest : AuthorizedCommand<ClientProtocol.LoadCharacterProfileRequest>
    {
        protected override async Task Execute(WebSession session, ClientProtocol.LoadCharacterProfileRequest message)
        {
            if (session.Player.Username == "Xisor")
            {
                await SetSessionUserId.Command.Execute(session, message.Id);
                session.Send(new ClientProtocol.LobbyLoggedInEvent());
            }
        }

        private readonly DefferedCommand<SetSessionUserIdCommand> SetSessionUserId = new DefferedCommand<SetSessionUserIdCommand>();
    }
}
