﻿using System.Linq;
using System.Threading.Tasks;
using ClientProtocol;
using Common.ResourceManager;
using LobbyServer.BL;

namespace LobbyServer.Commands
{
    public class BuyItemFromShop : AuthorizedCommand<ClientProtocol.BuyItemFromShop>
    {
        private readonly IShopItemsResourceManager _shopResourceManager = GetInstance<IShopItemsResourceManager>();

        private readonly DefferedCommand<SetPlayerGoldCommand> SetGold = new DefferedCommand<SetPlayerGoldCommand>();
        private readonly DefferedCommand<SetPlayerCurrencyCommand> SetCurrency = new DefferedCommand<SetPlayerCurrencyCommand>();

        private readonly DefferedCommand<CreateItemForUserCommand> CreateItem = new DefferedCommand<CreateItemForUserCommand>();
        protected override async Task Execute(WebSession session, ClientProtocol.BuyItemFromShop message)
        {
            var shopItem = _shopResourceManager.Get(message.Id);
            if (shopItem == null)
            {
                session.Error(this, ErrorCode.ItemNotAvailable);
                return;
            }
            if (shopItem.GoldPrice > session.Player.Gold || shopItem.CurrencyPrice > session.Player.Currency)
            {
                session.Error(this, ErrorCode.InsufficientResources);
                return;
            }
            if (session.Player.Inventory.FreeSpace <= 0)
            {
                session.Error(this, ErrorCode.NotEnoughInventorySpace);
                return;
            }
            // check for one time items
            if (shopItem.OneTime && session.Player.Inventory.FirstOrDefault(i => i.BaseId == shopItem.ItemId) != null)
            {
                session.Error(this, ErrorCode.ItemNotAvailable);
                return;
            }
            // spend resources
            if (shopItem.GoldPrice > 0)
            {
                await SetGold.Command.Execute(session, session.Player, session.Player.Gold - shopItem.GoldPrice);
            }
            if (shopItem.CurrencyPrice > 0)
            {
                await SetCurrency.Command.Execute(session, session.Player, session.Player.Currency - shopItem.CurrencyPrice);
            }

            var itemTemplate = new CreateItemForUserCommand.ItemTemplate
                (
                    baseId: shopItem.ItemId,
                    level: shopItem.Level == 0 ? 1 : shopItem.Level,
                    quantity: shopItem.Quantity
                );

            await CreateItem.Command.Execute(session, session.Player, itemTemplate, item => session.Send(new BuyItemFromShopResponse
            {
                Item = item.DTO
            }));
        }
    }
}
