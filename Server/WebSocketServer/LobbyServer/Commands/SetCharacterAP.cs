﻿using System.Threading.Tasks;
using ClientProtocol;
using LobbyServer.BL;

namespace LobbyServer.Commands
{
#if DEBUG
    public class SetCharacterAPRequest : AuthorizedCommand<ClientProtocol.SetCharacterAPRequest>
    {
        private readonly DefferedCommand<SetCharacterApCommand> SetCharacterAP = new DefferedCommand<SetCharacterApCommand>();
        protected override async Task Execute(WebSession session, ClientProtocol.SetCharacterAPRequest message)
        {
            var character = session.Player.Characters[message.CharacterId];
            if (character == null)
            {
                session.Error(this, ErrorCode.CharacterNotExists);
                return;
            }
            await SetCharacterAP.Command.Execute(session, character, message.AP);
        }
    }
#endif
}
