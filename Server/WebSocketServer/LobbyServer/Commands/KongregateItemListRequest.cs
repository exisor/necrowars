﻿
using System;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Security;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;
using ClientProtocol;
using Common.DTO;
using Newtonsoft.Json.Linq;

namespace LobbyServer.Commands
{
    public class KongregateItemListRequest : AuthorizedCommand<ClientProtocol.KongregateItemListRequest>
    {
        private readonly Uri _uri = new Uri("https://api.kongregate.com/api/items.json");
        public KongregateItemListRequest()
        {
            ServicePointManager.ServerCertificateValidationCallback = AcceptAllCerts;
        }
        private bool AcceptAllCerts(object sender, X509Certificate certificate, X509Chain chain,
            SslPolicyErrors sslPolicyErrors)
        {
            return true;
        }
        #region Overrides of AuthorizedCommand<KongregateItemListRequest>

        protected override async Task Execute(WebSession session, ClientProtocol.KongregateItemListRequest message)
        {
            var request = WebRequest.CreateHttp(_uri);
            request.ContentType = "application/json";
            request.Method = "POST";
            request.AllowWriteStreamBuffering = true;
            var data = Encoding.ASCII.GetBytes($"{{\"api_key\":\"{Kongregate.API_KEY}\" }}");
            request.ContentLength = data.Length;
            using (var stream = request.GetRequestStream())
            {
                stream.Write(data, 0, data.Length);
            }
            JObject jobj;
            using (var restResponse = await request.GetResponseAsync())
            using (var stream = restResponse.GetResponseStream())
            using (var reader = new StreamReader(stream))
            {
                var responseData = reader.ReadToEnd();
                jobj = JObject.Parse(responseData);
            }
            var success = (bool)jobj["success"];
            if (!success)
            {
                session.Error(this, ErrorCode.OperationFailed);
                return;
            }
            var response = new KongregateItemListResponse
            {
                Items = jobj["items"].Select(t => new KongregateItem
                {
                    Id = (int)t["id"],
                    Identifier = (string)t["identifier"],
                    Name = (string)t["name"],
                    Description = (string)t["description"],
                    Price = (int)t["price"],
                    RemainingUses = 0,
                }).ToList(),
            };
            session.Send(response);
        }

        #endregion
    }
}
