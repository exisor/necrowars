﻿using System;
using System.Linq;
using System.Threading.Tasks;
using ClientProtocol;
using Common.Resources;
using LobbyServer.BL;

namespace LobbyServer.Commands
{
    public class UpgradeItemRequest : AuthorizedCommand<ClientProtocol.UpgradeItemRequest>
    {
        private readonly DefferedCommand<SetPlayerCurrencyCommand> SetCurrency = new DefferedCommand<SetPlayerCurrencyCommand>();
        private readonly DefferedCommand<SetItemLevelCommand> SetItemLevel = new DefferedCommand<SetItemLevelCommand>();
        private readonly DefferedCommand<SetPlayerGoldCommand> SetGold = new DefferedCommand<SetPlayerGoldCommand>();

        private static readonly long[] FixedPriceIds = {5, 6, 7, 8, 9, 10};
        protected override async Task Execute(WebSession session, ClientProtocol.UpgradeItemRequest message)
        {
            var item = session.Player.Inventory[message.ItemId];
            if (item == null)
            {
                session.Error(this, ErrorCode.ItemNotAvailable);
                return;
            }
            var character = session.Player.Characters[message.CharacterId];
            if (character == null)
            {
                session.Error(this, ErrorCode.CharacterNotExists);
                return;
            }
            if (character.Level <= item.ItemLevel) return;

            var price = FixedPriceIds.Contains(item.BaseId)? new ItemUtilities.ComplexPrice(0, 1) : ItemUtilities.GetUpgradeItemPrice(item.ItemLevel, character.Level, item.Rarity, item.Enchant,
                item.Affixes.Sum(a => a.Enchant));

            if (price.Currency > session.Player.Currency)
            {
                session.Error(this, ErrorCode.InsufficientResources);
                return;
            }

            if (price.Gold > session.Player.Gold)
            {
                session.Error(this, ErrorCode.InsufficientResources);
                return;
            }

            await SetGold.Command.Execute(session, session.Player, session.Player.Gold - price.Gold);
            await SetCurrency.Command.Execute(session, session.Player, session.Player.Currency - price.Currency);
            await SetItemLevel.Command.Execute(session, item, character.Level);
        }
    }
}
