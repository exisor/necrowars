﻿using System.Threading.Tasks;
using ClientProtocol;
using Common.ResourceManager;
using LobbyServer.BL;

namespace LobbyServer.Commands
{
    public class CreateCharacterRequest : AuthorizedCommand<ClientProtocol.CreateCharacterRequest>
    {
        private const int MAXIMUM_CHARACTERS_PER_PLAYER = 1;

        private readonly ICharacterResourceManager _characterResourceManager = GetInstance<ICharacterResourceManager>();
        private readonly DefferedCommand<CreateCharacterCommand> CreateCharacter = new DefferedCommand<CreateCharacterCommand>();
        protected override async Task Execute(WebSession session, ClientProtocol.CreateCharacterRequest message)
        {
            // Is user have no free character slots
            if (session.Player.Characters.Count >= MAXIMUM_CHARACTERS_PER_PLAYER)
            {
                session.Error(this, ErrorCode.NoEmptyCharacterSlots);
                return;
            }

            // Checking for correct character id in request
            var characterBase = _characterResourceManager.Get(message.Id);
            if (characterBase == null)
            {
                session.Error(this, ErrorCode.InvalidDataInRequest);
                return;
            }

            await CreateCharacter.Command.Execute(session, session.Player, characterBase);
        }
    }
}
