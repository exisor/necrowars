﻿using System.Threading.Tasks;
using ClientProtocol;
using LobbyServer.BL;

namespace LobbyServer.Commands
{
    public class UnequipItemRequest : AuthorizedCommand<ClientProtocol.UnequipItemRequest>
    {
        private readonly DefferedCommand<UnequipItemCommand> UnequipItem = new DefferedCommand<UnequipItemCommand>();
        protected override async Task Execute(WebSession session, ClientProtocol.UnequipItemRequest message)
        {
            // check for existence
            var item = session.Player.Inventory[message.ItemId];
            if (item == null)
            {
                session.Error(this, ErrorCode.ItemNotAvailable);
                return;
            }
            // check for equipped already
            if (item.Equipped == null)
            {
                session.Error(this, ErrorCode.ItemNotAvailable);
                return;
            }
            
            await UnequipItem.Command.Execute(session, item);
        }
    }
}
