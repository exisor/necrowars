﻿using System;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Security;
using System.Security.Cryptography.X509Certificates;
using System.Threading.Tasks;
using ClientProtocol;
using Common.DTO;
using Newtonsoft.Json.Linq;

namespace LobbyServer.Commands
{
    public class KongregateUserItemsRequest : AuthorizedCommand<ClientProtocol.KongregateUserItemsRequest>
    {


        private static readonly string REQUEST_FORMAT = "https://api.kongregate.com/api/user_items.json?api_key={0}&user_id={1}";
        public KongregateUserItemsRequest()
        {
            ServicePointManager.ServerCertificateValidationCallback = AcceptAllCerts;
        }
        private bool AcceptAllCerts(object sender, X509Certificate certificate, X509Chain chain,
            SslPolicyErrors sslPolicyErrors)
        {
            return true;
        }
        #region Overrides of AuthorizedCommand<KongregateUserItemsRequest>

        protected override async Task Execute(WebSession session, ClientProtocol.KongregateUserItemsRequest message)
        {
            var reqString = string.Format(REQUEST_FORMAT, Kongregate.API_KEY, session.Metadata.KongregateId);
            session.Logger.Info($"Making request : {reqString}");
            var request = WebRequest.CreateHttp(new Uri(reqString));
            request.Method = "GET";
            JObject jobj;
            using (var restResponse = await request.GetResponseAsync())
            using (var stream = restResponse.GetResponseStream())
            using (var reader = new StreamReader(stream))
            {
                var responseData = reader.ReadToEnd();
                jobj = JObject.Parse(responseData);
            }
            var success = (bool)jobj["success"];
            if (!success)
            {
                session.Logger.Info(jobj["error"]);
                session.Error(this, ErrorCode.PlayerNotExists);
                return;
            }
            session.Send(new KongregateUserItemsResponse
            {
                Items = jobj["items"].Select(t => new KongregateItem
                {
                    Id = (int)t["id"],
                    Identifier = (string)t["identifier"],
                    Name = (string)t["name"],
                    Description = (string)t["description"],
                    Price = 0,
                    RemainingUses = (int)t["remaining_uses"]
                }).ToList()
            });
        }

        #endregion
    }
}
