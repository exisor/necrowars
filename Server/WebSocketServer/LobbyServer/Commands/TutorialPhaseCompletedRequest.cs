﻿//#define CHECK_TUTORIAL_PHASE
using System.Linq;
using System.Threading.Tasks;
using ClientProtocol;
using Common.ResourceManager;
using LobbyServer.BL;
using LobbyServer.Models;
using LobbyServer.Services;

namespace LobbyServer.Commands
{
    public class TutorialPhaseCompletedRequest : AuthorizedCommand<ClientProtocol.TutorialPhaseCompletedRequest>
    {
        private readonly DefferedCommand<SetTutorialPhaseCommand> SetTutorialPhase = new DefferedCommand<SetTutorialPhaseCommand>();
        private readonly DefferedCommand<AddItemsForPlayerCommand> AddItem = new DefferedCommand<AddItemsForPlayerCommand>();
        private readonly DefferedCommand<CreateItemForUserCommand> CreateItem = new DefferedCommand<CreateItemForUserCommand>();
        private readonly DefferedCommand<SetPlayerGoldCommand> SetGold = new DefferedCommand<SetPlayerGoldCommand>();
        private readonly ItemGenerator _itemGenerator = GetInstance<ItemGenerator>();
        private readonly IItemResourceManager _itemResourceManager = GetInstance<IItemResourceManager>();
        protected override async Task Execute(WebSession session, ClientProtocol.TutorialPhaseCompletedRequest message)
        {
            var player = session.Player;
#if CHECK_TUTORIAL_PHASE
            if (player.TutorialPhase >= message.PhaseNum)
            {
                session.Error(this, ErrorCode.OperationFailed);
                return;
            }
#endif
            await SetTutorialPhase.Command.Execute(session, player, message.PhaseNum);
            if (message.PhaseNum == 5)
            {
                var uniqueIds = new[] {5, 6, 7, 8, 9, 10};

                var items = uniqueIds.Select(id =>
                {
                    var resource = _itemResourceManager.Get(id);
                    return _itemGenerator.CreateItem(1, resource) as IItem;
                }).ToList();
                await AddItem.Command.Execute(session, session.Player.Inventory, items);

                session.Send(new ChestOpenEvent
                {
                    BaseId = 1,
                    ItemsReceived = items.Select(i => i.DTO).ToList(),
                });
                //await CreateItem.Command.Execute(session, session.Player, new CreateItemForUserCommand.ItemTemplate(
                //    baseId: 11,
                //    level: 1,
                //    quantity: 1), null);
                //await SetGold.Command.Execute(session, session.Player, session.Player.Gold + 2000);
            }
        }
    }
}
