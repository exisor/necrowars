﻿using System.Threading.Tasks;
using ClientProtocol;
using Common;
using Common.ResourceManager;
using LobbyServer.BL;

namespace LobbyServer.Commands
{
    public class SelectAbility : AuthorizedCommand<ClientProtocol.SelectAbility>
    {
        private readonly IAbilityResourceManager _abilityResourceManager = GetInstance<IAbilityResourceManager>();

        private readonly DefferedCommand<SelectCharacterAbilityCommand> SelectCharacterAbility =
            new DefferedCommand<SelectCharacterAbilityCommand>();
        protected override async Task Execute(WebSession session, ClientProtocol.SelectAbility message)
        {
            // Check character available for user
            var character = session.Player.Characters[message.CharacterId];
            if (character == null)
            {
                session.Error(this, ErrorCode.CharacterNotExists);
                return;
            }
            // check ability slot is available
            var abilitySlot = character.AbilitySlots[message.Slot.Slot];
            if (abilitySlot == null)
            {
                session.Error(this, ErrorCode.IncorrectAbilitySlot);
                return;
            }
            // check if ability available
            var ability = character.Abilities[message.Slot.AbilityId];
            if (ability == null || ability.Level == 0)
            {
                session.Error(this, ErrorCode.AbilityNotAvailable);
                return;
            }
            // check if passive
            var resource = _abilityResourceManager.Get(ability.Id);
            if (resource.Type == AbilityType.Passive)
            {
                session.Error(this, ErrorCode.AbilityNotAvailable);
                return;
            }
            // if selected then deselect
            if (ability.SelectedInSlot != null)
            {
                await SelectCharacterAbility.Command.Execute(session, character, ability.SelectedInSlot, null);
            }
            // writing data
            await SelectCharacterAbility.Command.Execute(session, character, abilitySlot, ability);
        }
    }
}
