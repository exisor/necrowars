﻿using System.Linq;
using System.Threading.Tasks;
using ClientProtocol;
using Common.ResourceManager;
using Common.Resources;
using LobbyServer.BL;

namespace LobbyServer.Commands
{
    public class EquipItemRequest : AuthorizedCommand<ClientProtocol.EquipItemRequest>
    {
        private readonly IItemResourceManager _itemResourceManager = GetInstance<IItemResourceManager>();

        private readonly DefferedCommand<UnequipItemCommand> UnequipItem = new DefferedCommand<UnequipItemCommand>();
        private readonly DefferedCommand<EquipItemCommand> EquipItem = new DefferedCommand<EquipItemCommand>();
        protected override async Task Execute(WebSession session, ClientProtocol.EquipItemRequest message)
        {
            var character = session.Player.Characters[message.CharacterId];
            if (character == null)
            {
                session.Error(this, ErrorCode.CharacterNotExists);
                return;
            }

            // check for existence
            var item = session.Player.Inventory[message.ItemId];
            if (item == null)
            {
                session.Error(this, ErrorCode.ItemNotAvailable);
                return;
            }

            // Check item level
            if (item.ItemLevel > character.Level)
            {
                session.Error(this, ErrorCode.CharacterLevelIsTooSmall);
                return;
            }
            // check for equipped already
            if (item.Equipped != null)
            {
                session.Error(this, ErrorCode.ItemNotAvailable);
                return;
            }

            var itemResource = _itemResourceManager.Get(item.BaseId);
            if (!itemResource.Types.Contains(ItemType.Equipment))
            {
                session.Error(this, ErrorCode.ItemNotAvailable);
                return;
            }
            // get item type
            var type = itemResource.Types.First(t => t == ItemType.Body || t == ItemType.Weapon || t == ItemType.Boots || t == ItemType.Gloves || t == ItemType.Offhand || t == ItemType.Head || t == ItemType.VisualHelm);
            // find already equipped item with the same type
            var sameTypeItem =
                character.EquippedItems.FirstOrDefault(i => _itemResourceManager.Get(i.BaseId).Types.Contains(type));
            if (sameTypeItem != null)
            {
                await UnequipItem.Command.Execute(session, sameTypeItem);
            }
            await EquipItem.Command.Execute(session, character, item);
        }
    }
}
