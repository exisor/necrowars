﻿using System.Threading.Tasks;
using ClientProtocol;
using LobbyServer.BL;

namespace LobbyServer.Commands
{
#if DEBUG
    public class RemoveSkillFromCharacterRequest : AuthorizedCommand<ClientProtocol.RemoveSkillFromCharacterRequest>
    {
        private readonly DefferedCommand<RemoveSkillFromCharacterCommand> RemoveSkill = new DefferedCommand<RemoveSkillFromCharacterCommand>();
        protected override async Task Execute(WebSession session, ClientProtocol.RemoveSkillFromCharacterRequest message)
        {
            var character = session.Player.Characters[message.CharacterId];
            if (character == null)
            {
                session.Error(this, ErrorCode.CharacterNotExists);
                return;
            }

            var ability = character.Abilities[message.SkillId];

            if (ability == null)
            {
                session.Error(this, ErrorCode.AbilityNotAvailable);
                return;
            }

            await RemoveSkill.Command.Execute(session, character, ability);
        }

    }
#endif
}
