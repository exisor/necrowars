﻿
using System.Threading.Tasks;
using ClientProtocol;

namespace LobbyServer.Commands
{
    public class CloseRequest : AuthorizedCommand<ClientProtocol.CloseRequest>
    {
        protected override async Task Execute(WebSession session, ClientProtocol.CloseRequest message)
        {
            session.Fatal(this, ErrorCode.AccessDenied);
        }
    }
}
