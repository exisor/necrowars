﻿
using System.Threading.Tasks;
using ClientProtocol;
using LobbyServer.Services;

namespace LobbyServer.Commands
{
    public class RequestBattle : AuthorizedCommand<ClientProtocol.RequestBattle>
    {
        private readonly BattleManager _battleManager = GetInstance<BattleManager>();
        protected override async Task Execute(WebSession session, ClientProtocol.RequestBattle message)
        {
            var character = session.Player.Characters[message.CharacterId];
            if (character == null)
            {
                session.Error(this, ErrorCode.CharacterNotExists);
                return;
            }

            if (session.Player.Inventory.FreeSpace <= 0)
            {
                session.Error(this, ErrorCode.NotEnoughInventorySpace);
                return;
            }
            if (!_battleManager.Queue(session, character))
            {
                session.Error(this, ErrorCode.ServerIsNotAvailable);
            }
        }
    }
}
