﻿using System;
using System.Diagnostics;
using System.Threading.Tasks;
using Autofac;
using ClientProtocol;
using Common;
using SuperSocket.WebSocket.SubProtocol;

namespace LobbyServer.Commands
{
    public abstract class AuthorizedCommand<TMessage> : SubCommandBase<WebSession>
        where TMessage : class
    {
        public override async void ExecuteCommand(WebSession session, SubRequestInfo requestInfo)
        {
            // Authorize
            if (!session.UserId.HasValue)
            {
                session.Fatal(this, ErrorCode.AccessDenied);
                return;
            }
            TMessage message;
            try
            {
                session.LogMessage(requestInfo);
                message = ProtobufSerializer.Deserialize<TMessage>(requestInfo.Body);
            }
            catch (Exception e)
            {
                session.Logger.Error(e.Message);
                session.Fatal(this, ErrorCode.InvalidDataInRequest);
                return;
            }
            try
            {
                using (var _ = await session.Sync.Aquire())
                {
                    await Execute(session, message);
                }
            }
            catch (Exception e)
            {
                session.Logger.ErrorFormat("{0}\n\n{1}", e.Message, e.StackTrace);
                session.Fatal(this, ErrorCode.OperationFailed);
            }
        }

        protected abstract Task Execute(WebSession session, TMessage message);

        protected static T GetInstance<T>()
        {
            return WebServer.Context.Resolve<T>();
        }
    }
}
