﻿#if DEBUG
using System.Threading.Tasks;
using ServerProtocol;

namespace LobbyServer.Commands
{
    public class SetUserMetadata : AuthorizedCommand<ClientProtocol.SetUserMetadata>
    {
        #region Overrides of AuthorizedCommand<SetUserMetadata>

        protected override async Task Execute(WebSession session, ClientProtocol.SetUserMetadata message)
        {
            session.Metadata = new WebSession.SessionMetadata {KongregateId = message.KongregateId};
        }

        #endregion
    }
}
#endif