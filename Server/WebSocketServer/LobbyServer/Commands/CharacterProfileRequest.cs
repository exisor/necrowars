﻿using System.Linq;
using System.Threading.Tasks;
using ClientProtocol;
using LobbyServer.Data;

namespace LobbyServer.Commands
{
    public class CharacterProfileRequest : AuthorizedCommand<ClientProtocol.CharacterProfileRequest>
    {
        private readonly IPlayerRepository _playerRepository = GetInstance<IPlayerRepository>();
        private readonly ICharacterRepository _characterRepository = GetInstance<ICharacterRepository>();
        protected override async Task Execute(WebSession session, ClientProtocol.CharacterProfileRequest message)
        {
            var character = await _characterRepository.Get(message.CharacterId);
            if (character == null)
            {
                session.Error(this, ErrorCode.CharacterNotExists);
                return;
            }
            var player = await _playerRepository.Get(character.UserId);
            var playerModel = new Models.Player(player);
            var characterModel = playerModel.Characters[message.CharacterId];
            session.Send(new CharacterProfileResponse
            {
                Character = characterModel.DTO,
                PlayerName = playerModel.Username,
                Items = playerModel.Inventory.Where(i => i.Equipped == characterModel).Select(i => i.DTO).ToList()
            });
        }
    }
}

