﻿
#if DEBUG
using System.Collections.Generic;
using System.Threading.Tasks;
using Common.ResourceManager;
using LobbyServer.BL;
using LobbyServer.Models;
using LobbyServer.Services;

namespace LobbyServer.Commands
{
    public class CreateItemRequest : AuthorizedCommand<ClientProtocol.CreateItemRequest>
    {
        private readonly ItemGenerator _itemGenerator = GetInstance<ItemGenerator>();
        private readonly IItemResourceManager _itemResourceManager = GetInstance<IItemResourceManager>();
        private readonly DefferedCommand<AddItemsForPlayerCommand> AddItem = new DefferedCommand<AddItemsForPlayerCommand>();
        protected override async Task Execute(WebSession session, ClientProtocol.CreateItemRequest message)
        {
            var resource = _itemResourceManager.Get(message.BaseId);
            var item = _itemGenerator.CreateItem(message.ItemLevel, resource, message.Rarity);

            await AddItem.Command.Execute(session, session.Player.Inventory, new List<IItem> { item});
        }
    }
}

#endif