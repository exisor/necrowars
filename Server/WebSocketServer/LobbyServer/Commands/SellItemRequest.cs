﻿using System.Threading.Tasks;
using ClientProtocol;
using Common.Resources;
using LobbyServer.BL;

namespace LobbyServer.Commands
{
    public class SellItemRequest : AuthorizedCommand<ClientProtocol.SellItemRequest>
    {
        private readonly DefferedCommand<UnequipItemCommand> UnequipItem = new DefferedCommand<UnequipItemCommand>();
        private readonly DefferedCommand<DestroyItemCommand> DestroyItem = new DefferedCommand<DestroyItemCommand>();
        private readonly DefferedCommand<SetPlayerGoldCommand> SetGold = new DefferedCommand<SetPlayerGoldCommand>();

        protected override async Task Execute(WebSession session, ClientProtocol.SellItemRequest message)
        {
            // check if exists
            var item = session.Player.Inventory[message.Id];
            if (item == null)
            {
                session.Error(this, ErrorCode.ItemNotAvailable);
                return;
            }
            // check if equipped
            if (item.Equipped != null)
            {
                await UnequipItem.Command.Execute(session, item);
            }
            // calculate the price
            var price = ItemUtilities.GetPrice(item.ItemLevel, item.Rarity)*item.Quantity;

            await DestroyItem.Command.Execute(session, session.Player.Inventory, item);

            await SetGold.Command.Execute(session, session.Player, session.Player.Gold + price.Gold);
        }
    }
}
