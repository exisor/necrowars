﻿using System;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Security;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;
using ClientProtocol;
using Common.ResourceManager;
using LobbyServer.BL;
using LobbyServer.S2SHandler;
using Newtonsoft.Json.Linq;

namespace LobbyServer.Commands
{
    public class ActivateKongregateItemRequest : AuthorizedCommand<ClientProtocol.ActivateKongregateItemRequest>
    {

        private static readonly string REQUEST_FORMAT = "https://api.kongregate.com/api/user_items.json?api_key={0}&user_id={1}";

        private readonly IKongregateShopResourceManager _kongShop = GetInstance<IKongregateShopResourceManager>();

        private readonly DefferedCommand<CreateItemForUserCommand> CreateItem = new DefferedCommand<CreateItemForUserCommand>();
            
        private readonly DefferedCommand<UpdateCurrencyBoughtCommand> CurrencyBought = new DefferedCommand<UpdateCurrencyBoughtCommand>();
        public ActivateKongregateItemRequest()
        {
            ServicePointManager.ServerCertificateValidationCallback = AcceptAllCerts;
        }
        private bool AcceptAllCerts(object sender, X509Certificate certificate, X509Chain chain,
            SslPolicyErrors sslPolicyErrors)
        {
            return true;
        }
        #region Overrides of AuthorizedCommand<ActivateKongregateItemRequest>

        protected override async Task Execute(WebSession session, ClientProtocol.ActivateKongregateItemRequest message)
        {
            var request = WebRequest.CreateHttp(new Uri(string.Format(REQUEST_FORMAT, Kongregate.API_KEY, session.Metadata.KongregateId)));
            request.Method = "GET";
            JObject jobj;
            using (var restResponse = await request.GetResponseAsync())
            using (var stream = restResponse.GetResponseStream())
            using (var reader = new StreamReader(stream))
            {
                var responseData = reader.ReadToEnd();
                jobj = JObject.Parse(responseData);
            }
            var success = (bool)jobj["success"];
            if (!success)
            {
                session.Error(this, ErrorCode.PlayerNotExists);
                return;
            }
            foreach (var item in jobj["items"])
            {
                var remainingUses = item["remaining_uses"];
                if (remainingUses == null || (int)remainingUses <= 0)
                {
                    session.Error(this, ErrorCode.ItemNotAvailable);
                    return;
                }

                var result = await ConsumeItem(session.Metadata.KongregateId, (int)item["id"], message.GameToken);
                if (!result)
                {
                    session.Error(this, ErrorCode.ItemNotAvailable);
                    return;
                }
                var type = (string)item["identifier"];
                var shopItem = _kongShop.GetByKongId(type);
                if (shopItem == null)
                {
                    session.Error(this, ErrorCode.ItemNotAvailable);
                    return;
                }
            
                var template = new CreateItemForUserCommand.ItemTemplate(
                    baseId: shopItem.ItemBaseId,
                    level: 1,
                    quantity: shopItem.Quantity
                    );
                await CreateItem.Command.Execute(session, session.Player, template, i => session.Send(new BuyItemFromShopResponse
                {
                    Item = i.DTO
                }));
                await CurrencyBought.Command.Execute(session, shopItem.Quantity);
            }
        }

        private static readonly Uri CONSUME_ITEM_URL = new Uri("https://api.kongregate.com/api/use_item.json");
        private static readonly string CONSUME_REQUEST_FORMAT = @"{{""api_key"":""{0}"",""user_id"":{1},""game_auth_token"":""{2}"",""id"":{3}}}";
        private async Task<bool> ConsumeItem(long userId, int itemId, string token)
        {
            var request = WebRequest.CreateHttp(CONSUME_ITEM_URL);
            request.Method = "POST";
            request.ContentType = "application/json";
            request.AllowWriteStreamBuffering = true;
            var data = Encoding.ASCII.GetBytes(string.Format(CONSUME_REQUEST_FORMAT, Kongregate.API_KEY, userId, token, itemId));
            request.ContentLength = data.Length;
            using (var s = request.GetRequestStream())
            {
                s.Write(data, 0, data.Length);
            }
            using (var restResponse = await request.GetResponseAsync())
            using (var s = restResponse.GetResponseStream())
            using (var r = new StreamReader(s))
            {
                var responseData = r.ReadToEnd();
                var jobj = JObject.Parse(responseData);
                return (bool) jobj["success"];
            }
        }

        #endregion
    }
}
