﻿using System.Threading.Tasks;
using ClientProtocol;
using LobbyServer.Services;

namespace LobbyServer.Commands
{
    public class LeaderboardsRequest : AuthorizedCommand<ClientProtocol.LeaderboardsRequest>
    {
        private readonly Leaderboard _leaderboard = GetInstance<Leaderboard>();
        protected override async Task Execute(WebSession session, ClientProtocol.LeaderboardsRequest message)
        {
            var response = new LeaderboardResponse
            {
                Leaderboard = _leaderboard.GetRecordsForCharacter(message.CharacterId)
            };
            session.Send(response);
        }
    }
}
