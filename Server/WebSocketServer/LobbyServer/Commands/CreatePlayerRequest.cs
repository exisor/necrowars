﻿using System;
using System.Threading.Tasks;
using ClientProtocol;
using LobbyServer.BL;

namespace LobbyServer.Commands
{
    public class CreatePlayerRequest : AuthorizedCommand<ClientProtocol.CreatePlayerRequest>
    {
        private readonly DefferedCommand<CreatePlayerCommand> CreatePlayer = new DefferedCommand<CreatePlayerCommand>();
        protected override async Task Execute(WebSession session, ClientProtocol.CreatePlayerRequest message)
        {
            try
            {
                await CreatePlayer.Command.Execute(session, message.Name);
                session.Send(new CreatePlayerResponse {Player = session.Player.DTO});
            }
            catch (Exception e)
            {
                session.Logger.Info(e);
                session.Send(new CreatePlayerResponse
                {
                    Player = null,
                });
                session.Error(this, ErrorCode.NameInUse);
            }
        }
    }
}
