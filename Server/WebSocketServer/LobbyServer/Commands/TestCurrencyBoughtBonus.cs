﻿using System.Threading.Tasks;
using LobbyServer.BL;

#if DEBUG

namespace LobbyServer.Commands
{
    public class TestCurrencyBoughtBonus : AuthorizedCommand<ClientProtocol.TestCurrencyBoughtBonus>
    {
        private DefferedCommand<UpdateCurrencyBoughtCommand> CurrencyBought = new DefferedCommand<UpdateCurrencyBoughtCommand>();
        protected override async Task Execute(WebSession session, ClientProtocol.TestCurrencyBoughtBonus message)
        {
            await CurrencyBought.Command.Execute(session, message.CurrencyBought);
        }
    }
}
#endif