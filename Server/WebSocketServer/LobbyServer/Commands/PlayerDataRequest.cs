﻿using System;
using System.Threading.Tasks;
using ClientProtocol;
using LobbyServer.BL;
using LobbyServer.Models;

namespace LobbyServer.Commands
{
    public class PlayerDataRequest : AuthorizedCommand<ClientProtocol.PlayerDataRequest>
    {
        private readonly DefferedCommand<TriggerLoginCommand> Login = new DefferedCommand<TriggerLoginCommand>();

        protected override async Task Execute(WebSession session, ClientProtocol.PlayerDataRequest message)
        {
            if (session.Player == null)
            {
                session.Send(new PlayerDataResponse
                {
                    Data = null,
                });
                return;
            }
            session.Send(new PlayerDataResponse
            {
                Data = session.Player.DTO
            });
            await Login.Command.Execute(session);
        }
    }
}
