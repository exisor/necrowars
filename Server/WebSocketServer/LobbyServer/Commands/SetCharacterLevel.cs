﻿using System.Threading.Tasks;
using ClientProtocol;
using LobbyServer.BL;

namespace LobbyServer.Commands
{
#if DEBUG
    public class SetCharacterLevel : AuthorizedCommand<ClientProtocol.SetCharacterLevel>
    {
        private readonly DefferedCommand<SetCharacterLevelCommand> SetLevel = new DefferedCommand<SetCharacterLevelCommand>();

        private readonly DefferedCommand<SetCharacterRequiredExperienceForLevelCommand> SetRequiredExp =
            new DefferedCommand<SetCharacterRequiredExperienceForLevelCommand>();
        protected override async Task Execute(WebSession session, ClientProtocol.SetCharacterLevel message)
        {
            var character = session.Player.Characters[message.CharacterId];
            if (character == null)
            {
                session.Error(this, ErrorCode.CharacterNotExists);
                return;
            }
            await SetLevel.Command.Execute(session, character, message.Level);
            await SetRequiredExp.Command.Execute(session, character, character.Level);
        }
    }
#endif
}
