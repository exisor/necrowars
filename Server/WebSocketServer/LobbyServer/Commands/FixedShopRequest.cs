﻿using System.Linq;
using System.Threading.Tasks;
using Common.DTO;
using Common.ResourceManager;

namespace LobbyServer.Commands
{
    public class FixedShopRequest : AuthorizedCommand<ClientProtocol.FixedShopRequest>
    {
        private readonly IShopItemsResourceManager _shopResourceManager = GetInstance<IShopItemsResourceManager>();
        
        protected override async Task Execute(WebSession session, ClientProtocol.FixedShopRequest message)
        {
            var characterLevel = session.Player.Characters.First().Level;
            // pot custom hack
            var maxlevel =
                _shopResourceManager.All.Where(s => s.ItemId == 3 && s.Level <= characterLevel).Max(s => s.Level);
            var items = _shopResourceManager.All.Where(shopitem =>
                    // OneTime item check
                        (!shopitem.OneTime ||
                         session.Player.Inventory.FirstOrDefault(i => i.BaseId == shopitem.ItemId) == null)

                        &&
                        // Level requirement check
                        (shopitem.Level == 0 || shopitem.Level == maxlevel)

                        &&
                        // limited availability check
                        (shopitem.Limited == null || shopitem.Limited.Ongoing))
                .Select(i => i).ToList();
            foreach (var overrideItem in items.Where(i => i.Overrides != 0).ToList())
            {
                items.Remove(items.FirstOrDefault(i => i.Id == overrideItem.Overrides));
            }
            session.Send(new ClientProtocol.FixedShopResponse {Shop = new FixedShop
            {
                Items = items.Select(i => new ShopItem
                {
                    BaseId = i.ItemId,
                    CurrencyPrice = i.CurrencyPrice,
                    GoldPrice = i.GoldPrice,
                    Id = i.Id,
                    Quantity = i.Quantity,
                    Level = i.Level,
                    Limited = i.Limited != null,
                    ExpiresInMinutes = i.Limited?.ExpiresInMinutes ?? 0,
                    Discount = i.Limited == null ? 0 : i.CurrencyPrice > 0 ? GetDiscount(i.CurrencyPrice, _shopResourceManager.Get(i.Overrides)?.CurrencyPrice ?? 0) :
                    GetDiscount(i.GoldPrice, _shopResourceManager.Get(i.Overrides)?.GoldPrice ?? 0)
                }).ToList()
            }});
        }

        private double GetDiscount(int newPrice, int oldPrice)
        {
            if (oldPrice == 0) return 0;
            return (double) newPrice/(double)oldPrice;
        }
    }
}
