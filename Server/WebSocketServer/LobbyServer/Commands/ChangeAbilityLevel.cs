﻿using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ClientProtocol;
using Common.ResourceManager;
using LobbyServer.BL;
using LobbyServer.Models;

namespace LobbyServer.Commands
{
    public class ChangeAbilityLevel : AuthorizedCommand<ClientProtocol.ChangeAbilityLevel>
    {
        private const int REFUND_ITEM_ID = 24;

        private readonly IAbilityResourceManager _abilityResourceManager = GetInstance<IAbilityResourceManager>();


        private readonly DefferedCommand<SetCharacterAbilityLevelCommand> SetCharacterAbilityLevel = new DefferedCommand<SetCharacterAbilityLevelCommand>();
        private readonly DefferedCommand<SetCharacterApCommand> SetCharacterAP = new DefferedCommand<SetCharacterApCommand>();
        private readonly DefferedCommand<DestroyItemCommand> DestroyItem = new DefferedCommand<DestroyItemCommand>();
        private readonly DefferedCommand<ChangeItemQuantityCommand> ChangeItemQuantity = new DefferedCommand<ChangeItemQuantityCommand>();
        protected override async Task Execute(WebSession session, ClientProtocol.ChangeAbilityLevel message)
        {
            // check for character available
            var character = session.Player.Characters[message.CharacterId];
            if (character == null)
            {
                session.Error(this, ErrorCode.CharacterNotExists);
                return;
            }
            // check for ability available
            var ability = character.Abilities[message.AbilityId];
            if (ability == null)
            {
                session.Error(this, ErrorCode.AbilityNotAvailable);
                return;
            }
            var abilityLevel = ability.Level;
            var targetLevel = abilityLevel + message.LevelDifference;

            if (targetLevel < 0)
            {
                session.Error(this, ErrorCode.InvalidDataInRequest);
                return;
            }
            // checking if ability can level up
            var resource = _abilityResourceManager.Get(ability.Id);
            if (!resource.isEndless && targetLevel > 1)
            {
                session.Error(this, ErrorCode.AbilityNotAvailable);
                return;
            }

            // Cant delevel if any dependant abilities has more then 1 lvl
            if (_abilityResourceManager.GetAffectedAbilities(ability.Id).Any(dependantAbility =>
                character.Abilities[dependantAbility.Id]?.Level > 0 &&
                dependantAbility.AbilityRequirements.First(a => a.Id == ability.Id).Level > targetLevel))
            {
                session.Error(this, ErrorCode.InvalidDataInRequest);
                return;
            }

            var difference =
            await TryBuyAbilityLevels(session, message.LevelDifference, character);
            if (difference == 0)
            {
                return;
            }
            // Execute command
            await SetCharacterAbilityLevel.Command.Execute(session, ability, character, abilityLevel + difference);
        }

        private async Task<int> TryBuyAbilityLevels(WebSession session, int levelDiff, ICharacter character)
        {
            if (levelDiff == 0)
            {
                return 0;
            }
            // check for ap available
            if (character.AP < levelDiff)
            {
                session.Error(this, ErrorCode.InsufficientResources);
                return 0;
            }
            if (levelDiff < 0)
            {
                var items = session.Player.Inventory.Where(i => i.BaseId == REFUND_ITEM_ID).ToList();
                var sum = items.Count > 0 ? items.Sum(i => i.Quantity) : 0;

                if (sum < -levelDiff)
                {
                    session.Error(this, ErrorCode.InsufficientResources);
                    return 0;
                }
                sum = -levelDiff;
                // consume items
                foreach (var item in items)
                {
                    var difference = item.Quantity - sum;
                    if (difference <= 0)
                    {
                        sum = -difference;
                        await DestroyItem.Command.Execute(session, session.Player.Inventory, item);
                        if (sum == 0)
                        {
                            break;
                        }
                    }
                    else
                    {
                        await ChangeItemQuantity.Command.Execute(session, item, difference);
                        break;
                    }
                }
            }
            await SetCharacterAP.Command.Execute(session, character, character.AP - levelDiff);
            return levelDiff;
        }
    }
}
