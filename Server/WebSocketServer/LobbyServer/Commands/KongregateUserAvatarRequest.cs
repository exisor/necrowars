﻿using System.IO;
using System.Net;
using System.Threading.Tasks;
using ClientProtocol;
using Newtonsoft.Json.Linq;

namespace LobbyServer.Commands
{
    public class KongregateUserAvatarRequest : AuthorizedCommand<ClientProtocol.KongregateUserAvatarRequest>
    {
        private const string REQUEST_FORMAT =
            "https://api.kongregate.com/api/user_info.json?user_id={0}&friends=false";
        protected override async Task Execute(WebSession session, ClientProtocol.KongregateUserAvatarRequest message)
        {
            var reqstring = string.Format(REQUEST_FORMAT, message.KongUserId);
            var request = WebRequest.CreateHttp(reqstring);
            request.Method = "GET";
            JObject jobj;
            using (var restResponse = await request.GetResponseAsync())
            using (var stream = restResponse.GetResponseStream())
            using (var reader = new StreamReader(stream))
            {
                var responseData = reader.ReadToEnd();
                jobj = JObject.Parse(responseData);
            }
            var success = (bool) jobj["success"];
            var response = new KongregateUserAvatarResponse();
            if (!success)
            {
                session.Error(this, ErrorCode.PlayerNotExists);
            }
            else
            {
                response.Url = (string)jobj["user_vars"]["avatar_url"];
            }
            session.Send(response);
        }
    }
}
