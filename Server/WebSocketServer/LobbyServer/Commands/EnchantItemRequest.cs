﻿using System;
using System.Threading.Tasks;
using ClientProtocol;
using Common.ResourceManager;
using Common.Resources;
using LobbyServer.BL;
using LobbyServer.Models;
using LobbyServer.Services;
using System.Linq;
using Common.DTO;

namespace LobbyServer.Commands
{
    public class EnchantItemRequest : AuthorizedCommand<ClientProtocol.EnchantItemRequest>
    {
        private const int MAX_ENCHANT = 15;

        private readonly DefferedCommand<SetPlayerGoldCommand> SetGold = new DefferedCommand<SetPlayerGoldCommand>();
        private readonly DefferedCommand<EnchantItemCommand> Enchant = new DefferedCommand<EnchantItemCommand>();
        private readonly DefferedCommand<DestroyItemCommand> DestroyItem = new DefferedCommand<DestroyItemCommand>();
        private readonly IItemResourceManager _itemResourceManager = GetInstance<IItemResourceManager>();

        private static readonly long[] FixedPriceIds = { 5, 6, 7, 8, 9, 10 };
        protected override async Task Execute(WebSession session, ClientProtocol.EnchantItemRequest message)
        {
            var item = session.Player.Inventory[message.ItemId];
            if (item == null)
            {
                session.Error(this, ErrorCode.ItemNotAvailable);
                return;
            }

            if (item.Enchant >= MAX_ENCHANT)
            {
                session.Error(this, ErrorCode.ItemNotAvailable);
                return;
            }

            var resource = _itemResourceManager.Get(item.BaseId);
            if (!resource.Types.Contains(ItemType.Equipment))
            {
                session.Error(this, ErrorCode.ItemNotAvailable);
                return;
            }
            var price = FixedPriceIds.Contains(item.BaseId) ? 900 :GetPrice(item);
            if (session.Player.Gold < price)
            {
                session.Error(this, ErrorCode.InsufficientResources);
                return;
            }

            await SetGold.Command.Execute(session, session.Player, session.Player.Gold - price);
            EnchantResult result;
            if (Roll(item))
            {
                // increase enchant level
                await Enchant.Command.Execute(session, item, item.Enchant + 1);
                result = EnchantResult.Success;
            }
            else
            {
                // punish
                if (item.Enchant < 5)
                {
                    await Enchant.Command.Execute(session, item, item.Enchant - 1);
                    result = EnchantResult.LevelDecreased;
                }
                else if (item.Enchant < 8)
                {
                    await Enchant.Command.Execute(session, item, 0);
                    result = EnchantResult.LevelZero;
                }
                else if (item.Enchant < 10)
                {
                    // 30 % destroy 70% zerolvl
                    var chance = RandomService.Instance.NextDouble();
                    if (chance > 0.3)
                    {
                        await Enchant.Command.Execute(session, item, 0);
                        result = EnchantResult.LevelZero;
                    }
                    else
                    {
                        await DestroyItem.Command.Execute(session, session.Player.Inventory, item);
                        result = EnchantResult.Broken;
                    }
                }
                else
                {
                    await DestroyItem.Command.Execute(session, session.Player.Inventory, item);
                    result = EnchantResult.Broken;
                }
            }
            session.Send(new EnchantItemResponse
            {
                Result = result
            });
        }

        private static int GetPrice(IItem item)
        {
            return ItemUtilities.GetEnchantItemPrice(item.ItemLevel, item.Rarity, item.Enchant).Gold;
        }

        private static bool Roll(IItem item)
        {
            double chance;
            if (item.Enchant < 3)
            {
                return true;
            }
            if (item.Enchant < 5)
            {
                chance = 0.66;
            }
            else if (item.Enchant < 8)
            {
                chance = 0.5;
            }
            else if (item.Enchant < 10)
            {
                chance = 0.4;
            }
            else if (item.Enchant < 13)
            {
                chance = 0.33;
            }
            else
            {
                chance = 0.1;
            }
            return RandomService.Instance.NextDouble() > chance;
        }
    }
}
