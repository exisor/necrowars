﻿using System.Collections.Generic;
using System.Linq;
using Common;

namespace LobbyServer.Models
{
    public class AchievementsCollection : ReadOnlyDictionary<IAchievement>, IStoredModel<byte[]>, INetworkModel<List<Common.DTO.Achievement>>
    {
        public AchievementsCollection(byte[] data) : base(data != null ? ProtobufSerializer.Deserialize<List<Common.DTO.Achievement>>(data).Select(a => new Achievement(a))
            : null)
        {
        }

        #region Implementation of IStoredModel<out byte[]>

        public byte[] Data => ProtobufSerializer.SerializeBinary(DTO);

        #endregion

        #region Implementation of INetworkModel<List<Achievement>>

        public List<Common.DTO.Achievement> DTO => this.Select(a => a.DTO).ToList();

        #endregion

        public void Add(IAchievement achievement)
        {
            Values.Add(achievement.Id, achievement);
        }
    }
}
