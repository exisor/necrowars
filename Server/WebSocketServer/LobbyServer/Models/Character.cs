using System.Collections.Generic;
using System.Linq;
using LobbyServer.BL;

namespace LobbyServer.Models
{
    public class Character : ICharacter, IStoredModel<Data.Character>
    {
        public Character(Data.Character storedData)
        {
            Data = storedData;
            Abilities = new AbilityCollection(storedData.AbilityData);
            AbilitySlots = new AbilitySlots(storedData.SelectedAbilities, Abilities);

            RequiredExperience = Common.Util.GetRequiredExperienceForLevel(Level);
        }

        public long Id => Data.Id;
        public long BaseId => Data.BaseId;
        public int Level
        {
            get { return Data.Level; } set { Data.Level = value; } }

        public long Experience
        {
            get { return Data.Experience; } set { Data.Experience = value; }
        }

        public long RequiredExperience
        {
            get;set;
        }

        public AbilityCollection Abilities { get; }
        public AbilitySlots AbilitySlots { get; }

        public int AP
        {
            get { return Data.Ap; } set { Data.Ap = value; } }

        public int APRefunded
        {
            get { return Data.APRefunded; } set { Data.APRefunded = value; } }

        public readonly List<IItem> Items = new List<IItem>();
        public IEnumerable<IItem> EquippedItems => Items;

        public Common.DTO.Character DTO => new Common.DTO.Character
        {
            Id = Id,
            Abilities = Abilities.DTO,
            AbilitySlots = AbilitySlots.DTO,
            Level = Level,
            AP = AP,
            APRefunded = APRefunded,
            BaseId = BaseId,
            Experience = Experience,
            RequiredExperience = RequiredExperience,
            UserId = Data.UserId,
            EquippedItems = Items.Select(i => i.Id).ToList()
        };
        public Data.Character Data { get; }

    }
}