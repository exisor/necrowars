using System;
using System.Collections.Generic;
using System.Linq;
using Common;

namespace LobbyServer.Models
{
    public class AbilityCollection : ReadOnlyDictionary<IAbility>, INetworkModel<List<Common.DTO.Ability>>, IStoredModel<byte[]>
    {
        public AbilityCollection(byte[] data)
            : base(ProtobufSerializer.Deserialize<List<Common.DTO.Ability>>(data).Select(a => new Ability(a))) { }

        public List<Common.DTO.Ability> DTO => this.Select(a => a.DTO).ToList();
        public byte[] Data => ProtobufSerializer.SerializeBinary(DTO);

        public void Add(IAbility ability)
        {
            Values.Add(ability.Id, ability);
        }
        public void Remove(IAbility ability)
        {
            Values.Remove(ability.Id);
        }
    }
}