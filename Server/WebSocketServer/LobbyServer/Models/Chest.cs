﻿namespace LobbyServer.Models
{
    internal class Chest : IChest, IStoredModel<Data.Chest>
    {
        public static Chest New(long baseId, int level)
        {
            return new Chest(new Data.Chest
            {
                BaseId = baseId, 
                Level = level,
            });
        }

        public Chest(Data.Chest chest)
        {
            Data = chest;
        }

        public long BaseId => Data.BaseId;
        public int Level => Data.Level;
        public long Id => Data.Id;
        public Data.Chest Data { get; }
    }
}
