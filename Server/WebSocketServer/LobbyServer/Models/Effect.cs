﻿using System;
using Common.DTO;

namespace LobbyServer.Models
{
    public class Effect : IEffect, IStoredModel<Data.Effect>
    {
        private readonly Data.Effect _effect;
        public Effect(Data.Effect effect)
        {
            _effect = effect;
            Expired = _effect.Estimated.Seconds <= 0;
        }

        public long Id => _effect.Id;
        public EffectType BaseId => (EffectType)_effect.EffectBase;

        public PowerUp DTO => new PowerUp
        {
            Id = Id,
            BaseId = (long)BaseId,
            Expires = _effect.Estimated
        };

        public DateTime ExpirationTime => _effect.Started + TimeSpan.FromSeconds(_effect.Duration);
        public bool Expired { get; set; }
        public Data.Effect Data => _effect;
    }
}