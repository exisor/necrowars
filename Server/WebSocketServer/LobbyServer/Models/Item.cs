using System.Collections.Generic;
using System.Linq;
using Common;
using Common.DTO;

namespace LobbyServer.Models
{
    internal class Item : IItem, IStoredModel<Data.Item>
    {
        public static readonly long[] OneTimeItems = {26,};

        private ICharacter _equipped;

        public Item(Data.Item data)
        {
            Data = data;
            if (OneTimeItems.Contains(data.BaseId))
            {
                TakeSpaceInInventory = false;
            }
            else
            {
                TakeSpaceInInventory = true;
            }
        }

        public long BaseId => Data.BaseId;

        public ICharacter Equipped
        {
            get { return _equipped; }
            set
            {
                ((List<IItem>) _equipped?.EquippedItems)?.Remove(this);
                _equipped = value;
                if (_equipped == null)
                {
                    Data.CharacterId = null;
                    TakeSpaceInInventory = true;
                }
                else
                {
                    Data.CharacterId = _equipped.Id;
                    ((List<IItem>)_equipped.EquippedItems).Add(this);
                    TakeSpaceInInventory = false;
                }
            }
        }

        public Rarity Rarity => (Rarity) Data.Rarity;
        public int ItemLevel { get { return Data.Itemlevel; } set { Data.Itemlevel = value; } }
        public int Quantity { get { return Data.Quantity; } set { Data.Quantity = value; } }
        public IEnumerable<ItemAffix> Affixes => ProtobufSerializer.Deserialize<List<ItemAffix>>(Data.AffixData);
        public int Enchant { get { return Data.Enchant; } set { Data.Enchant = value; } }
        public bool TakeSpaceInInventory { get; set; }

        public Common.DTO.Item DTO => new Common.DTO.Item
        {
            ItemLevel = ItemLevel,
            BaseId = BaseId,
            Enchant = Enchant,
            Rarity = Rarity,
            Id = Id,
            Affixes = Affixes.ToList(),
            Quantity = Quantity
        };

        public long Id => Data.Id;
        public Data.Item Data { get; }
    }
}