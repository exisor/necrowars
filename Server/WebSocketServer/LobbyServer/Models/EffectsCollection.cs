﻿using System.Collections.Generic;
using System.Linq;
using Common.DTO;

namespace LobbyServer.Models
{
    public class EffectsCollection : ReadOnlyDictionary<IEffect>, INetworkModel<List<Common.DTO.PowerUp>>
    {
        private readonly HashSet<EffectType> _effects = new HashSet<EffectType>();
        private readonly object l = new object();
        public EffectsCollection(IEnumerable<Data.Effect> effects) : base(effects?.Select(e => new Effect(e)))
        {
        }

        public void Remove(IEffect effect)
        {
            lock (l)
            {
                Values.Remove(effect.Id);
                _effects.Remove(effect.BaseId);
            }
        }

        public void Add(IEffect effect)
        {
            lock (l)
            {
                Values[effect.Id] = effect;
                _effects.Add(effect.BaseId);
            }
        }

        public bool HasEffect(EffectType type)
        {
            lock (l)
            {
                return _effects.Contains(type);
            }
        }

        public List<PowerUp> DTO => this.Select(e => e.DTO).ToList();
    }

}
