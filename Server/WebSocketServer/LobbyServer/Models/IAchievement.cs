﻿using Common;

namespace LobbyServer.Models
{
    public interface IAchievement : INetworkModel<Common.DTO.Achievement>, IIdentified
    {
        int Value { get; }
    }
}
