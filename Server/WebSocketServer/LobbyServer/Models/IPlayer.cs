﻿using Common;

namespace LobbyServer.Models
{
    public interface IPlayer : INetworkModel<Common.DTO.Player>, IIdentified
    {
        long Currency { get; }
        long Gold { get; }
        string Username { get; }
        CharactersCollection Characters { get; }
        Inventory Inventory { get; }
        EffectsCollection Effects { get; }
        int TutorialPhase { get; }
        int BonusInventory { get; }
        AchievementsCollection Achievements { get; }
        long QuestId { get; }
        int CurrencyBought { get; }
    }
}