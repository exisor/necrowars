﻿using System.Collections.Generic;
using Common;

namespace LobbyServer.Models
{
    public interface ICharacter : IIdentified, INetworkModel<Common.DTO.Character>
    {
        int Level { get;}
        long Experience { get;}
        long RequiredExperience { get; }
        AbilityCollection Abilities { get; }
        AbilitySlots AbilitySlots { get; }
        int AP { get; }
        int APRefunded { get; }
        IEnumerable<IItem> EquippedItems { get; }
    }
} 