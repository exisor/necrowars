﻿namespace LobbyServer.Models
{
    interface IChest
    {
        long BaseId { get; }
        int Level { get; }
        long Id { get; }
    }
}
