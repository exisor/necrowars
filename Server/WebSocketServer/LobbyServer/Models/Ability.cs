namespace LobbyServer.Models
{
    public class Ability : IAbility
    {
        public Ability(Common.DTO.Ability abilityDto)
        {
            Id = abilityDto.Id;
            Level = abilityDto.Level;
        }

        public long Id { get; }
        public IAbilitySlot SelectedInSlot { get; set; }
        public int Level { get; set; }

        public Common.DTO.Ability DTO => new Common.DTO.Ability
        {
            Id = Id,
            Level = Level,
        };
    }
}