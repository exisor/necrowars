﻿using System;
using Common;

namespace LobbyServer.Models
{
    public interface IEffect : IIdentified, INetworkModel<Common.DTO.PowerUp>
    {
        EffectType BaseId { get; }
        DateTime ExpirationTime { get; }
        bool Expired { get; }
    }
}
