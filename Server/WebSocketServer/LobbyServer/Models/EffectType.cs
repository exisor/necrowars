﻿namespace LobbyServer.Models
{
    public enum EffectType : byte
    {
        ExpPotion = 1,
        GoldPotion = 2,
        Premium = 3,
    }
}
