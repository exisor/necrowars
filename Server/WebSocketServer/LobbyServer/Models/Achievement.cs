﻿namespace LobbyServer.Models
{
    public class Achievement : IAchievement
    {
        public Achievement(Common.DTO.Achievement achievementData)
        {
            Id = achievementData.Id;
            Value = achievementData.Value;
        }

        #region Implementation of INetworkModel<Achievement>

        public Common.DTO.Achievement DTO => new Common.DTO.Achievement
        {
            Id = Id,
            Value = Value,
        };

        #endregion

        #region Implementation of IIdentified

        public long Id { get; }

        #endregion

        #region Implementation of IAchievement

        public int Value { get; set; }

        #endregion
    }
}