﻿using System.Collections.Generic;
using System.Linq;
using Common;

namespace LobbyServer.Models
{
    public class AbilitySlots : ReadOnlyDictionary<IAbilitySlot>, INetworkModel<List<Common.DTO.AbilitySlot>>
    {
        public AbilitySlots(byte[] data, AbilityCollection abilities)
            : base(ProtobufSerializer.Deserialize<List<Common.DTO.AbilitySlot>>(data).Select(slot => new AbilitySlot(slot.Slot, (Ability)abilities[slot.AbilityId]))) { }

        public List<Common.DTO.AbilitySlot> DTO
            => this.Select(slot => slot.DTO).ToList();
    }
}