﻿using System.Collections.Generic;
using System.Linq;

namespace LobbyServer.Models
{
    public class Inventory : ReadOnlyDictionary<IItem>, INetworkModel<List<Common.DTO.Item>>
    {
        public const int CAPACITY = 10;
        public Inventory(IEnumerable<Data.Item> data, CharactersCollection characters)
            : base(data?.Select(i => new Item(i)))
        {
            this.Cast<Item>().Where(i => i.Data.CharacterId != null).ToList().ForEach(i => i.Equipped = characters[i.Data.CharacterId.Value]);
        }
        public void Add(IItem item)
        {
            Values[item.Id] = item;
        }

        public void AddMultiple(IEnumerable<IItem> items)
        {
            foreach (var item in items)
            {
                Values[item.Id] = item;
            }
        }

        public IItem Remove(long id)
        {
            IItem retval;
            if (!Values.TryGetValue(id, out retval))
            {
                return null;
            }
            Values.Remove(id);
            return retval;
        }

        public List<Common.DTO.Item> DTO => this.Select(i => i.DTO).ToList();

        public int FreeSpace => CAPACITY - this.Count(i => i.TakeSpaceInInventory) + BonusCapacity;

        public int BonusCapacity { get; set; }
    }
}
