using System.Collections.Generic;
using System.Linq;

namespace LobbyServer.Models
{
    public class CharactersCollection : ReadOnlyDictionary<ICharacter>, INetworkModel<List<Common.DTO.Character>>
    {
        public CharactersCollection(IEnumerable<Data.Character> charactersData)
            : base(charactersData?.Select(data => new Character(data))) { }

        public void Add(ICharacter value)
        {
            Values[value.Id] = value;
        }

        public List<Common.DTO.Character> DTO => this.Select(c => c.DTO).ToList();
    }
}