using Common;

namespace LobbyServer.Models
{
    public interface IAbility : IIdentified, INetworkModel<Common.DTO.Ability>
    {
        IAbilitySlot SelectedInSlot { get; }
        int Level { get; set; }
    }
}