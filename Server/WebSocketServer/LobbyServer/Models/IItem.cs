﻿using System.Collections.Generic;
using Common;
using Common.DTO;

namespace LobbyServer.Models
{
    public interface IItem : IIdentified, INetworkModel<Common.DTO.Item>
    {
        long BaseId { get; }
        ICharacter Equipped { get; }
        Rarity Rarity { get; }
        int ItemLevel { get; }
        int Quantity { get; }
        IEnumerable<ItemAffix> Affixes { get; }
        int Enchant { get; }
        bool TakeSpaceInInventory { get; }
    }
}