﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Common;

namespace LobbyServer.Models
{
    public abstract class ReadOnlyDictionary<TValue> : IEnumerable<TValue>
        where TValue : IIdentified
    {
        protected readonly Dictionary<long, TValue> Values;
        protected ReadOnlyDictionary(IEnumerable<TValue> values)
        {
            Values = values?.ToDictionary(v => v.Id, v => v) ?? new Dictionary<long, TValue>();
        }

        public TValue this[long index]
        {
            get
            {
                TValue retval;
                Values.TryGetValue(index, out retval);
                return retval;
            }
        }

        public IEnumerator<TValue> GetEnumerator()
        {
            return Values.Values.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        public int Count => Values.Count;
    }
}
