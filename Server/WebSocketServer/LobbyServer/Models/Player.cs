﻿using System;

namespace LobbyServer.Models
{
    internal class Player : IPlayer, IStoredModel<Data.Player>
    {
        public Player(Data.Player playerData)
        {
            Data = playerData;

            Id = Data.Id;

            Characters = new CharactersCollection(Data.Characters);
            Inventory = new Inventory(Data.Inventory, Characters) {BonusCapacity = BonusInventory};
            Effects = new EffectsCollection(Data.Effects);
            Achievements = new AchievementsCollection(Data.AchievementsData);
        }

        public long Id { get; }

        public long Currency
        {
            get { return Data.Currency; }
            set
            {
                Data.Currency = value;
            }
        }

        public long Gold
        {
            get { return Data.Gold; }
            set
            {
                Data.Gold = value;
            }
        }

        public string Username => Data.Username;
        public CharactersCollection Characters { get; }

        public Common.DTO.Player DTO => new Common.DTO.Player
        {
            Characters = Characters.DTO,
            Items = Inventory.DTO,
            Currency = Currency,
            Username = Username,
            Gold = Gold,
            PowerUps = Effects.DTO,
            InventorySize = Inventory.CAPACITY + BonusInventory,
            TutorialPhase = TutorialPhase,
            Achievements = Achievements.DTO,
            DailyQuestId = QuestId,
            DailyQuestProgress = Data.QuestProgress,
            QuestExpireTime = (int) (DateTime.Now - Data.LastUpdate).TotalSeconds
        };
        public Inventory Inventory { get; }
        public EffectsCollection Effects { get; }
        public int TutorialPhase { get { return Data.TutorialPhase; }set { Data.TutorialPhase = value; } }

        public int BonusInventory
        {
            get
            {
                return Data.AdditionalInventorySlots;
            }
            set
            {
                Data.AdditionalInventorySlots = value;
                Inventory.BonusCapacity = value;
            }
        }

        public AchievementsCollection Achievements { get; }
        public long QuestId { get; set; }

        public int CurrencyBought
        {
            get { return Data.CurrencyBought; }
            set { Data.CurrencyBought = value; }
        }

        public Data.Player Data { get; }
    }
}
