﻿namespace LobbyServer.Models
{
    internal interface IStoredModel<out TData>
    {
        TData Data { get; }
    }
}
