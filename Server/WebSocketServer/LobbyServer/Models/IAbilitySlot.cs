﻿using Common;

namespace LobbyServer.Models
{
    public interface IAbilitySlot : IIdentified, INetworkModel<Common.DTO.AbilitySlot>
    {
        IAbility Ability { get; }
    }
}