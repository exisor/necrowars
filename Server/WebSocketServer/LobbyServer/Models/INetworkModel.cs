﻿namespace LobbyServer.Models
{
    public interface INetworkModel <TDTO>
    {
        TDTO DTO { get; }
    }
}
