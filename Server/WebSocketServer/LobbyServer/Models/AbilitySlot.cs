namespace LobbyServer.Models
{
    internal class AbilitySlot : IAbilitySlot
    {
        private Ability _ability;

        public AbilitySlot(int id, Ability ability)
        {
            Id = id;
            Ability = ability;
        }

        public long Id { get; }

        public IAbility Ability
        {
            get { return _ability; }
            set
            {
                if (_ability != null)
                {
                    _ability.SelectedInSlot = null;
                }
                _ability = (Ability)value;
                if (_ability != null)
                {
                    _ability.SelectedInSlot = this;
                }
            }
        }

        public Common.DTO.AbilitySlot DTO => new Common.DTO.AbilitySlot {AbilityId = Ability?.Id ?? 0, Slot = (int)Id};
    }
}