﻿using System.Threading.Tasks;
using Common;
using LobbyServer.Data;
using LobbyServer.Models;
using Character = LobbyServer.Models.Character;

namespace LobbyServer.BL
{
    internal class SetCharacterAbilityLevelCommand : Command<IAbility, ICharacter, int>
    {
        private readonly ICharacterRepository _characterRepository;

        public SetCharacterAbilityLevelCommand(ICharacterRepository characterRepository)
        {
            _characterRepository = characterRepository;
        }

        public override async Task Execute(WebSession session, IAbility ability, ICharacter icharacter, int level)
        {
            var character = (Character) icharacter;
            ability.Level = level;
            character.Data.AbilityData = ProtobufSerializer.SerializeBinary(character.Abilities.DTO);

            await _characterRepository.Update(character.Data);
            session.Send(new ClientProtocol.AbilityChangedEvent
            {
                Level = level,
                AbilityId = ability.Id,
                CharacterId = character.Id,
            });

        }
    }
}