﻿using System.Threading.Tasks;

namespace LobbyServer.BL
{
    internal interface ICommand : ICommonCommand
    {
        Task Execute(WebSession session);
    }

    internal interface ICommand<in TArg> : ICommonCommand
    {
        Task Execute(WebSession session, TArg args);
    }

    internal interface ICommand<in TArg1, in TArg2> : ICommonCommand
    {
        Task Execute(WebSession session, TArg1 arg1, TArg2 arg2);
    }

    internal interface ICommand<in TArg1, in TArg2, in TArg3> : ICommonCommand
    {
        Task Execute(WebSession session, TArg1 arg1, TArg2 arg2, TArg3 arg3);
    }
    internal interface ICommand<in TArg1, in TArg2, in TArg3, in TArg4> : ICommonCommand
    {
        Task Execute(WebSession session, TArg1 arg1, TArg2 arg2, TArg3 arg3, TArg4 arg4);
    }
}
