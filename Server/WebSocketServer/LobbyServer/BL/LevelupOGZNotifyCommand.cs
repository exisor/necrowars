﻿
using System;
using System.IO;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace LobbyServer.BL
{
    internal class LevelupOGZNotifyCommand : Command<int>
    {
        private const string REQUEST_FORMAT = "https://gateway.overgamez.com/api/apps/55/users/{0}/attributes";
        private const string ACCESS_TOKEN = "vh.031ac387de678fcc99f7ed11fdd1a308666e799ff00a64de8c9d3da86cfb0eeb";

        public override async Task Execute(WebSession session, int level)
        {
            var reqString = string.Format(REQUEST_FORMAT, session.Metadata.OGZId);
            var request = WebRequest.CreateHttp(new Uri(reqString));
            request.Method = "POST";

            request.ContentType = "application/json";
            request.AllowWriteStreamBuffering = true;

            var datastring = $"{{\"access_token\":\"{ACCESS_TOKEN}\", \"attributes\": {{\"level\":{level}}}}}";
            var data =
                Encoding.UTF8.GetBytes(datastring);
            request.ContentLength = data.Length;
            using (var s = request.GetRequestStream())
            using (var w = new StreamWriter(s))
            {
                await w.WriteAsync(datastring);
            }
            await request.GetResponseAsync();
        }
    }
}
