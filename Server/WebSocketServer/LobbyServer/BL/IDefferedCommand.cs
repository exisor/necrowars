﻿using Autofac;

namespace LobbyServer.BL
{
    internal interface IDefferedCommand
    {
    }

    internal interface IDefferedCommand<out TCommand> : IDefferedCommand where TCommand : class, ICommonCommand
    {
        TCommand Command { get; }
    }

    internal class DefferedCommand<TCommand> : IDefferedCommand<TCommand> where TCommand : class, ICommonCommand
    {
        private TCommand _command;
        public TCommand Command => _command ?? (_command = WebServer.Context.Resolve<TCommand>());
    }
}
