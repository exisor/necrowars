﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ClientProtocol;
using Common.ResourceManager;
using Common.Resources;
using LobbyServer.Data;
using Item = Common.DTO.Item;
using Player = LobbyServer.Models.Player;

namespace LobbyServer.BL
{
    internal class UpdateCurrencyBoughtCommand : Command<int>
    {
        private readonly IPlayerRepository _playerRepository;
        private readonly ICurrencyBonusRewardResourceManager _currencyBonusRewardResourceManager;
        private readonly DefferedCommand<CreateItemForUserCommand> CreateItem = new DefferedCommand<CreateItemForUserCommand>();
        private readonly int _cycleLength;

        public UpdateCurrencyBoughtCommand(IPlayerRepository playerRepository, ICurrencyBonusRewardResourceManager currencyBonusRewardResourceManager)
        {
            _playerRepository = playerRepository;
            _currencyBonusRewardResourceManager = currencyBonusRewardResourceManager;
            _cycleLength = _currencyBonusRewardResourceManager.CycleLength;
        }

        public override async Task Execute(WebSession session, int value)
        {
            var player = (Player)session.Player;

            if (_cycleLength == 0) return;
            var snap_value = value;
            var currentValueInCycle = player.CurrencyBought%_cycleLength;
            var nextTarget = GetNextTarget(currentValueInCycle);
            var level = session.Player.Characters.First().Level;
            var items = new List<Item>();
            while (nextTarget.Threshold <= currentValueInCycle + value)
            {
                await CreateItem.Command.Execute(session, session.Player,
                    new CreateItemForUserCommand.ItemTemplate(nextTarget.ItemBaseId, level, nextTarget.Quantity),
                    item => items.Add(item.DTO));
                value -= nextTarget.Threshold - currentValueInCycle;
                currentValueInCycle = nextTarget.Threshold%_cycleLength;
                nextTarget = GetNextTarget(currentValueInCycle);
            }
            if (items.Count > 0)
            {
                var packedItems = new List<Item>();
                foreach (var item in items)
                {
                    var storedItems = packedItems.FirstOrDefault(i => i.BaseId == item.BaseId);
                    if (storedItems == null)
                    {
                        packedItems.Add(item);
                    }
                    else
                    {
                        storedItems.Quantity += item.Quantity;
                    }
                }
                session.Send(new ChestOpenEvent { BaseId = 4, ItemsReceived = packedItems });
            }
            player.CurrencyBought += snap_value;
            await _playerRepository.Update(player.Data);
            session.Send(new CurrencyBoughtChangedEvent { NewCurrencyBought = player.CurrencyBought });
        }

        private CurrencyBonusReward GetNextTarget(int value)
        {
            return _currencyBonusRewardResourceManager.All.First(b => b.Threshold > value);
        }
    }
}
