﻿using System;
using System.Threading.Tasks;
using LobbyServer.Data;
using Player = LobbyServer.Models.Player;

namespace LobbyServer.BL
{
    internal class TriggerLoginCommand : Command
    {
        private readonly IPlayerRepository _playerRepository;
        private readonly IPlayerEventsRepository _playerEventsRepository;
        private readonly DefferedCommand<GrantDailyGiftCommand> GrantDailyGift = new DefferedCommand<GrantDailyGiftCommand>();
        private readonly DefferedCommand<ApplyPlayerEventCommand> ApplyEvent = new DefferedCommand<ApplyPlayerEventCommand>();
        public TriggerLoginCommand(IPlayerRepository playerRepository, IPlayerEventsRepository playerEventsRepository)
        {
            _playerRepository = playerRepository;
            _playerEventsRepository = playerEventsRepository;
        }

        public override async Task Execute(WebSession session)
        {
            var player = (Player) session.Player;
            var loginDate = player.Data.LastLogin;
            player.Data.LastLogin = DateTime.Now;
            await _playerRepository.Update(player.Data);
            if (player.Data.LastLogin - loginDate > player.Data.LastLogin - DateTime.Today)
            {
                await GrantDailyGift.Command.Execute(session, player);
            }
            if (player.Data.Events != null) { 
                foreach (var e in player.Data.Events)
                {
                    await _playerEventsRepository.Delete(e);
                    await ApplyEvent.Command.Execute(session, player, e);
                }
            }
        }
    }
}