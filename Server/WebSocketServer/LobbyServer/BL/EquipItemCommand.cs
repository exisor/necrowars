﻿using System.Linq;
using System.Threading.Tasks;
using Common.DTO;
using Common.ResourceManager;
using Common.Resources;
using LobbyServer.BL.InstantItems;
using LobbyServer.Data;
using LobbyServer.Models;
using Item = LobbyServer.Models.Item;

namespace LobbyServer.BL
{
    internal class EquipItemCommand : Command<ICharacter, IItem>
    {
        private const long EQUIP_RARE_ITEMS_ACHIEVEMENT_ID = 2;

        private readonly IItemRepository _itemRepository;
        private readonly IItemHandlerCollection _itemHandlers;

        private readonly DefferedCommand<ProgressAchievementCommand> Achievement = new DefferedCommand<ProgressAchievementCommand>();
        private readonly IItemResourceManager _itemResourceManager;
        public EquipItemCommand(IItemRepository itemRepository, IItemHandlerCollection itemHandlers, IItemResourceManager itemResourceManager)
        {
            _itemRepository = itemRepository;
            _itemHandlers = itemHandlers;
            _itemResourceManager = itemResourceManager;
        }

        private bool ItemNotVisual(IItem i)
        {
            return !_itemResourceManager.Get(i.BaseId).Types.Contains(ItemType.VisualHelm);
        }

        public override async Task Execute(WebSession session, ICharacter character, IItem iitem)
        {
            var item = (Item) iitem;

            item.Equipped = character;

            var handler = _itemHandlers.OnEquip(item.BaseId);
            
            await _itemRepository.Update(item.Data);

            if (handler != null && handler.CanHandle(session, character, item))
            {
                await handler.Handle(session, character, item);
            }

            session.Send(new ClientProtocol.ItemEquippedEvent
            {
                Id = item.Id,
            });

            // if not all slots filled - no achievements!
            if (character.EquippedItems.Count(ItemNotVisual) != 6)
            {
                return;
            }
            // achievement
            var achievement = session.Player.Achievements[EQUIP_RARE_ITEMS_ACHIEVEMENT_ID];
            // rare items check
            if ((achievement == null || achievement.Value == 0) && AllItemsOfRarity(character, Rarity.Rare))
            {
                await Achievement.Command.Execute(session, session.Player, EQUIP_RARE_ITEMS_ACHIEVEMENT_ID, 1);
                achievement = session.Player.Achievements[EQUIP_RARE_ITEMS_ACHIEVEMENT_ID];
            }
            // epic items
            if (achievement != null && achievement.Value == 1 && AllItemsOfRarity(character, Rarity.Epic))
            {
                await Achievement.Command.Execute(session, session.Player, EQUIP_RARE_ITEMS_ACHIEVEMENT_ID, 1);
            }
            // legendary
            if (achievement != null && achievement.Value == 2 && AllItemsOfRarity(character, Rarity.Legendary))
            {
                await Achievement.Command.Execute(session, session.Player, EQUIP_RARE_ITEMS_ACHIEVEMENT_ID, 1);
            }
            // unique
            if (achievement != null && achievement.Value == 3 && AllItemsOfRarity(character, Rarity.Unique))
            {
                await Achievement.Command.Execute(session, session.Player, EQUIP_RARE_ITEMS_ACHIEVEMENT_ID, 1);
            }
        }

        private bool AllItemsOfRarity(ICharacter character, Rarity rarity)
        {
            return character.EquippedItems.Where(ItemNotVisual).All(i => i.Rarity >= rarity);
        }
    }
}
