﻿using System.Collections.Generic;
using Common.DTO;

namespace LobbyServer.BL.UniqueItems
{
    interface IUniqueItemGenerator
    {
        long BaseId { get; }
    }
}
