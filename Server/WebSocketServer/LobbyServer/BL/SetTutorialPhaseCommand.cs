﻿using System.Threading.Tasks;
using ClientProtocol;
using LobbyServer.Data;
using LobbyServer.Models;
using Player = LobbyServer.Models.Player;

namespace LobbyServer.BL
{

    internal class SetTutorialPhaseCommand : Command<IPlayer, int>
    {
        private readonly IPlayerRepository _playerRepository;

        public SetTutorialPhaseCommand(IPlayerRepository playerRepository)
        {
            _playerRepository = playerRepository;
        }

        public override async Task Execute(WebSession session, IPlayer iplayer, int phaseNum)
        {
            var player = (Player) iplayer;
            player.TutorialPhase = phaseNum;
            await _playerRepository.Update(player.Data);
            session.Send(new TutorialPhaseChangedEvent
            {
                TutorialPhase = player.TutorialPhase
            });
        }
    }
}
