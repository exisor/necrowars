﻿using System;
using System.Threading.Tasks;
using LobbyServer.Data;

namespace LobbyServer.BL
{
    internal class CreatePlayerCommand : Command<string>
    {
        private readonly IPlayerRepository _repository;
        private readonly DefferedCommand<GrantDailyGiftCommand> GrantDailyGift = new DefferedCommand<GrantDailyGiftCommand>();
        public CreatePlayerCommand(IPlayerRepository repository)
        {
            _repository = repository;
        }

        public override async Task Execute(WebSession session, string name)
        {
            var player = new Player
            {
                Id = session.UserId.Value,
                Currency = 0,
                Gold = 0,
                Username = name,
                LastLogin = DateTime.Now
            };
            await _repository.Insert(player);
            session.Player = new Models.Player(player);
            await GrantDailyGift.Command.Execute(session, session.Player);
        }
    }
}
