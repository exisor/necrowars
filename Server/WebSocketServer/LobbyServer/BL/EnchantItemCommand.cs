﻿using System.Linq;
using System.Threading.Tasks;
using ClientProtocol;
using Common;
using Common.ResourceManager;
using LobbyServer.Data;
using LobbyServer.Models;
using Item = LobbyServer.Models.Item;

namespace LobbyServer.BL
{
    internal class EnchantItemCommand : Command<IItem, int>
    {
        private readonly IItemRepository _itemRepository;
        private readonly IItemResourceManager _itemResourceManager;
        public EnchantItemCommand(IItemRepository itemRepository, IItemResourceManager itemResourceManager)
        {
            _itemRepository = itemRepository;
            _itemResourceManager = itemResourceManager;
        }

        public override async Task Execute(WebSession session, IItem iitem, int value)
        {
            var item = (Item) iitem;
            item.Enchant = value;

            var resource = _itemResourceManager.Get(iitem.BaseId);
            var affixes = item.Affixes.ToList();
            var implicitAffix = affixes.FirstOrDefault(a => a.Id == resource.FixedAffixes.FirstOrDefault(id => id == 29 || id == 30));
            if (implicitAffix != null)
            {
                implicitAffix.Enchant = value;
            }
            foreach (var itemAffix in affixes.Where(a=> a.Enchant > value))
            {
                itemAffix.Enchant = value;
            }
            item.Data.AffixData = ProtobufSerializer.SerializeBinary(affixes);
            await _itemRepository.Update(item.Data);
            session.Send(new ItemChangedEvent
            {
                Item = item.DTO
            });
        }
    }
}
