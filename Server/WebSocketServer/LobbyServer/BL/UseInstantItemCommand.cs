﻿using System.Threading.Tasks;
using Common.ResourceManager;
using Common.Resources;
using LobbyServer.BL.InstantItems;
using LobbyServer.Models;

namespace LobbyServer.BL
{
    internal class UseInstantItemCommand : Command<IPlayer, ICharacter, IItem>
    {
        private readonly IItemHandlerCollection _itemHandlerCollection;
        private readonly IItemResourceManager _itemResourceManager;
        private readonly DefferedCommand<ChangeItemQuantityCommand> ChangeQuantity = new DefferedCommand<ChangeItemQuantityCommand>();
        private readonly DefferedCommand<DestroyItemCommand> DestroyItem = new DefferedCommand<DestroyItemCommand>();

        public UseInstantItemCommand(IItemHandlerCollection itemHandlerCollection, IItemResourceManager itemResourceManager)
        {
            _itemHandlerCollection = itemHandlerCollection;
            _itemResourceManager = itemResourceManager;
        }

        public override async Task Execute(WebSession session, IPlayer player, ICharacter character, IItem item)
        {
            var handler = _itemHandlerCollection.OnUse(item.BaseId);
            if (handler == null) return;

            var resource = _itemResourceManager.Get(item.BaseId);
            if (resource.Types.Contains(ItemType.Consumable) && handler.CanHandle(session, character, item))
            {
                if (item.Quantity - 1 <= 0)
                {
                    await DestroyItem.Command.Execute(session, session.Player.Inventory, item);
                }
                else
                {
                    await ChangeQuantity.Command.Execute(session, item, item.Quantity - 1);
                }
            }
            await handler.Handle(session, character, item);
        }
    }
}
