﻿using System.Threading.Tasks;
using LobbyServer.Data;
using LobbyServer.Models;
using Character = LobbyServer.Models.Character;

namespace LobbyServer.BL
{
    internal class SetCharacterApCommand : Command<ICharacter, int>
    {
        private readonly ICharacterRepository _repository;

        public SetCharacterApCommand(ICharacterRepository repository)
        {
            _repository = repository;
        }

        public override async Task Execute(WebSession session, ICharacter icharacter, int ap)
        {
            var character = (Character) icharacter;
            character.AP = ap;
            await _repository.Update(character.Data);

            session.Send(new ClientProtocol.ApChangedEvent
            {
                CharacterId = character.Id,
                NewAp = character.AP,
            });
        }
    }
}
