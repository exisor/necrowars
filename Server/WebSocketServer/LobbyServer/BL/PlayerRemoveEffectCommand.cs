﻿using System.Threading.Tasks;
using LobbyServer.Data;
using LobbyServer.Models;
using Effect = LobbyServer.Models.Effect;

namespace LobbyServer.BL
{
    internal class PlayerRemoveEffectCommand : Command<EffectsCollection, IEffect>
    {
        private readonly IEffectRepository _effectRepository;

        public PlayerRemoveEffectCommand(IEffectRepository effectRepository)
        {
            _effectRepository = effectRepository;
        }

        public override async Task Execute(WebSession session, EffectsCollection collection, IEffect ieffect)
        {
            var effect = (Effect)ieffect;
            collection.Remove(effect);
            await _effectRepository.Delete(effect.Data);
            session.Send(new ClientProtocol.PowerUpExpiredEvent
            {
                Id = effect.Id
            });
        }
    }
}
