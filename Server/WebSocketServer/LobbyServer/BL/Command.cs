using System.Threading.Tasks;

namespace LobbyServer.BL
{
    internal abstract class Command<TArg1, TArg2, TArg3> : ICommand<TArg1, TArg2, TArg3>
    {
        public abstract Task Execute(WebSession session, TArg1 arg1, TArg2 arg2, TArg3 arg3);

        public Task Execute(WebSession session, object obj)
        {
            var args = (object[]) obj;
            return Execute(session, (TArg1) args[0], (TArg2) args[1], (TArg3) args[2]);
        }

    }

    internal abstract class Command<TArg1, TArg2> : ICommand<TArg1, TArg2>
    {
        public abstract Task Execute(WebSession session, TArg1 arg1, TArg2 arg2);

        public Task Execute(WebSession session, object obj)
        {
            var args = (object[]) obj;
            return Execute(session, (TArg1)args[0], (TArg2)args[1]);
        }
    }

    internal abstract class Command<TArg> : ICommand<TArg>
    {
        public abstract Task Execute(WebSession session, TArg args);

        public Task Execute(WebSession session, object obj)
        {
            return Execute(session, (TArg)obj);
        }
    }

    internal abstract class Command : ICommand
    {
        public Task Execute(WebSession session, object obj)
        {
            return Execute(session);
        }

        public abstract Task Execute(WebSession session);
    }

    internal abstract class Command<TArg1, TArg2, TArg3, TArg4> : ICommand<TArg1, TArg2, TArg3, TArg4>
    {
        #region Implementation of ICommonCommand

        public abstract Task Execute(WebSession session, TArg1 arg1, TArg2 arg2, TArg3 arg3, TArg4 arg4);

        public Task Execute(WebSession session, object obj)
        {
            var args = (object[])obj;
            return Execute(session, (TArg1)args[0], (TArg2)args[1], (TArg3)args[2], (TArg4)args[3]);
        }

        #endregion
    }
}