﻿using System.Threading.Tasks;
using LobbyServer.Data;
using LobbyServer.Models;
using Player = LobbyServer.Models.Player;

namespace LobbyServer.BL
{
    internal class SetPlayerCurrencyCommand : Command<IPlayer, long>
    {
        private readonly IPlayerRepository _repository;

        public SetPlayerCurrencyCommand(IPlayerRepository repository)
        {
            _repository = repository;
        }

        public override async Task Execute(WebSession session, IPlayer iplayer, long currency)
        {
            var player = (Player) iplayer;
            player.Currency = currency;

            await _repository.Update(player.Data);
            session.Send(new ClientProtocol.CurrencyChangedEvent
            {
                NewCurrency = currency
            });
        }
    }
}
