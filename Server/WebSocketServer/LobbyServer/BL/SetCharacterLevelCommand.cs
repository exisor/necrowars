﻿using System.Threading.Tasks;
using ClientProtocol;
using LobbyServer.Data;
using LobbyServer.Models;
using Character = LobbyServer.Models.Character;

namespace LobbyServer.BL
{
    internal class SetCharacterLevelCommand : Command<ICharacter, int>
    {
        private const long LEVELUP_ACHIEVEMENT_ID = 1;

        private readonly ICharacterRepository _characterRepository;
        private readonly DefferedCommand<ProgressAchievementCommand> Achievement = new DefferedCommand<ProgressAchievementCommand>();

        public SetCharacterLevelCommand(ICharacterRepository characterRepository)
        {
            _characterRepository = characterRepository;
        }

        public override async Task Execute(WebSession session, ICharacter icharacter, int level)
        {
            var character = (Character) icharacter;
            var difference = level - character.Level;

            character.Level = level;

            await _characterRepository.Update(character.Data);
            session.Send(new CharacterLevelChangedEvent
            {
                CharacterId = character.Id,
                Level = level,
            });

            // lvlup achievement
            if (difference > 0)
            {
                await Achievement.Command.Execute(session, session.Player, LEVELUP_ACHIEVEMENT_ID, difference);
            }
        }
    }
}
