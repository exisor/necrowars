﻿using System.Threading.Tasks;
using Common;
using LobbyServer.Data;
using LobbyServer.Models;
using Character = LobbyServer.Models.Character;

namespace LobbyServer.BL
{
    internal class SelectCharacterAbilityCommand : Command<ICharacter, IAbilitySlot, IAbility>
    {
        private readonly ICharacterRepository _repository;

        public SelectCharacterAbilityCommand(ICharacterRepository repository)
        {
            _repository = repository;
        }

        public override async Task Execute(WebSession session, ICharacter icharacter, IAbilitySlot islot, IAbility ability)
        {
            var slot = (AbilitySlot) islot;
            var character = (Character) icharacter;
            slot.Ability = ability;
            character.Data.SelectedAbilities = ProtobufSerializer.SerializeBinary(character.AbilitySlots.DTO);
            await _repository.Update(character.Data);
            session.Send(new ClientProtocol.AbilitySlotChangedEvent
            {
                CharacterId = character.Id,
                Slot = slot.DTO
            });
        }
    }
}
