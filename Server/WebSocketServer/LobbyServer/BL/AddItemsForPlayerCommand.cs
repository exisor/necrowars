﻿using System.Collections.Generic;
using System.Linq;
using LobbyServer.Data;
using LobbyServer.Models;
using Item = LobbyServer.Models.Item;
using System.Threading.Tasks;
using Common.DTO;
using Common.ResourceManager;

namespace LobbyServer.BL
{
    internal class AddItemsForPlayerCommand : Command<Inventory, List<IItem>>
    {

        private readonly IItemRepository _repository;
        private readonly IItemResourceManager _itemResourceManager;
        private readonly DefferedCommand<ChangeItemQuantityCommand> ChangeItemQuantity = new DefferedCommand<ChangeItemQuantityCommand>();
        public AddItemsForPlayerCommand(IItemRepository repository, IItemResourceManager itemResourceManager)
        {
            _repository = repository;
            _itemResourceManager = itemResourceManager;
        }

        public override async Task Execute(WebSession session, Inventory inventory, List<IItem> items)
        {
            var addedItems = new List<Common.DTO.Item>();
            foreach (var item in items.Cast<Item>())
            {
                var itemBase = _itemResourceManager.Get(item.BaseId);
                if (itemBase.StackSize > 1)
                {
                    var sameStacks = inventory.Where(i => i.BaseId == itemBase.Id && i.ItemLevel == item.ItemLevel && i.Quantity < itemBase.StackSize).Cast<Item>();

                    foreach (var sameStack in sameStacks)
                    {
                        var canStoreItems = itemBase.StackSize - sameStack.Quantity;

                        item.Quantity -= canStoreItems;
                        if (item.Quantity <= 0)
                        {
                            await ChangeItemQuantity.Command.Execute(session, sameStack, sameStack.Quantity + item.Quantity + canStoreItems);
                            break;
                        }
                        else
                        {
                            await ChangeItemQuantity.Command.Execute(session, sameStack, sameStack.Quantity + canStoreItems);
                        }
                    }
                }
                if (item.Quantity <= 0) continue;
                item.Data.OwnerId = session.Player.Id;
                await _repository.Insert(item.Data);
                inventory.Add(item);
                addedItems.Add(item.DTO);
            }
            session.Send(new ClientProtocol.NewItemsEvent
            {
                Items = addedItems,
            });
        }
    }
}
