﻿
using System.Threading.Tasks;
using LobbyServer.BL.InstantItems;
using LobbyServer.Data;
using LobbyServer.Models;
using Item = LobbyServer.Models.Item;

namespace LobbyServer.BL
{
    internal class UnequipItemCommand : Command<IItem>
    {
        private readonly IItemRepository _itemRepository;
        private readonly IItemHandlerCollection _itemHandlers;

        public UnequipItemCommand(IItemRepository itemRepository, IItemHandlerCollection itemHandlers)
        {
            _itemRepository = itemRepository;
            _itemHandlers = itemHandlers;
        }

        public override async Task Execute(WebSession session, IItem iitem)
        {
            var item = (Item) iitem;
            var handler = _itemHandlers.OnUnequip(item.BaseId);
            if (handler != null && handler.CanHandle(session, item.Equipped, item))
            {
                await handler.Handle(session, item.Equipped, item);
            }
            item.Equipped = null;
            await _itemRepository.Update(item.Data);

            session.Send(new ClientProtocol.ItemUnequippedEvent
            {
                Id = item.Id
            });

        }
    }
}
