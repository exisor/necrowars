﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Common.ResourceManager;
using LobbyServer.Data;
using LobbyServer.Services;
using Player = LobbyServer.Models.Player;

namespace LobbyServer.BL
{
    internal class SetSessionUserIdCommand : Command<long>
    {
        private readonly IPlayerRepository _playerRepository;
        private readonly PlayersService _players;
#if DAILY_QUESTS
        private readonly IDailyQuestResourceManager _dailyQuestResourceManager;
#endif
        public SetSessionUserIdCommand(IPlayerRepository playerRepository, 
            PlayersService players
#if DAILY_QUESTS
            ,IDailyQuestResourceManager dailyQuestResourceManager
#endif
            )
        {
            _playerRepository = playerRepository;
            _players = players;
#if DAILY_QUESTS
            _dailyQuestResourceManager = dailyQuestResourceManager;
#endif
        }

        public override async Task Execute(WebSession session, long id)
        {
            session.UserId = id;

            var playerData = await _playerRepository.Get(id);
            if (playerData != null)
            {
                var player = new Player(playerData);
                session.Player = player;
#if DAILY_QUESTS
                if (!await _players.CheckDaily(playerData.LastLogin, session))
                {
                    var rnd = new Random((int)(playerData.LastLogin.Ticks + player.Id));

                    var quest = _dailyQuestResourceManager.All.OrderBy(_ => rnd.NextDouble()).FirstOrDefault();

                    player.QuestId = quest?.Id ?? 0;
                }
#endif
            }
        }
    }
}
