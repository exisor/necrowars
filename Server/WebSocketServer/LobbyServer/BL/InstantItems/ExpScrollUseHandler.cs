﻿using System;
using System.Linq;
using System.Threading.Tasks;
using LobbyServer.Models;
using LobbyServer.Services;

namespace LobbyServer.BL.InstantItems
{
    public class ExpScrollUseHandler : ItemHandler
    {
        private const long MINIMUM_EXPERIENCE = 1000000;
        private DefferedCommand<LevelupCharacterCommand> LevelUp = new DefferedCommand<LevelupCharacterCommand>();
        private DefferedCommand<SetCharacterExperienceCommand> SetExperience = new DefferedCommand<SetCharacterExperienceCommand>();
        public override long Id => 28;
        public override ItemHandlerType Type => ItemHandlerType.Use;
        protected override async Task HandleInternal(WebSession session, ICharacter character, IItem item)
        {
            if (character == null) character = session.Player.Characters.First();

            var topExp = Math.Max(character.RequiredExperience - character.Experience, MINIMUM_EXPERIENCE);
            var expToLevel = character.Experience;
            // 30% chance that 1000000
            if (RandomService.Instance.NextDouble() <= 0.3)
            {
                expToLevel += MINIMUM_EXPERIENCE;
            }
            else
            {
                expToLevel += (long)
                (topExp*
                 Math.Min(Math.Round(RandomService.Instance.NextDouble()),Math.Round(RandomService.Instance.NextDouble())));
            }
            while (expToLevel > character.RequiredExperience)
            {
                expToLevel -= character.RequiredExperience;

                await LevelUp.Command.Execute(session, character);
            }
            await SetExperience.Command.Execute(session, character, expToLevel);
        }
    }
}
