namespace LobbyServer.BL.InstantItems
{
    internal class ChargeUniqueUnequipHandler : RemoveSkillUniqueUnequipHandler
    {
        public override long Id => 5;
        protected override int SkillId => 38;
    }
}