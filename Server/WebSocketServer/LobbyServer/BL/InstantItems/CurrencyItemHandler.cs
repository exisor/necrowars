﻿using System.Threading.Tasks;
using LobbyServer.Models;

namespace LobbyServer.BL.InstantItems
{
    public class CurrencyItemHandler : ItemHandler
    {
        private readonly DefferedCommand<SetPlayerCurrencyCommand> SetCurrency = new DefferedCommand<SetPlayerCurrencyCommand>();
        public override long Id => 11;
        public override ItemHandlerType Type => ItemHandlerType.Use;
        protected override async Task HandleInternal(WebSession session, ICharacter character, IItem item)
        {
            await SetCurrency.Command.Execute(session, session.Player, session.Player.Currency + item.Quantity);
        }
    }
}