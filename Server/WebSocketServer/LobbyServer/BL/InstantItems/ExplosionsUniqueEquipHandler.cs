namespace LobbyServer.BL.InstantItems
{
    internal class ExplosionsUniqueEquipHandler : AddSkillUniqueEquipHandler
    {
        public override long Id => 9;
        protected override int SkillId => 42;
    }
}