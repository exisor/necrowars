﻿namespace LobbyServer.BL.InstantItems
{
    public class RandomLegendaryBootsItemHandler : RandomLegendaryItemHandler
    {
        private static readonly long[] UNIQUES = {
            46L,
            51L,
            56L,
            61L
        };
        public override long Id => 41;
        protected override long ItemBaseId => 18;
        protected override long[] Uniques => UNIQUES;
    }
}