﻿namespace LobbyServer.BL.InstantItems
{
    public class RandomLegendaryOffhandItemHandler : RandomLegendaryItemHandler
    {
        private static readonly long[] UNIQUES = {
            47L,
            52L,
            57L,
            62L
        };
        public override long Id => 38;
        protected override long ItemBaseId => 19;
        protected override long[] Uniques => UNIQUES;
    }
}