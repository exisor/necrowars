using System;
using System.Collections.Generic;
using System.Linq;

namespace LobbyServer.BL.InstantItems
{
    public class ItemHandlerCollection : IItemHandlerCollection
    {
        private class Handlers : Dictionary<long, IItemHandler>
        {
            public new IItemHandler this[long key]
            {
                get
                {
                    IItemHandler retval;
                    TryGetValue(key, out retval);
                    return retval;
                }
                set { base[key] = value; }
            }
        }

        private readonly Dictionary<ItemHandlerType, Handlers> _handlers;

        public ItemHandlerCollection(IEnumerable<IItemHandler> itemHandlers)
        {
            _handlers = Enum.GetValues(typeof (ItemHandlerType))
                .Cast<ItemHandlerType>()
                .ToDictionary(key => key, _ => new Handlers());
            foreach (var itemHandler in itemHandlers)
            {
                _handlers[itemHandler.Type].Add(itemHandler.Id, itemHandler);
            }
        }
        public IItemHandler OnEquip(long id)
        {
            return _handlers[ItemHandlerType.Equip][id];
        }

        public IItemHandler OnUnequip(long id)
        {
            return _handlers[ItemHandlerType.Unequip][id];
        }

        public IItemHandler OnUse(long id)
        {
            return _handlers[ItemHandlerType.Use][id];
        }
    }
}