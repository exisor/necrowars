namespace LobbyServer.BL.InstantItems
{
    internal class DrakarkUniqueUnequipHandler : RemoveSkillUniqueUnequipHandler
    {
        public override long Id => 8;
        protected override int SkillId => 41;
    }
}