﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ClientProtocol;
using Common.DTO;
using LobbyServer.Models;
using LobbyServer.Services;
using Item = Common.DTO.Item;

namespace LobbyServer.BL.InstantItems
{
    public abstract class RandomLegendaryItemHandler : ItemHandler
    {
        private const double _uniqueChance = 0.25;
        private readonly DefferedCommand<CreateItemForUserCommand> CreateItem = new DefferedCommand<CreateItemForUserCommand>();
        protected abstract long ItemBaseId { get; }
        public override ItemHandlerType Type => ItemHandlerType.Use;
        protected abstract long[] Uniques { get; }
        protected override async Task HandleInternal(WebSession session, ICharacter character, IItem item)
        {
            if (character == null) character = session.Player.Characters.First();

            var isUnique = Uniques != null && RandomService.Instance.NextDouble() < _uniqueChance;

            var baseId = isUnique ? Uniques[RandomService.Instance.Next(Uniques.Length)] : ItemBaseId;
            var rarity = isUnique ? Rarity.Unique : Rarity.Legendary;

            var template = new CreateItemForUserCommand.ItemTemplate(baseId, character.Level, 1, rarity);

            await CreateItem.Command.Execute(session, session.Player, template, i =>
            {
                session.Send(new ChestOpenEvent {BaseId = 4, ItemsReceived = new List<Item> {i.DTO}});
            });
        }
    }
}