﻿namespace LobbyServer.BL.InstantItems
{
    public class RandomLegendaryGlovesItemHandler : RandomLegendaryItemHandler
    {
        private static readonly long[] UNIQUES = {
            45L,
            50L,
            55L,
            60L
        };
        public override long Id => 40;
        protected override long ItemBaseId => 17;
        protected override long[] Uniques => UNIQUES;
    }
}