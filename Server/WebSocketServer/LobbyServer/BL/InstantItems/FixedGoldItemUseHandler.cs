﻿
using System.Threading.Tasks;
using LobbyServer.Models;

namespace LobbyServer.BL.InstantItems
{
    internal class FixedGoldItemUseHandler : ItemHandler
    {
        private readonly DefferedCommand<SetPlayerGoldCommand> SetGold = new DefferedCommand<SetPlayerGoldCommand>();
        public override long Id => 29;
        public override ItemHandlerType Type =>ItemHandlerType.Use;
        protected override async Task HandleInternal(WebSession session, ICharacter character, IItem item)
        {
            await SetGold.Command.Execute(session, session.Player, session.Player.Gold + item.Quantity);
        }
    }
}
