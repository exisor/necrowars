using System;
using System.Threading.Tasks;
using LobbyServer.Models;

namespace LobbyServer.BL.InstantItems
{
    internal abstract class AddSkillUniqueEquipHandler : ItemHandler
    {
        private readonly DefferedCommand<AddSkillForCharacterCommand> AddSkill = new DefferedCommand<AddSkillForCharacterCommand>();
        protected abstract int SkillId { get; }
        public override ItemHandlerType Type => ItemHandlerType.Equip;
        protected override async Task HandleInternal(WebSession session, ICharacter character, IItem item)
        {
            if (character == null) throw new ArgumentNullException(nameof(character));

            await AddSkill.Command.Execute(session, character, new Ability(new Common.DTO.Ability
            {
                Id = SkillId,
                Level = 1,
            }));
        }
    }
}