﻿using System;
using System.Threading.Tasks;
using Common;
using LobbyServer.Data;
using LobbyServer.Models;

namespace LobbyServer.BL.InstantItems
{
    public abstract class ItemHandler : IItemHandler
    {
        protected ItemHandler() { }
        public abstract long Id { get; }
        public abstract ItemHandlerType Type { get; }

        public Task Handle(WebSession session, ICharacter character, IItem item)
        {
            if (item == null) throw new ArgumentException("item");
            if (item.BaseId != Id) throw new ArgumentException("item.BaseId");

            return HandleInternal(session, character, item);
        }

        public virtual bool CanHandle(WebSession session, ICharacter character, IItem item)
        {
            return true;
        }

        protected abstract Task HandleInternal(WebSession session, ICharacter character, IItem item);
    }
}