﻿using System.Configuration;

namespace LobbyServer.BL.InstantItems
{
    public interface IItemHandlerCollection
    {
        IItemHandler OnEquip(long id);
        IItemHandler OnUnequip(long id);
        IItemHandler OnUse(long id);
    }
}