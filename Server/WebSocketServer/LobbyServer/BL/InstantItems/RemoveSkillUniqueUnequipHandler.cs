using System;
using System.Threading.Tasks;
using LobbyServer.Models;

namespace LobbyServer.BL.InstantItems
{
    internal abstract class RemoveSkillUniqueUnequipHandler : ItemHandler
    {
        private readonly DefferedCommand<RemoveSkillFromCharacterCommand> RemoveSkill = new DefferedCommand<RemoveSkillFromCharacterCommand>();
        protected abstract int SkillId { get; }
        public override ItemHandlerType Type => ItemHandlerType.Unequip;
        public override bool CanHandle(WebSession session, ICharacter character, IItem item)
        {
            if (character == null) throw new ArgumentNullException(nameof(character));

            var ability = character.Abilities[SkillId];
            if (ability == null)
            {
                return false;
            }
            return true;
        }

        protected override async Task HandleInternal(WebSession session, ICharacter character, IItem item)
        {
            var ability = character.Abilities[SkillId];
            await RemoveSkill.Command.Execute(session, character, ability);
        }
    }
}