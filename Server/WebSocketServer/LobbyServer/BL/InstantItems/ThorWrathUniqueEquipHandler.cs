namespace LobbyServer.BL.InstantItems
{
    internal class ThorWrathUniqueEquipHandler : AddSkillUniqueEquipHandler
    {
        public override long Id => 10;
        protected override int SkillId => 43;
    }
}