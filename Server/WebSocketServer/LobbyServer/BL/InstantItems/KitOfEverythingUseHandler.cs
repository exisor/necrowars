﻿using System.Collections.Generic;
using System.Threading.Tasks;
using ClientProtocol;
using LobbyServer.Models;

namespace LobbyServer.BL.InstantItems
{
    internal class KitOfEverythingUseHandler : ItemHandler
    {
        private readonly DefferedCommand<CreateItemForUserCommand> CreateItem = new DefferedCommand<CreateItemForUserCommand>();

        public override long Id => 26;
        public override ItemHandlerType Type =>ItemHandlerType.Use;
        protected override async Task HandleInternal(WebSession session, ICharacter character, IItem item)
        {
            var items = new[]
            {
                // surprise chest
                new CreateItemForUserCommand.ItemTemplate(
                    baseId: 25,
                    level: 1,
                    quantity: 1),
                // inv++
                new CreateItemForUserCommand.ItemTemplate(
                    baseId: 20,
                    level: 1,
                    quantity: 1),
                // scrolls of refund
                new CreateItemForUserCommand.ItemTemplate(
                    baseId: 24,
                    level: 1,
                    quantity: 10),
                // hp potion lv1
                new CreateItemForUserCommand.ItemTemplate(
                    baseId: 4,
                    level: 1,
                    quantity: 10),
                // cd potion lvl
                new CreateItemForUserCommand.ItemTemplate(
                    baseId: 3,
                    level: 1,
                    quantity: 10),
            };
            var itemsAdded = new List<Common.DTO.Item>();
            foreach (var itemTemplate in items)
            {
                await CreateItem.Command.Execute(session, session.Player, itemTemplate, i => itemsAdded.Add(i.DTO));
            }
            session.Send(new ChestOpenEvent
            {
                BaseId = 1,
                ItemsReceived = itemsAdded
            });
        }
    }
}
