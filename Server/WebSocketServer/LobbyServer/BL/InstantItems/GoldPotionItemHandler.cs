using System;
using System.Threading.Tasks;
using ClientProtocol;
using LobbyServer.Models;

namespace LobbyServer.BL.InstantItems
{
    internal class GoldPotionItemHandler : ItemHandler
    {
        private readonly DefferedCommand<AddEffectForPlayerCommand> AddEffect = new DefferedCommand<AddEffectForPlayerCommand>();
        public override long Id => 14;
        public override ItemHandlerType Type => ItemHandlerType.Use;
        public override bool CanHandle(WebSession session, ICharacter character, IItem item)
        {

            if (session.Player.Effects.HasEffect(EffectType.GoldPotion))
            {
                session.Error(this, ErrorCode.EffectAlreadyApplied);
                return false;
            }
            return true;
        }

        protected override async Task HandleInternal(WebSession session, ICharacter character, IItem item)
        {

            var effect = new Effect(new Data.Effect
            {
                Duration = (long)TimeSpan.FromHours(1).TotalSeconds,
                EffectBase = (long)EffectType.GoldPotion,
                UserId = session.Player.Id
            });

            await AddEffect.Command.Execute(session, session.Player, effect);
        }
    }
}