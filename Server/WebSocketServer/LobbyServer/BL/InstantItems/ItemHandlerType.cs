namespace LobbyServer.BL.InstantItems
{
    public enum ItemHandlerType
    {
        Use,
        Equip,
        Unequip,
    }
}