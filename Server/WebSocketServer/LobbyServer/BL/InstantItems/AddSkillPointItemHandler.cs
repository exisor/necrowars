﻿using System;
using System.Linq;
using System.Threading.Tasks;
using LobbyServer.Data;
using LobbyServer.Models;

namespace LobbyServer.BL.InstantItems
{
    internal class AddSkillPointItemHandler : ItemHandler
    {
        private readonly DefferedCommand<SetCharacterApCommand> SetAP = new DefferedCommand<SetCharacterApCommand>();

        public override long Id => 23;
        public override ItemHandlerType Type => ItemHandlerType.Use;
        protected override async Task HandleInternal(WebSession session, ICharacter character, IItem item)
        {
            if (character == null) character = session.Player.Characters.First();
            if (item == null) throw new ArgumentNullException(nameof(item));

            await SetAP.Command.Execute(session, character, character.AP + item.Quantity);
        }
    }
}
