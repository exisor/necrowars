namespace LobbyServer.BL.InstantItems
{
    internal class WallOfShieldsUniqueEquipHandler : AddSkillUniqueEquipHandler
    {
        public override long Id => 6;
        protected override int SkillId => 39;
    }
}