namespace LobbyServer.BL.InstantItems
{
    internal class WallOfShieldsUniqueUnequipHandler : RemoveSkillUniqueUnequipHandler
    {
        public override long Id => 6;
        protected override int SkillId => 39;
    }
}