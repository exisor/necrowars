namespace LobbyServer.BL.InstantItems
{
    internal class ChargeUniqueEquipHandler : AddSkillUniqueEquipHandler
    {
        protected override int SkillId => 38;
        public override long Id => 5;
    }
}