﻿using System.Threading.Tasks;
using LobbyServer.Models;

namespace LobbyServer.BL.InstantItems
{
    public interface IItemHandler
    {
        long Id { get; }
        ItemHandlerType Type { get; }
        Task Handle(WebSession session, ICharacter character, IItem item);
        bool CanHandle(WebSession session, ICharacter character, IItem item);
    }
}
