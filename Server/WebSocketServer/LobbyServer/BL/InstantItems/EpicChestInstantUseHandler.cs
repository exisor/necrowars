﻿using System.Linq;
using System.Threading.Tasks;
using LobbyServer.Models;

namespace LobbyServer.BL.InstantItems
{
    internal class EpicChestInstantUseHandler : ItemHandler
    {
        private readonly DefferedCommand<OpenChestForCharacterCommand> OpenChest = new DefferedCommand<OpenChestForCharacterCommand>();

        public override long Id => 22;
        public override ItemHandlerType Type => ItemHandlerType.Use;
        protected override async Task HandleInternal(WebSession session, ICharacter character, IItem item)
        {
            if (character == null)
            {
                character = session.Player.Characters.First();
            }
            for (var i = 0; i < item.Quantity; i++)
            {
                var chest = Chest.New(3, character.Level);
                await OpenChest.Command.Execute(session, session.Player, character, chest);
            }
        }
    }
}