﻿using System.Threading.Tasks;
using ClientProtocol;
using LobbyServer.Data;
using LobbyServer.Models;
using Player = LobbyServer.Models.Player;

namespace LobbyServer.BL.InstantItems
{
    public class BonusInventoryItemHandler : ItemHandler
    {
        private readonly IPlayerRepository _playerRepository;

        public BonusInventoryItemHandler(IPlayerRepository playerRepository)
        {
            _playerRepository = playerRepository;
        }

        #region Overrides of ItemHandler
        public override long Id => 20;
        public override ItemHandlerType Type => ItemHandlerType.Use;
        protected override async Task HandleInternal(WebSession session, ICharacter character, IItem item)
        {
            var player = (Player) session.Player;
            player.BonusInventory += 5 * item.Quantity;
            await _playerRepository.Update(player.Data);
            session.Send(new InventoryCapacityChangedEvent {NewCapacity = player.BonusInventory + Inventory.CAPACITY});
        }

        #endregion
    }
}