﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ClientProtocol;
using Common.DTO;
using LobbyServer.Models;

namespace LobbyServer.BL.InstantItems
{
    internal class SixSlotChestItemUseHandler : ItemHandler
    {
        private readonly DefferedCommand<CreateItemForUserCommand> CreateItem = new DefferedCommand<CreateItemForUserCommand>();
        public override long Id => 30;
        public override ItemHandlerType Type => ItemHandlerType.Use;
        protected override async Task HandleInternal(WebSession session, ICharacter character, IItem item)
        {
            if (character == null) character = session.Player.Characters.First();
            var itemTemplates =
                new[] {1, 2, 16, 17, 18, 19}.Select(
                    id => new CreateItemForUserCommand.ItemTemplate(id, character.Level, 1, Rarity.Epic));
            var items = new List<Common.DTO.Item>();
            foreach (var itemTemplate in itemTemplates)
            {
                await CreateItem.Command.Execute(session, session.Player, itemTemplate, i => items.Add(i.DTO));
            }
            session.Send(new ChestOpenEvent {BaseId = 4, ItemsReceived = items});
        }
    }
}
