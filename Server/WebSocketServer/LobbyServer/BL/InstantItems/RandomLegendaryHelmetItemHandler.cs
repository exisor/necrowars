﻿namespace LobbyServer.BL.InstantItems
{
    public class RandomLegendaryHelmetItemHandler : RandomLegendaryItemHandler
    {
        private static readonly long[] UNIQUES = {
            44L,
            49L,
            54L,
            59L
        };
        public override long Id => 39;
        protected override long ItemBaseId => 16;
        protected override long[] Uniques => UNIQUES;
    }
}