namespace LobbyServer.BL.InstantItems
{
    internal class DrakkarUniqueEquipHandler : AddSkillUniqueEquipHandler
    {
        public override long Id => 8;
        protected override int SkillId => 41;
    }
}