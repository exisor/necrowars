﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Common.DTO;
using Common.ResourceManager;
using LobbyServer.Models;
using LobbyServer.Services;

namespace LobbyServer.BL.InstantItems
{
    internal class RandomUniqueItemHandler : ItemHandler
    {
        private readonly ItemGenerator _itemGenerator;
        private readonly IItemResourceManager _itemResourceManager;
        private readonly DefferedCommand<AddItemsForPlayerCommand> AddItem = new DefferedCommand<AddItemsForPlayerCommand>();
        private struct UniqueChance
        {
            public float Weight;
            public long BaseId;
            public int MinimumLevel;
        }

        private readonly List<UniqueChance> _chances = new List<UniqueChance>
        {
            new UniqueChance {BaseId = 5, MinimumLevel = 1, Weight = 500},
            new UniqueChance {BaseId = 6, MinimumLevel = 1, Weight = 500},
            new UniqueChance {BaseId = 7, MinimumLevel = 1, Weight = 500},
            new UniqueChance {BaseId = 8, MinimumLevel = 1, Weight = 500},
            new UniqueChance {BaseId = 9, MinimumLevel = 1, Weight = 500},
            new UniqueChance {BaseId = 10, MinimumLevel = 1, Weight = 500},
        };

        public RandomUniqueItemHandler(ItemGenerator itemGenerator, IItemResourceManager itemResourceManager)
        {
            _itemGenerator = itemGenerator;
            _itemResourceManager = itemResourceManager;
        }

        public override long Id => 12;
        public override ItemHandlerType Type => ItemHandlerType.Use;
        protected override async Task HandleInternal(WebSession session, ICharacter character, IItem item)
        {
            if (character == null)
            {
                character = session.Player.Characters.First();
            }
            var roll = _chances.Where(c=> c.MinimumLevel <= character.Level).OrderByDescending(c => c.Weight*RandomService.Instance.NextDouble()).First();
            var resource = _itemResourceManager.Get(roll.BaseId);
            var uniqueItem = _itemGenerator.CreateItem(character.Level, resource, Rarity.Unique);

            await AddItem.Command.Execute(session, session.Player.Inventory, new List<IItem> {uniqueItem});
        }
    }
}