namespace LobbyServer.BL.InstantItems
{
    internal class ThorWrathUniqueUnequipHandler : RemoveSkillUniqueUnequipHandler
    {
        public override long Id => 10;
        protected override int SkillId => 43;
    }
}