﻿using System.Linq;
using System.Threading.Tasks;
using ClientProtocol;
using LobbyServer.Models;

namespace LobbyServer.BL.InstantItems
{
    public class SkillPointsResetUserHandler : ItemHandler
    {
        private readonly DefferedCommand<SetCharacterAbilityLevelCommand> SetAbilityLevel = new DefferedCommand<SetCharacterAbilityLevelCommand>();
        private readonly DefferedCommand<SetCharacterApCommand> SetAp = new DefferedCommand<SetCharacterApCommand>();
        private readonly DefferedCommand<SelectCharacterAbilityCommand> SelectAbility = new DefferedCommand<SelectCharacterAbilityCommand>();
        public override long Id => 27;
        public override ItemHandlerType Type =>ItemHandlerType.Use;
        protected override async Task HandleInternal(WebSession session, ICharacter icharacter, IItem item)
        {
            var totalAP = 0;
            var character = (Character) icharacter ?? session.Player.Characters.First();
            // for all abilities except resurrection
            foreach (var ability in character.Abilities.Where(a => a.Id != 1))
            {
                totalAP += ability.Level;
                await SetAbilityLevel.Command.Execute(session, ability, character, 0);
                if (ability.SelectedInSlot != null)
                {
                    await SelectAbility.Command.Execute(session, character, ability.SelectedInSlot, null);
                }
            }
            await SetAp.Command.Execute(session, character, character.AP + totalAP);
            session.Send(new SkillsResetEvent());
        }
    }
}