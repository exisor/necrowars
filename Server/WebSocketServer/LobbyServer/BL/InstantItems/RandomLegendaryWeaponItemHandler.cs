﻿namespace LobbyServer.BL.InstantItems
{
    public class RandomLegendaryWeaponItemHandler : RandomLegendaryItemHandler
    {
        private static readonly long[] UNIQUES = {
            63L
        };
        public override long Id => 37;
        protected override long ItemBaseId => 2;
        protected override long[] Uniques => null;
    }
}