﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ClientProtocol;
using Common.DTO;
using LobbyServer.Models;
using LobbyServer.Services;

namespace LobbyServer.BL.InstantItems
{
    public class SurpriseChestUseHandler : ItemHandler
    {
        private readonly DefferedCommand<CreateItemForUserCommand> CreateItem = new DefferedCommand<CreateItemForUserCommand>();

        public struct ItemDropProbability
        {
            public long BaseId;
            public int Quantity;
            public Rarity? Rarity;
            public double Weight;
        }

        private static readonly List<ItemDropProbability> ItemList = new List<ItemDropProbability>
        {
            #region BODY
            // Body Common
            new ItemDropProbability
            {
                BaseId = 1,
                Quantity = 1,
                Rarity = Rarity.Common,
                Weight = 1700,
            },
            // Body Rare
            new ItemDropProbability
            {
                BaseId = 1,
                Quantity = 1,
                Rarity = Rarity.Rare,
                Weight = 1500,
            },
            // Body Epic
            new ItemDropProbability
            {
                BaseId = 1,
                Quantity = 1,
                Rarity = Rarity.Epic,
                Weight = 1300,
            },
            // Body Legendary
            new ItemDropProbability
            {
                BaseId = 1,
                Quantity = 1,
                Rarity = Rarity.Legendary,
                Weight = 1100,
            },
            #endregion
            #region WEAPON
            // WEAPON Common
            new ItemDropProbability
            {
                BaseId = 2,
                Quantity = 1,
                Rarity = Rarity.Common,
                Weight = 1700,
            },
            // WEAPON Rare
            new ItemDropProbability
            {
                BaseId = 2,
                Quantity = 1,
                Rarity = Rarity.Rare,
                Weight = 1500,
            },
            // WEAPON Epic
            new ItemDropProbability
            {
                BaseId = 2,
                Quantity = 1,
                Rarity = Rarity.Epic,
                Weight = 1300,
            },
            // WEAPON Legendary
            new ItemDropProbability
            {
                BaseId = 2,
                Quantity = 1,
                Rarity = Rarity.Legendary,
                Weight = 1100,
            },
            #endregion
            #region HPPOT
            new ItemDropProbability
            {
                BaseId = 3,
                Quantity = 1,
                Rarity = null,
                Weight = 1900,
            },
            new ItemDropProbability
            {
                BaseId = 3,
                Quantity = 2,
                Rarity = null,
                Weight = 1800,
            },
            new ItemDropProbability
            {
                BaseId = 3,
                Quantity = 5,
                Rarity = null,
                Weight = 1700,
            },
            new ItemDropProbability
            {
                BaseId = 3,
                Quantity = 20,
                Rarity = null,
                Weight = 1600,
            },
            new ItemDropProbability
            {
                BaseId = 3,
                Quantity = 50,
                Rarity = null,
                Weight = 1500,
            },
            new ItemDropProbability
            {
                BaseId = 3,
                Quantity = 100,
                Rarity = null,
                Weight = 1400,
            },
            #endregion
            #region CD Potion
            new ItemDropProbability
            {
                BaseId = 4,
                Quantity = 1,
                Rarity = null,
                Weight = 1700,
            },
            new ItemDropProbability
            {
                BaseId = 4,
                Quantity = 2,
                Rarity = null,
                Weight = 1500,
            },
            new ItemDropProbability
            {
                BaseId = 4,
                Quantity = 3,
                Rarity = null,
                Weight = 1300,
            },
            new ItemDropProbability
            {
                BaseId = 4,
                Quantity = 4,
                Rarity = null,
                Weight = 1100,
            },
            new ItemDropProbability
            {
                BaseId = 4,
                Quantity = 5,
                Rarity = null,
                Weight = 10,
            },
            #endregion
            #region Currency
            new ItemDropProbability
            {
                BaseId = 11,
                Quantity = 1,
                Rarity = null,
                Weight = 1500,
            },
            new ItemDropProbability
            {
                BaseId = 11,
                Quantity = 2,
                Rarity = null,
                Weight = 1300,
            },
            new ItemDropProbability
            {
                BaseId = 11,
                Quantity = 3,
                Rarity = null,
                Weight = 1100,
            },
            new ItemDropProbability
            {
                BaseId = 11,
                Quantity = 4,
                Rarity = null,
                Weight = 900,
            },
            new ItemDropProbability
            {
                BaseId = 11,
                Quantity = 5,
                Rarity = null,
                Weight = 10,
            },
            #endregion
            // Random unique
            new ItemDropProbability
            {
                BaseId = 12,
                Quantity = 1,
                Rarity = null,
                Weight = 1100,
            },
            #region Exp Potion
            new ItemDropProbability
            {
                BaseId = 13,
                Quantity = 1,
                Rarity = null,
                Weight = 1500,
            },
            new ItemDropProbability
            {
                BaseId = 13,
                Quantity = 2,
                Rarity = null,
                Weight = 1300,
            },
            new ItemDropProbability
            {
                BaseId = 13,
                Quantity = 3,
                Rarity = null,
                Weight = 1100,
            },
            new ItemDropProbability
            {
                BaseId = 13,
                Quantity = 4,
                Rarity = null,
                Weight = 1000,
            },
            #endregion
            #region Gold Potion
            new ItemDropProbability
            {
                BaseId = 14,
                Quantity = 1,
                Rarity = null,
                Weight = 1500,
            },
            new ItemDropProbability
            {
                BaseId = 14,
                Quantity = 2,
                Rarity = null,
                Weight = 1300,
            },
            new ItemDropProbability
            {
                BaseId = 14,
                Quantity = 3,
                Rarity = null,
                Weight = 1100,
            },
            new ItemDropProbability
            {
                BaseId = 14,
                Quantity = 4,
                Rarity = null,
                Weight = 1000,
            },
            #endregion
            #region Legendary Chest
            new ItemDropProbability
            {
                BaseId = 15,
                Quantity = 1,
                Rarity = null,
                Weight = 1600,
            },
            new ItemDropProbability
            {
                BaseId = 15,
                Quantity = 2,
                Rarity = null,
                Weight = 1400,
            },
            new ItemDropProbability
            {
                BaseId = 15,
                Quantity = 3,
                Rarity = null,
                Weight = 1200,
            },
            new ItemDropProbability
            {
                BaseId = 15,
                Quantity = 4,
                Rarity = null,
                Weight = 1000,
            },
            #endregion
            #region HEAD
            // HEAD Common
            new ItemDropProbability
            {
                BaseId = 16,
                Quantity = 1,
                Rarity = Rarity.Common,
                Weight = 1700,
            },
            // HEAD Rare
            new ItemDropProbability
            {
                BaseId = 16,
                Quantity = 1,
                Rarity = Rarity.Rare,
                Weight = 1500,
            },
            // HEAD Epic
            new ItemDropProbability
            {
                BaseId = 16,
                Quantity = 1,
                Rarity = Rarity.Epic,
                Weight = 1300,
            },
            // HEAD Legendary
            new ItemDropProbability
            {
                BaseId = 16,
                Quantity = 1,
                Rarity = Rarity.Legendary,
                Weight = 1100,
            },
            #endregion
            #region GLOVES
            // GLOVES Common
            new ItemDropProbability
            {
                BaseId = 17,
                Quantity = 1,
                Rarity = Rarity.Common,
                Weight = 1700,
            },
            // GLOVES Rare
            new ItemDropProbability
            {
                BaseId = 17,
                Quantity = 1,
                Rarity = Rarity.Rare,
                Weight = 1500,
            },
            // GLOVES Epic
            new ItemDropProbability
            {
                BaseId = 17,
                Quantity = 1,
                Rarity = Rarity.Epic,
                Weight = 1300,
            },
            // GLOVES Legendary
            new ItemDropProbability
            {
                BaseId = 17,
                Quantity = 1,
                Rarity = Rarity.Legendary,
                Weight = 1100,
            },
            #endregion
            #region BOOTS
            // BOOTS Common
            new ItemDropProbability
            {
                BaseId = 18,
                Quantity = 1,
                Rarity = Rarity.Common,
                Weight = 1700,
            },
            // BOOTS Rare
            new ItemDropProbability
            {
                BaseId = 18,
                Quantity = 1,
                Rarity = Rarity.Rare,
                Weight = 1500,
            },
            // BOOTS Epic
            new ItemDropProbability
            {
                BaseId = 18,
                Quantity = 1,
                Rarity = Rarity.Epic,
                Weight = 1300,
            },
            // BOOTS Legendary
            new ItemDropProbability
            {
                BaseId = 18,
                Quantity = 1,
                Rarity = Rarity.Legendary,
                Weight = 1100,
            },
            #endregion
            #region Offhand
            // Offhand Common
            new ItemDropProbability
            {
                BaseId = 19,
                Quantity = 1,
                Rarity = Rarity.Common,
                Weight = 1700,
            },
            // Offhand Rare
            new ItemDropProbability
            {
                BaseId = 19,
                Quantity = 1,
                Rarity = Rarity.Rare,
                Weight = 1500,
            },
            // Offhand Epic
            new ItemDropProbability
            {
                BaseId = 19,
                Quantity = 1,
                Rarity = Rarity.Epic,
                Weight = 1300,
            },
            // Offhand Legendary
            new ItemDropProbability
            {
                BaseId = 19,
                Quantity = 1,
                Rarity = Rarity.Legendary,
                Weight = 1100,
            },
            #endregion
            #region Inventory++
            new ItemDropProbability
            {
                BaseId = 20,
                Quantity = 1,
                Rarity = null,
                Weight = 1500,
            },

            new ItemDropProbability
            {
                BaseId = 20,
                Quantity = 2,
                Rarity = null,
                Weight = 1100,
            },
            #endregion
            #region GOLD
            new ItemDropProbability
            {
                BaseId = 21,
                Quantity = 1,
                Rarity = null,
                Weight = 2100
            },
            new ItemDropProbability
            {
                BaseId = 21,
                Quantity = 2,
                Rarity = null,
                Weight = 1900
            },
            new ItemDropProbability
            {
                BaseId = 21,
                Quantity = 5,
                Rarity = null,
                Weight = 1700
            },
            new ItemDropProbability
            {
                BaseId = 21,
                Quantity = 10,
                Rarity = null,
                Weight = 1500
            },
            new ItemDropProbability
            {
                BaseId = 21,
                Quantity = 20,
                Rarity = null,
                Weight = 1300
            },
            new ItemDropProbability
            {
                BaseId = 21,
                Quantity = 50,
                Rarity = null,
                Weight = 1100
            },
            new ItemDropProbability
            {
                BaseId = 21,
                Quantity = 100,
                Rarity = null,
                Weight = 900
            },
            #endregion
            #region Epic chest
            new ItemDropProbability
            {
                BaseId = 22,
                Quantity = 1,
                Rarity = null,
                Weight = 2100,
            },
            new ItemDropProbability
            {
                BaseId = 22,
                Quantity = 2,
                Rarity = null,
                Weight = 1700,
            },
            new ItemDropProbability
            {
                BaseId = 22,
                Quantity = 3,
                Rarity = null,
                Weight = 1300,
            },
            new ItemDropProbability
            {
                BaseId = 22,
                Quantity = 4,
                Rarity = null,
                Weight = 900,
            },
            #endregion
            #region Skill Point
            new ItemDropProbability
            {
                BaseId = 23,
                Quantity = 1,
                Rarity = null,
                Weight = 1400,
            },
            new ItemDropProbability
            {
                BaseId = 23,
                Quantity = 2,
                Rarity = null,
                Weight = 1200,
            },
            new ItemDropProbability
            {
                BaseId = 23,
                Quantity = 3,
                Rarity = null,
                Weight = 900,
            },
            new ItemDropProbability
            {
                BaseId = 23,
                Quantity = 4,
                Rarity = null,
                Weight = 700,
            },
            #endregion
            #region Skill Point refund
            new ItemDropProbability
            {
                BaseId = 24,
                Quantity = 1,
                Rarity = null,
                Weight = 1800,
            },
            new ItemDropProbability
            {
                BaseId = 24,
                Quantity = 2,
                Rarity = null,
                Weight = 1300,
            },
            new ItemDropProbability
            {
                BaseId = 24,
                Quantity = 3,
                Rarity = null,
                Weight = 1000,
            },
            new ItemDropProbability
            {
                BaseId = 24,
                Quantity = 4,
                Rarity = null,
                Weight = 800,
            },
            #endregion
            #region Surprise Chest
            new ItemDropProbability
            {
                BaseId = 25,
                Quantity = 1,
                Rarity = null,
                Weight = 1100,
            },
            new ItemDropProbability
            {
                BaseId = 25,
                Quantity = 2,
                Rarity = null,
                Weight = 1000,
            },
            new ItemDropProbability
            {
                BaseId = 25,
                Quantity = 3,
                Rarity = null,
                Weight = 900,
            },
            #endregion
            new ItemDropProbability
            {
                BaseId = 28,
                Quantity = 1,
                Rarity = null,
                Weight = 1200,
            },
            new ItemDropProbability
            {
                BaseId = 30,
                Quantity = 1,
                Rarity = null,
                Weight = 1200,
            }
        };

        public override long Id => 25;
        public override ItemHandlerType Type => ItemHandlerType.Use;
        protected override async Task HandleInternal(WebSession session, ICharacter character, IItem item)
        {
            var roll = Enumerable.Range(0, RandomService.Instance.Next(1, 5)).Select(_ =>
            {
                var r = Roll();
                return new CreateItemForUserCommand.ItemTemplate(baseId: r.BaseId, level: character.Level,
                    quantity: r.Quantity, rarity: r.Rarity);

            });
            var list = new List<Common.DTO.Item>();
            foreach (var itemTemplate in roll)
            {
                await CreateItem.Command.Execute(session, session.Player, itemTemplate, i => list.Add(i.DTO));
            }
            session.Send(new ChestOpenEvent {BaseId = 4, ItemsReceived = list});
        }

        public static ItemDropProbability Roll()
        {
            return ItemList.OrderByDescending(p => p.Weight*RandomService.Instance.NextDouble() * RandomService.Instance.NextDouble()).First();
        }
    }
}
