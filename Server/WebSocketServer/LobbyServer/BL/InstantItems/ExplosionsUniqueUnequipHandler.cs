namespace LobbyServer.BL.InstantItems
{
    internal class ExplosionsUniqueUnequipHandler : RemoveSkillUniqueUnequipHandler
    {
        public override long Id => 9;
        protected override int SkillId => 42;
    }
}