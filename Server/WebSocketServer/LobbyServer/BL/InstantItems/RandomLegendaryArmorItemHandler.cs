﻿using System.Collections.Generic;

namespace LobbyServer.BL.InstantItems
{
    public class RandomLegendaryArmorItemHandler : RandomLegendaryItemHandler
    {
        private static readonly long[] UNIQUES = {
            43L,
            48L,
            53L,
            58L
        };
        public override long Id => 36;
        protected override long ItemBaseId => 1;
        protected override long[] Uniques => UNIQUES;
    }
}