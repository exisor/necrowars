namespace LobbyServer.BL.InstantItems
{
    internal class BerserkUniqueEquipHandler : AddSkillUniqueEquipHandler
    {
        public override long Id => 7;
        protected override int SkillId => 40;
    }
}