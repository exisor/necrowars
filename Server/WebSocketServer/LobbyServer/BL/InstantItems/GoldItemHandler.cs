﻿using System.Linq;
using System.Threading.Tasks;
using Common.ResourceManager;
using LobbyServer.Models;

namespace LobbyServer.BL.InstantItems
{
    public class GoldItemHandler : ItemHandler
    {
        private readonly IAffixResourceManager _affixResourceManager;
        private readonly DefferedCommand<SetPlayerGoldCommand> SetGold = new DefferedCommand<SetPlayerGoldCommand>();
        public GoldItemHandler(IAffixResourceManager affixResourceManager)
        {
            _affixResourceManager = affixResourceManager;
        }

        public override long Id => 21;
        public override ItemHandlerType Type =>ItemHandlerType.Use;
        protected override async Task HandleInternal(WebSession session, ICharacter character, IItem item)
        {
            var affixes = _affixResourceManager.GetAffixValues(item.DTO);
            var gold = affixes.First().GetValue("Gold");
            await SetGold.Command.Execute(session, session.Player, session.Player.Gold + gold*item.Quantity);
        }
    }
}