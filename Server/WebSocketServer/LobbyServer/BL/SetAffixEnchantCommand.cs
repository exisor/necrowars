﻿using System.Linq;
using System.Threading.Tasks;
using Common;
using Common.DTO;
using LobbyServer.Data;
using LobbyServer.Models;
using Item = LobbyServer.Models.Item;

namespace LobbyServer.BL
{
    internal class SetAffixEnchantCommand : Command<IItem, long, int>
    {
        private readonly IItemRepository _itemRepository;

        public SetAffixEnchantCommand(IItemRepository itemRepository)
        {
            _itemRepository = itemRepository;
        }

        public override async Task Execute(WebSession session, IItem iitem, long affixId, int value)
        {
            var item = (Item) iitem;
            var affixes = item.Affixes.ToList();
            var affix = affixes.First(a => a.Id == affixId);
            affix.Enchant = value;
            item.Data.AffixData = ProtobufSerializer.SerializeBinary(affixes);
            await _itemRepository.Update(item.Data);
            session.Send(new ClientProtocol.ItemChangedEvent
            {
                Item = item.DTO
            });
        }
    }
}
