﻿using System.Threading.Tasks;
using LobbyServer.Data;
using LobbyServer.Models;
using Effect = LobbyServer.Models.Effect;

namespace LobbyServer.BL
{
    internal class AddEffectForPlayerCommand : Command<IPlayer, IEffect>
    {
        private readonly IEffectRepository _effectRepository;

        public AddEffectForPlayerCommand(IEffectRepository effectRepository)
        {
            _effectRepository = effectRepository;
        }

        public override async Task Execute(WebSession session, IPlayer player, IEffect ieffect)
        {
            var effect = (Effect) ieffect;
            await _effectRepository.Insert(effect.Data);
            player.Effects.Add(effect);
            session.Send(new ClientProtocol.NewPowerUpEvent
            {
                PowerUp = effect.DTO
            });
        }
    }
}
