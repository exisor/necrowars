﻿using System.Threading.Tasks;

namespace LobbyServer.BL
{
    internal interface ICommonCommand
    {
        Task Execute(WebSession session, object obj);
    }
}