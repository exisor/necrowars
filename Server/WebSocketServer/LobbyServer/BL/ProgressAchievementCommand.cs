﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ClientProtocol;
using Common.ResourceManager;
using LobbyServer.Data;
using LobbyServer.Models;
using Achievement = LobbyServer.Models.Achievement;
using Player = LobbyServer.Models.Player;

namespace LobbyServer.BL
{
    internal class ProgressAchievementCommand : Command<IPlayer, long, int>
    {
        private readonly IAchievementResourceManager _achievementResourceManager;
        private readonly IPlayerRepository _playerRepository;

        private readonly DefferedCommand<CreateItemForUserCommand> CreateItem = new DefferedCommand<CreateItemForUserCommand>();
        public ProgressAchievementCommand(IAchievementResourceManager achievementResourceManager, IPlayerRepository playerRepository)
        {
            _achievementResourceManager = achievementResourceManager;
            _playerRepository = playerRepository;
        }

        #region Overrides of Command<IPlayer,IAchievement,IValue>

        /// <summary>
        /// 
        /// </summary>
        /// <param name="session">player session</param>
        /// <param name="iplayer">player</param>
        /// <param name="achievementId">achievement base id</param>
        /// <param name="progress">added progress</param>
        /// <returns></returns>
        public override async Task Execute(WebSession session, IPlayer iplayer, long achievementId, int progress)
        {
            var player = (Player) iplayer;
            var achievement = (Achievement) player.Achievements[achievementId];
            if (achievement == null)
            {
                achievement = new Achievement(new Common.DTO.Achievement {Id = achievementId, Value = 0});
                player.Achievements.Add(achievement);
            }

            var achievementResource = _achievementResourceManager.Get(achievementId);
            var nextStep = GetNextStep(achievementResource, achievement.Value);

            // if next step not exists then achievement is completed so cant be progressed
            if (nextStep == null)
            {
                return;
            }

            var rewards = new List<CreateItemForUserCommand.ItemTemplate>();
            while (progress > 0 && nextStep != null)
            {
                var valueToProgress = Math.Min(progress, nextStep.Threshold - achievement.Value);
                progress -= valueToProgress;

                achievement.Value += valueToProgress;


                // if threshold is reached
                if (achievement.Value == nextStep.Threshold)
                {
                    var reward = new CreateItemForUserCommand.ItemTemplate
                        (
                            baseId: nextStep.RewardId,
                            level: 1,
                            quantity: nextStep.Quantity
                        );
                    rewards.Add(reward);
                    nextStep = GetNextStep(achievementResource, achievement.Value);
                }
            }
            // write progress to db
            player.Data.AchievementsData = player.Achievements.Data;
            await _playerRepository.Update(player.Data);
            // send to notification to client
            session.Send(new AchievementProgressEvent {NewData = achievement.DTO});

            if (rewards.Count == 0) return;
            // grant rewards
            var rewardItems = new List<Common.DTO.Item>();
            foreach (var template in rewards)
            {
                await CreateItem.Command.Execute(session, player, template, item => rewardItems.Add(item.DTO));
            }
            session.Send(new AchievementRewardEvent
            {
                AchievementId = achievementId,
                Reward = rewardItems
            });
        }


        private static Common.Resources.AchievementStep GetNextStep(Common.Resources.Achievement achievement, int currentValue)
        {
            return achievement.Steps.OrderBy(s => s.Threshold)
                    .FirstOrDefault(s => s.Threshold > currentValue);
        }
        #endregion
    }
}
