﻿using System.Threading.Tasks;
using LobbyServer.Models;

namespace LobbyServer.BL
{
    internal class LevelupCharacterCommand : Command<ICharacter>
    {
        private readonly DefferedCommand<SetCharacterApCommand> SetAp = new DefferedCommand<SetCharacterApCommand>();
        private readonly DefferedCommand<SetCharacterLevelCommand> SetLevel = new DefferedCommand<SetCharacterLevelCommand>();
        private readonly DefferedCommand<SetCharacterRequiredExperienceForLevelCommand> SetCharacterRequiredExperience = new DefferedCommand<SetCharacterRequiredExperienceForLevelCommand>();
        private readonly DefferedCommand<LevelupOGZNotifyCommand> NotifyOGZ = new DefferedCommand<LevelupOGZNotifyCommand>();
        public override async Task Execute(WebSession session, ICharacter character)
        {
            await SetLevel.Command.Execute(session, character, character.Level + 1);
            await SetAp.Command.Execute(session, character, character.AP + 1);
            await SetCharacterRequiredExperience.Command.Execute(session, character, character.Level);
        }
    }
}
