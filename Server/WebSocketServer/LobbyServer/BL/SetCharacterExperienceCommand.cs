﻿using System.Threading.Tasks;
using LobbyServer.Data;
using LobbyServer.Models;
using Character = LobbyServer.Models.Character;

namespace LobbyServer.BL
{
    internal class SetCharacterExperienceCommand : Command<ICharacter, long>
    {
        private readonly ICharacterRepository _repository;

        public SetCharacterExperienceCommand(ICharacterRepository repository)
        {
            _repository = repository;
        }

        public override async Task Execute(WebSession session, ICharacter icharacter, long experience)
        {
            var character = (Character) icharacter;
            character.Experience = experience;
            await _repository.Update(character.Data);

            session.Send(new ClientProtocol.ExperienceChangedEvent
            {
                CharacterId = character.Id,
                Experience = character.Experience,
            });
        }
    }
}
