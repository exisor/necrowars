﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Common.DTO;
using Common.ResourceManager;
using Common.Resources;
using LobbyServer.Models;
using LobbyServer.Services;

namespace LobbyServer.BL
{
    internal class CreateItemForUserCommand : Command<IPlayer, CreateItemForUserCommand.ItemTemplate, Action<IItem>>
    {
        internal struct ItemTemplate
        {
            public readonly long BaseId;
            public readonly int Level;
            public readonly Rarity? Rarity;
            public readonly int Quantity;

            public ItemTemplate(long baseId, int level, int quantity, Rarity? rarity = null)
            {
                BaseId = baseId;
                Level = level;
                Quantity = quantity;
                Rarity = rarity;
            }
        }


        private readonly IItemResourceManager _itemResourceManager;
        private readonly ItemGenerator _itemGenerator;
        private readonly DefferedCommand<UseInstantItemCommand> UseItem = new DefferedCommand<UseInstantItemCommand>();
        private readonly DefferedCommand<AddItemsForPlayerCommand> AddItems = new DefferedCommand<AddItemsForPlayerCommand>();
        public CreateItemForUserCommand(IItemResourceManager itemResourceManager, ItemGenerator itemGenerator)
        {
            _itemResourceManager = itemResourceManager;
            _itemGenerator = itemGenerator;
        }

        #region Overrides of Command<IPlayer,ItemTemplate>

        public override async Task Execute(WebSession session, IPlayer iplayer, ItemTemplate itemTemplate, Action<IItem> callback)
        {
            var itemBase = _itemResourceManager.Get(itemTemplate.BaseId);
            var item = _itemGenerator.CreateItem(itemTemplate.Level, itemBase, itemTemplate.Rarity);
            var quantity = itemTemplate.Quantity;
            item.Quantity = quantity;
            if (itemBase.Types.Contains(ItemType.Instant))
            {
                if (itemBase.Types.Contains(ItemType.OneTime))
                {
                    await AddItems.Command.Execute(session, iplayer.Inventory, new List<IItem> { item });
                }
                await UseItem.Command.Execute(session, iplayer, null, item);
            }
            else
            {
                await AddItems.Command.Execute(session, iplayer.Inventory, new List<IItem> {item});
            }
            item.Quantity = quantity;
            callback?.Invoke(item);
        }

        #endregion
    }
}
