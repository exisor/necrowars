﻿
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Common;
using LobbyServer.Data;
using LobbyServer.Models;
using Character = Common.Resources.Character;
using AbilitySlot = Common.DTO.AbilitySlot;

namespace LobbyServer.BL
{
    internal class CreateCharacterCommand : Command<IPlayer, Common.Resources.Character>
    {
        private readonly ICharacterRepository _repository;

        public CreateCharacterCommand(ICharacterRepository repository)
        {
            _repository = repository;
        }

        public override async Task Execute(WebSession session, IPlayer player, Character characterResource)
        {
            var characterData = new Data.Character
            {
                UserId = player.Id,
                Ap = 0,
                BaseId = characterResource.Id,
                Experience = 0,
                Level = 1,
                AbilityData = ProtobufSerializer.SerializeBinary(
                characterResource.AvailableAbilities.Select(abilityId =>
                {
                    var retval = new Common.DTO.Ability
                    {
                        Id = abilityId,
                        Level = abilityId == 1 ? 1 : 0,
                    };

                    return retval;
                }).ToList()),
                SelectedAbilities = ProtobufSerializer.SerializeBinary(new List<AbilitySlot>
                {
                    AbilitySlot.Empty(0),AbilitySlot.Empty(1),AbilitySlot.Empty(2)
                })
            };
            await _repository.Insert(characterData);
            var character = new Models.Character(characterData);
            player.Characters.Add(character);

            session.Send(new ClientProtocol.NewCharacterEvent
            {
                Character = character.DTO
            });
        }
    }
}
