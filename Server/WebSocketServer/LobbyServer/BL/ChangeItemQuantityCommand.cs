﻿
using System.Threading.Tasks;
using ClientProtocol;
using LobbyServer.Data;
using LobbyServer.Models;
using Item = LobbyServer.Models.Item;

namespace LobbyServer.BL
{
    internal class ChangeItemQuantityCommand : Command<IItem, int>
    {
        private readonly IItemRepository _itemRepository;

        public ChangeItemQuantityCommand(IItemRepository itemRepository)
        {
            _itemRepository = itemRepository;
        }

        public override async Task Execute(WebSession session, IItem iitem, int value)
        {
            var item = (Item) iitem;
            item.Quantity = value;
            await _itemRepository.Update(item.Data);

            session.Send(new ItemChangedEvent
            {
                Item = item.DTO
            });
        }
    }
}
