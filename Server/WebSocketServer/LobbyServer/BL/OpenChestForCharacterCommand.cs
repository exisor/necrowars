﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ClientProtocol;
using Common.ResourceManager;
using LobbyServer.Models;
using LobbyServer.Services;
using IItem = LobbyServer.Models.IItem;

namespace LobbyServer.BL
{
    internal class OpenChestForCharacterCommand : Command<IPlayer, ICharacter, IChest>
    {
        private readonly IChestResourceManager _chestResourceManager;
        private readonly DefferedCommand<CreateItemForUserCommand> _createItem = new DefferedCommand<CreateItemForUserCommand>();
        private readonly DefferedCommand<ProgressAchievementCommand> Achievement = new DefferedCommand<ProgressAchievementCommand>();
#if DAILY_QUESTS
        private readonly DefferedCommand<ProgressDailyQuestCommand> DailyQuest = new DefferedCommand<ProgressDailyQuestCommand>();
#endif
        private const long CHEST_ACHIEVEMENT_ID = 5;

        private const long DAILY_CHEST_QUEST_ID = 2;
        public OpenChestForCharacterCommand(IChestResourceManager chestResourceManager)
        {
            _chestResourceManager = chestResourceManager;
        }

        public override async Task Execute(WebSession session, IPlayer iplayer, ICharacter icharacter, IChest ichest)
        {
            var chestBase = _chestResourceManager.Get(ichest.BaseId);
            var totalItems = new List<IItem>();
            for (var i = 0; i < chestBase.NumberOfItems; ++i)
            {
                var roll = chestBase.ChestItems.OrderByDescending(p => p.Weight * RandomService.Instance.NextDouble()).First();
                var itemTemplate = new CreateItemForUserCommand.ItemTemplate
                    (
                        baseId: roll.ItemId,
                        level: icharacter.Level,
                        quantity:RandomService.Instance.Next(roll.QuantityMin, roll.QuantityMax),
                        rarity: roll.Rarity
                    );
                await _createItem.Command.Execute(session, iplayer, itemTemplate, item => totalItems.Add(item));
            }
            session.Send(new ChestOpenEvent
            {
                ItemsReceived = totalItems.Select(i => i.DTO).ToList(),
                BaseId = chestBase.Id,
            });
#if DAILY_QUESTS
            // dailyQuest
            if (iplayer.QuestId == DAILY_CHEST_QUEST_ID)
            {
                await DailyQuest.Command.Execute(session, 1);
            }
#endif
            // achievement

            var chestAchievement = iplayer.Achievements[CHEST_ACHIEVEMENT_ID];
            // Common chest
            if ((chestAchievement == null || chestAchievement.Value == 0) && chestBase.Id >= 1)
            {
                await Achievement.Command.Execute(session, iplayer, CHEST_ACHIEVEMENT_ID, 1);
                chestAchievement = iplayer.Achievements[CHEST_ACHIEVEMENT_ID];
            }
            // Rare chest
            if (chestAchievement != null && chestAchievement.Value == 1 && chestBase.Id >= 2)
            {
                await Achievement.Command.Execute(session, iplayer, CHEST_ACHIEVEMENT_ID, 1);
            }
            // Epic chest
            if (chestAchievement != null && chestAchievement.Value == 2 && chestBase.Id >= 3)
            {
                await Achievement.Command.Execute(session, iplayer, CHEST_ACHIEVEMENT_ID, 1);
            }
            // Legendary chest
            if (chestAchievement != null && chestAchievement.Value == 3 && chestBase.Id >= 4)
            {
                await Achievement.Command.Execute(session, iplayer, CHEST_ACHIEVEMENT_ID, 1);
            }
        }
    }
}
