﻿

using System.Threading.Tasks;
using Common;
using LobbyServer.Data;
using LobbyServer.Models;
using Character = LobbyServer.Models.Character;

namespace LobbyServer.BL
{
    internal class RemoveSkillFromCharacterCommand : Command<ICharacter, IAbility>
    {
        private readonly ICharacterRepository _characterRepository;
        private readonly DefferedCommand<SelectCharacterAbilityCommand> SelectAbility = new DefferedCommand<SelectCharacterAbilityCommand>();
        public RemoveSkillFromCharacterCommand(ICharacterRepository characterRepository)
        {
            _characterRepository = characterRepository;
        }

        public override async Task Execute(WebSession session, ICharacter icharacter, IAbility iability)
        {
            var character = (Character) icharacter;

            if (iability.SelectedInSlot != null)
            {
                await SelectAbility.Command.Execute(session, character, iability.SelectedInSlot, null);
            }

            character.Abilities.Remove(iability);
            character.Data.AbilityData = ProtobufSerializer.SerializeBinary(character.Abilities.DTO);

            await _characterRepository.Update(character.Data);

            session.Send(new ClientProtocol.AbilityLostEvent
            {
                Ability = iability.DTO
            });
        }
    }
}
