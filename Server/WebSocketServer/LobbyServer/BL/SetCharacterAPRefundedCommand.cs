﻿
using System.Threading.Tasks;
using LobbyServer.Data;
using LobbyServer.Models;
using Character = LobbyServer.Models.Character;

namespace LobbyServer.BL
{
    internal class SetCharacterAPRefundedCommand : Command<ICharacter, int>
    {
        private readonly ICharacterRepository _repository;

        public SetCharacterAPRefundedCommand(ICharacterRepository repository)
        {
            _repository = repository;
        }

        public override async Task Execute(WebSession session, ICharacter icharacter, int apRefunded)
        {
            var character = (Character) icharacter;
            character.APRefunded = apRefunded;
            await _repository.Update(character.Data);

            session.Send(new ClientProtocol.APRefundedChangedEvent
            {
                CharacterId = character.Id,
                NewAPRefunded = character.APRefunded,
            });
        }
    }
}
