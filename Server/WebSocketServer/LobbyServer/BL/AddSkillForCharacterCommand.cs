﻿using System.Threading.Tasks;
using Common;
using LobbyServer.Data;
using LobbyServer.Models;
using Character = LobbyServer.Models.Character;

namespace LobbyServer.BL
{
    internal class AddSkillForCharacterCommand : Command<ICharacter,IAbility>
    {
        private readonly ICharacterRepository _characterRepository;

        public AddSkillForCharacterCommand(ICharacterRepository characterRepository)
        {
            _characterRepository = characterRepository;
        }

        public override async Task Execute(WebSession session, ICharacter icharacter, IAbility ability)
        {
            var character = (Character) icharacter;
            character.Abilities.Add(ability);
            character.Data.AbilityData = ProtobufSerializer.SerializeBinary(character.Abilities.DTO);

            await _characterRepository.Update(character.Data);

            session.Send(new ClientProtocol.NewAbilityEvent
            {
                Ability = ability.DTO
            });
        }
    }
}
