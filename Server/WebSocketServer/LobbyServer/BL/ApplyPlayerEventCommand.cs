﻿using System.Collections.Generic;
using System.Threading.Tasks;
using ClientProtocol;
using Common;
using LobbyServer.Data;
using LobbyServer.Models;
using Item = Common.DTO.Item;

namespace LobbyServer.BL
{
    internal class ApplyPlayerEventCommand : Command<IPlayer, PlayerEvent>
    {
        public override async Task Execute(WebSession session, IPlayer player, PlayerEvent ev)
        {
            var list = ProtobufSerializer.Deserialize<List<Item>>(ev.Items);
            session.Send(new NewPlayerEventEvent
            {
                EventId = ev.MessageId,
                Items = list,
            });
        }
    }
}
