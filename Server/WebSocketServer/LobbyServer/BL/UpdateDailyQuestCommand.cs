﻿#if DAILY_QUESTS

using System;
using System.Linq;
using System.Threading.Tasks;
using ClientProtocol;
using Common.ResourceManager;
using LobbyServer.Data;
using LobbyServer.Services;
using Player = LobbyServer.Models.Player;

namespace LobbyServer.BL
{
    internal class UpdateDailyQuestCommand : Command
    {
        private readonly IDailyQuestResourceManager _dailyQuestResourceManager;
        private readonly IPlayerRepository _playerRepository; 
        public UpdateDailyQuestCommand(IDailyQuestResourceManager dailyQuestResourceManager, IPlayerRepository playerRepository)
        {
            _dailyQuestResourceManager = dailyQuestResourceManager;
            _playerRepository = playerRepository;
        }

#region Overrides of Command

        public override async Task Execute(WebSession session)
        {
            var player = (Player) session.Player;

            var time = DateTime.Now;
            var rnd = new Random((int) (time.Ticks + player.Id));

            var quest =  _dailyQuestResourceManager.All.OrderBy(_ => rnd.NextDouble()).FirstOrDefault();

            player.QuestId = quest?.Id ?? 0;
            player.Data.LastUpdate = time;
            player.Data.QuestProgress = 0;

            await _playerRepository.Update(player.Data);

            session.Send(new NewQuestEvent
            {
                BaseId = player.QuestId,
                LifeTimeSeconds = (int) PlayersService.DailyQuestLifeSpan.TotalSeconds
            });
        }

#endregion
    }
}
#endif