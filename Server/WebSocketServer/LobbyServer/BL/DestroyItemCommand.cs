﻿using System.Threading.Tasks;
using LobbyServer.Data;
using LobbyServer.Models;
using Item = LobbyServer.Models.Item;

namespace LobbyServer.BL
{
    internal class DestroyItemCommand : Command<Inventory, IItem>
    {
        private readonly IItemRepository _itemRepository;

        public DestroyItemCommand(IItemRepository itemRepository)
        {
            _itemRepository = itemRepository;
        }

        public override async Task Execute(WebSession session, Inventory inventory, IItem item)
        {
            inventory.Remove(item.Id);

            await _itemRepository.Delete(((Item) item).Data);

            session.Send(new ClientProtocol.ItemLostEvent
            {
                Id = item.Id
            });
        }
    }
}
