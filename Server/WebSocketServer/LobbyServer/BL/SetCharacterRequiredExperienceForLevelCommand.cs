﻿using System.Threading.Tasks;
using LobbyServer.Data;
using LobbyServer.Models;
using Character = LobbyServer.Models.Character;

namespace LobbyServer.BL
{
    internal class SetCharacterRequiredExperienceForLevelCommand : Command<ICharacter, int>
    {
        public override async Task Execute(WebSession session, ICharacter icharacter, int level)
        {
            var character = (Character) icharacter;
            character.RequiredExperience = Common.Util.GetRequiredExperienceForLevel(level);

            //session.Send(new ClientProtocol.RequiredExperienceChangedEvent
            //{
            //    CharacterId = character.Id, 
            //    RequiredExperience = character.RequiredExperience
            //});
        }

        
    }
}
