﻿using System.Threading.Tasks;
using LobbyServer.Data;
using LobbyServer.Models;
using Player = LobbyServer.Models.Player;

namespace LobbyServer.BL
{
    internal class SetPlayerGoldCommand : Command<IPlayer, long>
    {
        private readonly IPlayerRepository _repository;

        public SetPlayerGoldCommand(IPlayerRepository repository)
        {
            _repository = repository;
        }

        public override async Task Execute(WebSession session, IPlayer iplayer, long gold)
        {
            var player = (Player) iplayer;
            player.Gold = gold;

            await _repository.Update(player.Data);
            session.Send(new ClientProtocol.GoldChangedEvent
            {
                NewGold = gold
            });
        }
    }
}
