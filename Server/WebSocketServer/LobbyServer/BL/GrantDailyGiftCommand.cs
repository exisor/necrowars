﻿using System;
using System.Linq;
using System.Threading.Tasks;
using ClientProtocol;
using Common.ResourceManager;
using LobbyServer.Data;
using LobbyServer.Models;
using Player = LobbyServer.Models.Player;

namespace LobbyServer.BL
{
    internal class GrantDailyGiftCommand : Command<IPlayer>
    {
        private readonly IDailyRewardResourceManager _dailyRewardResourceManager;
        private readonly IPlayerRepository _playerRepository;
        private readonly DefferedCommand<CreateItemForUserCommand> CreateItem = new DefferedCommand<CreateItemForUserCommand>();

        public GrantDailyGiftCommand(IDailyRewardResourceManager dailyRewardResourceManager, IPlayerRepository playerRepository)
        {
            _dailyRewardResourceManager = dailyRewardResourceManager;
            _playerRepository = playerRepository;
        }

        public override async Task Execute(WebSession session, IPlayer iplayer)
        {
            var player = (Player) iplayer;
            if (player == null) throw new ArgumentNullException(nameof(player));

            var level = 1;
            var character = player.Characters.FirstOrDefault();
            if (character != null)
            {
                level = character.Level;
            }
            player.Data.DailyRound = player.Data.DailyRound % 7 + 1;
            await _playerRepository.Update(player.Data);

            var reward = _dailyRewardResourceManager.Get(player.Data.DailyRound);

            var template = new CreateItemForUserCommand.ItemTemplate
            (
                baseId: reward.ItemId,
                level: level,
                quantity: reward.Quantity
            );

            session.Send(new DailyRewardsEvent
            {
                Current = player.Data.DailyRound,
                Items = _dailyRewardResourceManager.All.Select(r => r.ItemId).ToList()
            });
            await CreateItem.Command.Execute(session, player, template, null);

        }
    }
}
