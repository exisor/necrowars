﻿#if DAILY_QUESTS
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ClientProtocol;
using Common.ResourceManager;
using LobbyServer.Data;
using Item = Common.DTO.Item;
using Player = LobbyServer.Models.Player;

namespace LobbyServer.BL
{
    internal class ProgressDailyQuestCommand : Command<int>
    {
        private readonly IPlayerRepository _playerRepository;
        private readonly IDailyQuestResourceManager _dailyQuestResourceManager;
        private readonly DefferedCommand<CreateItemForUserCommand> CreateItem = new DefferedCommand<CreateItemForUserCommand>();

        public ProgressDailyQuestCommand(IPlayerRepository playerRepository, IDailyQuestResourceManager dailyQuestResourceManager)
        {
            _playerRepository = playerRepository;
            _dailyQuestResourceManager = dailyQuestResourceManager;
        }

#region Overrides of Command<int>
        /// <summary>
        /// 
        /// </summary>
        /// <param name="session"></param>
        /// <param name="addedProgress">ADDED progress</param>
        /// <returns></returns>
        public override async Task Execute(WebSession session, int addedProgress)
        {
            var player = (Player)session.Player;
            if (player.QuestId == 0) return;
            var quest = _dailyQuestResourceManager.Get(player.QuestId);
            if (quest.Threshold <= player.Data.QuestProgress) return;

            player.Data.QuestProgress = Math.Min(player.Data.QuestProgress + addedProgress, quest.Threshold);
            session.Send(new QuestProgressEvent {Progress = player.Data.QuestProgress});
            if (player.Data.QuestProgress == quest.Threshold)
            {
                // reward
                var template = new CreateItemForUserCommand.ItemTemplate
                (
                    baseId: quest.RewardId,
                    level: player.Characters.First().Level,
                    quantity: quest.Quantity
                );

                await CreateItem.Command.Execute(session, player, template, item => session.Send(new QuestCompleteEvent
                {
                    Reward = new List<Item> { item.DTO }
                }));
            }

            await _playerRepository.Update(player.Data);

        }

#endregion
    }
}
#endif