﻿using System.Threading.Tasks;
using ClientProtocol;
using LobbyServer.Data;
using LobbyServer.Models;
using Item = LobbyServer.Models.Item;

namespace LobbyServer.BL
{
    internal class SetItemLevelCommand : Command<IItem, int>
    {
        private readonly IItemRepository _itemRepository;

        public SetItemLevelCommand(IItemRepository itemRepository)
        {
            _itemRepository = itemRepository;
        }

        public override async Task Execute(WebSession session, IItem iitem, int level)
        {
            var item = (Item) iitem;
            item.ItemLevel = level;
            await _itemRepository.Update(item.Data);
            session.Send(new ItemChangedEvent
            {
                Item = item.DTO,
            });
        }
    }
}
