﻿using System;
using SuperSocket.SocketBase;
using SuperSocket.SocketBase.Protocol;

namespace LobbyServer
{
    public class Session : AppSession<Session>
    {
        public Server Server => (Server) AppServer;

        public event Action<Session> Disconnected;

        protected override void OnSessionClosed(CloseReason reason)
        {
            Disconnected?.Invoke(this);
            base.OnSessionClosed(reason);
        }

        protected override void HandleUnknownRequest(StringRequestInfo requestInfo)
        {
            Logger.Info($"Unknown request : {requestInfo.Key} : {requestInfo.Body}");
        }
    }
}
