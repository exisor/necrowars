﻿using System.Configuration;
using System.Net;
using Autofac;
using Common.ResourceManager;
using DAL;
using LobbyServer.BL;
using LobbyServer.BL.InstantItems;
using LobbyServer.Data;
using LobbyServer.Services;
using MasterServer.REST;
using SuperSocket.SocketBase;
using SuperSocket.SocketBase.Logging;
using SuperSocket.WebSocket;

namespace LobbyServer
{
    public class WebServer : WebSocketServer<WebSession>
    {

        public static ILog Log;
        private static IContainer _context;
        public Server S2S { get; private set; }

        public static IContainer Context
        {
            get { return _context ?? (_context = CreateContext()); }
            set { _context = value; }
        }

        private static IContainer CreateContext()
        {
            var container = new ContainerBuilder();

            container.RegisterType<GameDatabaseProvider>().AsImplementedInterfaces().SingleInstance();
            container.RegisterType<PlayerRepository>().AsImplementedInterfaces().SingleInstance();
            container.RegisterType<CharacterRepository>().AsImplementedInterfaces().SingleInstance();
            container.RegisterType<BattleManager>().AsSelf().SingleInstance();
            container.RegisterType<ItemRepository>().AsImplementedInterfaces().SingleInstance();
            container.RegisterType<ShopService>().AsSelf().SingleInstance();
            container.RegisterType<ItemGenerator>().AsSelf().SingleInstance();
            container.RegisterType<EffectRepository>().AsImplementedInterfaces().SingleInstance();
            container.RegisterType<PlayersService>().AsSelf().SingleInstance();
            container.RegisterType<LeaderboardsRepository>().AsImplementedInterfaces().SingleInstance();
            container.RegisterType<Leaderboard>().AsSelf().SingleInstance();
            container.RegisterType<PlayerEventsRepository>().AsImplementedInterfaces().SingleInstance();
            container.RegisterAssemblyTypes(typeof(IItemHandler).Assembly)
                .AssignableTo<IItemHandler>()
                .Where(t => !t.IsAbstract && !t.IsInterface)
                .As<IItemHandler>()
                .AsSelf()
                .SingleInstance();
            container.RegisterType<ItemHandlerCollection>().AsImplementedInterfaces().SingleInstance();
            container.RegisterAssemblyTypes(typeof (IResourceManager).Assembly)
                .AssignableTo<IResourceManager>()
                .AsImplementedInterfaces()
                .SingleInstance();
            container.RegisterAssemblyTypes(typeof (WebServer).Assembly)
                .AssignableTo<ICommonCommand>()
                .AsSelf()
                .SingleInstance();
            container.RegisterAssemblyTypes(typeof(WebServer).Assembly)
                .AssignableTo<IRequestHandler>()
                .AsSelf()
                .AsImplementedInterfaces()
                .SingleInstance();
            container.RegisterType<RESTServer>().AsSelf().SingleInstance();
            container.RegisterType<HttpsListener>().AsImplementedInterfaces();
            container.RegisterType<RequestHandlersCollection>().AsImplementedInterfaces().SingleInstance();
            return container.Build();
        }

        private RESTServer _rest;
        protected override void OnStarted()
        {
            Log = Logger;
            S2S = (Server)Bootstrap.GetServerByName("Lobby Server (Tcp)");
#if !DEBUG
            _rest = Context.Resolve<RESTServer>();
            _rest.Start(1337);
#endif
            base.OnStarted();
        }
    }
}
