﻿using System;
using System.Net;
using System.Threading;
using System.Threading.Tasks;
using LobbyServer;
using SuperSocket.SocketBase.Logging;

namespace MasterServer.REST
{
    public sealed class RESTServer : IDisposable
    {
        private readonly IListener _listener;
        private bool _disposed;
        public static ILog Log;
        public RESTServer(IListener listener)
        {
            Log = WebServer.Log;
            _listener = listener;
        }

        public void Start(int port)
        {
            _listener.Listen(port);
        }

        public void Stop()
        {
            Dispose();
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        private void Dispose(bool disposing)
        {
            if (_disposed) return;

            _listener.Dispose();
            _disposed = true;
        }

        ~RESTServer()
        {
            Dispose(false);
        }
    }
}
