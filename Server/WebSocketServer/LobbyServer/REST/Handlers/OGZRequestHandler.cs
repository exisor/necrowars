﻿using System.Threading.Tasks;
using Common.ResourceManager;
using LobbyServer.BL;
using LobbyServer.Services;
using MasterServer.REST;

namespace LobbyServer.REST.Handlers
{
    internal class OGZRequestHandler : RequestHandler<OGZRequest>
    {
        private readonly PlayersService _players;
        private readonly IKongregateShopResourceManager _kongregateShopResourceManager;
        private DefferedCommand<CreateItemForUserCommand> CreateItem = new DefferedCommand<CreateItemForUserCommand>();
        private readonly DefferedCommand<UpdateCurrencyBoughtCommand> CurrencyBought = new DefferedCommand<UpdateCurrencyBoughtCommand>();
        private const int ERROR_CODE_OTHER = 500;
        public OGZRequestHandler(PlayersService players, IKongregateShopResourceManager kongregateShopResourceManager)
        {
            _players = players;
            _kongregateShopResourceManager = kongregateShopResourceManager;
        }
        public override RestRequestMethod Method => RestRequestMethod.POST;
        public override string Name => "OGZCallback";
        protected override async Task HandleInternal(OGZRequest request, Response response)
        {
            var uid = request.Param<long>("uid");
            var itemId = request.Param<long>("item");
            var playerSession = _players.GetSessionByOGZId(uid);
            if (playerSession == null)
            {
                // fail
                await Fail(response, ERROR_CODE_OTHER, "Player is not logged in");
                return;
            }
            var shopItem = _kongregateShopResourceManager.Get(itemId);
            if (shopItem == null)
            {
                await Fail(response, ERROR_CODE_OTHER, "Item is not available");
                return;
            }
            var template = new CreateItemForUserCommand.ItemTemplate(
                    baseId: shopItem.ItemBaseId,
                    level: 1,
                    quantity: shopItem.Quantity
                    );
            await CreateItem.Command.Execute(playerSession, playerSession.Player, template, null);
            await response.Send("{\"success\":true}");
            RESTServer.Log.Info($"Successfully created item from player id : [{playerSession.Player.Id}], itemid : [{template.BaseId}], quantity : [{template.Quantity}]");
            await CurrencyBought.Command.Execute(playerSession, shopItem.Quantity);
        }

        private Task Fail(Response response, int code, string error)
        {
            RESTServer.Log.Error($"Code : {code}, message : {error}");
            return response.Send($"{{\"success\":false,\"code\":{code},\"message\":{error},\"critical\":true}}");
        }
    }
}
