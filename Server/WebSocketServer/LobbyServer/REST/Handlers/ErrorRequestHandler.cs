﻿using System;
using System.Threading.Tasks;
using SuperSocket.SocketBase;

namespace MasterServer.REST.Handlers
{
    public class ErrorRequestHandler : RequestHandler<ErrorRequest>
    {

        public override RestRequestMethod Method => RestRequestMethod.ERROR;
        public override string Name => "Error";
        protected override Task HandleInternal(ErrorRequest request, Response response)
        {
            return response.Send(""
                
                //$"<HTML><BODY><PRE>{request.Data.ToString().Replace(Environment.NewLine, "<BR>")}</PRE></BODY></HTML>"
                );
        }
    }
}
