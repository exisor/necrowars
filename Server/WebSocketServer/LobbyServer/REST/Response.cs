﻿using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace MasterServer.REST
{
    public class Response
    {
        private readonly HttpListenerResponse _baseResponse;
        public Response(HttpListenerResponse baseResponse)
        {
            _baseResponse = baseResponse;
        }

        public Task Send(string data)
        {
            _baseResponse.StatusCode = 200;
            var responseBytes =
                Encoding.UTF8.GetBytes(data);
            _baseResponse.ContentLength64 = responseBytes.Length;
            using (var stream = _baseResponse.OutputStream)
            {
                return stream.WriteAsync(responseBytes, 0, responseBytes.Length);
            }
        }
    }
}