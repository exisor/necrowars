﻿using System.Threading.Tasks;

namespace MasterServer.REST
{
    public abstract class RequestHandler<T> : IRequestHandler where T : Request
    {

        public abstract RestRequestMethod Method { get; }
        public abstract string Name { get; }

        public Task Handle(Request request, Response response)
        {
            var req = request as T;
            return req == null ? Task.CompletedTask : HandleInternal(req, response);
        }

        protected abstract Task HandleInternal(T request, Response response);
    }
}