﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Net;
using System.Security.Cryptography;
using System.Text;
using Newtonsoft.Json.Linq;

namespace MasterServer.REST
{
    public abstract class Request
    {
        protected Request()
        {
            Method = RestRequestMethod.POST;
        }
        public static Request FromHttp(HttpListenerRequest request)
        {
            Request ret = null;
            switch (request.HttpMethod)
            {
                case "POST":
                    break;
                default:
                    return Error($"Bad request : {Environment.NewLine}. Method : {request.HttpMethod} is not supported");
            }
            using (var stream = request.InputStream)
            using (var reader = new StreamReader(stream, request.ContentEncoding))
            {
                var encodedData = reader.ReadToEnd();
                try
                {
                    if (false)
#pragma warning disable 162
                    {
                        ret = new KongregateRequest(encodedData, request.RawUrl.Split(new[] { "rest/" }, StringSplitOptions.RemoveEmptyEntries).LastOrDefault());
                    }
                    else
                    {
                        ret = new OGZRequest(encodedData);
                    }
#pragma warning restore 162
                }
                catch (Exception e)
                {
                    RESTServer.Log.Error(e);
                    ret = Error(e.Message);
                }
                return ret;
                
            }
        }

        private static Request Error(string errorData)
        {
            return new ErrorRequest(new JObject {{"error", errorData}});
                
        }

        protected static byte[] FromBase64URL(string data)
        {
            var replaced = data.Replace('_', '/').Replace('-', '+');
            switch (data.Length % 4)
            {
                case 2:
                    replaced += "==";
                    break;
                case 3:
                    replaced += "=";
                    break;
            }
            return Convert.FromBase64String(replaced);
        }

        protected static bool ByteArrayEquals(byte[] left, byte[] right)
        {
            if (left.Length != right.Length)
            {
                return false;
            }

            for (var i = 0; i < left.Length; ++i)
            {
                if (left[i] == right[i]) return false;
            }
            return true;
        }

        public string Name { get; protected set; }
        public RestRequestMethod Method { get; protected set; }
    }

    public class ErrorRequest : Request
    {
        public ErrorRequest(JObject data)
        {
            Data = data;
            Name = "Error";
            Method = RestRequestMethod.ERROR;
        }
        public JObject Data { get; protected set; }
    }
    public class KongregateRequest : Request
    {
        private const string API_KEY = "9d984ea4-f48c-4b7a-86c3-2d170361c6b2";
        public KongregateRequest(string data, string methodname)
        {
            if (!data.Contains("signed_request="))
            {
                throw new Exception("Bad request");
            }
            data = data.Replace("signed_request=", string.Empty);
            var split = data.Split('.');
            if (split.Length != 2)
            {
                throw new Exception("Bad request");
            }
            try
            {
                var sig = FromBase64URL(split[0]);
                Data = JObject.Parse(Encoding.UTF8.GetString(FromBase64URL(split[1])));

                var algorithm = (string)Data["algorithm"];
                if (algorithm.ToUpper() != "HMAC-SHA256")
                {
                    throw new Exception($"Unknown algorithm : {algorithm}. Expected HMAC-SHA256");
                }

                using (var sha = new HMACSHA256(Encoding.UTF8.GetBytes(API_KEY)))
                {
                    var expectedSig = sha.ComputeHash(Encoding.UTF8.GetBytes(split[1]));
                    if (!ByteArrayEquals(expectedSig, sig))
                    {
                        throw new Exception("Signature mismatch!");
                    }
                }

                if (methodname == "event")
                {
                    methodname = Data["event"].ToString();
                }
                Name = methodname;
            }
            catch (Exception e)
            {
                RESTServer.Log.Error(e);
                throw new Exception(
                        $"Bad request : {data} {Environment.NewLine}Exception : {Environment.NewLine}{e.Message}{Environment.NewLine}{e.StackTrace}");
            }
        }
        public JObject Data { get; protected set; }
    }

    public class OGZRequest : Request
    {
        private static string DecodeUrlString(string url)
        {
            string newUrl;
            while ((newUrl = Uri.UnescapeDataString(url)) != url)
                url = newUrl;
            return newUrl;
        }
        private const string SECRET = "Wki74GO-HtIWlpEkEBlaUw";
        public OGZRequest(string data)
        {
            RESTServer.Log.Info($"Incoming data : {data}");
            try
            {
                var decode = DecodeUrlString(data);
                _params =
                        decode.Split(new[] { "&" }, StringSplitOptions.RemoveEmptyEntries)
                            .Select(s => s.Split(new[] { "=" }, StringSplitOptions.None))
                            .ToDictionary(s => s[0], s => s[1]);
                var hashString =
                    string.Join("",
                    _params.Where(kvp => kvp.Key != "sig")
                        .OrderBy(kvp => kvp.Key)
                        .Select(kvp => string.Join("=", kvp.Key, kvp.Value))) + SECRET;
                string hash;
                using (var md5 = MD5.Create())
                {
                    hash = BitConverter.ToString(md5.ComputeHash(Encoding.ASCII.GetBytes(hashString))).Replace("-", string.Empty).ToLower();
                }
                if (!string.Equals(hash, _params["sig"]))
                {
                    throw new Exception("Invalid signature");
                }
                Name = "OGZCallback";
            }
            catch (Exception e)
            {
                throw new Exception($"Bad request : {data} {Environment.NewLine}Exception : {Environment.NewLine}{e.Message}{Environment.NewLine}{e.StackTrace}");
            }
        }

        private readonly Dictionary<string, string> _params;

        public T Param<T>(string key)
        {
            
            try
            {
                return (T) Convert.ChangeType(_params[key], typeof(T));
            }
            catch (Exception)
            {
                return default(T);
            }
        }

    }
}
