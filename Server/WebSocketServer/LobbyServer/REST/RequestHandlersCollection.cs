﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace MasterServer.REST
{
    public class RequestHandlersCollection : IRequestHandlersCollection
    {
        private const string DEFAULT_REQUEST_NAME = "404";
        private class DefaultRequestHandler : RequestHandler<Request>
        {
            public override RestRequestMethod Method => RestRequestMethod.ERROR;
            public override string Name => DEFAULT_REQUEST_NAME;
            protected override Task HandleInternal(Request request, Response response)
            {
                return response.Send($"<HTML><BODY>Your request:<BR><PRE></PRE><BR>{request.Name} : {request.Method}</BODY></HTML>");
            }
        }
        private readonly Dictionary<string, IRequestHandler> _handlers;
        private readonly DefaultRequestHandler _default = new DefaultRequestHandler();
        public RequestHandlersCollection(IEnumerable<IRequestHandler> handlers)
        {
            _handlers = handlers.ToDictionary(key => key.Name, value => value);
        }

        private Task Handle(Request request, Response response)
        {
            RESTServer.Log.Info($"Received request : {request.Name}, {request.Method}");
            IRequestHandler handler;
            if (!_handlers.TryGetValue(request.Name, out handler))
            {
                handler = _default;
            }
            return handler.Handle(request, response);
        }

        public Task Handle(HttpListenerContext context)
        {
            return Handle(Request.FromHttp(context.Request), new Response(context.Response));
        }
    }
}