﻿using System;
using System.Collections.Generic;
using System.Linq;
using Common;
using Common.DTO;
using Common.ResourceManager;
using Item = LobbyServer.Models.Item;

namespace LobbyServer.Services
{
    internal class ItemGenerator
    {
        private readonly IAffixResourceManager _affixResourceManager;

        public ItemGenerator(IAffixResourceManager affixResourceManager)
        {
            _affixResourceManager = affixResourceManager;
        }

        public Item CreateItem(int itemlevel, Common.Resources.Item itemResource, Rarity? overrideRarity = null)
        {

            var closestLevel = itemResource.LevelSteps.Count == 0
                ? itemlevel
                : itemResource.LevelSteps.OrderByDescending(s => s).FirstOrDefault(e => e <= itemlevel);
            if (closestLevel == 0) closestLevel = itemResource.LevelSteps.OrderByDescending(s => s).Last();
            var affixes = new List<ItemAffix>();
            affixes.AddRange(
                itemResource.FixedAffixes.Select(
                    affixId => new ItemAffix {Enchant = 0, Id = affixId, Seed = GetSeed()}));
            
            var rarity = overrideRarity ?? (itemResource.Rarity == Rarity.Random ? GetRandomRarity() : itemResource.Rarity);

            affixes.AddRange(
                itemResource.AffixProbabilities
                    .Where(prob => _affixResourceManager.Get(prob.AffixId).MinimumItemLevel <= closestLevel)
                    .OrderByDescending(prob => prob.Weight*RandomService.Instance.Next())
                    .Take(itemResource.OverrideRarityAffixCount == -1 ? GetNumberOfAffixes(rarity) : itemResource.OverrideRarityAffixCount)
                    .Select(prob => new ItemAffix
                    {
                        Enchant = 0,
                        Id = prob.AffixId,
                        Seed = GetSeed(),
                    }));
            var item = new Data.Item
            {
                BaseId = itemResource.Id,
                Enchant = 0,
                Rarity = (short) rarity,
                CharacterId = null,
                Quantity = 1,
                Itemlevel = closestLevel,
                AffixData = ProtobufSerializer.SerializeBinary(affixes)
            };
            return new Item(item);
        }

        private int GetNumberOfAffixes(Rarity rarity)
        {
            switch (rarity)
            {
                case Rarity.Common:
                    return 1;
                case Rarity.Rare:
                    return 2;
                case Rarity.Epic:
                    return 3;
                case Rarity.Legendary:
                    return 4;
                case Rarity.Unique:
                    return 0;
                default:
                    throw new ArgumentOutOfRangeException(nameof(rarity), rarity, null);
            }
        }

        private int GetSeed()
        {
            return RandomService.Instance.Next(int.MinValue + 1, int.MaxValue);
        }

        private Rarity GetRandomRarity()
        {
            return (Rarity)RandomService.Instance.Next(0, 4);
        }
    }
}
