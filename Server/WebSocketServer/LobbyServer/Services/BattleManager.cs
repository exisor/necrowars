﻿using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using Common;
using Common.ResourceManager;
using Common.Resources;
using LobbyServer.Models;

namespace LobbyServer.Services
{
    internal class BattleManager
    {
        private Session _matchmaker;
        private readonly ConcurrentDictionary<long, WebSession> _usersWaiting = new ConcurrentDictionary<long, WebSession>();
        private readonly IItemResourceManager _itemResourceManager;

        public BattleManager(IItemResourceManager itemResourceManager)
        {
            _itemResourceManager = itemResourceManager;
        }

        internal bool Queue(WebSession session, ICharacter character)
        {
            if (_matchmaker == null) return false;
                
            session.Disconnected += SessionOnDisconnected;
            _usersWaiting[character.Id] = session;
            var specs = new List<BattleSpecEnum>
            {
                session.Target == ClientTarget.Unity ? BattleSpecEnum.UnityClient : BattleSpecEnum.WebGL
            };
            var msg = ProtobufSerializer.MakeStringMessage(new ServerProtocol.RequestGameServerForUser
            {
                SelectedCharacter = character.DTO,
                PlayerName = session.Player.Username,
                Items = character
                .EquippedItems
                .Select(i => i.DTO)
                .Concat(
                    session.Player.Inventory
                    .Where(i => _itemResourceManager.Get(i.BaseId).Types.Contains(ItemType.Battle))
                    .Select(i => i.DTO)
                    .GroupBy(key => key.BaseId& 0xffffffff + (key.ItemLevel << 32), element => element, (key, list) =>
                        list.Aggregate((retval, item) => retval + item)
                ))
                .ToList(),
                Specs = specs,
            });

            // notify matchmaker
            _matchmaker.Send(msg);
            return true;
        }

        private void SessionOnDisconnected(WebSession webSession)
        {
            Cancel(webSession.Player.Id);
        }

        public void Cancel(long characterId)
        {
            var session = Remove(characterId);
            if (session == null) return;

            // notify matchmaker
            _matchmaker?.Send(ProtobufSerializer.MakeStringMessage(new ServerProtocol.CancelRequestGameServerForUser
            {
                CharacterId = characterId
            }));
        }

        private WebSession Remove(long characterId)
        {
            WebSession retval;
            _usersWaiting.TryRemove(characterId, out retval);
            return retval;
        }

        public WebSession BattleFound(long characterId)
        {
            return Remove(characterId);
        }

        public void MatchmakerConnected(Session session)
        {
            _matchmaker = session;
        }

        public void MatchmakerDisconnected()
        {
            _matchmaker = null;
        }
    }
}
