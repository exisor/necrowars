﻿using System;
using System.Collections.Concurrent;
using System.Linq;
using System.Threading.Tasks;
using LobbyServer.BL;
using LobbyServer.Models;

namespace LobbyServer.Services
{
    internal class PlayersService
    {
#if DAILY_QUESTS
        public static readonly TimeSpan DailyQuestLifeSpan = TimeSpan.FromMinutes(1);
#endif
        private readonly ConcurrentDictionary<long, WebSession> _sessions = new ConcurrentDictionary<long, WebSession>();
        private readonly ConcurrentDictionary<long, WebSession> _OGZsessions = new ConcurrentDictionary<long, WebSession>();
        private readonly DefferedCommand<PlayerRemoveEffectCommand> PlayerRemoveEffect = new DefferedCommand<PlayerRemoveEffectCommand>();
#if DAILY_QUESTS
        private readonly DefferedCommand<UpdateDailyQuestCommand> UpdateDailyQuest = new DefferedCommand<UpdateDailyQuestCommand>();
#endif
        public PlayersService()
        {
            GlobalTimer.LowResTimer.Tick += LowResTimerOnTick;
        }

        private async void LowResTimerOnTick(DateTime dateTime)
        {
            GlobalTimer.LowResTimer.Tick -= LowResTimerOnTick;
            foreach (var webSession in _sessions)
            {
                var ws = webSession.Value;
                // effects
                if (ws.Player == null) continue;
                var effectsCollection = ws.Player.Effects;
                foreach (var effect in effectsCollection.Where(e => e.ExpirationTime < dateTime).ToList())
                {
                    await PlayerRemoveEffect.Command.Execute(ws, effectsCollection, effect);
                }
                // dailies
#if DAILY_QUESTS
                await CheckDaily(dateTime, ws);
#endif
            }

            GlobalTimer.LowResTimer.Tick += LowResTimerOnTick;
        }

        public void RegisterOnlinePlayer(WebSession session)
        {
            if (session.UserId == null) return;
            _sessions[session.UserId.Value] = session;
            if (session.Metadata.OGZId != 0)
            {
                _OGZsessions[session.Metadata.OGZId] = session;
            }
        }

        public void UnRegisterOnlinePlayer(WebSession session)
        {
            WebSession _;
            if (session.UserId == null) return;
            _sessions.TryRemove(session.UserId.Value, out _);
            if (session.Metadata.OGZId != 0)
            {
                _OGZsessions.TryRemove(session.Metadata.OGZId, out _);
            }
        }

#if DAILY_QUESTS
        public async Task<bool> CheckDaily(DateTime dateTime, WebSession session)
        {
            var player = (Player)session.Player;
            if (dateTime - player.Data.LastUpdate <= DailyQuestLifeSpan) return false;
            await UpdateDailyQuest.Command.Execute(session);
            return true;
        }
#endif
        public WebSession this[long id]
        {
            get
            {
                WebSession retval;
                _sessions.TryGetValue(id, out retval);
                return retval;
            }
        }

        public WebSession GetSessionByOGZId(long id)
        {
            WebSession ret;
            _OGZsessions.TryGetValue(id, out ret);
            return ret;
        }

        public WebSession GetSessionForCharacter(long characterId)
        {
            return _sessions.Values.FirstOrDefault(s => s.Player.Characters[characterId] != null);
        }
    }
}
