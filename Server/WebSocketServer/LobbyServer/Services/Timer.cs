﻿using System;
using System.Timers;

namespace LobbyServer.Services
{
    internal class Timer : ITimer
    {
        private readonly System.Timers.Timer _timer;
        public Timer(int resolution)
        {
            Resolution = resolution;
            _timer = new System.Timers.Timer(resolution) {AutoReset = true};
            _timer.Elapsed += TimerOnElapsed;
            _timer.Start();
        }

        private void TimerOnElapsed(object sender, ElapsedEventArgs elapsedEventArgs)
        {
            OnTick(elapsedEventArgs.SignalTime);
        }

        public int Resolution { get; }
        public event Action<DateTime> Tick;

        protected virtual void OnTick(DateTime dateTime)
        {
            Tick?.Invoke(dateTime);
        }

        public void Dispose()
        {
            _timer.Dispose();
        }
    }
}