﻿using System.Collections.Concurrent;
using ServerProtocol;

namespace LobbyServer.Services
{
    public class UserService
    {
        public class SessionInfo
        {
            public string sessonKey;
            public WebSession.SessionMetadata metadata = new WebSession.SessionMetadata();
        }
        private readonly ConcurrentDictionary<long, SessionInfo> _sessions = new ConcurrentDictionary<long, SessionInfo>();

        public void CreateSession(long userId, SessionInfo sessionInfo)
        {
            _sessions[userId] = sessionInfo;
        }

        public SessionInfo RequestSession(long userId, string sessionKey)
        {
            SessionInfo session;
            if (!_sessions.TryGetValue(userId, out session))
            {
                return null;
            }
            if (session.sessonKey.Equals(sessionKey))
            {
                return session;
            }
            return null;
        }

        public void DisposeSession(long userId)
        {
            SessionInfo _;
            _sessions.TryRemove(userId, out _);
        }
    }
}