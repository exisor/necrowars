﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using LobbyServer.BL;
using LobbyServer.Data;
using ServerProtocol;
using LeaderboardRecord = Common.DTO.LeaderboardRecord;

using static System.Math;

namespace LobbyServer.Services
{
    internal class Leaderboard
    {
        private readonly ILeaderboardsRepository _leaderboardsRepository;
        private readonly ICharacterRepository _characterRepository;
        private readonly IPlayerRepository _playerRepository;
        private Dictionary<long, LeaderboardRecord> _records;
        private List<LeaderboardRecord> _top50;
        private readonly PlayersService _players;

        private const long PLAYER_OF_THE_DAY_ACHIEVEMENT_ID = 6;
        private readonly DefferedCommand<ProgressAchievementCommand> Achievement = new DefferedCommand<ProgressAchievementCommand>();
        public Leaderboard(ILeaderboardsRepository leaderboardsRepository, IPlayerRepository playerRepository, ICharacterRepository characterRepository, PlayersService players)
        {
            _leaderboardsRepository = leaderboardsRepository;
            _playerRepository = playerRepository;
            _characterRepository = characterRepository;
            _players = players;
            GlobalTimer.FourHoursTimer.Tick += FourHoursTimerOnTick;
            FourHoursTimerOnTick(DateTime.Now);
        }

        private async void FourHoursTimerOnTick(DateTime dateTime)
        {
            await Update();
        }

        public List<LeaderboardRecord> GetRecordsForCharacter(long characterId)
        {
            if (_records == null || _top50 == null) return new List<LeaderboardRecord>();

            var retval = _top50.ToList();
            // if character is not in top 50
            LeaderboardRecord yourRecord;
            if (_records.TryGetValue(characterId, out yourRecord) && yourRecord.Position > 50)
            {
                retval.Add(yourRecord);
            }
            return retval;
        }

        private async Task Update()
        {
            _records = null;
            _top50 = null;
            var data = await _leaderboardsRepository.GetAll();
            var place = 1;
            var sorted = new List<LeaderboardRecord>();

            Data.Player topPlayerData = null;
            foreach(var dataRecord in data.OrderByDescending(r => r.Points))
            {
                var playerId = (await _characterRepository.Get(dataRecord.CharacterId)).UserId;
                var player = await _playerRepository.Get(playerId);
                if (topPlayerData == null)
                {
                    topPlayerData = player;
                }
                var playerName = player.Username;
                var retval = new LeaderboardRecord
                {
                    ArmySize = dataRecord.ArmySize,
                    CharacterId = dataRecord.CharacterId,
                    Points = dataRecord.Points,
                    Date = dataRecord.Date,
                    Kills = dataRecord.Kills,
                    PlayerName = playerName,
                    Position = place,
                    TopGold = dataRecord.TopGold,
                    TopTime = dataRecord.TopTime,
                };
                ++place;
                sorted.Add(retval);
            }

            _top50 = sorted.Take(50).ToList();
            _records = sorted.ToDictionary(r => r.CharacterId, r => r);

            // achievement
            if (topPlayerData != null)
            {
                var session = _players[topPlayerData.Id] ?? await OfflineWebSession.Get(topPlayerData.Id);
                await Achievement.Command.Execute(session, session.Player, PLAYER_OF_THE_DAY_ACHIEVEMENT_ID, 1);
            }
        }

        public async Task PostResult(BattleEndedForPlayer battleEndedData)
        {
            var record = await _leaderboardsRepository.GetCharacterRecord(battleEndedData.CharacterId) ??
                         new Data.LeaderboardRecord();
            record.CharacterId = battleEndedData.CharacterId;
            record.ArmySize = Max(battleEndedData.ArmySize, record.ArmySize);
            record.Kills = Max(battleEndedData.Kills, record.Kills);
            record.Points = Max((int)battleEndedData.Points, record.Points);
            record.TopGold = Max(battleEndedData.GoldGained, record.TopGold);
            record.TopTime = Max(battleEndedData.TimeAlive, record.TopTime);
            record.Date = DateTime.Now;
            // if new record
            if (record.Id == 0)
            {
                await _leaderboardsRepository.Insert(record);
            }
            else
            {
                await _leaderboardsRepository.Update(record);
            }
        }
    }
}
