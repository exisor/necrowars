﻿using System;

namespace LobbyServer.Services
{
    internal interface ITimer : IDisposable
    {
        int Resolution {get; }
        event Action<DateTime> Tick;
    }
}
