﻿
using System.Collections.Generic;
using Common.DTO;

namespace LobbyServer.Services
{
    public class ShopService
    {
        public List<ShopItem> Items => new List<ShopItem>
        {
            new ShopItem
            {
                BaseId = 1,
                CurrencyPrice = 0,
                GoldPrice = 1000,
                Id = 1,
            },
            new ShopItem
            {
                BaseId = 1,
                CurrencyPrice = 10,
                GoldPrice = 0,
                Id = 2,
            },
            new ShopItem
            {
                BaseId = 2,
                CurrencyPrice = 100,
                Id = 3,
            },
            new ShopItem
            {
                BaseId = 3,
                CurrencyPrice = 100,
                Id = 4,
            }
        };
    }
}
