﻿using System;

namespace LobbyServer.Services
{
    internal static class GlobalTimer
    {
        private static readonly ITimer _lowResTimer;
        private static object lockObject = new object();
        private static readonly ITimer _fourHourTimer;

        static GlobalTimer()
        {
            _lowResTimer = new Timer(1000);
            _fourHourTimer = new Timer((int)TimeSpan.FromHours(4).TotalMilliseconds);
        }

        public static ITimer LowResTimer
        {
            get
            {
                lock (lockObject)
                {
                    return _lowResTimer;
                }
            }
        }

        public static ITimer FourHoursTimer
        {
            get
            {
                lock (lockObject)
                {
                    return _fourHourTimer;
                }
            }
        }
    }
}
