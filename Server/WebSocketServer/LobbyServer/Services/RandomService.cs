﻿using System;

namespace LobbyServer.Services
{
    public static class RandomService
    {
        public static Random Instance { get; } = new Random(Environment.TickCount);
    }
}
