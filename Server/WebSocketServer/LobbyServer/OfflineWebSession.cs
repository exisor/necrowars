﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using LobbyServer.BL;
using SuperSocket.SocketBase;
using SuperSocket.WebSocket.Protocol;

namespace LobbyServer
{
    public class OfflineWebSession : WebSession
    {
        private static DefferedCommand<SetSessionUserIdCommand> SetUserId = new DefferedCommand<SetSessionUserIdCommand>();
        public static async Task<OfflineWebSession> Get(long id)
        {
            var retval = new OfflineWebSession();
            await SetUserId.Command.Execute(retval, id);
            return retval;
        }
        public override void Close()
        {

        }

        public override void Send<T>(T obj)
        {
        }

        public override void Send(string message)
        {
        }

        public override void Send(byte[] data, int offset, int length)
        {
        }

        public override void Send(ArraySegment<byte> segment)
        {
        }

        public override void Send(IList<ArraySegment<byte>> segments)
        {
        }

        public override void Send(string message, params object[] paramValues)
        {
        }

        public override void Close(CloseReason reason)
        {
        }

        public override void Initialize(IAppServer<WebSession, IWebSocketFragment> appServer, ISocketSession socketSession)
        {
        }
    }
}