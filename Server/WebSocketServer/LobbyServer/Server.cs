﻿using System;
using System.Net;
using System.Threading.Tasks;
using Autofac;
using ClientProtocol;
using Common;
using LobbyServer.Services;
using ServerCommon;
using SuperSocket.Common;
using SuperSocket.SocketBase;

namespace LobbyServer
{
    public class Server : ServerBase<Session, Server>
    {
        public WebServer WebServer { get; private set; }
        public UserService Users { get; private set; }
        public static Session Matchmaker { get; private set; }

        public override EndPoint MasterEndPoint { get; } = new IPEndPoint(IPAddress.Parse("127.0.0.1"), 2021);
        private EndPoint _clientEndPoint;
        public override EndPoint ClientEndPoint => _clientEndPoint;
        protected override ServerRole Role => ServerRole.Lobby;
        protected override string Game => 
#if DEBUG
            "Necrowars-test";
#else
            "Necrowars";
#endif
        private readonly BattleManager _battleManager = WebServer.Context.Resolve<BattleManager>();
        public string GameName => Game;
        protected override async void OnStarted()
        {
            var endpointStrings = Config.Options.GetValue("broadcastAddress")
                .Split(new[] {":"}, StringSplitOptions.None);
            _clientEndPoint = CreateEndpoint(new EndPointInfo
            {
                address = endpointStrings[0],
                port = int.Parse(endpointStrings[1])
            });

            WebServer = (WebServer)Bootstrap.GetServerByName("Lobby Server (Web)");
            Users = new UserService();
            base.OnStarted();
            await FindMatchmaker();
        }

        public async Task ConnectToMM(string address, int port)
        {
            try
            {
                var result = await ((IActiveConnector) this).ActiveConnect(CreateEndpoint(new EndPointInfo
                {
                    address = address,
                    port = port
                }));
                Matchmaker = (Session) result.Session;
                Matchmaker.Disconnected += MatchmakerOnDisconnected;
                _battleManager.MatchmakerConnected(Matchmaker);
            }
            catch (Exception e)
            {
                Logger.Error($"{e}\nAddress : {address}, Port : {port}");
            }
        }

        private async void MatchmakerOnDisconnected(Session session)
        {
            Matchmaker.Disconnected -= MatchmakerOnDisconnected;
            _battleManager.MatchmakerDisconnected();
            await FindMatchmaker();
        }

        private async Task FindMatchmaker()
        {
            while (MasterSession == null)
            {
                await Task.Delay(1000);
            }
            while (Matchmaker == null)
            {
                MasterSession.Send(ProtobufSerializer.MakeStringMessage(new RequestServer
                {
                    GameName = Game
                    ,ServerRole = ServerRole.Matchmaker,
                }));
                await Task.Delay(1000);
            }
        }
    }
}
