﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Threading.Tasks;
using Common;
using Common.ResourceManager;
using Common.Resources;
using Newtonsoft.Json.Linq;
using NUnit.Framework;
using NUnit.Framework.Constraints;
using NUnit.Framework.Internal.Execution;

namespace LobbyServer.Test
{
    //[TestFixture]
    //public class DBTest
    //{
    //    [Test]
    //    public async Task MultiMapData()
    //    {
    //        //var db = new GameDatabaseProvider();
    //        //var playerRepo = new PlayerRepository(db);

    //        //var player = await playerRepo.Get(1);
    //        //Assert.NotNull(player.Characters);
    //    }
    //}

    //[TestFixture]
    //public class TestJSON
    //{
    //    private class Test
    //    {
    //        public bool X { get; set; }
    //        public string Y { get; set; }
    //        public long U { get; set; }
    //    }
    //    [Test]
    //    public void Arrays()
    //    {
    //        //var msg =
    //        //    ProtobufSerializer.SerializeBinary();
    //        //var liest = ProtobufSerializer.Deserialize<List<Test>>(msg);

    //        var source = new List<Test> {new Test {X = true, Y = "xuy", U = 123,}, new Test()};
    //        var serializer = new JsonSerializer();
    //        byte[] bytes;
    //        using (var ms = new MemoryStream())
    //        {
    //            using (var writer = new BsonWriter(ms))
    //            {
    //                serializer.Serialize(writer, source);
    //            }
    //            bytes = ms.ToArray();
    //        }

    //        using (var ms = new MemoryStream(bytes))
    //        {
    //            using (var reader = new BsonReader(ms))
    //            {
    //                reader.ReadRootValueAsArray = true;
    //                var liest = serializer.Deserialize<List<Test>>(reader);
    //                Assert.NotNull(liest);
    //                Assert.AreEqual("xuy", liest[0].Y);
    //            }
    //        }

    //        //var data = JsonConvert.SerializeObject(source);
    //        //var liest = JsonConvert.DeserializeObject<List<Test>>(data);
    //        //Assert.NotNull(liest);
    //        //Assert.AreEqual("xuy", liest[0].Y);
    //    }
    //}

    //    [TestFixture]
    //public class TestServerProto
    //{
    //    [Test]
    //    public void TestUserLoggedIn()
    //    {
    //        var m = new UserLoggedIn
    //        {
    //            Game = "TestGame",
    //            Id = 1,
    //            Metadata = new UserMetadata
    //            {
    //                KongregateUserId = 2,
    //            },
    //            SessionKey = "123"
    //        };
    //        var n =
    //            ProtobufSerializer.Deserialize<UserLoggedIn>(
    //                new MessageInfo(ProtobufSerializer.MakeStringMessage(m)).Data);
    //        Assert.AreEqual(n.Metadata.KongregateUserId, m.Metadata.KongregateUserId);
    //    }
    //}
    //[TestFixture]
    //public class TestSurpriseChest
    //{
    //    [Test]
    //    public void Test()
    //    {
    //        var list = new List<SurpriseChestUseHandler.ItemDropProbability>();
    //        for (var i = 0; i < 10000; ++i)
    //        {
    //            list.Add(SurpriseChestUseHandler.Roll());
    //        }
    //        var outputList = new int[26];
    //        var rarity = new Dictionary<Rarity, int>
    //        {
    //            {Rarity.Common, 0},
    //            {Rarity.Epic, 0},
    //            {Rarity.Rare, 0},
    //            {Rarity.Legendary, 0}
    //        };

    //        foreach (var itemDropProbability in list)
    //        {
    //            outputList[itemDropProbability.BaseId - 1]+= itemDropProbability.Quantity;
    //            if (itemDropProbability.Rarity.HasValue)
    //            {
    //                rarity[itemDropProbability.Rarity.Value]++;
    //            }
    //            Console.WriteLine("Item : [{0}] Quantity : [{1}], Rarity : [{2}]", itemDropProbability.BaseId, itemDropProbability.Quantity, itemDropProbability.Rarity);
    //        }
    //        Console.WriteLine("TOTAL =================================================");
    //        var index = 1;
    //        foreach (var i in outputList)
    //        {
    //            Console.WriteLine("Total : [{0}] = [{1}]", index++, i);
    //        }
    //        Console.WriteLine("RARITY ================================================");
    //        foreach (var i in rarity)
    //        {
    //            Console.WriteLine("Items with rarity [{0}] : [{1}]", i.Key, i.Value);
    //        }

    //        Console.ReadLine();
    //    }
    //}



    [TestFixture]
    public class TestRestOGZ
    {

        private static string DecodeUrlString(string url)
        {
            string newUrl;
            while ((newUrl = Uri.UnescapeDataString(url)) != url)
                url = newUrl;
            return newUrl;
        }
        //    [Test]
        //    public void TestShop()
        //    {
        //        const string SECRET = "1ba3Cs6ROEWw8_aAI_epZg";
        //        const string income =
        //            "platform=vk&uid=31244&sn_uid=144961&app_id=66&sn_app_id=5817496&tid=2629&sn_tid=1266743&item=1&price=1.0&currency=VKVOTE&amount=1&idata=%7B%7D&tdata=&created_at=1487773378&time=1487773378&sig=73ea9c3fca93599045f18807d1736f3f";
        //        var decode = DecodeUrlString(income);
        //        var _params =
        //                decode.Split(new[] { "&" }, StringSplitOptions.RemoveEmptyEntries)
        //                    .Select(s => s.Split(new[] { "=" }, StringSplitOptions.None))
        //                    .ToDictionary(s => s[0], s => s[1]);
        //        var hashString =
        //            string.Join("",
        //            _params.Where(kvp => kvp.Key != "sig")
        //                .OrderBy(kvp => kvp.Key)
        //                .Select(kvp => string.Join("=", kvp.Key, kvp.Value))) + SECRET;
        //        string hash;
        //        using (var md5 = MD5.Create())
        //        {
        //            hash = BitConverter.ToString(md5.ComputeHash(Encoding.ASCII.GetBytes(hashString))).Replace("-", string.Empty).ToLower();
        //        }
        //        if (!string.Equals(hash, _params["sig"]))
        //        {
        //            throw new Exception("Invalid signature");
        //        }

        //    }
        //[Test]
        public async Task TestLevelNotify()
        {
            var ws = new WebSession {Metadata = new WebSession.SessionMetadata {OGZId = 126135253}};
            var cmd = new LobbyServer.BL.LevelupOGZNotifyCommand();

            await cmd.Execute(ws, 1);

            const string reqstring = "https://gateway.overgamez.com/api/apps/55/users/126135253/attributes?access_token=vh.031ac387de678fcc99f7ed11fdd1a308666e799ff00a64de8c9d3da86cfb0eeb";

            var request = WebRequest.CreateHttp(new Uri(reqstring));
            request.Method = "GET";

            JObject jobj;
            using (var resp = await request.GetResponseAsync())
            using (var s = resp.GetResponseStream())
            using (var r = new StreamReader(s))
            {
                var data = await r.ReadToEndAsync();
                jobj = JObject.Parse(data);
            }
            var succ = (bool)jobj["success"];
            Assert.True(succ);

            var attr = jobj["data"];
            var level = (int) attr["level"];
            Assert.AreEqual(1, level);
        }
    }

    public struct Vector3
    {
        public float x, y, z;

        public static Vector3 operator -(Vector3 lv, Vector3 rv)
        {
            return new Vector3 {x = lv.x - rv.x, y = lv.y - rv.y, z = lv.z - rv.z};
        }

        public static Vector3 operator *(Vector3 lv, float n)
        {
            return new Vector3 {x = lv.x * n, y = lv.y * n, z = lv.y * n};
        }

        public static Vector3 operator *(float n, Vector3 lv)
        {
            return lv*n;
        }
        public static Vector3 operator +(Vector3 lv, Vector3 rv)
        {
            return new Vector3 { x = lv.x + rv.x, y = lv.y + rv.y, z = lv.z + rv.z };
        }
    }
    public interface IPath
    {
        Vector3 start { get; }
        Vector3 end { get; }
        Vector3 getPoint(float t);
    }
    public struct Spline : IPath
    {
        private Vector3 p0;
        private Vector3 p1;

        public Spline(Vector3 s, Vector3 e)
        {
            p0 = s;
            p1 = e;
        }
        public Vector3 start => p0;
        public Vector3 end => p1;
        public Vector3 getPoint(float t)
        {
            if (t <= 0f || t >= 1) throw new Exception();
            return (1 - t) * p0 + t * p1;
        }

        public IPath backward()
        {
            return new Spline(p1, p0);
        }
    }
    public class SplineCollection
    {
        private Dictionary<Vector3, List<IPath>> _splines = new Dictionary<Vector3, List<IPath>>();
        public void CreateSpline(Vector3 p0, Vector3 p1)
        {
            var forward = new Spline(p0, p1);
            var backward = forward.backward();
            AddPath(forward);
            AddPath(backward);
        }

        private void AddPath(IPath p)
        {
            var start = p.start;
            List<IPath> list;
            if (!_splines.TryGetValue(start, out list))
            {
                list = new List<IPath>();
            }
            list.Add(p);
        }

        public IEnumerable<IPath> GetPaths(Vector3 p)
        {
            List<IPath> ret;
            _splines.TryGetValue(p, out ret);
            return ret;
        }
    }

    public class FixedShopItemsResourceManager : ShopItemsResourceManager
    {
        protected override string FileName => $@"D:\dev\Projects\Necrowar\necrowars\Server\WebSocketServer\bin\Debug\{base.FileName}";
    }

    [TestFixture]
    public class TestSpecials
    {
        [Test]
        public void LoadResources()
        {
            var shop = new FixedShopItemsResourceManager();
            var testItem = shop.Get(27);
            Assert.NotNull(testItem);
            Assert.NotNull(testItem.Limited);
            Assert.True(testItem.Limited.Ongoing);
            var testItem2 = shop.Get(28);
            Assert.NotNull(testItem2);
            Assert.NotNull(testItem2.Limited);
            Assert.False(testItem2.Limited.Ongoing);

            Func<int, int, double> getDiscount = (newVal, oldVal) =>
            {
                if (oldVal == 0) return 0;
                return (double)newVal / (double)oldVal;
            };

            var original = shop[testItem.Overrides];
            Assert.AreEqual(0.1f, getDiscount(testItem.CurrencyPrice, original.CurrencyPrice));
        }

        [Test]
        public void TestDateTime()
        {
            var todayLength1 = new DayOfWeekLimited
            {
                DayOfWeek = DateTime.Today.DayOfWeek,
                Length = TimeSpan.FromDays(1).TotalMinutes,
            };
            Assert.True(todayLength1.Ongoing);
            var yesterdayLength1 = new DayOfWeekLimited
            {
                DayOfWeek = (DateTime.Today - TimeSpan.FromDays(1)).DayOfWeek,
                Length = TimeSpan.FromDays(1).TotalMinutes,
            };
            Assert.False(yesterdayLength1.Ongoing);
            var yesterdayLength2 = new DayOfWeekLimited
            {
                DayOfWeek = (DateTime.Today - TimeSpan.FromDays(1)).DayOfWeek,
                Length = TimeSpan.FromDays(2).TotalMinutes,
            };
            Assert.True(yesterdayLength2.Ongoing);
            var weekAgo = new DayOfWeekLimited
            {
                DayOfWeek = (DateTime.Today - TimeSpan.FromDays(6)).DayOfWeek,
                Length = TimeSpan.FromDays(7).TotalMinutes
            };
            Assert.True(weekAgo.Ongoing);
            var specialDay = new DayOfYearLimited
            {
                Date = DateTime.Today,
                Length = TimeSpan.FromDays(10).TotalMinutes,
            };
            Assert.True(specialDay.Ongoing);
            var specialDayYearBefore = new DayOfYearLimited
            {
                Date = DateTime.Today - TimeSpan.FromDays(365),
                Length = TimeSpan.FromDays(10).TotalMinutes
            };
            Assert.False(specialDayYearBefore.Ongoing);
            var t1 = new ShopItem {Limited = specialDayYearBefore};
            var data = XmlSerializer.Serialize(t1);
        }
    }
}
