﻿using System;
using System.Collections.Generic;
using System.Linq;
using Common;
using Common.ResourceManager;
using Common.Resources;
using NUnit.Framework;

namespace LobbyServer.Test
{
    [TestFixture]
    public class AffixResourcesTest
    {
        [Test]
        public void Serialization()
        {
            var list = new List<Affix>();
            var resource = new AffixValues
            {
                Id = 1,
                MinimumItemLevel = 1,
                Values = new List<ValueCalculator>
                {
                    new ExponentValueCalculator
                    {
                        Name = "HP",

                        Multiplier = 400,
                        Exponent = 1.05f,

                        MinQuality = 0.5f,
                        QualityRange = 0.5f,
                        QualityPerEnchant = 0.05f,
                    }
                }
            };
            resource.Level = 10;
            resource.Enchant = 0;
            resource.SetSeed(0);
            var initialHP = resource.GetValue("HP");
            Assert.AreNotEqual(0, initialHP);
            list.Add(resource);
            var s = XmlSerializer.Serialize(list);
            Assert.NotNull(s);
            var deserialized = XmlSerializer.Deserialize<List<Affix>>(s);
            Assert.NotNull(deserialized);
            Assert.AreEqual(1, deserialized.Count);
            var cloned = deserialized[0] as AffixValues;
            Assert.NotNull(cloned);
            cloned.Level = 10;
            cloned.Enchant = 0;
            cloned.SetSeed(0);
            Assert.AreEqual(initialHP, cloned.GetValue("HP"));
        }

        private class FixedAffixResourceManager : AffixResourceManager
        {
            protected override string FileName => $@"D:\dev\Projects\Necrowar\necrowars\Server\WebSocketServer\bin\Debug\{base.FileName}";
        }
        [Test]
        public void AffixResources()
        {
            var rm = new FixedAffixResourceManager();
            Assert.AreEqual(38, rm.All.Count());
        }


        [Test]
        public void FixedHP()
        {
            CheckAffix(1, new AffixCheckValues("HP", 193));
        }

        [Test]
        public void FixedArmour()
        {
            CheckAffix(2, new AffixCheckValues("Armour", 6115));
        }

        [Test]
        public void ArmourPercent()
        {
            CheckAffix(3, new AffixCheckValues("Armour", 43));
        }

        [Test]
        public void HPPercent()
        {
            CheckAffix(4, new AffixCheckValues("HP", 38));
        }

        [Test]
        public void MinionHPPercent()
        {
            CheckAffix(5, new AffixCheckValues("HP", 87));
        }

        [Test]
        public void FixedMinionHP()
        {
            CheckAffix(6, new AffixCheckValues("HP", 49));
        }

        [Test]
        public void MinionArmourOfMaster()
        {
            CheckAffix(7, new AffixCheckValues("Armour", 58));
        }

        [Test]
        public void FixedMinionArmour()
        {
            CheckAffix(8, new AffixCheckValues("Armour", 781));
        }

        [Test]
        public void MinionArmourPercent()
        {
            CheckAffix(9, new AffixCheckValues("Armour", 29));
        }

        [Test]
        public void ReduceAoeDamage()
        {
            CheckAffix(10, new AffixCheckValues("Damage", 15));
        }

        [Test]
        public void ReduceNegativeEffectsDuration()
        {
            CheckAffix(11, new AffixCheckValues("Duration", 16));
        }

        [Test]
        public void IncreasePositiveEffectsDuration()
        {
            CheckAffix(12, new AffixCheckValues("Duration", 36));
        }

        [Test]
        public void ReduceEnergyConsumption()
        {
            CheckAffix(13, new AffixCheckValues("Energy", 18));
        }

        [Test]
        public void HPRegen()
        {
            CheckAffix(14, new AffixCheckValues("HP", 24));
        }

        [Test]
        public void MinionHPRegen()
        {
            CheckAffix(15, new AffixCheckValues("HP", 9));
        }

        [Test]
        public void PassiveSkillBonus()
        {
            CheckAffix(16, new AffixCheckValues("Skill", 10));
            var affix = InitializeAffix(16);
            var skills = affix["AvailableSkills"] as RandomFromCollectionValueCalculator;
            Assert.NotNull(skills);

            var shouldBe = new List<int> {2, 3, 4, 5, 6, 7};
            foreach (var i in shouldBe)
            {
                Assert.Contains(i, skills.Ids);
                skills.Ids.Remove(i);
            }
            Assert.AreEqual(0, skills.Ids.Count);
            Assert.AreNotEqual(0, affix.GetValue("AvailableSkills"));
        }

        [Test]
        public void ActiveSkillBonus()
        {
            CheckAffix(17, new AffixCheckValues("Skill", 6));
            var affix = InitializeAffix(17);
            var skills = affix["AvailableSkills"] as RandomFromCollectionValueCalculator;
            Assert.NotNull(skills);

            var shouldBe = new List<int> { 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 32, 33, 34, 35, 36, 37 };
            foreach (var i in shouldBe)
            {
                Assert.Contains(i, skills.Ids);
                skills.Ids.Remove(i);
            }
            Assert.AreEqual(0, skills.Ids.Count);
            Assert.AreNotEqual(0, affix.GetValue("AvailableSkills"));
        }

        [Test]
        public void FixedDamage()
        {
            CheckAffix(18, new AffixCheckValues("DamageMin", 6), new AffixCheckValues("DamageMax", 47));
        }

        [Test]
        public void DamagePercent()
        {
            CheckAffix(19, new AffixCheckValues("Damage", 43));
        }

        [Test]
        public void MinionDamagePercent()
        {
            CheckAffix(20, new AffixCheckValues("Damage", 56));
        }

        [Test]
        public void MasterDamageToMinion()
        {
            CheckAffix(21, new AffixCheckValues("Damage", 17));
        }

        [Test]
        public void AttackSpeedPercent()
        {
            CheckAffix(22, new AffixCheckValues("AttackSpeed", 30));
        }

        [Test]
        public void MinionAttackSpeedPercent()
        {
            CheckAffix(23, new AffixCheckValues("AttackSpeed", 17));
        }

        [Test]
        public void LifeLeech()
        {
            CheckAffix(24, new AffixCheckValues("Leech", 2));
        }

        [Test]
        public void EnergyLeech()
        {
            CheckAffix(25, new AffixCheckValues("Leech", 2));
        }

        [Test]
        public void AOEDamage()
        {
            CheckAffix(26, new AffixCheckValues("Damage", 57));
        }

        [Test]
        public void AOERadius()
        {
            CheckAffix(27, new AffixCheckValues("AOE", 15));
        }
        [Test]
        public void DecreasedCooldown()
        {
            CheckAffix(28, new AffixCheckValues("Cooldown", 15));
        }
        
        [Test]
        public void ImplicitArmour()
        {
            CheckAffix(29, new AffixCheckValues("Armour", 6115));
        }
        
        [Test]
        public void ImplicitDamage()
        {
            CheckAffix(30, new AffixCheckValues("DamageMin", 31), new AffixCheckValues("DamageMax", 88));
        }
        

        #region helpers
        private struct AffixCheckValues
        {
            public string Name;
            public int Value;

            public AffixCheckValues(string name, int value)
            {
                Name = name;
                Value = value;
            }
        }

        private void CheckAffix(long id, params AffixCheckValues[] values)
        {
            var affix = InitializeAffix(id);
            var rnd = new Random(0);
            foreach (var affixCheckValues in values)
            {
                var value = affix[affixCheckValues.Name];
                if (value is ExponentValueCalculator)
                {
                    Assert.AreEqual(ArrangeValue(affixCheckValues.Value, GetQuality(value as ExponentValueCalculator, rnd)), value.Value);
                }
                else
                {
                    Assert.AreEqual(affixCheckValues.Value, value.Value);
                }
            }
        }

        private AffixValues InitializeAffix(long id)
        {
            var rm = new FixedAffixResourceManager();
            var values = rm.Get(id) as AffixValues;
            Assert.NotNull(values);
            values.Level = 200;
            values.Enchant = 0;
            values.SetSeed(0);
            return values;
        }

        private int ArrangeValue(int val, double quality)
        {
            return (int) (val* quality);
        }

        private double GetQuality(ExponentValueCalculator value, Random random)
        {
            return value.MinQuality + random.NextDouble() * value.QualityRange;
        }
        #endregion
    }
}
