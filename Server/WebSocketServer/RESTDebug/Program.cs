﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using MasterServer.REST;
using MasterServer.REST.Handlers;

namespace RESTDebug
{
    class Program
    {
        private static bool AcceptAllCertifications(object sender, System.Security.Cryptography.X509Certificates.X509Certificate certification, System.Security.Cryptography.X509Certificates.X509Chain chain, System.Net.Security.SslPolicyErrors sslPolicyErrors)
        {
            return true;
        }
        static void Main(string[] args)
        {
            ServicePointManager.ServerCertificateValidationCallback = AcceptAllCertifications;
            var l = new HttpListener();
            l.Prefixes.Add("https://*:1337/");
            l.Start();
            var h = new RequestHandlersCollection(new [] {new ErrorRequestHandler(), });
            while (true)
            {
                var ctx = l.GetContext();
                h.Handle(ctx).Wait();
            }
        }
    }
}
