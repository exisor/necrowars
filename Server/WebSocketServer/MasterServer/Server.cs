﻿using System.Net;
using MasterServer.REST;
using MasterServer.REST.Handlers;
using SuperSocket.SocketBase;

namespace MasterServer
{
    public class Server : AppServer<Session>
    {
        public ServerManager ServerManager { get; private set; }

        private REST.RESTServer Rest;

        protected override void OnStarted()
        {
            base.OnStarted();
            ServerManager = new ServerManager();
        }
    }
}