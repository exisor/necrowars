﻿using ClientProtocol;
using Common;
using SuperSocket.WebSocket.SubProtocol;

namespace MasterServer.Commands
{
    public class RequestServer : SubCommandBase<WebSession>
    {
        public override void ExecuteCommand(WebSession session, SubRequestInfo requestInfo)
        {
            session.Logger.Info("RequestServer received");
            var message = ProtobufSerializer.Deserialize<ClientProtocol.RequestServer>(requestInfo.Body);
            session.Logger.Info($"Received message. GameName : {message.GameName} Role : {message.ServerRole}");
            var server = session.Server.ServerManager.GetServer(message.GameName, message.ServerRole);
            if (server != null)
            {
                var data = ProtobufSerializer.MakeStringMessage(new RequestServerResponse
                {
                    Address = server.Address,
                    Port = server.Port,
                    Role = message.ServerRole
                });
                session.Logger.Info($"Sending data : [{data}] to {session.RemoteEndPoint}");
                session.Send(data);
            }
            else
            {
                session.Send(ProtobufSerializer.MakeStringMessage(new Error(this, ErrorCode.ServerIsNotAvailable)));
            }
        }
    }
}