﻿using System;
using SuperSocket.SocketBase;
using SuperSocket.SocketBase.Protocol;

namespace MasterServer
{
    public class Session : AppSession<Session>
    {
        public Server Server => (Server) AppServer;
        public event Action<Session> Disconnected;

        protected override void HandleUnknownRequest(StringRequestInfo requestInfo)
        {
            Logger.Info($"Unknown request : \nKey : {requestInfo.Key}\nBody : {requestInfo.Body}");
            base.HandleUnknownRequest(requestInfo);
        }

        protected override void OnSessionClosed(CloseReason reason)
        {
            base.OnSessionClosed(reason);
            Disconnected?.Invoke(this);
        }
    }
}