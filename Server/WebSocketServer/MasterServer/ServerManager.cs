﻿using System.Collections.Generic;
using System.Linq;
using Common;

namespace MasterServer
{
    public class ServerManager
    {
        private readonly List<ServerInfo> _servers = new List<ServerInfo>();

        public void RegisterServer(string game, ServerRole role, string address, int port, Session session)
        {
            _servers.Add(new ServerInfo {Game = game, Role = role, Session = session, Address = address, Port = port});
            session.Disconnected += UnregisterServer;
        }

        public void UnregisterServer(Session session)
        {
            var server = _servers.ToList().FirstOrDefault(s => s.Session == session);
            if (server == null) return;
            _servers.Remove(server);
            server.Session.Disconnected -= UnregisterServer;
        }

        public ServerInfo GetServer(string game, ServerRole role)
        {
            return _servers.FirstOrDefault(s => s.Game == game && s.Role == role);
        }

        public class ServerInfo
        {
            public string Address;
            public string Game;
            public int Port;
            public ServerRole Role;
            public Session Session;
        }
    }
}