﻿using System.Net;
using System.Threading.Tasks;

namespace MasterServer.REST
{
    public interface IRequestHandlersCollection

    {
        Task Handle(HttpListenerContext context);
    }
}