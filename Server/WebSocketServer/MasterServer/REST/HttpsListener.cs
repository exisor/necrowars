﻿using System;
using System.Net;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace MasterServer.REST
{
    public class HttpsListener : Listener
    {
        private HttpListener _listener;
        private readonly CancellationTokenSource _cts = new CancellationTokenSource();
        public HttpsListener(IRequestHandlersCollection handlersCollection) : base(handlersCollection)
        {
        }

        #region Overrides of Listener

        public override void Listen(IPEndPoint endpoint)
        {
            _listener = new HttpListener();
            _listener.Prefixes.Add($"https://{endpoint.Address}:{endpoint.Port}/rest/");
            _listener.Start();

            Task.Run(async () =>
                {
                    while (_listener.IsListening)
                    {
                        var ctx = await _listener.GetContextAsync();
                        try
                        {
                            await HandlersCollection.Handle(ctx);
                        }
                        catch (Exception e)
                        {
                            await Console.Error.WriteLineAsync($"{e.Message} :\n{e.StackTrace}");
                        }
                    }
                }, _cts.Token
            );
        }

        protected override void OnDisposing()
        {
            _cts.Cancel();
            _listener.Stop();
            _cts.Dispose();
        }


        #endregion
    }
}