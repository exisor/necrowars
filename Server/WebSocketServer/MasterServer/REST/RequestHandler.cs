﻿using System.Threading.Tasks;

namespace MasterServer.REST
{
    public abstract class RequestHandler : IRequestHandler
    {
        #region Implementation of IRequestHandler

        public abstract RestRequestMethod Method { get; }
        public abstract string Name { get; }
        public abstract Task Handle(Request request, Response response);

        #endregion
    }
}