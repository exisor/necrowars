﻿using System.Threading.Tasks;

namespace MasterServer.REST.Handlers
{
    public class KongregateItemOrderRequestHandler : RequestHandler
    {
        #region Overrides of RequestHandler

        public override RestRequestMethod Method => RestRequestMethod.Any;
        public override string Name => "item_order_request";
        public override async Task Handle(Request request, Response response)
        {
            var gameId = (int) request.Data["game_id"];
            var buyerId = (int) request.Data["buyer_id"];
            var recipientId = (int) request.Data["recipient_id"];
            var orderId = (int) request.Data["order_id"];
            var orderInfo = (string) request.Data["order_info"];
        }

        #endregion
    }
}
