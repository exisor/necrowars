﻿using System;
using System.Threading.Tasks;

namespace MasterServer.REST.Handlers
{
    public class ErrorRequestHandler : RequestHandler
    {
        #region Overrides of RequestHandler

        public override RestRequestMethod Method => RestRequestMethod.ERROR;
        public override string Name => "Error";
        public override Task Handle(Request request, Response response)
        {
            return response.Send($"<HTML><BODY><PRE>{request.Data.ToString().Replace(Environment.NewLine, "<BR>")}</PRE></BODY></HTML>");
        }

        #endregion
    }
}
