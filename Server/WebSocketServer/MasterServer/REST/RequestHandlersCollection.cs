﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace MasterServer.REST
{
    public class RequestHandlersCollection : IRequestHandlersCollection
    {
        private const string DEFAULT_REQUEST_NAME = "404";
        private class DefaultRequestHandler : RequestHandler
        {
            #region Overrides of RequestHandler

            public override RestRequestMethod Method => RestRequestMethod.ERROR;
            public override string Name => DEFAULT_REQUEST_NAME;
            public override Task Handle(Request request, Response response)
            {
                return response.Send($"<HTML><BODY>Your request:<BR><PRE>{request.Data}</PRE><BR>{request.Name} : {request.Method}</BODY></HTML>");
            }

            #endregion
        }
        private readonly Dictionary<string, IRequestHandler> _handlers;
        private readonly DefaultRequestHandler _default = new DefaultRequestHandler();
        public RequestHandlersCollection(IEnumerable<IRequestHandler> handlers)
        {
            _handlers = handlers.ToDictionary(key => key.Name, value => value);
        }

        #region Implementation of IRequestHandlersCollection

        private Task Handle(Request request, Response response)
        {
            IRequestHandler handler;
            if (!_handlers.TryGetValue(request.Name, out handler))
            {
                handler = _default;
            }
            return handler.Handle(request, response);
        }

        #endregion

        #region Implementation of IRequestHandlersCollection

        public Task Handle(HttpListenerContext context)
        {
            return Handle(Request.FromHttp(context.Request), new Response(context.Response));
        }

        #endregion
    }
}