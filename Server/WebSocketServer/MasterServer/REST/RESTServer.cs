﻿using System;
using System.Net;
using System.Threading;
using System.Threading.Tasks;

namespace MasterServer.REST
{
    public sealed class RESTServer : IDisposable
    {
        private readonly IListener _listener;
        private bool _disposed;

        public RESTServer(IListener listener)
        {
            _listener = listener;
        }

        public void Start(IPEndPoint endPoint)
        {
            _listener.Listen(endPoint);
        }

        public void Stop()
        {
            Dispose();
        }

        #region Implementation of IDisposable

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        private void Dispose(bool disposing)
        {
            if (_disposed) return;

            _listener.Dispose();
            _disposed = true;
        }

        #endregion

        ~RESTServer()
        {
            Dispose(false);
        }
    }
}
