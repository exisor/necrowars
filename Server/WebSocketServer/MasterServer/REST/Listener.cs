﻿using System;
using System.Net;
using System.Threading.Tasks;

namespace MasterServer.REST
{
    public abstract class Listener : IListener
    {
        protected readonly IRequestHandlersCollection HandlersCollection;
        protected bool _disposed;
        protected Listener(IRequestHandlersCollection handlersCollection)
        {
            HandlersCollection = handlersCollection;
        }
        #region Implementation of IListener

        public abstract void Listen(IPEndPoint endpoint);
        public int Connections { get; set; } = 4;

        #endregion

        #region Implementation of IDisposable

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (_disposed) return;
            OnDisposing();
            _disposed = true;
        }

        ~Listener()
        {
            Dispose(false);
        }
        
        protected virtual void OnDisposing()
        { }
        #endregion
    }
}