﻿using System.Net;
using System.Threading.Tasks;

namespace MasterServer.REST
{
    public interface IRequestHandler
    {
        RestRequestMethod Method { get; }
        string Name { get; }
        Task Handle(Request request, Response response);
    }
}