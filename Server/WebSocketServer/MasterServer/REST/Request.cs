﻿using System;
using System.IO;
using System.Linq;
using System.Net;
using System.Security.Cryptography;
using System.Text;
using Newtonsoft.Json.Linq;

namespace MasterServer.REST
{
    public class Request
    {
        private const string API_KEY = "9d984ea4-f48c-4b7a-86c3-2d170361c6b2";
        public static Request FromHttp(HttpListenerRequest request)
        {
            var retval = new Request();

            switch (request.HttpMethod)
            {
                case "POST":
                    retval.Method = RestRequestMethod.POST;
                    break;
                default:
                    return Error($"Bad request : {Environment.NewLine}. Method : {request.HttpMethod} is not supported");
            }

            using (var stream = request.InputStream)
            using (var reader = new StreamReader(stream, request.ContentEncoding))
            {
                var encodedData = reader.ReadToEnd();
                if (!encodedData.Contains("signed_request="))
                {
                    return Error("Bad request");
                }
                encodedData = encodedData.Replace("signed_request=", string.Empty);
                var split = encodedData.Split('.');
                if (split.Length != 2)
                {
                    return Error("Bad request");
                }
                try
                {
                    var sig = FromBase64URL(split[0]);
                    retval.Data = JObject.Parse(Encoding.UTF8.GetString(FromBase64URL(split[1])));

                    var algorithm = (string)retval.Data["algorithm"];
                    if (algorithm.ToUpper() != "HMAC-SHA256")
                    {
                        return Error($"Unknown algorithm : {algorithm}. Expected HMAC-SHA256");
                    }

                    using (var sha = new HMACSHA256(Encoding.UTF8.GetBytes(API_KEY)))
                    {
                        var expectedSig = sha.ComputeHash(Encoding.UTF8.GetBytes(split[1]));
                        if (!ByteArrayEquals(expectedSig, sig))
                        {
                            return Error($"Signature mismatch!");
                        }
                    }

                    var name =
                        request.RawUrl.Split(new[] {"rest/"}, StringSplitOptions.RemoveEmptyEntries).LastOrDefault();
                    if (name == "event")
                    {
                        name = retval.Data["event"].ToString();
                    }
                    retval.Name = name;
                    return retval;
                }
                catch (Exception e)
                {
                    return
                        Error(
                            $"Bad request : {encodedData} {Environment.NewLine}Exception : {Environment.NewLine}{e.Message}{Environment.NewLine}{e.StackTrace}");
                }
            }
        }

        private static Request Error(string errorData)
        {
            return new Request
            {
                Data = new JObject {{"error", errorData}},
                Name = "Error",
                Method = RestRequestMethod.ERROR
            };
        }

        private static byte[] FromBase64URL(string data)
        {
            var replaced = data.Replace('_', '/').Replace('-', '+');
            switch (data.Length % 4)
            {
                case 2:
                    replaced += "==";
                    break;
                case 3:
                    replaced += "=";
                    break;
            }
            return Convert.FromBase64String(replaced);
        }

        private static bool ByteArrayEquals(byte[] left, byte[] right)
        {
            if (left.Length != right.Length)
            {
                return false;
            }

            for (var i = 0; i < left.Length; ++i)
            {
                if (left[i] == right[i]) return false;
            }
            return true;
        }

        public string Name { get; private set; }
        public JObject Data { get; private set; }
        public RestRequestMethod Method { get; private set; }
    }
}