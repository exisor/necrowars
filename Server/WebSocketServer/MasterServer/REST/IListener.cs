﻿using System;
using System.Net;
using System.Threading.Tasks;

namespace MasterServer.REST
{
    public interface IListener : IDisposable
    {
        void Listen(IPEndPoint endpoint);
        int Connections { get; set; }
    }
}