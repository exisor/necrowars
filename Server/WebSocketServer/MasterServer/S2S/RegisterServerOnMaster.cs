﻿using Common;
using SuperSocket.SocketBase.Command;
using SuperSocket.SocketBase.Protocol;

namespace MasterServer.S2S
{
    public class RegisterServerOnMaster : StringCommandBase<Session>
    {
        public override void ExecuteCommand(Session session, StringRequestInfo requestInfo)
        {
            var message =
                ProtobufSerializer.Deserialize<ServerProtocol.RegisterServerOnMaster>(requestInfo.GetFirstParam());
            ((Server) session.AppServer).ServerManager.RegisterServer(message.Game, message.Role, message.Address,
                message.Port, session);
            session.Logger.Info(
                $"RegisterServerOnMaster : Game : {message.Game}, Role : {message.Role}, Address : {message.Address}, Port : {message.Port}");
        }
    }
}