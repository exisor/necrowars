﻿using Common;
using SuperSocket.SocketBase.Command;
using SuperSocket.SocketBase.Protocol;

namespace MasterServer.S2S
{
    public class UserLoggedIn : StringCommandBase<Session>
    {
        public override void ExecuteCommand(Session session, StringRequestInfo requestInfo)
        {
            var message = ProtobufSerializer.Deserialize<ServerProtocol.UserLoggedIn>(requestInfo.Body);
            var lobby = session.Server.ServerManager.GetServer(message.Game, ServerRole.Lobby);
            if (lobby == null)
            {
                session.Logger.Info($"No lobby servers for game : {message.Game}");
                return;
            }
            lobby.Session.Send(ProtobufSerializer.MakeStringMessage(message));
        }
    }
}