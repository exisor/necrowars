﻿using ClientProtocol;
using Common;
using SuperSocket.SocketBase.Command;
using SuperSocket.SocketBase.Protocol;

namespace MasterServer.S2S
{
    public class RequestServer : StringCommandBase<Session>
    {
        public override void ExecuteCommand(Session session, StringRequestInfo requestInfo)
        {
            session.Logger.Info("RequestServer received");
            var message = ProtobufSerializer.Deserialize<ClientProtocol.RequestServer>(requestInfo.Body);
            session.Logger.Info($"Received message. GameName : {message.GameName} Role : {message.ServerRole}");
            var server = session.Server.ServerManager.GetServer(message.GameName, message.ServerRole);
            if (server != null)
            {
                var data = ProtobufSerializer.MakeStringMessage(new RequestServerResponse
                {
                    Address = server.Address,
                    Port = server.Port,
                    Role = message.ServerRole
                });
                session.Logger.Info($"Sending data : [{data}] to {session.RemoteEndPoint}");
                session.Send(data);
            }
            else
            {
                session.Send(ProtobufSerializer.MakeStringMessage(new Error(this, ErrorCode.ServerIsNotAvailable)));
            }
        }
    }
}