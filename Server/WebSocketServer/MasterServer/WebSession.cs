﻿using SuperSocket.WebSocket;

namespace MasterServer
{
    public class WebSession : WebSocketSession<WebSession>
    {
        public WebServer Server => (WebServer) AppServer;
    }
}