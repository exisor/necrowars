﻿using SuperSocket.SocketBase;
using SuperSocket.WebSocket;

namespace MasterServer
{
    public class WebServer : WebSocketServer<WebSession>
    {
        private Server _server;
        public ServerManager ServerManager => _server.ServerManager;

        protected override void OnStarted()
        {
            base.OnStarted();
            _server = Bootstrap.GetServerByName("Master Server (Tcp)") as Server;
        }
    }

    
}