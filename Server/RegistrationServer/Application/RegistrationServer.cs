﻿using System.Collections.Generic;
using System.Reflection;
using System.Threading.Tasks;
using Autofac;
using BareBone.Base.Server;
using BareBone.Configuration;
using RegistrationServer.Protocol;

namespace RegistrationServer.Application
{
    public class RegistrationServer : ServerBase
    {
        public RegistrationServer(IServerConfiguration configuration) : base(configuration)
        {
        }

        protected override void MapBindings(ContainerBuilder builder)
        {
        }

#pragma warning disable CS1998 // Async method lacks 'await' operators and will run synchronously
        protected override async Task OnStart()
#pragma warning restore CS1998 // Async method lacks 'await' operators and will run synchronously
        {
        }

        protected override IEnumerable<Assembly> ProtocolAssemblies => new List<Assembly> { Constants.PROTOCOL_ASSEMBLY};
    }
}