﻿using System;
using System.Threading.Tasks;
using BareBone.Base.ApplicationPlatform;

namespace ServerLauncher
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                MainAsync().Wait();
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                Console.ReadKey();
            }
        }

        private static async Task MainAsync()
        {
            var app = ApplicationPlatform.RunningApplicationPlatform;
            await app.Init();
            await app.Run();
            for (var input = Console.ReadLine(); input != "quit"; input = Console.ReadLine())
            {
                
            }
            app.Shutdown();
        }
    }
}
