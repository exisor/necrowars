﻿namespace BareBone.Configuration
{
    public partial class ServersCollection
    {
        private bool HandleUnrecognizedElement(string elementName, global::System.Xml.XmlReader reader)
        {
            switch (elementName)
            {
                case "manageServer":
                {
                    var manageConfig = new ManageServerConfiguration();
                    manageConfig.Deserialize(reader);
                    Add(manageConfig);
                    return true;
                }
                case "server":
                {
                    var serverConfig = new ServerConfiguration();
                    serverConfig.Deserialize(reader);
                    Add(serverConfig);
                    return true;
                }
                case "masterServer":
                {
                    var serverConfig = new MasterServerConfiguration();
                    serverConfig.Deserialize(reader);
                    Add(serverConfig);
                    return true;
                }
                case "slaveServer":
                {
                    var serverConfig = new SlaveServerConfiguration();
                    serverConfig.Deserialize(reader);
                    Add(serverConfig);
                    return true;
                }
                default:
                    return false;
            }
        }
    }
}