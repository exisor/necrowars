﻿namespace BareBone.Configuration
{
    public partial class SlaveServerConfiguration : ISlaveServerConfiguration
    {
    }

    public interface ISlaveServerConfiguration : IServerConfiguration
    {
        string MasterAddress { get; }
        int MasterPort { get; }
        int ConnectionTimeout { get; }
    }
}
