﻿using System.Collections.Generic;
using System.Linq;

namespace BareBone.Configuration
{
    public partial class PlatformConfiguration : IPlatformConfiguration
    {
        public IEnumerable<IServerConfiguration> ServerConfigurations => Servers.Cast<IServerConfiguration>();
    }

    public interface IPlatformConfiguration
    {
        IEnumerable<IServerConfiguration> ServerConfigurations { get; }
    }
}
