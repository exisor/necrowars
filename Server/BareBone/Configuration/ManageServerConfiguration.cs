﻿namespace BareBone.Configuration

{

    public interface IManageServerConfiguration : IServerConfiguration
    {
        string Password { get; }
    }
    public partial class ManageServerConfiguration : IManageServerConfiguration
    {

    }

}
