﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml;

namespace BareBone.Configuration
{
    public interface IServerConfiguration
    {
        string Name { get; }
        string Type { get; }
        int PeerBuferSize { get; }
        float TickRate { get; }
        IEnumerable<IListenerConfiguration> ListenerConfigurations { get; }
        string DatabaseConnectionString { get; }
    }
    [Serializable]
    public partial class ServerConfiguration : IServerConfiguration
    {
        internal void Deserialize(XmlReader reader)
        {
            base.DeserializeElement(reader, false);
        }

        public IEnumerable<IListenerConfiguration> ListenerConfigurations => Listeners.Cast<IListenerConfiguration>();
    }
}
