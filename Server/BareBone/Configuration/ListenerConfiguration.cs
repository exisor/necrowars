﻿
using System;
using Protocol;

namespace BareBone.Configuration
{
    public partial class ListenerConfiguration : IListenerConfiguration
    {
        public ListenerType ListenerType => (ListenerType)Enum.Parse(typeof (ListenerType), Type);
        public SocketType SocketType => (SocketType) Enum.Parse(typeof (SocketType), Socket);
    }

    public interface IListenerConfiguration
    {
        int Port { get; }
        string Name { get; }
        string Address { get; }
        int MaxClients { get; }
        ListenerType ListenerType { get; }
        SocketType SocketType { get;  }
    }
}
