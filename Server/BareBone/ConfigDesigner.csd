﻿<?xml version="1.0" encoding="utf-8"?>
<configurationSectionModel xmlns:dm0="http://schemas.microsoft.com/VisualStudio/2008/DslTools/Core" dslVersion="1.0.0.0" Id="fe0d2fb7-256f-4c22-afaa-6502057db15f" namespace="BareBone.Configuration" xmlSchemaNamespace="urn:BareBone" xmlns="http://schemas.microsoft.com/dsltools/ConfigurationSectionDesigner">
  <typeDefinitions>
    <externalType name="String" namespace="System" />
    <externalType name="Boolean" namespace="System" />
    <externalType name="Int32" namespace="System" />
    <externalType name="Int64" namespace="System" />
    <externalType name="Single" namespace="System" />
    <externalType name="Double" namespace="System" />
    <externalType name="DateTime" namespace="System" />
    <externalType name="TimeSpan" namespace="System" />
  </typeDefinitions>
  <configurationElements>
    <configurationSection name="PlatformConfiguration" displayName="PlatformConfiguration" codeGenOptions="Singleton, XmlnsProperty" xmlSectionName="platformConfiguration">
      <elementProperties>
        <elementProperty name="Servers" isRequired="false" isKey="false" isDefaultCollection="true" xmlName="servers" isReadOnly="false" displayName="servers">
          <type>
            <configurationElementCollectionMoniker name="/fe0d2fb7-256f-4c22-afaa-6502057db15f/ServersCollection" />
          </type>
        </elementProperty>
      </elementProperties>
    </configurationSection>
    <configurationElementCollection name="ServersCollection" hasCustomChildElements="true" xmlItemName="server" codeGenOptions="Indexer, AddMethod, RemoveMethod, GetItemMethods">
      <itemType>
        <configurationElementMoniker name="/fe0d2fb7-256f-4c22-afaa-6502057db15f/ServerConfiguration" />
      </itemType>
    </configurationElementCollection>
    <configurationElement name="ServerConfiguration" displayName="server">
      <attributeProperties>
        <attributeProperty name="Name" isRequired="true" isKey="true" isDefaultCollection="false" xmlName="name" isReadOnly="false">
          <type>
            <externalTypeMoniker name="/fe0d2fb7-256f-4c22-afaa-6502057db15f/String" />
          </type>
        </attributeProperty>
        <attributeProperty name="Type" isRequired="false" isKey="false" isDefaultCollection="false" xmlName="type" isReadOnly="false">
          <type>
            <externalTypeMoniker name="/fe0d2fb7-256f-4c22-afaa-6502057db15f/String" />
          </type>
        </attributeProperty>
        <attributeProperty name="PeerBuferSize" isRequired="false" isKey="false" isDefaultCollection="false" xmlName="peerBuferSize" isReadOnly="false">
          <type>
            <externalTypeMoniker name="/fe0d2fb7-256f-4c22-afaa-6502057db15f/Int32" />
          </type>
        </attributeProperty>
        <attributeProperty name="TickRate" isRequired="false" isKey="false" isDefaultCollection="false" xmlName="tickRate" isReadOnly="false">
          <type>
            <externalTypeMoniker name="/fe0d2fb7-256f-4c22-afaa-6502057db15f/Single" />
          </type>
        </attributeProperty>
        <attributeProperty name="DatabaseConnectionString" isRequired="false" isKey="false" isDefaultCollection="false" xmlName="databaseConnectionString" isReadOnly="false">
          <type>
            <externalTypeMoniker name="/fe0d2fb7-256f-4c22-afaa-6502057db15f/String" />
          </type>
        </attributeProperty>
      </attributeProperties>
      <elementProperties>
        <elementProperty name="Listeners" isRequired="false" isKey="false" isDefaultCollection="false" xmlName="listeners" isReadOnly="false">
          <type>
            <configurationElementCollectionMoniker name="/fe0d2fb7-256f-4c22-afaa-6502057db15f/ListenersCollection" />
          </type>
        </elementProperty>
      </elementProperties>
    </configurationElement>
    <configurationElement name="ManageServerConfiguration" displayName="manageServer">
      <baseClass>
        <configurationElementMoniker name="/fe0d2fb7-256f-4c22-afaa-6502057db15f/ServerConfiguration" />
      </baseClass>
      <attributeProperties>
        <attributeProperty name="Password" isRequired="false" isKey="false" isDefaultCollection="false" xmlName="password" isReadOnly="false">
          <type>
            <externalTypeMoniker name="/fe0d2fb7-256f-4c22-afaa-6502057db15f/String" />
          </type>
        </attributeProperty>
      </attributeProperties>
    </configurationElement>
    <configurationElement name="MasterServerConfiguration" displayName="masterServer">
      <baseClass>
        <configurationElementMoniker name="/fe0d2fb7-256f-4c22-afaa-6502057db15f/ServerConfiguration" />
      </baseClass>
    </configurationElement>
    <configurationElementCollection name="ListenersCollection" displayName="listeners" xmlItemName="listener" codeGenOptions="Indexer, AddMethod, RemoveMethod, GetItemMethods">
      <itemType>
        <configurationElementMoniker name="/fe0d2fb7-256f-4c22-afaa-6502057db15f/ListenerConfiguration" />
      </itemType>
    </configurationElementCollection>
    <configurationElement name="ListenerConfiguration" displayName="listener">
      <attributeProperties>
        <attributeProperty name="Address" isRequired="false" isKey="false" isDefaultCollection="false" xmlName="address" isReadOnly="false">
          <type>
            <externalTypeMoniker name="/fe0d2fb7-256f-4c22-afaa-6502057db15f/String" />
          </type>
        </attributeProperty>
        <attributeProperty name="Name" isRequired="true" isKey="true" isDefaultCollection="false" xmlName="name" isReadOnly="false">
          <type>
            <externalTypeMoniker name="/fe0d2fb7-256f-4c22-afaa-6502057db15f/String" />
          </type>
        </attributeProperty>
        <attributeProperty name="Port" isRequired="false" isKey="false" isDefaultCollection="false" xmlName="port" isReadOnly="false">
          <type>
            <externalTypeMoniker name="/fe0d2fb7-256f-4c22-afaa-6502057db15f/Int32" />
          </type>
        </attributeProperty>
        <attributeProperty name="MaxClients" isRequired="false" isKey="false" isDefaultCollection="false" xmlName="maxClients" isReadOnly="false">
          <type>
            <externalTypeMoniker name="/fe0d2fb7-256f-4c22-afaa-6502057db15f/Int32" />
          </type>
        </attributeProperty>
        <attributeProperty name="Type" isRequired="false" isKey="false" isDefaultCollection="false" xmlName="type" isReadOnly="false">
          <type>
            <externalTypeMoniker name="/fe0d2fb7-256f-4c22-afaa-6502057db15f/String" />
          </type>
        </attributeProperty>
      </attributeProperties>
    </configurationElement>
    <configurationElement name="SlaveServerConfiguration" displayName="slaveServer">
      <baseClass>
        <configurationElementMoniker name="/fe0d2fb7-256f-4c22-afaa-6502057db15f/ServerConfiguration" />
      </baseClass>
      <attributeProperties>
        <attributeProperty name="MasterAddress" isRequired="false" isKey="false" isDefaultCollection="false" xmlName="masterAddress" isReadOnly="false">
          <type>
            <externalTypeMoniker name="/fe0d2fb7-256f-4c22-afaa-6502057db15f/String" />
          </type>
        </attributeProperty>
        <attributeProperty name="MasterPort" isRequired="false" isKey="false" isDefaultCollection="false" xmlName="masterPort" isReadOnly="false">
          <type>
            <externalTypeMoniker name="/fe0d2fb7-256f-4c22-afaa-6502057db15f/Int32" />
          </type>
        </attributeProperty>
        <attributeProperty name="ConnectionTimeout" isRequired="false" isKey="false" isDefaultCollection="false" xmlName="connectionTimeout" isReadOnly="false">
          <type>
            <externalTypeMoniker name="/fe0d2fb7-256f-4c22-afaa-6502057db15f/Int32" />
          </type>
        </attributeProperty>
      </attributeProperties>
    </configurationElement>
  </configurationElements>
  <propertyValidators>
    <validators />
  </propertyValidators>
</configurationSectionModel>