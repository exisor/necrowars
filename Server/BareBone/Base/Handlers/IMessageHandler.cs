﻿using System;
using System.IO;
using System.Threading.Tasks;
using BareBone.Base.Peer;
using BareBone.Base.Pool;
using Protocol;

namespace BareBone.Base.Handlers
{
    public interface IMessageHandler<in TPeer>
        where TPeer : IPeer
    {
        Task Handle(Stream stream, TPeer peer);
        int MessageCode { get; }
        Type MessageType { get; }
        Task Handle(Message message, TPeer peer);
        IPoolObjectWrapper<Message> GetMessageWrapper();
    }
}
