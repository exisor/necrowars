﻿using System.Threading.Tasks;
using BareBone.Base.Peer;

namespace BareBone.Base.Handlers
{
    public interface IMessageHandlersCollection<in TPeer>
        where TPeer : IPeer
    {
        Task Handle(byte[] byteData, int offset, int length, TPeer peer);
    }
}
