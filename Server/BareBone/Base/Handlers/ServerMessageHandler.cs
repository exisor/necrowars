using BareBone.Base.Peer;
using log4net;
using Protocol;

namespace BareBone.Base.Handlers
{
    public abstract class ServerMessageHandler<TMessage> : MessageHandler<TMessage, ServerPeer>, IServerMessageHandler
        where TMessage : Message
    {
        protected ServerMessageHandler(ILog logger) : base(logger)
        {
        }
    }
}