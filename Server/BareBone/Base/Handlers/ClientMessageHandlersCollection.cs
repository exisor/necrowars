using System.Collections.Generic;
using BareBone.Base.Peer;
using BareBone.Base.Serialization;
using log4net;

namespace BareBone.Base.Handlers
{
    public class ClientMessageHandlersCollection : MessageHandlersCollection<ClientPeer>
    {
        public ClientMessageHandlersCollection(IEnumerable<IClientMessageHandler> handlers, ILog log, ISerializer serializer) : base(handlers, log, serializer)
        {
        }
    }
}