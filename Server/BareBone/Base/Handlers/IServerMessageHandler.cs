﻿using BareBone.Base.Peer;

namespace BareBone.Base.Handlers
{
    public interface IServerMessageHandler: IMessageHandler<ServerPeer>
    {
    }
}