using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
using BareBone.Base.Peer;
using BareBone.Base.Serialization;
using log4net;

namespace BareBone.Base.Handlers
{
    public abstract class MessageHandlersCollection<TPeer> : IMessageHandlersCollection<TPeer>
        where TPeer : IPeer
    {
        private readonly ILog _log;
        private readonly Dictionary<int, IMessageHandler<TPeer>> _handlers = new Dictionary<int, IMessageHandler<TPeer>>();
        private readonly byte[] _buffer = new byte[2048];
        private readonly Task _emptyTask;
        private readonly ISerializer _serializer;
        protected MessageHandlersCollection(IEnumerable<IMessageHandler<TPeer>> handlers, ILog log, ISerializer serializer)
        {
            _log = log;
            _serializer = serializer;
            _emptyTask = Task.CompletedTask;
            foreach (var messageHandler in handlers)
            {
                if (_handlers.ContainsKey(messageHandler.MessageCode))
                {
                    _log.Warn($"Duplicating message handlers for Type: [{messageHandler.MessageType}] and Type: [{_handlers[messageHandler.MessageCode]}]");
                }
                _handlers[messageHandler.MessageCode] = messageHandler;
                _serializer.AddMessage(messageHandler.MessageType);
            }
        }

        
        public async Task Handle(byte[] byteData, int offset, int length, TPeer peer)
        {
            using (var stream = new MemoryStream(byteData))
            {
                var firstByte = byteData[4];
                // getting server code from 0 1111 000 pattern
                var serverCode = (firstByte & 0x78) >> 3;
                // getting message code
                var dataAccessOffset = 5;
                var dataDecodeOffset = 0;
                byte reader;
                var accumulator = 0;
                do
                {
                    reader = byteData[dataAccessOffset];
                    accumulator += (reader & 0x7F) << dataDecodeOffset*7;
                    ++dataAccessOffset;
                    ++dataDecodeOffset;
                } while ((reader & 0x80) > 0);
                accumulator = (accumulator << 4) + serverCode;
                IMessageHandler<TPeer> handler;
                if (!_handlers.TryGetValue(accumulator, out handler))
                {
                    _log.Error($"No handler for messageCode : [{accumulator}]");
                    return;
                }
                stream.Position = 0;
                using (var msg = handler.GetMessageWrapper())
                {
                    _serializer.Deserialize(stream, msg.Value);
                    await handler.Handle(msg.Value, peer);
                }
            }
        }
    }
}