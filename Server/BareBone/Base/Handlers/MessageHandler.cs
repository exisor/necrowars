﻿using System;
using System.IO;
using System.Reflection;
using System.Threading.Tasks;
using BareBone.Base.Peer;
using BareBone.Base.Pool;
using log4net;
using ProtoBuf;
using Protocol;

namespace BareBone.Base.Handlers
{
    public abstract class MessageHandler<TMessage, TPeer> : IMessageHandler<TPeer> 
        where TMessage : Message
        where TPeer : IPeer
    {
        protected readonly ILog Log;
        private readonly MessagePool<TMessage> _pool;
        protected MessageHandler(ILog logger)
        {
            Log = logger;
            _pool = new MessagePool<TMessage>();
            MessageCode = typeof (TMessage).GetCustomAttribute<ProtoSubContract>().Tag;
        }

        public async Task Handle(Stream stream, TPeer peer)
        {
            using (var pooledMessage = _pool.Get())
            {
                var message = pooledMessage.Value;
                Serializer.MergeWithLengthPrefix(stream, message, PrefixStyle.Fixed32);
                await Handle(message, peer);
            }
        }

        public int MessageCode { get; }
        public Type MessageType => typeof (TMessage);
        public Task Handle(Message message, TPeer peer)
        {
            return Handle((TMessage) message, peer);
        }

        public IPoolObjectWrapper<Message> GetMessageWrapper()
        {
            return _pool.Get();
        }

        protected abstract Task Handle(TMessage message, TPeer peer);
    }
}