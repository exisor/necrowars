﻿using System;
using BareBone.Base.Peer;
using log4net;
using Protocol;

namespace BareBone.Base.Handlers
{
    public abstract class ClientMessageHandler<TMessage> : MessageHandler<TMessage, ClientPeer>, IClientMessageHandler
        where TMessage : Message
    {
        protected ClientMessageHandler(ILog logger) : base(logger)
        {
        }

        protected bool Auth<TUserData>(Func<TUserData, bool> validateFunc, ClientPeer peer)
            where TUserData : class, IUserData
        {
            var data = peer.Data<TUserData>();
            var retval = validateFunc(data);
            if (!retval)
            {
                Log.Warn($"Unauthorized request from ip : {peer.EndPoint.Address}");
                peer.Close();
            }
            return retval;
        }
    }
}