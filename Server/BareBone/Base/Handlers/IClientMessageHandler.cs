﻿using BareBone.Base.Peer;

namespace BareBone.Base.Handlers
{
    public interface IClientMessageHandler : IMessageHandler<ClientPeer>
    {
        
    }
}