﻿using System;
using System.Net;
using System.Threading.Tasks;
using Protocol;

namespace BareBone.Base.Peer
{
    public interface IPeer : IDisposable
    {
        IPEndPoint EndPoint { get; }
        Task Send(Message message);
        void Close();
        event Action Disconnecting;
    }
}
