﻿using System.Net.Sockets;

namespace BareBone.Base.Peer
{
    public interface IPeerFactory<out TPeer>
        where TPeer : IPeer
    {
        TPeer CreatePeer(Socket socket);
    }
}
