using System;
using System.Collections.Generic;
using System.IO;
using System.Net.Sockets;
using System.Threading.Tasks;
using BareBone.Base.Handlers;
using BareBone.Base.RPC;
using BareBone.Base.Serialization;
using log4net;
using Protocol;

namespace BareBone.Base.Peer
{
    public class ServerPeer : Peer<ServerPeer>
    {
        #region RPCForwarderPeer
        private class RPCForwarderPeer : ServerPeer
        {
            private readonly Func<Message, Task> _callback;
            public RPCForwarderPeer(Func<Message, Task> callback)
            {
                _callback = callback;
            }

            public override Task Send(Message message)
            {
                return _callback(message);
            }
        }
        #endregion
        #region factory
        public delegate ServerPeer Factory(Socket socket);
        #endregion
        #region constructors
        public ServerPeer(Socket socket, ILog logger, IServerInfo serverInfo, ServerMessageHandlersCollection handlers, ISerializer serializer) 
            : base(socket, logger, serverInfo, handlers, serializer)
        {
        }

        private ServerPeer()
        {
            
        }
        #endregion
        #region RPC
        public async Task<TResult> Rpc<TResult>(Message request, TResult response)
            where TResult : Message
        {
            var call = new RPCCall<TResult>(request, response, Serializer);
            await call.Call(this);
            return await call.Result;
        }

        public readonly Dictionary<Guid, IRPCCall> PendingRpc = new Dictionary<Guid, IRPCCall>();

        public Task HandleRpcCall(RPCCallData rpcData)
        {
            return Handlers.Handle(rpcData.RpcData, 0, rpcData.RpcData.Length, new RPCForwarderPeer(_ =>
            {
                using (var stream = new MemoryStream())
                {
                    Serializer.Serialize(stream, _);
                    rpcData.RpcData = stream.ToArray();
                }
                return Send(rpcData);
            }));
        }
        #endregion
        #region Properties
        public string ClientAddress { get; set; }
        public int ClientPort { get; set; }
        public string Game { get; set; }
        #endregion
    }

}