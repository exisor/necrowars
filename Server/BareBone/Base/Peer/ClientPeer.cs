using System;
using System.Collections.Generic;
using System.Net.Sockets;
using BareBone.Base.Handlers;
using BareBone.Base.Serialization;
using log4net;
using Protocol;

namespace BareBone.Base.Peer
{
    public class ClientPeer : Peer<ClientPeer>
    {
        public delegate ClientPeer Factory(Socket socket);

        private readonly Dictionary<Type, IUserData> _userData = new Dictionary<Type, IUserData>();

        public ClientPeer(Socket socket, ILog logger, IServerInfo serverInfo, ClientMessageHandlersCollection handlers, ISerializer serialzier, IEnumerable<IUserData> userData) 
            : base(socket, logger, serverInfo, handlers, serialzier)
        {
            if (userData == null) return;
            foreach (var data in userData)
            {
                _userData[data.GetType()] = data;
            }
        }

        public T Data<T>()
            where T : class, IUserData
        {
            IUserData retval;
            if (!_userData.TryGetValue(typeof (T), out retval))
            {
                Log.Warn($"Requested non existed userData from peer. Type : {typeof(T)}");
            }
            return retval as T;
        }

    }
}