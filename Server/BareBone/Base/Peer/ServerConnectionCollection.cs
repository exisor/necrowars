using System.Threading.Tasks;

namespace BareBone.Base.Peer
{
    public class ServerConnectionCollection : ConnectionCollection<ServerPeer>, IServerConnectionCollection
    {
        public ServerPeer MasterPeer { get; private set; }
        public void ConnectMaster(ServerPeer peer)
        {
            MasterPeer = peer;
            Connect(peer);
            peer.Disconnecting += () => MasterPeer = null;
        }
    }
}