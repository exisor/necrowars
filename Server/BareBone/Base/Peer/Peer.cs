﻿using System;
using System.IO;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using System.Threading.Tasks;
using BareBone.Base.Handlers;
using BareBone.Base.Serialization;
using log4net;
using Protocol;

namespace BareBone.Base.Peer
{
    public abstract class Peer<TPeer> : IPeer
        where TPeer : Peer<TPeer>, IPeer
    {
        protected readonly ILog Log;
        private readonly Socket _socket;

        private readonly byte[] _sendBufer;
        private MemoryStream _sendBuferStream;

        protected readonly IMessageHandlersCollection<TPeer> Handlers;

        private readonly CancellationTokenSource _cancellationTokenSource;
        protected readonly ISerializer Serializer;
        protected Peer(Socket socket, ILog logger, IServerInfo serverInfo, IMessageHandlersCollection<TPeer> handlers, ISerializer serializer)
        {
            Log = logger;
            _socket = socket;
            Handlers = handlers;
            Serializer = serializer;

            _sendBufer = new byte[serverInfo.PeerBuferSize];
            _sendBuferStream = new MemoryStream(_sendBufer, true);

            EndPoint = (IPEndPoint)socket.RemoteEndPoint;
            Log.Info($"New client connected from endpoint : [{EndPoint}]");

            _cancellationTokenSource = new CancellationTokenSource();
            var cancellationToken = _cancellationTokenSource.Token;
            Task.Run(Listen, cancellationToken);
        }

        protected Peer()
        {
            
        }
        private async Task Listen()
        {
            var buffer = new byte[2048];
            for (;;)
            {
                try
                {
                    if (!_socket.Connected)
                    {
                        Disconnect();
                        return;
                    }

                    var bytesRead = await Task.Factory.FromAsync(
                        // ReSharper disable once AssignNullToNotNullAttribute
                        _socket.BeginReceive(buffer, 0, buffer.Length, SocketFlags.None, null, _socket),
                        _socket.EndReceive).ConfigureAwait(false);
                    if (bytesRead <= 0) continue;
                    await Handlers.Handle(buffer, 0, bytesRead, (TPeer)this).ConfigureAwait(false);
                }
                catch (ObjectDisposedException)
                {
                    break;
                }
                catch (Exception e)
                {
                    Log.Error("Peer socket listen error", e);
                    break;
                }
            }
            Disconnect();
        }

        protected void Disconnect()
        {
            if (_socket.Connected) _socket.Close();
            _cancellationTokenSource.Cancel();
            var hanlde = Disconnecting;
            hanlde?.Invoke();
        }

        public IPEndPoint EndPoint { get; }

        public virtual async Task Send(Message message)
        {
            _sendBuferStream.Position = 0;
            Serializer.Serialize(_sendBuferStream, message);
            var size = BitConverter.ToInt32(_sendBufer, 0) + 4;
            await Task.Factory.FromAsync(
                _socket.BeginSend(_sendBufer, 0, size, SocketFlags.None, null, _socket),
                _socket.EndSend)
                .ConfigureAwait(false);
        }

        public void Close()
        {
            Dispose();
        }

        public event Action Disconnecting;

        public virtual void Dispose()
        {
            if (_socket.Connected) Disconnect();
            _sendBuferStream?.Dispose();
            _sendBuferStream = null;
            Disconnecting = null;
        }
    }
}