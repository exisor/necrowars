using System.Threading.Tasks;

namespace BareBone.Base.Peer
{
    public interface IServerConnectionCollection : IConnectionCollection<ServerPeer>
    {
        ServerPeer MasterPeer { get; }
        void ConnectMaster(ServerPeer peer);
    }
}