﻿using System.Net.Sockets;

namespace BareBone.Base.Peer
{
    public class ServerPeerFactory : IPeerFactory<ServerPeer>
    {
        private readonly ServerPeer.Factory _factory;

        public ServerPeerFactory(ServerPeer.Factory factory)
        {
            _factory = factory;
        }

        public ServerPeer CreatePeer(Socket socket)
        {
            return _factory(socket);
        }
    }
}