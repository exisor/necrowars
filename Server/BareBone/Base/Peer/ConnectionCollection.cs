using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BareBone.Base.Peer
{
    public abstract class ConnectionCollection<TPeer> : IConnectionCollection<TPeer>
        where TPeer : IPeer
    {
        private readonly List<TPeer> _peers = new List<TPeer>();

        public void Connect(TPeer peer)
        {
            peer.Disconnecting += () =>
            {
                _peers.Remove(peer);
            };
            _peers.Add(peer);
            OnNewPeerConnected(peer);
        }

        public IEnumerable<TPeer> Peers => _peers;

        public int ConnectedPeers => _peers.Count;
        public event Action<IPeer> NewPeerConnected;

        protected virtual void RaiseNewPeerConnected(IPeer arg)
        {
            var handle = NewPeerConnected;
            handle?.Invoke(arg);
        }
        public void Dispose()
        {
            foreach (var peer in Peers.ToList())
            {
                peer.Close();
            }
        }

        protected virtual void OnNewPeerConnected(IPeer peer)
        {
            RaiseNewPeerConnected(peer);
        }
    }
}