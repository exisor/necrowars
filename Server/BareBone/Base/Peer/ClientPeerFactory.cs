﻿using System.Net.Sockets;

namespace BareBone.Base.Peer
{
    public class ClientPeerFactory : IPeerFactory<ClientPeer>
    {
        private readonly ClientPeer.Factory _factory;

        public ClientPeerFactory(ClientPeer.Factory factory)
        {
            _factory = factory;
        }

        public ClientPeer CreatePeer(Socket socket)
        {
            return _factory(socket);
        }
    }
}