﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace BareBone.Base.Peer
{
    public interface IConnectionCollection: IDisposable
    {
        int ConnectedPeers { get; }

        event Action<IPeer> NewPeerConnected;
    }

    public interface IConnectionCollection<TPeer> : IConnectionCollection where TPeer : IPeer
    {
        void Connect(TPeer peer);
        IEnumerable<TPeer> Peers { get; }
    }
}
