﻿using System.Threading.Tasks;
using Protocol;

namespace BareBone.Base.Peer
{
    public class ClientConnectionCollection : ConnectionCollection<ClientPeer>
    {
        private readonly ServerInfo _info;

        public ClientConnectionCollection(IServerInfo info)
        {
            _info = (ServerInfo)info;
        }

        protected override void OnNewPeerConnected(IPeer peer)
        {
            _info.CurrentClients = ConnectedPeers;
            base.OnNewPeerConnected(peer);
        }
    }

}