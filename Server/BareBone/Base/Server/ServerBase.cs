using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Reflection;
using System.Threading;
using System.Threading.Tasks;
using Autofac;
using BareBone.Base.DAL;
using BareBone.Base.Handlers;
using BareBone.Base.Listener;
using BareBone.Base.Peer;
using BareBone.Base.Pool;
using BareBone.Base.RPC;
using BareBone.Base.Serialization;
using BareBone.Configuration;
using log4net;
using Protocol;

namespace BareBone.Base.Server
{
    public abstract class ServerBase : IServer
    {
        #region fields

        private readonly CancellationTokenSource _cancellationTokenSource;
        private readonly CancellationToken _cancellationToken;
        private readonly ServerInfo _info;
        protected readonly ILog Log;
        protected readonly List<IListener> Listeners = new List<IListener>();
        private readonly  string _connectionString;
        #endregion
        #region constructor
        protected ServerBase(IServerConfiguration configuration)
        {
            Log = LogManager.GetLogger(configuration.Name);
            _connectionString = configuration.DatabaseConnectionString;
            _info = new ServerInfo
            {
                Name = configuration.Name,
                Id = Guid.NewGuid(),
                Status = ServerStatus.Down,
                PeerBuferSize = configuration.PeerBuferSize,
                Listeners = configuration.ListenerConfigurations.Select(config => new ListenerInfo
                {
                    Address = config.Address,
                    MaxClients = config.MaxClients,
                    Port = config.Port,
                    Type = config.ListenerType,
                    Name = config.Name,
                    SocketType = config.SocketType,
                }).ToList()
            };
            _cancellationTokenSource = new CancellationTokenSource();
            _cancellationToken = _cancellationTokenSource.Token;
        }
        #endregion
        #region IServer
        public IServerInfo Info => _info;
        public async Task Init()
        {
            Log.Info("Initializing server");
            _info.Status = ServerStatus.Initializing;
            try
            {
                await Task.Run(() => InitContainer(), _cancellationToken);
            }
            catch (Exception e)
            {
                Log.Error("Error when initializing container", e);
                _info.Status = ServerStatus.Error;
                throw;
            }
            try
            {
                var factory = Container.Resolve<IListenerFactory>();
                foreach (var listenerInfo in _info.Listeners)
                {
                    var listener = factory.CreateListener(listenerInfo);
                    Listeners.Add(listener);
                }
            }
            catch (Exception e)
            {
                Log.Error("Cannot create listeners", e);
                _info.Status = ServerStatus.Error;
                throw;
            }
            var serializer = Container.Resolve<ISerializer>();
            ProtocolAssemblies.ToList().ForEach(serializer.AddMessagesFromAssembly);
            _info.Status = ServerStatus.Initialized;
            Log.Info("Initialization success");
        }
        public async Task Run()
        {
            Log.Info("Starting listener");
            _info.Status = ServerStatus.Starting;
            try
            {
                await Task.WhenAll(Listeners.Select(listener => listener.Listen()));
            }
            catch (Exception e)
            {
                Log.Error("Error when trying to initialize socket listener", e);
                _info.Status = ServerStatus.Error;
                throw;
            }
            Log.Info("Starting server");
            _info.Status = ServerStatus.Started;
            await OnStart();
        }
        public void Shutdown()
        {
            Log.Info("Shutting down");
            _info.Status = ServerStatus.ShuttingDown;
            _cancellationTokenSource.Cancel();
            foreach (var listener in Listeners)
            {
                listener.Dispose();
            }
            _info.Status = ServerStatus.Down;
            Log.Info("Server down");
        }

        public bool IsRunning => _info.Status == ServerStatus.Started || _info.Status == ServerStatus.Starting;

        #endregion
        #region protected members
        protected IContainer Container { get; set; }
        protected abstract void MapBindings(ContainerBuilder builder);
        protected abstract Task OnStart();
        protected abstract IEnumerable<Assembly> ProtocolAssemblies { get; }
        #endregion
        #region Helpers

        private void InitContainer()
        {
            var assembly = GetType().Assembly;
            var builder = new ContainerBuilder();
            builder.RegisterInstance(_info).As<IServerInfo>().SingleInstance();
            builder.RegisterType<ClientPeer>();
            builder.RegisterType<ClientConnectionCollection>().SingleInstance();
            builder.RegisterType<ClientListener>();
            builder.RegisterType<ClientPeerFactory>();
            builder.RegisterType<ServerPeer>();
            builder.RegisterType<ServerConnectionCollection>().AsImplementedInterfaces().AsSelf().SingleInstance();
            builder.RegisterType<ServerListener>();
            builder.RegisterType<ServerPeerFactory>();
            builder.RegisterInstance(Log).As<ILog>().SingleInstance();
            builder.RegisterType<ClientMessageHandlersCollection>().SingleInstance();
            builder.RegisterType<ServerMessageHandlersCollection>().SingleInstance();
            builder.RegisterType<ListenerFactory>().AsImplementedInterfaces();
            builder.RegisterType<Serializer>().As<ISerializer>().SingleInstance();
            builder.RegisterAssemblyTypes(assembly)
                .AssignableTo<IUserData>()
                .As<IUserData>();

            builder.RegisterType<RPCMessageHandler>().As<IServerMessageHandler>().SingleInstance();
            builder.RegisterAssemblyTypes(assembly)
                .AssignableTo<IClientMessageHandler>()
                .As<IClientMessageHandler>()
                .SingleInstance();
            builder.RegisterAssemblyTypes(assembly)
                .AssignableTo<IServerMessageHandler>()
                .As<IServerMessageHandler>()
                .SingleInstance();
            // DAL

            builder.RegisterType<DapperDataProvider>().As<IDataProvider>().WithParameter("connectionString", _connectionString)
                .SingleInstance();
            MapBindings(builder);
            Container = builder.Build();
        }

        #endregion

        public void Dispose()
        {
        }
    }
}
