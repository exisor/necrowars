﻿using System;
using BareBone.Configuration;
using log4net;

namespace BareBone.Base.Server
{
    internal class ServerFactory : IServerFactory
    {
        private readonly ILog _log;
        internal ServerFactory(ILog logger)
        {
            _log = logger;
        }
        public IServer CreateServer(IServerConfiguration configuration)
        {
            try
            {
                var server = Activator.CreateInstance(Type.GetType(configuration.Type, true), configuration);
                return server as IServer;
            }
            catch (Exception e)
            {
                _log.Error("Cannot create type from assembly", e);
                return null;
            }
        }
    }
}