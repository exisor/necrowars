﻿using BareBone.Configuration;

namespace BareBone.Base.Server
{
    interface IServerFactory
    {
        IServer CreateServer(IServerConfiguration configuration);
    }
}
