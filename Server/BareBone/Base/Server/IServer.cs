﻿using System;
using System.Threading.Tasks;
using Protocol;

namespace BareBone.Base.Server
{
    public interface IServer : IDisposable
    {
        Task Init();
        IServerInfo Info { get; }
        Task Run();
        void Shutdown();
        bool IsRunning { get; }
    }
}
