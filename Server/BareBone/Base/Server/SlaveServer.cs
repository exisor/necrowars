using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Threading.Tasks;
using Autofac;
using BareBone.Base.Listener;
using BareBone.Base.Peer;
using BareBone.Configuration;
using MasterServer.Protocol.Messages.Request;
using Protocol;
using SocketType = System.Net.Sockets.SocketType;

namespace BareBone.Base.Server
{
    public abstract class SlaveServer : ServerBase
    {
        private readonly IPEndPoint _metaEndPoint;
        private ServerPeerFactory _serverPeerFactory;
        private IServerConnectionCollection _serverConnectionCollection;
        private readonly ServerRegisterRequest _registerRequest = new ServerRegisterRequest();
        protected SlaveServer(ISlaveServerConfiguration configuration) : base(configuration)
        {
            _metaEndPoint = new IPEndPoint(IPAddress.Parse(configuration.MasterAddress), configuration.MasterPort);
            MasterConnectionTimeout = configuration.ConnectionTimeout;
        }

        protected override async Task OnStart()
        {
            _serverPeerFactory = Container.Resolve<ServerPeerFactory>();
            _serverConnectionCollection = Container.Resolve<IServerConnectionCollection>();
            await ConnectToMaster();
        }

        protected abstract Task OnConnectedToMaster();

        protected int MasterConnectionTimeout { get; }

        #region helpers

        private async Task ConnectToMaster()
        {
            await Task.Factory.StartNew(async () =>
            {
                var socket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
                for (;;)
                {
                    await Task.Factory.FromAsync(socket.BeginConnect, socket.EndConnect, _metaEndPoint, socket);
                    if (socket.Connected)
                    {
                        break;
                    }
                    await Task.Delay(MasterConnectionTimeout);
                }
                var peer = _serverPeerFactory.CreatePeer(socket);
                peer.Disconnecting += async () => await ConnectToMaster();
                _serverConnectionCollection.ConnectMaster(peer);

                var clientListener = Listeners.FirstOrDefault(l => l is ClientListener);
                if (clientListener == null)
                {
                    Log.Warn("No client listeners available on server. Cannot register as service");
                }
                else
                {
                    _registerRequest.Role = Role;
                    _registerRequest.GameName = Game;

                    _registerRequest.Address = clientListener.EndPoint.Address.ToString();
                    _registerRequest.Port = clientListener.EndPoint.Port;

                    await _serverConnectionCollection.MasterPeer.Send(_registerRequest);
                }
                await OnConnectedToMaster();
            });
        }

        protected abstract ServerRole Role { get; }
        protected abstract string Game { get; }

        #endregion
    }
}