﻿using System.Threading.Tasks;
using BareBone.Base.Handlers;
using BareBone.Base.Peer;
using log4net;
using Protocol;

namespace BareBone.Base.RPC
{
    public class RPCMessageHandler : ServerMessageHandler<RPCCallData>
    {
        public RPCMessageHandler(ILog logger) : base(logger)
        {
        }

        protected override async Task Handle(RPCCallData message, ServerPeer peer)
        {
            IRPCCall pendingCall;
            if (!peer.PendingRpc.TryGetValue(message.Id, out pendingCall))
            {
                await peer.HandleRpcCall(message).ConfigureAwait(false);
                return;
            }
            pendingCall.Complete(message.RpcData);
        }
    }
}
