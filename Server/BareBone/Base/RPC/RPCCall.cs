﻿using System;
using System.IO;
using System.Runtime.Remoting.Messaging;
using System.Threading;
using System.Threading.Tasks;
using BareBone.Base.Peer;
using BareBone.Base.Pool;
using BareBone.Base.Serialization;
using Protocol;

namespace BareBone.Base.RPC
{
    public class RPCCall<TReturnValue> : IRPCCall, IRPCCall<TReturnValue>
        where TReturnValue : Message
    {
        private readonly RPCCallData _data = new RPCCallData();
        private readonly ISerializer _serializer;
        private readonly TaskCompletionSource<TReturnValue> _tcs = new TaskCompletionSource<TReturnValue>();
        private readonly CancellationTokenSource _cts = new CancellationTokenSource();
        private readonly TReturnValue _retval;
        public RPCCall(Message messageToSend, TReturnValue retVal, ISerializer serializer)
        {
            Result = _tcs.Task;
            _serializer = serializer;
            _retval = retVal;
            Id = Guid.NewGuid();
            _data.Id = Id;

            using (var stream = new MemoryStream())
            {
                _serializer.Serialize(stream, messageToSend);
                _data.RpcData = stream.ToArray();
            }
        }

        public void Complete(byte[] data)
        {
            using (var stream = new MemoryStream(data))
            {
                _serializer.Deserialize(stream, _retval);
            }
            _tcs.SetResult(_retval);
        }

        public Guid Id { get; }
        public async Task Call(ServerPeer peer)
        {
            peer.PendingRpc[Id] = this;
            await peer.Send(_data);
            await Task.Factory.StartNew(async () =>
            {
                await Task.Delay(3000, _cts.Token);
                if (!Result.IsCompleted)
                {
                    _tcs.SetException(new TimeoutException("Master server is not responding"));
                }
            }, _cts.Token);
        }

        public Task<TReturnValue> Result { get; }
    }

    public interface IRPCCall<TReturnValue>
        where TReturnValue : Message
    {
        Task<TReturnValue> Result { get; }
    }

    public interface IRPCCall
    {
        void Complete(byte[] data);
        Guid Id { get; }
        Task Call(ServerPeer peer);
    }
}
