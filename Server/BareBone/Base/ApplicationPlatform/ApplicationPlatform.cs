﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Threading.Tasks;
using BareBone.Base.Server;
using BareBone.Configuration;
using log4net;
using ProtoBuf.Meta;
using Protocol;

namespace BareBone.Base.ApplicationPlatform
{
    public class ApplicationPlatform : IApplicationPlatform
    {
        private static IApplicationPlatform _platform;
        public static IApplicationPlatform RunningApplicationPlatform => _platform ?? NewApplicationPlatform;

        public static IApplicationPlatform NewApplicationPlatform
        {
            get
            {
                if (_platform != null) _platform.Shutdown();
                _platform = new ApplicationPlatform();
                return _platform;
            }
        }

        private static ILog Log;
        private readonly IServerFactory _serverFactory;

        private readonly ConcurrentDictionary<string, IServer> _servers = new ConcurrentDictionary<string, IServer>();
        private readonly ConcurrentDictionary<string, IServerInfo> _infos = new ConcurrentDictionary<string, IServerInfo>();

        internal ApplicationPlatform()
        {
            log4net.Config.XmlConfigurator.Configure();
            Log = LogManager.GetLogger(typeof (ApplicationPlatform));
            Log.Info("ApplicationPlatform started");
            _serverFactory = new ServerFactory(Log);
        }

        public Task Init()
        {
            LoadConfig();
            return CreateServers();
        }

        public Task Run()
        {
            return
                Task.WhenAll(
                    _servers.Values.Where(server => server.Info.Status == ServerStatus.Initialized).Select(RunServer));
        }

        private async Task CreateServers()
        {
            await Task.WhenAll(Configuration.ServerConfigurations.Select(CreateServer));
            Servers = _infos.Values;
        }
        public IPlatformConfiguration Configuration { get; private set; }
        public IEnumerable<IServerInfo> Servers { get; private set; }
        public void LoadConfig()
        {
            Configuration = ConfigurationManager.GetSection("platformConfiguration") as IPlatformConfiguration;
        }

        public async Task CreateServer(IServerConfiguration configuration)
        {
            IServer oldServer;
            var name = configuration.Name;
            if (_servers.TryGetValue(name, out oldServer))
            {
                if (oldServer.IsRunning) ShutdownServer(oldServer);
            }
            var server = _serverFactory.CreateServer(configuration);
            if (server == null)
            {
                Log.ErrorFormat("Can't create server from configuration. Invalid config for serverName : {0}", name);
                return;
            }
            await server.Init();
            _servers[name] = server;
            _infos[name] = server.Info;
        }

        public void Shutdown()
        {
            foreach (var server in _servers.Values)
            {
                if (server.IsRunning)  ShutdownServer(server);
            }
            _platform = null;
        }

        public void ShutdownServer(string name)
        {
            try
            {
                var server = _servers[name];
                ShutdownServer(server);
            }
            catch (KeyNotFoundException e)
            {
                Log.Error($"No such server [{name}]", e);
                throw;
            }
        }

        private void ShutdownServer(IServer server)
        {
            if (!server.IsRunning)
            {
                throw new InvalidOperationException($"Server [{server.Info.Name}] is not running, can't shutdown");
            }
            server.Shutdown();
        }

        public Task RunServer(string name)
        {
            try
            {
                var server = _servers[name];
                return RunServer(server);
            }
            catch (KeyNotFoundException e)
            {
                Log.Error($"No such server [{name}]", e);
                throw;
            }
        }

        private Task RunServer(IServer server)
        {
            if (server.Info.Status != ServerStatus.Initialized)
            {
                throw new InvalidOperationException($"Server [{server.Info.Name}] is not initialized or already running. Can't run");
            }
            return server.Run();
        }
    }
}
