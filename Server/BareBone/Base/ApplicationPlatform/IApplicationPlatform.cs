﻿using System.Collections.Generic;
using System.Threading.Tasks;
using BareBone.Configuration;
using Protocol;

namespace BareBone.Base.ApplicationPlatform
{
    public interface IApplicationPlatform
    {
        Task Init();
        Task Run();
        IPlatformConfiguration Configuration { get; }
        IEnumerable<IServerInfo> Servers { get; }
        void LoadConfig();
        Task CreateServer(IServerConfiguration configuration);
        void Shutdown();
        void ShutdownServer(string name);
        Task RunServer(string name);

    }
}