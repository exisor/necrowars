using System.Net;
using System.Net.Sockets;
using BareBone.Base.Peer;

namespace BareBone.Base.Listener
{
    public class ServerListener : Listener<ServerPeer>
    {
        public delegate ServerListener Factory(IPEndPoint endpoint, int maxClients, Socket socket);
        public ServerListener(
            IPEndPoint endpoint, int maxClients, Socket socket, ServerPeerFactory peerFactory, ServerConnectionCollection connectionCollection) 
            : base(endpoint, maxClients, socket, peerFactory: peerFactory, connectionCollection: connectionCollection)
        {
        }
    }
}