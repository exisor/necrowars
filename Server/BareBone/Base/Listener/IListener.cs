﻿
using System;
using System.Net;
using System.Threading.Tasks;

namespace BareBone.Base.Listener
{
    public interface IListener : IDisposable
    {
        IPEndPoint EndPoint { get; }
        Task Listen();
    }
}
