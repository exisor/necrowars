﻿using Protocol;

namespace BareBone.Base.Listener
{
    internal interface IListenerFactory
    {
        IListener CreateListener(IListenerInfo listenerInfo);
    }
}
