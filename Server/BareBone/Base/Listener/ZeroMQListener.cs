﻿using System.Net;
using System.Threading;
using System.Threading.Tasks;
using BareBone.Base.Peer;
using ZeroMQ;

namespace BareBone.Base.Listener
{
    public abstract class ZeroMQListener<TPeer> : IListener
        where TPeer : IPeer
    {
        private readonly ZContext _context;
        private IPEndPoint _endpoint;
        private int _maxClients;
        private IPeerFactory<TPeer> _peerFactory;
        private IConnectionCollection<TPeer> _connectionCollection;
        private readonly CancellationTokenSource _cts = new CancellationTokenSource();

        protected ZeroMQListener(
            IPEndPoint endpoint,
            int maxClients,
            IPeerFactory<TPeer> peerFactory,
            IConnectionCollection<TPeer> connectionCollection)
        {
            _context = new ZContext();
            _endpoint = endpoint;
            _maxClients = maxClients;
            _peerFactory = peerFactory;
            _connectionCollection = connectionCollection;
        }

        public void Dispose()
        {
            _cts.Cancel();
            _context.Dispose();
        }

        public IPEndPoint EndPoint { get; }

        public Task Listen()
        {
            return Task.Factory.StartNew(() =>
            {
                using (var socket = new ZSocket(_context, ZSocketType.ROUTER))
                {
                    socket.Bind($"tcp://{_endpoint.Address}:{_endpoint.Port}");
                    //socket.Rece
                }
            }, _cts.Token, TaskCreationOptions.LongRunning, TaskScheduler.Current);
        }
    }
}