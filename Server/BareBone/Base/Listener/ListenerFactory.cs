﻿using System;
using System.Net;
using System.Net.Sockets;
using Protocol;
using SocketType = Protocol.SocketType;

namespace BareBone.Base.Listener
{
    public class ListenerFactory : IListenerFactory
    {
        private readonly ClientListener.Factory _clientListenerFactory;
        private readonly ServerListener.Factory _serverListenerFactory;

        public ListenerFactory(ClientListener.Factory clientListenerFactory, ServerListener.Factory serverListenerFactory)
        {
            _clientListenerFactory = clientListenerFactory;
            _serverListenerFactory = serverListenerFactory;
        }

        public IListener CreateListener(IListenerInfo listenerInfo)
        {
            Socket socket;
            switch (listenerInfo.SocketType)
            {
                case SocketType.Tcp:
                    socket = new Socket(AddressFamily.InterNetwork, System.Net.Sockets.SocketType.Stream, ProtocolType.Tcp);
                    break;
                case SocketType.WebSocket:
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }
            switch (listenerInfo.Type)
            {
                case ListenerType.Client:
                    return _clientListenerFactory(new IPEndPoint(IPAddress.Parse(listenerInfo.Address), listenerInfo.Port), listenerInfo.MaxClients);
                case ListenerType.Server:
                    return _serverListenerFactory(new IPEndPoint(IPAddress.Parse(listenerInfo.Address), listenerInfo.Port), listenerInfo.MaxClients);
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }
    }
}