﻿using System;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using System.Threading.Tasks;
using BareBone.Base.Peer;

namespace BareBone.Base.Listener
{
    public abstract class Listener<TPeer> : IListener
        where TPeer : IPeer
    {
        private readonly Socket _socket;
        private readonly int _maxClients;
        private readonly CancellationTokenSource _cts = new CancellationTokenSource();
        private readonly IPeerFactory<TPeer> _peerFactory;
        private readonly IConnectionCollection<TPeer> _connectionCollection;

        protected Listener(IPEndPoint endpoint, int maxClients, Socket socket, IPeerFactory<TPeer> peerFactory, IConnectionCollection<TPeer> connectionCollection)
        {
            _peerFactory = peerFactory;
            _connectionCollection = connectionCollection;
            EndPoint = endpoint;
            _socket = socket;
            _maxClients = maxClients;
        }

        public IPEndPoint EndPoint { get; }

        public Task Listen()
        {
            _socket.Bind(EndPoint);
            _socket.Listen(_maxClients);
            return Task.Factory.StartNew(async () =>
            {
                while (!_cts.Token.IsCancellationRequested)
                {
                    Socket clientSocket;
                    try
                    {
                        clientSocket =
                            await
                                Task.Factory.FromAsync(_socket.BeginAccept, _socket.EndAccept, true)
                                    .ConfigureAwait(false);
                    }
                    catch (ObjectDisposedException)
                    {
                        // suppress intentionally
                        break;
                    }
                    var peer = _peerFactory.CreatePeer(clientSocket);
                    _connectionCollection.Connect(peer);
                }
            });
        }

        public void Dispose()
        {
            _socket.Close();
        }
    }
}