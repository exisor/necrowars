﻿using System.Net;
using System.Net.Sockets;
using BareBone.Base.Peer;

namespace BareBone.Base.Listener
{
    public class ClientListener : Listener<ClientPeer>
    {
        public delegate ClientListener Factory(IPEndPoint endpoint, int maxClients, Socket socket);
        public ClientListener(IPEndPoint endpoint, int maxClients, Socket socket, ClientPeerFactory peerFactory, ClientConnectionCollection connectionCollection) 
            : base(endpoint, maxClients, socket, peerFactory: peerFactory, connectionCollection: connectionCollection)
        {
        }
    }
}