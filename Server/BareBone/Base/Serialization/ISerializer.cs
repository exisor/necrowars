﻿using System;
using System.IO;
using System.Reflection;

namespace BareBone.Base.Serialization
{
    public interface ISerializer
    {
        void AddMessagesFromAssembly(Assembly assembly);
        void AddMessage(Type messageType);
        void Serialize<T>(Stream stream, T data);
        void Deserialize<T>(Stream stream, T data);
        string GetProto<T>();
    }
}
