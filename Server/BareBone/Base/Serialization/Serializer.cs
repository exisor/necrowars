﻿using System;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.InteropServices;
using ProtoBuf;
using ProtoBuf.Meta;
using Protocol;

namespace BareBone.Base.Serialization
{
    public class Serializer : ISerializer
    {
        private readonly RuntimeTypeModel _model;
        private readonly MetaType _messageMeta;
        public Serializer()
        {
            _model = TypeModel.Create();
            _messageMeta = _model.Add(typeof(Message), true);
        }

        public void AddMessagesFromAssembly(Assembly assembly)
        {
            assembly.GetTypes()
                .Where(t => t.IsDefined(typeof (ProtoSubContract), false) && t.BaseType == typeof (Message)).ToList()
                .ForEach(AddMessage);
        }

        public void AddMessage(Type messageType)
        {
            var tag = messageType.GetCustomAttribute<ProtoSubContract>().Tag;
            var subtype =
                _messageMeta.GetSubtypes()
                    .FirstOrDefault(s => s.FieldNumber == tag && s.DerivedType.Type == messageType);
            if (subtype == null)
            {
                _model.Add(messageType, true);
                _messageMeta.AddSubType(tag, messageType);
            }
        }

        public void Serialize<T>(Stream stream, T data)
        {
            _model.SerializeWithLengthPrefix(stream, data, data.GetType(), PrefixStyle.Fixed32, 0);
        }

        public void Deserialize<T>(Stream stream, [In, Out] T data)
        {
            _model.DeserializeWithLengthPrefix(stream, data, typeof (T), PrefixStyle.Fixed32, 0);
        }

        public string GetProto<T>()
        {
            return _model.GetSchema(typeof (T));
        }
    }
}