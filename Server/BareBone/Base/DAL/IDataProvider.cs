﻿using System;
using System.Data;
using System.Threading.Tasks;

namespace BareBone.Base.DAL
{
    public interface IDataProvider
    {
        IDbConnection Connect();
        Task Transaction(Func<IDbConnection, Task> executeFunc);
    }
}
