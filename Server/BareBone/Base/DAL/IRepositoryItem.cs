﻿using System;
using System.Threading.Tasks;

namespace BareBone.Base.DAL
{
    public interface IRepositoryItem<out TData> : IDisposable
    {
        TData Value { get; }
        Task Update();
        Task Insert();
        Task Delete();
    }
}
