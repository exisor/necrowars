﻿using System.Threading.Tasks;
using BareBone.Base.Pool;
using Dapper.FastCrud;

namespace BareBone.Base.DAL
{
    public abstract class RepositoryItem<TData> : PoolObjectWrapper<TData>, IRepositoryItem<TData>
        where TData : class
    {
        private readonly IDataProvider _dataProvider;

        protected RepositoryItem(IPool<TData> pool, TData original, IDataProvider dataProvider) : base(pool, original)
        {
            _dataProvider = dataProvider;
        }

        public virtual async Task Update()
        {
            await _dataProvider.Transaction(async _ =>
            {
                await _.UpdateAsync(Value);
            });
        }

        public virtual async Task Insert()
        {
            await _dataProvider.Transaction(async _ =>
            {
                await _.InsertAsync(Value);
            });
        }

        public virtual async Task Delete()
        {
            await _dataProvider.Transaction(async _ =>
            {
                await _.DeleteAsync(Value);
            });
        }

        public override void Dispose()
        {
            InitializeObject(Value);
            base.Dispose();
        }

        protected abstract void InitializeObject(TData obj);
    }
}