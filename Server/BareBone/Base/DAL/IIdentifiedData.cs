﻿namespace BareBone.Base.DAL
{
    public interface IIdentifiedData
    {
        long Id { get; set; }
    }
}
