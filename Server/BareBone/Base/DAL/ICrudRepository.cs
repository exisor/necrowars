﻿using System.Threading.Tasks;

namespace BareBone.Base.DAL
{
    public interface ICrudRepository<TData>
        where TData : class, IIdentifiedData
    {
        Task<TData> Get(long id);
        Task Insert(TData data);
        Task Update(TData data);
        Task Delete(TData data);
    }
}
