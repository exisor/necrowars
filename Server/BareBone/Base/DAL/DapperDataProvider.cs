﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Threading.Tasks;
using Dapper.FastCrud;
using Npgsql;

namespace BareBone.Base.DAL
{
    public class DapperDataProvider : IDataProvider
    {
        private const string CONNECTION_STRING = @"Server=127.0.0.1;Port=5432;Database=bitit;Uid=bitit;Pwd=bititpwd;";
        private readonly string _connectionString;

        public DapperDataProvider(string connectionString)
        {
            _connectionString = connectionString;
            OrmConfiguration.DefaultDialect = SqlDialect.PostgreSql;
        }
        public IDbConnection Connect()
        {
            return new NpgsqlConnection(_connectionString);
        }

        public async Task Transaction(Func<IDbConnection, Task> executeFunc)
        {
            try
            {
                using (var conn = Connect())
                {
                    conn.Open();

                    await executeFunc(conn);
                }
            }
            catch (TimeoutException)
            {
                throw new Exception($"{GetType()} got SQLtimeout exception.");
            }
            catch (SqlException)
            {
                throw new Exception($"{GetType()} got SQL exception");
            }
        }
    }
}