using System.Threading.Tasks;
using Dapper.FastCrud;

namespace BareBone.Base.DAL
{
    public abstract class CrudRepository<TData> : ICrudRepository<TData> 
        where TData : class, IIdentifiedData, new()
    {
        protected readonly IDataProvider DataProvider;
        protected CrudRepository(IDataProvider dataProvider)
        {
            DataProvider = dataProvider;
        }

        public async Task<TData> Get(long id)
        {
            TData retval = null;
            await DataProvider.Transaction(async _ =>
            {
                retval = await _.GetAsync(new TData {Id = id});
            });
            return retval;
        }

        public Task Insert(TData data)
        {
            return DataProvider.Transaction(_ => _.InsertAsync(data));
        }

        public Task Update(TData data)
        {
            return DataProvider.Transaction(_ => _.UpdateAsync(data));
        }

        public Task Delete(TData data)
        {
            return DataProvider.Transaction(_ => _.DeleteAsync(data));
        }
    }
}