﻿namespace BareBone.Base.DAL
{
    public interface IRepositoryPool<out TData>
    {
        IRepositoryItem<TData> Get();
    }
}
