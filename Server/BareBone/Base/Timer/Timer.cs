﻿using System;
using System.Diagnostics;
using System.Threading;
using System.Threading.Tasks;

namespace BareBone.Base.Timer
{
    public class Timer : ITimer
    {
        private readonly CancellationTokenSource _cancellationTokenSource;
        private readonly CancellationToken _cancellationToken;

        private readonly long _interval;
        private long _framesSkippedFromLast;

        private Task _timedTask;
        private readonly Action _timedAction;

        public Timer(Action timedAction, float tickRate)
        {
            _cancellationTokenSource = new CancellationTokenSource();
            _cancellationToken = CancellationToken.None;
            _interval = (long)(1000.0f / tickRate);
            _timedAction = timedAction;
        }

        public void Start()
        {
            if (_timedTask != null)
            {
                throw new InvalidOperationException("Task has already started");
            }
            var watch = new Stopwatch();

            _timedTask = Task.Factory.StartNew(async () =>
            {
                for (; !_cancellationToken.IsCancellationRequested;)
                {
                    if (watch.IsRunning)
                    {
                    }
                    else
                    {
                        await Task.Factory.StartNew(() =>
                        {
                            watch.Start();
                            _timedAction.Invoke();
                            watch.Stop();
                        }, _cancellationToken);
                    }
                    var delay = _interval - watch.ElapsedMilliseconds % _interval;
                    _framesSkippedFromLast = watch.ElapsedMilliseconds/_interval;
                    SkippedFrames += _framesSkippedFromLast;
                    watch.Reset();
                    await Task.Delay((int)delay, _cancellationToken);
                }
            }, _cancellationToken);
        }

        public void Stop()
        {
            _cancellationTokenSource.Cancel();
        }

        public long SkippedFrames { get; private set; }
    }
}
