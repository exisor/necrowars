﻿namespace BareBone.Base.Timer
{
    public interface ITimer
    {
        void Start();
        void Stop();
        long SkippedFrames { get; }
    }
}
