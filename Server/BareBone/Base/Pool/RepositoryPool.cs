﻿using System;
using BareBone.Base.DAL;

namespace BareBone.Base.Pool
{
    public class RepositoryPool<TPooled> : Pool<TPooled, RepositoryItem<TPooled>>, IRepositoryPool<TPooled>
        where TPooled : class
    {
        public new IRepositoryItem<TPooled> Get()
        {
            return base.Get() as IRepositoryItem<TPooled>;
        }

        public RepositoryPool(Func<IPool<TPooled>, TPooled, IPoolObjectWrapper<TPooled>> wrapperFactory) : base(wrapperFactory)
        {

        }
    }
}