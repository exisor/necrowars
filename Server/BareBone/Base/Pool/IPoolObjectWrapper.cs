﻿using System;

namespace BareBone.Base.Pool
{
    public interface IPoolObjectWrapper<out TObject> : IDisposable
        where TObject : class
    {
        TObject Value { get; }
    }
}
