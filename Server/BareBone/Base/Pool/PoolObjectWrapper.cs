﻿namespace BareBone.Base.Pool
{
    public class PoolObjectWrapper<TObject> : IPoolObjectWrapper<TObject>
        where TObject : class
    {
        private readonly IPool<TObject> _pool;
        public PoolObjectWrapper(IPool<TObject> pool, TObject original)
        {
            _pool = pool;
            Value = original;
        }

        public virtual void Dispose()
        {
            _pool.Release(this);
        }

        public TObject Value { get; }
    }
}