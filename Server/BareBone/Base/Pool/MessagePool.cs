﻿using System;
using Protocol;

namespace BareBone.Base.Pool
{
    public class MessagePool<TMessage> : Pool<TMessage, PoolObjectWrapper<TMessage>>
        where TMessage : Message
    {
        public MessagePool() : base((pool, value) => new PoolObjectWrapper<TMessage>(pool, value))
        {
        }
    }
}