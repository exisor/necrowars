﻿using System;
using System.Collections.Concurrent;
using System.Linq.Expressions;
using System.Runtime.CompilerServices;

namespace BareBone.Base.Pool
{
    public abstract class Pool<TPooled, TWrapper> : IPool<TPooled>
        where TPooled : class
        where TWrapper : class, IPoolObjectWrapper<TPooled>
    {
        private readonly ConcurrentBag<TWrapper> _bag = new ConcurrentBag<TWrapper>();
        private readonly Func<TPooled> _factory;
        private readonly Func<IPool<TPooled>, TPooled, IPoolObjectWrapper<TPooled>> _wrapperFactory;
        protected Pool(Func<IPool<TPooled>, TPooled, IPoolObjectWrapper<TPooled>> wrapperFactory)
            :this(Expression.Lambda<Func<TPooled>>(Expression.New(typeof(TPooled).GetConstructor(Type.EmptyTypes))).Compile(), 
                 wrapperFactory)
        {
        }

        protected Pool(Func<TPooled> constructor, Func<IPool<TPooled>, TPooled, IPoolObjectWrapper<TPooled>> wrapperFactory)
        {
            _factory = constructor;
            _wrapperFactory = wrapperFactory;
        }

        public IPoolObjectWrapper< TPooled > Get()
        {
            TWrapper retval;
            return _bag.TryTake(out retval) ? retval : _wrapperFactory(this, _factory());
        }

        public void Release(IPoolObjectWrapper<TPooled> pooledObject)
        {
            _bag.Add(pooledObject as TWrapper);
        }
    }
}