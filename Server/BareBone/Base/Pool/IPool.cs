﻿namespace BareBone.Base.Pool
{
    public interface IPool<TPooled>
        where TPooled : class
    {
        IPoolObjectWrapper<TPooled> Get();
        void Release(IPoolObjectWrapper<TPooled> pooledObject);
    }
}
