﻿using System.Collections.Generic;
using System.Reflection;
using System.Threading.Tasks;
using Autofac;
using BareBone.Base.Server;
using BareBone.Configuration;
using MasterServer.Logic.GamesManager;
using MasterServer.Logic.ServerBalancer;
using MasterServer.Logic.ServersManager;
using MasterServer.Protocol;

namespace MasterServer.Application
{
    public class MasterServer : ServerBase
    {
        public MasterServer(IServerConfiguration configuration) : base(configuration) {}

        protected override void MapBindings(ContainerBuilder builder)
        {
            builder.RegisterType<GamesManager>().As<IGamesManager>().SingleInstance();
            builder.RegisterType<ServersManager>();
            builder.RegisterType<ServersManagerFactory>().As<IServersManagerFactory>();
            builder.RegisterType<ServerBalancerFactory>().As<IServerBalancerFactory>();
        }
#pragma warning disable CS1998
        protected override async Task OnStart()
        {
        }
#pragma warning restore CS1998
        protected override IEnumerable<Assembly> ProtocolAssemblies => new List<Assembly> { Constants.PROTOCOL_ASSEMBLY };
    }
}
