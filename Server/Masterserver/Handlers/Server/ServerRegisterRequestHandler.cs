﻿using System.Threading.Tasks;
using BareBone.Base.Handlers;
using BareBone.Base.Peer;
using BareBone.Base.Pool;
using log4net;
using MasterServer.Logic.GamesManager;
using MasterServer.Protocol.Messages.Request;
using MasterServer.Protocol.Messages.Response;

namespace MasterServer.Handlers.Server
{
    public class ServerRegisterRequestHandler : ServerMessageHandler<ServerRegisterRequest>
    {
        private readonly IGamesManager _gamesManager;
        private readonly MessagePool<ServerRegisterResponse> _responsePool = new MessagePool<ServerRegisterResponse>();
        public ServerRegisterRequestHandler(ILog logger, IGamesManager gamesManager) : base(logger)
        {
            _gamesManager = gamesManager;
        }

        protected override async Task Handle(ServerRegisterRequest message, ServerPeer peer)
        {
            using (var responseWrapper = _responsePool.Get())
            {
                var response = responseWrapper.Value;
                response.Success = _gamesManager.GetOrCreateServersManager(message.GameName)
                    .RegisterServer(message.Role, peer);
                if (response.Success)
                {
                    peer.ClientAddress = message.Address;
                    peer.ClientPort = message.Port;
                    peer.Game = message.GameName;
                }
                await peer.Send(response);
            }
        }
    }
}
