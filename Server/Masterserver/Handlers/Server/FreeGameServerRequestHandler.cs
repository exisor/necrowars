﻿using System.Threading.Tasks;
using BareBone.Base.Handlers;
using BareBone.Base.Peer;
using BareBone.Base.Pool;
using log4net;
using MasterServer.Logic.GamesManager;
using MasterServer.Protocol;
using MasterServer.Protocol.Messages.Request;
using MasterServer.Protocol.Messages.Response;
using Protocol;

namespace MasterServer.Handlers.Server
{
    public class FreeGameServerRequestHandler : ServerMessageHandler<FreeGameServerRequest>
    {
        private readonly MessagePool<FreeGameServerResponse> _responsePool = new MessagePool<FreeGameServerResponse>();
        private readonly IGamesManager _gamesManager;
        public FreeGameServerRequestHandler(ILog logger, IGamesManager gamesManager) : base(logger)
        {
            _gamesManager = gamesManager;
        }

        protected override Task Handle(FreeGameServerRequest message, ServerPeer peer)
        {
            using (var wrapper = _responsePool.Get())
            {
                var response = wrapper.Value;

                response.Address = null;
                response.Port = 0;

                var serversManager = _gamesManager.GetServersManager(message.Game);
                if (serversManager == null)
                {
                    response.Status = FreeGameServerResponse.Error.GameIsNotRegistered;
                    return peer.Send(response);
                }

                var server = serversManager.GetServer(ServerRole.Lobby);
                if (server == null)
                {
                    response.Status = FreeGameServerResponse.Error.AllServersIsBusy;
                    return peer.Send(response);
                }

                response.Address = server.ClientAddress;
                response.Port = server.ClientPort;
                return peer.Send(response);
            }
        }
    }
}
