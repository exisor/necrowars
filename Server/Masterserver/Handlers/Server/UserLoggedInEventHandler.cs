﻿using System.Threading.Tasks;
using BareBone.Base.Handlers;
using BareBone.Base.Peer;
using log4net;
using MasterServer.Logic.GamesManager;
using MasterServer.Protocol.Messages.Event;

namespace MasterServer.Handlers.Server
{
    public class UserLoggedInEventHandler : ServerMessageHandler<UserLoggedInEvent>
    {
        private readonly IGamesManager _gamesManager;
        public UserLoggedInEventHandler(ILog logger, IGamesManager gamesManager) : base(logger)
        {
            _gamesManager = gamesManager;
        }

        protected override async Task Handle(UserLoggedInEvent message, ServerPeer peer)
        {
            var serversManager = _gamesManager.GetServersManager(peer.Game);
            if (serversManager == null)
            {
                Log.Warn($"No server manager for game {peer.Game}");
                return;
            }
            await serversManager.NotifyAboutUserLoggedIn(message);
        }
    }
}
