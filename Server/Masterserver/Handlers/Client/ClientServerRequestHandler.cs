﻿
using System.Threading.Tasks;
using BareBone.Base.Handlers;
using BareBone.Base.Peer;
using BareBone.Base.Pool;
using log4net;
using MasterServer.Logic.GamesManager;
using MasterServer.Protocol.Messages.Request;
using MasterServer.Protocol.Messages.Response;

namespace MasterServer.Handlers.Client
{
    public class ClientServerRequestHandler : ClientMessageHandler<ClientServerRequest>
    {
        private readonly MessagePool<ClientServerRequestResponse> _responsePool = new MessagePool<ClientServerRequestResponse>();
        private readonly IGamesManager _gamesManager;
        public ClientServerRequestHandler(ILog logger, IGamesManager gamesManager) : base(logger)
        {
            _gamesManager = gamesManager;
        }

        protected override async Task Handle(ClientServerRequest message, ClientPeer peer)
        {
            using (var responseWrapper = _responsePool.Get())
            {
                var response = responseWrapper.Value;
                var manager = _gamesManager.GetServersManager(message.Game);
                if (manager == null)
                {
                    response.Error = ClientServerRequestResponseError.GameIsNotRegistered;
                    await peer.Send(response);
                    return;
                }
                var server = manager.GetServer(message.Role);
                if (server == null)
                {
                    response.Error = ClientServerRequestResponseError.ServerIsNotAvailable;
                    await peer.Send(response);
                    return;
                }
                response.Address = server.ClientAddress;
                response.Port = server.ClientPort;
                response.Error = ClientServerRequestResponseError.OK;
                await peer.Send(response);
            }
        }
    }
}
