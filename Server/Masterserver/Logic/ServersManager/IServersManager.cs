﻿using System.Threading.Tasks;
using BareBone.Base.Peer;
using MasterServer.Protocol.Messages.Event;
using Protocol;

namespace MasterServer.Logic.ServersManager
{
    public interface IServersManager
    {
        bool RegisterServer(ServerRole role, ServerPeer serverPeer);
        ServerPeer GetServer(ServerRole role);
        Task NotifyAboutUserLoggedIn(UserLoggedInEvent message);
        string Game { get; }
    }
}
