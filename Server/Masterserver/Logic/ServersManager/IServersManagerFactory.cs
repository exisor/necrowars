﻿namespace MasterServer.Logic.ServersManager
{
    internal interface IServersManagerFactory
    {
        IServersManager Create(string gameName);
    }
}
