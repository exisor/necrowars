﻿using System.Collections.Concurrent;
using System.Linq;
using System.Threading.Tasks;
using BareBone.Base.Peer;
using log4net;
using MasterServer.Logic.ServerBalancer;
using MasterServer.Protocol.Messages.Event;
using Protocol;

namespace MasterServer.Logic.ServersManager
{
    public class ServersManager : IServersManager
    {
        public delegate ServersManager Factory(string gameName);

        private readonly IServerBalancerFactory _serverBalancerFactory;
        private readonly ConcurrentDictionary<ServerRole, IServerBalancer> _balancers = new ConcurrentDictionary<ServerRole, IServerBalancer>();
        private readonly ILog _log;

        public ServersManager(string gameName, IServerBalancerFactory serverBalancerFactory, ILog log)
        {
            Game = gameName;
            _serverBalancerFactory = serverBalancerFactory;
            _log = log;
        }

        public bool RegisterServer(ServerRole role, ServerPeer serverPeer)
        {
            lock (_balancers)
            {
                IServerBalancer balancer;
                if (!_balancers.TryGetValue(role, out balancer))
                {
                    balancer = _serverBalancerFactory.Create(role);
                    _balancers[role] = balancer;
                }
                balancer.AddServer(serverPeer);
                return true;
            }
        }

        public ServerPeer GetServer(ServerRole role)
        {
            IServerBalancer balancer;
            if (!_balancers.TryGetValue(role, out balancer))
            {
                _log.Warn($"No servers of type : [{role}] available");
                return null;
            }
            var retval = balancer.TryGetServer();
            if (retval == null)
            {
                _log.Warn($"All servers of type : [{role}] is busy");
                return null;
            }
            return retval;
        }

        public Task NotifyAboutUserLoggedIn(UserLoggedInEvent message)
        {
            return Task.WhenAll(_balancers.Values.Where(b => b.Role != ServerRole.Login && b.Role != ServerRole.Master).Select(balancer => balancer.NotifyAboutUserLoggedIn(message)));
        }

        public string Game { get; }
    }
}