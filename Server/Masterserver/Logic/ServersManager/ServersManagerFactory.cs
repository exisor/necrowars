namespace MasterServer.Logic.ServersManager
{
    public class ServersManagerFactory : IServersManagerFactory
    {
        private readonly ServersManager.Factory _factory;

        public ServersManagerFactory(ServersManager.Factory factory)
        {
            _factory = factory;
        }

        public IServersManager Create(string gameName)
        {
            return _factory(gameName);
        }
    }
}