﻿using MasterServer.Logic.ServersManager;

namespace MasterServer.Logic.GamesManager
{
    public interface IGamesManager
    {
        IServersManager GetServersManager(string gameName);
        IServersManager GetOrCreateServersManager(string gameName);
    }
}
