using System.Collections.Concurrent;
using MasterServer.Logic.ServersManager;

namespace MasterServer.Logic.GamesManager
{
    class GamesManager : IGamesManager
    {
        private readonly IServersManagerFactory _serversManagerFactory;
        private readonly ConcurrentDictionary<string, IServersManager> _serversManagers = new ConcurrentDictionary<string, IServersManager>();

        public GamesManager(IServersManagerFactory serversManagerFactory)
        {
            _serversManagerFactory = serversManagerFactory;
        }

        public IServersManager GetServersManager(string gameName)
        {
            IServersManager retval;
            _serversManagers.TryGetValue(gameName, out retval);
            return retval;
        }

        public IServersManager GetOrCreateServersManager(string gameName)
        {
            lock (_serversManagers)
            {
                var retval = GetServersManager(gameName);
                if (retval == null)
                {
                    retval = _serversManagerFactory.Create(gameName);
                    _serversManagers[gameName] = retval;
                }
                return retval;
            }
        }
    }
}