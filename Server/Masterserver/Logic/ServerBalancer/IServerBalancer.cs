﻿using System.Threading.Tasks;
using BareBone.Base.Peer;
using MasterServer.Protocol;
using MasterServer.Protocol.Messages.Event;
using Protocol;

namespace MasterServer.Logic.ServerBalancer
{
    public interface IServerBalancer
    {
        void AddServer(ServerPeer serverPeer);
        ServerPeer TryGetServer();
        ServerRole Role { get; }
        Task NotifyAboutUserLoggedIn(UserLoggedInEvent meesage);
    }
}
