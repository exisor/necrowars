﻿using MasterServer.Protocol;
using Protocol;

namespace MasterServer.Logic.ServerBalancer
{
    public class ServerBalancerFactory : IServerBalancerFactory
    {
        public IServerBalancer Create(ServerRole role)
        {
            return new FirstServerBalancer(role);
        }
    }
}