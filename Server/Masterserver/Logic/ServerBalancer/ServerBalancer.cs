using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BareBone.Base.Peer;
using MasterServer.Protocol.Messages.Event;
using Protocol;

namespace MasterServer.Logic.ServerBalancer
{
    public abstract class ServerBalancer : IServerBalancer
    {
        protected readonly List<ServerPeer> Servers = new List<ServerPeer>();
        public abstract void AddServer(ServerPeer serverPeer);

        public abstract ServerPeer TryGetServer();

        public abstract ServerRole Role { get; }
        public Task NotifyAboutUserLoggedIn(UserLoggedInEvent message)
        {
            return Task.WhenAll(Servers.Select(server => server.Send(message)));
        }
    }
}