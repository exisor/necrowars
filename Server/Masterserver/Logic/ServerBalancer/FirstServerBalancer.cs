using System.Collections.Generic;
using System.Linq;
using BareBone.Base.Peer;
using Protocol;

namespace MasterServer.Logic.ServerBalancer
{
    public class FirstServerBalancer : ServerBalancer
    {
        public FirstServerBalancer(ServerRole role)
        {
            Role = role;
        }
        public override void AddServer(ServerPeer serverPeer)
        {
            Servers.Add(serverPeer);
            serverPeer.Disconnecting += () => Servers.Remove(serverPeer);
        }

        public override ServerPeer TryGetServer()
        {
            return Servers.FirstOrDefault();
        }

        public override ServerRole Role { get; }
    }
}