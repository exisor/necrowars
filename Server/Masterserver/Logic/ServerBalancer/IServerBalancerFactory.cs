﻿

using MasterServer.Protocol;
using Protocol;

namespace MasterServer.Logic.ServerBalancer
{
    public interface IServerBalancerFactory
    {
        IServerBalancer Create(ServerRole role);
    }
}
